package com.frost.leap.ots;

import com.frost.leap.utils.TextUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 26/08/20.
 * <p>
 * Frost Interactive
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Utility.class)
public class ClaimOTSUnitTest {

    boolean expertResult;
    boolean actualResult;

    String profilePic = "";
    String fatherName = "";
    String highestQualification = "";
    String collegeName = "";
    String currentStatus = "";
    String selectGoal = "";
    String password = "";

    String address = "";
    String city = "";
    String state = "";
    String pincode = "";
    String frontSideID = "";
    String backSideID = "";

    @Mock
    TextUtils textUtils;

    @Mock
    Utility utility;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void doProfilePicEmptyCheckValidation() {
        profilePic = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(profilePic);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doProfilePicNullCheckValidation() {
        profilePic = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(profilePic);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doProfilePicValidCheckValidation() {
        profilePic = "j32987afajdiooirfasjfProfilePic.jpg";

        expertResult = false;
        actualResult = textUtils.isEmpty(profilePic);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doFatherNameEmptyCheckValidation() {
        fatherName = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(fatherName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doFatherNameNullCheckValidation() {
        fatherName = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(fatherName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doFatherNameValidCheckValidation() {
        fatherName = "Chennakesavulug";

        expertResult = false;
        actualResult = textUtils.isEmpty(fatherName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dohighestQualificationEmptyCheckValidation() {
        highestQualification = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(highestQualification);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dohighestQualificationNullCheckValidation() {
        highestQualification = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(highestQualification);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dohighestQualificationValidCheckValidation() {
        highestQualification = "B.Tech";

        expertResult = false;
        actualResult = textUtils.isEmpty(highestQualification);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void docollegeNameEmptyCheckValidation() {
        collegeName = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(collegeName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void docollegeNameNullCheckValidation() {
        collegeName = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(collegeName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void docollegeNameValidCheckValidation() {
        collegeName = "Mallareddy College";

        expertResult = false;
        actualResult = textUtils.isEmpty(collegeName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmploymentStatusEmptyCheckValidation() {
        currentStatus = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(currentStatus);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmploymentStatusNullCheckValidation() {
        currentStatus = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(currentStatus);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmploymentStatusValidCheckValidation() {
        currentStatus = "chenna123";

        expertResult = false;
        actualResult = textUtils.isEmpty(currentStatus);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doselectGoalEmptyCheckValidation() {
        selectGoal = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(selectGoal);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doselectGoalNullCheckValidation() {
        selectGoal = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(selectGoal);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doselectGoalValidCheckValidation() {
        selectGoal = "chenna123";

        expertResult = false;
        actualResult = textUtils.isEmpty(selectGoal);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doPasswordEmptyCheckValidation() {
        password = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(password);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doPasswordNullCheckValidation() {
        password = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(password);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doPasswordValidCheckValidation() {
        password = "chenna123";

        expertResult = false;
        actualResult = textUtils.isEmpty(password);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doaddressEmptyCheckValidation() {
        address = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(address);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doaddressdNullCheckValidation() {
        address = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(address);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doaddressValidCheckValidation() {
        address = "3-311/A,Boggarapuvari Street";

        expertResult = false;
        actualResult = textUtils.isEmpty(address);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void docityEmptyCheckValidation() {
        city = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(city);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void docitydNullCheckValidation() {
        city = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(city);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void docityValidCheckValidation() {
        city = "Markapuram";

        expertResult = false;
        actualResult = textUtils.isEmpty(city);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dostateEmptyCheckValidation() {
        state = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(state);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dostateNullCheckValidation() {
        state = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(state);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dostateValidCheckValidation() {
        state = "Andra Pradesh";

        expertResult = false;
        actualResult = textUtils.isEmpty(state);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dopincodeEmptyCheckValidation() {
        pincode = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(pincode);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dopincodeNullCheckValidation() {
        pincode = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(pincode);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dopincodeValidCheckValidation() {
        pincode = "523316";

        expertResult = false;
        actualResult = textUtils.isEmpty(pincode);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dofrontSideIDEmptyCheckValidation() {
        frontSideID = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(frontSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dofrontSideIDNullCheckValidation() {
        frontSideID = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(frontSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dofrontSideIDalidCheckValidation() {
        frontSideID = "frontsideIDProof.jpg";

        expertResult = false;
        actualResult = textUtils.isEmpty(frontSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dobackSideIDEmptyCheckValidation() {
        backSideID = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(backSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dobackSideIDNullCheckValidation() {
        backSideID = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(backSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dobackSideIDalidCheckValidation() {
        backSideID = "backsideIDProof.jpg";

        expertResult = false;
        actualResult = textUtils.isEmpty(backSideID);
        Assert.assertEquals(expertResult, actualResult);
    }
}
