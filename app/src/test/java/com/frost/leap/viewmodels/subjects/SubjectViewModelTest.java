package com.frost.leap.viewmodels.subjects;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import apprepos.subject.SubjectRepositoryManager;
import apprepos.subject.model.SubjectModel;
import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 07-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({SubjectViewModel.class, Utility.class, BaseViewModel.class})
public class SubjectViewModelTest {

    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }


    @Test
    public void fetchSubjects_noInternetTest_throwsException() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getOfflineCatalogueSubjects("1111-2222-3333-4444")).thenReturn(Observable.error(new Exception("Test Exception")));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getOfflineCatalogueSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_noInternetTest_noData() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getOfflineCatalogueSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(new ArrayList<>()));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getOfflineCatalogueSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_noInternetTest_offlineData() throws InterruptedException {
        List<SubjectModel> dummySubjects = new ArrayList<>();
        dummySubjects.add(new SubjectModel());
        dummySubjects.add(new SubjectModel());
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getOfflineCatalogueSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(dummySubjects));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getOfflineCatalogueSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.updateSubjectsData().getValue());
        Assert.assertEquals(dummySubjects, subjectViewModel.updateSubjectsData().getValue());
    }


    @Test
    public void fetchSubjects_200_refresh_test() throws InterruptedException {
        List<SubjectModel> dummySubjects = new ArrayList<>();
        dummySubjects.add(new SubjectModel());
        dummySubjects.add(new SubjectModel());
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(dummySubjects)));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", true);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_200_test() throws InterruptedException {
        List<SubjectModel> dummySubjects = new ArrayList<>();
        dummySubjects.add(new SubjectModel());
        dummySubjects.add(new SubjectModel());
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(dummySubjects)));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        ReflectionTestUtils.setField(subjectViewModel, "subjectModelList", dummySubjects);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.DONE, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchSubjects_200_no_data() throws InterruptedException {
        List<SubjectModel> dummySubjects = new ArrayList<>();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(dummySubjects)));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);

        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_500_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.error(500,
                ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes()))));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        ReflectionTestUtils.setField(subjectViewModel, "baseUserRepositoryManager", userRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", true);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_401_to_200_test() throws InterruptedException {
        List<SubjectModel> dummySubjects = new ArrayList<>();
        dummySubjects.add(new SubjectModel());
        dummySubjects.add(new SubjectModel());

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        ReflectionTestUtils.setField(subjectViewModel, "baseUserRepositoryManager", userRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", true);
        Thread.sleep(1000);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(dummySubjects)));
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_error_test() throws InterruptedException {

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        SubjectRepositoryManager subjectRepositoryManager = PowerMockito.mock(SubjectRepositoryManager.class);
        PowerMockito.when(subjectRepositoryManager.getSubjects("1111-2222-3333-4444")).thenReturn(Observable.error(new Exception("Test exception")));

        SubjectViewModel subjectViewModel = new SubjectViewModel();
        ReflectionTestUtils.setField(subjectViewModel, "subjectRepositoryManager", subjectRepositoryManager);
        subjectViewModel.fetchSubjects("1111-2222-3333-4444", false);
        Mockito.verify(subjectRepositoryManager, Mockito.times(1)).getSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(subjectViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, subjectViewModel.getNetworkCallStatus().getValue().getNetworkStatus());

    }


}
