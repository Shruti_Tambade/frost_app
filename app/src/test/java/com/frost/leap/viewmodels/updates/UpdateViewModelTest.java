package com.frost.leap.viewmodels.updates;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import apprepos.updates.UpdateRepositoryManager;
import apprepos.updates.model.UpdateModel;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 04-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({UpdateViewModel.class, Utility.class})
public class UpdateViewModelTest {


    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Test
    public void fetchUpdateDateByID_noInternet_test() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        UpdateViewModel updateViewModel = new UpdateViewModel();
        updateViewModel.fetchUpdateDateByID("1111-2222-3333-4444");
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdateDateByID_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdateDataByID("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(new UpdateModel())));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdateDateByID("1111-2222-3333-4444");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdateDataByID("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdateDateByID_200_noData_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdateDataByID("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(null)));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdateDateByID("1111-2222-3333-4444");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdateDataByID("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchUpdateDateByID_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "update id not found");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdateDataByID("1111-2222-3333-4444")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdateDateByID("1111-2222-3333-4444");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdateDataByID("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdateDateByID_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdateDataByID("1111-2222-3333-4444")).thenReturn(Observable.error(new Exception("Test Exception")));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdateDateByID("1111-2222-3333-4444");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdateDataByID("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdatesList_noInternet_test() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        UpdateViewModel updateViewModel = new UpdateViewModel();
        updateViewModel.fetchUpdatesList(null, null, null);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchUpdatesList_200_test() throws InterruptedException {
        List<UpdateModel> list = new ArrayList<>();
        list.add(new UpdateModel());
        list.add(new UpdateModel());

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdatesList(new ArrayList<>(), null, null)).thenReturn(Observable.just(Response.success(list)));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdatesList(new ArrayList<>(), null, null);
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdatesList(new ArrayList<>(), null, null);
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdatesList_200_noData_test() throws InterruptedException {
        List<UpdateModel> list = new ArrayList<>();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdatesList(new ArrayList<>(), null, null)).thenReturn(Observable.just(Response.success(list)));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdatesList(new ArrayList<>(), null, null);
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdatesList(new ArrayList<>(), null, null);
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdatesList_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "catalogue ids not found");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdatesList(new ArrayList<>(), null, null)).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdatesList(new ArrayList<>(), null, null);
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdatesList(new ArrayList<>(), null, null);
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUpdatesList_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.getUpdatesList(new ArrayList<>(), null, null)).thenReturn(Observable.error(new Exception("Test Exception")));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.fetchUpdatesList(new ArrayList<>(), null, null);
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).getUpdatesList(new ArrayList<>(), null, null);
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void joinUpdate_noInternet_test() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        UpdateViewModel updateViewModel = new UpdateViewModel();
        updateViewModel.joinUpdate(null, null);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void joinUpdate_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.joinMeeting("1111-2222-3333-4444", "YES")).thenReturn(Observable.just(Response.success(new JsonObject())));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.joinUpdate("1111-2222-3333-4444", "YES");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).joinMeeting("1111-2222-3333-4444", "YES");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void joinUpdate_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "update ids not found");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.joinMeeting("1111-2222-3333-4444", "YES")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.joinUpdate("1111-2222-3333-4444", "YES");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).joinMeeting("1111-2222-3333-4444", "YES");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void joinUpdate_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UpdateRepositoryManager updateRepositoryManager = PowerMockito.mock(UpdateRepositoryManager.class);
        PowerMockito.when(updateRepositoryManager.joinMeeting("1111-2222-3333-4444", "YES")).thenReturn(Observable.error(new Exception("Test Exception")));
        UpdateViewModel updateViewModel = new UpdateViewModel();
        ReflectionTestUtils.setField(updateViewModel, "updateRepositoryManager", updateRepositoryManager);
        updateViewModel.joinUpdate("1111-2222-3333-4444", "YES");
        Mockito.verify(updateRepositoryManager, Mockito.times(1)).joinMeeting("1111-2222-3333-4444", "YES");
        Thread.sleep(1000);
        Assert.assertNotNull(updateViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, updateViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }
}
