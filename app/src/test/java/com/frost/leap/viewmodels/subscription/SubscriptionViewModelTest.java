package com.frost.leap.viewmodels.subscription;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.catalogue.CatalogueViewModel;
import com.frost.leap.viewmodels.profile.InvoiceViewModel;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.CatalogueRepositoryManager;
import apprepos.catalogue.model.CatalogueModel;
import apprepos.user.UserRepositoryManager;
import apprepos.user.model.Invoice;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 09/09/20.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({CatalogueViewModel.class, Utility.class, InvoiceViewModel.class})
public class SubscriptionViewModelTest {

    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Test
    public void fetchInvoices_200_test() throws InterruptedException {
        List<Invoice> list = new ArrayList<>();
        list.add(new Invoice());
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(Observable.just(Response.success(list)));

        InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
        ReflectionTestUtils.setField(invoiceViewModel, "userRepositoryManager", userRepositoryManager);
        invoiceViewModel.fetchInvoiceHistory();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserInvoices();
        Thread.sleep(1000);
        Assert.assertNotNull(invoiceViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, invoiceViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchInvoices_noData_test() throws InterruptedException {
        List<Invoice> list = new ArrayList<>();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(Observable.just(Response.success(list)));
        InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
        ReflectionTestUtils.setField(invoiceViewModel, "userRepositoryManager", userRepositoryManager);
        invoiceViewModel.fetchInvoiceHistory();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserInvoices();
        Thread.sleep(1000);
        Assert.assertNotNull(invoiceViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, invoiceViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchInvoices_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "store removed");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
        ReflectionTestUtils.setField(invoiceViewModel, "userRepositoryManager", userRepositoryManager);
        invoiceViewModel.fetchInvoiceHistory();

        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserInvoices();
        Thread.sleep(1000);
        Assert.assertNotNull(invoiceViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, invoiceViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchInvoices_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
        ReflectionTestUtils.setField(invoiceViewModel, "userRepositoryManager", userRepositoryManager);
        invoiceViewModel.fetchInvoiceHistory();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserInvoices();
        Thread.sleep(1000);
        Assert.assertNotNull(invoiceViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, invoiceViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchInvoices_401_to_200_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
        ReflectionTestUtils.setField(invoiceViewModel, "userRepositoryManager", userRepositoryManager);
        invoiceViewModel.fetchInvoiceHistory();
        Thread.sleep(1000);
        List<Invoice> list = new ArrayList<>();
        list.add(new Invoice());
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(Observable.just(Response.success(list)));
        Thread.sleep(1000);
        Assert.assertNotNull(invoiceViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, invoiceViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchInvoices_error_test() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserInvoices()).thenReturn(
                Observable.error(new Exception("Test Exception"))
        );
        InvoiceViewModel invoiceViewModel = new InvoiceViewModel();
        ReflectionTestUtils.setField(invoiceViewModel, "userRepositoryManager", userRepositoryManager);
        invoiceViewModel.fetchInvoiceHistory();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserInvoices();
        Thread.sleep(1000);
        Assert.assertNotNull(invoiceViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, invoiceViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


}
