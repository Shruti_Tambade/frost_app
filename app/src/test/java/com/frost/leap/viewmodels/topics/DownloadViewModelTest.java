package com.frost.leap.viewmodels.topics;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.BaseViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 10-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DownloadViewModel.class, Utility.class, BaseViewModel.class})
public class DownloadViewModelTest {
    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }


}

