package com.frost.leap.viewmodels.subjects;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import apprepos.subject.NoteRepositoryManager;
import apprepos.subject.model.Note;
import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 07-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({NotesViewModel.class, Utility.class, BaseViewModel.class})
public class NotesViewModelTest {

    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Test
    public void fetchNotes_noInternet() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        NotesViewModel notesViewModel = new NotesViewModel();
        notesViewModel.fetchNotes(null);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchNotes_200_test() throws InterruptedException {
        List<String> studentSubjectIds = new ArrayList<>();
        studentSubjectIds.add("1");
        studentSubjectIds.add("2");
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));

        List<Note> dummyList = new ArrayList<>();
        dummyList.add(new Note());
        dummyList.add(new Note());

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        NoteRepositoryManager noteRepositoryManager = PowerMockito.mock(NoteRepositoryManager.class);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(Observable.just(Response.success(dummyList)));
        NotesViewModel notesViewModel = new NotesViewModel();
        ReflectionTestUtils.setField(notesViewModel, "repositoryManager", noteRepositoryManager);
        notesViewModel.fetchNotes(studentSubjectIds);
        Mockito.verify(noteRepositoryManager, Mockito.times(1)).getUserNotes(jsonObject);
        Thread.sleep(1000);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchNotes_200_noData() throws InterruptedException {
        List<String> studentSubjectIds = new ArrayList<>();
        studentSubjectIds.add("1");
        studentSubjectIds.add("2");
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));

        List<Note> dummyList = new ArrayList<>();

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        NoteRepositoryManager noteRepositoryManager = PowerMockito.mock(NoteRepositoryManager.class);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(Observable.just(Response.success(dummyList)));
        NotesViewModel notesViewModel = new NotesViewModel();
        ReflectionTestUtils.setField(notesViewModel, "repositoryManager", noteRepositoryManager);
        notesViewModel.fetchNotes(studentSubjectIds);
        Mockito.verify(noteRepositoryManager, Mockito.times(1)).getUserNotes(jsonObject);
        Thread.sleep(1000);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_500_test() throws InterruptedException {
        List<String> studentSubjectIds = new ArrayList<>();
        studentSubjectIds.add("1");
        studentSubjectIds.add("2");
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        NoteRepositoryManager noteRepositoryManager = PowerMockito.mock(NoteRepositoryManager.class);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(Observable.just(Response.error(500,
                ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes()))));
        NotesViewModel notesViewModel = new NotesViewModel();
        ReflectionTestUtils.setField(notesViewModel, "repositoryManager", noteRepositoryManager);
        notesViewModel.fetchNotes(studentSubjectIds);
        Mockito.verify(noteRepositoryManager, Mockito.times(1)).getUserNotes(jsonObject);
        Thread.sleep(1000);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_401_test() throws InterruptedException {

        List<String> studentSubjectIds = new ArrayList<>();
        studentSubjectIds.add("1");
        studentSubjectIds.add("2");
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));

        JsonObject error = new JsonObject();
        error.addProperty("message", "un authorized");

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        NoteRepositoryManager noteRepositoryManager = PowerMockito.mock(NoteRepositoryManager.class);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), error.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        NotesViewModel notesViewModel = new NotesViewModel();
        ReflectionTestUtils.setField(notesViewModel, "repositoryManager", noteRepositoryManager);
        ReflectionTestUtils.setField(notesViewModel, "baseUserRepositoryManager", userRepositoryManager);
        notesViewModel.fetchNotes(studentSubjectIds);
        Mockito.verify(noteRepositoryManager, Mockito.times(1)).getUserNotes(jsonObject);
        Thread.sleep(1000);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_401_to_200_test() throws InterruptedException {

        List<Note> dummyList = new ArrayList<>();
        dummyList.add(new Note());
        dummyList.add(new Note());


        List<String> studentSubjectIds = new ArrayList<>();
        studentSubjectIds.add("1");
        studentSubjectIds.add("2");
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));

        JsonObject errorJsonObject = new JsonObject();
        errorJsonObject.addProperty("message", "un authorized");

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        NoteRepositoryManager noteRepositoryManager = PowerMockito.mock(NoteRepositoryManager.class);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), errorJsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        NotesViewModel notesViewModel = new NotesViewModel();
        ReflectionTestUtils.setField(notesViewModel, "repositoryManager", noteRepositoryManager);
        ReflectionTestUtils.setField(notesViewModel, "baseUserRepositoryManager", userRepositoryManager);
        notesViewModel.fetchNotes(studentSubjectIds);
        Thread.sleep(1000);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(Observable.just(Response.success(dummyList)));
        Thread.sleep(1000);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_error_test() throws InterruptedException {

        List<String> studentSubjectIds = new ArrayList<>();
        studentSubjectIds.add("1");
        studentSubjectIds.add("2");
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        NoteRepositoryManager noteRepositoryManager = PowerMockito.mock(NoteRepositoryManager.class);
        PowerMockito.when(noteRepositoryManager.getUserNotes(jsonObject)).thenReturn(Observable.error(new Exception("Test exception")));

        NotesViewModel notesViewModel = new NotesViewModel();
        ReflectionTestUtils.setField(notesViewModel, "repositoryManager", noteRepositoryManager);
        notesViewModel.fetchNotes(studentSubjectIds);
        Mockito.verify(noteRepositoryManager, Mockito.times(1)).getUserNotes(jsonObject);
        Thread.sleep(1000);
        Assert.assertNotNull(notesViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, notesViewModel.getNetworkCallStatus().getValue().getNetworkStatus());

    }

    @Test
    public void filterNotes_all_null_test() {
        String ALL_NOTES = "[{\"studentNoteId\":\"21a2f4e3-12dc-4c03-aca9-4702d4f6a68e\",\"studentId\":null,\"studentSubjectId\":\"3102c612-8de9-4e28-ab72-b3fb5580e582\",\"studentUnitId\":\"6ff628d1-5f1f-46a9-a22b-382d36a11e11\",\"studentTopicId\":\"bfd409d7-e1a0-4fcb-9401-de9f8cbd8e0a\",\"studentSubtopicId\":\"4f26b253-b2c4-40b7-ad0b-64ddc3072691\",\"timestampInSecond\":2,\"videoLengthInSeconds\":1237,\"noteTitle\":null,\"note\":\"it's a good time to explore new a a s and a a a a a a a a a a a a a a a a a a a s\",\"topicName\":\"Limits\",\"createdDate\":\"2020-03-31T16:40:54.68\",\"subTopicName\":\"Limits\"},{\"studentNoteId\":\"9f6f3638-dad4-403a-afc4-a826c2245a64\",\"studentId\":null,\"studentSubjectId\":\"1dcb5c6c-89c7-49bf-918d-5afeb97ce3e9\",\"studentUnitId\":\"43c69d79-58ec-45b9-881c-74ffe97612c8\",\"studentTopicId\":\"13f4481b-02d4-4ade-87a8-419a8de48ad3\",\"studentSubtopicId\":\"7d7ec9c1-a774-43a6-adb1-988bd014df48\",\"timestampInSecond\":2,\"videoLengthInSeconds\":0,\"noteTitle\":null,\"note\":\"tell us what you think you are not the intended recipient you are not the intended recipient\",\"topicName\":\"Introduction to Verbal Ability\",\"createdDate\":\"2020-03-15T17:07:46.565\",\"subTopicName\":\"Introduction to Verbal Ability\"},{\"studentNoteId\":\"dd55ea52-6486-43bb-b9be-17bcf1ae93a3\",\"studentId\":null,\"studentSubjectId\":\"bf65d354-176c-48c3-8954-5ea6efc5afb7\",\"studentUnitId\":\"fd9b06e0-593c-416f-8730-039249bb989a\",\"studentTopicId\":\"beb3b9ac-75d6-4292-a99c-a7998808e061\",\"studentSubtopicId\":\"a3713a94-baa2-4d2f-93d6-816f5faf6a99\",\"timestampInSecond\":41,\"videoLengthInSeconds\":0,\"noteTitle\":null,\"note\":\"yes meri college life and the other hand\",\"topicName\":\"Laurent Series\",\"createdDate\":\"2020-03-15T17:07:25.03\",\"subTopicName\":\"Types of Singularities on the basis of Laurent Series Expansion\"},{\"studentNoteId\":\"8de7011f-1abd-4bd4-b6f5-d66cd8ea6a5e\",\"studentId\":null,\"studentSubjectId\":\"bf65d354-176c-48c3-8954-5ea6efc5afb7\",\"studentUnitId\":\"fd9b06e0-593c-416f-8730-039249bb989a\",\"studentTopicId\":\"beb3b9ac-75d6-4292-a99c-a7998808e061\",\"studentSubtopicId\":\"a3713a94-baa2-4d2f-93d6-816f5faf6a99\",\"timestampInSecond\":58,\"videoLengthInSeconds\":0,\"noteTitle\":null,\"note\":\"we are not the intended recipient you are not the intended recipient you are not the intended recipient you are not the intended recipient you are not\",\"topicName\":\"Laurent Series\",\"createdDate\":\"2020-03-15T17:06:58.689\",\"subTopicName\":\"Types of Singularities on the basis of Laurent Series Expansion\"}]";
        List<Note> noteList = new Gson().fromJson(ALL_NOTES, new TypeToken<List<Note>>() {
        }.getType());
        NotesViewModel notesViewModel = new NotesViewModel();
        notesViewModel.noteList = noteList;
        notesViewModel.filterNotes(null, null, null);
        Assert.assertNotNull(notesViewModel.updateNotesList().getValue());
        Assert.assertEquals(noteList, notesViewModel.updateNotesList().getValue());
    }

}
