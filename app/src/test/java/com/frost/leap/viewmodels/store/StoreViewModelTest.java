package com.frost.leap.viewmodels.store;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.SubCategoryModel;
import apprepos.store.StoreRepositoryManager;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.store.model.SubscriptionModel;
import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 04-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({StoreViewModel.class, Utility.class, BaseViewModel.class})
public class StoreViewModelTest {

    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());

    }


    @Test
    public void fetchStoreData_NoInternetTest() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        StoreViewModel storeViewModel = new StoreViewModel();
        storeViewModel.fetchStoreData();
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchStoreData_200_test() throws InterruptedException {
        List<SubCategoryModel> list = new ArrayList<>();
        list.add(new SubCategoryModel());
        list.add(new SubCategoryModel());
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(Observable.just(Response.success(list)));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchStoreData();
        Mockito.verify(storeRepositoryManager).getStoreCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchStoreData_NoData_test() throws InterruptedException {
        List<SubCategoryModel> list = new ArrayList<>();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(Observable.just(Response.success(list)));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchStoreData();
        Mockito.verify(storeRepositoryManager).getStoreCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchStoreData_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "store removed");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchStoreData();
        Mockito.verify(storeRepositoryManager).getStoreCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchStoreData_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        ReflectionTestUtils.setField(storeViewModel, "baseUserRepositoryManager", userRepositoryManager);
        storeViewModel.fetchStoreData();
        Mockito.verify(storeRepositoryManager, Mockito.times(1)).getStoreCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchStoreData_401_to_200_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        ReflectionTestUtils.setField(storeViewModel, "baseUserRepositoryManager", userRepositoryManager);
        storeViewModel.fetchStoreData();
        Thread.sleep(1000);
        List<SubCategoryModel> list = new ArrayList<>();
        list.add(new SubCategoryModel());
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(Observable.just(Response.success(list)));
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchStoreData_error_test() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreCatalogues()).thenReturn(
                Observable.error(new Exception("Test Exception"))
        );
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchStoreData();
        Mockito.verify(storeRepositoryManager).getStoreCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_noInternet_test() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        StoreViewModel storeViewModel = new StoreViewModel();
        storeViewModel.fetchSubjects("1111-2222-3333-4444");
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(new CatalogueOverviewModel())));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchSubjects("1111-2222-3333-4444");
        Mockito.verify(storeRepositoryManager).getStoreSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "invalid subject catalogue id");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchSubjects("1111-2222-3333-4444");
        Mockito.verify(storeRepositoryManager).getStoreSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreSubjects("1111-2222-3333-4444")).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        ReflectionTestUtils.setField(storeViewModel, "baseUserRepositoryManager", userRepositoryManager);
        storeViewModel.fetchSubjects("1111-2222-3333-4444");
        Mockito.verify(storeRepositoryManager, Mockito.times(1)).getStoreSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_401_to_200_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreSubjects("1111-2222-3333-4444")).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        ReflectionTestUtils.setField(storeViewModel, "baseUserRepositoryManager", userRepositoryManager);
        storeViewModel.fetchSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        PowerMockito.when(storeRepositoryManager.getStoreSubjects("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(new CatalogueOverviewModel())));
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubjects_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getStoreSubjects("1111-2222-3333-4444"))
                .thenReturn(Observable.error(new Exception("Test Exception")));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchSubjects("1111-2222-3333-4444");
        Mockito.verify(storeRepositoryManager).getStoreSubjects("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchCatalogueSubscriptionStatus_noInternet_test() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        StoreViewModel storeViewModel = new StoreViewModel();
        storeViewModel.fetchCatalogueSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchCatalogueSubscriptionStatus_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555")).thenReturn(Observable.just(Response.success(new SubscriptionModel())));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchCatalogueSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Mockito.verify(storeRepositoryManager, Mockito.times(1)).getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchCatalogueSubscriptionStatus_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "invalid subject id");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchCatalogueSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Mockito.verify(storeRepositoryManager, Mockito.times(1)).getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchCatalogueSubscriptionStatus_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555")).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        ReflectionTestUtils.setField(storeViewModel, "baseUserRepositoryManager", userRepositoryManager);
        storeViewModel.fetchCatalogueSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Mockito.verify(storeRepositoryManager, Mockito.times(1)).getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchCatalogueSubscriptionStatus_401_to_200_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555")).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        ReflectionTestUtils.setField(storeViewModel, "baseUserRepositoryManager", userRepositoryManager);
        storeViewModel.fetchCatalogueSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Thread.sleep(1000);
        PowerMockito.when(storeRepositoryManager.getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555")).thenReturn(Observable.just(Response.success(new SubscriptionModel())));
        Thread.sleep(1000);
        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchCatalogueSubscriptionStatus_error_test() throws InterruptedException {

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        StoreRepositoryManager storeRepositoryManager = PowerMockito.mock(StoreRepositoryManager.class);
        PowerMockito.when(storeRepositoryManager.getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555")).thenReturn(Observable.error(
                new Exception("Test Exception")
        ));
        StoreViewModel storeViewModel = new StoreViewModel();
        ReflectionTestUtils.setField(storeViewModel, "repositoryManager", storeRepositoryManager);
        storeViewModel.fetchCatalogueSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Mockito.verify(storeRepositoryManager, Mockito.times(1)).getSubscriptionStatus("1111-2222-3333-4444", "1111-2222-3333-5555");
        Thread.sleep(1000);

        Assert.assertNotNull(storeViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, storeViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


}
