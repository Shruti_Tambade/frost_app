package com.frost.leap.viewmodels.start;

import android.text.TextUtils;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 02-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({LostMobileViewModel.class, TextUtils.class, Utility.class, Mixpanel.class, FirebaseAnalytics.class})
public class LostMobileViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }


    @Test
    public void doEmailValidation_isNull() {
        String expectResult = "Email address cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(null)).thenReturn(true);
        lostMobileViewModel.doEmailValidation(null);
        Assert.assertNotNull(lostMobileViewModel.error.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.error.getValue().getMessage());
    }

    @Test
    public void doEmailValidation_isEmpty() {
        String expectResult = "Email address cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        lostMobileViewModel.doEmailValidation("");
        Assert.assertNotNull(lostMobileViewModel.error.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.error.getValue().getMessage());
    }

    @Test
    public void doEmailValidation_invalidEmail() {
        String expectResult = "Please enter a valid email address";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc.gmail.com")).thenReturn(false);
        lostMobileViewModel.doEmailValidation("abc.gmail.com");
        Assert.assertNotNull(lostMobileViewModel.error.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.error.getValue().getMessage());
    }

    @Test
    public void doEmailValidation_validEmail() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        Assert.assertTrue(lostMobileViewModel.doEmailValidation("abc@gmail.com"));
    }

    @Test
    public void doMobileValidation_isNull() {
        String expectResult = "Phone number cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(null)).thenReturn(true);
        lostMobileViewModel.doMobileValidation(null);
        Assert.assertNotNull(lostMobileViewModel.error.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.error.getValue().getMessage());
    }

    @Test
    public void doMobileValidation_isEmpty() {
        String expectResult = "Phone number cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        lostMobileViewModel.doMobileValidation("");
        Assert.assertNotNull(lostMobileViewModel.error.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.error.getValue().getMessage());
    }


    @Test
    public void doMobileValidation_inValid() {
        String expectResult = "The phone number you have entered is invalid";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        lostMobileViewModel.doMobileValidation("1234567");
        Assert.assertNotNull(lostMobileViewModel.error.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.error.getValue().getMessage());
    }

    @Test
    public void doMobileValidation_valid() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        Assert.assertTrue(lostMobileViewModel.doMobileValidation("1234567890"));
    }

    @Test
    public void doOTPValidation_isNull() {
        String expectResult = "Verification code cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(null)).thenReturn(true);

        lostMobileViewModel.doOTPValidation(null);

        Assert.assertNotNull(lostMobileViewModel.message.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.message.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_isEmpty() {
        String expectResult = "Verification code cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        lostMobileViewModel.doOTPValidation("");
        Assert.assertNotNull(lostMobileViewModel.message.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.message.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_isInvalid() {
        String expectResult = "Please enter a valid 6 digit verification code";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        lostMobileViewModel.doOTPValidation("123");
        Assert.assertNotNull(lostMobileViewModel.message.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.message.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_valid() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        Assert.assertTrue(lostMobileViewModel.doOTPValidation("123456"));
    }

    @Test
    public void requestLostPhone_NoInternet() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        lostMobileViewModel.requestLostPhone();
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void resentEmailOtp_NoInternet() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        lostMobileViewModel.resentEmailOtp("abc@gmail.com");
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void verifyEmailOtp_NoInternet() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        lostMobileViewModel.verifyEmailOtp("abc@gmail.com");
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void requestAlternativeNumber_NoInternet() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        lostMobileViewModel.requestAlternativeNumber();
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void requestAlternativeNumberAgain_NoInternet() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        lostMobileViewModel.requestAlternativeNumberAgain();
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void verifyMobileOtp_NoInternet() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        lostMobileViewModel.verifyMobileOtp();
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void requestLostPhone_200_success() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLostPhone("abc@gmail.com")).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        lostMobileViewModel.requestLostPhone();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLostPhone("abc@gmail.com");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, lostMobileViewModel.networkCall.getValue().getNetworkStatus());

    }


    @Test
    public void requestLostPhone_500_Test() throws Exception {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "email is not exist");

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLostPhone("abc@gmail.com")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        lostMobileViewModel.requestLostPhone();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLostPhone("abc@gmail.com");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
//        Assert.assertEquals(NetworkStatus.FAIL, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void requestLostPhone_errorTest() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLostPhone("abc@gmail.com")).thenReturn(Observable.error(new Exception("Test exception")));
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        lostMobileViewModel.requestLostPhone();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLostPhone("abc@gmail.com");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.ERROR, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void resentEmailOtp_200_success() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLostPhone("abc@gmail.com")).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        lostMobileViewModel.resentEmailOtp("abc@gmail.com");
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLostPhone("abc@gmail.com");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, lostMobileViewModel.networkCall.getValue().getNetworkStatus());

    }


    @Test
    public void resentEmailOtp_500_Test() throws Exception {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "email is not exist");

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLostPhone("abc@gmail.com")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));


        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        lostMobileViewModel.resentEmailOtp("abc@gmail.com");
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLostPhone("abc@gmail.com");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.FAIL, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void resentEmailOtp_errorTest() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLostPhone("abc@gmail.com")).thenReturn(Observable.error(new Exception("Test exception")));
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        lostMobileViewModel.resentEmailOtp("abc@gmail.com");
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLostPhone("abc@gmail.com");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.ERROR, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void verifyEmailOtp_200_Test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.verifyEmailOtp("abc@gmail.com", "123456")).thenReturn(
                Observable.just(Response.success(new JsonObject()))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "otpCode", "123456");
        lostMobileViewModel.verifyEmailOtp("abc@gmail.com");
        Mockito.verify(userRepositoryManager, Mockito.times(1)).verifyEmailOtp("abc@gmail.com", "123456");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void verifyEmailOtp_500_Test() throws Exception {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Invalid otp");

        PowerMockito.mockStatic(Mixpanel.class);
        PowerMockito.mockStatic(FirebaseAnalytics.class);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.verifyEmailOtp("abc@gmail.com", "123456")).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes())))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "otpCode", "123456");
        lostMobileViewModel.verifyEmailOtp("abc@gmail.com");
        Mockito.verify(userRepositoryManager, Mockito.times(1)).verifyEmailOtp("abc@gmail.com", "123456");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.FAIL, lostMobileViewModel.networkCall.getValue().getNetworkStatus());

    }

    @Test
    public void verifyEmailOtp_errorTest() throws Exception {

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.verifyEmailOtp("abc@gmail.com", "123456")).thenReturn(Observable.error(new Exception("Test exception")));
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "otpCode", "123456");
        lostMobileViewModel.verifyEmailOtp("abc@gmail.com");
        Mockito.verify(userRepositoryManager, Mockito.times(1)).verifyEmailOtp("abc@gmail.com", "123456");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.ERROR, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void requestAlternativeNumber_200_Test() throws InterruptedException {
        PowerMockito.mockStatic(Mixpanel.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "1111-2222-3333-4444");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.updateAlternativeNumber("abc@gmail.com", "1234567890")).thenReturn(
                Observable.just(Response.success(jsonObject))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        lostMobileViewModel.requestAlternativeNumber();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).updateAlternativeNumber("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void requestAlternativeNumber_500_Test() throws InterruptedException {
        PowerMockito.mockStatic(Mixpanel.class);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "number already exist");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.updateAlternativeNumber("abc@gmail.com", "1234567890")).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        lostMobileViewModel.requestAlternativeNumber();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).updateAlternativeNumber("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void requestAlternativeNumber_errorTest() throws InterruptedException {
        PowerMockito.mockStatic(Mixpanel.class);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.updateAlternativeNumber("abc@gmail.com", "1234567890")).thenReturn(Observable.error(new Exception("Test exception")));
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        lostMobileViewModel.requestAlternativeNumber();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).updateAlternativeNumber("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.ERROR, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void requestAlternativeNumberAgain_200_Test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "1111-2222-3333-4444");
        PowerMockito.mockStatic(Mixpanel.class);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.updateAlternativeNumber("abc@gmail.com", "1234567890")).thenReturn(
                Observable.just(Response.success(jsonObject))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        lostMobileViewModel.requestAlternativeNumberAgain();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).updateAlternativeNumber("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void requestAlternativeNumberAgain_500_Test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "number already exist");
        PowerMockito.mockStatic(Mixpanel.class);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.updateAlternativeNumber("abc@gmail.com", "1234567890")).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        lostMobileViewModel.requestAlternativeNumberAgain();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).updateAlternativeNumber("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void requestAlternativeNumberAgain_errorTest() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.updateAlternativeNumber("abc@gmail.com", "1234567890")).thenReturn(Observable.error(new Exception("Test exception")));
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        lostMobileViewModel.requestAlternativeNumberAgain();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).updateAlternativeNumber("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.networkCall.getValue());
        Assert.assertEquals(NetworkStatus.ERROR, lostMobileViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void verifyMobileOtp_200_Test() throws InterruptedException {

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.verifyAlternativeNumberOTP("1234567890", "1111-2222-3333-4444", "123456")).thenReturn(
                Observable.just(Response.success(new JsonObject()))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        ReflectionTestUtils.setField(lostMobileViewModel, "userId", "1111-2222-3333-4444");
        ReflectionTestUtils.setField(lostMobileViewModel, "otpCode", "123456");
        lostMobileViewModel.verifyMobileOtp();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).verifyAlternativeNumberOTP("1234567890", "1111-2222-3333-4444", "123456");

        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());

    }


    @Test
    public void verifyMobileOtp_500_Test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Invalid otp!");
        PowerMockito.mockStatic(Mixpanel.class);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.verifyAlternativeNumberOTP("1234567890", "1111-2222-3333-4444", "123456")).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        ReflectionTestUtils.setField(lostMobileViewModel, "userId", "1111-2222-3333-4444");
        ReflectionTestUtils.setField(lostMobileViewModel, "otpCode", "123456");
        lostMobileViewModel.verifyMobileOtp();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).verifyAlternativeNumberOTP("1234567890", "1111-2222-3333-4444", "123456");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void verifyMobileOtp_errorTest() throws InterruptedException {

        PowerMockito.mockStatic(Mixpanel.class);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.verifyAlternativeNumberOTP("1234567890", "1111-2222-3333-4444", "123456")).thenReturn(
                Observable.error(new Exception("Test Exception"))
        );
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        ReflectionTestUtils.setField(lostMobileViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(lostMobileViewModel, "mobileNumber", "1234567890");
        ReflectionTestUtils.setField(lostMobileViewModel, "userId", "1111-2222-3333-4444");
        ReflectionTestUtils.setField(lostMobileViewModel, "otpCode", "123456");
        lostMobileViewModel.verifyMobileOtp();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).verifyAlternativeNumberOTP("1234567890", "1111-2222-3333-4444", "123456");
        Thread.sleep(1000);
        Assert.assertNotNull(lostMobileViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, lostMobileViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


}
