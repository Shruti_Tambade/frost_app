package com.frost.leap.viewmodels;

import android.content.Context;
import android.text.TextUtils;

import com.frost.leap.viewmodels.start.StartViewModel;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import java.util.Random;

import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 21-08-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({StartViewModel.class, TextUtils.class, Utility.class})
public class StartViewModelTest {


    @Mock
    UserRepositoryManager userRepositoryManager;

    @Mock
    Context context;

    StartViewModel startViewModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        startViewModel = new StartViewModel();

        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Test
    public void checkMobileIsBlank() {
        String mobileNumber = null;
        boolean expertResult = false;
        Assert.assertEquals(expertResult, startViewModel.doMobileValidation(mobileNumber));
    }

    @Test
    public void checkMobileNotBlank() {
        String mobileNumber = "1222";
        boolean expertResult = false;
        Assert.assertEquals(expertResult, startViewModel.doMobileValidation(mobileNumber));
    }

    @Test
    public void checkMobileNumber_IsCorrect() {
        String mobileNumber = "7207824353";
        boolean expertResult = true;
        Assert.assertEquals(expertResult, (new StartViewModel()).doMobileValidation(mobileNumber));
    }

    @Test
    public void checkMobileNumber_IsWrong() {
        String mobileNumber = "720824353";
        boolean expertResult = false;
        Assert.assertEquals(expertResult, startViewModel.doMobileValidation(mobileNumber));
    }


    @Test
    public void otpValidation_isNull() {
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(null)).thenReturn(true);
        Assert.assertEquals(false, startViewModel.doOTPValidation(null));
    }

    @Test
    public void otpValidation_isEmpty() {
        Assert.assertFalse(startViewModel.doOTPValidation(""));
    }

    @Test
    public void otpValidation_InValid() {
        Assert.assertFalse(startViewModel.doOTPValidation("1234"));
    }

    @Test
    public void otpValidation_Valid() {
        Assert.assertTrue(startViewModel.doOTPValidation("123456"));
    }


    @Test
    public void requestLogin_isNotInternet() {
        StartViewModel startViewModel = PowerMockito.mock(StartViewModel.class);
        PowerMockito.when(startViewModel.isNetworkAvailable()).thenReturn(false);
        boolean actualResult = startViewModel.isNetworkAvailable();

        Mockito.verify(startViewModel, Mockito.times(1)).isNetworkAvailable();
        Assert.assertFalse(actualResult);
    }

    @Test
    public void requestLogin_isInternetAvailable() {
        StartViewModel startViewModel = PowerMockito.mock(StartViewModel.class);
        PowerMockito.when(startViewModel.isNetworkAvailable()).thenReturn(true);
        boolean actualResult = startViewModel.isNetworkAvailable();

        Mockito.verify(startViewModel, Mockito.times(1)).isNetworkAvailable();
        Assert.assertTrue(actualResult);
    }


    @Test
    public void requestLogin_200_Test() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "valid user");
        Response<JsonObject> response = Response.success(jsonObject);

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLogin("1234567890")).thenReturn(Observable.just(response));

        TestObserver<Response<JsonObject>> testObserver = userRepositoryManager.requestLogin("1234567890").test();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLogin("1234567890");

        List<Response<JsonObject>> responseList = testObserver.values();
        testObserver.assertValue(response);
        testObserver.dispose();

        Assert.assertEquals(200, responseList.get(0).code());
    }


    @Test
    public void requestLogin_401_Test() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un_authorized");
        ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"),
                jsonObject.toString().getBytes());
        Response<JsonObject> response = Response.error(401, responseBody);

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLogin("1234567890")).thenReturn(Observable.just(response));

        TestObserver<Response<JsonObject>> testObserver = userRepositoryManager.requestLogin("1234567890").test();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLogin("1234567890");

        List<Response<JsonObject>> responseList = testObserver.values();
        testObserver.assertValue(response);
        testObserver.dispose();

        Assert.assertEquals(401, responseList.get(0).code());
    }


    @Test
    public void requestLogin_500_Test() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "in valid email or phone number");

        ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"),
                jsonObject.toString().getBytes());
        Response<JsonObject> response = Response.error(500, responseBody);

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLogin("1234567890")).thenReturn(Observable.just(response));

        TestObserver<Response<JsonObject>> testObserver = userRepositoryManager.requestLogin("1234567890").test();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLogin("1234567890");

        List<Response<JsonObject>> responseList = testObserver.values();
        testObserver.assertValue(response);
        testObserver.dispose();

        Assert.assertEquals(500, responseList.get(0).code());
    }


    @Test
    public void requestLogin_Error_Test() {
        Exception exception = new Exception("Test Exception");
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLogin("1234567890")).thenReturn(Observable.error(exception));

        TestObserver<Response<JsonObject>> testObserver = userRepositoryManager.requestLogin("1234567890").test();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLogin("1234567890");

        testObserver.assertError(exception);
        Assert.assertEquals(exception.getMessage(), testObserver.errors().get(0).getMessage());
        testObserver.dispose();
    }

    @Test
    public void requestLogin_Any_Test() {
        int responseCode = 503;
        ResponseBody responseBody = ResponseBody.create(MediaType.get("text/plain"),
                "Server Error".getBytes());
        Response<JsonObject> response = Response.error(responseCode, responseBody);

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.requestLogin("1234567890")).thenReturn(Observable.just(response));

        TestObserver<Response<JsonObject>> testObserver = userRepositoryManager.requestLogin("1234567890").test();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).requestLogin("1234567890");

        List<Response<JsonObject>> responseList = testObserver.values();
        testObserver.assertValue(response);
        testObserver.dispose();

        Assert.assertEquals(responseCode, responseList.get(0).code());
    }
}