package com.frost.leap.viewmodels.profile;

import android.text.TextUtils;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.start.LostMobileViewModel;
import com.frost.leap.viewmodels.start.SignUpViewModel;
import com.frost.leap.viewmodels.start.StartViewModel;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import storage.AppSharedPreferences;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 03/09/20.
 * <p>
 * Frost Interactive
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({StartViewModel.class, ProfileSettingsViewModel.class, LostMobileViewModel.class, TextUtils.class, Utility.class})
public class ProfileViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Test
    public void fetchUserProfileByID_noInternet_test() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        ProfileSettingsViewModel profileSettingsViewModel = new ProfileSettingsViewModel();
        profileSettingsViewModel.fetchUserDetails();
        Assert.assertNotNull(profileSettingsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, profileSettingsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUserProfileID_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        AppSharedPreferences appSharedPreferences = PowerMockito.mock(AppSharedPreferences.class);
        PowerMockito.when(appSharedPreferences.getString(Constants.UserInfo.USER_ID)).thenReturn("123456789");

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserDetails()).thenReturn(Observable.just(Response.success(new User())));

        ProfileSettingsViewModel profileSettingsViewModel = new ProfileSettingsViewModel();
        ReflectionTestUtils.setField(profileSettingsViewModel, "userRepositoryManager", userRepositoryManager);
        profileSettingsViewModel.fetchUserDetails();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserDetails();
        Thread.sleep(1000);
        Assert.assertNotNull(profileSettingsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, profileSettingsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUserProfileByID_200_noData_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserDetails()).thenReturn(Observable.just(Response.success(null)));
        ProfileSettingsViewModel profileSettingsViewModel = new ProfileSettingsViewModel();
        ReflectionTestUtils.setField(profileSettingsViewModel, "userRepositoryManager", userRepositoryManager);
        profileSettingsViewModel.fetchUserDetails();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserDetails();
        Thread.sleep(1000);
        Assert.assertNotNull(profileSettingsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, profileSettingsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchUserProfileByID_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "update id not found");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserDetails()).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        ProfileSettingsViewModel profileSettingsViewModel = new ProfileSettingsViewModel();
        ReflectionTestUtils.setField(profileSettingsViewModel, "userRepositoryManager", userRepositoryManager);
        profileSettingsViewModel.fetchUserDetails();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserDetails();
        Thread.sleep(1000);
        Assert.assertNotNull(profileSettingsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, profileSettingsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchUserProfileByID_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserDetails()).thenReturn(Observable.error(new Exception("Test Exception")));
        ProfileSettingsViewModel profileSettingsViewModel = new ProfileSettingsViewModel();
        ReflectionTestUtils.setField(profileSettingsViewModel, "userRepositoryManager", userRepositoryManager);
        profileSettingsViewModel.fetchUserDetails();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).getUserDetails();
        Thread.sleep(1000);
        Assert.assertNotNull(profileSettingsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, profileSettingsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void doValidationFirstName_Empty() {
        String expectResult = "First name cannot be empty";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        signUpViewModel.doValidationSignUp("", "", "", "", "", "", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doUpdateFirstName() {
        boolean expectResult = true;
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        PowerMockito.when(Utility.isValidMobile("1234567890")).thenReturn(true);
        Assert.assertEquals(expectResult,
                signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com"
                        , "1234567890", "1234567", "1234567", true));
    }

    @Test
    public void doValidationLastName_Empty() {
        String expectResult = "Last name cannot be empty";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        signUpViewModel.doValidationSignUp("Chenna", "", "", "", "", "", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doValidationMobileNumber_Empty() {
        String expectResult = "Phone number cannot be empty";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("chennarao07@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("Chenna", "Rao", "chennarao07@gmail.com", "", "", "", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doValidationMobileNumber_InValid() {
        String expectResult = "The phone number you have entered is invalid";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("chennarao07@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("Chenna", "Rao", "chennarao07@gmail.com", "12345", "", "", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doValidationPassword_Empty() {
        String expectResult = "Password cannot be empty";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("chennarao07@gmail.com")).thenReturn(true);
        PowerMockito.when(Utility.isValidMobile("8977474109")).thenReturn(true);
        signUpViewModel.doValidationSignUp("Chenna", "Rao", "chennarao07@gmail.com", "8977474109", "", "", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doValidationPassword_InValid() {
        String expectResult = "Password should have atleast 6 characters";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("chennarao07@gmail.com")).thenReturn(true);
        PowerMockito.when(Utility.isValidMobile("8977474109")).thenReturn(true);
        signUpViewModel.doValidationSignUp("Chenna", "Rao", "chennarao07@gmail.com", "8977474109", "chen", "", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_isNull() {
        String expectResult = "Verification code cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(null)).thenReturn(true);

        lostMobileViewModel.doOTPValidation(null);

        Assert.assertNotNull(lostMobileViewModel.message.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.message.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_isEmpty() {
        String expectResult = "Verification code cannot be empty";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        lostMobileViewModel.doOTPValidation("");
        Assert.assertNotNull(lostMobileViewModel.message.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.message.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_isInvalid() {
        String expectResult = "Please enter a valid 6 digit verification code";
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        lostMobileViewModel.doOTPValidation("123");
        Assert.assertNotNull(lostMobileViewModel.message.getValue());
        Assert.assertEquals(expectResult, lostMobileViewModel.message.getValue().getMessage());
    }

    @Test
    public void doOTPValidation_valid() {
        LostMobileViewModel lostMobileViewModel = new LostMobileViewModel();
        Assert.assertTrue(lostMobileViewModel.doOTPValidation("123456"));
    }

    @Test
    public void doValidationPasswordReset_passwordMinimum() {
        String expectResult = "Password should have atleast 6 characters";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "1234567890", "12345", "123456", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }


    @Test
    public void doValidationPasswordReset_passwordAndConfirmPasswordDidNotMatch() {
        String expectResult = "Those passwords didn't match. Try again";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "1234567890", "1234567", "12345", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }

    @Test
    public void doValidationFatherName_Empty() {
        String fatherName = "";

        boolean expertResult = true;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(fatherName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationFatherName_valid() {
        String fatherName = "Chennakesavulu";

        boolean expertResult = false;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(fatherName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationCollegeName_Empty() {
        String collegeName = "";

        boolean expertResult = true;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(collegeName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationCollegeName_valid() {
        String collegeName = "MallReddy";

        boolean expertResult = false;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(collegeName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationHouseName_Empty() {
        String houseName = "";

        boolean expertResult = true;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(houseName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationHouseName_valid() {
        String houseName = "3-4-45";

        boolean expertResult = false;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(houseName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationCityName_Empty() {
        String cityName = "";

        boolean expertResult = true;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(cityName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationCityName_valid() {
        String cityName = "Hyderabad";

        boolean expertResult = false;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(cityName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationStateName_Empty() {
        String stateName = "";

        boolean expertResult = true;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(stateName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationStateName_valid() {
        String stateName = "Telangana";

        boolean expertResult = false;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(stateName);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationPincode_Empty() {
        String pinCode = "";

        boolean expertResult = true;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(pinCode);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidationPincode_valid() {
        String pinCode = "523316";

        boolean expertResult = false;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        boolean actualResult = TextUtils.isEmpty(pinCode);

        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dofrontSideIDEmptyCheckValidation() {
        String frontSideID = "";

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);

        boolean expertResult = true;
        boolean actualResult = TextUtils.isEmpty(frontSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dofrontSideIDNullCheckValidation() {
        String frontSideID = null;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);

        boolean expertResult = false;
        boolean actualResult = TextUtils.isEmpty(frontSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dofrontSideIDalidCheckValidation() {
        String frontSideID = "frontsideIDProof.jpg";

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);

        boolean expertResult = false;
        boolean actualResult = TextUtils.isEmpty(frontSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dobackSideIDEmptyCheckValidation() {
        String backSideID = "";

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);

        boolean expertResult = true;
        boolean actualResult = TextUtils.isEmpty(backSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dobackSideIDNullCheckValidation() {
        String backSideID = null;

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);

        boolean expertResult = false;
        boolean actualResult = TextUtils.isEmpty(backSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void dobackSideIDalidCheckValidation() {
        String backSideID = "backsideIDProof.jpg";

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);

        boolean expertResult = false;
        boolean actualResult = TextUtils.isEmpty(backSideID);
        Assert.assertEquals(expertResult, actualResult);
    }

}
