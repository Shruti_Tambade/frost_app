package com.frost.leap.viewmodels.topics;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import apprepos.topics.TopicRepositoryManager;
import apprepos.topics.model.TopicModel;
import apprepos.user.UserRepositoryManager;
import apprepos.video.model.video.PageContentDataModel;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 10-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({TopicsViewModel.class, Utility.class, BaseViewModel.class})
public class TopicsViewModelTest {

    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }


    @Test
    public void fetchTopics_noInternet_noOfflineData() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getOfflineUnitTopic("1111-2222-3333-4444")).thenReturn(Observable.error(new Exception("Test Exception")));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getOfflineUnitTopic("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchTopics_noInternet_hasOfflineData() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getOfflineUnitTopic("1111-2222-3333-4444")).thenReturn(Observable.just(new TopicModel()));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getOfflineUnitTopic("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchTopics_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getTopics("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(new TopicModel())));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getTopics("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchTopics_500_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getTopics("1111-2222-3333-4444")).thenReturn(Observable.just(Response.error(500,
                ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes()))));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getTopics("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchTopics_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getTopics("1111-2222-3333-4444")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getTopics("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchTopics_401_to_200_test() throws InterruptedException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getTopics("1111-2222-3333-4444")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Thread.sleep(1000);
        PowerMockito.when(topicRepositoryManager.getTopics("1111-2222-3333-4444")).thenReturn(Observable.just(Response.success(new TopicModel())));
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchTopics_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getTopics("1111-2222-3333-4444")).thenReturn(Observable.error(new Exception()));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchTopics("1111-2222-3333-4444");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getTopics("1111-2222-3333-4444");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void updateVideoStream_noInternet() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        topicsViewModel.updateVideoStream("1", "1", "1", 1, "1");
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void updateVideoStream_200_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.updateVideoStream("1", "1", "1", 1, "1")).thenReturn(Observable.just(Response.success(new JsonObject())));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.updateVideoStream("1", "1", "1", 1, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).updateVideoStream("1", "1", "1", 1, "1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void updateVideoStream_500_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.updateVideoStream("1", "1", "1", 1, "1")).thenReturn(Observable.just(Response.error(500,
                ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes()))));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.updateVideoStream("1", "1", "1", 1, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).updateVideoStream("1", "1", "1", 1, "1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void updateVideoStream_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.updateVideoStream("1", "1", "1", 1, "1")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.updateVideoStream("1", "1", "1", 1, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).updateVideoStream("1", "1", "1", 1, "1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void updateVideoStream_401_to_200_test() throws InterruptedException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.updateVideoStream("1", "1", "1", 1, "1")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.updateVideoStream("1", "1", "1", 1, "1");
        Thread.sleep(1000);
        PowerMockito.when(topicRepositoryManager.updateVideoStream("1", "1", "1", 1, "1")).thenReturn(Observable.just(Response.success(new JsonObject())));
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void updateVideoStream_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.updateVideoStream("1", "1", "1", 1, "1")).thenReturn(Observable.error(new Exception()));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.updateVideoStream("1", "1", "1", 1, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).updateVideoStream("1", "1", "1", 1, "1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void verifyAndFetchVideoUrl_noInternet() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        topicsViewModel.verifyAndFetchVideoUrl(null, "1");
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void verifyAndFetchVideoUrl_200_test() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getVideoUrl("1")).thenReturn(Observable.just(Response.success(new PageContentDataModel())));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.verifyAndFetchVideoUrl(null, "1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void verifyAndFetchVideoUrl_500_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getVideoUrl("1")).thenReturn(Observable.just(Response.error(500,
                ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes()))));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.verifyAndFetchVideoUrl(null, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getVideoUrl("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void verifyAndFetchVideoUrl_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getVideoUrl("1")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.verifyAndFetchVideoUrl(null, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getVideoUrl("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void verifyAndFetchVideoUrl_401_to_200_test() throws InterruptedException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getVideoUrl("1")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.verifyAndFetchVideoUrl(null, "1");
        Thread.sleep(1000);
        PowerMockito.when(topicRepositoryManager.getVideoUrl("1")).thenReturn(Observable.just(Response.success(new PageContentDataModel())));
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void verifyAndFetchVideoUrl_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getVideoUrl("1")).thenReturn(Observable.error(new Exception()));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.verifyAndFetchVideoUrl(null, "1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getVideoUrl("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchSubtopicsRating_noInternet() {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        topicsViewModel.fetchSubtopicsRating("1");
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubtopicsRating_200_test() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getSubTopicRating("1")).thenReturn(
                Observable.just(Response.success(ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes())))
        );
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchSubtopicsRating("1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getSubTopicRating("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchSubtopicsRating_500_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getSubTopicRating("1")).thenReturn(Observable.just(Response.error(500,
                ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes()))));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchSubtopicsRating("1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getSubTopicRating("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.FAIL, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchSubtopicsRating_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getSubTopicRating("1")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.fetchSubtopicsRating("1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getSubTopicRating("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchSubtopicsRating_401_to_200_test() throws InterruptedException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getSubTopicRating("1")).thenReturn(Observable.just(
                Response.error(401,
                        ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes()))));

        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        ReflectionTestUtils.setField(topicsViewModel, "baseUserRepositoryManager", userRepositoryManager);
        topicsViewModel.fetchSubtopicsRating("1");
        Thread.sleep(1000);
        PowerMockito.when(topicRepositoryManager.getSubTopicRating("1")).thenReturn(
                Observable.just(Response.success(ResponseBody.create(MediaType.get("text/plain"), new JsonObject().toString().getBytes())))
        );        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchSubtopicsRating_error_test() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        TopicRepositoryManager topicRepositoryManager = PowerMockito.mock(TopicRepositoryManager.class);
        PowerMockito.when(topicRepositoryManager.getSubTopicRating("1")).thenReturn(
                Observable.error(new Exception()));
        TopicsViewModel topicsViewModel = new TopicsViewModel();
        ReflectionTestUtils.setField(topicsViewModel, "repositoryManager", topicRepositoryManager);
        topicsViewModel.fetchSubtopicsRating("1");
        Mockito.verify(topicRepositoryManager, Mockito.times(1)).getSubTopicRating("1");
        Thread.sleep(1000);
        Assert.assertNotNull(topicsViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, topicsViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


}
