package com.frost.leap.viewmodels.start;

import android.text.TextUtils;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 02-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({SignUpViewModel.class, TextUtils.class, Utility.class})
public class SignUpViewModelTest {


    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }


    @Test
    public void doValidationSignUp_isNull() {
        boolean expectResult = false;
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(null)).thenReturn(true);
        Assert.assertEquals(expectResult, signUpViewModel.doValidationSignUp(null, null, null, null, null, null, false));
    }

    @Test
    public void doValidationSignUp_isEmpty() {
        boolean expectResult = false;
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        Assert.assertEquals(expectResult, signUpViewModel.doValidationSignUp("", "", "", "", "", "", false));
    }


    @Test
    public void doValidationSignUp_inValidEmail() {
        String expectResult = "Please enter a valid email address";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("ABC")).thenReturn(false);
        signUpViewModel.doValidationSignUp("ABC", "ABC", "ABC", "1234567890", "123456", "123456", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }


    @Test
    public void doValidationSignUp_passwordMinimum() {
        String expectResult = "Password should have atleast 6 characters";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "1234567890", "12345", "123456", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }


    @Test
    public void doValidationSignUp_passwordAndConfirmPasswordDidNotMatch() {
        String expectResult = "Those passwords didn't match. Try again";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "1234567890", "1234567", "12345", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }


    @Test
    public void doValidationSignUp_invalidMobileNumber() {
        String expectResult = "The phone number you have entered is invalid";
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "123490", "1234567", "1234567", false);
        Assert.assertEquals(expectResult, signUpViewModel.error.getValue().getMessage());
    }


    @Test
    public void doValidationSignUp_isNotAgree() {
        boolean expectResult = false;
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        PowerMockito.when(Utility.isValidMobile("1234567890")).thenReturn(true);
        Assert.assertEquals(expectResult, signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "1234567890", "1234567", "1234567", false));
    }


    @Test
    public void doValidationSignUp() {
        boolean expectResult = true;
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty("")).thenReturn(true);
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isValidEmail("abc@gmail.com")).thenReturn(true);
        PowerMockito.when(Utility.isValidMobile("1234567890")).thenReturn(true);
        Assert.assertEquals(expectResult, signUpViewModel.doValidationSignUp("ABC", "ABC", "abc@gmail.com", "1234567890", "1234567", "1234567", true));
    }


    @Test
    public void checkUserExist_notInternet() {
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        signUpViewModel.checkUserExist();
        Assert.assertEquals(NetworkStatus.NO_INTERNET, signUpViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void checkUserExist_200_test() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        JsonObject jsonObject = new JsonObject();
        PowerMockito.when(userRepositoryManager.checkUserExist("abc@gmail.com", "1234567890")).thenReturn(Observable.just(Response.success(
                jsonObject
        )));
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        ReflectionTestUtils.setField(signUpViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(signUpViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(signUpViewModel, "phoneNumber", "1234567890");
        signUpViewModel.checkUserExist();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).checkUserExist("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertEquals(NetworkStatus.SUCCESS, signUpViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void checkUserExist_500_test() throws Exception {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "already exist email or phone number");

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.checkUserExist("abc@gmail.com", "1234567890")).thenReturn(Observable.just(
                Response.error(500, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        ReflectionTestUtils.setField(signUpViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(signUpViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(signUpViewModel, "phoneNumber", "1234567890");
        signUpViewModel.checkUserExist();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).checkUserExist("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertEquals(NetworkStatus.FAIL, signUpViewModel.networkCall.getValue().getNetworkStatus());
    }

    @Test
    public void checkUserExist_401_test() throws Exception {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "unauthorized");

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.checkUserExist("abc@gmail.com", "1234567890")).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        ReflectionTestUtils.setField(signUpViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(signUpViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(signUpViewModel, "phoneNumber", "1234567890");
        signUpViewModel.checkUserExist();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).checkUserExist("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertEquals(NetworkStatus.FAIL, signUpViewModel.networkCall.getValue().getNetworkStatus());
    }


    @Test
    public void checkUserExist_error_test() throws Exception {

        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        UserRepositoryManager userRepositoryManager = Mockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.checkUserExist("abc@gmail.com", "1234567890")).thenReturn(Observable.error(new Exception("Test exception")));
        SignUpViewModel signUpViewModel = new SignUpViewModel();
        ReflectionTestUtils.setField(signUpViewModel, "repositoryManager", userRepositoryManager);
        ReflectionTestUtils.setField(signUpViewModel, "email", "abc@gmail.com");
        ReflectionTestUtils.setField(signUpViewModel, "phoneNumber", "1234567890");
        signUpViewModel.checkUserExist();
        Mockito.verify(userRepositoryManager, Mockito.times(1)).checkUserExist("abc@gmail.com", "1234567890");
        Thread.sleep(1000);
        Assert.assertEquals(NetworkStatus.ERROR, signUpViewModel.networkCall.getValue().getNetworkStatus());
    }


}
