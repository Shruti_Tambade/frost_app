package com.frost.leap.viewmodels.catalogue;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.google.gson.JsonObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.CatalogueRepositoryManager;
import apprepos.catalogue.model.CatalogueModel;
import apprepos.user.UserRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 04-09-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({CatalogueViewModel.class, Utility.class})
public class CatalogueViewModelTest {

    @Rule
    InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Test
    public void fetchCatalogues_noInternet_noOfflineData() throws InterruptedException{
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(catalogueRepositoryManager.getOfflineCatalogues()).thenReturn(Observable.just(new ArrayList<>()));
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        catalogueViewModel.fetchCatalogues();
        Thread.sleep(1000);
        Mockito.verify(catalogueRepositoryManager, Mockito.times(1)).getOfflineCatalogues();
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_INTERNET, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchCatalogues_noInternet_offlineData() throws InterruptedException {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(false);
        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        List<CatalogueModel> list = new ArrayList<>();
        list.add(new CatalogueModel());
        PowerMockito.when(catalogueRepositoryManager.getOfflineCatalogues()).thenReturn(Observable.just(list));
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        catalogueViewModel.fetchCatalogues();
        Mockito.verify(catalogueRepositoryManager, Mockito.times(1)).getOfflineCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.updateCatalogueData().getValue());
        Assert.assertEquals(list.size(), catalogueViewModel.updateCatalogueData().getValue().size());
    }

    @Test
    public void fetchCatalogues_200_test() throws InterruptedException {
        List<CatalogueModel> list = new ArrayList<>();
        list.add(new CatalogueModel());
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(catalogueRepositoryManager.getCatalogues()).thenReturn(Observable.just(Response.success(list)));
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        catalogueViewModel.fetchCatalogues();
        Mockito.verify(catalogueRepositoryManager, Mockito.times(1)).getCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchCatalogues_noData_test() throws InterruptedException {
        List<CatalogueModel> list = new ArrayList<>();
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(catalogueRepositoryManager.getCatalogues()).thenReturn(Observable.just(Response.success(list)));
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        catalogueViewModel.fetchCatalogues();
        Mockito.verify(catalogueRepositoryManager, Mockito.times(1)).getCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.NO_DATA, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchCatalogues_500_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "store removed");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);

        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(catalogueRepositoryManager.getCatalogues()).thenReturn(
                Observable.just(Response.error(500, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        catalogueViewModel.fetchCatalogues();

        Mockito.verify(catalogueRepositoryManager, Mockito.times(1)).getCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchCatalogues_401_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(catalogueRepositoryManager.getCatalogues()).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.error(401, ResponseBody.create(MediaType.get("text/plain"),
                        jsonObject.toString().getBytes()))
        ));
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        ReflectionTestUtils.setField(catalogueViewModel, "baseUserRepositoryManager", userRepositoryManager);
        catalogueViewModel.fetchCatalogues();
        Mockito.verify(catalogueRepositoryManager, Mockito.times(1)).getCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SERVER_ERROR, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


    @Test
    public void fetchCatalogues_401_to_200_test() throws InterruptedException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "un authorized");
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        CatalogueRepositoryManager catalogueRepositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(catalogueRepositoryManager.getCatalogues()).thenReturn(
                Observable.just(Response.error(401, ResponseBody.create(MediaType.get("text/plain"), jsonObject.toString().getBytes())))
        );
        UserRepositoryManager userRepositoryManager = PowerMockito.mock(UserRepositoryManager.class);
        PowerMockito.when(userRepositoryManager.getUserToken()).thenReturn("1111-2222-3333-4444");
        PowerMockito.when(userRepositoryManager.refreshToken()).thenReturn(Observable.just(
                Response.success(jsonObject)
        ));
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", catalogueRepositoryManager);
        ReflectionTestUtils.setField(catalogueViewModel, "baseUserRepositoryManager", userRepositoryManager);
        catalogueViewModel.fetchCatalogues();
        Thread.sleep(1000);
        List<CatalogueModel> list = new ArrayList<>();
        list.add(new CatalogueModel());
        PowerMockito.when(catalogueRepositoryManager.getCatalogues()).thenReturn(Observable.just(Response.success(list)));
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.SUCCESS, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }

    @Test
    public void fetchCatalogues_error_test() throws Exception {
        PowerMockito.mockStatic(Utility.class);
        PowerMockito.when(Utility.isNetworkAvailable(null)).thenReturn(true);
        CatalogueRepositoryManager repositoryManager = PowerMockito.mock(CatalogueRepositoryManager.class);
        PowerMockito.when(repositoryManager.getCatalogues()).thenReturn(
                Observable.error(new Exception("Test Exception"))
        );
        CatalogueViewModel catalogueViewModel = new CatalogueViewModel();
        ReflectionTestUtils.setField(catalogueViewModel, "catalogueRepository", repositoryManager);
        catalogueViewModel.fetchCatalogues();
        Mockito.verify(repositoryManager, Mockito.times(1)).getCatalogues();
        Thread.sleep(1000);
        Assert.assertNotNull(catalogueViewModel.getNetworkCallStatus().getValue());
        Assert.assertEquals(NetworkStatus.ERROR, catalogueViewModel.getNetworkCallStatus().getValue().getNetworkStatus());
    }


}
