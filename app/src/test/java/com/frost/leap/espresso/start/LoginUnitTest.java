package com.frost.leap.espresso.start;

import com.frost.leap.utils.TextUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 21-08-2020.
 * <p>
 * Frost
 */
@RunWith(PowerMockRunner.class)
public class LoginUnitTest {
    @Mock
    TextUtils textUtils;
    @Mock
    Utility utilityObject;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void checkMobileIsBlank() {
        String mobileNumber = null;
        boolean expertResult = true;
        boolean actualResult = textUtils.isEmpty(mobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNotBlank() {
        String mobileNumber = "1222";
        boolean expertResult = false;
        boolean actualResult = textUtils.isEmpty(mobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNumber_IsCorrect() {
        String mobileNumber = "7207824353";
        boolean expertResult = true;
        boolean actualResult = utilityObject.isValidMobile(mobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNumber_IsWrong() {
        String mobileNumber = "720824353";
        boolean expertResult = false;
        boolean actualResult = utilityObject.isValidMobile(mobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }






}