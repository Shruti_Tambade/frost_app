package com.frost.leap.espresso.start;

import com.frost.leap.utils.TextUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 25/08/20.
 * <p>
 * Frost Interactive
 */
@RunWith(PowerMockRunner.class)
public class AccountRecovery {

    String email = "";
    String emailOTP = "";
    String newMobileNumber = "";
    String phoneOTP = "";

    boolean expertResult;
    boolean actualResult;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Mock
    TextUtils textUtils;

    @Mock
    Utility utility;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void doEmailEmptyCheckValidation() {
        email = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(email);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmailNullCheckValidation() {
        email = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(email);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doInValidEmailCheckValidation() {
        email = "chennarao";

        expertResult = true;
        actualResult = !email.matches(emailPattern);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidEmailCheckValidation() {
        email = "chennarao07@gmail.com";

        expertResult = true;
        actualResult = email.matches(emailPattern);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmailOTPEmptyCheckValidation() {
        emailOTP = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(emailOTP);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmailOTPNullCheckValidation() {
        emailOTP = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(emailOTP);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doInValidEmailOTPCheckValidation() {
        emailOTP = "2345";

        expertResult = true;
        actualResult = !(emailOTP.length() >= 6 ? true : false);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidEmailOTPCheckValidation() {
        emailOTP = "123456";

        expertResult = true;
        actualResult = (emailOTP.length() >= 6 ? true : false);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileIsEmpty() {
        newMobileNumber = "";

        boolean expertResult = true;
        boolean actualResult = textUtils.isEmpty(newMobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileIsBlank() {
        newMobileNumber = null;

        boolean expertResult = true;
        boolean actualResult = textUtils.isEmpty(newMobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNumberLength() {
        newMobileNumber = "897747";

        boolean expertResult = true;
        boolean actualResult = !utility.isValidMobile(newMobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNumberisValid() {
        newMobileNumber = "8977474109";

        boolean expertResult = true;
        boolean actualResult = utility.isValidMobile(newMobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doMobileOTPEmptyCheckValidation() {
        phoneOTP = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(phoneOTP);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doMobileOTPNullCheckValidation() {
        phoneOTP = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(phoneOTP);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doInValidMobileOTPCheckValidation() {
        phoneOTP = "2345";

        expertResult = true;
        actualResult = !(phoneOTP.length() >= 6 ? true : false);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidMobileOTPCheckValidation() {
        phoneOTP = "123456";

        expertResult = true;
        actualResult = (phoneOTP.length() >= 6 ? true : false);
        Assert.assertEquals(expertResult, actualResult);
    }


}
