package com.frost.leap.espresso.start;


import com.frost.leap.utils.TextUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 24/08/20.
 * <p>
 * Frost Interactive
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Utility.class)
public class SignupUnitTest {
    boolean expertResult;
    boolean actualResult;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    String firstName = "";
    String lastName = "";
    String email = "";
    String password = "";
    String confirmPassWord = "";
    String mobileNumber = "";

    @Mock
    TextUtils textUtils;

    @Mock
    Utility utility;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
//        PowerMockito.mockStatic(Utility.class);
//
//        PowerMockito.when(Utility.isValidEmail("chennara0")).thenCallRealMethod();
    }

    @Test
    public void doFirstNameEmptyCheckValidation() {
        firstName = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(firstName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doFirstNameNullCheckValidation() {
        firstName = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(firstName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doFirstNameValidCheckValidation() {
        firstName = "Chenna";

        expertResult = false;
        actualResult = textUtils.isEmpty(firstName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doLastNameEmptyCheckValidation() {
        lastName = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(lastName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doLasttNameNullCheckValidation() {
        lastName = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(lastName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doLastNameValidCheckValidation() {
        lastName = "Rao";

        expertResult = false;
        actualResult = textUtils.isEmpty(lastName);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmailEmptyCheckValidation() {
        email = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(email);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doEmailNullCheckValidation() {
        email = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(email);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doInValidEmailCheckValidation() {
        email = "chennarao";

        expertResult = true;
        actualResult = !email.matches(emailPattern);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doValidEmailCheckValidation() {
        email = "chennarao07@gmail.com";

        expertResult = true;
        actualResult = email.matches(emailPattern);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doPasswordEmptyCheckValidation() {
        password = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(password);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doPasswordNullCheckValidation() {
        password = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(password);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doPasswordValidCheckValidation() {
        password = "chenna123";

        expertResult = false;
        actualResult = textUtils.isEmpty(password);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doConfirmPasswordEmptyCheckValidation() {
        confirmPassWord = "";

        expertResult = true;
        actualResult = textUtils.isEmpty(confirmPassWord);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doConfirmPasswordNullCheckValidation() {
        confirmPassWord = null;

        expertResult = true;
        actualResult = textUtils.isEmpty(confirmPassWord);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doConfirmPasswordValidCheckValidation() {
        confirmPassWord = "chenna123";

        expertResult = false;
        actualResult = textUtils.isEmpty(confirmPassWord);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doConfirmPasswordmInvalidMatchingCheck() {
        String passWord = "chenna123";
        String confirmPassWord = "chenna321";

        expertResult = true;
        actualResult = !passWord.equals(confirmPassWord);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void doConfirmPasswordmValidMatchingCheck() {
        password = "chenna123";
        confirmPassWord = "chenna123";

        expertResult = true;
        actualResult = password.equals(confirmPassWord);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileIsEmpty() {
        mobileNumber = "";

        boolean expertResult = true;
        boolean actualResult = textUtils.isEmpty(mobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileIsBlank() {
        mobileNumber = null;

        boolean expertResult = true;
        boolean actualResult = textUtils.isEmpty(mobileNumber);
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNumberLength() {
        mobileNumber = "897747";

        boolean expertResult = false;
        boolean actualResult = mobileNumber.matches("[0-9]{10}");
        Assert.assertEquals(expertResult, actualResult);
    }

    @Test
    public void checkMobileNumberisValid() {
        mobileNumber = "8977474109";

        boolean expertResult = true;
        boolean actualResult = mobileNumber.matches("[0-9]{10}");
        Assert.assertEquals(expertResult, actualResult);
    }


}
