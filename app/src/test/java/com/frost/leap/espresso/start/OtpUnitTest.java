package com.frost.leap.espresso.start;

import com.frost.leap.utils.TextUtils;
import com.google.gson.JsonObject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 25-08-2020.
 * <p>
 * Frost
 */

@RunWith(PowerMockRunner.class)
public class OtpUnitTest {

    @Mock
    TextUtils textUtils;

    @Mock
    Utility utility;

    @Test
    public void doOTPValidation() {
        String otpCode = "123456";
        if (textUtils.isEmpty(otpCode)) {
            System.out.println("Verification code cannot be empty");
            return;
        } else if (otpCode.length() < 6) {
            System.out.println("Please enter a valid 6 digit verification code");
            return;

        }
        System.out.println("valid 6 digit verification code");
    }


    @Test
    public void responseValidationForVerifyOtp() throws Exception {
        // 0 -> ERROR, 1 -> POSITIVE, -1 -> AUTH, -2 -> SERVER ERROR,
        int value = 12;
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    System.out.println("POSITIVE RESPONSE");
                } else if (response.code() == 500) {
                    System.out.println("ERROR RESPONSE");
                    System.out.println("MESSAGE: " + utility.showErrorMessage(response.errorBody()));
                } else if (response.code() == 401) {
                    System.out.println("UN AUTHORIZE RESPONSE");
                } else { //
                    System.out.println("SERVER ERROR RESPONSE");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                System.out.println("ERROR HANDLER");
            }

            @Override
            public void onComplete() {
                System.out.println("COMPLETED");
            }
        };

        Observable<Response<JsonObject>> observable = Observable.create(emitter -> {
            ResponseBody responseBody;
            Response<JsonObject> response;
            JsonObject jsonObject;
            switch (value) {
                case 1:
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("message", "OTP verification successfully");
                    response = Response.success(jsonObject);
                    emitter.onNext(response);
                    emitter.onComplete();
                    break;

                case 0:
                    jsonObject = new JsonObject();
                    jsonObject.addProperty("message", "OTP verification failed");
                    responseBody = ResponseBody.create(MediaType.get("text/plain"),
                            jsonObject.toString().getBytes());
                    response = Response.error(500, responseBody);
                    emitter.onNext(response);
                    emitter.onComplete();
                    break;

                case -1:
                    responseBody = ResponseBody.create(MediaType.get("text/plain"),
                            "unauthorize");
                    response = Response.error(401, responseBody);
                    emitter.onNext(response);
                    emitter.onComplete();
                    break;
                case -2:
                    responseBody = ResponseBody.create(MediaType.get("text/plain"),
                            "server error");
                    response = Response.error(503, responseBody);
                    emitter.onNext(response);
                    emitter.onComplete();
                    break;
                default:
                    emitter.onError(new Exception("TEST"));
                    break;
            }

        });
        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

        Thread.sleep(2000);
    }

}
