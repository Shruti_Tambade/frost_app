package com.frost.leap.utils;


import android.util.Patterns;

import androidx.annotation.Nullable;


/**
 * Created by Gokul Kalagara (Mr. Psycho) on 24-08-2020.
 * <p>
 * Frost
 */
public class TextUtils {

    public static boolean isEmpty(@Nullable CharSequence str) {
        return str == null || str.length() == 0;
    }

}
