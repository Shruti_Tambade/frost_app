package supporters.constants;

/**
 * Created by R!ZWAN SHEIKH on 2019-09-17.
 * <p>
 * Frost
 */
public enum VideoQuality {
    AUTO,
    RS1080,
    RS720,
    RS540,
    RS480,
    RS360,
    RS270,
    RS144,
    DEFAULT
}
