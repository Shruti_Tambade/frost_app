package supporters.constants;

/**
 * Created by R!ZWAN SHEIKH on 2019-09-17.
 * <p>
 * Frost
 */
public enum PlayBackSpeed {
    SPp25X,
    SPp5X,
    SPp75X,
    SP1X,
    SP1p25X,
    SP1p5X,
    SP1p75X,
    SP2X
}
