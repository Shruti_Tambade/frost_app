package supporters.constants;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 25-03-2019.
 * <p>
 * Frost
 */
public enum NetworkStatus {
    NO_INTERNET,
    INVALID_DATA,
    IN_PROCESS,
    ERROR,
    SERVER_ERROR,
    NO_DATA,
    DONE,
    SUCCESS,
    FAIL,
    UNAUTHORIZED,
    TOKEN_STATUS
}
