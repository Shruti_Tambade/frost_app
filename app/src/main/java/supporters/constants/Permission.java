package supporters.constants;

public enum Permission
{
    LOCATION,
    COARSE_LOCATION,
    READ_SMS,
    WRITE_SMS,
    READ_STORAGE,
    WRITE_STORAGE,
    CAMERA,
    RECORD,
    READ_CALENDAR,
    WRITE_CALENDAR,
    CALL,
    PHONE_STATE
}
