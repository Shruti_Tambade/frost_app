package supporters.constants;

/**
 * Created by R!ZWAN SHEIKH on 2019-09-13.
 * <p>
 * Frost
 */

public enum VideoPlayerState {

    IN_EXPANDED_VIEW,
    IN_MINI_VIEW,
    IN_LANDSCAPE_MODE,
    IN_PORTRAIT_MODE,
    IN_PIP
}
