package supporters.constants;


import com.frost.leap.BuildConfig;

import java.util.Arrays;
import java.util.List;

public class Constants {

    public enum Environment {
        PROD,
        DEV,
        TEST
    }

    public static final String ALLOW_LICENSE_CACHE = "allow_license_cache";

    public static final String DEEP_LEARN_RSA_PUBLIC_KEY = "deep_learn_rsa_public_key";

    public static final String RSA_PUBLIC_KEY = "MIHBMA0GCSqGSIb3DQEBAQUAA4GvADCBqwKBowlKuOMD8m2hC+7bdwsaL331wlFeP4WGDNLtHn7bZKsF5L88/gDru5opXO3q27gQ0qNsBkm2f2xt1jMqEpzctxL6ij09UMGNvu8mr8zh/+qqHUXa5IihJ0ir3epl5PR9QTTcqKlTpMSXXTXLPHXEcyDKWXJMyk6FvIqwd7YYQY+FkXAFlhp+4ebis/xnhP+to8G2aJICPdX7o+vG1o0pnip2losCAwEAAQ==";

    public static final Environment environment = BuildConfig.ENVIRONMENT_TYPE.equals("TEST") ? Environment.TEST
            : (BuildConfig.ENVIRONMENT_TYPE.equals("DEV")
            ? Environment.DEV : Environment.PROD);

    //MixPanel Token ID
    //    public static final String MIXPANEL_TOKEN_TEST = "08466a06bc5533f14f5b8053b92b1e2a";
    //    public static final String MIXPANEL_TOKEN_PROD = "08466a06bc5533f14f5b8053b92b1e2a";
    //    public static final String MIXPANEL_TOKEN_PROD = "ace5b8585b3fcb15364fb9a0d6527d6e";

    public static final String MIXPANEL_TOKEN = BuildConfig.MIXPANEL_TOKEN;

    public static final String VERSION_DETAILS = "Version: (" + BuildConfig.VERSION_NAME + ") ";

    public static final String APP_INFO = "Type: " + BuildConfig.ENVIRONMENT_TYPE + " " + VERSION_DETAILS;

    public static final String POWERED_BY = "Powered by: FROST INTERACTIVE";

    public static final String GIVE_ALL_PERMISSIONS = "Give all the permissions in order to access the application";

    public static final String PLATFORM = "ANDROID";

    public static final List<String> CATALOGUE_CATEGORIES = Arrays.asList("LEARN", "NOTES", "ASK");

    public static final List<String> CATALOGUE_OTS_CLAIM_CATEGORIES = Arrays.asList("LEARN", "NOTES", "ASK", "OTS");

    public static final List<String> DASHBOARD_TABS = Arrays.asList("UPDATES", "ARTICLES");

    public static final List<String> CATALOGUE_OVERVIEW = Arrays.asList("Overview", "Curriculum");

    public static final List<String> UNIT_CATEGORIES = Arrays.asList("Topics", "Notes");

    public static final List<String> HIGH_QUALIFICATIONS = Arrays.asList("B.Tech", "M.Tech");

    public static final List<String> CURRENT_GOALS = Arrays.asList("GATE 2021", "GATE 2022", "GATE 2023",
            "ESE 2021", "ESE 2022", "ESE 2023");

    public static final String TITLE = "title";

    public static final String FRAGMENT_KEY = "fragment_key";

    public static final String ACTION_KEY = "action_key";

    public static final String OFFLINE_MODE = "Offline Mode";

    public static final String CONNECTION_ERROR = "Network Error";

    public static final String PLEASE_CHECK_INTERNET = "Please Check Your Internet Connection";

    public static final String APP_NAME = "Deep Learn";

    private static final String PACKAGE = "com.frost.leap";

    public static final String PREFS = PACKAGE;

    public static final String APP_TAG_MESSAGE = "#Stay Home. Stay Safe.";

    public static final String RD_PENDING_DOWNLOADS = "rd_pending_downloads";

    public static final String NOTIFY_DOWNLOADS = "notify_downloads_";


    public static final String RECEIVER = "receiver";

    public static int GALLERY_LIMIT = 1;

    public static final long MEDIA_SIZE_LIMIT_IN_BYES = 30 * 1024 * 1024;

    public static final String SAMPLE_FILE_SIZE = "51200";

    public static final String APP_STORE_BANNER = "app_store_banner";

    public static final String APP_UPDATE_VERSION_DETAILS = "app_update_version_details";

    public static final String DRM_SECURITY_LEVEL = "drm_security_level";

    public static final String CREDENTIALS = "credentials";

    public static final String AUTO_RESOLUTIONS = "auto_resolutions";

    public static final String PHONE_SUPPORT = "phone_support";

    public static final String USER_CATALOGUES = "user_catalogues";

    public static final String CATALOGUE_SUBJECTS = "catalogue_subjects_";

    public static final String UNIT_TOPICS = "unit_topics_";

    public static final String HLS_VIDEOS = "hls_videos";

    public static final String DRM_LICENSE_TAG = "drm_license_tag";

    public static final String DRM_OFFLINE_TAG = "drm_offline_tag";

    public static final String LICENSE_DRM = "license_drm_";

    public static final String INFO_VIDEO = "info_video_";

    public static final String EXPIRY_DRM = "expiry_drm_";

    public static final String LICENSE_DURATION_DRM = "license_duration_drm_";

    public static final String DOWNLOAD_LOCATION = "download_location";

    public static final String APP_DATABASE = "deep_database";

    public static final String FILE_PROVIDER_AUTHORITY = PACKAGE + ".fileprovider";

    public static final String SOMETHING_WENT_WRONG = "Something Went Wrong";

    public static final String SOMETHING_WENT_WRONG_SERVER = "Something Went Wrong in Server";

    public static final String UN_AUTHORIZE = "Unauthorized";

    public static final String AUTHORIZATION = "authorization";

    public static final String PLEASE_FILL_THE_FIELDS = "Please fill the fields";

    public static final String ERROR = "Error";

    public static final String REJECTED = "Rejected";

    public static final String ACCEPTED = "Accepted";

    public static final String USER_FCM_TOKEN = "user_fcm_token";

    public static final String OTP_WORD_COUNT = "otp_word_count";

    public static final String TOKEN = "token";

    public static final String RESOLUTION = "resolution";

    public static final int COUNT_DOWN_TIME = 15000;

    public static final int COUNT_DOWN_INTERVAL = 1000;


    public static final String EDITED_PROFILE_PIC = "DEEP_LEARN_USER_EDITED_";

    public static final String CACHE_PDF_FILE = "DEEP_LEARN_CACHE_PDF_FILE.pdf";

    public static final int RETROFIT_CONNECTION_TIME_OUT = 5;

    public static final String[] YEAR_MONTH = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct", "Nov", "Dec"};

    public static final long UNDER_EXPIRY_TIME_LIMIT = 15 * 24 * 60 * 60;

    public static final String NO_DATA_AVAILABLE = "No data available";

    public static final String SAMPLE_USER_PROFILE_PIC = "https://lh5.googleusercontent.com/-nqHdVgQyBl4/AAAAAAAAAAI/AAAAAAAAAZQ/Zh8U9kHXWwM/il/photo.jpg";

    public static final String AVATAR_IMAGE = "https://ssl.gstatic.com/images/branding/product/1x/avatar_circle_blue_512dp.png";

    public static final String PLACE_HOLDER_IMAGE = "http://placehold.it/120x120&text=%20";

    public static final String USER_NAME = "user_name";

    public static final String USER_STATUS = "user_status";

    public static final String IS_LOGIN = "is_login";

    public static final String IS_LICENSE_MIGRATED = "is_license_migrated";

    public static final int BUFFER_SIZE = 8192;

    public static final String ACTION_NETWORK_STATE = "app_action_network_state";

    public static final String ACTION_UPDATE_OTP = "app_action_update_otp";

    public static final String ACTION_UPDATE_SUBJECT_PROGRESS = "app_action_update_subject_progress";

    public static final String ACTION_UPDATE_TOPIC_PROGRESS = "app_action_update_topic_progress";

    public static final String ACTION_UPDATE_VIDEO_URLS = "app_action_update_video_urls";

    public static final String ACTION_UPDATE_NOTES = "app_action_update_notes";

    public static final String ACTION_CMS_UPDATES = "app_action_cms_updates";


    public static final String DOWNLOAD_RESOLUTION_QUALITY = "download_resolution_quality";

    public static final String ASK_ME_EVERYTIME = "ask_me_everytime";

    public static final String IS_FIRST_LOGIN = "is_first_login";


    public static final int SECOND_MILLIS = 1000;

    public static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;

    public static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;

    public static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static final String CIPHER_NAME = "AES/CBC/PKCS5PADDING";

    public static final int CIPHER_KEY_LEN = 16; //128 bits

    public static final String IS_DARK_MODE = "is_dark_mode";

    public static final String AUTO_DARK_MODE = "auto_dark_mode";

    public static final String KEYSTORE_PASSWORD = "#@nd-ileap";

    public static final String VIDEO_QUALITY = "video_quality";


    public static final String ORG_CODE = BuildConfig.ORG_CODE;
    public static String PACKAGE_GROUP_CODE = "demoGroup";
    public static String PACKAGE_CODE = "demoPackage";
    public static final String STUDENT_TYPE_CODE = "deep-learn-students";


    public static final String BASE_URL = BuildConfig.BASE_URL;

    public static final String AUTH_PREFIX = BuildConfig.AUTH_PREFIX;
    public static final String STUDENT_PREFIX = BuildConfig.STUDENT_PREFIX;
    public static final String USER_PREFIX = BuildConfig.USER_PREFIX;
    public static final String CMS_PREFIX = BuildConfig.CMS_PREFIX;
    public static final String SUBJECTS_PREFIX = BuildConfig.SUBJECTS_PREFIX;
    public static final String PUBLISHER_PREFIX = BuildConfig.PUBLISHER_PREFIX;
    public static final String PAYMENT_PREFIX = BuildConfig.PAYMENT_PREFIX;

    public static final String FRESH_DESK_URL = "https://frostsupport.freshdesk.com/";

    public static final String FRESH_DESK_AUTH_KEY = BuildConfig.FRESH_DESK_AUTH_KEY;

    public static final String OTS_LOGIN_URL = "https://uat-ots-test.aceenggacademy.com/";

    public static final String OTS_APP_URL = "https://play.google.com/store/apps/details?id=com.aceenggacademy.mobile&hl=en";

    public static final String ACE_WEBSITE_URL = "https://www.aceenggacademy.com/";

    public class UserInfo {

        public static final String USER_ID = "user_id";

        public static final String COUNTRY_CODE = "country_code";

        public static final String MOBILE_NUMBER = "mobile_number";

        public static final String EMAIL = "email";

        public static final String FIRST_NAME = "first_name";

        public static final String LAST_NAME = "last_name";

        public static final String DATE_OF_BIRTH = "date_of_birth";

        public static final String PROFILE_PIC = "profile_pic";

        public static final String USER_THEME = "user_theme";

        public static final String GENDER = "gender";

        public static final String USER_DATA = "user_data";
    }


    public class UserConstants {

        // LOGIN VERIFY REFESH TOKEN LOGOUT

        public static final String REQUEST_LOGIN_URL = AUTH_PREFIX + "/student/send/otp";

        public static final String VERIFY_LOGIN_OTP_URL = AUTH_PREFIX + "/login/student";

        public static final String REFRESH_TOKEN_URL = AUTH_PREFIX + "/refresh/token";

        public static final String USER_LOGOUT_URL = AUTH_PREFIX + "/signout";

        // DEVICE
        public static final String UPDATE_DEVICE_DETAILS_URL = USER_PREFIX + "/studentMobileAppVersion/update";


        // REGISTER
        public static final String CHECK_USER_EXIST_URL = USER_PREFIX + "/user/check/mobile";

        public static final String REGISTER_USER_URL = USER_PREFIX + "/user/student";


        // LOST PHONE
        public static final String REQUEST_LOST_PHONE_URL = USER_PREFIX + "/user/forgot/password";

        public static final String VERIFY_EMAIL_OTP_URL = USER_PREFIX + "/user/verify/otp";

        public static final String ALTERNATIVE_MOBILE_URL = USER_PREFIX + "/user/update/mobile";

        public static final String VERIFY_OTP_ALTERNATIVE_MOBILE_URL = USER_PREFIX + "/user/mobile/otpverify";


        // TERMS
//        public static final String TERMS_AND_USES_URL = "https://docs.google.com/gview?embedded=true&url=https://d1k3nlqshgkoey.cloudfront.net/resources/e83fbcf4-0fd4-42cf-9f8a-793c3322eee4-Termsofuse.pdf";
//        public static final String TERMS_AND_USES_URL = "https://docs.google.com/gview?embedded=true&url=https://deep-learn.in/assets/images/Termsofuse.pdf";
        public static final String TERMS_AND_USES_URL = "https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fterms-conditions%2Ftermsandconditions.pdf?alt=media&token=3487867c-54dd-492c-b8e6-258c75d23318";

        //        public static final String PRIVACY_POLICIES_URL = "https://docs.google.com/gview?embedded=true&url=https://d1k3nlqshgkoey.cloudfront.net/Privacy_Policy.pdf";
//        public static final String PRIVACY_POLICIES_URL = "https://docs.google.com/gview?embedded=true&url=https://res.cloudinary.com/chinna972/image/upload/v1579589143/privacy_policy.pdf";
        public static final String PRIVACY_POLICIES_URL = "https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fprivacy-policy%2Fprivacy_policy.pdf?alt=media&token=903f5895-9eb7-4bcc-b49c-2ab70fe7de11";


        //FAQ'S
        public static final String FAQS = "https://deep-learn.in/#/index/previewFAQ";


        // PROFILE
        public static final String GET_USER_DETAILS_URL = STUDENT_PREFIX + "/student/profile/";

        public static final String UPDATE_USER_DETAILS = STUDENT_PREFIX + "/student/updateprofileinfo";

        public static final String UPLOAD_PROFILE_PIC_S3_URL = STUDENT_PREFIX + "/student/updateimage";

        public static final String GET_USER_LINK_DEVICES_URL = USER_PREFIX + "/user/devices";

        public static final String UPDATE_PASSWORD_URL = USER_PREFIX + "/user/updatepassword";

        public static final String UPDATE_MOBILE_URL = USER_PREFIX + "/user/updatemobile";

        public static final String VERIFY_OTP_URL = USER_PREFIX + "/user/mobile/otpverify";


        // ASK QUERY
        public static final String SUPPORT_INFO_URL = STUDENT_PREFIX + "/student/support";

        public static final String ADD_FRESH_DESK_TICKET = FRESH_DESK_URL + "api/v2/tickets";

        public static final String ASK_AN_EXPERT_URL = FRESH_DESK_URL + "api/v2/tickets";


        // INVOICES
        public static final String GET_USER_BILLING_HISTORY_URL = PAYMENT_PREFIX + "/razorpayinvoice/studentid/";

        public static final String INVOICE_PREVIEW_URL = BASE_URL + PAYMENT_PREFIX + "/order/orderid/";


        public static final String UPDATE_USER_INFO_IMAGE = STUDENT_PREFIX + "/student/info/updateimage";

        public static final String UPDATE_OTS_DETAILS = STUDENT_PREFIX + "/student/details";

        public static final String GET_ADDITIONAL_DETAILS = STUDENT_PREFIX + "/student/additional/";

        public static final String UPDATE_ADDITIONAL_DETAILS = STUDENT_PREFIX + "/student/additional/details";


    }

    public class CatalogueConstants {

        public static final String GET_CATALOGUES_LIST_URL = STUDENT_PREFIX + "/catalogues/student/studentid/";

        public static final String GET_SUBJECTS_LIST_URL = STUDENT_PREFIX + "/student/catalogue/subjects/";

        public static final String GET_TOPICS_LIST_URL = STUDENT_PREFIX + "/student/unit/topics/";

        public static final String GET_SUBTOPIC_VIDEO_PATH_URL = CMS_PREFIX + "/pagecontent/pagecontentid/";

        public static final String GET_SUBTOPIC_RATING = STUDENT_PREFIX + "/studentsubtopicrating/";

        public static final String POST_SUBTOPIC_RATING = STUDENT_PREFIX + "/studentsubtopicrating";

    }

    public class QuizConstants {

        public static final String QUIZ_QUESTION_DATA_URL = STUDENT_PREFIX + "/student/studentquiz/quizid";

        public static final String SUBMIT_QUIZ_URL = STUDENT_PREFIX + "/student/studentquiz/submitquiz";

        public static final String UPDATE_STATUS_QUIZ_URL = STUDENT_PREFIX + "/student/update/subject/quiz";

        public static final String PREVIEW_QUIZ_URL = STUDENT_PREFIX + "/student/studentquiz/preview/";

    }

    public class VideoConstants {

        public static final String DRM_SERVER_URL = BASE_URL + CMS_PREFIX + "/05E5F765721CCD6F01682B2E2B0B3EC10B3DF63D2C76DB85";

        public static final String DRM_SERVER_MOBILE_URL = BASE_URL + CMS_PREFIX;

        public static final String DRM_LICENSE_TAG = "/mobile/05E5F765721CCD6F01682B2E2B0B3EC10B3DF63D2C76DB85";

        public static final String DRM_SERVER_OFFLINE_URL = BASE_URL + CMS_PREFIX;

        public static final String DRM_OFFLINE_TAG = "/mobile/05E5F765721CCD6F01682B2E2B0B3EC10B3DF63D2C76DB85/offline";

        public static final String VIDEO_STREAM_UPDATE = STUDENT_PREFIX + "/student/update/subject/subtopic";

        public static final String GET_VIDEO_URLS = CMS_PREFIX + "/pagecontent/pagecontentids/all";

        public static final String GET_XML_TOKEN = CMS_PREFIX + "/getXmlToken";

        public static final long VIDEO_EXPIRY_TIME_LIMIT = 20 * 24 * 60 * 60 * 1000;
    }

    public class ResourcesConstants {

        public static final String CATALOGUE_RESOURCES = STUDENT_PREFIX + "/student/catalogue/resource/studentcatalogueId/";
    }

    public class NotesConstants {


        public static final String GET_SUBJECTS_INFO = STUDENT_PREFIX + "/mobile/student/subjects/notes/";

        public static final String CRUD_NOTES = STUDENT_PREFIX + "/student/subject/studentnote";


        public static final String GET_NOTES_LIST = STUDENT_PREFIX + "/mobile/student/subjects/studentnotes/studentsubjectids";


        public static final String GET_USER_NOTES = STUDENT_PREFIX + "/mobile/student/subjects/notes/asc/studentsubjectids";
    }

    public class StoreConstants {

        public static final String STORE_CATALOGUES = PUBLISHER_PREFIX + "/mobile/student/catalogue/all";

        public static final String STORE_CATALOGUES_BY_CATEGORY = PUBLISHER_PREFIX + "/mobile/student/category/all";

        public static final String GET_STORE_CATALOGUE_PREVIEW = PUBLISHER_PREFIX + "/mobile/student/catalogueid/";

        public static final String STUDENT_ORDER_URL = PAYMENT_PREFIX + "/studentorder";

        public static final String UPDATE_PAYMENT_STATUS_URL = PAYMENT_PREFIX + "/razorpaypayment";

        public static final String GET_CATALOGUE_SUBSCRIPTION_STATUS = STUDENT_PREFIX + "/student/catalogue/check/studentid/catalogueid";

    }

    public class CouponConstants {

        public static final String GET_COUPON_INFO = PUBLISHER_PREFIX + "/coupon/couponcode/";
    }

    public class UpdateConstants {
        public static final String GET_UPDATES_LIST = "notifications/studentupdates";

        public static final String GET_UPDATE_DATA_BY_ID = "notifications/update/updateid";

        public static final String UPDATE_CHOICE_TYPE = "notifications/updatechoicetype";
    }

    public class LeadSquared {

        public static final String LEAD_SQUARED_ACCESS_KEY = "u$rc7f0781e82714640443332eacca6e5e9";

        public static final String LEAD_SQUARED_SECRET_KEY = "7bd3e86027d8e0cbd654b19576b4babf687fc9c3";

        public static final String CREATE_LEAD = BuildConfig.LEAD_SQUARED_BASE_URL + "LeadManagement.svc/Lead.Create";
//
//        public static final String UPDATE_LEAD = BuildConfig.LEAD_SQUARED_BASE_URL + "LeadManagement.svc/Lead.Update?" +
//                "accessKey=" + LeadSquared.LEAD_SQUARED_ACCESS_KEY
//                + "&secretKey=" + LeadSquared.LEAD_SQUARED_SECRET_KEY
//                + "&leadId=";
//
//        public static final String GET_LEAD_BY_EMAIL_ID = BuildConfig.LEAD_SQUARED_BASE_URL + "LeadManagement.svc/Leads.GetByEmailaddress?" +
//                "accessKey=" + LeadSquared.LEAD_SQUARED_ACCESS_KEY
//                + "&secretKey=" + LeadSquared.LEAD_SQUARED_SECRET_KEY
//                + "&emailaddress=";


        public static final String UPDATE_LEAD = BuildConfig.LEAD_SQUARED_BASE_URL + "LeadManagement.svc/Lead.Update";

        public static final String GET_LEAD_BY_EMAIL_ID = BuildConfig.LEAD_SQUARED_BASE_URL + "LeadManagement.svc/Leads.GetByEmailaddress";

    }

}
