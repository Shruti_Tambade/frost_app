package supporters.constants;

public enum RController {
    NO_INTERNET,
    LOADING,
    LOADED,
    LOAD_MORE,
    SOMETHING_WENT_WRONG,
    NETWORK_ERROR_RETRY,
    IN_SEARCH,
    DONE,
    NO_DATA
}
