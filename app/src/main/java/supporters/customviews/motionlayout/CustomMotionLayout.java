package supporters.customviews.motionlayout;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import androidx.constraintlayout.motion.widget.MotionLayout;

import com.frost.leap.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import supporters.utils.Logger;


public class CustomMotionLayout extends MotionLayout {
    private View viewToDetectTouch;
    private GestureDetector gestureDetector;

    public synchronized View getViewToDetectTouch() {
        if (viewToDetectTouch == null) {
            viewToDetectTouch = findViewById(R.id.topContainer);
        }
        return findViewById(R.id.topContainer);
    }


    private Rect viewRect = new Rect();
    private boolean touchStarted = false;
    private List<TransitionListener> transitionListenerList = new ArrayList<>();

    private boolean isCollapsed = false, isTouchReleased = false;

    {

        addTransitionListener(new TransitionListener() {

            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {

            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {

            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {
                switch (i) {
                    case R.id.expanded:
                        isTouchReleased = false;
                        isCollapsed = false;
                        break;
                    case R.id.collapsed:
                        isCollapsed = true;
                        break;
                }
            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {

            }
        });

        super.setTransitionListener(new TransitionListener() {

            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {

            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
                //removeNulls(transitionListenerList);
                transitionListenerList.removeAll(Collections.singleton(null));
                for (TransitionListener list : transitionListenerList) {
                    list.onTransitionChange(motionLayout, i, i1, v);
                }

            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {
                transitionListenerList.removeAll(Collections.singleton(null));
                for (TransitionListener list : transitionListenerList) {
                    list.onTransitionCompleted(motionLayout, i);
                }

            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {

            }
        });
    }

    public CustomMotionLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setTransitionListener(TransitionListener listener) {
        addTransitionListener(listener);
    }

    public void addTransitionListener(TransitionListener listener) {
        transitionListenerList.add(listener);
    }

    public void addGestureDetector(GestureDetector gestureDetector) {
        this.gestureDetector = gestureDetector;
    }

//    GestureDetector gestureDetector = new GestureDetector(super.getContext(), new GestureDetector.SimpleOnGestureListener() {
//        @Override
//        public boolean onSingleTapConfirmed(MotionEvent e) {
//            transitionToEnd();
//            return false;
//        }
//    });


    float downx = 0, downy = 0, upx = 0, upy = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gestureDetector != null) {
            gestureDetector.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downx = event.getX();
                downy = event.getY();

                if (!isCollapsed && !isTouchReleased)
                    isTouchReleased = false;
                Logger.d("ACTION_DOWN", "Transition started");
                break;

            case MotionEvent.ACTION_UP:
                upx = event.getX();
                upy = event.getY();
                if (isCollapsed && downx == upx && downy == upy) {
                    this.transitionToStart();
                    isTouchReleased = false;
                } else
                    isTouchReleased = true;
                Logger.d("ACTION_UP", "Transition Released");
                break;

            case MotionEvent.ACTION_MOVE:
                upx = event.getX();
                upy = event.getY();
                if (!isCollapsed && !isTouchReleased)
                    isTouchReleased = false;

                Logger.d("ACTION_MOVE", "Transition Touch MOVING");
                break;


        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_UP:
                touchStarted = false;
                return super.onTouchEvent(event);
            case MotionEvent.ACTION_CANCEL:
                touchStarted = false;
                return super.onTouchEvent(event);
        }

        if (!touchStarted) {
            if (getViewToDetectTouch() == null) {
                return false;
            } else {
                getViewToDetectTouch().getHitRect(viewRect);
                touchStarted = viewRect.contains(((int) event.getX()), ((int) event.getY()));
            }
        }

        return touchStarted && super.onTouchEvent(event);
    }


}
