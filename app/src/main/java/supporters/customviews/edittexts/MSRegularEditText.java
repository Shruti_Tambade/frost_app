package supporters.customviews.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import supporters.utils.Utility;


public class MSRegularEditText extends AppCompatEditText {
    public MSRegularEditText(Context context) {
        super(context);
        init();
    }

    public MSRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MSRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(Utility.getTypeface(19, getContext()));
    }

}

