package supporters.customviews.edittexts;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import supporters.utils.Utility;


public class SSPRegularEditText extends AppCompatEditText {
    public SSPRegularEditText(Context context) {
        super(context);
        init();
    }

    public SSPRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SSPRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(Utility.getTypeface(14, getContext()));
    }

}

