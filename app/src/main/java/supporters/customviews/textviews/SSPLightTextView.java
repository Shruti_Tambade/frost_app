package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;


public class SSPLightTextView extends AppCompatTextView
{
    public SSPLightTextView(Context context) {
        super(context);
        init();
    }

    public SSPLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SSPLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {

        setTypeface(Utility.getTypeface(13,getContext()));
    }

}