package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 26/05/2020.
 */
public class ProductSansBoldTextView extends AppCompatTextView {
    public ProductSansBoldTextView(Context context) {
        super(context);
        init();
    }

    public ProductSansBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProductSansBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(Utility.getTypeface(25,Utility.getContext()));
    }
}

