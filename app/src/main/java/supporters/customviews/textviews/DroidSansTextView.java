package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/3/2019.
 */
public class DroidSansTextView extends AppCompatTextView {
    public DroidSansTextView(Context context) {
        super(context);
        init();
    }

    public DroidSansTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DroidSansTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(Utility.getTypeface(23,Utility.getContext()));
    }
}

