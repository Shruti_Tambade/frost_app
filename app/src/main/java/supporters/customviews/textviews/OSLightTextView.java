package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;


public class OSLightTextView extends AppCompatTextView
{
    public OSLightTextView(Context context) {
        super(context);
        init();
    }

    public OSLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OSLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {

        setTypeface(Utility.getTypeface(0,getContext()));
    }

}