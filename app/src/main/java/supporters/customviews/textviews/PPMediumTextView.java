package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;


public class PPMediumTextView extends AppCompatTextView
{
    public PPMediumTextView(Context context) {
        super(context);
        init();
    }

    public PPMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PPMediumTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(10,getContext()));
    }

}

