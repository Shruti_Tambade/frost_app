package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;


public class SSPRegularTextView extends AppCompatTextView {
    public SSPRegularTextView(Context context) {
        super(context);
        init();
    }

    public SSPRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SSPRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        setTypeface(Utility.getTypeface(14, getContext()));
    }

}