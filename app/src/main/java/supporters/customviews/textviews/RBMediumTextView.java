package supporters.customviews.textviews;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import supporters.utils.Utility;


public class RBMediumTextView extends AppCompatTextView
{
    public RBMediumTextView(Context context) {
        super(context);
        init();
    }

    public RBMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RBMediumTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        setTypeface(Utility.getTypeface(6,getContext()));
    }

}
