package supporters.utils;

import android.os.Bundle;

import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.generic.ShareDataManager;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mixpanel.android.BuildConfig;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import apprepos.topics.TopicRepositoryManager;
import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import supporters.constants.Constants;

/**
 * Created by CHENNA RAO on 2020-02-04.
 */
public class Mixpanel {

    private static MixpanelAPI mixpanel;
    private static JSONObject props;
    private static JSONArray jsonArray, jsonArray1;
    private static User user;
    private static UserRepositoryManager userRepositoryManager;
    private static boolean isAllow = BuildConfig.DEBUG ? true : true;

    private static FirebaseAnalytics mFirebaseAnalytics;

    /*
     * verifing the mixpanel
     */
    public static void verifyMixpanel(boolean isSignup) {

        if (!isAllow)
            return;

        if (mixpanel == null)
            mixpanel = MixpanelAPI.getInstance(Utility.getContext(), Constants.MIXPANEL_TOKEN);

        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        // Obtain the FirebaseAnalytics instance.
        if (mFirebaseAnalytics == null)
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(Utility.getContext());

        if (mixpanel.getPeople() != null)
            mixpanel.getPeople().getNotificationIfAvailable();

        try {
            props = new JSONObject();
            jsonArray = new JSONArray();
            jsonArray1 = new JSONArray();

            if (isSignup)
                props.put("Plan Type", "Free");

            props.put("$time", Utility.getMixPanleUTC());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public static void signupButtonClick() {

        if (!isAllow)
            return;

        verifyMixpanel(true);

        mixpanel.track("Click Signup", props);
        mixpanel.flush();


        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.METHOD, "Testing");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);


    }

    public static void recoveryButtonClick() {

        if (!isAllow)
            return;

        verifyMixpanel(true);

        mixpanel.track("Click Recovery", props);
        mixpanel.flush();

    }

    public static void createSignUp(boolean isVerifyOtp, User userData, boolean isSubmitButton, String signupMessage) {

        if (!isAllow)
            return;

        user = userData;
        verifyMixpanel(true);

        mixpanel.identify(user.getCredentials().getEmail());
        createSignupEvent(isSubmitButton, isVerifyOtp, signupMessage);

        if (isVerifyOtp && signupMessage.equals("Successfull Signup")) {

            mixpanel.getPeople().identify(user.getCredentials().getEmail());
            mixpanel.getPeople().set("Plan Type", "Free");
            mixpanel.getPeople().set("First Name", user.getProfileInfo().getFirstName());
            mixpanel.getPeople().set("Last Name", user.getProfileInfo().getLastName());
            mixpanel.getPeople().set("$name", user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName());
            mixpanel.getPeople().set("$email", user.getCredentials().getEmail());
            mixpanel.getPeople().set("Mobile", "+91 " + user.getCredentials().getMobile());
            mixpanel.getPeople().set("Date of SignUp", Utility.getMixPanleUTC());

            mixpanel.track("Sign Up Verify OTP ", props);
        } else {

            mixpanel.track("Sign Up Submit", props);
        }

        mixpanel.flush();

    }

    private static void createSignupEvent(boolean isSubmitButton, boolean isVerifyOtp, String signupMessage) {

        try {
            props.put("First Name", user.getProfileInfo().getFirstName());
            props.put("Last Name", user.getProfileInfo().getLastName());
            props.put("$name", user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName());
            props.put("$email", user.getCredentials().getEmail());
            props.put("Mobile", "+91 " + user.getCredentials().getMobile());
            if (isVerifyOtp)
                props.put("Status", signupMessage);
            else
                props.put("Status", isSubmitButton);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void signupResendOTP(User userData, boolean isSuccess) {

        if (!isAllow)
            return;

        user = userData;
        verifyMixpanel(true);

        try {
            props.put("First Name", user.getProfileInfo().getFirstName());
            props.put("Last Name", user.getProfileInfo().getLastName());
            props.put("$name", user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName());
            props.put("$email", user.getCredentials().getEmail());
            props.put("Mobile", "+91 " + user.getCredentials().getMobile());
            if (isSuccess)
                props.put("Status", "OTP resent Successfully");
            else
                props.put("Status", "OTP resend Failure");

            mixpanel.track("Sign Up Verify Resend", props);
            mixpanel.flush();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void clickLogin(String mobile, boolean isSuccess) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {

            props.put("Mobile", "+91 " + mobile);
            props.put("Status", isSuccess);

            mixpanel.track("Login", props);
            mixpanel.flush();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void loginVerifyResend(String mobile, boolean isSuccess) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            if (isSuccess) {
                props.put("Mobile", "+91 " + mobile);
                props.put("Status", "OTP resent Successfully");
            } else
                props.put("Status", "OTP resend Failure");

            mixpanel.track("Login Verify Resend", props);
            mixpanel.flush();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void loginVerify(boolean isVerifyOtp, String message, User user, String mobile, boolean isDashBoard) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            if (isVerifyOtp) {

                mixpanel.identify(user.getCredentials().getEmail());
                mixpanel.getPeople().identify(user.getCredentials().getEmail());
                mixpanel.getPeople().set("First Name", user.getProfileInfo().getFirstName());
                mixpanel.getPeople().set("Last Name", user.getProfileInfo().getLastName());
                mixpanel.getPeople().set("$name", user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName());
                mixpanel.getPeople().set("$email", user.getCredentials().getEmail());
                mixpanel.getPeople().set("Mobile", "+91 " + user.getCredentials().getMobile());
                mixpanel.getPeople().set("Most Recent Login", Utility.getMixPanleUTC());
                mixpanel.getPeople().set("Distinct ID", user.getCredentials().getEmail());

                props.put("First Name", user.getProfileInfo().getFirstName());
                props.put("Last Name", user.getProfileInfo().getLastName());
                props.put("$name", user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName());
                props.put("$email", user.getCredentials().getEmail());
                props.put("Mobile", "+91 " + user.getCredentials().getMobile());

            } else
                props.put("Mobile", "+91 " + mobile);

            props.put("Status", message);

            if (!isDashBoard)
                mixpanel.track("Login Verify", props);

            mixpanel.flush();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void clickTabs(int type) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        if (type == 1) {
            mixpanel.track("Dashboard Button");
        } else if (type == 2) {
            mixpanel.track("Store Button");
        } else if (type == 3) {
            mixpanel.track("Settings Button");
        }

        mixpanel.flush();

    }

    public static void dashboardView(String message, List<String> courseList) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (courseList != null) {

            mixpanel.getPeople().set("Plan Type", message);

            for (int i = 0; i < courseList.size(); i++) {
                jsonArray.put(courseList.get(i));
                mixpanel.getPeople().union("Purchased Courses", jsonArray);
            }

            mixpanel.flush();
        }
    }

    public static void clickExporeNow(String email) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (email != null) {
            if (mixpanel.getPeople().getDistinctId() != null && !mixpanel.getPeople().getDistinctId().isEmpty())
                mixpanel.getPeople().set("Plan Type", "Free");
            else {
                mixpanel.identify(user.getCredentials().getEmail());
                mixpanel.getPeople().identify(user.getCredentials().getEmail());
                mixpanel.getPeople().set("Plan Type", "Free");
            }
        } else
            mixpanel.getPeople().set("Plan Type", "Free");

        mixpanel.track("Explore Now Button");

        mixpanel.flush();
    }

    public static void clickStoreCourse(String catalogueName, String subCategoryName) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        jsonArray.put(catalogueName);
        mixpanel.getPeople().union("Explored Courses", jsonArray);

        jsonArray1.put(subCategoryName);
        mixpanel.getPeople().union("Explored Subcategories", jsonArray1);


        mixpanel.track("Explore Course ");
        mixpanel.flush();

    }

    public static void applyOrRemoveCoupon(String courseName, String couponName, boolean isApplied, boolean isValidCoupon) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            if (isApplied)
                props.put("Coupon Name", couponName);
            props.put("Course Name", courseName);
            props.put("isValidCoupon", isValidCoupon);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isApplied)
            mixpanel.track("Apply Coupon", props);
        else
            mixpanel.track("Remove Coupon", props);

        mixpanel.flush();

    }

    public static void clickCompleteOrder(String catalogueName, String subScriptionType, String coursePrice, String couponCode) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Course Name", catalogueName);
            props.put("Subscription Type", subScriptionType);
            props.put("Course Price", coursePrice);

            if (couponCode != null)
                props.put("Applied Coupon", couponCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Complete Order", props);

        mixpanel.flush();
    }

    public static void orderComplete(String courseName, String subScriptionType, String coursePrice, String apppliedCoupon, String purChasedPrice, boolean orderComplete) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Course Name", courseName);
            props.put("Subscription Type", subScriptionType);
            props.put("Course Price", coursePrice);
            props.put("Purchased Price", purChasedPrice);

            if (apppliedCoupon != null)
                props.put("Applied Coupon", apppliedCoupon);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (orderComplete) {

            if (apppliedCoupon != null) {
                jsonArray.put(apppliedCoupon);
                mixpanel.getPeople().union("Coupons", jsonArray);
            }

            jsonArray1.put(courseName);
            mixpanel.getPeople().union("Purchased Courses", jsonArray1);

            mixpanel.getPeople().set("Plan Type", "Premium");
            mixpanel.getPeople().set("Recent Purchase Date", Utility.getMixPanleUTC());


            mixpanel.track("Order Successful", props);

        } else {
            mixpanel.track("Order Failed", props);

        }
        mixpanel.flush();
    }

    public static void ClickGetStarted(String catalogueName) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Course Name", catalogueName);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        mixpanel.track("Click Get Started", props);
        mixpanel.flush();

    }

    public static void clickCurriculum(String catalogueName, boolean isTabClick) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Course Name", catalogueName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isTabClick)
            mixpanel.track("Curriculum Tab Button", props);
        else
            mixpanel.track("View Curriculum", props);
        mixpanel.flush();

    }

    public static void clickLogout() {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        mixpanel.getPeople().set("Recent Logout", Utility.getMixPanleUTC());

        try {
            props.put("Time Stamp", Utility.getMixPanleUTC());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Logout", props);
        mixpanel.flush();

    }

    public static void settingPageCLickEvents(int type) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (type == 1) {
            mixpanel.track("Profile Settings");
        } else if (type == 2) {
            mixpanel.track("Additional Settings");
        } else if (type == 3) {
            mixpanel.track("Billing History");
        } else if (type == 4) {
            mixpanel.track("Linked Devices");
        } else if (type == 5) {
            mixpanel.track("About Deep Learn");
        } else if (type == 6) {
            mixpanel.track("Support");
        } else if (type == 7) {
            mixpanel.track("FAQ");
        }

        mixpanel.flush();

    }

    public static void updateName(String firstName, String lastName, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS")) {
            mixpanel.getPeople().set("First Name", firstName);
            mixpanel.getPeople().set("Last Name", lastName);
            mixpanel.getPeople().set("$name", firstName + " " + lastName);
        }

        try {
            props.put("First Name", firstName);
            props.put("Last Name", lastName);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Name", props);
        mixpanel.flush();
    }

    public static void updateDOB(String dob, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("DOB", dob);

        try {
            props.put("DOB", dob);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update DOB", props);
        mixpanel.flush();
    }

    public static void updateGender(String gender, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("Gender", gender);

        try {
            props.put("Gender", gender);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Gender", props);
        mixpanel.flush();
    }

    public static void updateMobileNumber(String mobileNumber, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("Mobile", mobileNumber);

        try {
            props.put("Mobile", mobileNumber);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Mobile Number", props);
        mixpanel.flush();
    }

    public static void updatePassword(String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Password", props);
        mixpanel.flush();
    }

    public static void updateProfilePic(String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("status", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update ProfilePic", props);
        mixpanel.flush();
    }

    public static void updateFatherName(String fatherName, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("FatherName", fatherName);

        try {
            props.put("FatherName", fatherName);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Father Name", props);
        mixpanel.flush();
    }

    public static void updateHigherQualification(String higherQualification, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("Highest Qualification", higherQualification);

        try {
            props.put("Highest Qualification", higherQualification);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Qualification", props);
        mixpanel.flush();
    }

    public static void updateUniversityName(String universityName, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("University Name", universityName);

        try {
            props.put("University Name", universityName);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update University Name", props);
        mixpanel.flush();
    }

    public static void updateEmploymentStatus(String employementStatus, String companyName, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS")) {
            if (employementStatus.equals("EMPLOYED")) {
                mixpanel.getPeople().set("Employment Status", employementStatus);
                mixpanel.getPeople().set("Company Name", companyName);
            } else
                mixpanel.getPeople().set("Employment Status", employementStatus);
        }

        try {
            props.put("Employment Status", employementStatus);
            props.put("Company Name", companyName);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Employment Status", props);
        mixpanel.flush();
    }

    public static void updateCurrentGoal(String currentGoal, String status) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        if (status.equals("SUCCESS"))
            mixpanel.getPeople().set("Current Goal", currentGoal);

        try {
            props.put("Current Goal", currentGoal);
            props.put("isAllow", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Current Goal", props);
        mixpanel.flush();
    }

    public static void updateIDProofUpdate() {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        mixpanel.getPeople().set("ID Proof", "Uploded");

        try {
            props.put("isAllow", "SUCCESS");
            props.put("ID Proof", "Uploded");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update ID Proof", props);
        mixpanel.flush();
    }

    public static void updateDeliveryAddress(boolean isCollege) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            if (isCollege) {
                props.put("Delivery Address Type", "College Address");
                mixpanel.getPeople().set("Delivery Address Type", "College Address");
            } else {
                props.put("Delivery Address Type", "Other");
                mixpanel.getPeople().set("Delivery Address Type", "Other");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Update Delivery Address", props);
        mixpanel.flush();
    }

    public static void submitTicket(String courseName, String issueType, MenuItem menuItem, String message) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        jsonArray.put(issueType);
        mixpanel.getPeople().union("Issue Types Raised", jsonArray);

        try {
            if (menuItem != null) {
                props.put("Issue Type", menuItem.getIssueType());
                props.put("Issue Topic", menuItem.getIssueTopic());
                props.put("message", message);
                props.put("Course", courseName);
            }
            props.put("Issue Type", issueType);
            props.put("Course", courseName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Submit Ticket", props);
        mixpanel.flush();
    }

    public static void aceessCourse(String course, String validiy) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Validity", validiy);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Access Course", props);
        mixpanel.flush();
    }


    public static void accessSubjects(String course, String subjectName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Access Subject", props);
        mixpanel.flush();
    }

    public static void accessUnits(String course, String subjectName, String unitName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Access Unit", props);
        mixpanel.flush();
    }

    public static void accessSubTopics(String course, String subjectName, String unitName, String topicName, String subTopicName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Access Subtopic", props);
        mixpanel.flush();
    }

    public static void addRating(String course, String subjectName, String unitName, String topicName, String subTopicName, int rating) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);
            props.put("Rating", rating);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Add Rating", props);
        mixpanel.flush();
    }

    public static void addReview(String course, String subjectName, String unitName, String topicName, String subTopicName, int rating, String clarity, String presenetation, String depth) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);
            props.put("Rating", rating);

            props.put("Clarity", clarity);
            props.put("Presentation", presenetation);
            props.put("Depth of Understanding", depth);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Add Review", props);
        mixpanel.flush();
    }


    public static void accessTools(String course, String subjectName, String unitName, String topicName, String subTopicName, String type) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);
            props.put("Type", type);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Access Tools", props);
        mixpanel.flush();
    }

    public static void askExpertTool(String course, String subjectName, String unitName, String topicName, String subTopicName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Ask an Expert Tool", props);
        mixpanel.flush();
    }


    public static void cancelExpertPage(String course, String subjectName, String unitName, String topicName, String subTopicName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Cancel Expert Page", props);
        mixpanel.flush();
    }

    public static void submitQuestion(String course, String subjectName, String unitName, String topicName, String subTopicName, boolean isMedia) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);
            props.put("AddImage", isMedia);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Submit Question_Tool", props);
        mixpanel.flush();
    }

    public static void addNote(String course, String subjectName, String unitName, String topicName, String subTopicName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Save Note", props);
        mixpanel.flush();
    }

    public static void editNote(String course, String outCome) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Outcome", outCome);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Edit Note", props);
        mixpanel.flush();
    }

    public static void deleteNote(String course) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Delete Note", props);
        mixpanel.flush();
    }

    public static void cancelNote(String course, String subjectName, String unitName, String topicName, String subTopicName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Cancel Note", props);
        mixpanel.flush();
    }

    public static void notesTab(String course) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Notes_Tab", props);
        mixpanel.flush();
    }

    public static void notesFilter(String course) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Note Filter", props);
        mixpanel.flush();
    }

    public static void askExpertTab(String course) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Ask an Expert_Tab", props);
        mixpanel.flush();
    }

    public static void otsTab(String course) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("OTS_Tab", props);
        mixpanel.flush();
    }

    public static void subtmitQuestionTab(String course, String subjectName, String unitName, String topicName, String doubt, boolean isMedia) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("AddImage", isMedia);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Submit Question_Tab", props);
        mixpanel.flush();
    }


    public static void installedApplications() {
        if (!isAllow)
            return;
        verifyMixpanel(false);
        JSONArray installedApps = Utility.getUserAppsInMobile();
        if (installedApps != null) {
            mixpanel.getPeople().union("Installed Applications", installedApps);
        }
        mixpanel.flush();
    }

    public static void videoDownloadSuccess(JSONObject jsonObject) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            if (jsonObject == null)
                return;

            props.put("Course", jsonObject.getString("courseName"));
            props.put("Subject Name", jsonObject.getString("subjectName"));
            props.put("Unit Name", jsonObject.getString("unitName"));
            props.put("Topic Name", jsonObject.getString("topicName"));
            props.put("Subtopic Name", jsonObject.getString("subTopicName"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.getPeople().increment("Downloaded Videos", 1);

        mixpanel.track("Video Downloaded Successful", props);
        mixpanel.flush();
    }

    public static void clickVideoDownloadButton(String course, String subjectName,
                                                String unitName, String topicName,
                                                String subTopicName, String videoResolution) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);
            props.put("Video Resolution", videoResolution);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Video Downloaded Button Click", props);
        mixpanel.flush();
    }

    public static void cancelDownloadedVideo(String course, String subjectName,
                                             String unitName, String topicName,
                                             String subTopicName) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Course", course);
            props.put("Subject Name", subjectName);
            props.put("Unit Name", unitName);
            props.put("Topic Name", topicName);
            props.put("Subtopic Name", subTopicName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Cancel Downloaded Video", props);
        mixpanel.flush();
    }

    public static void recoveryEmailOTPSent(String email, String message) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Email", email);
            props.put("Message", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Email_OTP_Sent_Recovery", props);
        mixpanel.flush();
    }

    public static void recoveryEmailOTPVerfication(String email, String message) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Email", email);
            props.put("Message", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Email Verification Otp_Recovery", props);
        mixpanel.flush();
    }

    public static void verificationOTPResendRecovery(String email, String message) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Email", email);
            props.put("Message", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Email Verification Otp_Resend_Recovery", props);
        mixpanel.flush();
    }

    public static void recoveryMobileOTPSent(String email, String mobile, String message) {

        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Email", email);
            props.put("Mobile Number", mobile);
            props.put("Message", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Mobile_OTP_Sent_Recovery", props);
        mixpanel.flush();
    }

    public static void mobileverificationOTPResendRecovery(String email, String mobile, String message) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Email", email);
            props.put("Mobile Number", mobile);
            props.put("Message", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Mobile Verification Otp_Resend_Recovery", props);
        mixpanel.flush();
    }

    public static void recoveryMobileOTPVerfication(String email, String mobile, String message) {

        if (!isAllow)
            return;


        verifyMixpanel(false);

        try {
            props.put("Email", email);
            props.put("Mobile Number", mobile);
            props.put("Message", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("Mobile Verification Otp_Recovery", props);
        mixpanel.flush();
    }


    public static void addingExoPlayerError(String url, ExoPlaybackException error, boolean offline) {
        if (!isAllow)
            return;

        verifyMixpanel(false);

        try {
            props.put("Offline", offline);

            try {
                if (ShareDataManager.getInstance().getDashUrlsModel() != null && url.equalsIgnoreCase(ShareDataManager.getInstance().getDashUrlsModel().getDashUrl())) {
                    props.put("License", (ShareDataManager.getInstance().getDashUrlsModel().getLicense() == null ? "NO DATA" : ShareDataManager.getInstance().getDashUrlsModel().getLicense()));
                } else
                    props.put("License", "NO DATA");
            } catch (Exception e) {
                props.put("License", "NO DATA");
            }
            props.put("Error", error.getMessage());
            props.put("Error-Type", error.type);
            props.put("LocalizedMessage", error.getLocalizedMessage());
            props.put("SecurityLevel", TopicRepositoryManager.getInstance().getSecurityLevel() == null
                    ? "NULL" : TopicRepositoryManager.getInstance().getSecurityLevel());

            StringWriter stringWriter = new StringWriter();
            error.printStackTrace(new PrintWriter(stringWriter));
            props.put("PrintStackTrace", stringWriter.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mixpanel.track("ExoPlayer Play Back Exception", props);
        mixpanel.flush();

    }

}
