package supporters.utils;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.components.media.constants.MediaType;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.components.profile.models.Type;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.generic.dialog.CustomAlertDialog;
import com.frost.leap.generic.dialog.CustomUpdateAlertDialog;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.catalogue.model.benefit.BenefitModel;
import apprepos.introduction.model.IntroductionModel;
import apprepos.store.model.FeaturesModel;
import apprepos.store.model.InstructorsModel;
import apprepos.updates.model.UpdateModel;
import apprepos.user.UserRepositoryManager;
import apprepos.user.model.Invoice;
import id.zelory.compressor.Compressor;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.Permission;

public class Utility {


    private Utility() {

    }

    /*
     *  we are checking the internet is available or not
     *  it will return true or false
     *  */
    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                ShareDataManager.getInstance().setOnline(false);
                return false;
                /* aka, do nothing */
            }
            ShareDataManager.getInstance().setNetworkInfo(networkInfo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            ShareDataManager.getInstance().setOnline(false);
            return false;

        }
    }


    public static Context getContext() {
        return HelperApplication.getInstance().getApplicationContext();
    }


    public static String getIpAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                List<InetAddress> addresses = Collections.list(networkInterface.getInetAddresses());
                for (InetAddress address : addresses) {
                    if (!address.isLoopbackAddress()) {
                        return address.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } // for now eat exceptions
        return "192.168.0.1439";
    }

    /*
     *  Check the email id valid or not
     */
    public static boolean isValidEmail(CharSequence paramCharSequence) {
        if (paramCharSequence == null) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(paramCharSequence).matches();
    }

    /*
     *   Check the password is valid or not
     *   It check the password should contain minumum fields.
     *
     * */
    public static boolean isValidPassword(String password) {

        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,16}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static boolean isGPSEnable(Activity activity) {
        LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public void test() {

    }

    public static void forceShutdown() {
        int i = 1, j = 0;
        Logger.d("SHUTDOWN", "" + (i / j));
    }

    public static String getPhoneUniqueId(Context context) {
        return android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
    }

    public static int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getVideoPlayerHeight(Context context) {
        return getScreenWidth(context) * 9 / 16;
    }


    public static String getShortName(String firstName, String lastName) {

        if (firstName == null) {
            return "A";
        }
        if (lastName == null) {
            lastName = "";
        }
        firstName.trim().toLowerCase();
        lastName.trim().toLowerCase();

        StringBuilder sb = new StringBuilder();
        try {
            if (firstName.length() > 0) {
                sb.append(Character.toUpperCase(firstName.charAt(0)));
            }
            if (lastName.length() > 0) {
                sb.append(Character.toUpperCase(lastName.charAt(0)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ("" + firstName.charAt(0)).toUpperCase();
        }


        return sb.toString();
    }


    /*
     *   It checks the response is null or failure
     */
    public static boolean isSuccessful(JsonObject jsonObject) {
        try {

            if (jsonObject.get("error").getAsBoolean()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
     *   It checks the http status and return the status
     *   responsetype Non-Reactive Reactive
     *   httpstatus 200 or other
     */
    public static boolean isSuccessHeader(Response response) {
        try {
            if (response.headers().get("httpstatus").equals("succeed")) {
                return true;
            } else if (response.headers().get("responsetype").equals("Reactive")
                    && response.headers().get("httpstatus").equals("200")) {
                return true;
            } else if (response.headers().get("responsetype").equals("Non-Reactive")
                    && response.headers().get("httpstatus").equals("200")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /*
     *   If responce contains error it will return the what error message get from server
     */
    public static String getErrorMessage(JsonObject jsonObject) {
        try {
            return jsonObject.get("message").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
            return Constants.SOMETHING_WENT_WRONG;
        }
    }

    public static String getMessage(JsonObject jsonObject) {
        try {
            return jsonObject.get("message").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
            if (jsonObject.has("showMessage")) {
                return jsonObject.get("showMessage").getAsString();
            }
            return Constants.SOMETHING_WENT_WRONG;
        }
    }


    /*
     *   return the mobile Height in pixels
     **/
    public static int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static int getFullScreenHeight(Context context) {

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

//    public static int getScreenHeight(Context context)
//    {
//        return context.getResources().getDisplayMetrics().heightPixels - getNavigationBarHeight(context) - getStatusBarHeight(context);
//    }


    public static int getNavigationBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public static LinearLayout.LayoutParams getLayoutParams(double ratio, int currentWidth) {
        ratio = ratio == 0 ? 1 : ratio;
        return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) (currentWidth / ratio));
    }

    public static boolean isValidMobile(String paramString) {
        if (paramString == null)
            return false;
        return paramString.matches("[0-9]{10}");
    }

    /*
     *   if you want to set height and width  programatically
     *   should return the size in DP
     */
    public static int dpSize(Context context, int sizeInDp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (sizeInDp * scale + 0.5f);
    }

    public static Dialog generateProgressDialog(Context activity) {
        try {

            Dialog progressDialog = new Dialog(activity);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().getAttributes().windowAnimations = R.style.AppTheme;
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.getWindow().clearFlags(Window.FEATURE_NO_TITLE);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressdialog);
            return progressDialog;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void closeProgressDialog(Dialog progressDialog) {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /*
     *   set the elevation for Tollabr programatically
     */
    public static void setAppBarElevation(AppBarLayout appBarLayout, float elevation) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(new int[0], ObjectAnimator.ofFloat(appBarLayout, "elevation", elevation));
            appBarLayout.setStateListAnimator(stateListAnimator);
        }
    }


    /*
     *   it check the runtime permissions
     */
    public static boolean checkPermissionRequest(Permission permission, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            switch (permission) {
                case LOCATION:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ? false : true;
                case COARSE_LOCATION:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ? false : true;
                case READ_SMS:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ? false : true;
                case WRITE_SMS:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ? false : true;
                case READ_STORAGE:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ? false : true;
                case WRITE_STORAGE:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ? false : true;
                case CAMERA:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ? false : true;
                case RECORD:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ? false : true;
                case READ_CALENDAR:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED ? false : true;
                case WRITE_CALENDAR:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED ? false : true;

                case CALL:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ? false : true;

                case PHONE_STATE:
                    return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ? false : true;

            }
            return false;
        } else
            return true;
    }


    /*
     *   return the first text in Capitals in a word
     *   Example : sunday (Sunday)
     */
    public static String getCamelCase(String name) {
        if (name == null)
            return null;

        int flag = 1;
        name = name.trim();
        name = name.toLowerCase();

        while (flag == 1) {
            if (name.contains("  ")) {
                name = name.replaceAll("  ", " ");
            } else {
                flag = 0;
            }
        }

        StringBuilder sb = new StringBuilder();
        try {
            String[] words = name.split(" ");
            if (words[0].length() > 0) {
                sb.append(Character.toUpperCase(words[0].charAt(0)) + words[0].subSequence(1, words[0].length()).toString());
                for (int i = 1; i < words.length; i++) {
                    sb.append(" ");
                    sb.append(Character.toUpperCase(words[i].charAt(0)) + words[i].subSequence(1, words[i].length()).toString());
                }
            }
        } catch (Exception e) {
            //System.out.println(e.toString());
            e.printStackTrace();
            return name;
        }
        return sb.toString();

    }


    public static String getTextCase(String text) {
        if (text == null)
            return null;

        text = Character.toUpperCase(text.charAt(0))
                + text.substring(1);

        return text;
    }

    /*
     *   if user doesn't accept the runtime permission we again ask for permission
     */
    public static void raisePermissionRequest(Permission permission, Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            switch (permission) {
                case COARSE_LOCATION:
                    if (!checkPermissionRequest(Permission.COARSE_LOCATION, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, generateRequestCodes().get("LOCATION_REQUEST"));
                    }
                    break;
                case LOCATION:
                    if (!checkPermissionRequest(Permission.LOCATION, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, generateRequestCodes().get("LOCATION_REQUEST"));
                    }
                    break;

                case READ_STORAGE:
                    if (!checkPermissionRequest(Permission.READ_STORAGE, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, generateRequestCodes().get("READ_STORAGE_REQUEST"));
                    }
                    break;

                case WRITE_STORAGE:
                    if (!checkPermissionRequest(Permission.WRITE_STORAGE, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, generateRequestCodes().get("WRITE_STORAGE_REQUEST"));
                    }
                    break;

                case READ_SMS:
                    if (!checkPermissionRequest(Permission.READ_SMS, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_SMS}, generateRequestCodes().get("READ_SMS_REQUEST"));
                    }
                    break;
                case WRITE_SMS:
                    if (!checkPermissionRequest(Permission.WRITE_SMS, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.SEND_SMS}, generateRequestCodes().get("WRITE_SMS_REQUEST"));
                    }
                    break;

                case CAMERA:
                    if (!checkPermissionRequest(Permission.CAMERA, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, generateRequestCodes().get("CAMERA_REQUEST"));
                    }
                    break;
                case RECORD:
                    if (!checkPermissionRequest(Permission.RECORD, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO}, generateRequestCodes().get("RECORD_REQUEST"));
                    }
                    break;
                case READ_CALENDAR:
                    if (!checkPermissionRequest(Permission.READ_CALENDAR, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CALENDAR}, generateRequestCodes().get("READ_CALENDAR_REQUEST"));
                    }
                    break;
                case WRITE_CALENDAR:
                    if (!checkPermissionRequest(Permission.WRITE_CALENDAR, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_CALENDAR}, generateRequestCodes().get("WRITE_CALENDAR_REQUEST"));
                    }
                    break;
                case CALL:
                    if (!checkPermissionRequest(Permission.CALL, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, generateRequestCodes().get("CALL_PHONE"));
                    }
                    break;

                case PHONE_STATE:
                    if (!checkPermissionRequest(Permission.CALL, activity)) {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, generateRequestCodes().get("CALL_PHONE"));
                    }
                    break;
            }
        }
    }


    /*
     *   This is used to set the Result name
     */
    public static HashMap<String, Integer> generateRequestCodes() {
        HashMap<String, Integer> hashMap = new HashMap<>();

        //permissions
        hashMap.put("COARSE_LOCATION_REQUEST", 24000);
        hashMap.put("LOCATION_REQUEST", 24001);
        hashMap.put("WRITE_SMS_REQUEST", 24002);
        hashMap.put("READ_SMS_REQUEST", 24003);
        hashMap.put("READ_STORAGE_REQUEST", 24004);
        hashMap.put("WRITE_STORAGE_REQUEST", 24005);
        hashMap.put("CAMERA_REQUEST", 24006);
        hashMap.put("RECORD_REQUEST", 24007);
        hashMap.put("READ_CALENDAR_REQUEST", 24008);
        hashMap.put("WRITE_CALENDAR_REQUEST", 24009);
        hashMap.put("CALL_PHONE", 24010);


        // OTS REGISTRATION
        hashMap.put("OTS_REGISTRATION", 23001);

        //snap from camera
        hashMap.put("SNAP_FROM_CAMERA", 20001);
        hashMap.put("VIDEO_FROM_CAMERA", 20002);

        //pick media from gallery
        hashMap.put("MEDIA_FROM_GALLERY", 19001);

        //google
        hashMap.put("OPEN_PLACES_SEARCH", 4001);
        hashMap.put("GOOGLE_SIGN_IN", 4002);
        hashMap.put("ACCESS_GOOGLE_LOCATION", 4003);

        //profile
        hashMap.put("CHANGE_MOBILE_NUMBER_REQUEST", 2000);
        hashMap.put("USER_REGISTER", 2001);
        hashMap.put("EDIT_ADDRESS", 2002);

        //Profile
        hashMap.put("UPDATE_PROFILE", 1201);

        //Refresh
        hashMap.put("UPDATE_STORE_COURSE", 1251);

        //Refresh Update Cards
        hashMap.put("UPDATE_UPDATE_CARDS", 1252);


        hashMap.put("CREATE_EMPLOYEE", 1000);
        hashMap.put("CREATE_GROUP", 1001);
        hashMap.put("FILTER_EMPLOYEE", 1002);

        //image
        hashMap.put("IMAGE_CROP", 1101);


        //Update
        hashMap.put("UPDATE_DEVICE_DETAILS", 901);


        // Mike Search
        hashMap.put("REQUEST_MIKE_SEARCH", 302);

        // Mobile hint otp
        hashMap.put("REQUEST_HINT_MOBILE", 301);


        // License Expiry Verify
        hashMap.put("LICENSE_EXPIRY_VERIFY", 201);


        return hashMap;
    }

    /*
     *   it return the snackbar message with diffrent color based on the type
     */
    public static void showSnackBar(Context activity, View view, String text, int type) {
        if (view == null || text == null || activity == null) {
            return;
        }
        Snackbar snackBar = Snackbar.make(view, text, Snackbar.LENGTH_SHORT);
        TextView txtMessage = (TextView) snackBar.getView().findViewById(R.id.snackbar_text);
        txtMessage.setTextColor(ContextCompat.getColor(activity, R.color.white));
        if (type == 2)
            snackBar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.black));
        else if (type == 1)
            snackBar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.app_snack_bar_true));
        else {
            snackBar.getView().setBackgroundColor(ContextCompat.getColor(activity, R.color.bold_text_color));
        }
        txtMessage.setTypeface(getTypeface(5, activity));
        snackBar.show();
    }


    /*
     *   it return the font style
     */
    public static Typeface getTypeface(int value, Context context) {
        String path = "fonts/opensans/OpenSans-Regular.ttf";
        switch (value) {
            // open sans
            case 0:
                path = "fonts/opensans/OpenSans-Light.ttf";
                break;
            case 1:
                path = "fonts/opensans/OpenSans-Regular.ttf";
                break;
            case 2:
                path = "fonts/opensans/OpenSans-SemiBold.ttf";
                ;
                break;
            case 3:
                path = "fonts/opensans/OpenSans-Bold.ttf";
                break;

            // roboto
            case 4:
                path = "fonts/roboto/Roboto-Light.ttf";
                break;
            case 5:
                path = "fonts/roboto/Roboto-Regular.ttf";
                break;
            case 6:
                path = "fonts/roboto/Roboto-Medium.ttf";
                break;
            case 7:
                path = "fonts/roboto/Roboto-Bold.ttf";
                break;

            // poppins
            case 8:
                path = "fonts/poppins/Poppins-Light.ttf";
                break;

            case 9:
                path = "fonts/poppins/Poppins-Regular.ttf";
                break;

            case 10:
                path = "fonts/poppins/Poppins-Medium.ttf";
                break;

            case 11:
                path = "fonts/poppins/Poppins-SemiBold.ttf";
                break;

            case 12:
                path = "fonts/poppins/Poppins-Bold.ttf";
                break;


            // source sans pro

            case 13:
                path = "fonts/sourcessanspro/SourceSansPro-Light.ttf";
                break;
            case 14:
                path = "fonts/sourcessanspro/SourceSansPro-Regular.ttf";
                break;
            case 15:
                path = "fonts/sourcessanspro/SourceSansPro-SemiBold.ttf";
                break;
            case 16:
                path = "fonts/sourcessanspro/SourceSansPro-Bold.ttf";
                break;

            //montserrat

            case 17:
                path = "fonts/montserrat/Montserrat-Bold.otf";
                break;
            case 18:
                path = "fonts/montserrat/Montserrat-Medium.otf";
                break;
            case 19:
                path = "fonts/montserrat/Montserrat-Regular.otf";
                break;
            case 20:
                path = "fonts/montserrat/Montserrat-SemiBold.otf";
                break;

            //PlayfairDisplay

            case 21:
                path = "fonts/playfairdisplay/PlayfairDisplay-Regular.ttf";
                break;

            case 22:
                path = "fonts/playfairdisplay/PlayfairDisplay-Bold.ttf";
                break;

            //Droid Sans
            case 23:
                path = "fonts/droidsans/DroidSans.ttf";
                break;

            case 24:
                path = "fonts/droidsans/DroidSans-Bold.ttf";
                break;

            //Product Sans
            case 25:
                path = "fonts/productsans/ProductSansBold.ttf";
                break;

            case 26:
                path = "fonts/productsans/ProductSansNormal.ttf";
                break;


        }
        return Typeface.createFromAsset(context.getAssets(), path);
    }


    /*
     *   This method hide the keyboard
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = activity.getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Nullable
    public static Bitmap urlToBitMap(String urlLink) {
        try {
            URL url = new URL(urlLink);
            Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            return image;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /*
     *   it return the circle image in bitmaps
     */
    public static Bitmap getRoundBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    /*
     *   it set the image name taken from camera and return the file
     */
    public static File createImageFile(Activity activity) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = getContext().getResources().getString(R.string.app_name) + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile
                (
                        imageFileName,  /* prefix */
                        ".jpg",         /* suffix */
                        storageDir      /* directory */
                );
        Logger.d("Storage Dir", storageDir.getAbsolutePath());
        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }


    public static String getPath(final Context context, final Uri uri) {

        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/"
                                + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{split[1]};

                    return getDataColumn(context, contentUri, selection,
                            selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri
                .getAuthority());
    }


    public static int getCompressionQuality(double size) {
        // size in kb
        return size < 100 ? 100 :
                size < 200 ? 95 :
                        size < 500 ? 75 :
                                size < 1000 ? 60 :
                                        size < 2000 ? 45 :
                                                size < 5000 ? 30 :
                                                        size < 10000 ? 10 : 5;
    }


    public static void notifyMessage(String title, String message, boolean positive) {
        if (getContext() == null) {
            return;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(),
                "ILEAP_NOTIFY_CHANNEL_ALERT")
                .setSmallIcon(R.drawable.notify_logo)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setAutoCancel(true);

        if (title != null) {
            mBuilder.setContentTitle(title);
        } else {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                mBuilder.setContentTitle(Constants.APP_NAME);
            }
        }
        if (message != null) {
            mBuilder.setContentText(message);
        }

        mBuilder.setColor(Color.parseColor(positive ? "#007AFF" : "#000000"));
        mBuilder.setChannelId("ILEAP_NOTIFY_CHANNEL_ALERT");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL_ALERT", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(new Random().nextInt(99999), mBuilder.build());
        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
            notificationManager.notify(new Random().nextInt(99999), mBuilder.build());
        }

    }

    public static void notifyMessage(String title, String message, PendingIntent intent) {
        if (getContext() == null) {
            return;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(),
                "ILEAP_NOTIFY_CHANNEL")
                .setContentIntent(intent)
                .setSmallIcon(R.drawable.notify_logo)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setAutoCancel(true);


        if (title != null) {
            mBuilder.setContentTitle(title);
        } else {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                mBuilder.setContentTitle(Constants.APP_NAME);
            }
        }
        if (message != null) {
            mBuilder.setContentText(message);
        }

        mBuilder.setColor(Color.parseColor("#000000"));
        mBuilder.setChannelId("ILEAP_NOTIFY_CHANNEL");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(new Random().nextInt(99999), mBuilder.build());
        } else {
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
            notificationManager.notify(new Random().nextInt(99999), mBuilder.build());
        }

    }


    public static String makeDateToAgo(String s) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //simDf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = simDf.parse(s);
            Timestamp timestamp = new Timestamp(yourDate.getTime()); //5 * 60 * 60 * 1000 + 30 * 60 * 1000
            return getTimeAgo(timestamp.getTime());

        } catch (Exception e) {
            e.printStackTrace();
            return "long ago";
        }
    }

    public static String makeJSDateReadableOther(String s) {
        try {
            String ret = "";
            String[] arr = s.split("-");
            ret = Integer.parseInt(arr[1]) + "/" + arr[0] + ret;
            ret = arr[2].split("T")[0] + "/" + ret;
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getNormalToUTCDate(String value) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = dateFormat.parse(value);
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            return simDf.format(yourDate);
        } catch (Exception e) {
            e.printStackTrace();
            return value;
        }
    }

    public static String getUTCDateToNormal(String dateString) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            simDf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = simDf.parse(dateString);
            SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");
            return output.format(yourDate);

        } catch (Exception e) {
            e.printStackTrace();
            return makeJSDateReadableOther(dateString);
        }
    }

    public static String getUTCDateToDay(String dateString) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            simDf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = simDf.parse(dateString);
            SimpleDateFormat output = new SimpleDateFormat("dd");
            return output.format(yourDate);

        } catch (Exception e) {
            e.printStackTrace();
            return makeJSDateReadableOther(dateString);
        }
    }

    public static String getUTCDateToMonth(String dateString) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            simDf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = simDf.parse(dateString);
            SimpleDateFormat output = new SimpleDateFormat("MM");
            return output.format(yourDate);

        } catch (Exception e) {
            e.printStackTrace();
            return makeJSDateReadableOther(dateString);
        }
    }

    public static String getUTCDateToYear(String dateString) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            simDf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = simDf.parse(dateString);
            SimpleDateFormat output = new SimpleDateFormat("yyyy");
            return output.format(yourDate);

        } catch (Exception e) {
            e.printStackTrace();
            return makeJSDateReadableOther(dateString);
        }
    }

    public static String getUTCDate(String s) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            simDf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date yourDate = simDf.parse(s);
            Timestamp timestamp = new Timestamp(yourDate.getTime());
            return getTimeAgo(timestamp.getTime());

        } catch (Exception e) {
            e.printStackTrace();
            return "long ago";
        }
    }

    public static String getUpdatePageDate(String dateString, UpdateModel updateModel) {
        try {
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date yourDate = simDf.parse(dateString);
            String dayOfTheWeek = (String) DateFormat.format("EEEE", yourDate); // Thursday
            SimpleDateFormat output = new SimpleDateFormat("dd MMM");

            return updateModel.getDay() + " — " + output.format(yourDate);

        } catch (Exception e) {
            e.printStackTrace();
            return makeJSDateReadableOther(dateString);
        }

    }

    public static String getTimeInAMPM(String time) {
        //Displaying given time in 12 hour format with AM/PM
        //old format
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date3 = sdf.parse(time);
            //new format
            SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm aa");
            return sdf2.format(date3);
        } catch (ParseException e) {
            e.printStackTrace();
            return makeJSDateReadableOther(time);
        }

    }

    public static String getProperDayofMonth(String dayofMonth) {
        String day = null;

        if (dayofMonth.equals("1"))
            day = "01";
        else if (dayofMonth.equals("2"))
            day = "02";
        else if (dayofMonth.equals("3"))
            day = "03";
        else if (dayofMonth.equals("4"))
            day = "04";
        else if (dayofMonth.equals("5"))
            day = "05";
        else if (dayofMonth.equals("6"))
            day = "06";
        else if (dayofMonth.equals("7"))
            day = "07";
        else if (dayofMonth.equals("8"))
            day = "08";
        else if (dayofMonth.equals("9"))
            day = "09";

        return day;

    }

    public static String getCurrentUTCDate() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Date time = calendar.getTime();
//        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return outputFmt.format(time);
    }

    public static String getMixPanleUTC() {

        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        return dateFormat.format(new Date());
    }

    public static String getIndianTimeStamp() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        calendar.setTime(new Date());
        Date time = calendar.getTime();

        return "" + time.getTime();
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            time *= 1000;
        }
        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return "Just now";
        }

        // TODO: localize
        final long diff = now - time;
        if (diff <= Constants.MINUTE_MILLIS) {
            return "Just now";
        } else if (diff < 2 * Constants.MINUTE_MILLIS) {
            return "1 minute ago";
        } else if (diff < 50 * Constants.MINUTE_MILLIS) {
            return diff / Constants.MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * Constants.MINUTE_MILLIS) {
            return "1 hour ago";
        } else if (diff < 24 * Constants.HOUR_MILLIS) {
            return diff / Constants.HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * Constants.HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / Constants.DAY_MILLIS + " days ago";
        }
    }

    public static long getTimeInMillis(String date, String time) {
        try {
            date = date.substring(0, 10) + " " + time;
            SimpleDateFormat simDf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date yourDate = simDf.parse(date);

            return yourDate.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getDuration(String startTime, String endTime) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(startTime);
            date2 = format.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return "HH:MM";
        }
        long diff = date2.getTime() - date1.getTime();

        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        return diffHours == 0 ? diffMinutes + "Min" : diffHours + " Hrs, " + diffMinutes + " Min";

    }


    public static String getCurrentDate() {

        String date = new SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault()).format(new Date());
        Logger.d("CURRENT_DATE", date);

        return date;
    }

    public static String getCurrentDateFormat() {

        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        Logger.d("CURRENT_DATE", date);

        return date;
    }

    public static String getCurrentTime() {
        String time = new SimpleDateFormat("h:mm a", Locale.getDefault()).format(new Date());
        Logger.d("CURRENT_TIME", time);

        return time;
    }


    public static String encrypt(String key, String iv, String data) {
        try {
            if (key.length() < Constants.CIPHER_KEY_LEN) {
                int numPad = Constants.CIPHER_KEY_LEN - key.length();
                for (int i = 0; i < numPad; i++) {
                    key += "0"; //0 pad to len 16 bytes
                }
            } else if (key.length() > Constants.CIPHER_KEY_LEN) {
                key = key.substring(0, Constants.CIPHER_KEY_LEN); //truncate to 16 bytes
            }
            IvParameterSpec initVector = new IvParameterSpec(iv.getBytes("ISO-8859-1"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("ISO-8859-1"), "AES");
            Cipher cipher = Cipher.getInstance(Constants.CIPHER_NAME);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);
            byte[] encryptedData = cipher.doFinal((data.getBytes()));
            String base64_EncryptedData = new String(android.util.Base64.encodeToString(encryptedData, android.util.Base64.DEFAULT));
            String base64_IV = new String(android.util.Base64.encodeToString(iv.getBytes("ISO-8859-1"), android.util.Base64.DEFAULT));
            return base64_EncryptedData + ":" + base64_IV;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static void showToast(Context c, String s, boolean duration) {
        if (c == null) return;
//        Toast tst = Toast.makeText(c, s, duration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
//        TextView v = tst.getView().findViewById(android.R.id.message);
//        tst.show();

        Toast toast = Toast.makeText(c, s, duration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.corner_radius_black_3);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTextColor(ContextCompat.getColor(c, R.color.white));
        text.setPadding(Utility.dpSize(Utility.getContext(), 7), 0, Utility.dpSize(Utility.getContext(), 7), 0);
        /*Here you can do anything with above textview like text.setTextColor(Color.parseColor("#000000"));*/
        toast.show();
    }

    public static void showToast(Context context, boolean duration, String messsage) {
        if (context == null) return;
//        Toast tst = Toast.makeText(c, t, duration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
//        tst.setText(s);
//        tst.show();

        Toast toast = Toast.makeText(context, messsage, duration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.corner_radius_black_3);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTextColor(ContextCompat.getColor(context, R.color.white));
        /*Here you can do anything with above textview like text.setTextColor(Color.parseColor("#000000"));*/
        toast.show();
    }

    public static boolean unAuthorize(ResponseBody responseBody) {
        try {
            JsonObject jsonObject = (JsonObject) new JsonParser().parse(responseBody.string());
            return jsonObject.get("unAuthorize").getAsBoolean();
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }


    public static ColorStateList generateColors(boolean isDark) {

        int[] colors = new int[0];

        int[][] states = new int[][]{
                new int[]{android.R.attr.state_checked}, // disabled
                new int[]{-android.R.attr.state_checked}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed
        };

        if (isDark) {
            colors = new int[]{
                    Color.parseColor("#f2f2f2"),
                    Color.parseColor("#827c7c"),
                    Color.parseColor("#827c7c"),
            };
        } else {
            colors = new int[]{
                    Color.parseColor("#000000"),
                    Color.parseColor("#CDA0A0A0"),
                    Color.parseColor("#000000"),
            };
        }

        return new ColorStateList(states, colors);

    }


    public static String showErrorMessage(ResponseBody responseBody) {
        try {
            if (responseBody == null)
                return Constants.SOMETHING_WENT_WRONG;
            JsonObject jsonObject = (JsonObject) new JsonParser().parse(responseBody.string());
            return jsonObject.get("message").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
            return Constants.SOMETHING_WENT_WRONG;
        }
    }

    public static File getCompressedFileFromFile(Uri uri, MediaType type, double size) {

        File file = new File(getPath(getContext(), uri));
        if (file.exists()) {
            if (type == MediaType.IMAGE) {
                try {
                    Logger.d("Previous size", "" + file.length());
                    size = size / 1024; // convert to KB'S from Bytes
                    file = new Compressor(getContext()).setQuality(getCompressionQuality(size)).compressToFile(file);
                    Logger.d("comp image size", "" + file.length());
                    Logger.d("comp image quality", "" + getCompressionQuality(size));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return file;
        }
        return null;
    }

    /*
     *   Custome tabLayout with custom textview with image and without Image
     */
    public static void updateTabLayout(TabLayout tabLayout, Activity activity, int color, Typeface typeface, int type) {
        try {
            if (typeface == null) {
                typeface = getTypeface(2, activity);
            }
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TextView tv = null;
                if (type == 1) {
                    tv = (TextView) LayoutInflater.from(activity).inflate(R.layout.custom_tablayout_textview1, null);
                } else if (type == 4) {
                    tv = (TextView) LayoutInflater.from(activity).inflate(R.layout.custom_tablayout_textview, null).findViewById(R.id.tvTitle);
                }
                if (type == 2) {
                    tv = (TextView) LayoutInflater.from(activity).inflate(R.layout.custom_tablayout_textview1, null);
                    tv.setScaleY(-1);
                }
                tv.setTypeface(typeface);
                tv.setTextColor(ContextCompat.getColor(getContext(), color));
                tabLayout.getTabAt(i).setCustomView(tv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changeTabsFont(TabLayout tabLayout) {
        Logger.d("In change tab font");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        Logger.d("Tab count:-" + tabsCount);
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof AppCompatTextView) {
                    Logger.d("In font");
                    Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto/Roboto-Medium.ttf");
                    TextView viewChild = (TextView) tabViewChild;
                    viewChild.setTypeface(type);
                    viewChild.setAllCaps(false);
                }
            }
        }
    }


    public static String getDateFomat(String timeCreated, int type) {

        Date oneWayTripDate = null;
        SimpleDateFormat input = null;
        SimpleDateFormat output = null;
        if (type == 3) {
            input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            output = new SimpleDateFormat("dd-MMMM-yyyy");
        } else if (type == 0) {
            input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            output = new SimpleDateFormat("dd");
        } else if (type == 2) {
            input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            output = new SimpleDateFormat("MMMM");
        } else if (type == 1) {
            input = new SimpleDateFormat("yyyy-MM-dd");
            output = new SimpleDateFormat("dd MMMM");
        } else if (type == 4) {
            input = new SimpleDateFormat("yyyy-MM-dd");
            output = new SimpleDateFormat("dd.MM.yyyy");
        } else if (type == 5) {
            input = new SimpleDateFormat("yyyy-MM-dd");
            output = new SimpleDateFormat("MMMM dd, yyyy");
        } else if (type == 6) {
            input = new SimpleDateFormat("yyyy-MM-dd");
            output = new SimpleDateFormat("dd MMMM, yyyy");
        }
        try {
            oneWayTripDate = input.parse(timeCreated);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        Logger.d("===============", "======currentData======" + output.format(oneWayTripDate));
        return output.format(oneWayTripDate).toString();
    }

    public static int getCountOfDays(String createdDateString, String expireDateString) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        long diff = 0;
        int days = 0;
        try {
            Date date1 = myFormat.parse(createdDateString);
            Date date2 = myFormat.parse(expireDateString);
            diff = date2.getTime() - date1.getTime();
            System.out.println("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        return (days + 1);
    }

    public static String formatDate(String s) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = null;
        try {
            Date date = simpleDateFormat.parse(s);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", new Locale("en", "UK"));
            sdf.format(calendar.getTime());
            dateStr = sdf.format(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateStr;
    }

    //textSize=R.dimen.text_size_20
    public static int setTextSize(int textSize) {
        return (int) (getContext().getResources().getDimension(textSize) / getContext().getResources().getDisplayMetrics().density);
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targtetHeight = v.getMeasuredHeight();
        if (v.isShown()) {
            collapse(v);
        } else {
            v.getLayoutParams().height = 0;
            v.setVisibility(View.VISIBLE);
            Animation a = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime,
                                                   Transformation t) {
                    v.getLayoutParams().height = interpolatedTime == 1 ? ViewGroup.LayoutParams.WRAP_CONTENT
                            : (int) (targtetHeight * interpolatedTime);
                    v.requestLayout();
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };
            a.setDuration((int) (150));
            v.startAnimation(a);
        }

    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime,
                                               Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight
                            - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (v.getLayoutParams().height + 150));
        v.startAnimation(a);
    }

    public static String splitToComponentTimes(BigDecimal biggy) {
        String time = "";
        long longVal = biggy.longValue();
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        if (hours > 0) {
            time = hours + " Hours, " + mins + " Minutes";
        } else if (mins > 0) {
            time = mins + " Minutes";
        } else {
            time = secs + " Seconds";
        }


        return time;
    }

    public static String calculateTimeDuration(Long seconds) {
        if (seconds == null || seconds == 0) {
            return "00:00";
        }

        long milliSeconds = seconds * 1000;
        if (milliSeconds >= 60 * 60 * 1000) {
            return String.format(TimeUnit.MILLISECONDS.toHours(milliSeconds) > 99 ? "%d:%02d:%02d" : "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(milliSeconds),
                    TimeUnit.MILLISECONDS.toMinutes(milliSeconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(milliSeconds) % TimeUnit.MINUTES.toSeconds(1));
        }

        return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(milliSeconds) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) % TimeUnit.MINUTES.toSeconds(1));
    }


    public static double roundTwoDecimals(double d) {
        try {
            DecimalFormat twoDForm = new DecimalFormat("#.#");
            return Double.valueOf(twoDForm.format(d));
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0;
        }
    }


    public static int getBitrate(int progressiveValue) {

        //for 16:9 screen
        int returnValue = progressiveValue / 9 * 16;

        // will multiply the screen value into progressiveValueL̥
        returnValue = returnValue * progressiveValue;

        return returnValue;
    }

    @ColorInt
    public static int getColorsFromAttrs(Context context, @AttrRes int attrFields) {
        Resources.Theme theme = context.getTheme();

        TypedValue typedValue = new TypedValue();

        theme.resolveAttribute(attrFields, typedValue, true);

        @ColorInt int colorValues = typedValue.data;

        return colorValues;
    }


    public static String getFileType(String file) {
        String[] fileType;
        fileType = file.split("\\.");

        if (file.length() > 0)
            return fileType[fileType.length - 1];
        else
            return null;
    }

    public static void requestDialog(Activity activity, ICustomAlertDialog iCustomAlertDialog, String title, String description, int type) {

        try {
            CustomAlertDialog dialog = new CustomAlertDialog(activity, iCustomAlertDialog, title, description, type);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.show();
            DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
            int width = (int) (metrics.widthPixels * 0.85);
            dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestDialogAlert(Activity activity, ICustomAlertDialog iCustomAlertDialog,
                                          String title, String description, int type,
                                          String actionName, boolean isHard,
                                          boolean isCancel) {


        try {
            CustomUpdateAlertDialog dialog = new CustomUpdateAlertDialog(activity, iCustomAlertDialog, title,
                    description, type, actionName, isHard);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(!isCancel);
            dialog.show();
            DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
            int width = (int) (metrics.widthPixels * 0.85);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long checkAvailableSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getBlockCount();
        long megAvailable = bytesAvailable / 1048576;
        System.out.println("Megs :" + megAvailable);

        return bytesAvailable;

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static void changeTheme(Activity activity) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
        activity.finish();
        //fade in and face Out code changed please use the another anim for this if theme concept came.
        activity.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public static boolean sensorCheck(UserRepositoryManager userRepositoryManager, Context context) {
        SensorManager sensorManager = null;
        Sensor light = null;
        final float[] sensorValue = {-1};
        final boolean[] isSensor = {false};
        final boolean[] isDark = {false};

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        sensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

                if (userRepositoryManager.appSharedPreferences.getBoolean(Constants.AUTO_DARK_MODE)) {
                    if (!isSensor[0]) {
                        isSensor[0] = true;
                        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                            if (event.values[0] < 3 || event.values[0] > 15) {
                                sensorValue[0] = event.values[0];
                            }
                            if (sensorValue[0] < 3) {
                                //Darktheme
                                userRepositoryManager.appSharedPreferences.setBoolean(Constants.IS_DARK_MODE, true);
                                isDark[0] = true;


                            } else {
                                //LightTheme
                                userRepositoryManager.appSharedPreferences.setBoolean(Constants.IS_DARK_MODE, false);
                                isDark[0] = true;
                            }
                        }
                    }
                }

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        }, light, SensorManager.SENSOR_DELAY_NORMAL);

        return isDark[0];
    }


    public static List<FeaturesModel> getFeaturesList() {

        List<FeaturesModel> featuresModelList = new ArrayList<>();

        FeaturesModel featuresModel1 = new FeaturesModel();

        featuresModel1.setFeatureName("Free Online Test Series");
        featuresModel1.setFeatureImage(R.drawable.online_test_icon);

        FeaturesModel featuresMode2 = new FeaturesModel();

        featuresMode2.setFeatureName("Doubt Clearing Support");
        featuresMode2.setFeatureImage(R.drawable.support_icon);

        FeaturesModel featuresMode3 = new FeaturesModel();

        featuresMode3.setFeatureName("Total No.Of Video Hours");
        featuresMode3.setFeatureImage(R.drawable.hours_icon);

        FeaturesModel featuresMode4 = new FeaturesModel();

        featuresMode4.setFeatureName("Additional Digital Resources");
        featuresMode4.setFeatureImage(R.drawable.ic_digital_resources);

        FeaturesModel featuresMode5 = new FeaturesModel();

        featuresMode5.setFeatureName("Continuously Updated Courses");
        featuresMode5.setFeatureImage(R.drawable.ic_update_icon);

        FeaturesModel featuresMode6 = new FeaturesModel();

        featuresMode6.setFeatureName("Classroom Study Material");
        featuresMode6.setFeatureImage(R.drawable.ic_study_material);

        FeaturesModel featuresMode7 = new FeaturesModel();

        featuresMode7.setFeatureName("Note Taking During Video Classes");
        featuresMode7.setFeatureImage(R.drawable.note_taking_icon);

        FeaturesModel featuresMode8 = new FeaturesModel();

        featuresMode8.setFeatureName("EMI Option Available");
        featuresMode8.setFeatureImage(R.drawable.emi_icon);

        FeaturesModel featuresMode9 = new FeaturesModel();

        featuresMode9.setFeatureName("IN-App Offline Downloads");
        featuresMode9.setFeatureImage(R.drawable.in_app_download_icon);


        featuresModelList.add(featuresModel1);
        featuresModelList.add(featuresMode2);
        featuresModelList.add(featuresMode3);
        featuresModelList.add(featuresMode4);
        featuresModelList.add(featuresMode5);
        featuresModelList.add(featuresMode6);
        featuresModelList.add(featuresMode7);
        featuresModelList.add(featuresMode8);
        featuresModelList.add(featuresMode9);

        return featuresModelList;
    }

    public static List<InstructorsModel> getInstructorsList() {

        List<InstructorsModel> featuresModelList = new ArrayList<>();

        InstructorsModel featuresModel = new InstructorsModel();

        featuresModel.setInstructorName("Marc Adrian Alexander");
        featuresModel.setAbout("Private online Principles of Physics tutor");
        featuresModel.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081933/Leap/B86EB814-32BE-407B-A1CF-7D7C2E2F1403.png");

        InstructorsModel featuresMode2 = new InstructorsModel();

        featuresMode2.setInstructorName("Marc Coppolo Alexander");
        featuresMode2.setAbout("Private online Principles of Physics tutor for Science and Physices");
        featuresMode2.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081938/Leap/203A9A1D-6041-4914-BF4D-A505EC2DB782.png");

        InstructorsModel featuresMode3 = new InstructorsModel();

        featuresMode3.setInstructorName("Marc Salina Alexander");
        featuresMode3.setAbout("Private online Principles of Physics tutor");
        featuresMode3.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081942/Leap/68D44C97-68C1-4B81-B432-D5567D8E0AA8.png");

        InstructorsModel featuresMode4 = new InstructorsModel();

        featuresMode4.setInstructorName("Marc John Alexander");
        featuresMode4.setAbout("Private online Principles of Physics tutor");
        featuresMode4.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081933/Leap/B86EB814-32BE-407B-A1CF-7D7C2E2F1403.png");

        InstructorsModel featuresMode5 = new InstructorsModel();

        featuresMode5.setInstructorName("Marc Shri Alexander");
        featuresMode5.setAbout("Private online Principles of Physics tutor");
        featuresMode5.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081938/Leap/203A9A1D-6041-4914-BF4D-A505EC2DB782.png");

        InstructorsModel featuresMode6 = new InstructorsModel();

        featuresMode6.setInstructorName("Marc Robert Alexander");
        featuresMode6.setAbout("Private online Principles of Physics tutor");
        featuresMode6.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081942/Leap/68D44C97-68C1-4B81-B432-D5567D8E0AA8.png");

        InstructorsModel featuresMode7 = new InstructorsModel();

        featuresMode7.setInstructorName("Marc Jane Alexander");
        featuresMode7.setAbout("Private online Principles of Physics tutor");
        featuresMode7.setInstructorImage("https://res.cloudinary.com/chinna972/image/upload/v1570081933/Leap/B86EB814-32BE-407B-A1CF-7D7C2E2F1403.png");


        featuresModelList.add(featuresModel);
        featuresModelList.add(featuresMode2);
        featuresModelList.add(featuresMode3);
        featuresModelList.add(featuresMode4);
        featuresModelList.add(featuresMode5);
        featuresModelList.add(featuresMode6);
        featuresModelList.add(featuresMode7);

        return featuresModelList;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int toSdp(int sdp) {
        return getContext().getResources().getDimensionPixelSize(sdp);
    }

    public static boolean isTablet() {
        boolean xlarge = ((getContext().getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((getContext().getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }


    public static Uri getUriFromInvoice(Invoice invoice) {
        if (invoice == null || invoice.getOrderId() == null)
            return null;

        return Uri.parse(Constants.UserConstants.INVOICE_PREVIEW_URL + invoice.getOrderId());
    }


    public static String getAppSignature() {
        ArrayList<String> appSignaturesHashs = new ArrayList<>();

        try {
            // Get all package details
            String packageName = getContext().getPackageName();
            PackageManager packageManager = getContext().getPackageManager();
            Signature[] signatures = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;

            for (Signature signature : signatures) {
                String hash = null;
                hash = Utility.hash(packageName, signature.toCharsString());

                if (hash != null) {
                    appSignaturesHashs.add(String.format("%s", hash));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.d("getAppSignatures", "Package not found");
        }
        return appSignaturesHashs != null && appSignaturesHashs.size() > 0 ? appSignaturesHashs.get(0) : null;
    }

    public static String hash(String packageName, String signature) {
        String HASH_TYPE = "SHA-256";
        int NUM_HASHED_BYTES = 9;
        int NUM_BASE64_CHAR = 11;
        String appInfo = packageName + " " + signature;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(HASH_TYPE);
            messageDigest.update(appInfo.getBytes(StandardCharsets.UTF_8));
            byte[] hashSignature = messageDigest.digest();
            // truncated into NUM_HASHED_BYTES
            hashSignature = Arrays.copyOfRange(hashSignature, 0, NUM_HASHED_BYTES);
            // encode into Base64
            String base64Hash = Base64.encodeToString(hashSignature, Base64.NO_PADDING | Base64.NO_WRAP);
            base64Hash = base64Hash.substring(0, NUM_BASE64_CHAR);

            return base64Hash;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Logger.d("hash", "No Such Algorithm Exception");
        }
        return null;
    }


    public static List<BenefitModel> getBenefitsList() {

        List<BenefitModel> benefitModelArrayList = new ArrayList<>();

        BenefitModel benefitModel = new BenefitModel();
        benefitModel.setImage(R.drawable.ic_school);
        benefitModel.setName("Those Considering Graduate School");
        benefitModel.setDescription("Get ready for your MTech/M.Sc/Ph.D. or other graduate programs’ entrance tests such as GATE with this online course from the top faculty at ACE Engineering Academy.");

        BenefitModel benefitModel1 = new BenefitModel();
        benefitModel1.setImage(R.drawable.ic_college_students);
        benefitModel1.setName("College Students and Recent Graduates");
        benefitModel1.setDescription("Get ready for university and college examinations and get ready for the real world. Stand out from the crowd and impress during interviews as you prepare for your next opportunity.");

        BenefitModel benefitModel2 = new BenefitModel();
        benefitModel2.setImage(R.drawable.ic_professional);
        benefitModel2.setName("Those Considering PSUs");
        benefitModel2.setDescription("Apply to top Public Service Undertakings such as BHEL, GAIL, IOCL, SAIL, BEL, Oil Indian Limited, Power Grid, Bharat Sanchar Nigam Limited, PEC Limited, THDC India Limited and many more through GATE.");

        benefitModelArrayList.add(benefitModel);
        benefitModelArrayList.add(benefitModel1);
        benefitModelArrayList.add(benefitModel2);

        return benefitModelArrayList;
    }

    public static String getCipherData(String data, String publicKeyData) {

        Logger.d("UTC DATE", data);
        String dataEncrypted = null;
        Cipher cipherRSA = null;
        try {
            X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.decode(publicKeyData, Base64.NO_WRAP | Base64.NO_PADDING));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(publicSpec);
            cipherRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipherRSA.init(Cipher.ENCRYPT_MODE, publicKey);
            dataEncrypted = Base64.encodeToString(cipherRSA.doFinal(data.getBytes()), Base64.NO_WRAP);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.d("Cipher Data", dataEncrypted);
        return dataEncrypted;
    }


    public static String decodeCipherKey(String encrptedData, String privateKeyData) {

        Logger.d("DECODE", encrptedData);
        String data = null;
        Cipher cipherRSA = null;
        try {

            PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.decode(privateKeyData, Base64.NO_WRAP | Base64.NO_PADDING));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = keyFactory.generatePrivate(privateSpec);

            cipherRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipherRSA.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] secretKeyBytes = cipherRSA.doFinal(Base64.decode(encrptedData, Base64.NO_WRAP | Base64.NO_PADDING));

            data = new String(secretKeyBytes, Charset.forName("UTF-8"));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public static int getActionBarHeight(Context context) {
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        TypedArray a = context.obtainStyledAttributes(new TypedValue().data, textSizeAttr);
        int height = a.getDimensionPixelSize(0, 0);
        a.recycle();
        return height;
    }


    //Remove Cache of the app
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else {
            return false;
        }
    }

    public static double getDeviceMemory(Context context, int storage) {
        try {

            StatFs stat = new StatFs(
                    Utility.checkPermissionRequest(Permission.WRITE_STORAGE, context) && Utility.isExternalStorageWritable(context) && storage == 2
                            ? ContextCompat.getExternalFilesDirs(context, null)[1].getAbsolutePath() :
                            Environment.getExternalStorageDirectory().getAbsolutePath());
            long bytesAvailable;


//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
//            } else {
//                bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
//            }
            long megAvailable = bytesAvailable / (1024 * 1024);
            double gbAvailable = (double) megAvailable / (double) 1000;
            Logger.d("Available MB : " + megAvailable);
            return gbAvailable;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static List<CatalogueModel> getCatalogueList() {

        List<CatalogueModel> featuresModelList = new ArrayList<>();

        CatalogueModel featuresModel1 = new CatalogueModel();

        featuresModel1.setCatalogueName("GATE - Civil Engineering");
        featuresModel1.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021370/Catalogues/ResizedIcons/Group_1817_2x.png");
        featuresModel1.setCatalogueImageColor("254.189.46.#81E4CD");

        CatalogueModel featuresMode2 = new CatalogueModel();

        featuresMode2.setCatalogueName("GATE - Electronics & Communication");
        featuresMode2.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/Group_4164_2x.png");
        featuresMode2.setCatalogueImageColor("254.189.46.#A2C9F5");

        CatalogueModel featuresMode3 = new CatalogueModel();

        featuresMode3.setCatalogueName("GATE - Electrical Engineering");
        featuresMode3.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021371/Catalogues/ResizedIcons/Group_4159_Copy.png");
        featuresMode3.setCatalogueImageColor("254.189.46.#FE9F5F");

        CatalogueModel featuresMode4 = new CatalogueModel();

        featuresMode4.setCatalogueName("GATE - Mechanical Engineering");
        featuresMode4.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021369/Catalogues/ResizedIcons/Group_550_2x.png");
        featuresMode4.setCatalogueImageColor("254.189.46.#FEBD2E");


        featuresModelList.add(featuresModel1);
        featuresModelList.add(featuresMode2);
        featuresModelList.add(featuresMode3);
        featuresModelList.add(featuresMode4);

        return featuresModelList;
    }

    public static List<CatalogueModel> getCatalogueList1() {

        List<CatalogueModel> featuresModelList = new ArrayList<>();


        CatalogueModel featuresMode5 = new CatalogueModel();

        featuresMode5.setCatalogueName("ESE - Civil Engineering");
        featuresMode5.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/illustration_2x.png");
        featuresMode5.setCatalogueImageColor("254.189.46.#FE7F6D");

        CatalogueModel featuresMode6 = new CatalogueModel();

        featuresMode6.setCatalogueName("ESE - Electronics & Communication");
        featuresMode6.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/sdfdsaf.png");
        featuresMode6.setCatalogueImageColor("254.189.46.#A9CF67");

        CatalogueModel featuresMode7 = new CatalogueModel();

        featuresMode7.setCatalogueName("ESE - Electrical Engineering");
        featuresMode7.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/illustration11_2x.png");
        featuresMode7.setCatalogueImageColor("254.189.46.#E1A6C0");

        CatalogueModel featuresMode8 = new CatalogueModel();

        featuresMode8.setCatalogueName("ESE - Mechanical Engineering");
        featuresMode8.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/Group_4162_2x.png");
        featuresMode8.setCatalogueImageColor("254.189.46.#B4A0DE");


        featuresModelList.add(featuresMode5);
        featuresModelList.add(featuresMode6);
        featuresModelList.add(featuresMode7);
        featuresModelList.add(featuresMode8);

        return featuresModelList;
    }

    public static List<CatalogueModel> getCatalogueList3() {

        List<CatalogueModel> featuresModelList = new ArrayList<>();

        CatalogueModel featuresModel1 = new CatalogueModel();

        featuresModel1.setCatalogueName("GATE - Civil Engineering");
        featuresModel1.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021370/Catalogues/ResizedIcons/Group_1817_2x.png");
        featuresModel1.setCatalogueImageColor("254.189.46.#81E4CD");

        CatalogueModel featuresMode2 = new CatalogueModel();

        featuresMode2.setCatalogueName("GATE - Electronics & Communication");
        featuresMode2.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/Group_4164_2x.png");
        featuresMode2.setCatalogueImageColor("254.189.46.#A2C9F5");

        CatalogueModel featuresMode3 = new CatalogueModel();

        featuresMode3.setCatalogueName("GATE - Electrical Engineering");
        featuresMode3.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021371/Catalogues/ResizedIcons/Group_4159_Copy.png");
        featuresMode3.setCatalogueImageColor("254.189.46.#FE9F5F");

        CatalogueModel featuresMode4 = new CatalogueModel();

        featuresMode4.setCatalogueName("GATE - Mechanical Engineering");
        featuresMode4.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021369/Catalogues/ResizedIcons/Group_550_2x.png");
        featuresMode4.setCatalogueImageColor("254.189.46.#FEBD2E");

        CatalogueModel featuresMode5 = new CatalogueModel();

        featuresMode5.setCatalogueName("ESE - Civil Engineering");
        featuresMode5.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/illustration_2x.png");
        featuresMode5.setCatalogueImageColor("254.189.46.#FE7F6D");

        CatalogueModel featuresMode6 = new CatalogueModel();

        featuresMode6.setCatalogueName("ESE - Electronics & Communication");
        featuresMode6.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/sdfdsaf.png");
        featuresMode6.setCatalogueImageColor("254.189.46.#A9CF67");

        CatalogueModel featuresMode7 = new CatalogueModel();

        featuresMode7.setCatalogueName("ESE - Electrical Engineering");
        featuresMode7.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/illustration11_2x.png");
        featuresMode7.setCatalogueImageColor("254.189.46.#E1A6C0");

        CatalogueModel featuresMode8 = new CatalogueModel();

        featuresMode8.setCatalogueName("ESE - Mechanical Engineering");
        featuresMode8.setCatalogueImage("https://res.cloudinary.com/chinna972/image/upload/v1584021368/Catalogues/ResizedIcons/Group_4162_2x.png");
        featuresMode8.setCatalogueImageColor("254.189.46.#B4A0DE");


        featuresModelList.add(featuresModel1);
        featuresModelList.add(featuresMode2);
        featuresModelList.add(featuresMode3);
        featuresModelList.add(featuresMode4);
        featuresModelList.add(featuresMode5);
        featuresModelList.add(featuresMode6);
        featuresModelList.add(featuresMode7);
        featuresModelList.add(featuresMode8);

        return featuresModelList;
    }


    public static JSONArray getUserAppsInMobile() {

        JSONArray jsonArray = new JSONArray();
        int flags = PackageManager.GET_META_DATA |
                PackageManager.GET_SHARED_LIBRARY_FILES |
                PackageManager.GET_UNINSTALLED_PACKAGES;

        PackageManager pm = getContext().getPackageManager();
        List<ApplicationInfo> applications = pm.getInstalledApplications(flags);

        for (ApplicationInfo appInfo : applications) {
            try {
                if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
                    continue;
                } else {// Installed by user`
                    Logger.d("USER", pm.getApplicationLabel(appInfo) + " " + appInfo.packageName);
                }
                jsonArray.put(pm.getApplicationLabel(appInfo));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return jsonArray;
    }

    public static List<MenuItem> generateSupportItems() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(1, "Subscription Related", "Find out more about subscriptions", "GENERAL QUERIES"));
        list.add(new MenuItem(2, "Online Test Series", "Find more information about OTS", null));
        list.add(new MenuItem(3, "Study Material", "Find more information about Study Material", null));
        list.add(new MenuItem(4, "Device & Region Compatibility", "Find more information related to compatibility", null));

        list.add(new MenuItem(5, "General Issues", "Find more information related to content", "CONTENT RELATED QUERIES"));
        list.add(new MenuItem(6, "Reporting Content Issues", "Find out how you can report content", null));
        list.add(new MenuItem(7, "New Content Requests", "Want us to add more content? Let us know", null));

        list.add(new MenuItem(8, "General Queries", "Find information related to taxes and more", "PAYMENT RELATED QUERIES"));
        list.add(new MenuItem(9, "Payment Issues", "Got a problem with your payment? Let us know", null));

        list.add(new MenuItem(10, "General Queries", "Find solutions for general issues", "TECHNICAL ISSUES"));
        list.add(new MenuItem(11, "Video Player and Streaming Related", "Find solutions for streaming issues", null));

        list.add(new MenuItem(12, "Feature Requests", "Got a feature in your mind? Let us know", "MISCELLANEOUS REQUESTS"));
        list.add(new MenuItem(13, "New Course Request", "Need a new course? Let us know", null));

        return list;
    }

    public static List<MenuItem> getSubscriptionRelatedItems() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(1, "What is the validity of my Subscription?",
                "The subscription is valid from the date of purchase. This is if you choose the 1-year subscription, it would be valid for 1 year from the date of purchase. Similarly, if you choose the 2-year subscription, it would be valid for 2 years from the date of purchase. "
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(2, "Can I convert my GATE Subscription to ESE Subscription?"
                , "Yes, you can convert your subscription from GATE to ESE. Please contact us to proceed further. \n\n " +
                "1. If you have purchased GATE study material and have received it, you will have to pay an additional fee if you want ESE study material. \n\n" +
                "2. If you have not received the study material, you will have to pay the difference amount for ESE study material. \n\n" +
                "3. If you have not received the GATE study material and you do not want the ESE study material, we will adjust the amount for the ESE subscription purchase."
                , true, null, "I would like to convert my GATE Subscription to ESE Subscription"
                , "General Query", "Subscription Related"));
        list.add(new MenuItem(3, "Can I convert my ESE Subscription to GATE Subscription?"
                , "No, you cannot convert your ESE Subscription to a GATE Subscription."
                , false, null, "", null, null));
        list.add(new MenuItem(4, "I bought my course by mistake, can I change the subscription?"
                , "Yes, you can change your subscription from one branch to another if you raise a request within 36 hours of purchasing the course. To change your branch, please contact us."
                , true, null, "I have purchased a different branch subscription by mistake and would like to change my branch."
                , "General Query", "Subscription Related"));
        list.add(new MenuItem(5, "Where can I get more details about the Study Material?"
                , "Please go to the Study Material topic under General Enquiries to learn more about the study material we provide."
                , false, null, "", null, null));
        list.add(new MenuItem(6, "Where can I get more details about the Online Test Series?"
                , "Please go to the Online Test Series topic under General Enquiries to learn more about the free online test series."
                , false, null, "", null, null));

        list.add(new MenuItem(7, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(8, "Email Support", "I want to get in touch through email", false, null, ""
                , "General Query", "Subscription Related"));


        return list;
    }

    public static List<MenuItem> getOnlineTestSeries() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(9, "How can I claim my free ACE Online Test Series?",
                "You can claim free Online Test Series under the “OTS Tab” which is present in the course dashboard which can be accessed by clicking on the course in the main dashboard."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(10, "Can I get ESE Online Test Series by purchasing a GATE course?",
                "You are eligible to claim the ESE Online Test Series only if you buy one of our ESE Courses. If you would like to access the ESE Online Test Series, you will have to purchase the ESE Online Test Series separately by clicking here."
                , false
                , null, "", null, null));
        list.add(new MenuItem(11, "Can I get the GATE Online Test Series by purchasing an ESE course?",
                "You are eligible to claim the GATE Online Test Series only if you buy one of our GATE Courses. If you would like to access the GATE Online Test Series, you will have to purchase the GATE Online Test Series separately by clicking here."
                , false
                , null, "", null, null));
        list.add(new MenuItem(12, "How can I start practicing tests in Online Test Series?",
                "You can start practicing in Online Test Series by clicking here. You can also download the ACE OTS android application by clicking here."
                , false
                , null, "", null, null));
        list.add(new MenuItem(13, "When will the Online Test Series schedule start?",
                "Unfortunately, our team does not manage ACE Online Test Series. However, you can contact the ACE Engineering Academy’s support team by clicking here."
                , false
                , null, "", null, null));

        list.add(new MenuItem(50, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(51, "Email Support", "I want to get in touch through email", false, null, ""
                , "General Query", "Online Test Series"));

        return list;
    }

    public static List<MenuItem> getStudyMaterial() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(14, "What is the format of the study material? Digital or Hard Copy?",
                "We only provide study material in physical form. When you purchase your subscription, you can claim the study material by going to the “OTS Tab”."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(15, "What Study Material will I get with my Subscription?",
                "you can find the study material list in the following attachments."
                , false
                , null, "", null, null));
        list.add(new MenuItem(16, "Where is my Study Material?",
                "You will get an email from us with the tracking link when your material is dispatched. If it has been more than 2 weeks since you have purchased the course and you have not received an email from us, please contact us. \n\n" +
                        "NOTE: The study material will only provided to those who have purchased the course along with the material. If you haven't purchased the material and want to purchase it now, please reach out to our support team."
                , true
                , null, "I have not received any email about material dispatch even 2 weeks after purchasing the course"
                , "General Query", "Study Material"));

        list.add(new MenuItem(52, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(53, "Email Support", "I want to get in touch through email", false, null, ""
                , "General Query", "Study Material"));

        return list;
    }

    public static List<MenuItem> getDeviceandRegionCompatibility() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(17, "What Android Versions do you support?",
                "Our application is only available on android smartphones with android version 6 or higher."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(18, "Can I access my course on an  Android Tablet?",
                "Yes, you can access your course on an Android Tablet. Although our UI is not optimized for a tablet, you should not face problems accessing your courses. We are currently working on optimizing the UI for tablets."
                , false
                , null, "", null, null));
        list.add(new MenuItem(19, "Can I access my course on a smart TV?",
                "No, we do not have a smart TV application. You cannot cast the video to a smart TV as casting is not supported."
                , false
                , null, "", null, null));
        list.add(new MenuItem(20, "Can I access my course on a Laptop/Desktop?",
                "No, the web application has been deprecated due to some security issues. However, we are working on an alternative solution for Desktop which should be available soon. We will update you when it is ready."
                , false
                , null, "", null, null));
        list.add(new MenuItem(21, "Can I access my course on an iPhone/iPad?",
                "Our iOS Application is still under development. We will let you know when the application’s beta is launched. We would love to get feedback from you."
                , false
                , null, "", null, null));
        list.add(new MenuItem(22, "How can I access my course outside of India?",
                "You cannot access our course outside of India. Our app is available in the Indian PlayStore only and the videos are streamable only in India."
                , false
                , null, "", null, null));


        list.add(new MenuItem(54, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(55, "Email Support", "I want to get in touch through email", false, null, ""
                , "General Query", "Device and Region Compatibility"));

        return list;
    }

    public static List<MenuItem> getContentGeneralQueries() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(23, "I get a popup saying videos are not live, what do I do? Why are all the subjects not uploaded yet?",
                "You will see this popup only if we haven’t uploaded lecture videos to a subject yet.  Subjects are uploaded sequentially in the order they are taught in classroom coaching. We will send emails periodically to update you on what’s coming next. In the meantime, you can continue your preparation with other subjects."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(24, "When will the remaining content be uploaded?",
                "Subjects are uploaded sequentially in the order they are taught in classroom coaching. We will send emails periodically to update you on what’s coming next. Due to the rigorous content quality check process, our content is uploaded periodically."
                , false
                , null, "", null, null));
        list.add(new MenuItem(25, "Is there an upload schedule so that I can structure my preparation?",
                "Yes, we will periodically email you to update on what’s coming next. Please check your email from us."
                , false
                , null, "", null, null));


        list.add(new MenuItem(56, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(57, "Email Support", "I want to get in touch through email", false, null, ""
                , "Content Related Query", "General Content Issues"));

        return list;
    }

    public static List<MenuItem> getReportingContentIssues() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(26, "The audio is not clear in a video. What do I do?",
                "If you find that the audio in a particular video is not clear, please report the video. You can find the report option both in full screen and portrait mode. Click the report button and select the option “Voice isn’t clear”."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(27, "Video has editing issues such as blur. What do I do?",
                "If you find that the editing in a particular video is not good, please report the video. You can find the report option both in full screen and portrait mode. Click the report button and select the option “Editing Issues”."
                , false
                , null, "", null, null));
        list.add(new MenuItem(28, "When I click on a video, it says “Video not available”. What do I do?",
                "If you find a missing video, please report the video. You can find the report option both in full screen and portrait mode. Click the report button and select the option “Video is missing”."
                , false
                , null, "", null, null));
        list.add(new MenuItem(29, "The lecture wasn’t finished but the video ended. What do I do?",
                "If you find such a video, please report the video. You can find the report option both in full screen and portrait mode. Click the report button and select the option “Video ended abruptly”."
                , false
                , null, "", null, null));
        list.add(new MenuItem(30, "The lecture video has no correlation with the video before it. What do I do?",
                "If you believe that the lecture video has no correlation with the topic, please report the video. You can find the report option both in full screen and portrait mode. Click the report button and select the option “No correlation with the previous video”."
                , false
                , null, "", null, null));
        list.add(new MenuItem(31, "I don’t like the teaching methodology or of a particular instructor. What do I do?",
                "If you do not like the teaching methodology of a particular instructor, please contact us and describe the problem with as many details as possible so that we can accurately assess the issue."
                , true
                , null, "I have an issue with the teaching methodology of a particular instructor"
                , "Content Related Query", "Reporting Content Issues"));


        list.add(new MenuItem(58, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(59, "Email Support", "I want to get in touch through email", false, null, ""
                , "Content Related Query", "Reporting Content Issues"));

        return list;
    }

    public static List<MenuItem> getNewContentRequests() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(32, "I want you to add more content or topics. What do I do?",
                "If you want us to add more content, please contact us and describe your request with as many details as possible. If needed, we will contact you to understand your request better."
                , true
                , "FREQUENTLY ASKED QUESTIONS", "I have a request for more content in my course."
                , "Content Related Query", "New Content Requests"));


        list.add(new MenuItem(60, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(61, "Email Support", "I want to get in touch through email", false, null, ""
                , "Content Related Query", "New Content Requests"));

        return list;
    }

    public static List<MenuItem> getPaymentGeneralQueries() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(33, "What payment options do I have?",
                "We use Razorpay as our payment gateway provider. All the options supported by Razorpay are supported by us. Please click “complete order” and wait for the Razorpay popup to load to check all the options available."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(34, "Do I have to pay an additional GST for my purchase?",
                "No, GST is included in the price that has been mentioned. You do not need to pay any additional GST."
                , false
                , null, "", null, null));
        list.add(new MenuItem(35, "Do I have an option to pay in installments?",
                "No, we do not have an option to pay installments. However, you can choose to pay using our EMI option. We do not have any control over the EMI options. If you miss any payments, please contact your bank to take it further."
                , false
                , null, "", null, null));
        list.add(new MenuItem(36, "Where can I find my invoice?",
                "You can find your invoice in the billing history. Please check your billing history under “Order & Billing” in the settings page."
                , false
                , null, "", null, null));
        list.add(new MenuItem(37, "I can’t pay in digital mode. What do I do?",
                "If you cannot pay in digital mode, please contact us and we will try our best to facilitate a convenient mode of payment for you."
                , true
                , null, "I need a non-digital mode of paying for the course."
                , "Payment Related Inquiry", "General Payment Queries"));


        list.add(new MenuItem(62, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(63, "Email Support", "I want to get in touch through email", false, null, ""
                , "Payment Related Inquiry", "General Payment Queries"));

        return list;
    }

    public static List<MenuItem> getPaymentIssues() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(38, "I did not get access to the course even after successfully completing my payment. What do I do?",
                "If your course doesn’t appear in the dashboard even after a successful payment, please contact us immediately and we will assign a developer to fix your issue immediately. Please attach your payment receipt if possible."
                , true
                , "FREQUENTLY ASKED QUESTIONS", "Course wasn’t assigned even after successful payment."
                , "Payment Related Inquiry", "Payment Issues"));
        list.add(new MenuItem(39, "My payments keep getting declined",
                "We do not control the payments as we use Razorpay as our gateway provider. If you are having issues despite trying many times, please contact us and we will try to resolve the issue as soon as possible."
                , true
                , null, "My payments get declined continuously."
                , "Payment Related Inquiry", "Payment Issues"));


        list.add(new MenuItem(64, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(65, "Email Support", "I want to get in touch through email", false, null, ""
                , "Payment Related Inquiry", "Payment Issues"));

        return list;
    }

    public static List<MenuItem> getTechnicalGeneralIssues() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(40, "I get a popup saying “Something Went Wrong”. What do I do?",
                "If you get a popup saying “Something Went Wrong”, it is most likely because our servers are down. Servers are down when the number of requests from our users is very high. Please check back in 10 minutes as we try to restore them as soon as possible."
                , false
                , "FREQUENTLY ASKED QUESTIONS", "", null, null));
        list.add(new MenuItem(41, "I can’t log in because my OTP verification fails. What do I do?",
                "If your OTP verification fails, please try again in 5 minutes. Sometimes, our OTP partner blocks continuous requests as their server flag them as spam. However, if your requests don’t go through at all, please contact us and we will work with you to resolve it."
                , true
                , null, "My OTP verification keeps failing."
                , "Technical Issues", "General Technical Issues"));
        list.add(new MenuItem(42, "My application keeps crashing. What do I do?",
                "If our application keeps crashing, please ensure that you are using the updated version of the application. If that doesn’t fix your problem, please try the following steps to reset the cache:\n\n" +
                        "1. Go to Android Settings \n" +
                        "2. Go to Apps in the settings menu\n" +
                        "3. Search for the DeepLearn application\n" +
                        "4. Click Clear Cache and Stored data\n" +
                        "5. Quit the application and restart your phone\n\n" +
                        "If this doesn’t fix your problem. Please contact us and we will work with you to resolve the issue."
                , true
                , null, "My application keeps crashing."
                , "Technical Issues", "General Technical Issues"));


        list.add(new MenuItem(66, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(67, "Email Support", "I want to get in touch through email", false, null, ""
                , "Technical Issues", "General Technical Issues"));

        return list;
    }

    public static List<MenuItem> getVideoPlayerStreamingRelated() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(43, "Videos don’t play at all. What do I do?",
                "If your videos don’t play, please ensure that you are using the updated version of the application. If that doesn’t fix your problem, please try the following steps to reset the cache:\n\n" +
                        "1. Go to Android Settings \n" +
                        "2. Go to Apps in the settings menu\n" +
                        "3. Search for the DeepLearn application\n" +
                        "4. Click Clear Cache and Stored data\n" +
                        "5. Quit the application and restart your phone\n\n" +
                        "If this doesn’t fix your problem. Please contact us and we will work with you to resolve the issue."
                , true
                , "FREQUENTLY ASKED QUESTIONS", "My videos don’t play at all."
                , "Technical Issues", "Video Player and Streaming Related"));
        list.add(new MenuItem(44, "My videos buffer a lot. What do I do?",
                "If your videos buffer a lot, please ensure that you are using the updated version of the application. Please check if you have adequate network bandwidth. If the videos buffer a lot despite good network conditions, please contact us."
                , true
                , null, "My videos buffer a lot."
                , "Technical Issues", "Video Player and Streaming Related"));
        list.add(new MenuItem(45, "Video controls don’t work. Can’t pause, play or do anything. What do I do?",
                "If your video controls such as Play and Pause button don’t work, please ensure that you are using the updated version of the application. If that doesn’t fix your problem, please try the following steps to reset the cache:\n\n" +
                        "1. Go to Android Settings \n" +
                        "2. Go to Apps in the settings menu\n" +
                        "3. Search for the DeepLearn application\n" +
                        "4. Click Clear Cache and Stored data\n" +
                        "5. Quit the application and restart your phone\n\n" +
                        "If this doesn’t fix your problem. Please contact us and we will work with you to resolve the issue."
                , true
                , null, "My video controls don’t work at all."
                , "Technical Issues", "Video Player and Streaming Related"));


        list.add(new MenuItem(68, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(69, "Email Support", "I want to get in touch through email", false, null, ""
                , "Technical Issues", "Video Player and Streaming Related"));

        return list;
    }

    public static List<MenuItem> getFeatureRequests() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(46, "I have a feature request, what do I do?",
                "If you have a feature request, please contact us and we will inform the product team. We take feedback from our users very seriously and will try our best to implement it. Please contact us to raise a request and try to describe your request as much as possible. If necessary, our team will contact you to understand your request better."
                , true
                , "FREQUENTLY ASKED QUESTIONS", "I have a feature request."
                , "Miscellaneous Requests", "Feature Requests"));
        list.add(new MenuItem(47, "I have raised a feature request, however, it hasn’t been implemented yet. What do I do?",
                "We take feedback from our users very seriously. However, certain requests could take longer than expected depending on the technical complexity of the request."
                , false
                , null, "", null, null));


        list.add(new MenuItem(70, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(71, "Email Support", "I want to get in touch through email", false, null, ""
                , "Miscellaneous Requests", "Feature Requests"));

        return list;
    }

    public static List<MenuItem> getNewCourseRequests() {
        List<MenuItem> list = new ArrayList<>();
        list.add(new MenuItem(48, "I want a new course from Deep Learn. What do I do?",
                "We take feedback from our users very seriously and will try our best to implement it. Please contact us to raise a request and try to describe your request as much as possible. If necessary, our team will contact you to understand your request better."
                , true
                , "FREQUENTLY ASKED QUESTIONS"
                , "I have a new course request."
                , "Miscellaneous Requests", "New Course Requests"));
        list.add(new MenuItem(49, "I have raided a new course request. However, it hasn’t been implemented yet. What do I do?",
                "We take feedback from our users very seriously. However, new course requests could take time as they are thoroughly vetted to ensure business viability before deploying resources to implement it."
                , false
                , null, "", null, null));


        list.add(new MenuItem(72, "Phone Support", "I need to talk to a representative on the phone", "MORE HELP"));
        list.add(new MenuItem(73, "Email Support", "I want to get in touch through email", false, null, ""
                , "Miscellaneous Requests", "New Course Requests"));

        return list;
    }

    public static List<MenuItem> getPhoneDetails() {
        List<MenuItem> list = UserRepositoryManager.getInstance().getPhoneSupportMenu();
        if (list == null) {
            list = new ArrayList<>();
            list.add(new MenuItem(1, "8977474109", "Bangalore", "AVAILABLE PHONE LINES"));
        }
        return list;
    }

    public static List<String> getCoursesList(boolean isOptional) {

        List<String> coursesList = new ArrayList<>();

        if (isOptional)
            coursesList.add("Choose Course (optional)");
        else
            coursesList.add("Choose Course");

        coursesList.add("GATE - Computer Science");
        coursesList.add("GATE - Electrical Engineering");
        coursesList.add("GATE - Electronics and Communication Engineering");
        coursesList.add("GATE - Civil Engineering");
        coursesList.add("GATE - Instrumentation Engineering");
        coursesList.add("GATE - Mechanical Engineering");
        coursesList.add("ESE - General Studies & Aptitude");
        coursesList.add("ESE - Civil Engineering");
        coursesList.add("ESE - Electronics and Communication Engineering");
        coursesList.add("ESE - Electrical Engineering");
        coursesList.add("ESE - Mechanical Engineering");

        return coursesList;

    }

    public static List<Type> getTypeList() {

        List<Type> typeList = new ArrayList<>();

        Type type = new Type();
        type.setTypeName("Issue Type");

        List<String> issueTopicsList = new ArrayList<>();
        issueTopicsList.add("Issue Topic");

        type.setTopicsList(issueTopicsList);


        Type type1 = new Type();
        type1.setTypeName("General Query");

        List<String> issueTopicsList1 = new ArrayList<>();
        issueTopicsList1.add("Issue Topic");
        issueTopicsList1.add("Subscription Related");
        issueTopicsList1.add("Online Test Series");
        issueTopicsList1.add("Study Material");
        issueTopicsList1.add("Device and Region Compatibility");
        issueTopicsList1.add("Other");

        type1.setTopicsList(issueTopicsList1);


        Type type2 = new Type();
        type2.setTypeName("Content Related Query");

        List<String> issueTopicsList2 = new ArrayList<>();
        issueTopicsList2.add("Issue Topic");
        issueTopicsList2.add("General Content Issues");
        issueTopicsList2.add("Reporting Content Issues");
        issueTopicsList2.add("New Content Requests");
        issueTopicsList2.add("Other");

        type2.setTopicsList(issueTopicsList2);

        Type type3 = new Type();
        type3.setTypeName("Payment Related Inquiry");

        List<String> issueTopicsList3 = new ArrayList<>();
        issueTopicsList3.add("Issue Topic");
        issueTopicsList3.add("General Payment Queries");
        issueTopicsList3.add("Payment Issues");
        issueTopicsList3.add("Other");

        type3.setTopicsList(issueTopicsList3);

        Type type4 = new Type();
        type4.setTypeName("Technical Issues");

        List<String> issueTopicsList4 = new ArrayList<>();
        issueTopicsList4.add("Issue Topic");
        issueTopicsList4.add("General Technical Issues");
        issueTopicsList4.add("Video Player and Streaming Related");
        issueTopicsList4.add("Other");

        type4.setTopicsList(issueTopicsList4);

        Type type5 = new Type();
        type5.setTypeName("Miscellaneous Requests");

        List<String> issueTopicsList5 = new ArrayList<>();
        issueTopicsList5.add("Issue Topic");
        issueTopicsList5.add("Feature Requests");
        issueTopicsList5.add("New Course Requests");
        issueTopicsList5.add("Other");

        type5.setTopicsList(issueTopicsList5);


        typeList.add(type);
        typeList.add(type1);
        typeList.add(type2);
        typeList.add(type3);
        typeList.add(type4);
        typeList.add(type5);


        return typeList;

    }

    public static List<IntroductionModel> getIntroductionDetails() {

        List<IntroductionModel> featuresModelList = new ArrayList<>();

        IntroductionModel featuresModel1 = new IntroductionModel();

        featuresModel1.setTitle("Prepare for GATE, \n ESE & PSUs.");
        featuresModel1.setDescription("Learn from experts at ACE Engineering Academy, India’s top institute for GATE,ESE & PSUs.");
        featuresModel1.setImage(R.drawable.screen1);

        IntroductionModel featuresMode2 = new IntroductionModel();

        featuresMode2.setTitle("Got a doubt? Pfft. \n Leave it to us.");
        featuresMode2.setDescription("Get your questions clarified from our experts with Live Doubt Clearing Sessions.");
        featuresMode2.setImage(R.drawable.screen2);

        IntroductionModel featuresMode3 = new IntroductionModel();

        featuresMode3.setTitle("Practice. \n Anywhere. Anytime.");
        featuresMode3.setDescription("Practice with conceptual quizzes and Online Test Series.");
        featuresMode3.setImage(R.drawable.screen3);

        IntroductionModel featuresMode4 = new IntroductionModel();

        featuresMode4.setTitle("Notes on the \n cloud.");
        featuresMode4.setDescription("Take in-app notes and access them anywhere. Voice notes also supported.");
        featuresMode4.setImage(R.drawable.screen4);


        featuresModelList.add(featuresModel1);
        featuresModelList.add(featuresMode2);
        featuresModelList.add(featuresMode3);
        featuresModelList.add(featuresMode4);

        return featuresModelList;
    }


    public static boolean isExternalStorageWritable(Context context) {
        if (context == null)
            return false;
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) && ContextCompat.getExternalFilesDirs(context, null).length >= 2;
    }


}

