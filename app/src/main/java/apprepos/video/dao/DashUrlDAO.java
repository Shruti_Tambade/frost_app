package apprepos.video.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import apprepos.video.model.video.DashUrlsModel;

/**
 * Created by Chenna Rao on 10/22/2019.
 */

@Dao
public interface DashUrlDAO {

    @Query("SELECT * FROM dash_urls")
    List<DashUrlsModel> getAll();

    @Update
    void update(List<DashUrlsModel> dashUrlsModelList);

    @Update
    void pushUpdatedDashUrl(DashUrlsModel dashUrlsModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrUpdateDashModel(DashUrlsModel dashUrlsModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDashModel(List<DashUrlsModel> dashUrlsModelList);

    @Query("SELECT * FROM dash_urls WHERE dashUrl=:url")
    DashUrlsModel getDashUrlsByUrl(String url);

    @Query("SELECT * FROM dash_urls WHERE pageContentId=:pageContentId")
    DashUrlsModel findPageContentUrl(String pageContentId);

    @Query("SELECT * FROM dash_urls WHERE pageContentId IN (:pageContentIds) and manifestData IS NULL")
    List<DashUrlsModel> getDashUrlsEmptyDashManifest(List<String> pageContentIds);

    @Query("SELECT * FROM dash_urls WHERE pageContentId IN (:pageContentIds)")
    List<DashUrlsModel> getDashUrlsBasedOnPageContentIds(List<String> pageContentIds);

    @Query("SELECT * FROM dash_urls WHERE timeLimit < (:timeLimit)")
    List<DashUrlsModel> getDashUrlsPendingDownloads(long timeLimit);

    @Query("SELECT * FROM dash_urls WHERE pageContentId IN (:pageContentIds) and timeLimit < (:timeLimit)")
    List<DashUrlsModel> getDashUrlsPendingDownloadsByIds(List<String> pageContentIds, long timeLimit);

    @Query("UPDATE dash_urls SET license= (:license), timeLimit = (:timeLimit)  where pageContentId = (:pageContentId)")
    void updateExpiredDashLicense(String pageContentId, String license, long timeLimit);

    @Query("DELETE FROM dash_urls")
    void deleteAll();
}
