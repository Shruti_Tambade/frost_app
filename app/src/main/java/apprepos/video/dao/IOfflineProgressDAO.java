package apprepos.video.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import apprepos.video.model.video.OfflineProgressModel;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 16-07-2020.
 * <p>
 * Frost
 */
@Dao
public interface IOfflineProgressDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(OfflineProgressModel offlineProgressModel);

    @Query("SELECT * FROM offline_progress where studentSubTopicId=:studentSubTopicId")
    OfflineProgressModel getOfflineProgressModelBySubTopicID(String studentSubTopicId);

    @Query("SELECT * FROM offline_progress where studentSubTopicId=:studentSubTopicId and videoCompletion<:videoCompletion")
    OfflineProgressModel getOfflineProgressModel(String studentSubTopicId, double videoCompletion);


    @Query("SELECT * FROM offline_progress")
    List<OfflineProgressModel> getAllOfflineProgresses();

    @Query("DELETE FROM offline_progress")
    void deleteAllOfflineProgresses();

    @Query("DELETE FROM offline_progress where studentSubTopicId =:studentSubTopicId")
    void deleteOfflineProgress(String studentSubTopicId);

}
