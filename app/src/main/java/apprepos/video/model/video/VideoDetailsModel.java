package apprepos.video.model.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class VideoDetailsModel implements Parcelable {

    private String videoLang;
    private String videoUrl;
    @SerializedName("videoDuration")
    private Integer videoDuration;

    public VideoDetailsModel(){

    }

    protected VideoDetailsModel(Parcel in) {
        videoLang = in.readString();
        videoUrl = in.readString();
        if (in.readByte() == 0) {
            videoDuration = null;
        } else {
            videoDuration = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoLang);
        dest.writeString(videoUrl);
        if (videoDuration == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(videoDuration);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoDetailsModel> CREATOR = new Creator<VideoDetailsModel>() {
        @Override
        public VideoDetailsModel createFromParcel(Parcel in) {
            return new VideoDetailsModel(in);
        }

        @Override
        public VideoDetailsModel[] newArray(int size) {
            return new VideoDetailsModel[size];
        }
    };

    public String getVideoLang() {
        return videoLang;
    }

    public void setVideoLang(String videoLang) {
        this.videoLang = videoLang;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Integer getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(Integer videoDuration) {
        this.videoDuration = videoDuration;
    }
}
