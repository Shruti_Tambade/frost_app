package apprepos.video.model.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class ContentModel implements Parcelable {

    @SerializedName("videoData")
    private List<VideoDataModel> videoData = null;

    public ContentModel(){

    }

    protected ContentModel(Parcel in) {
        videoData = in.createTypedArrayList(VideoDataModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(videoData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContentModel> CREATOR = new Creator<ContentModel>() {
        @Override
        public ContentModel createFromParcel(Parcel in) {
            return new ContentModel(in);
        }

        @Override
        public ContentModel[] newArray(int size) {
            return new ContentModel[size];
        }
    };

    public List<VideoDataModel> getVideoData() {
        return videoData;
    }

    public void setVideoData(List<VideoDataModel> videoData) {
        this.videoData = videoData;
    }
}
