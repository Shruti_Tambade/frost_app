package apprepos.video.model.video;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
@Entity(tableName = "dash_urls")
public class DashUrlsModel {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "pageContentId")
    private String pageContentId;

    @ColumnInfo(name = "dashUrl")
    private String dashUrl;

    @ColumnInfo(name = "isDownloaded", defaultValue = "0")
    private int isDownloaded;

    @ColumnInfo(name = "license")
    private String license;

    @ColumnInfo(name = "manifestData")
    private String manifestData;

    @ColumnInfo(name = "timeLimit", defaultValue = "0")
    private long timeLimit;

    @ColumnInfo(name = "size", defaultValue = "0")
    private long size = 0;


    public DashUrlsModel() {

    }

    public String getPageContentId() {
        return pageContentId;
    }

    public void setPageContentId(String pageContentId) {
        this.pageContentId = pageContentId;
    }

    public String getDashUrl() {
        return dashUrl;
    }

    public void setDashUrl(String dashUrl) {
        this.dashUrl = dashUrl;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public long getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(long timeLimit) {
        this.timeLimit = timeLimit;
    }

    public String getManifestData() {
        return manifestData;
    }

    public void setManifestData(String manifestData) {
        this.manifestData = manifestData;
    }

    public int getIsDownloaded() {
        return isDownloaded;
    }

    public void setIsDownloaded(int isDownloaded) {
        this.isDownloaded = isDownloaded;
    }
}
