package apprepos.video.model.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class VideoDataModel implements Parcelable {

    private Integer videoId;
    @SerializedName("videoDetails")
    private VideoDetailsModel videoDetails;
    @SerializedName("transcripts")
    private List<Object> transcripts = null;

    public VideoDataModel(){

    }

    protected VideoDataModel(Parcel in) {
        if (in.readByte() == 0) {
            videoId = null;
        } else {
            videoId = in.readInt();
        }
        videoDetails = in.readParcelable(VideoDetailsModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (videoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(videoId);
        }
        dest.writeParcelable(videoDetails, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoDataModel> CREATOR = new Creator<VideoDataModel>() {
        @Override
        public VideoDataModel createFromParcel(Parcel in) {
            return new VideoDataModel(in);
        }

        @Override
        public VideoDataModel[] newArray(int size) {
            return new VideoDataModel[size];
        }
    };

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public VideoDetailsModel getVideoDetails() {
        return videoDetails;
    }

    public void setVideoDetails(VideoDetailsModel videoDetails) {
        this.videoDetails = videoDetails;
    }

    public List<Object> getTranscripts() {
        return transcripts;
    }

    public void setTranscripts(List<Object> transcripts) {
        this.transcripts = transcripts;
    }
}
