package apprepos.video.model.video;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 16-07-2020.
 * <p>
 * Frost
 */
@Entity(tableName = "offline_progress")
public class OfflineProgressModel {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "studentSubTopicId")
    private String studentSubTopicId;

    @NonNull
    @ColumnInfo(name = "studentTopicId")
    private String studentTopicId;

    @NonNull
    @ColumnInfo(name = "studentUnitId")
    private String studentUnitId;

    @NonNull
    @ColumnInfo(name = "studentSubjectId")
    private String studentSubjectId;


    @NonNull
    @ColumnInfo(name = "videoCompletion", defaultValue = "0")
    private double videoCompletion;


    public String getStudentSubTopicId() {
        return studentSubTopicId;
    }

    public void setStudentSubTopicId(String studentSubTopicId) {
        this.studentSubTopicId = studentSubTopicId;
    }

    public String getStudentTopicId() {
        return studentTopicId;
    }

    public void setStudentTopicId(String studentTopicId) {
        this.studentTopicId = studentTopicId;
    }

    public String getStudentUnitId() {
        return studentUnitId;
    }

    public void setStudentUnitId(String studentUnitId) {
        this.studentUnitId = studentUnitId;
    }

    public String getStudentSubjectId() {
        return studentSubjectId;
    }

    public void setStudentSubjectId(String studentSubjectId) {
        this.studentSubjectId = studentSubjectId;
    }

    public double getVideoCompletion() {
        return videoCompletion;
    }

    public void setVideoCompletion(double videoCompletion) {
        this.videoCompletion = videoCompletion;
    }
}
