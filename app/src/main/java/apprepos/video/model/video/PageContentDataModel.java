package apprepos.video.model.video;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class PageContentDataModel implements Parcelable {

    @SerializedName("pageContentId")
    private String pageContentId;
    @SerializedName("content")
    private ContentModel content;
    @SerializedName("dashUrl")
    private String dashUrl;

    public PageContentDataModel(){

    }

    protected PageContentDataModel(Parcel in) {
        pageContentId = in.readString();
        content = in.readParcelable(ContentModel.class.getClassLoader());
        dashUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pageContentId);
        dest.writeParcelable(content, flags);
        dest.writeString(dashUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PageContentDataModel> CREATOR = new Creator<PageContentDataModel>() {
        @Override
        public PageContentDataModel createFromParcel(Parcel in) {
            return new PageContentDataModel(in);
        }

        @Override
        public PageContentDataModel[] newArray(int size) {
            return new PageContentDataModel[size];
        }
    };

    public String getPageContentId() {
        return pageContentId;
    }

    public void setPageContentId(String pageContentId) {
        this.pageContentId = pageContentId;
    }

    public ContentModel getContent() {
        return content;
    }

    public void setContent(ContentModel content) {
        this.content = content;
    }

    public String getDashUrl() {
        return dashUrl;
    }

    public void setDashUrl(String dashUrl) {
        this.dashUrl = dashUrl;
    }
}
