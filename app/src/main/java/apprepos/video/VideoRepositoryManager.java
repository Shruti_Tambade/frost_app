package apprepos.video;

import com.google.gson.JsonObject;

import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.video.model.video.DashUrlsModel;
import apprepos.video.model.video.PageContentDataModel;
import apprepos.topics.service.ITopicService;
import io.reactivex.Observable;
import retrofit2.Response;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao on 9/27/2019.
 */
public class VideoRepositoryManager extends BaseRepositoryManager {

    private static VideoRepositoryManager manager;

    private VideoRepositoryManager() {

    }

    public static VideoRepositoryManager getInstance() {
        if (manager == null)
            manager = new VideoRepositoryManager();

        return manager;
    }

    public Observable<Response<PageContentDataModel>> getVideoUrl(String pageContentId) {

        return getRetrofit().create(ITopicService.class).getVideoUrl(pageContentId, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<JsonObject>> updateVideoStream(String studentSubjectId, String unitId, String topicId
            , long videoCompletion, String subTopicId) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("studentSubjectId", studentSubjectId);
        jsonObject.addProperty("studentUnitId", unitId);
        jsonObject.addProperty("studentTopicId", topicId);
        jsonObject.addProperty("videoCompletionInSeconds", (double) videoCompletion / 1000);
        jsonObject.addProperty("studentSubtopicId", subTopicId);

        return getRetrofit().create(ITopicService.class).updateVideoStream(jsonObject, appSharedPreferences.getString(Constants.TOKEN));
    }


    //getAllVideoUrls Using ContentId
    public Observable<Response<List<DashUrlsModel>>> updateVideoUrls(List<String> objects) {

        return getRetrofit().create(ITopicService.class).updateVideourls(objects, appSharedPreferences.getString(Constants.TOKEN));
    }


    public Observable<Response<JsonObject>> getXmlToken() {
        return getRetrofit().create(ITopicService.class).getXmlToken(appSharedPreferences.getString(Constants.TOKEN));
    }
}
