package apprepos.catalogue.services;

import apprepos.catalogue.model.CatalogueModel;

import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 13-08-2019.
 * <p>
 * Frost
 */
public interface ICatalogueService {

    @GET(Constants.CatalogueConstants.GET_CATALOGUES_LIST_URL + "{id}")
    Observable<Response<List<CatalogueModel>>> getCatalogueList(@Path("id") String id, @Header("authorization") String authorization);


}
