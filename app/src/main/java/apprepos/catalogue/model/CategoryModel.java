package apprepos.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chenna Rao on 10/16/2019.
 */
public class CategoryModel {

    private String categoryId;
    private String categoryName;
    private List<CatalogueModel> catalogues = new ArrayList<>();
    private List<SubCategoryModel> subCategorys = new ArrayList<>();
    private boolean publishState = true;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<CatalogueModel> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(List<CatalogueModel> catalogues) {
        this.catalogues = catalogues;
    }

    public List<SubCategoryModel> getSubCategorys() {
        return subCategorys;
    }

    public void setSubCategorys(List<SubCategoryModel> subCategorys) {
        this.subCategorys = subCategorys;
    }

    public boolean isPublishState() {
        return publishState;
    }

    public void setPublishState(boolean publishState) {
        this.publishState = publishState;
    }
}
