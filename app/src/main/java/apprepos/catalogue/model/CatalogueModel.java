package apprepos.catalogue.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by Chenna Rao on 8/22/2019.
 */
public class CatalogueModel {

    private String studentId;
    private String studentCatalogueId;
    private String catalogueId;
    private String catalogueName;
    private String catalogueImage;
    private String catalogueImageColor;
    private String catalogueDescription;
    private String description;
    private String requestType;
    private Long totalDurationInSce;
    private Integer numberOfSubject;
    private Double completionPercentage;
    private String catalogueStatus;
    private Integer totalVideos;
    private Integer totalDurationInHours;
    private String packageGroupCode;
    private String packageCode;
    private boolean ots;
    @SerializedName("studentSubjectIds")
    private List<String> studentSubjectIds = null;

    public CatalogueModel() {

    }


    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentCatalogueId() {
        return studentCatalogueId;
    }

    public void setStudentCatalogueId(String studentCatalogueId) {
        this.studentCatalogueId = studentCatalogueId;
    }

    public String getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(String catalogueId) {
        this.catalogueId = catalogueId;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getCatalogueImage() {
        return catalogueImage;
    }

    public void setCatalogueImage(String catalogueImage) {
        this.catalogueImage = catalogueImage;
    }

    public String getCatalogueImageColor() {
        return catalogueImageColor;
    }

    public void setCatalogueImageColor(String catalogueImageColor) {
        this.catalogueImageColor = catalogueImageColor;
    }

    public String getCatalogueDescription() {
        return catalogueDescription;
    }

    public void setCatalogueDescription(String catalogueDescription) {
        this.catalogueDescription = catalogueDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Long getTotalDurationInSce() {
        return totalDurationInSce;
    }

    public void setTotalDurationInSce(Long totalDurationInSce) {
        this.totalDurationInSce = totalDurationInSce;
    }

    public Integer getNumberOfSubject() {
        return numberOfSubject;
    }

    public void setNumberOfSubject(Integer numberOfSubject) {
        this.numberOfSubject = numberOfSubject;
    }

    public Double getCompletionPercentage() {
        return completionPercentage;
    }

    public void setCompletionPercentage(Double completionPercentage) {
        this.completionPercentage = completionPercentage;
    }

    public String getCatalogueStatus() {
        return catalogueStatus;
    }

    public void setCatalogueStatus(String catalogueStatus) {
        this.catalogueStatus = catalogueStatus;
    }

    public Integer getTotalVideos() {
        return totalVideos;
    }

    public void setTotalVideos(Integer totalVideos) {
        this.totalVideos = totalVideos;
    }

    public Integer getTotalDurationInHours() {
        return totalDurationInHours;
    }

    public void setTotalDurationInHours(Integer totalDurationInHours) {
        this.totalDurationInHours = totalDurationInHours;
    }

    public String getPackageGroupCode() {
        return packageGroupCode;
    }

    public void setPackageGroupCode(String packageGroupCode) {
        this.packageGroupCode = packageGroupCode;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public boolean isOts() {
        return ots;
    }

    public void setOts(boolean ots) {
        this.ots = ots;
    }

    public List<String> getStudentSubjectIds() {
        return studentSubjectIds;
    }

    public void setStudentSubjectIds(List<String> studentSubjectIds) {
        this.studentSubjectIds = studentSubjectIds;
    }
}
