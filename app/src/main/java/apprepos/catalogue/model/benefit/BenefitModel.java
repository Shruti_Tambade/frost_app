package apprepos.catalogue.model.benefit;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 10/28/2019.
 */
public class BenefitModel implements Parcelable {

    private String name;
    private String description;
    private int image;

    public BenefitModel(){

    }

    protected BenefitModel(Parcel in) {
        name = in.readString();
        description = in.readString();
        image = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BenefitModel> CREATOR = new Creator<BenefitModel>() {
        @Override
        public BenefitModel createFromParcel(Parcel in) {
            return new BenefitModel(in);
        }

        @Override
        public BenefitModel[] newArray(int size) {
            return new BenefitModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
