package apprepos.catalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chenna Rao on 10/16/2019.
 */
public class SubCategoryModel implements Cloneable {

    private String subCategoryId;
    private String subCategoryName;
    private List<CatalogueModel> catalogues = new ArrayList<>();

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public List<CatalogueModel> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(List<CatalogueModel> catalogues) {
        this.catalogues = catalogues;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public SubCategoryModel getClone() {
        try {
            return (SubCategoryModel) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
