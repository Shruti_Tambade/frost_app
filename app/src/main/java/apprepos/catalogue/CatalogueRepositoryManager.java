package apprepos.catalogue;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.catalogue.model.CatalogueModel;
import apprepos.catalogue.services.ICatalogueService;
import apprepos.user.dao.ICacheUrlDAO;
import apprepos.user.model.CacheUrlModel;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 13-08-2019.
 * <p>
 * Frost
 */
public class CatalogueRepositoryManager extends BaseRepositoryManager {

    private static CatalogueRepositoryManager manager;

    private CatalogueRepositoryManager() {

    }

    public static CatalogueRepositoryManager getInstance() {
        if (manager == null)
            manager = new CatalogueRepositoryManager();
        return manager;
    }

    public Observable<Response<List<CatalogueModel>>> getCatalogues() {

        return getRetrofit().create(ICatalogueService.class).getCatalogueList(
                appSharedPreferences.getString(Constants.UserInfo.USER_ID),
                appSharedPreferences.getString(Constants.TOKEN));
    }


    public void updateUserCatalogues(List<CatalogueModel> list) {

        Observable.create(emitter -> {
            try {
                Gson gson = new Gson();
                Type type = new TypeToken<List<CatalogueModel>>() {
                }.getType();
                getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.USER_CATALOGUES, gson.toJson(list, type)));
                Logger.d("OFFLINE CATALOGUES", "STORED SUCCESSFULLY");
                emitter.onNext(true);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }

        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe();

    }


    public Observable<List<CatalogueModel>> getOfflineCatalogues() {

        return Observable.create(emitter -> {
            try {

                // 1.2.3 MIGRATING APP SHARED PREFERENCES -> DB
                if (appSharedPreferences.getString(Constants.USER_CATALOGUES) != null) {
                    getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.USER_CATALOGUES, appSharedPreferences.getString(Constants.USER_CATALOGUES)));
                    appSharedPreferences.delete(Constants.USER_CATALOGUES);
                }

                // DB INSERT OFFLINE CATALOGUES
                CacheUrlModel cacheUrlModel = getCacheUrlDAO().getCacheUrlByTag(Constants.USER_CATALOGUES);
                if (cacheUrlModel == null) {
                    emitter.onNext(new ArrayList<>());
                } else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<CatalogueModel>>() {
                    }.getType();
                    emitter.onNext(gson.fromJson(cacheUrlModel.getData(), type));
                }
                emitter.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }

        });
    }

    public ICacheUrlDAO getCacheUrlDAO() {
        return getDeepBaseProvider().getCacheUrlDAO();
    }
}
