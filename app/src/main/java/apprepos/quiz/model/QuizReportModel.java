package apprepos.quiz.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Frost Interactive
 * Created by Chenna Rao on 9/4/2019.
 */
public class QuizReportModel implements Parcelable {

    private String quizReportId;
    private Integer noOfQuestions;
    private Integer noOfQuestionsAttempted;
    private Integer noOfCorrectAnswers;
    private Integer noOfWrongAnswers;
    private Double scorePercentage;
    private String attemptedDateAndTime;

    protected QuizReportModel(Parcel in) {
        quizReportId = in.readString();
        if (in.readByte() == 0) {
            noOfQuestions = null;
        } else {
            noOfQuestions = in.readInt();
        }
        if (in.readByte() == 0) {
            noOfQuestionsAttempted = null;
        } else {
            noOfQuestionsAttempted = in.readInt();
        }
        if (in.readByte() == 0) {
            noOfCorrectAnswers = null;
        } else {
            noOfCorrectAnswers = in.readInt();
        }
        if (in.readByte() == 0) {
            noOfWrongAnswers = null;
        } else {
            noOfWrongAnswers = in.readInt();
        }
        if (in.readByte() == 0) {
            scorePercentage = null;
        } else {
            scorePercentage = in.readDouble();
        }
        attemptedDateAndTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(quizReportId);
        if (noOfQuestions == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(noOfQuestions);
        }
        if (noOfQuestionsAttempted == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(noOfQuestionsAttempted);
        }
        if (noOfCorrectAnswers == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(noOfCorrectAnswers);
        }
        if (noOfWrongAnswers == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(noOfWrongAnswers);
        }
        if (scorePercentage == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(scorePercentage);
        }
        dest.writeString(attemptedDateAndTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuizReportModel> CREATOR = new Creator<QuizReportModel>() {
        @Override
        public QuizReportModel createFromParcel(Parcel in) {
            return new QuizReportModel(in);
        }

        @Override
        public QuizReportModel[] newArray(int size) {
            return new QuizReportModel[size];
        }
    };

    public String getQuizReportId() {
        return quizReportId;
    }

    public void setQuizReportId(String quizReportId) {
        this.quizReportId = quizReportId;
    }

    public Integer getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public Integer getNoOfQuestionsAttempted() {
        return noOfQuestionsAttempted;
    }

    public void setNoOfQuestionsAttempted(Integer noOfQuestionsAttempted) {
        this.noOfQuestionsAttempted = noOfQuestionsAttempted;
    }

    public Integer getNoOfCorrectAnswers() {
        return noOfCorrectAnswers;
    }

    public void setNoOfCorrectAnswers(Integer noOfCorrectAnswers) {
        this.noOfCorrectAnswers = noOfCorrectAnswers;
    }

    public Integer getNoOfWrongAnswers() {
        return noOfWrongAnswers;
    }

    public void setNoOfWrongAnswers(Integer noOfWrongAnswers) {
        this.noOfWrongAnswers = noOfWrongAnswers;
    }

    public Double getScorePercentage() {
        return scorePercentage;
    }

    public void setScorePercentage(Double scorePercentage) {
        this.scorePercentage = scorePercentage;
    }

    public String getAttemptedDateAndTime() {
        return attemptedDateAndTime;
    }

    public void setAttemptedDateAndTime(String attemptedDateAndTime) {
        this.attemptedDateAndTime = attemptedDateAndTime;
    }
}


