package apprepos.quiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/4/2019.
 */
public class QuestionModel implements Parcelable {

    private String studentQuestionId;
    private String questionId;
    private Integer studentQuestionNumber;
    private String question;


    private boolean correct;
    private boolean unAttempted;

    @SerializedName("options")
    private List<OptionModel> options = null;


    protected QuestionModel(Parcel in) {
        studentQuestionId = in.readString();
        questionId = in.readString();
        if (in.readByte() == 0) {
            studentQuestionNumber = null;
        } else {
            studentQuestionNumber = in.readInt();
        }
        question = in.readString();
        correct = in.readByte() != 0;
        unAttempted = in.readByte() != 0;
        options = in.createTypedArrayList(OptionModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(studentQuestionId);
        dest.writeString(questionId);
        if (studentQuestionNumber == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(studentQuestionNumber);
        }
        dest.writeString(question);
        dest.writeByte((byte) (correct ? 1 : 0));
        dest.writeByte((byte) (unAttempted ? 1 : 0));
        dest.writeTypedList(options);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionModel> CREATOR = new Creator<QuestionModel>() {
        @Override
        public QuestionModel createFromParcel(Parcel in) {
            return new QuestionModel(in);
        }

        @Override
        public QuestionModel[] newArray(int size) {
            return new QuestionModel[size];
        }
    };

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public boolean isUnAttempted() {
        return unAttempted;
    }

    public void setUnAttempted(boolean unAttempted) {
        this.unAttempted = unAttempted;
    }

    public String getStudentQuestionId() {
        return studentQuestionId;
    }

    public void setStudentQuestionId(String studentQuestionId) {
        this.studentQuestionId = studentQuestionId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Integer getStudentQuestionNumber() {
        return studentQuestionNumber;
    }

    public void setStudentQuestionNumber(Integer studentQuestionNumber) {
        this.studentQuestionNumber = studentQuestionNumber;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<OptionModel> getOptions() {
        return options;
    }

    public void setOptions(List<OptionModel> options) {
        this.options = options;
    }
}
