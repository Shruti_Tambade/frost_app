package apprepos.quiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/4/2019.
 */
public class OptionModel implements Parcelable {

    @SerializedName("studentOptionId")
    private String studentOptionId;
    @SerializedName("optionNumber")
    private Integer optionNumber;
    @SerializedName("option")
    private String option;

    @SerializedName("ticked")
    private boolean ticked;
    private boolean correct;
    public boolean isSelected =false;


    protected OptionModel(Parcel in) {
        studentOptionId = in.readString();
        if (in.readByte() == 0) {
            optionNumber = null;
        } else {
            optionNumber = in.readInt();
        }
        option = in.readString();
        ticked = in.readByte() != 0;
        correct = in.readByte() != 0;
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(studentOptionId);
        if (optionNumber == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(optionNumber);
        }
        dest.writeString(option);
        dest.writeByte((byte) (ticked ? 1 : 0));
        dest.writeByte((byte) (correct ? 1 : 0));
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OptionModel> CREATOR = new Creator<OptionModel>() {
        @Override
        public OptionModel createFromParcel(Parcel in) {
            return new OptionModel(in);
        }

        @Override
        public OptionModel[] newArray(int size) {
            return new OptionModel[size];
        }
    };

    public String getStudentOptionId() {
        return studentOptionId;
    }

    public void setStudentOptionId(String studentOptionId) {
        this.studentOptionId = studentOptionId;
    }

    public Integer getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(Integer optionNumber) {
        this.optionNumber = optionNumber;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public Boolean getTicked() {
        return ticked;
    }

    public void setTicked(Boolean ticked) {
        this.ticked = ticked;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isTicked() {
        return ticked;
    }

    public void setTicked(boolean ticked) {
        this.ticked = ticked;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}
