package apprepos.quiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/4/2019.
 */
public class QuizQuestionsModel implements Parcelable {

    private String studentQuizId;
    private String quizId;
    private String studentId;
    private String topicId = "data";
    private String quizName;
    private Boolean completed;
    @SerializedName("questions")
    private List<QuestionModel> questions = null;
    @SerializedName("quizReport")
    private QuizReportModel quizReport;


    protected QuizQuestionsModel(Parcel in) {
        studentQuizId = in.readString();
        quizId = in.readString();
        studentId = in.readString();
        topicId = in.readString();
        quizName = in.readString();
        byte tmpCompleted = in.readByte();
        completed = tmpCompleted == 0 ? null : tmpCompleted == 1;
        questions = in.createTypedArrayList(QuestionModel.CREATOR);
        quizReport = in.readParcelable(QuizReportModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(studentQuizId);
        dest.writeString(quizId);
        dest.writeString(studentId);
        dest.writeString(topicId);
        dest.writeString(quizName);
        dest.writeByte((byte) (completed == null ? 0 : completed ? 1 : 2));
        dest.writeTypedList(questions);
        dest.writeParcelable(quizReport, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuizQuestionsModel> CREATOR = new Creator<QuizQuestionsModel>() {
        @Override
        public QuizQuestionsModel createFromParcel(Parcel in) {
            return new QuizQuestionsModel(in);
        }

        @Override
        public QuizQuestionsModel[] newArray(int size) {
            return new QuizQuestionsModel[size];
        }
    };

    public String getStudentQuizId() {
        return studentQuizId;
    }

    public void setStudentQuizId(String studentQuizId) {
        this.studentQuizId = studentQuizId;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public List<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionModel> questions) {
        this.questions = questions;
    }

    public QuizReportModel getQuizReport() {
        return quizReport;
    }

    public void setQuizReport(QuizReportModel quizReport) {
        this.quizReport = quizReport;
    }
}
