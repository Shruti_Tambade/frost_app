package apprepos.quiz;

import apprepos.quiz.model.QuizQuestionsModel;

import com.google.gson.JsonObject;

import apprepos.BaseRepositoryManager;
import apprepos.quiz.services.IQuizService;
import io.reactivex.Observable;
import retrofit2.Response;
import supporters.constants.Constants;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 06-09-2019.
 * <p>
 * Frost
 */
public class QuizRepositoryManager extends BaseRepositoryManager {

    private static QuizRepositoryManager manager;

    private QuizRepositoryManager() {

    }

    public static QuizRepositoryManager getInstance() {
        if (manager == null)
            manager = new QuizRepositoryManager();
        return manager;
    }

    public Observable<Response<QuizQuestionsModel>> getQuizQuestions(String quizId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        jsonObject.addProperty("quizId", quizId);
        return getRetrofit().create(IQuizService.class).getQuizQuestions(jsonObject,
                appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<JsonObject>> submitQuiz(QuizQuestionsModel quizQuestionsModel) {
        return getRetrofit().create(IQuizService.class).submitQuiz(quizQuestionsModel, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<JsonObject>> updateStatusQuiz(String subjectId, String unitId, String topicId, String quizId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("studentSubjectId", subjectId);
        jsonObject.addProperty("studentUnitId", unitId);
        jsonObject.addProperty("studentTopicId", topicId);
        jsonObject.addProperty("quizId", quizId);
        jsonObject.addProperty("completed", true);
        return getRetrofit().create(IQuizService.class).updateQuizStatus(jsonObject, appSharedPreferences.getString(Constants.TOKEN));
    }


    public Observable<Response<QuizQuestionsModel>> getPreviewQuiz(String quizId) {
        return getRetrofit().create(IQuizService.class).getPreviewQuiz(
                appSharedPreferences.getString(Constants.TOKEN)
                , quizId);
    }
}
