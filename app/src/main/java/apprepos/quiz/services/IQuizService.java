package apprepos.quiz.services;

import apprepos.quiz.model.QuizQuestionsModel;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 06-09-2019.
 * <p>
 * Frost
 */
public interface IQuizService {

    @POST(Constants.QuizConstants.QUIZ_QUESTION_DATA_URL)
    Observable<Response<QuizQuestionsModel>> getQuizQuestions(@Body JsonObject jsonObject, @Header("authorization") String authorization);

    @PUT(Constants.QuizConstants.SUBMIT_QUIZ_URL)
    Observable<Response<JsonObject>> submitQuiz(@Body QuizQuestionsModel quizQuestionsModel,
                                                        @Header("authorization") String authorization);

    @PUT(Constants.QuizConstants.UPDATE_STATUS_QUIZ_URL)
    Observable<Response<JsonObject>> updateQuizStatus(@Body JsonObject json,
                                                      @Header("authorization") String authorization);

    @GET(Constants.QuizConstants.PREVIEW_QUIZ_URL + "{quizId}")
    Observable<Response<QuizQuestionsModel>> getPreviewQuiz(@Header("authorization") String authorization,
                                                            @Path("quizId") String quizId);

}
