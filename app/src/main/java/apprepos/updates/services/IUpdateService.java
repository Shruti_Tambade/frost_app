package apprepos.updates.services;

import com.google.gson.JsonObject;

import java.util.List;

import apprepos.updates.model.UpdateModel;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao on 29-05-2020.
 * <p>
 * Frost
 */
public interface IUpdateService {

    @POST(Constants.UpdateConstants.GET_UPDATES_LIST)
    Observable<Response<List<UpdateModel>>> getUpdates(@Body JsonObject jsonObject);

    @GET(Constants.UpdateConstants.GET_UPDATE_DATA_BY_ID + "/" + "{updateId}")
    Observable<Response<UpdateModel>> getUpdateInfoByID(@Path("updateId") String updateId);

    @PUT(Constants.UpdateConstants.UPDATE_CHOICE_TYPE)
    Observable<Response<JsonObject>> updateChoiceType(@Body JsonObject jsonObject);

}
