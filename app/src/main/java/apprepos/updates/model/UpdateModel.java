package apprepos.updates.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apprepos.updates.model.CatalogueList;
import apprepos.updates.model.TestList;

/**
 * Created by Chenna Rao on 29-05-2020.
 * <p>
 * Frost
 */
public class UpdateModel implements Parcelable {

    private String updateId;
    private String updateType;
    private String updateLabel;
    private String updateTitle;
    private String effectiveDate;
    private String startTime;
    private String endTime;
    private String instructorName;
    @SerializedName("catalogueList")
    private List<CatalogueList> catalogueList = null;
    @SerializedName("catalogueIdList")
    private List<String> catalogueIdList = null;
    private String subjectName;
    private String sessionDescription;
    @SerializedName("testList")
    private List<TestList> testList = null;
    private String choiceType;
    private String joinLink;
    private String pdfLink;
    private boolean archived;
    @SerializedName("new")
    private boolean _new;
    private boolean liveNow;
    private String day;

    public UpdateModel() {

    }

    protected UpdateModel(Parcel in) {
        updateId = in.readString();
        updateType = in.readString();
        updateLabel = in.readString();
        updateTitle = in.readString();
        effectiveDate = in.readString();
        startTime = in.readString();
        endTime = in.readString();
        instructorName = in.readString();
        catalogueList = in.createTypedArrayList(CatalogueList.CREATOR);
        catalogueIdList = in.createStringArrayList();
        subjectName = in.readString();
        sessionDescription = in.readString();
        testList = in.createTypedArrayList(TestList.CREATOR);
        choiceType = in.readString();
        joinLink = in.readString();
        pdfLink = in.readString();
        archived = in.readByte() != 0;
        _new = in.readByte() != 0;
        liveNow = in.readByte() != 0;
        day = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(updateId);
        dest.writeString(updateType);
        dest.writeString(updateLabel);
        dest.writeString(updateTitle);
        dest.writeString(effectiveDate);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeString(instructorName);
        dest.writeTypedList(catalogueList);
        dest.writeStringList(catalogueIdList);
        dest.writeString(subjectName);
        dest.writeString(sessionDescription);
        dest.writeTypedList(testList);
        dest.writeString(choiceType);
        dest.writeString(joinLink);
        dest.writeString(pdfLink);
        dest.writeByte((byte) (archived ? 1 : 0));
        dest.writeByte((byte) (_new ? 1 : 0));
        dest.writeByte((byte) (liveNow ? 1 : 0));
        dest.writeString(day);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UpdateModel> CREATOR = new Creator<UpdateModel>() {
        @Override
        public UpdateModel createFromParcel(Parcel in) {
            return new UpdateModel(in);
        }

        @Override
        public UpdateModel[] newArray(int size) {
            return new UpdateModel[size];
        }
    };

    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getUpdateLabel() {
        return updateLabel;
    }

    public void setUpdateLabel(String updateLabel) {
        this.updateLabel = updateLabel;
    }

    public String getUpdateTitle() {
        return updateTitle;
    }

    public void setUpdateTitle(String updateTitle) {
        this.updateTitle = updateTitle;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public List<CatalogueList> getCatalogueList() {
        return catalogueList;
    }

    public void setCatalogueList(List<CatalogueList> catalogueList) {
        this.catalogueList = catalogueList;
    }

    public List<String> getCatalogueIdList() {
        return catalogueIdList;
    }

    public void setCatalogueIdList(List<String> catalogueIdList) {
        this.catalogueIdList = catalogueIdList;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSessionDescription() {
        return sessionDescription;
    }

    public void setSessionDescription(String sessionDescription) {
        this.sessionDescription = sessionDescription;
    }

    public List<TestList> getTestList() {
        return testList;
    }

    public void setTestList(List<TestList> testList) {
        this.testList = testList;
    }

    public String getChoiceType() {
        return choiceType;
    }

    public void setChoiceType(String choiceType) {
        this.choiceType = choiceType;
    }

    public String getJoinLink() {
        return joinLink;
    }

    public void setJoinLink(String joinLink) {
        this.joinLink = joinLink;
    }

    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public boolean is_new() {
        return _new;
    }

    public void set_new(boolean _new) {
        this._new = _new;
    }

    public boolean isLiveNow() {
        return liveNow;
    }

    public void setLiveNow(boolean liveNow) {
        this.liveNow = liveNow;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
