package apprepos.updates.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 29-05-2020.
 * <p>
 * Frost
 */
public class CatalogueList implements Parcelable {

    private String catalogueName;
    private String catalogueId;

    protected CatalogueList(Parcel in) {
        catalogueName = in.readString();
        catalogueId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(catalogueName);
        dest.writeString(catalogueId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CatalogueList> CREATOR = new Creator<CatalogueList>() {
        @Override
        public CatalogueList createFromParcel(Parcel in) {
            return new CatalogueList(in);
        }

        @Override
        public CatalogueList[] newArray(int size) {
            return new CatalogueList[size];
        }
    };

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(String catalogueId) {
        this.catalogueId = catalogueId;
    }
}
