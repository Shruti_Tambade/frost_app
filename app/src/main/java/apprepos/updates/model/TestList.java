package apprepos.updates.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 29-05-2020.
 * <p>
 * Frost
 */
public class TestList implements Parcelable {

    private String questionType;
    private Integer noOfQuestions;
    private Integer totalMarks;

    protected TestList(Parcel in) {
        questionType = in.readString();
        if (in.readByte() == 0) {
            noOfQuestions = null;
        } else {
            noOfQuestions = in.readInt();
        }
        if (in.readByte() == 0) {
            totalMarks = null;
        } else {
            totalMarks = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(questionType);
        if (noOfQuestions == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(noOfQuestions);
        }
        if (totalMarks == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalMarks);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TestList> CREATOR = new Creator<TestList>() {
        @Override
        public TestList createFromParcel(Parcel in) {
            return new TestList(in);
        }

        @Override
        public TestList[] newArray(int size) {
            return new TestList[size];
        }
    };

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public Integer getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }
}
