package apprepos.updates;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.updates.model.UpdateModel;
import apprepos.updates.services.IUpdateService;
import io.reactivex.Observable;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;

/**
 * Created by Chenna Rao on 29-05-2020.
 * <p>
 * Frost
 */
public class UpdateRepositoryManager extends BaseRepositoryManager {

    private static UpdateRepositoryManager manager;

    private UpdateRepositoryManager() {

    }

    public static UpdateRepositoryManager getInstance() {
        if (manager == null)
            manager = new UpdateRepositoryManager();
        return manager;
    }

    public Observable<Response<List<UpdateModel>>> getUpdatesList(List<String> catalogueIDS, String startDate, String endDate) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        jsonObject.addProperty("startDate", startDate);
        jsonObject.addProperty("endDate", endDate);

        JsonArray items = new JsonArray();
        for (int i = 0; i < catalogueIDS.size(); i++) {
            items.add(catalogueIDS.get(i));
        }

//        items.add("1623ab20-6cdf-4067-854b-8903e0d8275d");

        jsonObject.add("catalogueIds", items);

        Logger.d("catalogueIDs", String.valueOf(jsonObject));

        return getRetrofit().create(IUpdateService.class).getUpdates(jsonObject);
    }

    public Observable<Response<UpdateModel>> getUpdateDataByID(String updateID) {

        return getRetrofit().create(IUpdateService.class).getUpdateInfoByID( updateID);
    }


    public Observable<Response<JsonObject>> joinMeeting(String updateID, String choiceType) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        jsonObject.addProperty("updateId", updateID);
        jsonObject.addProperty("choiceType", choiceType);

        return getRetrofit().create(IUpdateService.class).updateChoiceType(jsonObject);
    }
}
