package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Chenna Rao on 10/17/2019.
 */
public class VideoPathModel implements Parcelable {

    public String previewId;
    public String videoUrl;
    public String thumbnail;

    protected VideoPathModel(Parcel in) {
        previewId = in.readString();
        videoUrl = in.readString();
        thumbnail = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(previewId);
        dest.writeString(videoUrl);
        dest.writeString(thumbnail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoPathModel> CREATOR = new Creator<VideoPathModel>() {
        @Override
        public VideoPathModel createFromParcel(Parcel in) {
            return new VideoPathModel(in);
        }

        @Override
        public VideoPathModel[] newArray(int size) {
            return new VideoPathModel[size];
        }
    };

    public String getPreviewId() {
        return previewId;
    }

    public void setPreviewId(String previewId) {
        this.previewId = previewId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
