package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Chenna Rao on 10/3/2019.
 */
public class InstructorsModel implements Parcelable {

    private String instructorId;
    private String instructorName;
    @SerializedName("role")
    private String role;
    @SerializedName("description")
    private String about;
    @SerializedName("photo")
    private String instructorImage;
    private String resume;

    public InstructorsModel(){

    }


    protected InstructorsModel(Parcel in) {
        instructorId = in.readString();
        instructorName = in.readString();
        role = in.readString();
        about = in.readString();
        instructorImage = in.readString();
        resume = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(instructorId);
        dest.writeString(instructorName);
        dest.writeString(role);
        dest.writeString(about);
        dest.writeString(instructorImage);
        dest.writeString(resume);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InstructorsModel> CREATOR = new Creator<InstructorsModel>() {
        @Override
        public InstructorsModel createFromParcel(Parcel in) {
            return new InstructorsModel(in);
        }

        @Override
        public InstructorsModel[] newArray(int size) {
            return new InstructorsModel[size];
        }
    };

    public String getInstructorId() {
        return instructorId;
    }

    public void setInstructorId(String instructorId) {
        this.instructorId = instructorId;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getInstructorImage() {
        return instructorImage;
    }

    public void setInstructorImage(String instructorImage) {
        this.instructorImage = instructorImage;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }
}
