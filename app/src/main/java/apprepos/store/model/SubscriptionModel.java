package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Chenna Rao on 10/11/2019.
 */
public class SubscriptionModel implements Parcelable {

    private boolean exists;
    private String message;
    private String catalogueStatus;

    public SubscriptionModel() {

    }

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }

    protected SubscriptionModel(Parcel in) {
        exists = in.readByte() != 0;
        message = in.readString();
        catalogueStatus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (exists ? 1 : 0));
        dest.writeString(message);
        dest.writeString(catalogueStatus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubscriptionModel> CREATOR = new Creator<SubscriptionModel>() {
        @Override
        public SubscriptionModel createFromParcel(Parcel in) {
            return new SubscriptionModel(in);
        }

        @Override
        public SubscriptionModel[] newArray(int size) {
            return new SubscriptionModel[size];
        }
    };

    public void setExists(Boolean exists) {
        this.exists = exists;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCatalogueStatus() {
        return catalogueStatus;
    }

    public void setCatalogueStatus(String catalogueStatus) {
        this.catalogueStatus = catalogueStatus;
    }
}
