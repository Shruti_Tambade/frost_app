package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;

/**
 * Created by Chenna Rao on 10/29/2019.
 */
public class CouponModel{

    private String couponId;
    private String couponName;
    private String description;
    private Double couponValue;
    private Boolean amount;
    private Boolean actual;
    private Boolean status;
    private String message;
    private String httpStatus;
    @SerializedName("catalogues")
    private List<CatalogueModel> catalogues = null;

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Double couponValue) {
        this.couponValue = couponValue;
    }

    public Boolean getAmount() {
        return amount;
    }

    public void setAmount(Boolean amount) {
        this.amount = amount;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus) {
        this.httpStatus = httpStatus;
    }

    public List<CatalogueModel> getCatalogues() {
        return catalogues;
    }

    public void setCatalogues(List<CatalogueModel> catalogues) {
        this.catalogues = catalogues;
    }
}
