package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 10/9/2019.
 */
public class StudentUnitModel {

    private String unitName;
    @SerializedName("studentTopics")
    private List<StudentTopicModel> studentTopics = null;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public List<StudentTopicModel> getStudentTopics() {
        return studentTopics;
    }

    public void setStudentTopics(List<StudentTopicModel> studentTopics) {
        this.studentTopics = studentTopics;
    }
}
