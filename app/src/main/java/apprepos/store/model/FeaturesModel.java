package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 10/1/2019.
 */
public class FeaturesModel implements Parcelable {

    private String featureName;
    private int featureImage;

    public FeaturesModel(){

    }

    public FeaturesModel(String featureName, int featureImage) {
        this.featureName = featureName;
        this.featureImage = featureImage;
    }

    protected FeaturesModel(Parcel in) {
        featureName = in.readString();
        featureImage = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(featureName);
        dest.writeInt(featureImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FeaturesModel> CREATOR = new Creator<FeaturesModel>() {
        @Override
        public FeaturesModel createFromParcel(Parcel in) {
            return new FeaturesModel(in);
        }

        @Override
        public FeaturesModel[] newArray(int size) {
            return new FeaturesModel[size];
        }
    };

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public int getFeatureImage() {
        return featureImage;
    }

    public void setFeatureImage(int featureImage) {
        this.featureImage = featureImage;
    }
}
