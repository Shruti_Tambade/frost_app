package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 10/9/2019.
 */
public class StudentSubjectModel {

    private String subjectId;
    private String subjectName;
    private Boolean comingSoon;
    private boolean isSelect=false;
    @SerializedName("studentUnits")
    private List<StudentUnitModel> studentUnits = null;


    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Boolean getComingSoon() {
        return comingSoon;
    }

    public void setComingSoon(Boolean comingSoon) {
        this.comingSoon = comingSoon;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public List<StudentUnitModel> getStudentUnits() {
        return studentUnits;
    }

    public void setStudentUnits(List<StudentUnitModel> studentUnits) {
        this.studentUnits = studentUnits;
    }
}
