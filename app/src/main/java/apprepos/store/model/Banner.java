package apprepos.store.model;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-06-2020.
 * <p>
 * Frost
 */
public class Banner {
    private boolean update;
    private String image;
    private String bgColor;
    private String textColor;
    private String message;

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
