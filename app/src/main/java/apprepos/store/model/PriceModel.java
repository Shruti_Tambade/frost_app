package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 10/9/2019.
 */
public class PriceModel implements Parcelable {

    private String priceId;
    private String expire;
    private String subscriptionType;
    private String label;
    private Double actualPrice;
    private Double finalPrice;
    private Double discount;
    private String discountName;
    private Boolean discountActiveStatus;
    private Boolean fixed;
    private boolean isCheched = false;


    protected PriceModel(Parcel in) {
        priceId = in.readString();
        expire = in.readString();
        subscriptionType = in.readString();
        label = in.readString();
        if (in.readByte() == 0) {
            actualPrice = null;
        } else {
            actualPrice = in.readDouble();
        }
        if (in.readByte() == 0) {
            finalPrice = null;
        } else {
            finalPrice = in.readDouble();
        }
        if (in.readByte() == 0) {
            discount = null;
        } else {
            discount = in.readDouble();
        }
        discountName = in.readString();
        byte tmpDiscountActiveStatus = in.readByte();
        discountActiveStatus = tmpDiscountActiveStatus == 0 ? null : tmpDiscountActiveStatus == 1;
        byte tmpFixed = in.readByte();
        fixed = tmpFixed == 0 ? null : tmpFixed == 1;
        isCheched = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(priceId);
        dest.writeString(expire);
        dest.writeString(subscriptionType);
        dest.writeString(label);
        if (actualPrice == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(actualPrice);
        }
        if (finalPrice == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(finalPrice);
        }
        if (discount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(discount);
        }
        dest.writeString(discountName);
        dest.writeByte((byte) (discountActiveStatus == null ? 0 : discountActiveStatus ? 1 : 2));
        dest.writeByte((byte) (fixed == null ? 0 : fixed ? 1 : 2));
        dest.writeByte((byte) (isCheched ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PriceModel> CREATOR = new Creator<PriceModel>() {
        @Override
        public PriceModel createFromParcel(Parcel in) {
            return new PriceModel(in);
        }

        @Override
        public PriceModel[] newArray(int size) {
            return new PriceModel[size];
        }
    };

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(Double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public Boolean getDiscountActiveStatus() {
        return discountActiveStatus;
    }

    public void setDiscountActiveStatus(Boolean discountActiveStatus) {
        this.discountActiveStatus = discountActiveStatus;
    }

    public Boolean getFixed() {
        return fixed;
    }

    public void setFixed(Boolean fixed) {
        this.fixed = fixed;
    }

    public boolean isCheched() {
        return isCheched;
    }

    public void setCheched(boolean cheched) {
        isCheched = cheched;
    }
}
