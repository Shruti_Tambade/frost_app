package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 10/9/2019.
 */
public class CatalogueOverviewModel {

    private String catalogueId;
    private String catalogueName;
    private String catalogueImage;
    private String catalogueImageColor;
    private String catalogueDescription;
    private String packageGroupCode;
    private String packageCode;
    @SerializedName("instructors")
    private List<InstructorsModel> instructors = null;
    @SerializedName("videoPaths")
    private List<VideoPathModel> videoPaths = null;
    @SerializedName("coupons")
    private List<Object> coupons = null;
    @SerializedName("prices")
    private List<PriceModel> prices = null;
    private Boolean comingSoon;
    @SerializedName("studentSubjects")
    private List<StudentSubjectModel> studentSubjects = null;

    public String getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(String catalogueId) {
        this.catalogueId = catalogueId;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getCatalogueImage() {
        return catalogueImage;
    }

    public void setCatalogueImage(String catalogueImage) {
        this.catalogueImage = catalogueImage;
    }

    public String getCatalogueImageColor() {
        return catalogueImageColor;
    }

    public void setCatalogueImageColor(String catalogueImageColor) {
        this.catalogueImageColor = catalogueImageColor;
    }

    public String getCatalogueDescription() {
        return catalogueDescription;
    }

    public void setCatalogueDescription(String catalogueDescription) {
        this.catalogueDescription = catalogueDescription;
    }

    public String getPackageGroupCode() {
        return packageGroupCode;
    }

    public void setPackageGroupCode(String packageGroupCode) {
        this.packageGroupCode = packageGroupCode;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public List<InstructorsModel> getInstructors() {
        return instructors;
    }

    public void setInstructors(List<InstructorsModel> instructors) {
        this.instructors = instructors;
    }

    public List<VideoPathModel> getVideoPaths() {
        return videoPaths;
    }

    public void setVideoPaths(List<VideoPathModel> videoPaths) {
        this.videoPaths = videoPaths;
    }

    public List<Object> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<Object> coupons) {
        this.coupons = coupons;
    }

    public List<PriceModel> getPrices() {
        return prices;
    }

    public void setPrices(List<PriceModel> prices) {
        this.prices = prices;
    }

    public Boolean getComingSoon() {
        return comingSoon;
    }

    public void setComingSoon(Boolean comingSoon) {
        this.comingSoon = comingSoon;
    }

    public List<StudentSubjectModel> getStudentSubjects() {
        return studentSubjects;
    }

    public void setStudentSubjects(List<StudentSubjectModel> studentSubjects) {
        this.studentSubjects = studentSubjects;
    }
}
