package apprepos.store.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import apprepos.topics.model.StudentContentModel;

/**
 * Created by Chenna Rao on 10/9/2019.
 */
public class StudentTopicModel {

    private String topicName;
    @SerializedName("studentContents")
    private List<StudentContentModel> studentContents = null;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public List<StudentContentModel> getStudentContents() {
        return studentContents;
    }

    public void setStudentContents(List<StudentContentModel> studentContents) {
        this.studentContents = studentContents;
    }

}
