package apprepos.store.service;

import com.google.gson.JsonObject;

import java.util.List;

import apprepos.catalogue.model.SubCategoryModel;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.store.model.CouponModel;
import apprepos.store.model.SubscriptionModel;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao on 9/30/2019.
 */
public interface IStoreCatalogueService {

    @GET(Constants.StoreConstants.STORE_CATALOGUES_BY_CATEGORY)
    Observable<Response<List<SubCategoryModel>>> getStoreCatalogueList(@Header("authorization") String authorization);

    @GET(Constants.StoreConstants.GET_STORE_CATALOGUE_PREVIEW + "{id}")
    Observable<Response<CatalogueOverviewModel>> getStoreSubjectsList(@Path("id") String id);

    @POST(Constants.StoreConstants.STUDENT_ORDER_URL)
    Observable<Response<JsonObject>> generateOrder(@Body JsonObject jsonObject, @Header("authorization") String authorization);

    @PUT(Constants.StoreConstants.UPDATE_PAYMENT_STATUS_URL)
    Observable<Response<JsonObject>> updatePaymentStatus(@Body JsonObject jsonObject, @Header("authorization") String authorization);

    @POST(Constants.StoreConstants.GET_CATALOGUE_SUBSCRIPTION_STATUS)
    Observable<Response<SubscriptionModel>> getCatalogueSubscriptionStatus(@Body JsonObject jsonObject, @Header("authorization") String authorization);

    @GET(Constants.CouponConstants.GET_COUPON_INFO + "{catalogueid}/" + "{couponcode}")
    Observable<Response<CouponModel>> getCouponData(@Path("catalogueid") String id, @Path("couponcode") String couponcode);
}
