package apprepos.store;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.catalogue.model.SubCategoryModel;
import apprepos.store.model.Banner;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.store.model.CouponModel;
import apprepos.store.model.SubscriptionModel;
import apprepos.store.service.IStoreCatalogueService;
import io.reactivex.Observable;
import retrofit2.Response;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao on 9/30/2019.
 */
public class StoreRepositoryManager extends BaseRepositoryManager {

    private static StoreRepositoryManager manager;

    private StoreRepositoryManager() {

    }

    public static StoreRepositoryManager getInstance() {
        if (manager == null)
            manager = new StoreRepositoryManager();

        return manager;
    }


    public Observable<Response<List<SubCategoryModel>>> getStoreCatalogues() {

        return getRetrofit().create(IStoreCatalogueService.class).getStoreCatalogueList(appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<CatalogueOverviewModel>> getStoreSubjects(String studentCatalogueId) {

        return getRetrofit().create(IStoreCatalogueService.class).getStoreSubjectsList(studentCatalogueId);
    }

    public Observable<Response<JsonObject>> generateOrder(String catalogueId, String name, String priceId, String couponId) {

        JsonObject item = new JsonObject();
        item.addProperty("itemId", catalogueId);
        item.addProperty("itemName", name);
        item.addProperty("itemPriceId", priceId);
        item.addProperty("couponId", couponId);


        JsonObject studentInfo = new JsonObject();
        studentInfo.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        studentInfo.addProperty("email", appSharedPreferences.getString(Constants.UserInfo.EMAIL));
        studentInfo.addProperty("name", appSharedPreferences.getString(Constants.UserInfo.FIRST_NAME) + " " + appSharedPreferences.getString(Constants.UserInfo.LAST_NAME));
        studentInfo.addProperty("mobile", appSharedPreferences.getString(Constants.UserInfo.MOBILE_NUMBER));


        JsonObject body = new JsonObject();
        JsonArray items = new JsonArray();
        items.add(item);
        body.add("items", items);
        body.add("studentInfo", studentInfo);


        return getRetrofit().create(IStoreCatalogueService.class).generateOrder(body, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<JsonObject>> updateOrderStatus(String orderId, String paymentId) {

        JsonObject studentInfo = new JsonObject();
        studentInfo.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        studentInfo.addProperty("email", appSharedPreferences.getString(Constants.UserInfo.EMAIL));
        studentInfo.addProperty("name", appSharedPreferences.getString(Constants.UserInfo.FIRST_NAME) + " " + appSharedPreferences.getString(Constants.UserInfo.LAST_NAME));
        studentInfo.addProperty("mobile", appSharedPreferences.getString(Constants.UserInfo.MOBILE_NUMBER));

        JsonObject body = new JsonObject();
        body.add("studentInfo", studentInfo);
        body.addProperty("razorpay_order_id", orderId);
        body.addProperty("razorpay_payment_id", paymentId);


        return getRetrofit().create(IStoreCatalogueService.class).updatePaymentStatus(body, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<SubscriptionModel>> getSubscriptionStatus(String catalogueId, String studentId) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("catalogueId", catalogueId);
        jsonObject.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));

        return getRetrofit().create(IStoreCatalogueService.class).getCatalogueSubscriptionStatus(jsonObject, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<CouponModel>> getCouponInfo(String catalogueId, String couponCode) {

        return getRetrofit().create(IStoreCatalogueService.class).getCouponData(catalogueId, couponCode);
    }


    public JsonObject getHlsVideos() {
        return appSharedPreferences.getString(Constants.HLS_VIDEOS) == null ? null : new JsonParser().parse(appSharedPreferences.getString(Constants.HLS_VIDEOS)).getAsJsonObject();
    }


    public Banner getStoreBanner() {
        String banner = appSharedPreferences.getString(Constants.APP_STORE_BANNER);
        if (TextUtils.isEmpty(banner)) {
            return null;
        }

        Gson gson = new Gson();
        Type type = new TypeToken<Banner>() {
        }.getType();
        return gson.fromJson(appSharedPreferences.getString(Constants.APP_STORE_BANNER), type);
    }
}
