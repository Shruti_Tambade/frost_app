package apprepos;

import network.NetworkManager;
import retrofit2.Retrofit;
import storage.AppSharedPreferences;
import storage.DeepDataBaseProvider;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr.Psycho) on 28-02-2019.
 * at 14:41
 * Frost-Interactive
 */

/*
 *   We extend this in network call places
 *   we use AppSharedPreferences for storing the data and retriving the data inside network calls
 *   AppRequest is used which type of request we want it return.
 *   NetworkManager is used to request which type of call you need (Mulitpart, JsonObject,KeyValuePair)
 */
public abstract class BaseRepositoryManager {

    public AppSharedPreferences appSharedPreferences = AppSharedPreferences.getInstance();

    private NetworkManager networkManager = null;

    public DeepDataBaseProvider getDeepBaseProvider()  {
        return DeepDataBaseProvider.getInstance(Utility.getContext());
    }

    public Retrofit getRetrofit() {
        try {
            if (networkManager == null)
                networkManager = NetworkManager.getInstance();

            return networkManager.getRetrofit();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Retrofit getRetrofitByBaseUrl(String baseUrl) {
        try {
            if (networkManager == null)
                networkManager = NetworkManager.getInstance();


            return networkManager.getRetrofitByBaseUrl(baseUrl);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getBufferedReaderByURL(String url) {
        if (networkManager == null)
            networkManager = NetworkManager.getInstance();

        return networkManager.getBufferedReaderByURL(url);
    }
}