package apprepos.topics.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import apprepos.topics.model.DownloadSubTopicModel;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 04-03-2020.
 * <p>
 * FROST
 */

@Dao
public interface IDownloadSubTopicDAO {

    @Query("SELECT * FROM download_subtopic")
    List<DownloadSubTopicModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DownloadSubTopicModel downloadSubTopicModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertList(List<DownloadSubTopicModel> downloadSubTopicModels);

    @Update
    void update(List<DownloadSubTopicModel> downloadSubTopicModels);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDownloadSubTopics(List<DownloadSubTopicModel> downloadSubTopicModels);

    @Query("SELECT * FROM download_subtopic WHERE pageContentId=:pageContentId")
    DownloadSubTopicModel getDownloadSubTopicByPageContentId(String pageContentId);

    @Query("SELECT * FROM download_subtopic WHERE subTopicId=:subTopicId")
    DownloadSubTopicModel getDownloadSubTopicBySubTopicId(String subTopicId);

    @Query("SELECT * FROM download_subtopic WHERE videoUrl=:videoUrl")
    List<DownloadSubTopicModel> getDownloadSubTopicByVideoUrl(String videoUrl);

    @Query("SELECT * FROM download_subtopic WHERE topicId=:topicId")
    List<DownloadSubTopicModel> getDownloadSubTopicByTopicId(String topicId);

    @Query("SELECT * FROM download_subtopic  WHERE unitId=:unitId ORDER BY position ASC")
    List<DownloadSubTopicModel> getDownloadSubTopicByUnitId(String unitId);

    @Query("SELECT * FROM download_subtopic WHERE subjectId=:subjectId")
    List<DownloadSubTopicModel> getDownloadSubTopicBySubjectId(String subjectId);

    @Query("SELECT * FROM download_subtopic WHERE catalogueId=:catalogueId")
    List<DownloadSubTopicModel> getDownloadSubTopicByCatalogueId(String catalogueId);

    @Delete
    void deleteDownloadSubTopic(DownloadSubTopicModel downloadSubTopicModel);

    @Query("DELETE FROM download_subtopic WHERE videoUrl=:videoUrl")
    void deleteDownloadSubTopicByVideoUrl(String videoUrl);

    @Query("DELETE FROM download_subtopic")
    void deleteAll();


}


