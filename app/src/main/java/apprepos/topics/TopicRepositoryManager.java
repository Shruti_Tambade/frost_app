package apprepos.topics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Pair;

import com.frost.leap.applications.HelperApplication;
import com.frost.leap.generic.ShareDataManager;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DeepLearnDrmSessionManager;
import com.google.android.exoplayer2.drm.DeepLearnOfflineLicenseHelper;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

import apprepos.BaseRepositoryManager;
import apprepos.topics.dao.IDownloadSubTopicDAO;
import apprepos.topics.model.DownloadSubTopicModel;
import apprepos.topics.model.TopicModel;
import apprepos.topics.service.ITopicService;
import apprepos.user.UserRepositoryManager;
import apprepos.user.dao.ICacheUrlDAO;
import apprepos.user.model.CacheUrlModel;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.dao.IOfflineProgressDAO;
import apprepos.video.model.video.DashUrlsModel;
import apprepos.video.model.video.OfflineProgressModel;
import apprepos.video.model.video.PageContentDataModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public class TopicRepositoryManager extends BaseRepositoryManager {

    private static TopicRepositoryManager manager;

    private TopicRepositoryManager() {

    }

    public static TopicRepositoryManager getInstance() {
        if (manager == null)
            manager = new TopicRepositoryManager();
        return manager;
    }


    public Observable<Response<TopicModel>> getTopics(String unitId) {

        return getRetrofit().create(ITopicService.class).getTopicsList(unitId, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<PageContentDataModel>> getVideoUrl(String pageContentId) {

        return getRetrofit().create(ITopicService.class).getVideoUrl(pageContentId, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<ResponseBody>> getSubTopicRating(String subTopicId) {

        return getRetrofit().create(ITopicService.class).getSubTopicRating(subTopicId, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<JsonObject>> updateVideoStream(String studentSubjectId, String unitId, String topicId
            , int videoCompletion, String subTopicId) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("studentSubjectId", studentSubjectId);
        jsonObject.addProperty("studentUnitId", unitId);
        jsonObject.addProperty("studentTopicId", topicId);
        jsonObject.addProperty("videoCompletionInSeconds", videoCompletion);
        jsonObject.addProperty("studentSubtopicId", subTopicId);

        return getRetrofit().create(ITopicService.class).updateVideoStream(jsonObject, appSharedPreferences.getString(Constants.TOKEN));
    }


    public Observable<Response<JsonObject>> getXmlToken() {
        return getRetrofit().create(ITopicService.class).getXmlToken(appSharedPreferences.getString(Constants.TOKEN));
    }


    public DashUrlDAO getDashUrlDAO() {
        return getDeepBaseProvider().dashUrlDAO();
    }

    public IDownloadSubTopicDAO getDownloadSubTopicDAO() {
        return getDeepBaseProvider().getDownloadSubTopicDAO();
    }


    private String getDRMLicenseUrl() {
        return Constants.VideoConstants.DRM_SERVER_MOBILE_URL + appSharedPreferences.getString(Constants.DRM_LICENSE_TAG);
    }


    public String getOfflineDRMLicense() {
        return Constants.VideoConstants.DRM_SERVER_OFFLINE_URL + appSharedPreferences.getString(Constants.DRM_OFFLINE_TAG);
    }

    public String getDRMKey() {
        return "" + Utility.getCipherData(Utility.getIndianTimeStamp(),
                getRSAPublicKey());
    }

    private String getRSAPublicKey() {
        return appSharedPreferences.getString(Constants.DEEP_LEARN_RSA_PUBLIC_KEY);
    }


    public void updateUnitTopic(String unitId, TopicModel topicModel) {

        Observable.create(emitter -> {
            try {
                Gson gson = new Gson();
                Type type = new TypeToken<TopicModel>() {
                }.getType();
                getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.UNIT_TOPICS + unitId, gson.toJson(topicModel, type)));
                Logger.d("OFFLINE TOPICS", "STORED SUCCESSFULLY");
                emitter.onNext(true);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }

        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe();
    }


    public Observable<TopicModel> getOfflineUnitTopic(String unitId) {

        return Observable.create(emitter -> {
            try {

                // 1.2.3 MIGRATING APP SHARED PREFERENCES -> DB
                if (appSharedPreferences.getString(Constants.UNIT_TOPICS + unitId) != null) {
                    getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.UNIT_TOPICS + unitId,
                            appSharedPreferences.getString(Constants.UNIT_TOPICS + unitId)));
                    appSharedPreferences.delete(Constants.UNIT_TOPICS + unitId);
                }

                // DB INSERT OFFLINE CATALOGUES
                CacheUrlModel cacheUrlModel = getCacheUrlDAO().getCacheUrlByTag(Constants.UNIT_TOPICS + unitId);
                if (cacheUrlModel == null) {
                    emitter.onError(new Exception());
                } else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<TopicModel>() {
                    }.getType();
                    emitter.onNext(gson.fromJson(cacheUrlModel.getData(), type));
                }
                emitter.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }

        });
    }


    public Observable<Boolean> hasOfflineDataOfTopic(String unitId) {
        return Observable.create(emitter -> {
            try {

                // 1.2.3 MIGRATING APP SHARED PREFERENCES -> DB
                if (appSharedPreferences.contains(Constants.UNIT_TOPICS + unitId)) {
                    getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.UNIT_TOPICS + unitId,
                            appSharedPreferences.getString(Constants.UNIT_TOPICS + unitId)));
                    appSharedPreferences.delete(Constants.UNIT_TOPICS + unitId);
                }

                // DB INSERT OFFLINE CATALOGUES
                CacheUrlModel cacheUrlModel = getCacheUrlDAO().getCacheUrlByTag(Constants.UNIT_TOPICS + unitId);
                emitter.onNext(cacheUrlModel != null ? true : false);
                emitter.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }

        });

    }

    public void deleteOfflineLicense(String url) {

        Observable<Boolean> observable = Observable.create(emitter -> {
            try {
                DashUrlsModel dashUrlsModel = getDeepBaseProvider().dashUrlDAO().getDashUrlsByUrl(url);
                dashUrlsModel.setIsDownloaded(0);
                dashUrlsModel.setLicense(null);
                dashUrlsModel.setTimeLimit(0);
                getDeepBaseProvider().dashUrlDAO().insertOrUpdateDashModel(dashUrlsModel);
                emitter.onNext(true);
            } catch (Exception e) {

                if (!emitter.isDisposed())
                    emitter.onError(e);
            }
            emitter.onComplete();
        });


        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean status) {
                        Logger.d("LICENSE CLEARED " + status);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        // deleting from sub-topic list
        deleteDownloadSubTopicInfo(url);
    }


    public void clearAllDashUrls() {
        Observable observable = Observable.create(emitter -> {
            try {
                getDeepBaseProvider().dashUrlDAO().deleteAll();
                emitter.onNext(true);
            } catch (Exception e) {

                if (!emitter.isDisposed())
                    emitter.onError(e);
            }
            emitter.onComplete();
        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean status) {
                        Logger.d("DELETED DASH URLS ITEMS " + status);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void clearAllDownloadSubTopics() {
        Observable observable = Observable.create(emitter -> {
            try {
                getDeepBaseProvider().getDownloadSubTopicDAO().deleteAll();
                emitter.onNext(true);
            } catch (Exception e) {

                if (!emitter.isDisposed())
                    emitter.onError(e);
            }
            emitter.onComplete();
        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean status) {
                        Logger.d("DELETED ALL ITEMS " + status);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void deleteDownloadSubTopicInfo(String videoUrl) {
        Observable observable = Observable.create(emitter -> {
            try {
                getDeepBaseProvider().getDownloadSubTopicDAO().deleteDownloadSubTopicByVideoUrl(videoUrl);
                emitter.onNext(true);
            } catch (Exception e) {
                if (!emitter.isDisposed())
                    emitter.onError(e);
            }
            emitter.onComplete();
        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean status) {
                        Logger.d("ITEM DELETED " + status);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void saveOfflineLicense(String url, String manifestData, byte[] license, String pageContentId, long milliSeconds) {

        DashUrlsModel dashUrlsModel = new DashUrlsModel();
        dashUrlsModel.setPageContentId(pageContentId);
        dashUrlsModel.setManifestData(manifestData);
        dashUrlsModel.setDashUrl(url);
        dashUrlsModel.setLicense(Arrays.toString(license));
        dashUrlsModel.setIsDownloaded(1);
        dashUrlsModel.setTimeLimit(milliSeconds + System.currentTimeMillis());

        Observable<Boolean> observable = Observable.create(emitter -> {
            try {
                getDeepBaseProvider().dashUrlDAO().insertOrUpdateDashModel(dashUrlsModel);
                emitter.onNext(true);
            } catch (Exception e) {

                if (!emitter.isDisposed())
                    emitter.onError(e);
            }

            emitter.onComplete();
        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean status) {
                        Logger.d("SAVE LICENSE " + status);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void insertOrUpdateDownloadSubTopic(DownloadSubTopicModel downloadSubTopicModel) {
        Observable observable = Observable.create(emitter -> {
            try {
                getDeepBaseProvider().getDownloadSubTopicDAO().insert(downloadSubTopicModel);
                emitter.onNext(true);
            } catch (Exception e) {

                if (!emitter.isDisposed())
                    emitter.onError(e);
            }

            emitter.onComplete();
        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean status) {
                        Logger.d("ITEM INSERTED " + status);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public byte[] getOfflineLicenseByDashUrlsModel(DashUrlsModel dashUrlsModel, String videoUrl) {
        if (dashUrlsModel == null)
            return null;

        if (videoUrl == null)
            return null;

        if (dashUrlsModel.getLicense() == null)
            return null;

        if (dashUrlsModel.getIsDownloaded() == 0)
            return null;

        if (!videoUrl.equalsIgnoreCase(dashUrlsModel.getDashUrl()))
            return null;


        try {
            DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto> offlineLicenseHelper = new DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto>(C.WIDEVINE_UUID,
                    FrameworkMediaDrm.DEFAULT_PROVIDER,
                    null,
                    null);
            byte[] offlineLicenses = decode(dashUrlsModel.getLicense());
            Pair<Long, Long> durationRemainingSec = offlineLicenseHelper.getLicenseDurationRemainingSec(offlineLicenses);

            if (durationRemainingSec.first == 0) { // AUTO DELETING
                HelperApplication.getInstance().getDownloadTracker().deleteDownload(Uri.parse(dashUrlsModel.getDashUrl()));
                deleteOfflineLicense(dashUrlsModel.getDashUrl());
                return null;
            } else {
                return offlineLicenses;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return decode(dashUrlsModel.getLicense());
    }


    public byte[] verifyUnderLicense(DashUrlsModel dashUrlsModel) {

        if (dashUrlsModel == null)
            return null;

        if (dashUrlsModel.getLicense() == null)
            return null;

        if (dashUrlsModel.getIsDownloaded() == 0)
            return null;

        try {
            DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto> offlineLicenseHelper = new DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto>(C.WIDEVINE_UUID,
                    FrameworkMediaDrm.DEFAULT_PROVIDER,
                    null,
                    null);
            byte[] offlineLicenses = decode(dashUrlsModel.getLicense());
            Pair<Long, Long> durationRemainingSec = offlineLicenseHelper.getLicenseDurationRemainingSec(offlineLicenses);

            if (durationRemainingSec.first == 0) {
                return null;
            } else if (durationRemainingSec.first < Constants.UNDER_EXPIRY_TIME_LIMIT) { // UNDER LIMIT
                return null;
            } else {
                return offlineLicenses;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return decode(dashUrlsModel.getLicense());
    }

    public byte[] decode(String stringArray) {
        if (stringArray != null) {
            String[] split = stringArray.substring(1, stringArray.length() - 1).split(", ");
            byte[] array = new byte[split.length];
            for (int i = 0; i < split.length; i++) {
                array[i] = Byte.parseByte(split[i]);
            }
            Logger.d("DATA", "" + array.toString());
            return array;
        }

        return null;
    }


    public String getUserId() {
        return appSharedPreferences.getString(Constants.UserInfo.USER_ID);
    }


    public String getAuthToken() {
        return appSharedPreferences.getString(Constants.TOKEN);
    }


    public void saveDRMTags(String drmLicenseTag, String drmOfflineTag) {

        if (TextUtils.isEmpty(drmLicenseTag) || TextUtils.isEmpty(drmOfflineTag)) {
            UserRepositoryManager.getInstance().saveDRMTagsFromLocal();
            return;
        }
        appSharedPreferences.setString(Constants.DRM_LICENSE_TAG, drmLicenseTag);
        appSharedPreferences.setString(Constants.DRM_OFFLINE_TAG, drmOfflineTag);
    }


    public void pushToNotifyDownloadedSuccessToMixPanel(String url, JsonObject jsonObject) {
        if (url == null)
            return;

        appSharedPreferences.setString(Constants.NOTIFY_DOWNLOADS + url, jsonObject.toString());
    }


    public void deleteNotifyDownload(String url) {
        if (url == null)
            return;

        appSharedPreferences.delete(Constants.NOTIFY_DOWNLOADS + url);
    }

    public JSONObject getDownloadSubTopicInfo(String url) {
        if (url == null)
            return null;

        try {

            String data = appSharedPreferences.getString(Constants.NOTIFY_DOWNLOADS + url);
            if (data == null)
                return null;

            JSONObject jsonObject = new JSONObject(data);
            deleteNotifyDownload(url);
            return jsonObject;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public boolean getAskMeEveryTime() {
        return appSharedPreferences.contains(Constants.ASK_ME_EVERYTIME) ?
                appSharedPreferences.getBoolean(Constants.ASK_ME_EVERYTIME) : true;
    }


    public int getDownloadResolution() {
        return appSharedPreferences.getInt(Constants.DOWNLOAD_RESOLUTION_QUALITY);
    }

    public DeepLearnDrmSessionManager generateDRMManager(String agent) {

        DefaultHttpDataSourceFactory factory = new DefaultHttpDataSourceFactory(agent);
        HttpMediaDrmCallback httpMediaDrmCallback = new HttpMediaDrmCallback(
                getOfflineDRMLicense(),
                true, factory);
        httpMediaDrmCallback.setKeyRequestProperty("authorization", "" + getAuthToken());
        httpMediaDrmCallback.setKeyRequestProperty("key", getDRMKey());

        try {
            DeepLearnDrmSessionManager.Builder drmBuilder = new DeepLearnDrmSessionManager.Builder();
            drmBuilder.setUuidAndExoMediaDrmProvider(C.WIDEVINE_UUID, FrameworkMediaDrm.DEFAULT_PROVIDER);
            drmBuilder.setMultiSession(true);

            DeepLearnDrmSessionManager deepLearnDrmSessionManager = drmBuilder.build(httpMediaDrmCallback);
            deepLearnDrmSessionManager.setSecurityLevel(getSecurityLevel());

            return deepLearnDrmSessionManager;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


    public DeepLearnDrmSessionManager<ExoMediaCrypto> generateDRMManager(String agent, Uri uri, String securityLevel) {
        DefaultHttpDataSourceFactory factory = new DefaultHttpDataSourceFactory(agent);
        HttpMediaDrmCallback httpMediaDrmCallback = new HttpMediaDrmCallback(
                getDRMLicenseUrl(),
                true, factory);
        httpMediaDrmCallback.setKeyRequestProperty("authorization", getAuthToken());
        httpMediaDrmCallback.setKeyRequestProperty("key", getDRMKey());

        try {
            DeepLearnDrmSessionManager.Builder drmBuilder = new DeepLearnDrmSessionManager.Builder();
            drmBuilder.setUuidAndExoMediaDrmProvider(C.WIDEVINE_UUID, FrameworkMediaDrm.DEFAULT_PROVIDER);
            drmBuilder.setMultiSession(true);
            DeepLearnDrmSessionManager<ExoMediaCrypto> drmSessionManager = drmBuilder.build(httpMediaDrmCallback);
            byte[] license = getOfflineLicenseByDashUrlsModel(ShareDataManager.getInstance().getDashUrlsModel(), uri.toString());
            drmSessionManager.setMode(DeepLearnDrmSessionManager.MODE_PLAYBACK, license);
            if (securityLevel != null) {
                drmSessionManager.setSecurityLevel("");
            } else {
                drmSessionManager.setSecurityLevel(getSecurityLevel());
            }

            return drmSessionManager;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public int getAutoResolutions() {

        ConnectivityManager connectivityManager = (ConnectivityManager) Utility.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isWifi = false;
        boolean isMobile = false;

        if (connectivityManager == null)
            return 360;


        for (Network network : connectivityManager.getAllNetworks()) {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(network);
            if (networkInfo == null)
                continue;

            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                isWifi |= networkInfo.isConnected();
            }
            if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                isMobile |= networkInfo.isConnected();
            }
        }

        if (appSharedPreferences.contains(Constants.AUTO_RESOLUTIONS)) {
            JsonObject jsonObject = new JsonParser().parse(appSharedPreferences.getString(Constants.AUTO_RESOLUTIONS)).getAsJsonObject();
            if (jsonObject != null) {
                if (isWifi) {
                    return jsonObject.get("wifi").getAsInt();
                } else {
                    return jsonObject.get(getNetwork(Utility.getContext())).getAsInt();
                }
            } else {
                return 270;
            }
        } else {
            if (isWifi) {
                return 540;
            } else {
                return 360;
            }
        }


    }


    private String getNetwork(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = Objects.requireNonNull(mTelephonyManager).getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN: {
                return "2g";
            }
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP: {
                return "3g";
            }
            case TelephonyManager.NETWORK_TYPE_LTE: {
                return "4g";
            }
            default:
                return "3g";
        }
    }


    public String getSecurityLevel() {
        return appSharedPreferences.getString(Constants.DRM_SECURITY_LEVEL);
    }


    public void pushToOfflineProgress(OfflineProgressModel offlineProgressModel) {
        if (offlineProgressModel == null)
            return;

        Observable<Boolean> observable = Observable.create(emitter -> {
            try {
                IOfflineProgressDAO offlineProgressDAO = getDeepBaseProvider().getOfflineProgressDAO();
                OfflineProgressModel existingOfflineProgressModel = offlineProgressDAO.getOfflineProgressModel(offlineProgressModel.getStudentSubTopicId(), offlineProgressModel.getVideoCompletion());
                if (existingOfflineProgressModel == null) {
                    offlineProgressDAO.insert(offlineProgressModel);
                }
                emitter.onNext(true);
                emitter.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }

        });

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(result -> {
                    Logger.d("PUSH", "Push to offline progress");
                });

    }


    public ICacheUrlDAO getCacheUrlDAO() {
        return getDeepBaseProvider().getCacheUrlDAO();
    }


    // 1.2.3 download licenses to local db
    public void migrationLicensesToDB() {

        if (appSharedPreferences.getBoolean(Constants.IS_LICENSE_MIGRATED)) {
            return;
        }
        Observable<Boolean> observable = Observable.create(emitter ->
        {
            HashMap<Uri, Download> downloadHashMap = HelperApplication.getInstance().getDownloadTracker().getAllDownloads();
            try {
                if (downloadHashMap != null && downloadHashMap.size() > 0) {
                    for (Uri uri : downloadHashMap.keySet()) {
                        try {
                            String license = appSharedPreferences.getString(Constants.LICENSE_DRM + uri.toString());
                            if (license != null) {
                                DashUrlDAO dashUrlDAO = getDeepBaseProvider().dashUrlDAO();
                                DashUrlsModel dashUrlsModel = dashUrlDAO.getDashUrlsByUrl(uri.toString());
                                dashUrlsModel.setLicense(license);
                                dashUrlsModel.setIsDownloaded(1);
                                dashUrlDAO.insertOrUpdateDashModel(dashUrlsModel);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Set<String> keySet = appSharedPreferences.getAllKeySet();
                    for (String key : keySet) {
                        if (key.contains(Constants.LICENSE_DRM) || key.contains(Constants.INFO_VIDEO)
                                || key.contains(Constants.EXPIRY_DRM) || key.contains(Constants.LICENSE_DURATION_DRM)) {
                            Logger.d("DELETING " + key);
                            appSharedPreferences.delete(key);
                        }
                    }
                }
                appSharedPreferences.setBoolean(Constants.IS_LICENSE_MIGRATED, true);
                emitter.onNext(true);
                emitter.onComplete();
            } catch (Exception | OutOfMemoryError | StackOverflowError e) {
                e.printStackTrace();
                if (!emitter.isDisposed())
                    emitter.onError(e);
                emitter.onComplete();
            }
        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Boolean o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public boolean getIsLicenseMigrated() {
        return appSharedPreferences.getBoolean(Constants.IS_LICENSE_MIGRATED);
    }

}
