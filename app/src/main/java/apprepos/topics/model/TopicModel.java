package apprepos.topics.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 8/27/2019.
 */
public class TopicModel  {

    @SerializedName("lastWatchedSubTopic")
    private SubTopicModel subTopicModel;
    @SerializedName("studentTopics")
    private List<StudentTopicsModel> studentTopicsModels = null;

    public SubTopicModel getSubTopicModel() {
        return subTopicModel;
    }

    public void setSubTopicModel(SubTopicModel subTopicModel) {
        this.subTopicModel = subTopicModel;
    }

    public List<StudentTopicsModel> getStudentTopicsModels() {
        return studentTopicsModels;
    }

    public void setStudentTopicsModels(List<StudentTopicsModel> studentTopicsModels) {
        this.studentTopicsModels = studentTopicsModels;
    }
}
