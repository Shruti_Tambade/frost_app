package apprepos.topics.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class SubTopicModel {

    private String studentSubTopicId;
    private String subTopicId;
    private String topicId;
    private String subTopicName;
    private String topicName;
    private String studentTopicId;
    private String description;
    private String pageContentId;
    private double videoCompletionInSeconds = 0;
    private Double videoCompletionPercentage = 0.0;
    private boolean lastWatched = false;
    private Long totalVideoDuration;

    public SubTopicModel() {

    }

    public String getStudentSubTopicId() {
        return studentSubTopicId;
    }

    public void setStudentSubTopicId(String studentSubTopicId) {
        this.studentSubTopicId = studentSubTopicId;
    }

    public String getSubTopicId() {
        return subTopicId;
    }

    public void setSubTopicId(String subTopicId) {
        this.subTopicId = subTopicId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getSubTopicName() {
        return subTopicName;
    }

    public void setSubTopicName(String subTopicName) {
        this.subTopicName = subTopicName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getStudentTopicId() {
        return studentTopicId;
    }

    public void setStudentTopicId(String studentTopicId) {
        this.studentTopicId = studentTopicId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPageContentId() {
        return pageContentId;
    }

    public void setPageContentId(String pageContentId) {
        this.pageContentId = pageContentId;
    }

    public double getVideoCompletionInSeconds() {
        return videoCompletionInSeconds;
    }

    public void setVideoCompletionInSeconds(double videoCompletionInSeconds) {
        this.videoCompletionInSeconds = videoCompletionInSeconds;
    }

    public Double getVideoCompletionPercentage() {
        return videoCompletionPercentage;
    }

    public void setVideoCompletionPercentage(Double videoCompletionPercentage) {
        this.videoCompletionPercentage = videoCompletionPercentage;
    }

    public boolean isLastWatched() {
        return lastWatched;
    }

    public void setLastWatched(boolean lastWatched) {
        this.lastWatched = lastWatched;
    }

    public Long getTotalVideoDuration() {
        return totalVideoDuration;
    }

    public void setTotalVideoDuration(Long totalVideoDuration) {
        this.totalVideoDuration = totalVideoDuration;
    }
}
