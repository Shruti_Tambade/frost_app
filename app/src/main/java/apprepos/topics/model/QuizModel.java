package apprepos.topics.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class QuizModel  {

    private String quizId;
    private String name;
    private Boolean completed;
    private Double videoCompletionPercentage = 0.0;
    private Boolean lastWatched = false;

    public QuizModel()
    {

    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Double getVideoCompletionPercentage() {
        return videoCompletionPercentage;
    }

    public void setVideoCompletionPercentage(Double videoCompletionPercentage) {
        this.videoCompletionPercentage = videoCompletionPercentage;
    }

    public Boolean getLastWatched() {
        return lastWatched;
    }

    public void setLastWatched(Boolean lastWatched) {
        this.lastWatched = lastWatched;
    }
}
