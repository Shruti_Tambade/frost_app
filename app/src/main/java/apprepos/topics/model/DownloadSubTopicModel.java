package apprepos.topics.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 04-03-2020.
 * <p>
 * FROST
 */

@Entity(tableName = "download_subtopic")
public class DownloadSubTopicModel {


    @NonNull
    @ColumnInfo(name = "catalogueId")
    private String catalogueId;

    @NonNull
    @ColumnInfo(name = "subjectId")
    private String subjectId;

    @NonNull
    @ColumnInfo(name = "unitId")
    private String unitId;

    @NonNull
    @ColumnInfo(name = "topicId")
    private String topicId;

    @NonNull
    @ColumnInfo(name = "subTopicId")
    private String subTopicId;


    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "pageContentId")
    private String pageContentId;


    @NonNull
    @ColumnInfo(name = "videoUrl")
    private String videoUrl;


    @NonNull
    @ColumnInfo(name = "position")
    private int position;


    public DownloadSubTopicModel(@NonNull String catalogueId, @NonNull String subjectId, @NonNull String unitId, @NonNull String topicId,
                                 @NonNull String subTopicId, @NonNull String pageContentId, int position) {
        this.catalogueId = catalogueId;
        this.subjectId = subjectId;
        this.unitId = unitId;
        this.topicId = topicId;
        this.subTopicId = subTopicId;
        this.pageContentId = pageContentId;
        this.position = position;
    }

    @NonNull
    public String getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(@NonNull String catalogueId) {
        this.catalogueId = catalogueId;
    }

    @NonNull
    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(@NonNull String subjectId) {
        this.subjectId = subjectId;
    }

    @NonNull
    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(@NonNull String unitId) {
        this.unitId = unitId;
    }

    @NonNull
    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(@NonNull String topicId) {
        this.topicId = topicId;
    }

    @NonNull
    public String getSubTopicId() {
        return subTopicId;
    }

    public void setSubTopicId(@NonNull String subTopicId) {
        this.subTopicId = subTopicId;
    }

    @NonNull
    public String getPageContentId() {
        return pageContentId;
    }

    public void setPageContentId(@NonNull String pageContentId) {
        this.pageContentId = pageContentId;
    }

    @NonNull
    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(@NonNull String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
