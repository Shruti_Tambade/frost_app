package apprepos.topics.model.reviewmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 10/14/2019.
 */
public class RatingModel implements Parcelable {

    private String ratingId;
    private String studentId;
    private String catalogueId;
    private String catalogueName;
    private String subjectId;
    private String subjectName;
    private String unitName;
    private String topicId;
    private String topicName;
    private String studentSubTopicId;
    private String subTopicId;
    private String subTopicName;
    private int rating;
    private String feedBackk;
    @SerializedName("questions")
    private List<RatingQuestionModel> questions = null;
    private String ratingDateTime;

    public RatingModel() {
    }


    protected RatingModel(Parcel in) {
        ratingId = in.readString();
        studentId = in.readString();
        catalogueId = in.readString();
        catalogueName = in.readString();
        subjectId = in.readString();
        subjectName = in.readString();
        unitName = in.readString();
        topicId = in.readString();
        topicName = in.readString();
        studentSubTopicId = in.readString();
        subTopicId = in.readString();
        subTopicName = in.readString();
        rating = in.readInt();
        feedBackk = in.readString();
        questions = in.createTypedArrayList(RatingQuestionModel.CREATOR);
        ratingDateTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ratingId);
        dest.writeString(studentId);
        dest.writeString(catalogueId);
        dest.writeString(catalogueName);
        dest.writeString(subjectId);
        dest.writeString(subjectName);
        dest.writeString(unitName);
        dest.writeString(topicId);
        dest.writeString(topicName);
        dest.writeString(studentSubTopicId);
        dest.writeString(subTopicId);
        dest.writeString(subTopicName);
        dest.writeInt(rating);
        dest.writeString(feedBackk);
        dest.writeTypedList(questions);
        dest.writeString(ratingDateTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RatingModel> CREATOR = new Creator<RatingModel>() {
        @Override
        public RatingModel createFromParcel(Parcel in) {
            return new RatingModel(in);
        }

        @Override
        public RatingModel[] newArray(int size) {
            return new RatingModel[size];
        }
    };

    public String getRatingId() {
        return ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(String catalogueId) {
        this.catalogueId = catalogueId;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getStudentSubTopicId() {
        return studentSubTopicId;
    }

    public void setStudentSubTopicId(String studentSubTopicId) {
        this.studentSubTopicId = studentSubTopicId;
    }

    public String getSubTopicId() {
        return subTopicId;
    }

    public void setSubTopicId(String subTopicId) {
        this.subTopicId = subTopicId;
    }

    public String getSubTopicName() {
        return subTopicName;
    }

    public void setSubTopicName(String subTopicName) {
        this.subTopicName = subTopicName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getFeedBackk() {
        return feedBackk;
    }

    public void setFeedBackk(String feedBackk) {
        this.feedBackk = feedBackk;
    }

    public List<RatingQuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<RatingQuestionModel> questions) {
        this.questions = questions;
    }

    public String getRatingDateTime() {
        return ratingDateTime;
    }

    public void setRatingDateTime(String ratingDateTime) {
        this.ratingDateTime = ratingDateTime;
    }
}
