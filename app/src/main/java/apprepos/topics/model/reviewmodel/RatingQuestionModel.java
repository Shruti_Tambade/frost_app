package apprepos.topics.model.reviewmodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 10/14/2019.
 */
public class RatingQuestionModel implements Parcelable {

    private String questionId;
    private String question;
    private int answer;


    public RatingQuestionModel() {

    }

    protected RatingQuestionModel(Parcel in) {
        questionId = in.readString();
        question = in.readString();
        answer = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(questionId);
        dest.writeString(question);
        dest.writeInt(answer);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RatingQuestionModel> CREATOR = new Creator<RatingQuestionModel>() {
        @Override
        public RatingQuestionModel createFromParcel(Parcel in) {
            return new RatingQuestionModel(in);
        }

        @Override
        public RatingQuestionModel[] newArray(int size) {
            return new RatingQuestionModel[size];
        }
    };

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
