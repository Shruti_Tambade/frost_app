package apprepos.topics.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class StudentContentModel  {

    private String contentType;
    @SerializedName("subTopic")
    private SubTopicModel subTopic;
    @SerializedName("quiz")
    private QuizModel quiz;

    private boolean inProcess = false;
    private float percentage = 0;
    private boolean isDownloaded = false;
    private boolean isFailed = false;

    public boolean isInProcess() {
        return inProcess;
    }

    public void setInProcess(boolean inProcess) {
        this.inProcess = inProcess;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public SubTopicModel getSubTopic() {
        return subTopic;
    }

    public void setSubTopic(SubTopicModel subTopic) {
        this.subTopic = subTopic;
    }

    public QuizModel getQuiz() {
        return quiz;
    }

    public void setQuiz(QuizModel quiz) {
        this.quiz = quiz;
    }

    public boolean isFailed() {
        return isFailed;
    }

    public void setFailed(boolean failed) {
        isFailed = failed;
    }
}
