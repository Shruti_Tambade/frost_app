package apprepos.topics.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 8/26/2019.
 */
public class StudentTopicsModel {

    private String studentTopicId;
    private String topicId;
    private String topicName;
    private Long totalDurationInSce;
    private Double completionPercentage;
    private Boolean lastWatched;
    @SerializedName("studentContents")
    private List<StudentContentModel> studentContents = null;
    private boolean isOffline = false;

    public StudentTopicsModel(){

    }


    public String getStudentTopicId() {
        return studentTopicId;
    }

    public void setStudentTopicId(String studentTopicId) {
        this.studentTopicId = studentTopicId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Long getTotalDurationInSce() {
        return totalDurationInSce;
    }

    public void setTotalDurationInSce(Long totalDurationInSce) {
        this.totalDurationInSce = totalDurationInSce;
    }

    public Double getCompletionPercentage() {
        return completionPercentage;
    }

    public void setCompletionPercentage(Double completionPercentage) {
        this.completionPercentage = completionPercentage;
    }

    public Boolean getLastWatched() {
        return lastWatched;
    }

    public void setLastWatched(Boolean lastWatched) {
        this.lastWatched = lastWatched;
    }

    public List<StudentContentModel> getStudentContents() {
        return studentContents;
    }

    public void setStudentContents(List<StudentContentModel> studentContents) {
        this.studentContents = studentContents;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }

}
