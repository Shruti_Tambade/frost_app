package apprepos.topics.service;

import com.google.gson.JsonObject;

import java.util.List;

import apprepos.topics.model.TopicModel;
import apprepos.video.model.video.DashUrlsModel;
import apprepos.video.model.video.PageContentDataModel;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public interface ITopicService {

    @GET(Constants.CatalogueConstants.GET_TOPICS_LIST_URL + "{id}")
    Observable<Response<TopicModel>> getTopicsList(@Path("id") String id, @Header("authorization") String authorization);

    @GET(Constants.CatalogueConstants.GET_SUBTOPIC_VIDEO_PATH_URL + "{id}")
    Observable<Response<PageContentDataModel>> getVideoUrl(@Path("id") String id, @Header("authorization") String authorization);

    @PUT(Constants.VideoConstants.VIDEO_STREAM_UPDATE)
    Observable<Response<JsonObject>> updateVideoStream(@Body JsonObject jsonObject, @Header("authorization") String authorization);

    @GET(Constants.VideoConstants.GET_XML_TOKEN)
    Observable<Response<JsonObject>> getXmlToken(@Header("authorization") String authorization);

    @GET(Constants.CatalogueConstants.GET_SUBTOPIC_RATING + "{id}")
    Observable<Response<ResponseBody>> getSubTopicRating(@Path("id") String id, @Header("authorization") String authorization);

    @GET(Constants.CatalogueConstants.POST_SUBTOPIC_RATING)
    Observable<Response<ResponseBody>> postSubTopicRating(@Header("authorization") String authorization);

    @POST(Constants.VideoConstants.GET_VIDEO_URLS)
    Observable<Response<List<DashUrlsModel>>> updateVideourls(@Body List<String> jsonObject, @Header("authorization") String authorization);

}
