package apprepos.subject.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Chenna Rao on 8/23/2019.
 */
public class UnitModel {

    @PrimaryKey
    @ColumnInfo(name = "studentUnitId")
    private String studentUnitId;
    @ColumnInfo(name = "unitName")
    private String unitName;
    @ColumnInfo(name = "totalDurationInSce")
    private Long totalDurationInSce;
    @ColumnInfo(name = "completionPercentage")
    private Double completionPercentage;
    @ColumnInfo(name = "lastWatched")
    private Boolean lastWatched;

    public UnitModel() {

    }

    protected UnitModel(Parcel in) {
        studentUnitId = in.readString();
        unitName = in.readString();
        if (in.readByte() == 0) {
            totalDurationInSce = null;
        } else {
            totalDurationInSce = in.readLong();
        }
        if (in.readByte() == 0) {
            completionPercentage = null;
        } else {
            completionPercentage = in.readDouble();
        }
        byte tmpLastWatched = in.readByte();
        lastWatched = tmpLastWatched == 0 ? null : tmpLastWatched == 1;
    }

    public String getStudentUnitId() {
        return studentUnitId;
    }

    public void setStudentUnitId(String studentUnitId) {
        this.studentUnitId = studentUnitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Long getTotalDurationInSce() {
        return totalDurationInSce;
    }

    public void setTotalDurationInSce(Long totalDurationInSce) {
        this.totalDurationInSce = totalDurationInSce;
    }

    public Double getCompletionPercentage() {
        return completionPercentage;
    }

    public void setCompletionPercentage(Double completionPercentage) {
        this.completionPercentage = completionPercentage;
    }

    public Boolean getLastWatched() {
        return lastWatched;
    }

    public void setLastWatched(Boolean lastWatched) {
        this.lastWatched = lastWatched;
    }
}
