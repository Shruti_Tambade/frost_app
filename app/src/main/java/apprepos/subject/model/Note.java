package apprepos.subject.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 03-01-2020.
 * <p>
 * FROST
 */
public class Note implements Parcelable {

    private String catalogueName;
    private String subjectName;
    private String unitName;

    private String studentNoteId;
    private String studentId;
    private String studentSubjectId;
    private String studentUnitId;
    private String studentTopicId;
    private String studentSubtopicId;
    private double timestampInSecond;
    private double videoLengthInSeconds;
    private String note;
    private String topicName;
    private String createdDate;
    private String subTopicName;
    private boolean showMore=false;

    public Note() {

    }


    protected Note(Parcel in) {
        catalogueName = in.readString();
        subjectName = in.readString();
        unitName = in.readString();
        studentNoteId = in.readString();
        studentId = in.readString();
        studentSubjectId = in.readString();
        studentUnitId = in.readString();
        studentTopicId = in.readString();
        studentSubtopicId = in.readString();
        timestampInSecond = in.readDouble();
        videoLengthInSeconds = in.readDouble();
        note = in.readString();
        topicName = in.readString();
        createdDate = in.readString();
        subTopicName = in.readString();
        showMore = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(catalogueName);
        dest.writeString(subjectName);
        dest.writeString(unitName);
        dest.writeString(studentNoteId);
        dest.writeString(studentId);
        dest.writeString(studentSubjectId);
        dest.writeString(studentUnitId);
        dest.writeString(studentTopicId);
        dest.writeString(studentSubtopicId);
        dest.writeDouble(timestampInSecond);
        dest.writeDouble(videoLengthInSeconds);
        dest.writeString(note);
        dest.writeString(topicName);
        dest.writeString(createdDate);
        dest.writeString(subTopicName);
        dest.writeByte((byte) (showMore ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getStudentNoteId() {
        return studentNoteId;
    }

    public void setStudentNoteId(String studentNoteId) {
        this.studentNoteId = studentNoteId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentSubjectId() {
        return studentSubjectId;
    }

    public void setStudentSubjectId(String studentSubjectId) {
        this.studentSubjectId = studentSubjectId;
    }

    public String getStudentUnitId() {
        return studentUnitId;
    }

    public void setStudentUnitId(String studentUnitId) {
        this.studentUnitId = studentUnitId;
    }

    public String getStudentTopicId() {
        return studentTopicId;
    }

    public void setStudentTopicId(String studentTopicId) {
        this.studentTopicId = studentTopicId;
    }

    public String getStudentSubtopicId() {
        return studentSubtopicId;
    }

    public void setStudentSubtopicId(String studentSubtopicId) {
        this.studentSubtopicId = studentSubtopicId;
    }

    public double getTimestampInSecond() {
        return timestampInSecond;
    }

    public void setTimestampInSecond(double timestampInSecond) {
        this.timestampInSecond = timestampInSecond;
    }

    public double getVideoLengthInSeconds() {
        return videoLengthInSeconds;
    }

    public void setVideoLengthInSeconds(double videoLengthInSeconds) {
        this.videoLengthInSeconds = videoLengthInSeconds;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getSubTopicName() {
        return subTopicName;
    }

    public void setSubTopicName(String subTopicName) {
        this.subTopicName = subTopicName;
    }

    public boolean isShowMore() {
        return showMore;
    }

    public void setShowMore(boolean showMore) {
        this.showMore = showMore;
    }
}
