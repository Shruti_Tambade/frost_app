package apprepos.subject.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Chenna Rao on 8/23/2019.
 */
public class SubjectModel {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "studentId")
    private String studentId;
    @ColumnInfo(name = "studentCatalogueId")
    private String studentCatalogueId;
    @ColumnInfo(name = "studentSubjectId")
    private String studentSubjectId;
    @ColumnInfo(name = "subjectName")
    private String subjectName;
    @ColumnInfo(name = "subjectId")
    private String subjectId;
    @ColumnInfo(name = "totalDurationInSce")
    private Long totalDurationInSce;
    @ColumnInfo(name = "completionPercentage")
    private Double completionPercentage;
    @ColumnInfo(name = "studentUnits")
    @SerializedName("studentUnits")
    private List<UnitModel> studentUnits = null;
    @ColumnInfo(name = "isSelect")
    private boolean isSelect = true;
    @ColumnInfo(name = "isOffline")
    private boolean isOffline;

    public SubjectModel() {

    }

    @NonNull
    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(@NonNull String studentId) {
        this.studentId = studentId;
    }

    public String getStudentCatalogueId() {
        return studentCatalogueId;
    }

    public void setStudentCatalogueId(String studentCatalogueId) {
        this.studentCatalogueId = studentCatalogueId;
    }

    public String getStudentSubjectId() {
        return studentSubjectId;
    }

    public void setStudentSubjectId(String studentSubjectId) {
        this.studentSubjectId = studentSubjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public Long getTotalDurationInSce() {
        return totalDurationInSce;
    }

    public void setTotalDurationInSce(Long totalDurationInSce) {
        this.totalDurationInSce = totalDurationInSce;
    }

    public Double getCompletionPercentage() {
        return completionPercentage;
    }

    public void setCompletionPercentage(Double completionPercentage) {
        this.completionPercentage = completionPercentage;
    }

    public List<UnitModel> getStudentUnits() {
        return studentUnits;
    }

    public void setStudentUnits(List<UnitModel> studentUnits) {
        this.studentUnits = studentUnits;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public void setOffline(boolean offline) {
        isOffline = offline;
    }
}
