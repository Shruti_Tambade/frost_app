package apprepos.subject.model;

import java.util.List;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 06-01-2020.
 * <p>
 * FROST
 */
public class SubjectInfo
{
    private String studentSubjectId;
    private String subjectName;
    private List<UnitInfo> unitNames;

    public String getStudentSubjectId() {
        return studentSubjectId;
    }

    public void setStudentSubjectId(String studentSubjectId) {
        this.studentSubjectId = studentSubjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<UnitInfo> getUnitNames() {
        return unitNames;
    }

    public void setUnitNames(List<UnitInfo> unitNames) {
        this.unitNames = unitNames;
    }

    public class UnitInfo {
        private String studentUnitId;
        private String unitName;

        public String getStudentUnitId() {
            return studentUnitId;
        }

        public void setStudentUnitId(String studentUnitId) {
            this.studentUnitId = studentUnitId;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }
    }
}
