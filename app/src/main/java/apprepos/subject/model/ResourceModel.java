package apprepos.subject.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/17/2019.
 */
public class ResourceModel implements Parcelable {

    private String resourceId;
    private String resourceUrl;
    private String resourceName;
    private Integer size;
    private String resourceDate;


    protected ResourceModel(Parcel in) {
        resourceId = in.readString();
        resourceUrl = in.readString();
        resourceName = in.readString();
        if (in.readByte() == 0) {
            size = null;
        } else {
            size = in.readInt();
        }
        resourceDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(resourceId);
        dest.writeString(resourceUrl);
        dest.writeString(resourceName);
        if (size == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(size);
        }
        dest.writeString(resourceDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResourceModel> CREATOR = new Creator<ResourceModel>() {
        @Override
        public ResourceModel createFromParcel(Parcel in) {
            return new ResourceModel(in);
        }

        @Override
        public ResourceModel[] newArray(int size) {
            return new ResourceModel[size];
        }
    };

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getResourceDate() {
        return resourceDate;
    }

    public void setResourceDate(String resourceDate) {
        this.resourceDate = resourceDate;
    }
}
