package apprepos.subject;

import com.frost.leap.components.media.models.GalleryFile;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;
import apprepos.subject.service.INoteService;
import apprepos.user.services.IUserService;
import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 03-01-2020.
 * <p>
 * FROST
 */
public class NoteRepositoryManager extends BaseRepositoryManager {

    private static NoteRepositoryManager manager;


    private NoteRepositoryManager() {

    }


    public static NoteRepositoryManager getInstance() {
        if (manager == null)
            manager = new NoteRepositoryManager();
        return manager;
    }

    public Observable<Response<List<SubjectInfo>>> getCatalogueSubjectsInfo(String catalogueId) {
        return getRetrofit().create(INoteService.class).getAllSubjectsInfo(appSharedPreferences.getString(Constants.TOKEN), catalogueId);
    }

    public Observable<Response<JsonObject>> getNotes(JsonObject body) {
        return getRetrofit().create(INoteService.class).getNotesList(appSharedPreferences.getString(Constants.TOKEN), body);
    }

    public Observable<Response<List<Note>>> getUserNotes(JsonObject body) {
        return getRetrofit().create(INoteService.class).getUserNotesList(appSharedPreferences.getString(Constants.TOKEN), body);
    }


    public Observable<Response<JsonObject>> createnote(Note note) {
        if (note.getStudentId() != null) {
            note.setStudentId(appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        }
        return getRetrofit().create(INoteService.class).createNote(appSharedPreferences.getString(Constants.TOKEN), note);
    }


    public Observable<Response<JsonObject>> updateNote(Note note) {
        return getRetrofit().create(INoteService.class).updateNote(appSharedPreferences.getString(Constants.TOKEN), note);
    }


    public Observable<Response<JsonObject>> deleteNote(String noteId) {
        return getRetrofit().create(INoteService.class).deleteNote(appSharedPreferences.getString(Constants.TOKEN), noteId);
    }


}
