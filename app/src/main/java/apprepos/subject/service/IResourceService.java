package apprepos.subject.service;

import java.util.List;

import apprepos.subject.model.ResourceModel;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/17/2019.
 */
public interface IResourceService {

    @GET(Constants.ResourcesConstants.CATALOGUE_RESOURCES + "{id}")
    Observable<Response<List<ResourceModel>>> getResourcesList(@Path("id") String id, @Header("authorization") String authorization);
}
