package apprepos.subject.service;

import com.google.gson.JsonObject;

import java.util.List;

import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 03-01-2020.
 * <p>
 * FROST
 */
public interface INoteService {


    @GET(Constants.NotesConstants.GET_SUBJECTS_INFO + "{catalogueId}")
    Observable<Response<List<SubjectInfo>>> getAllSubjectsInfo(@Header("authorization") String authorization, @Path("catalogueId") String catalogueId);

    @POST(Constants.NotesConstants.GET_NOTES_LIST)
    Observable<Response<JsonObject>> getNotesList(@Header("authorization") String authorization, @Body JsonObject jsonObject);

    @POST(Constants.NotesConstants.GET_USER_NOTES)
    Observable<Response<List<Note>>> getUserNotesList(@Header("authorization") String authorization, @Body JsonObject jsonObject);




    @POST(Constants.NotesConstants.CRUD_NOTES)
    Observable<Response<JsonObject>> createNote(@Header("authorization") String authorization, @Body Note note);

    @PUT(Constants.NotesConstants.CRUD_NOTES)
    Observable<Response<JsonObject>> updateNote(@Header("authorization") String authorization, @Body Note note);


    @DELETE(Constants.NotesConstants.CRUD_NOTES + "/" + "{noteId}")
    Observable<Response<JsonObject>> deleteNote(@Header("authorization") String authorization, @Path("noteId") String noteId);

}
