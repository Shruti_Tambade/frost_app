package apprepos.subject.service;

import com.google.gson.JsonArray;

import java.util.List;

import apprepos.subject.model.ResourceModel;
import apprepos.subject.model.SubjectModel;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import supporters.constants.Constants;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public interface ISubjectService {

    @GET(Constants.CatalogueConstants.GET_SUBJECTS_LIST_URL + "{id}")
    Observable<Response<List<SubjectModel>>> getSubjectsList(@Path("id") String id, @Header("authorization") String authorization);

    @GET(Constants.ResourcesConstants.CATALOGUE_RESOURCES + "{id}")
    Observable<Response<List<ResourceModel>>> getResourcesList(@Path("id") String id, @Header("authorization") String authorization);

}
