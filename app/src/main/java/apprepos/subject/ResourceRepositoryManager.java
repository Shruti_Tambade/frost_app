package apprepos.subject;

import com.frost.leap.services.models.FileDownload;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import apprepos.BaseRepositoryManager;
import apprepos.subject.model.ResourceModel;
import apprepos.subject.service.IResourceService;
import io.reactivex.Observable;
import retrofit2.Response;
import supporters.constants.Constants;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/17/2019.
 */
public class ResourceRepositoryManager extends BaseRepositoryManager {


    private static ResourceRepositoryManager manager;
    private ResourceRepositoryManager(){

    }

    public static ResourceRepositoryManager getInstance(){
        if(manager==null)
            manager = new ResourceRepositoryManager();
        return manager;
    }

    public Observable<Response<List<ResourceModel>>> getResources(String studentCatalogueId) {

        return getRetrofit().create(IResourceService.class).getResourcesList(studentCatalogueId, appSharedPreferences.getString(Constants.TOKEN));
    }


    public void createOrUpdateResource(String resourceUrl, FileDownload fileDownload)
    {
        Gson gson = new Gson();
        Type type = new TypeToken<FileDownload>(){}.getType();
        String data = gson.toJson(fileDownload, type);
        appSharedPreferences.setString("rd_" + resourceUrl.toLowerCase(),data);
    }


    public void deleteResource(String resourceUrl)
    {
        appSharedPreferences.delete("rd_"+resourceUrl.toLowerCase());
    }


    public boolean containsResource(String resourceUrl)
    {
        return appSharedPreferences.contains("rd_"+resourceUrl.toLowerCase());
    }

    public FileDownload getFileDownload(String resourceUrl)
    {
        Gson gson = new Gson();
        Type type = new TypeToken<FileDownload>(){}.getType();

        return gson.fromJson(appSharedPreferences.getString("rd_"+resourceUrl.toLowerCase()),type);
    }

    public Set<String> getAllPendingDownloads()
    {
        return appSharedPreferences.getKeySet(Constants.RD_PENDING_DOWNLOADS);
    }


    public void deleteAllPendingDownloads()
    {
        appSharedPreferences.delete(Constants.RD_PENDING_DOWNLOADS);
    }

    public void createOrInsertPendingDownloads(String url)
    {
        Set<String> pendingList = getAllPendingDownloads();
        if(pendingList==null)
        {
            pendingList = new HashSet<>();
        }
        pendingList.add(url.toLowerCase());
        appSharedPreferences.setKeySet(Constants.RD_PENDING_DOWNLOADS,pendingList);
    }

    public void overridePendingDownloads(Set<String> pendingList)
    {
        if(pendingList==null)
        {
            pendingList = new HashSet<>();
        }
        appSharedPreferences.setKeySet(Constants.RD_PENDING_DOWNLOADS,pendingList);
    }

    public void removePendingDownloads(String url)
    {
        Set<String> pendingList = getAllPendingDownloads();
        if(pendingList==null)
        {
            pendingList = new HashSet<>();
        }
        if(pendingList.remove(url.toLowerCase())) {
            appSharedPreferences.setKeySet(Constants.RD_PENDING_DOWNLOADS, pendingList);
        }
    }
}
