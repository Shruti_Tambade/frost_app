package apprepos.subject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.subject.model.ResourceModel;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.service.ISubjectService;
import apprepos.topics.dao.IDownloadSubTopicDAO;
import apprepos.user.dao.ICacheUrlDAO;
import apprepos.user.model.CacheUrlModel;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public class SubjectRepositoryManager extends BaseRepositoryManager {


    private static SubjectRepositoryManager manager;


    private SubjectRepositoryManager() {

    }

    public static SubjectRepositoryManager getInstance() {
        if (manager == null)
            manager = new SubjectRepositoryManager();

        return manager;
    }


    public Observable<Response<List<SubjectModel>>> getSubjects(String studentCatalogueId) {

        return getRetrofit().create(ISubjectService.class).getSubjectsList(studentCatalogueId, appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<List<ResourceModel>>> getResources(String studentCatalogueId) {

        return getRetrofit().create(ISubjectService.class).getResourcesList(studentCatalogueId, appSharedPreferences.getString(Constants.TOKEN));
    }


    public void updateCatalogueSubjects(List<SubjectModel> list, String catalogueId) {

        Observable.create(emitter -> {
            try {
                Gson gson = new Gson();
                Type type = new TypeToken<List<SubjectModel>>() {
                }.getType();
                getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.CATALOGUE_SUBJECTS + catalogueId, gson.toJson(list, type)));
                Logger.d("OFFLINE SUBJECTS", "STORED SUCCESSFULLY");
                emitter.onNext(true);
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }

        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io()).subscribe();

    }


    public Observable<List<SubjectModel>> getOfflineCatalogueSubjects(String catalogueId) {

        return Observable.create(emitter -> {
            try {

                // 1.2.3 MIGRATING APP SHARED PREFERENCES -> DB
                if (appSharedPreferences.getString(Constants.CATALOGUE_SUBJECTS + catalogueId) != null) {
                    getCacheUrlDAO().insertCacheUrl(new CacheUrlModel(Constants.CATALOGUE_SUBJECTS + catalogueId, appSharedPreferences.getString(Constants.CATALOGUE_SUBJECTS + catalogueId)));
                    appSharedPreferences.delete(Constants.CATALOGUE_SUBJECTS + catalogueId);
                }

                // DB INSERT OFFLINE CATALOGUES
                CacheUrlModel cacheUrlModel = getCacheUrlDAO().getCacheUrlByTag(Constants.CATALOGUE_SUBJECTS + catalogueId);
                if (cacheUrlModel == null) {
                    emitter.onError(new Exception());
                } else {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<SubjectModel>>() {
                    }.getType();
                    emitter.onNext(gson.fromJson(cacheUrlModel.getData(), type));
                    emitter.onComplete();
                }
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }

        });


    }


    public ICacheUrlDAO getCacheUrlDAO() {
        return getDeepBaseProvider().getCacheUrlDAO();
    }

    public IDownloadSubTopicDAO getDownloadSubTopicsDAO() {
        return getDeepBaseProvider().getDownloadSubTopicDAO();
    }

}
