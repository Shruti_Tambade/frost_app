package apprepos.lead;

import com.frost.leap.BuildConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import apprepos.BaseRepositoryManager;
import apprepos.lead.services.ILeadService;
import io.reactivex.Observable;
import retrofit2.Response;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao 13-07-2020.
 * <p>
 * Frost Interactive
 */
public class LeadRepositoryManager extends BaseRepositoryManager {

    private static LeadRepositoryManager manager;

    private LeadRepositoryManager() {

    }

    public static LeadRepositoryManager getInstance() {
        if (manager == null)
            manager = new LeadRepositoryManager();
        return manager;
    }

    public Observable<Response<JsonObject>> createLead(String email, String firstname, String lastname, String city, String phone) {

        JsonArray jsonArray = new JsonArray();

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("Attribute", "FirstName");
        jsonObject.addProperty("Value", firstname);
        jsonArray.add(jsonObject);

        jsonObject.addProperty("Attribute", "LastName");
        jsonObject.addProperty("Value", lastname);
        jsonArray.add(jsonObject);

        jsonObject.addProperty("Attribute", "Phone");
        jsonObject.addProperty("Value", phone);
        jsonArray.add(jsonObject);

        jsonObject.addProperty("Attribute", "mx_City");
        jsonObject.addProperty("Value", city);
        jsonArray.add(jsonObject);

        jsonObject.addProperty("Attribute", "EmailAddress");
        jsonObject.addProperty("Value", email);
        jsonArray.add(jsonObject);


        return getRetrofitByBaseUrl(BuildConfig.LEAD_SQUARED_BASE_URL).create(ILeadService.class).createLead(jsonArray,
                Constants.LeadSquared.LEAD_SQUARED_ACCESS_KEY,
                Constants.LeadSquared.LEAD_SQUARED_SECRET_KEY);
    }

    public Observable<Response<JsonArray>> getLeadByEmailID(String email) {

        return getRetrofitByBaseUrl(BuildConfig.LEAD_SQUARED_BASE_URL).create(ILeadService.class).getLeadByEmailID(
                Constants.LeadSquared.LEAD_SQUARED_ACCESS_KEY,
                Constants.LeadSquared.LEAD_SQUARED_SECRET_KEY,
                email);
    }

    public Observable<Response<JsonObject>> updateLead(String leadID) {

        JsonArray jsonArray = new JsonArray();

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("Attribute", "ProspectStage");
        jsonObject.addProperty("Value", "Student/Customer");
        jsonArray.add(jsonObject);


        return getRetrofitByBaseUrl(BuildConfig.LEAD_SQUARED_BASE_URL).create(ILeadService.class).updateLead(jsonArray,
                Constants.LeadSquared.LEAD_SQUARED_ACCESS_KEY,
                Constants.LeadSquared.LEAD_SQUARED_SECRET_KEY,
                leadID);
    }

}
