package apprepos.lead.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao 13-07-2020.
 * <p>
 * Frost Interactive
 */
public interface ILeadService {

    @POST(Constants.LeadSquared.CREATE_LEAD)
    Observable<Response<JsonObject>> createLead(@Body JsonArray jsonArray,
                                                @Query("accessKey") String accessKey,
                                                @Query("secretKey") String secretKey);

    @POST(Constants.LeadSquared.UPDATE_LEAD)
    Observable<Response<JsonObject>> updateLead(@Body JsonArray jsonArray, @Query("accessKey") String accessKey,
                                                @Query("secretKey") String secretKey,
                                                @Query("leadId") String leadId);

    @GET(Constants.LeadSquared.GET_LEAD_BY_EMAIL_ID)
    Observable<Response<JsonArray>> getLeadByEmailID(@Query("accessKey") String accessKey,
                                                      @Query("secretKey") String secretKey,
                                                      @Query("emailaddress") String emailaddress);


}
