package apprepos.lead.model;

import com.google.gson.annotations.SerializedName;

public class LeadModel {

    @SerializedName("ProspectID")
    private String prospectID;
    @SerializedName("ProspectAutoId")
    private String prospectAutoId;
    @SerializedName("FirstName")
    private String firstName;
    @SerializedName("LastName")
    private String lastName;
    @SerializedName("EmailAddress")
    private String emailAddress;
    @SerializedName("Company")
    private Object company;
    @SerializedName("Origin")
    private String origin;
    @SerializedName("Phone")
    private String phone;

    public String getProspectID() {
        return prospectID;
    }

    public void setProspectID(String prospectID) {
        this.prospectID = prospectID;
    }

    public String getProspectAutoId() {
        return prospectAutoId;
    }

    public void setProspectAutoId(String prospectAutoId) {
        this.prospectAutoId = prospectAutoId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Object getCompany() {
        return company;
    }

    public void setCompany(Object company) {
        this.company = company;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
