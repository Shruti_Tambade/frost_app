package apprepos.user;

import android.os.Build;
import android.text.TextUtils;

import com.frost.leap.BuildConfig;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import apprepos.BaseRepositoryManager;
import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.user.dao.ICacheUrlDAO;
import apprepos.user.model.AdditionalDetails;
import apprepos.user.model.Credentials;
import apprepos.user.model.Device;
import apprepos.user.model.Invoice;
import apprepos.user.model.LinkedDevice;
import apprepos.user.model.ProfileInfo;
import apprepos.user.model.SupportInfo;
import apprepos.user.model.User;
import apprepos.user.model.UserAgent;
import apprepos.user.services.IUserService;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 24-06-2019.
 * <p>
 * Frost
 */
public class UserRepositoryManager extends BaseRepositoryManager {

    private static UserRepositoryManager manager;

    private UserRepositoryManager() {

    }

    public static UserRepositoryManager getInstance() {
        if (manager == null)
            manager = new UserRepositoryManager();
        return manager;
    }

    public Observable<Response<JsonObject>> requestLogin(String mobileNumber) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("emailOrMobile", mobileNumber);
        //String signature = Utility.getContext().getResources().getString(R.string.app_signature);
        String signature = Utility.getAppSignature();

        return getRetrofit().create(IUserService.class).requestLogin(signature, jsonObject);
    }

    public Observable<Response<JsonObject>> verifyLoginOTP(String mobileNumber, String otpCode) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("emailOrMobile", mobileNumber);
        jsonObject.addProperty("otp", otpCode);

        Type UserAgentType = new TypeToken<UserAgent>() {
        }.getType();

        return getRetrofit().create(IUserService.class).verifyLoginOTP(jsonObject,
                "MOBILE",
                new Gson().toJson(new UserAgent(), UserAgentType),
                Utility.getIpAddress());
    }

    public Observable<Response<JsonObject>> checkUserExist(String email, String phoneNumber) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("mobile", phoneNumber);
        jsonObject.addProperty("userName", email);
//        String signature = Utility.getContext().getResources().getString(R.string.app_signature);
        String signature = Utility.getAppSignature();

        return getRetrofit().create(IUserService.class).checkUserExist(signature, jsonObject);
    }

    public Observable<Response<JsonObject>> registerUser(User user, String otp) {
        JsonObject jsonObject = new JsonObject();
        JsonObject jsonObjectProfile = new JsonObject();
        jsonObjectProfile.addProperty("firstName", user.getProfileInfo().getFirstName());
        jsonObjectProfile.addProperty("lastName", user.getProfileInfo().getLastName());
        jsonObjectProfile.addProperty("theme", "LIGHT");

        jsonObject.add("userProfile", jsonObjectProfile);
        jsonObject.addProperty("countryCode", "91");
        jsonObject.addProperty("mobile", user.getCredentials().getMobile());
        jsonObject.addProperty("email", user.getCredentials().getEmail());
        jsonObject.addProperty("mobileNumberVerified", true);
        jsonObject.addProperty("password", user.getCredentials().getPassword());

        Type UserAgentType = new TypeToken<UserAgent>() {
        }.getType();

        return getRetrofit().create(IUserService.class).registerUser(jsonObject,
                new Gson().toJson(new UserAgent(), UserAgentType),
                Utility.getIpAddress(), otp,
                "MOBILE");
    }

    public Observable<Response<JsonObject>> requestLostPhone(String email) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("emailOrMobile", email);
        jsonObject.addProperty("platform", Constants.PLATFORM);

        return getRetrofit().create(IUserService.class).requestLostPhone(jsonObject);
    }

    public Observable<Response<JsonObject>> verifyEmailOtp(String email, String otpCode) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("emailOrMobile", email);
        jsonObject.addProperty("otp", otpCode);

        return getRetrofit().create(IUserService.class).verifyEmailOtp(jsonObject);
    }

    public Observable<Response<JsonObject>> updateAlternativeNumber(String email, String phoneNumber) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("mobile", phoneNumber);

        return getRetrofit().create(IUserService.class).updateAlternativeNumber(jsonObject);
    }

    public Observable<Response<JsonObject>> verifyAlternativeNumberOTP(String phoneNumber, String userId, String otpCode) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("mobile", phoneNumber);
        jsonObject.addProperty("userId", userId);
        jsonObject.addProperty("otp", otpCode);

        return getRetrofit().create(IUserService.class).verifyAlternativeNumberOTP(jsonObject);
    }

    public Observable<Response<JsonObject>> refreshToken() {

        Type UserAgentType = new TypeToken<UserAgent>() {
        }.getType();


        return getRetrofit().create(IUserService.class).refreshToken(
                "MOBILE",
                new Gson().toJson(new UserAgent(), UserAgentType),
                Utility.getIpAddress(),
                appSharedPreferences.getString(Constants.TOKEN));
    }

    public Observable<Response<User>> getUserDetails() {
        JsonObject userInfo = new JsonObject();
        userInfo.addProperty("userId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));

        return getRetrofit().create(IUserService.class).getUserDetails(
                appSharedPreferences.getString(Constants.TOKEN), userInfo
                , appSharedPreferences.getString(Constants.UserInfo.USER_ID));
    }

    public Observable<Response<JsonObject>> updateUserDetails(User user) {
        return getRetrofit().create(IUserService.class).updateUserDetails(appSharedPreferences.getString(Constants.TOKEN), user.getProfileInfo());
    }

    public Observable<Response<JsonObject>> pushProfilePicToS3(GalleryFile galleryFile) {

        HashMap<String, RequestBody> data = new HashMap<>();
        RequestBody studentId = RequestBody.create(MediaType.parse("text/plain"),
                appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        data.put("studentId", studentId);
        File file = Utility.getCompressedFileFromFile(galleryFile.uri, galleryFile.mediaType, Double.parseDouble(galleryFile.size != null ? galleryFile.size : Constants.SAMPLE_FILE_SIZE));
        if (file == null) {
            return Observable.error(new FileNotFoundException());
        }
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("photo", file.getName(), reqFile);

        return getRetrofit().create(IUserService.class).uploadProfilePicToS3(
                data, image,
                appSharedPreferences.getString(Constants.TOKEN)
        );
    }

    public Observable<Response<JsonObject>> updateUserPassword(User user, String oldPassword, String password) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", user.getCredentials().getEmail());
        jsonObject.addProperty("oldPassword", oldPassword);
        jsonObject.addProperty("password", password);
        return getRetrofit().create(IUserService.class).updatePassword(appSharedPreferences.getString(Constants.TOKEN),
                jsonObject);
    }

    public Observable<Response<JsonObject>> updateUserMobile(String mobileNumber, String password) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        jsonObject.addProperty("mobile", mobileNumber);
        jsonObject.addProperty("password", password);
        return getRetrofit().create(IUserService.class).updateMobile(appSharedPreferences.getString(Constants.TOKEN),
                jsonObject);
    }

    //UpdateRating
    public Observable<Response<RatingModel>> updateRatingDetails(RatingModel ratingModel) {
        ratingModel.setStudentId(appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        return getRetrofit().create(IUserService.class).updateRatingDetails(appSharedPreferences.getString(Constants.TOKEN), ratingModel);
    }

    public Observable<Response<JsonObject>> verifyOTP(String mobileNumber, String otpCode) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));
        jsonObject.addProperty("mobile", mobileNumber);
        jsonObject.addProperty("otp", otpCode);
        return getRetrofit().create(IUserService.class).verifyOTP(appSharedPreferences.getString(Constants.TOKEN),
                jsonObject);
    }

    public Observable<Response<List<Invoice>>> getUserInvoices() {
        return getRetrofit().create(IUserService.class).getUserInvoices(
                appSharedPreferences.getString(Constants.TOKEN), appSharedPreferences.getString(Constants.UserInfo.USER_ID));
    }

    public Observable<Response<List<LinkedDevice>>> getUserLinkedDevices() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));

        return getRetrofit().create(IUserService.class).getUserLinkedDevices(appSharedPreferences.getString(Constants.TOKEN), jsonObject);
    }

    public Observable<Response<SupportInfo>> getSupportInfo() {
        return getRetrofit().create(IUserService.class).getSupportInfo();
    }

    public Observable<Response<JsonObject>> addFreshDeskTicket(String issueType, String messageDescription, String course, List<GalleryFile> list, MenuItem menuItem) {

        HashMap<String, RequestBody> data = new HashMap<>();

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), appSharedPreferences.getString(Constants.UserInfo.FIRST_NAME) + " " + appSharedPreferences.getString(Constants.UserInfo.LAST_NAME));
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), appSharedPreferences.getString(Constants.UserInfo.MOBILE_NUMBER));
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), appSharedPreferences.getString(Constants.UserInfo.EMAIL));
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), messageDescription);
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), issueType);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), "2");
        RequestBody priority = RequestBody.create(MediaType.parse("text/plain"), "1");

        if (course != null && !course.equals("Choose Course (optional)") && !course.equals("Choose Course")) {
            RequestBody courseBody = RequestBody.create(MediaType.parse("text/plain"), course);
            data.put("custom_fields[cf_course]", courseBody);
        }

        if (menuItem == null) {
            data.put("type", type);
            data.put("description", description);
        } else {

            data.put("type", type);
            data.put("description", description);

            RequestBody issueTopic = RequestBody.create(MediaType.parse("text/plain"), menuItem.getIssueTopic());
            data.put("custom_fields[cf_topic]", issueTopic);

            RequestBody systemMessage = RequestBody.create(MediaType.parse("text/plain"), menuItem.getContactMessage());
            data.put("custom_fields[cf_system_generated_message]", systemMessage);

        }

        data.put("name", name);
        data.put("phone", phone);
        data.put("email", email);
        data.put("subject", type);
//        data.put("type", type);
//        data.put("description", description);
        data.put("status", status);
        data.put("priority", priority);
        RequestBody phoneModelAndroidVersion = RequestBody.create(MediaType.parse("text/plain"), "Android");
        data.put("custom_fields[cf_ticket_source]", phoneModelAndroidVersion);


        if (list != null && list.size() > 0) {
            List<MultipartBody.Part> attachments = new ArrayList<>();
            for (GalleryFile galleryFile : list) {
                File file = Utility.getCompressedFileFromFile(galleryFile.uri, galleryFile.mediaType, Double.parseDouble(galleryFile.size != null ? galleryFile.size : Constants.SAMPLE_FILE_SIZE));
                if (file == null) {
                    return Observable.error(new FileNotFoundException());
                }
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                MultipartBody.Part image = MultipartBody.Part.createFormData("attachments[]", file.getName(), reqFile);
                attachments.add(image);
            }
            return getRetrofitByBaseUrl(Constants.FRESH_DESK_URL).create(IUserService.class)
                    .addFreshTicketWithAttachments(Constants.FRESH_DESK_AUTH_KEY, data, attachments);
        } else
            return getRetrofitByBaseUrl(Constants.FRESH_DESK_URL).create(IUserService.class)
                    .addFreshTicket(Constants.FRESH_DESK_AUTH_KEY, data);
    }

    public Observable<Response<JsonObject>> askAnExpert(String issueType, String messageDescription, String subjectInfo, List<GalleryFile> list, String[] tags) {

        HashMap<String, RequestBody> data = new HashMap<>();


        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), appSharedPreferences.getString(Constants.UserInfo.FIRST_NAME) + " " + appSharedPreferences.getString(Constants.UserInfo.LAST_NAME));
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), appSharedPreferences.getString(Constants.UserInfo.MOBILE_NUMBER));
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), appSharedPreferences.getString(Constants.UserInfo.EMAIL));
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), messageDescription);
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), issueType);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), "2");
        RequestBody priority = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody subject = RequestBody.create(MediaType.parse("text/plain"), subjectInfo == null ? "" : subjectInfo);
        RequestBody phoneModelAndroidVersion = RequestBody.create(MediaType.parse("text/plain"), "Android");

        List<RequestBody> tagRequestBodies = new ArrayList<>();
        if (tags != null) {
            for (int i = 0; i < tags.length; i++) {
                tagRequestBodies.add(RequestBody.create(MediaType.parse("text/plain"), tags[i]));
            }
        }


        data.put("name", name);
        data.put("phone", phone);
        data.put("email", email);
        data.put("subject", subject);
        data.put("description", description);
        data.put("type", type);
        data.put("status", status);
        data.put("priority", priority);
        data.put("custom_fields[cf_ticket_source]", phoneModelAndroidVersion);


        if (list != null && list.size() > 0) {
            List<MultipartBody.Part> attachments = new ArrayList<>();
            for (GalleryFile galleryFile : list) {
                File file = Utility.getCompressedFileFromFile(galleryFile.uri, galleryFile.mediaType, Double.parseDouble(galleryFile.size != null ? galleryFile.size : Constants.SAMPLE_FILE_SIZE));
                if (file == null) {
                    return Observable.error(new FileNotFoundException());
                }
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                MultipartBody.Part image = MultipartBody.Part.createFormData("attachments[]", file.getName(), reqFile);
                attachments.add(image);
            }
            return getRetrofitByBaseUrl(Constants.FRESH_DESK_URL).create(IUserService.class).askAnExpertWithAttachments(Constants.FRESH_DESK_AUTH_KEY, data, tagRequestBodies, attachments);
        } else
            return getRetrofitByBaseUrl(Constants.FRESH_DESK_URL).create(IUserService.class).askAnExpert(Constants.FRESH_DESK_AUTH_KEY, data, tagRequestBodies);
    }

    public Observable<Response<ResponseBody>> doLogout(String token) {
        return getRetrofit().create(IUserService.class).doLogout(token);
    }

    public Observable<Response<JsonObject>> updateDeviceDetails() {

        JsonObject body = new JsonObject();
        body.addProperty("appVersionCode", BuildConfig.VERSION_CODE);
        body.addProperty("appVersionName", BuildConfig.VERSION_NAME);
        body.addProperty("studentId", appSharedPreferences.getString(Constants.UserInfo.USER_ID));

        Gson gson = new Gson();
        Type type = new TypeToken<Device>() {
        }.getType();
        body.add("deviceInfoDTO", gson.fromJson(gson.toJson(new Device(), type), JsonElement.class));

        return getRetrofit().create(IUserService.class).updateDeviceDetails(appSharedPreferences.getString(Constants.TOKEN), body);
    }


    public Observable<Response<JsonObject>> pushImageToS3(GalleryFile galleryFile) {

        File file = Utility.getCompressedFileFromFile(galleryFile.uri, galleryFile.mediaType, Double.parseDouble(galleryFile.size != null ? galleryFile.size : Constants.SAMPLE_FILE_SIZE));
        if (file == null) {
            return Observable.error(new FileNotFoundException());
        }
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("photo", file.getName(), reqFile);

        return getRetrofit().create(IUserService.class).uploadImageToS3(
                image,
                appSharedPreferences.getString(Constants.TOKEN)
        );
    }


    public Observable<Response<JsonObject>> updateOTSDetails(JsonObject jsonObject) {
        return getRetrofit().create(IUserService.class).updateOTSDetails(
                appSharedPreferences.getString(Constants.TOKEN), jsonObject
        );
    }


    public Observable<Response<AdditionalDetails>> getAdditionalDetails() {
        return getRetrofit().create(IUserService.class).getAdditionalDetails(appSharedPreferences.getString(Constants.TOKEN),
                appSharedPreferences.getString(Constants.UserInfo.USER_ID));
    }

    public Observable<Response<JsonObject>> updateAdditionalDetails(AdditionalDetails additionalDetails) {

        if (additionalDetails.getStudentId() == null)
            additionalDetails.setStudentId(appSharedPreferences.getString(Constants.UserInfo.USER_ID));

        if (additionalDetails.getStudentPhoto() == null)
            additionalDetails.setStudentPhoto(appSharedPreferences.getString(Constants.UserInfo.PROFILE_PIC) == null
                    ? Constants.AVATAR_IMAGE : appSharedPreferences.getString(Constants.UserInfo.PROFILE_PIC));

        return getRetrofit().create(IUserService.class).updateAdditionalDetails(appSharedPreferences.getString(Constants.TOKEN),
                additionalDetails);
    }


    public void saveUserInfo(String token, JsonObject jsonObject) {
        appSharedPreferences.setString(Constants.TOKEN, token);
        if (jsonObject == null)
            return;

        appSharedPreferences.setString(Constants.UserInfo.USER_ID, jsonObject.get("userId").getAsString());
        appSharedPreferences.setString(Constants.UserInfo.USER_THEME, jsonObject.get("theme") == null ? "LIGHT" : jsonObject.get("theme").getAsString());
    }

    public void updateUserNumber(String mobileNumber) {
        appSharedPreferences.setString(Constants.UserInfo.MOBILE_NUMBER, mobileNumber);
    }

    public void saveUserDetails(User user) {
        Gson gson = new Gson();
        Type type = new TypeToken<User>() {
        }.getType();

        appSharedPreferences.setString(Constants.UserInfo.USER_DATA, gson.toJson(user, type));
        appSharedPreferences.setString(Constants.UserInfo.USER_ID, user.getStudentId());
        if (user.getProfileInfo() != null) {
            appSharedPreferences.setString(Constants.UserInfo.FIRST_NAME, user.getProfileInfo().getFirstName());
            appSharedPreferences.setString(Constants.UserInfo.LAST_NAME, user.getProfileInfo().getLastName());
            appSharedPreferences.setString(Constants.UserInfo.DATE_OF_BIRTH, user.getProfileInfo().getDateOfBirth());
            appSharedPreferences.setString(Constants.UserInfo.GENDER, user.getProfileInfo().getGender());
            appSharedPreferences.setString(Constants.UserInfo.PROFILE_PIC, user.getProfileInfo().getImage());
        }
        if (user.getCredentials() != null) {
            appSharedPreferences.setString(Constants.UserInfo.COUNTRY_CODE, user.getCredentials().getCountryCode());
            appSharedPreferences.setString(Constants.UserInfo.MOBILE_NUMBER, user.getCredentials().getMobile());
            appSharedPreferences.setString(Constants.UserInfo.EMAIL, user.getCredentials().getEmail());
        }

        appSharedPreferences.setBoolean(Constants.IS_LOGIN, true);
    }

    public boolean isLogin() {
        return appSharedPreferences.getBoolean(Constants.IS_LOGIN);
    }

    public User getUser() {
        User user = new User();

        ProfileInfo profileInfo = new ProfileInfo();
        profileInfo.setFirstName(appSharedPreferences.getString(Constants.UserInfo.FIRST_NAME));
        profileInfo.setLastName(appSharedPreferences.getString(Constants.UserInfo.LAST_NAME));
        profileInfo.setDateOfBirth(appSharedPreferences.getString(Constants.UserInfo.DATE_OF_BIRTH));
        profileInfo.setGender(appSharedPreferences.getString(Constants.UserInfo.GENDER));
        profileInfo.setImage(appSharedPreferences.getString(Constants.UserInfo.PROFILE_PIC));


        Credentials credentials = new Credentials();
        credentials.setCountryCode(appSharedPreferences.getString(Constants.UserInfo.COUNTRY_CODE));
        credentials.setMobile(appSharedPreferences.getString(Constants.UserInfo.MOBILE_NUMBER));
        credentials.setEmail(appSharedPreferences.getString(Constants.UserInfo.EMAIL));
        user.setStudentId(appSharedPreferences.getString(Constants.UserInfo.USER_ID));

        user.setProfileInfo(profileInfo);
        user.setCredentials(credentials);


        return user;
    }

    public void clearLoginDetails() {
        appSharedPreferences.delete(Constants.IS_LOGIN);
    }

    public void deleteUserData() {
        appSharedPreferences.deleteSharedPreferences();
    }

    public void deleteAllCacheUrls() {
        Observable<Boolean> observable = Observable.create(emitter -> {
            try {
                ICacheUrlDAO cacheUrlDAO = getDeepBaseProvider().getCacheUrlDAO();
                cacheUrlDAO.removeAllCacheUrls();
                emitter.onNext(true);
                emitter.onComplete();

            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }
        });

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean)
                            Logger.d("Removed all cache url");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public String getUserToken() {
        return appSharedPreferences.getString(Constants.TOKEN);
    }

    public String getFcmToken() {
        return appSharedPreferences.getString(Constants.USER_FCM_TOKEN);
    }

    public void setFcmToken(String token) {
        appSharedPreferences.setString(Constants.USER_FCM_TOKEN, token);
    }

    public void setOtpWordCount(String otpWordCount) {
        try {
            appSharedPreferences.setInt(Constants.OTP_WORD_COUNT, Integer.parseInt(otpWordCount));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public int getOtpWordCount() {
        return appSharedPreferences.getInt(Constants.OTP_WORD_COUNT) == 0 ? -1 : appSharedPreferences.getInt(Constants.OTP_WORD_COUNT);
    }


    public boolean getAskMeEveryTime() {
        return appSharedPreferences.contains(Constants.ASK_ME_EVERYTIME) ?
                appSharedPreferences.getBoolean(Constants.ASK_ME_EVERYTIME) : true;
    }

    public void setAskMeEveryTime(boolean value) {
        appSharedPreferences.setBoolean(Constants.ASK_ME_EVERYTIME, value);
    }

    public int getDownloadResolution() {
        return appSharedPreferences.getInt(Constants.DOWNLOAD_RESOLUTION_QUALITY);
    }


    public void setDownloadResolution(int quality) {
        appSharedPreferences.setInt(Constants.DOWNLOAD_RESOLUTION_QUALITY, quality);
    }


    public boolean checkDRMTags() {
        return appSharedPreferences.contains(Constants.DRM_LICENSE_TAG);
    }

    public void saveDRMTagsFromLocal() {
        appSharedPreferences.setString(Constants.DEEP_LEARN_RSA_PUBLIC_KEY, Constants.RSA_PUBLIC_KEY);
        appSharedPreferences.setString(Constants.DRM_LICENSE_TAG, Constants.VideoConstants.DRM_LICENSE_TAG);
        appSharedPreferences.setString(Constants.DRM_OFFLINE_TAG, Constants.VideoConstants.DRM_OFFLINE_TAG);

    }

    public void saveHlsVideos(String data) {
        if (TextUtils.isEmpty(data)) {
            return;
        }

        appSharedPreferences.setString(Constants.HLS_VIDEOS, data);
    }

    public void saveAutoResolutions(String autoResolutions) {
        if (TextUtils.isEmpty(autoResolutions)) {
            return;
        }
        appSharedPreferences.setString(Constants.AUTO_RESOLUTIONS, autoResolutions);
    }

    public void savePhoneSupport(String phoneSupport) {
        if (TextUtils.isEmpty(phoneSupport)) {
            return;
        }
        appSharedPreferences.setString(Constants.PHONE_SUPPORT, phoneSupport);
    }

    public void saveCredentials(String credentials) {
        if (TextUtils.isEmpty(credentials)) {
            return;
        }
        appSharedPreferences.setString(Constants.CREDENTIALS, credentials);
    }


    public List<MenuItem> getPhoneSupportMenu() {
        if (!appSharedPreferences.contains(Constants.PHONE_SUPPORT))
            return null;

        try {

            Gson gson = new Gson();
            Type type = new TypeToken<List<MenuItem>>() {
            }.getType();

            return gson.fromJson(appSharedPreferences.getString(Constants.PHONE_SUPPORT), type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getCredentials(String type) {
        if (appSharedPreferences.contains(Constants.CREDENTIALS)) {
            JsonObject jsonObject = new JsonParser().parse(appSharedPreferences.getString(Constants.CREDENTIALS)).getAsJsonObject();
            if (jsonObject != null) {
                for (String key : jsonObject.keySet()) {
                    return type.equals("key") ? key : jsonObject.get(key).getAsString();
                }
            }
        }
        return "";
    }

    public void storeUpdateDetails(String androidUpdateVersionDetails) {
        if (TextUtils.isEmpty(androidUpdateVersionDetails)) {
            return;
        }
        appSharedPreferences.setString(Constants.APP_UPDATE_VERSION_DETAILS, androidUpdateVersionDetails);
    }

    public void saveStoreBanner(String bannerDetails) {
        if (TextUtils.isEmpty(bannerDetails)) {
            return;
        }

        appSharedPreferences.setString(Constants.APP_STORE_BANNER, bannerDetails);
    }

    public void saveRSAPublicKey(String rsaPublicKey) {
        if (TextUtils.isEmpty(rsaPublicKey)) {
            return;
        }
        appSharedPreferences.setString(Constants.DEEP_LEARN_RSA_PUBLIC_KEY, rsaPublicKey);
    }

    public void allowLicenseCache(String allowLicenseCache) {
        if (TextUtils.isEmpty(allowLicenseCache)) {
            return;
        }
        try {
            int value = Integer.parseInt(allowLicenseCache);
            appSharedPreferences.setInt(Constants.ALLOW_LICENSE_CACHE, value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }


    public void setSecurityLevel(String level) {
        if (level == null) {
            return;
        }
        appSharedPreferences.setString(Constants.DRM_SECURITY_LEVEL, level);
    }

    public void setForceSecurityLevel(String forceSecurityLevel) {
        if (TextUtils.isEmpty(forceSecurityLevel))
            return;

        JsonObject jsonObject = new JsonParser().parse(forceSecurityLevel).getAsJsonObject();

        String key = null;
        for (String keys : jsonObject.keySet()) {
            if (keys.equalsIgnoreCase(Build.BRAND)) {
                key = keys;
            }
        }

        if (key != null) {
            JsonObject jsonDevice = jsonObject.getAsJsonObject(key);
            if (jsonDevice != null) {
                JsonArray jsonModels = jsonDevice.get("models").getAsJsonArray();
                for (int i = 0; i < jsonModels.size(); i++) {
                    if (jsonModels.get(i).getAsString().equalsIgnoreCase(Build.MODEL)) {
                        setSecurityLevel(jsonDevice.get("level").getAsString());
                        return;
                    }
                }

            }
        }

        if (jsonObject.has("all")) {
            if (jsonObject.get("all").getAsBoolean()) {
                setSecurityLevel("");
            }
        }

    }

    public int getVideoDownloadLocation() {

        //1 = Primary (Internal Storage)
        //2 = Secondary (External Storage) (Sd-Card)
        return appSharedPreferences.getInt(Constants.DOWNLOAD_LOCATION);
    }
}
