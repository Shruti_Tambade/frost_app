package apprepos.user.services;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.user.model.AdditionalDetails;
import apprepos.user.model.Invoice;
import apprepos.user.model.LinkedDevice;
import apprepos.user.model.ProfileInfo;
import apprepos.user.model.SupportInfo;
import apprepos.user.model.User;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import supporters.constants.Constants;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 24-06-2019.
 * <p>
 * Frost
 */
public interface IUserService {


    @POST(Constants.UserConstants.REQUEST_LOGIN_URL)
    public Observable<Response<JsonObject>> requestLogin(@Header("mobileToken") String signature,
                                                         @Body JsonObject jsonObject);

    @POST(Constants.UserConstants.VERIFY_LOGIN_OTP_URL)
    public Observable<Response<JsonObject>> verifyLoginOTP(@Body JsonObject jsonObject,
                                                           @Header("X-Device-For") String deviceType,
                                                           @Header("User-Agent") String useragent,
                                                           @Header("browserip") String ipAddress);


    @POST(Constants.UserConstants.CHECK_USER_EXIST_URL)
    public Observable<Response<JsonObject>> checkUserExist(@Header("mobileToken") String signature,
                                                           @Body JsonObject jsonObject);

    @POST(Constants.UserConstants.REGISTER_USER_URL)
    public Observable<Response<JsonObject>> registerUser(@Body JsonObject jsonObject,
                                                         @Header("User-Agent") String useragent,
                                                         @Header("browserip") String ipAddress,
                                                         @Header("otp") String otp,
                                                         @Header("X-Device-For") String deviceType);


    @POST(Constants.UserConstants.REQUEST_LOST_PHONE_URL)
    public Observable<Response<JsonObject>> requestLostPhone(@Body JsonObject jsonObject);


    @POST(Constants.UserConstants.VERIFY_EMAIL_OTP_URL)
    public Observable<Response<JsonObject>> verifyEmailOtp(@Body JsonObject jsonObject);


    @PUT(Constants.UserConstants.ALTERNATIVE_MOBILE_URL)
    public Observable<Response<JsonObject>> updateAlternativeNumber(@Body JsonObject jsonObject);


    @POST(Constants.UserConstants.VERIFY_OTP_ALTERNATIVE_MOBILE_URL)
    public Observable<Response<JsonObject>> verifyAlternativeNumberOTP(@Body JsonObject jsonObject);


    @GET(Constants.UserConstants.GET_USER_DETAILS_URL + "{studentId}")
    public Observable<Response<User>> getUserDetails(@Header("authorization") String authorization,
                                                     @Header("userinfo") JsonObject userInfo,
                                                     @Path("studentId") String studentId);


    @PUT(Constants.UserConstants.UPDATE_USER_DETAILS)
    public Observable<Response<JsonObject>> updateUserDetails(@Header("authorization") String authorization,
                                                              @Body ProfileInfo profileInfo);


    @Multipart
    @POST(Constants.UserConstants.UPLOAD_PROFILE_PIC_S3_URL)
    Observable<Response<JsonObject>> uploadProfilePicToS3(@PartMap() Map<String, RequestBody> partMap,
                                                          @Part MultipartBody.Part image,
                                                          @Header("authorization") String authorization);


    @POST(Constants.UserConstants.UPDATE_PASSWORD_URL)
    Observable<Response<JsonObject>> updatePassword(@Header("authorization") String authorization,
                                                    @Body JsonObject jsonObject);


    @POST(Constants.UserConstants.UPDATE_MOBILE_URL)
    Observable<Response<JsonObject>> updateMobile(@Header("authorization") String authorization,
                                                  @Body JsonObject jsonObject);


    @POST(Constants.UserConstants.VERIFY_OTP_URL)
    Observable<Response<JsonObject>> verifyOTP(@Header("authorization") String authorization,
                                               @Body JsonObject jsonObject);


    @GET(Constants.UserConstants.GET_USER_BILLING_HISTORY_URL + "{studentId}")
    public Observable<Response<List<Invoice>>> getUserInvoices(@Header("authorization") String authorization,
                                                               @Path("studentId") String studentId);


    @GET(Constants.UserConstants.GET_USER_LINK_DEVICES_URL)
    public Observable<Response<List<LinkedDevice>>> getUserLinkedDevices(@Header("authorization") String authorization, @Header("userinfo") JsonObject userInfo);


    @POST(Constants.UserConstants.REFRESH_TOKEN_URL)
    Observable<Response<JsonObject>> refreshToken(@Header("X-Device-For") String deviceType,
                                                  @Header("User-Agent") String useragent,
                                                  @Header("browserip") String browserIp,
                                                  @Header("authorization") String authorization);


    @GET(Constants.UserConstants.SUPPORT_INFO_URL)
    Observable<Response<SupportInfo>> getSupportInfo();


    @Multipart
    @POST(Constants.UserConstants.ASK_AN_EXPERT_URL)
    Observable<Response<JsonObject>> askAnExpert(@Header("Authorization") String authorization,
                                                 @PartMap() Map<String, RequestBody> partMap,
                                                 @Part("tags[]") List<RequestBody> tags);


    @Multipart
    @POST(Constants.UserConstants.ASK_AN_EXPERT_URL)
    Observable<Response<JsonObject>> askAnExpertWithAttachments(@Header("Authorization") String authorization,
                                                                @PartMap() Map<String, RequestBody> partMap,
                                                                @Part("tags[]") List<RequestBody> tags,
                                                                @Part List<MultipartBody.Part> multipleAttachments);


    @Multipart
    @POST(Constants.UserConstants.ADD_FRESH_DESK_TICKET)
    Observable<Response<JsonObject>> addFreshTicket(@Header("Authorization") String authorization,
                                                    @PartMap() HashMap<String, RequestBody> partMap);


    @Multipart
    @POST(Constants.UserConstants.ADD_FRESH_DESK_TICKET)
    Observable<Response<JsonObject>> addFreshTicketWithAttachments(@Header("Authorization") String authorization,
                                                                   @PartMap() HashMap<String, RequestBody> partMap,
                                                                   @Part List<MultipartBody.Part> multipleAttachments);


    @POST(Constants.UserConstants.USER_LOGOUT_URL)
    Observable<Response<ResponseBody>> doLogout(@Header("authorization") String authorization);

    @POST(Constants.CatalogueConstants.POST_SUBTOPIC_RATING)
    Observable<Response<RatingModel>> updateRatingDetails(@Header("authorization") String authorization,
                                                          @Body RatingModel ratingModel);

    @POST(Constants.UserConstants.UPDATE_DEVICE_DETAILS_URL)
    Observable<Response<JsonObject>> updateDeviceDetails(@Header("authorization") String authorization,
                                                         @Body JsonObject jsonObject);

    @Multipart
    @PUT(Constants.UserConstants.UPDATE_USER_INFO_IMAGE)
    Observable<Response<JsonObject>> uploadImageToS3(@Part MultipartBody.Part image,
                                                     @Header("authorization") String authorization);


    @PUT(Constants.UserConstants.UPDATE_OTS_DETAILS)
    Observable<Response<JsonObject>> updateOTSDetails(@Header("authorization") String authorization,
                                                      @Body JsonObject jsonObject);


    @GET(Constants.UserConstants.GET_ADDITIONAL_DETAILS + "{studentId}")
    Observable<Response<AdditionalDetails>> getAdditionalDetails(@Header("authorization") String authorization,
                                                                 @Path("studentId") String studentId);

    @PUT(Constants.UserConstants.UPDATE_ADDITIONAL_DETAILS)
    Observable<Response<JsonObject>> updateAdditionalDetails(@Header("authorization") String authorization,
                                                             @Body AdditionalDetails additionalDetails);





}
