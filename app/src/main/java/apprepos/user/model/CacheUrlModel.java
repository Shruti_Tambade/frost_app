package apprepos.user.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 10-08-2020.
 * <p>
 * Frost
 */
@Entity(tableName = "cache_url")
public class CacheUrlModel {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "tag")
    private String tag;

    @NonNull
    @ColumnInfo(name = "data")
    private String data;

    public CacheUrlModel(@NonNull String tag, @NonNull String data) {
        this.tag = tag;
        this.data = data;
    }

    @NonNull
    public String getTag() {
        return tag;
    }

    public void setTag(@NonNull String tag) {
        this.tag = tag;
    }

    @NonNull
    public String getData() {
        return data;
    }

    public void setData(@NonNull String data) {
        this.data = data;
    }
}
