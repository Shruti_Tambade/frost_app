package apprepos.user.model;

import android.os.Build;

import com.frost.leap.BuildConfig;

import apprepos.user.UserRepositoryManager;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 25-03-2019.
 * <p>
 * Frost
 */
public class Device {


    private String deviceId = Utility.getPhoneUniqueId(Utility.getContext());

    private String appVersionName = BuildConfig.VERSION_NAME;

    private int appVersionCode = BuildConfig.VERSION_CODE;

    private String os = "Android";

    private String deviceType = Utility.isTablet() ? "Tablet" : "Phone";

    private String manufacturer = Build.MANUFACTURER;

    private String brand = Build.BRAND;

    private String model = Build.MODEL;

    private int sdk = Build.VERSION.SDK_INT;

    private String buildVersion = Build.VERSION.RELEASE;

    private String board = Build.BOARD;

    private String hardware = Build.HARDWARE;

    private String bootLoader = Build.BOOTLOADER;

    private String fingerprint = Build.FINGERPRINT;

    private String fcmToken =  UserRepositoryManager.getInstance().getFcmToken();

    private int height = Utility.getScreenHeight(Utility.getContext());

    private int width = Utility.getScreenWidth(Utility.getContext());

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAppVersionName() {
        return appVersionName;
    }

    public void setAppVersionName(String appVersionName) {
        this.appVersionName = appVersionName;
    }

    public int getAppVersionCode() {
        return appVersionCode;
    }

    public void setAppVersionCode(int appVersionCode) {
        this.appVersionCode = appVersionCode;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSdk() {
        return sdk;
    }

    public void setSdk(int sdk) {
        this.sdk = sdk;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getBootLoader() {
        return bootLoader;
    }

    public void setBootLoader(String bootLoader) {
        this.bootLoader = bootLoader;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
