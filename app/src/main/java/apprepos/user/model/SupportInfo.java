package apprepos.user.model;

import java.util.List;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 01-10-2019.
 * <p>
 * FROST
 */
public class SupportInfo
{
    private List<String> issueType;
    private List<String> chooseCourse;
    private List<String> issueTopic;

    public List<String> getIssueType() {
        return issueType;
    }

    public void setIssueType(List<String> issueType) {
        this.issueType = issueType;
    }

    public List<String> getChooseCourse() {
        return chooseCourse;
    }

    public void setChooseCourse(List<String> chooseCourse) {
        this.chooseCourse = chooseCourse;
    }

    public List<String> getIssueTopic() {
        return issueTopic;
    }

    public void setIssueTopic(List<String> issueTopic) {
        this.issueTopic = issueTopic;
    }
}
