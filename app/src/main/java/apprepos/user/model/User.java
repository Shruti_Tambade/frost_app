package apprepos.user.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 24-06-2019.
 * <p>
 * Frost
 */
public class User implements Parcelable
{
  private String studentId;
  private Credentials credentials;
  private ProfileInfo profileInfo;


    public User(){

    }


    protected User(Parcel in) {
        studentId = in.readString();
        credentials = in.readParcelable(Credentials.class.getClassLoader());
        profileInfo = in.readParcelable(ProfileInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(studentId);
        dest.writeParcelable(credentials, flags);
        dest.writeParcelable(profileInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public ProfileInfo getProfileInfo() {
        return profileInfo;
    }

    public void setProfileInfo(ProfileInfo profileInfo) {
        this.profileInfo = profileInfo;
    }
}
