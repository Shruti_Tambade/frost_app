package apprepos.user.model;

import android.os.Parcel;
import android.os.Parcelable;

public class AdditionalDetails implements Parcelable
{
    private String studentId;
    private String studentPhoto;
    private String fatherName;
    private String employmentStatus;
    private String companyName;
    private String highestQualification;
    private String collegeName;
    private String universityName;
    private String currentGoal;
    private boolean deliveryInCollege;
    private String proofFront;
    private String proofBack;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentPhoto() {
        return studentPhoto;
    }

    public void setStudentPhoto(String studentPhoto) {
        this.studentPhoto = studentPhoto;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getHighestQualification() {
        return highestQualification;
    }

    public void setHighestQualification(String highestQualification) {
        this.highestQualification = highestQualification;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getCurrentGoal() {
        return currentGoal;
    }

    public void setCurrentGoal(String currentGoal) {
        this.currentGoal = currentGoal;
    }

    public boolean isDeliveryInCollege() {
        return deliveryInCollege;
    }

    public void setDeliveryInCollege(boolean deliveryInCollege) {
        this.deliveryInCollege = deliveryInCollege;
    }

    public String getProofFront() {
        return proofFront;
    }

    public void setProofFront(String proofFront) {
        this.proofFront = proofFront;
    }

    public String getProofBack() {
        return proofBack;
    }

    public void setProofBack(String proofBack) {
        this.proofBack = proofBack;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public static Creator<AdditionalDetails> getCREATOR() {
        return CREATOR;
    }

    private Address address;


    public AdditionalDetails(){

    }

    protected AdditionalDetails(Parcel in) {
        studentId = in.readString();
        studentPhoto = in.readString();
        fatherName = in.readString();
        employmentStatus = in.readString();
        companyName = in.readString();
        highestQualification = in.readString();
        collegeName = in.readString();
        universityName = in.readString();
        currentGoal = in.readString();
        deliveryInCollege = in.readByte() != 0;
        proofFront = in.readString();
        proofBack = in.readString();
        address = in.readParcelable(Address.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(studentId);
        dest.writeString(studentPhoto);
        dest.writeString(fatherName);
        dest.writeString(employmentStatus);
        dest.writeString(companyName);
        dest.writeString(highestQualification);
        dest.writeString(collegeName);
        dest.writeString(universityName);
        dest.writeString(currentGoal);
        dest.writeByte((byte) (deliveryInCollege ? 1 : 0));
        dest.writeString(proofFront);
        dest.writeString(proofBack);
        dest.writeParcelable(address, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AdditionalDetails> CREATOR = new Creator<AdditionalDetails>() {
        @Override
        public AdditionalDetails createFromParcel(Parcel in) {
            return new AdditionalDetails(in);
        }

        @Override
        public AdditionalDetails[] newArray(int size) {
            return new AdditionalDetails[size];
        }
    };
}
