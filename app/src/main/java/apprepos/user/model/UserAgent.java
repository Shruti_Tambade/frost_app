package apprepos.user.model;

import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 14-10-2019.
 * <p>
 * FROST
 */

public class UserAgent {

    private String ipAddress = Utility.getIpAddress();
    private String macAddress = Utility.getPhoneUniqueId(Utility.getContext());
    private String browser = "Native";
    private String deviceOs = "Android";
    private String deviceType = Utility.isTablet() ? "Tablet" : "Smartphone";

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
