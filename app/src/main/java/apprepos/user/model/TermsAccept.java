package apprepos.user.model;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 16-06-2020.
 * <p>
 * Frost
 */
public class TermsAccept {
    private String name;
    private String email;
    private String phoneNumber;
    private String dateOfAccept;
    private String timeOfAccept;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDateOfAccept() {
        return dateOfAccept;
    }

    public void setDateOfAccept(String dateOfAccept) {
        this.dateOfAccept = dateOfAccept;
    }

    public String getTimeOfAccept() {
        return timeOfAccept;
    }

    public void setTimeOfAccept(String timeOfAccept) {
        this.timeOfAccept = timeOfAccept;
    }
}
