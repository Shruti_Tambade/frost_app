package apprepos.user.model;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class Chronology
{

    private String calendarType;

    private String id;

    public String getCalendarType() {
        return calendarType;
    }

    public void setCalendarType(String calendarType) {
        this.calendarType = calendarType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
