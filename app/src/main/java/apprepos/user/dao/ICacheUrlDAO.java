package apprepos.user.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import apprepos.user.model.CacheUrlModel;


/**
 * Created by Gokul Kalagara (Mr. Psycho) on 10-08-2020.
 * <p>
 * Frost
 */
@Dao
public interface ICacheUrlDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCacheUrl(CacheUrlModel cacheUrlModel);


    @Query("SELECT * FROM cache_url WHERE tag = :tag")
    CacheUrlModel getCacheUrlByTag(String tag);

    @Query("delete from cache_url")
    void removeAllCacheUrls();

}
