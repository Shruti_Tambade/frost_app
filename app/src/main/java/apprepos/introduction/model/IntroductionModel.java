package apprepos.introduction.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Chenna Rao on 6/5/2020.
 */
public class IntroductionModel implements Parcelable {

    private String title;
    private int image;
    private String description;

    public IntroductionModel() {

    }


    protected IntroductionModel(Parcel in) {
        title = in.readString();
        image = in.readInt();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(image);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IntroductionModel> CREATOR = new Creator<IntroductionModel>() {
        @Override
        public IntroductionModel createFromParcel(Parcel in) {
            return new IntroductionModel(in);
        }

        @Override
        public IntroductionModel[] newArray(int size) {
            return new IntroductionModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
