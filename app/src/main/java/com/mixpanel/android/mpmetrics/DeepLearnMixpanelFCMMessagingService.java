package com.mixpanel.android.mpmetrics;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import com.frost.leap.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mixpanel.android.util.MPLog;

import java.util.Random;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 04-06-2020.
 * <p>
 * Frost
 */
public class DeepLearnMixpanelFCMMessagingService extends FirebaseMessagingService {
    private static final String LOGTAG = "MixpanelAPI.MixpanelFCMMessagingService";
    protected static final int NOTIFICATION_ID = 1;

    /* package */
    static void init() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            String registrationId = task.getResult().getToken();
                            addToken(registrationId);
                        }
                    }
                });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        MPLog.d(LOGTAG, "MP FCM on new message received");
        if (remoteMessage.getData() != null && remoteMessage.getData().containsKey("clear")) {
            String userId = remoteMessage.getData().get("userId");
            User user = UserRepositoryManager.getInstance().getUser();
            if (userId != null && user.getStudentId() != null && userId.equals(user.getStudentId())) {
                UserRepositoryManager.getInstance().deleteUserData();
            }
            return;
        }
        if (remoteMessage.getData() != null && remoteMessage.getData().containsKey("cms")) {
            try {

                String content = remoteMessage.getData().get("message");
                String bitmapUrl = remoteMessage.getData().get("url");
                String title = remoteMessage.getData().get("title");
                String bigText = remoteMessage.getData().get("bigText");
                String summary = remoteMessage.getData().get("summary");
                String color = remoteMessage.getData().get("color");
                String allowBig = remoteMessage.getData().get("bigAllowIcon");
                String bigIconUrl = remoteMessage.getData().get("bigIconUrl");
                String notificationId = "" + (1 + new Random().nextInt(99999));
                String optionType = remoteMessage.getData().get("type");
                String updateId = remoteMessage.getData().get("updateId");
                String updateType = remoteMessage.getData().get("updateType");
                sendNotification(updateId, updateType, content, bitmapUrl, title, bigText, summary, color, allowBig, bigIconUrl, notificationId, optionType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            onMessageReceived(getApplicationContext(), remoteMessage.toIntent());
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        MPLog.d(LOGTAG, "MP FCM on new push token: " + token);
        addToken(token);
    }

    /**
     * Util method to let subclasses customize the payload through the push notification intent.
     *
     * @param context The application context
     * @param intent  Push payload intent. Could be modified before calling super() from a sub-class.
     */
    protected void onMessageReceived(Context context, Intent intent) {
        showPushNotification(context, intent);
    }

    /**
     * Only use this method if you have implemented your own custom FirebaseMessagingService. This
     * is useful when you use multiple push providers.
     * This method should be called from a onNewToken callback. It adds a new FCM token to a Mixpanel
     * people profile.
     *
     * @param token Firebase Cloud Messaging token to be added to the people profile.
     */
    public static void addToken(final String token) {
        MixpanelAPI.allInstances(new MixpanelAPI.InstanceProcessor() {
            @Override
            public void process(MixpanelAPI api) {
                api.getPeople().setPushRegistrationId(token);
            }
        });
    }

    /**
     * Only use this method if you have implemented your own custom FirebaseMessagingService. This
     * is useful when you use multiple push providers.
     * Displays a Mixpanel push notification on the device.
     *
     * @param context       The application context you are tracking
     * @param messageIntent Intent that bundles the data used to build a notification. If the intent
     *                      is not valid, the notification will not be shown.
     *                      See {@link #showPushNotification(Context, Intent)}
     */
    public static void showPushNotification(Context context, Intent messageIntent) {
        MixpanelPushNotification mixpanelPushNotification = new MixpanelPushNotification(context.getApplicationContext());
        showPushNotification(context, messageIntent, mixpanelPushNotification);
    }

    /**
     * Only use this method if you have implemented your own custom FirebaseMessagingService. This is
     * useful if you need to override {@link MixpanelPushNotification} to further customize your
     * Mixpanel push notification.
     * Displays a Mixpanel push notification on the device.
     *
     * @param context                  The application context you are tracking
     * @param messageIntent            Intent that bundles the data used to build a notification. If the intent
     *                                 is not valid, the notification will not be shown.
     *                                 See {@link #showPushNotification(Context, Intent)}
     * @param mixpanelPushNotification A customized MixpanelPushNotification object.
     */
    public static void showPushNotification(Context context, Intent messageIntent, MixpanelPushNotification mixpanelPushNotification) {
        Notification notification = mixpanelPushNotification.createNotification(messageIntent);

        MixpanelNotificationData data = mixpanelPushNotification.getData();
        String message = data == null ? "null" : data.getMessage();
        MPLog.d(LOGTAG, "MP FCM notification received: " + message);

        if (notification != null) {
            if (!mixpanelPushNotification.isValid()) {
                MPLog.e(LOGTAG, "MP FCM notification has error");
            }
            final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (null != data.getTag()) {
                notificationManager.notify(data.getTag(), NOTIFICATION_ID, notification);
            } else {
                notificationManager.notify(mixpanelPushNotification.getNotificationId(), notification);
            }
        }
    }

    protected void cancelNotification(Bundle extras, NotificationManager notificationManager) {
        int notificationId = extras.getInt("mp_notification_id");
        String tag = extras.getString("mp_tag");
        boolean hasTag = tag != null;

        if (hasTag) {
            notificationManager.cancel(tag, MixpanelFCMMessagingService.NOTIFICATION_ID);
        } else {
            notificationManager.cancel(notificationId);
        }
    }

    private void sendNotification(String updateId, String updateType, String messageBody, String bitmapUrl, String title, String bigText, String summary, String color, String allowBig, String bigIconUrl, String notificationId, String optionType) {

        Intent broadCastIntent = new Intent();
        broadCastIntent.putExtra("Refresh", true);
        broadCastIntent.setAction("app_action_cms_updates");
        sendBroadcast(broadCastIntent);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("cms-deeplink://deep-learn/updates"));
        intent.putExtra("UpdateId", updateId);
        intent.putExtra("UpdateType", updateType);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(1000), intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "IDEEP_LEARN_NOTIFY_CHANNEL")
                .setTicker(title).setWhen(0)
                .setSmallIcon(R.drawable.deep_logo)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (title != null) {
            notificationBuilder.setContentTitle(title);
        } else {
            notificationBuilder.setContentTitle(Constants.APP_NAME);
        }
        if (color != null) {
            try {
                notificationBuilder.setColor(Color.parseColor(color));
            } catch (Exception e) {
                e.printStackTrace();
                notificationBuilder.setColor(Color.parseColor("#000000"));
            }
        } else {
            notificationBuilder.setColor(Color.parseColor("#000000"));
        }

        if (summary != null)
            bigPictureStyle.setSummaryText(messageBody);
        if (bigText != null) {
            bigPictureStyle.setBigContentTitle(bigText);
        } else {

        }
        if (bitmapUrl != null && bitmapUrl.length() > 0) {
            Bitmap bitmap1 = Utility.urlToBitMap(bitmapUrl);
            if (bitmap1 != null) {
                bigPictureStyle.bigPicture(bitmap1);
                notificationBuilder.setStyle(bigPictureStyle);

            }
        } else {
            //notificationBuilder.setStyle(bigPictureStyle);
        }
        if (allowBig != null) {
            if (allowBig.equals("1")) {
                if (bigIconUrl != null && bigIconUrl.trim().length() > 0) {
                    try {
                        notificationBuilder.setLargeIcon(Utility.getRoundBitmap(Utility.urlToBitMap(bigIconUrl)));
                    } catch (Exception e) {
                        e.printStackTrace();
                        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.deep_logo));
                    }
                } else
                    notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.deep_logo));
            } else {

            }
        } else {

        }
        notificationBuilder.setChannelId("IDEEP_LEARN_NOTIFY_CHANNEL");

        if (notificationId != null) {
            try {
                int notificationNumber = Integer.parseInt(notificationId);
                if (notificationNumber > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationManager oreoNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        NotificationChannel notificationChannel = new NotificationChannel("IDEEP_LEARN_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
                        oreoNotificationManager.createNotificationChannel(notificationChannel);
                        oreoNotificationManager.notify(notificationNumber, notificationBuilder.build());
                    } else {
                        notificationManager.notify(notificationNumber, notificationBuilder.build());
                    }
                }
            } catch (Exception e) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager oreoNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationChannel notificationChannel = new NotificationChannel("IDEEP_LEARN_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
                    oreoNotificationManager.createNotificationChannel(notificationChannel);
                    oreoNotificationManager.notify(new Random().nextInt(99999), notificationBuilder.build());
                } else {
                    notificationManager.notify(new Random().nextInt(99999), notificationBuilder.build());
                }
            }


        } else {
            notificationManager.notify(0, notificationBuilder.build());
        }
    }

}
