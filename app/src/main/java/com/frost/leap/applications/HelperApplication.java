package com.frost.leap.applications;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;

import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDexApplication;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.frost.leap.BuildConfig;
import com.frost.leap.services.video.DownloadTracker;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.database.DatabaseProvider;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.offline.ActionFileUpgradeUtil;
import com.google.android.exoplayer2.offline.DefaultDownloadIndex;
import com.google.android.exoplayer2.offline.DefaultDownloaderFactory;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.upstream.CustomHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Log;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.perf.FirebasePerformance;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.razorpay.Checkout;

import java.io.File;
import java.io.IOException;

import apprepos.user.UserRepositoryManager;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;
import storage.AppSharedPreferences;
import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 7/6/2019.
 */
public class HelperApplication extends MultiDexApplication {
    private static HelperApplication helperApplication;
    private static final String TAG = "HelperApplication";
    private static final String DOWNLOAD_ACTION_FILE = "actions";
    private static final String DOWNLOAD_TRACKER_ACTION_FILE = "tracked_actions";
    public static final String DOWNLOAD_CONTENT_DIRECTORY = "topics";
    private DatabaseProvider databaseProvider;
    private File downloadDirectory;
    private Cache downloadCache;
    private DownloadManager downloadManager;
    private DownloadTracker downloadTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        AppEventsLogger.activateApp(this);
        FirebaseApp.getInstance();
        FirebasePerformance.getInstance();
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(BuildConfig.DEBUG ? true : true);
        Fresco.initialize(this);
        Checkout.preload(getApplicationContext());
        AppSharedPreferences.getInstance().setContext(this);
        helperApplication = this;
        MixpanelAPI.getInstance(this, Constants.MIXPANEL_TOKEN);


        RxJavaPlugins.setErrorHandler(e -> {
            try {
                if (e instanceof UndeliverableException) {
                    // Merely log undeliverable exceptions
                    Logger.d("Exception", "" + e.getMessage());
                } else {
                    // Forward all others to current thread's uncaught exception handler
                    Thread.currentThread().setUncaughtExceptionHandler((t, e1) -> {
                        if (t.getUncaughtExceptionHandler() != null)
                            t.getUncaughtExceptionHandler().uncaughtException(t, e1);
                    });
                }
            } catch (Exception err) {
                err.printStackTrace();
            }
        });
    }

    private String getProcessName(Context context) {
        if (context == null) return null;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager == null) return null;
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
            if (processInfo.pid == android.os.Process.myPid()) {
                return processInfo.processName;
            }
        }
        return null;
    }

    public static HelperApplication getInstance() {
        if (helperApplication == null) {
            helperApplication = new HelperApplication();
        }
        return helperApplication;
    }

    public DataSource.Factory buildDataSourceFactory() {
        DefaultDataSourceFactory upstreamFactory =
                new DefaultDataSourceFactory(this, buildHttpDataSourceFactory());
        return buildReadOnlyCacheDataSource(upstreamFactory, getDownloadCache());
    }

    /**
     * Returns a {@link HttpDataSource.Factory}.
     */
    public HttpDataSource.Factory buildHttpDataSourceFactory() {
        return Build.VERSION.SDK_INT < 24 ?
                new CustomHttpDataSourceFactory(Constants.APP_NAME) :
                new DefaultHttpDataSourceFactory(Constants.APP_NAME);
    }

    /**
     * Returns whether extension renderers should be used.
     */
    public boolean useExtensionRenderers() {
        return "withExtensions".equals(BuildConfig.FLAVOR);
    }

    public RenderersFactory buildRenderersFactory(boolean preferExtensionRenderer) {
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(/* context= */ this)
                .setExtensionRendererMode(extensionRendererMode);
    }

    public DownloadManager getDownloadManager() {
        initDownloadManager();
        return downloadManager;
    }


    public DownloadTracker getDownloadTracker() {
        initDownloadManager();
        return downloadTracker;
    }

    public void recreateDownloadManager() {


        downloadManager = null;

        downloadDirectory = null;

        databaseProvider = null;

        if (downloadCache != null)
            downloadCache.release();

        downloadCache = null;

        downloadTracker = null;

        initDownloadManager();
    }

    protected synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            downloadCache =
                    new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor(), getDatabaseProvider());
        }
        return downloadCache;
    }

    private synchronized void initDownloadManager() {
        if (downloadManager == null) {
            DefaultDownloadIndex downloadIndex = new DefaultDownloadIndex(getDatabaseProvider());
            upgradeActionFile(
                    DOWNLOAD_ACTION_FILE, downloadIndex, /* addNewDownloadsAsCompleted= */ false);
            upgradeActionFile(
                    DOWNLOAD_TRACKER_ACTION_FILE, downloadIndex, /* addNewDownloadsAsCompleted= */ true);
            DownloaderConstructorHelper downloaderConstructorHelper =
                    new DownloaderConstructorHelper(getDownloadCache(), buildHttpDataSourceFactory());

            downloadManager =
                    new DownloadManager(
                            this, downloadIndex, new DefaultDownloaderFactory(downloaderConstructorHelper));
            downloadManager.setMaxParallelDownloads(5);
            downloadTracker = new DownloadTracker(/* context= */ this, buildDataSourceFactory(), downloadManager);
        }
    }

    private void upgradeActionFile(
            String fileName, DefaultDownloadIndex downloadIndex, boolean addNewDownloadsAsCompleted) {
        try {
            ActionFileUpgradeUtil.upgradeAndDelete(
                    new File(getDownloadDirectory(), fileName),
                    /* downloadIdProvider= */ null,
                    downloadIndex,
                    /* deleteOnFailure= */ true,
                    addNewDownloadsAsCompleted);
        } catch (IOException e) {
            Log.e(TAG, "Failed to upgrade action file: " + fileName, e);
        }
    }


    private DatabaseProvider getDatabaseProvider() {
        if (databaseProvider == null) {
            databaseProvider = new ExoDatabaseProvider(this);
        }
        return databaseProvider;
    }


    public File getDownloadDirectory() {

        if (downloadDirectory == null) {
            if (UserRepositoryManager.getInstance().getVideoDownloadLocation() == 2 && Utility.isExternalStorageWritable(getApplicationContext()) && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, getApplicationContext())) {
                Logger.d("Download Directory", "SD CARD");
                File[] externalStorageVolumes =
                        ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
                downloadDirectory = externalStorageVolumes[1];
            } else {
                Logger.d("Download Directory", "INTERNAL STORAGE");
                downloadDirectory = getExternalFilesDir(null);
                if (downloadDirectory == null) {
                    downloadDirectory = getFilesDir();
                }
            }
        }
        return downloadDirectory;
    }

    public static CacheDataSourceFactory buildReadOnlyCacheDataSource(
            DataSource.Factory upstreamFactory, Cache cache) {
        FileDataSource.Factory factory = new FileDataSource.Factory();

        return new CacheDataSourceFactory(
                cache,
                upstreamFactory,
                factory,
                /* cacheWriteDataSinkFactory= */ null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                /* eventListener= */ null);
    }


}