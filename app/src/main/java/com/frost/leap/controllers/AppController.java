package com.frost.leap.controllers;

/**
 * Created by Gokul Kalagara (Mr.Psycho) on 28-02-2019.
 * at 14:39
 * Frost-Interactive
 */
public class AppController {

    public static final int CATALOGUE_OVERVIEW = 11001;

    //Store
    public static final int STORE_OVERVIEW = 12001;


    // QUIZ
    public static final int QUIZ_OVERVIEW = 11002;

    public static final int QUIZ_START = 11003;

    public static final int QUIZ_REVIEW = 11004;

    public static final int QUIZ_RESULT = 11005;


    //PROFILE
    public static final int PROFILE_SETTINGS = 3001;

    public static final int GALLERY_FRAGMENT = 3002;

    public static final int ADDITIONAL_SETTINGS = 3003;

    public static final int BILLING_HISTORY = 3009;

    public static final int INVOICE_DETAILS = 3010;

    public static final int DEFAULT_APP_SETTINGS = 3014;

    public static final int LINKED_DEVICES = 3015;

    public static final int ABOUT_DEEP_LEARN = 3016;

    public static final int PROFILE_NOTIFICATIONS = 3017;

    public static final int DATA_USAGE = 3018;

    public static final int DISPLAY_THEME = 3019;

    public static final int SUBMIT_REQUEST = 3020;

    public static final int CLAIM_FORM = 3021;

    public static final int HELP_PAGE = 3022;

    public static final int QUERY_PAGE = 3023;

    public static final int DOUBT_CLEARING_PAGE = 3024;

    public static final int ALL_UPDATES_PAGE = 3025;


    // WEB
    public static final int WEB_OVERVIEW = 4001;

    // PDF
    public static final int PDF_OVERVIEW = 4002;


    //ACTION_SERVICES
    public static final int ACTION_DOWNLOAD_RESOURCE = 31;

    public static final int ACTION_UPDATE_ADDITIONAL_DETAILS = 41;

    public static final int ACTION_UPDATE_USER_PROFILE = 51;


    public static final int ACTION_ADD_FRESH_DESK_TICKET = 52;


    public static final int ACTION_LOGOUT = 53;

    public static final int ACTION_UPDATE_RATING = 54;

    public static final int ACTION_UPDATE_DEVICE_DETAILS = 55;

    public static final int ACTION_GET_VIDEO_URLS = 56;

    public static final int ACTION_ASK_AN_EXPERT = 57;

    public static final int ACTION_DELETE_NOTE = 58;

    public static final int ACTION_UPDATE_NOTE = 59;

    public static final int ACTION_CREATE_NOTE = 60;


    // UPDATE VIDEO STREAM
    public static final int ACTION_UPDATE_VIDEO_STREAM = 71;

    // DRM
    public static final int ACTION_DOWNLOAD_DRM_LICENSE = 81;

    // REPORT ABOUT VIDEO
    public static final int ACTION_REPORT_VIDEO = 85;

    // IS ACCEPT TERMS
    public static final int ACTION_IS_ACCEPT_TERMS = 86;

    // ACCEPT TERMS
    public static final int ACTION_ACCEPT_TERMS = 87;

    // DOWNLOAD PENDING LICENSE
    public static final int ACTION_CACHE_PENDING_LICENSE = 90;

    // PARTICULAR DASH URLS LICENSE
    public static final int ACTION_CACHE_PLAYLIST_URLS_LICENSE = 91;

    // CANCEL
    public static final int ACTION_LICENCE_VERIFY = 92;

    public static final int ACTION_DISMISS_LICENCE_VERIFY = 93;

    public static final int ACTION_DELETE_VIDEO_AND_LICENSE_EXPIRY = 94;


    //video player
    public static final int VIDEO_PLAYER = 13001;

    //LeadDetails

    public static final int CREATE_LEAD = 14001;

    public static final int UPDATE_LEAD = 14002;

    public static final int GET_LEAD_BY_EMAILID = 14003;

}
