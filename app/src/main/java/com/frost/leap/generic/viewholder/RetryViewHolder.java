package com.frost.leap.generic.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;


public class RetryViewHolder extends RecyclerView.ViewHolder {

    public TextView tvError, tvRetry;

    public RetryViewHolder(View itemView) {
        super(itemView);
        tvError = itemView.findViewById(R.id.tvError);
        tvRetry = itemView.findViewById(R.id.tvRetry);
    }
}
