package com.frost.leap.generic.viewholder;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;


public class SkeletonViewHolder extends RecyclerView.ViewHolder {
    public SkeletonViewHolder(View itemView) {
        super(itemView);
    }
}
