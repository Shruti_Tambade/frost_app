package com.frost.leap.generic.dialog;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frost.leap.R;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.google.android.exoplayer2.offline.Download;

import java.util.HashMap;

/**
 * Created by Gokul Kalagara on 12-08-2018.
 **/

public class CustomAlertDialog extends Dialog {

    private TextView tvTitle, tvDetails, tvYes, tvNo;
    private String title, details;
    private Context context;
    private int id;
    private ICustomAlertDialog iCustomAlertDialog;
    private boolean isAlert = false;
    private boolean isHard = false;
    private String actionName;

    public CustomAlertDialog(@NonNull Context context, ICustomAlertDialog iCustomAlertDialog, String title, String details, int id) {
        super(context);
        this.context = context;
        this.iCustomAlertDialog = iCustomAlertDialog;
        this.title = title;
        this.details = details;
        this.id = id;
    }


    public CustomAlertDialog(@NonNull Context context,
                             ICustomAlertDialog iCustomAlertDialog,
                             String title,
                             String details,
                             int id, String actionName, boolean isHard) {
        super(context);
        this.context = context;
        this.iCustomAlertDialog = iCustomAlertDialog;
        this.title = title;
        this.details = details;
        this.id = id;
        this.actionName = actionName;
        this.isAlert = true;
        this.isHard = isHard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_dialog);

        setUp();
    }

    private void setUp() {

        tvTitle = findViewById(R.id.tvTitle);
        tvDetails = findViewById(R.id.tvDetails);
        tvYes = findViewById(R.id.tvYes);
        tvNo = findViewById(R.id.tvNo);

        if (isAlert) {
            tvNo.setVisibility(View.GONE);
            tvYes.setText(actionName);
        }

        tvTitle.setText(title);
        if (details != null) {
            if (title.contains("Logout?")) {
                tvDetails.setText(Html.fromHtml(details));
            } else
                tvDetails.setText(details);
        } else
            tvDetails.setVisibility(View.GONE);


        tvNo.setOnClickListener(v -> {
            this.dismiss();
            iCustomAlertDialog.doNegativeAction();
        });

        tvYes.setOnClickListener(v -> {
            if (!isHard)
                this.dismiss();

            iCustomAlertDialog.doPositiveAction(id);
        });
    }
}
