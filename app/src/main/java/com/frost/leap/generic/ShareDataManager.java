package com.frost.leap.generic;

import android.net.NetworkInfo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.updates.model.UpdateModel;
import apprepos.video.model.video.DashUrlsModel;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 15-03-2020.
 * <p>
 * FROST
 */
public class ShareDataManager {

    private boolean hasNext = false;
    private static ShareDataManager manager;
    private static StoreOverviewData storeOverviewData;
    private boolean online = true;
    private NetworkInfo networkInfo;
    private long subTopicCurrentPosition = 0;
    private double subTopicPercentage = 0;
    private DashUrlsModel dashUrlsModel;
    private int aspectRatio = AspectRatioFrameLayout.RESIZE_MODE_FIT;
    private MutableLiveData<Boolean> isAcceptLiveData;
    private MutableLiveData<UpdateModel> dashBoardUpdateLiveData;
    private MutableLiveData<UpdateModel> allUpdatesLiveData;
    private MutableLiveData<Integer> tapLiveData;
    private MutableLiveData<Integer> liveDataResolutions;

    private ShareDataManager() {
        isAcceptLiveData = new MutableLiveData<>();
        dashBoardUpdateLiveData = new MutableLiveData<>();
        allUpdatesLiveData = new MutableLiveData<>();
        tapLiveData = new MutableLiveData<>();
        liveDataResolutions = new MutableLiveData<>();
    }

    public static ShareDataManager getInstance() {
        if (manager == null) {
            synchronized (ShareDataManager.class) {
                manager = new ShareDataManager();
            }
        }
        return manager;
    }

    public LiveData<Boolean> isAcceptTerms() {
        return isAcceptLiveData;
    }

    public LiveData<UpdateModel> getDashBoardUpdateLiveData() {
        return dashBoardUpdateLiveData;
    }

    public LiveData<UpdateModel> getAllUpdatesLiveData() {
        return allUpdatesLiveData;
    }

    public class StoreOverviewData {
        private CatalogueModel catalogueModel;
        private String catalogueName;

        public CatalogueModel getCatalogueModel() {
            return catalogueModel;
        }

        public void setCatalogueModel(CatalogueModel catalogueModel) {
            this.catalogueModel = catalogueModel;
        }

        public String getCatalogueName() {
            return catalogueName;
        }

        public void setCatalogueName(String catalogueName) {
            this.catalogueName = catalogueName;
        }
    }

    public StoreOverviewData getStoreOverviewData() {
        if (storeOverviewData == null) {
            storeOverviewData = new StoreOverviewData();
        }
        return storeOverviewData;
    }

    public boolean getHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public NetworkInfo getNetworkInfo() {
        return networkInfo;
    }

    public void setNetworkInfo(NetworkInfo networkInfo) {
        this.networkInfo = networkInfo;
    }

    public long getSubTopicCurrentPosition() {
        return subTopicCurrentPosition;
    }

    public void setSubTopicCurrentPosition(long subTopicCurrentPosition) {
        this.subTopicCurrentPosition = subTopicCurrentPosition;
    }

    public double getSubTopicPercentage() {
        return subTopicPercentage > 99 || subTopicPercentage < 0.1 ? 0 : subTopicPercentage;
    }

    public void setSubTopicPercentage(double subTopicPercentage) {
        this.subTopicPercentage = subTopicPercentage;
    }


    public DashUrlsModel getDashUrlsModel() {
        return dashUrlsModel;
    }

    public void setDashUrlsModel(DashUrlsModel dashUrlsModel) {
        this.dashUrlsModel = dashUrlsModel;
    }

    public int getAspectRatio() {
        return aspectRatio;
    }

    public void setAspectRatio(int aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    public void setIsAccept(boolean isAccept) {
        isAcceptLiveData.postValue(isAccept);
    }

    public void setDashBoardUpdateData(UpdateModel updateModel) {
        dashBoardUpdateLiveData.postValue(updateModel);
    }

    public void setAllUpdatesData(UpdateModel updateModel) {
        allUpdatesLiveData.postValue(updateModel);
    }

    public void setTapLiveData(int tap) {
        tapLiveData.setValue(tap);
    }

    public LiveData<Integer> getTapLiveData() {
        return tapLiveData;
    }

    public LiveData<Integer> getLiveDataResolution() {
        return liveDataResolutions;
    }

    public void setLiveDataResolutions(int resolutions) {
        liveDataResolutions.setValue(resolutions);
    }

}
