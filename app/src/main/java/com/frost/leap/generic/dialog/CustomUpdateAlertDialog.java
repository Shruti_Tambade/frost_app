package com.frost.leap.generic.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.frost.leap.R;
import com.frost.leap.interfaces.ICustomAlertDialog;

import static android.view.View.GONE;

/**
 * Created by Chenna Rao on 16-03-2020.
 **/

public class CustomUpdateAlertDialog extends Dialog {

    private TextView tvHeading, tvDescription, tvSubmit, tvSkip;
    private ImageView ivIcon;
    private View view1, view2;
    private String title, details;
    private Context context;
    private int id;
    private ICustomAlertDialog iCustomAlertDialog;
    private boolean isAlert = false;
    private boolean isHard = false;
    private String actionName;

    public CustomUpdateAlertDialog(@NonNull Context context, ICustomAlertDialog iCustomAlertDialog, String title, String details, int id) {
        super(context);
        this.context = context;
        this.iCustomAlertDialog = iCustomAlertDialog;
        this.title = title;
        this.details = details;
        this.id = id;
    }


    public CustomUpdateAlertDialog(@NonNull Context context,
                                   ICustomAlertDialog iCustomAlertDialog,
                                   String title,
                                   String details,
                                   int id, String actionName, boolean isHard) {
        super(context);
        this.context = context;
        this.iCustomAlertDialog = iCustomAlertDialog;
        this.title = title;
        this.details = details;
        this.id = id;
        this.actionName = actionName;
        this.isAlert = true;
        this.isHard = isHard;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_dialog_app_update);

        setUp();
    }

    private void setUp() {

        tvHeading = findViewById(R.id.tvHeading);
        tvDescription = findViewById(R.id.tvDescription);
        tvSubmit = findViewById(R.id.tvSubmit);
        tvSkip = findViewById(R.id.tvSkip);
        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        ivIcon = findViewById(R.id.ivIcon);

        if (title == null) {
            view2.setVisibility(GONE);
            view1.setVisibility(View.VISIBLE);
            tvDescription.setVisibility(GONE);
            tvSkip.setVisibility(GONE);

            tvHeading.setText(details);
            tvSubmit.setText(actionName);
            ivIcon.setImageResource(R.drawable.ic_subject_icon);
            tvSubmit.setBackgroundResource(R.drawable.corner_radius_blue_3);
        } else {
            view1.setVisibility(GONE);
            view2.setVisibility(View.VISIBLE);

            tvSkip.setVisibility(isHard ? GONE : View.VISIBLE);

            tvHeading.setText(title);
            tvDescription.setText(details);
            tvSubmit.setText(actionName);
            ivIcon.setImageResource(R.drawable.ic_update_app_icon);
            tvSubmit.setBackgroundResource(R.drawable.corner_radius_red_3);
        }

        tvSubmit.setOnClickListener(v -> {
            if (!isHard)
                this.dismiss();

            iCustomAlertDialog.doPositiveAction(id);
        });

        tvSkip.setOnClickListener(v -> {
            this.dismiss();
        });
    }
}
