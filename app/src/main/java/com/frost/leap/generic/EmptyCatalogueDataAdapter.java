package com.frost.leap.generic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.catalogue.ICatalogueAdapter;
import com.frost.leap.databinding.EmptyCatalogueDataItemBinding;

import supporters.utils.Mixpanel;


public class EmptyCatalogueDataAdapter extends RecyclerView.Adapter<EmptyCatalogueDataAdapter.EmptyViewHolder> {

    private Context context;
    private String content;
    private int imageId;
    private int type;
    private String email;
    private ICatalogueAdapter iCatalogueAdapter;


    public EmptyCatalogueDataAdapter(ICatalogueAdapter iCatalogueAdapter, Context context, String content, int imageId, int type, String email) {
        this.context = context;
        this.content = content;
        this.imageId = imageId;
        this.type = type;
        this.iCatalogueAdapter = iCatalogueAdapter;
        this.email = email;
    }

    @Override
    public EmptyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new EmptyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.empty_catalogue_data_item, parent, false));
    }

    @Override
    public void onBindViewHolder(EmptyViewHolder holder, int position) {

        holder.bindData();

        holder.binding.exploreCourse.setOnClickListener(v -> {
            Mixpanel.clickExporeNow(email);
            iCatalogueAdapter.gotoStore();
        });

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {

        private EmptyCatalogueDataItemBinding binding;

        public EmptyViewHolder(EmptyCatalogueDataItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bindData() {
            binding.tvRetry.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
            binding.tvError.setText(content);
            binding.imgError.setImageResource(imageId);

            if (type == 2) {
                binding.tvRetry.setVisibility(View.GONE);
                binding.llExplore.setVisibility(View.VISIBLE);
            }
        }
    }
}
