package com.frost.leap.generic.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;


public class LoaderViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout llLoader, llInside;
    public ProgressBar progressBar;
    public TextView tvTitle, tvContent;

    public LoaderViewHolder(View itemView) {
        super(itemView);

        tvContent = itemView.findViewById(R.id.tvContent);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        progressBar = itemView.findViewById(R.id.progressBar);
        llInside = itemView.findViewById(R.id.llInside);
        llLoader = itemView.findViewById(R.id.llLoader);
    }
}
