package com.frost.leap.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import apprepos.user.UserRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Utility;


/**
 * Created by Gokul Kalagara (Mr.Psycho) on 28-07-2019.
 * at 14:55
 * Frost-Interactive
 */
public abstract class BaseViewModel extends ViewModel {

    public MutableLiveData<Error> error;
    public MutableLiveData<NetworkCall> networkCall;
    public MutableLiveData<Integer> isRefreshToken;
    public MutableLiveData<Message> message;
    public Disposable tokenDisposable;
    private UserRepositoryManager baseUserRepositoryManager;

    public BaseViewModel() {
        message = new MediatorLiveData<>();
        error = new MediatorLiveData<>();
        isRefreshToken = new MediatorLiveData<>();
        addNetworkCall();
    }


    public void addNetworkCall() {
        networkCall = new MediatorLiveData<>();
    }

    public LiveData<Message> getMessage() {
        return message;
    }

    public LiveData<Error> getError() {
        return error;
    }

    public LiveData<NetworkCall> getNetworkCallStatus() {
        return networkCall;
    }

    public LiveData<Integer> controlRefreshToken() {
        return isRefreshToken;
    }

    public boolean isNetworkAvailable() {
        return Utility.isNetworkAvailable(Utility.getContext());
    }


    public void refreshToken(IRefreshToken iRefreshToken) {
        if (!isNetworkAvailable()) {
            isRefreshToken.postValue(-1);
            iRefreshToken.isTokenRefreshed(false);
            return;
        }


        String token = getUserToken();
        isRefreshToken.postValue(0);

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                tokenDisposable = d;
            }

            @Override
            public void onNext(Response<JsonObject> response) {

                if (response != null && response.code() == 200) {
                    baseUserRepositoryManager.saveUserInfo(response.headers().get(Constants.AUTHORIZATION),
                            response.body());
                    isRefreshToken.postValue(1);
                    iRefreshToken.isTokenRefreshed(true);
                } else if (response != null && response.code() == 401) {
                    if (!token.equals(getUserToken())) {
                        isRefreshToken.postValue(1);
                        iRefreshToken.isTokenRefreshed(true);
                    } else {
                        isRefreshToken.postValue(2);
                        iRefreshToken.isTokenRefreshed(false);
                    }
                } else {
                    isRefreshToken.postValue(-1);
                    iRefreshToken.isTokenRefreshed(false);
                }
            }

            @Override
            public void onError(Throwable e) {
                isRefreshToken.postValue(-1);
                iRefreshToken.isTokenRefreshed(false);
            }

            @Override
            public void onComplete() {

            }
        };
        if (baseUserRepositoryManager == null)
            baseUserRepositoryManager = UserRepositoryManager.getInstance();

        baseUserRepositoryManager.refreshToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    public interface IRefreshToken {
        void isTokenRefreshed(boolean isRefresh);
    }

    public void clearLoginDetails() {
        if (baseUserRepositoryManager == null)
            baseUserRepositoryManager = UserRepositoryManager.getInstance();
        baseUserRepositoryManager.clearLoginDetails();
    }

    public void deleteUserData() {
        if (baseUserRepositoryManager == null)
            baseUserRepositoryManager = UserRepositoryManager.getInstance();
        baseUserRepositoryManager.deleteUserData();
        baseUserRepositoryManager.deleteAllCacheUrls();
    }


    public String getUserToken() {
        if (baseUserRepositoryManager == null)
            baseUserRepositoryManager = UserRepositoryManager.getInstance();
        return baseUserRepositoryManager.getUserToken();
    }


}
