package com.frost.leap.viewmodels.start;

import android.os.Bundle;
import android.text.TextUtils;

import com.frost.leap.R;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.Credentials;
import apprepos.user.model.ProfileInfo;
import apprepos.user.model.User;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 30-09-2019.
 * <p>
 * FROST
 */

public class SignUpViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;
    private String firstName, lastName, email, phoneNumber, password;

    public SignUpViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
    }



    public class NetworkTag {
        public static final int CHECK_USER_EXIST = 1;
    }

    /**
     * do validation before sign up
     *
     * @param firstName
     * @param lastName
     * @param email
     * @param mobileNumber
     * @param password
     * @param confirmPassword
     * @param isAgree
     * @return
     */
    public boolean doValidationSignUp(String firstName, String lastName, String email, String mobileNumber, String password,
                                      String confirmPassword, boolean isAgree) {


        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName)
                && TextUtils.isEmpty(email) && TextUtils.isEmpty(password)
                && TextUtils.isEmpty(confirmPassword) && TextUtils.isEmpty(mobileNumber)) {

            error.setValue(new Error(R.id.tlFirstName, "First name cannot be empty"));
            error.setValue(new Error(R.id.tlLastName, "Last name cannot be empty"));
            error.setValue(new Error(R.id.tlEmailAddress, "Email cannot be empty"));
            error.setValue(new Error(R.id.tlPassword, "Password cannot be empty"));
            error.setValue(new Error(R.id.tlConfirmPassword, "Password cannot be empty"));
            error.setValue(new Error(R.id.tlPhoneNumber, "Phone number cannot be empty"));

            return false;

        }


        if (TextUtils.isEmpty(firstName.trim())) {
            error.postValue(new Error(R.id.tlFirstName, "First name cannot be empty"));
            return false;
        }
        if (TextUtils.isEmpty(lastName.trim())) {
            error.postValue(new Error(R.id.tlLastName, "Last name cannot be empty"));
            return false;
        }

        if (TextUtils.isEmpty(email)) {
            error.postValue(new Error(R.id.tlEmailAddress, "Email cannot be empty"));
            return false;
        }

        if (!Utility.isValidEmail(email)) {
            error.postValue(new Error(R.id.tlEmailAddress, "Please enter a valid email address"));
            return false;
        }

        if (TextUtils.isEmpty(mobileNumber)) {
            error.postValue(new Error(R.id.tlPhoneNumber, "Phone number cannot be empty"));
            return false;
        }

        if (!Utility.isValidMobile(mobileNumber)) {
            error.postValue(new Error(R.id.tlPhoneNumber, "The phone number you have entered is invalid"));
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            error.postValue(new Error(R.id.tlPassword, "Password cannot be empty"));
            return false;
        }

        if (password.length() < 6) {
            error.postValue(new Error(R.id.tlPassword, "Password should have atleast 6 characters"));
            return false;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            error.postValue(new Error(R.id.tlConfirmPassword, "Password cannot be empty"));
            return false;
        }

        if (!password.equals(confirmPassword)) {
            error.postValue(new Error(R.id.tlConfirmPassword, "Those passwords didn't match. Try again"));
            return false;
        }

        if (!isAgree) {
            message.postValue(new Message("Please accept the terms & conditions to continue", 2));
            return false;
        }

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = mobileNumber;
        this.password = password;
        return true;

    }


    public void checkUserExist() {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                User user = new User();
                ProfileInfo profileInfo = new ProfileInfo();
                Credentials credentials = new Credentials();
                profileInfo.setFirstName(firstName);
                profileInfo.setLastName(lastName);
                credentials.setMobile(phoneNumber);
                credentials.setEmail(email);
                credentials.setPassword(password);
                user.setProfileInfo(profileInfo);
                user.setCredentials(credentials);

                Bundle bundle = new Bundle();
                bundle.putParcelable("User", user);

                if (response != null && response.code() == 200) {

                    String otpWord = response.headers().get("otplength");
                    if (otpWord != null) {
                        repositoryManager.setOtpWordCount(otpWord);
                    }
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, bundle, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, bundle, NetworkStatus.FAIL));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, bundle, NetworkStatus.FAIL));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, bundle, NetworkStatus.FAIL));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();

                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.checkUserExist(email, phoneNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

}
