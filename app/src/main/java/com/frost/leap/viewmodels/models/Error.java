package com.frost.leap.viewmodels.models;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 18-02-2020.
 * <p>
 * FROST
 */
public class Error {
    private int id;

    public Error(int id, String message) {
        this.id = id;
        this.message = message;
    }

    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
