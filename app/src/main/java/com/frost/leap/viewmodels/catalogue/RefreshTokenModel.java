package com.frost.leap.viewmodels.catalogue;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Chenna Rao on 8/28/2019.
 */
public class RefreshTokenModel implements Parcelable {

    private String userId;
    private String firstName;
    private String lastName;
    private Boolean enableSearch;
    private String theme;
    private String image;

    protected RefreshTokenModel(Parcel in) {
        userId = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        byte tmpEnableSearch = in.readByte();
        enableSearch = tmpEnableSearch == 0 ? null : tmpEnableSearch == 1;
        theme = in.readString();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeByte((byte) (enableSearch == null ? 0 : enableSearch ? 1 : 2));
        dest.writeString(theme);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RefreshTokenModel> CREATOR = new Creator<RefreshTokenModel>() {
        @Override
        public RefreshTokenModel createFromParcel(Parcel in) {
            return new RefreshTokenModel(in);
        }

        @Override
        public RefreshTokenModel[] newArray(int size) {
            return new RefreshTokenModel[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getEnableSearch() {
        return enableSearch;
    }

    public void setEnableSearch(Boolean enableSearch) {
        this.enableSearch = enableSearch;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
