package com.frost.leap.viewmodels.subjects;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.services.models.FileDownload;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.util.List;

import apprepos.subject.ResourceRepositoryManager;
import apprepos.subject.model.ResourceModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/17/2019.
 */
public class ResourceViewModel extends BaseViewModel {

    private MutableLiveData<List<ResourceModel>> resourceLiveData;
    private ResourceRepositoryManager resourceRepositoryManager;

    public ResourceViewModel() {
        super();
        resourceLiveData = new MutableLiveData<>();
        resourceRepositoryManager = ResourceRepositoryManager.getInstance();
    }

    public LiveData<List<ResourceModel>> updateResourcesData() {
        return resourceLiveData;
    }

    public static final class NetworkTags {

        public static final int RESOURCES = 1;
    }

    public void fetchResources(String studentCatalogueId) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.IN_PROCESS));
        Observer<Response<List<ResourceModel>>> observer = new Observer<Response<List<ResourceModel>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<ResourceModel>> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            resourceLiveData.postValue(response.body());
                            networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.SUCCESS));
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.NO_DATA));
//                            message.postValue(new Message("No ResourcesConstatns", 2));
                        }
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.ERROR));
//                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchResources(studentCatalogueId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
//                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.RESOURCES, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (resourceRepositoryManager == null)
            resourceRepositoryManager = ResourceRepositoryManager.getInstance();

        resourceRepositoryManager.getResources(studentCatalogueId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    // resource
    public boolean checkIsExist(ResourceModel resourceModel) {
        if (resourceRepositoryManager == null)
            resourceRepositoryManager = ResourceRepositoryManager.getInstance();

        if (resourceRepositoryManager.containsResource(resourceModel.getResourceUrl())) {
            return true;
        }
        return false;
    }


    public FileDownload getFileDownload(ResourceModel resourceModel) {
        if (resourceRepositoryManager == null)
            resourceRepositoryManager = ResourceRepositoryManager.getInstance();

        if (resourceRepositoryManager.containsResource(resourceModel.getResourceUrl())) {
            return resourceRepositoryManager.getFileDownload(resourceModel.getResourceUrl());
        }
        return null;
    }

    public void createResource(ResourceModel resourceModel, FileDownload fileDownload) {
        if (resourceRepositoryManager == null)
            resourceRepositoryManager = ResourceRepositoryManager.getInstance();

        resourceRepositoryManager.createOrUpdateResource(resourceModel.getResourceUrl(), fileDownload);
        resourceRepositoryManager.createOrInsertPendingDownloads(resourceModel.getResourceUrl());
    }

    public void deleteResource(ResourceModel resourceModel) {
        resourceRepositoryManager.deleteResource(resourceModel.getResourceUrl());
        resourceRepositoryManager.removePendingDownloads(resourceModel.getResourceUrl());

    }

}
