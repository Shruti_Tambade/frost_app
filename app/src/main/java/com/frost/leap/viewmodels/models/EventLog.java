package com.frost.leap.viewmodels.models;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 17-05-2020.
 * <p>
 * Frost
 */
public class EventLog  {

    private String key;
    private String value;

    public EventLog(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
