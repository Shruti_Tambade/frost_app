package com.frost.leap.viewmodels.profile;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.LinkedDevice;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class LinkedDeviceViewModel extends BaseViewModel {
    private UserRepositoryManager repositoryManager;
    private MutableLiveData<List<LinkedDevice>> liveData;

    public LinkedDeviceViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
        liveData = new MutableLiveData<>();
    }


    public class NetworkTags {
        public static final int LINKED_DEVICES = 1;
    }

    public LiveData<List<LinkedDevice>> updateLinkedDevices() {
        return liveData;
    }

    public void fetchLinkedDevices() {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.IN_PROCESS));

        Observer<Response<List<LinkedDevice>>> observer = new Observer<Response<List<LinkedDevice>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<LinkedDevice>> response) {
                if (response != null && response.code() == 200) {
                    if (response.body() != null && response.body().size() > 0) {
                        liveData.postValue(response.body());
                    } else {
                        message.postValue(new Message("Linked devices not available", 2));
                        networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.NO_DATA));
                    }
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchLinkedDevices();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.LINKED_DEVICES, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.getUserLinkedDevices()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);


    }
}
