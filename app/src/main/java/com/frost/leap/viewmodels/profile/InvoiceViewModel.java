package com.frost.leap.viewmodels.profile;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.Invoice;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class InvoiceViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;
    private MutableLiveData<List<Invoice>> liveData;

    public InvoiceViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
        liveData = new MutableLiveData<>();
    }

    public class NetworkTags {
        public static final int INVOICE_HISTORY = 1;
    }

    public LiveData<List<Invoice>> updateInvoices() {
        return liveData;
    }

    public void fetchInvoiceHistory() {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.IN_PROCESS));

        Observer<Response<List<Invoice>>> observer = new Observer<Response<List<Invoice>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<Invoice>> response) {
                if (response != null && response.code() == 200) {
                    if (response.body() != null && response.body().size() > 0)
                        liveData.postValue(response.body());
                    else {
                        networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.NO_DATA));
                        message.postValue(new Message("Invoices are not available", 2));
                    }
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchInvoiceHistory();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.ERROR));
                        }

                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.INVOICE_HISTORY, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.getUserInvoices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);


    }


}
