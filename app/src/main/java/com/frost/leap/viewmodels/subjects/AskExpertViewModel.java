package com.frost.leap.viewmodels.subjects;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import com.frost.leap.R;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;

import apprepos.subject.NoteRepositoryManager;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 03-01-2020.
 * <p>
 * FROST
 */
public class AskExpertViewModel extends BaseViewModel {

    private NoteRepositoryManager repositoryManager;

    public AskExpertViewModel() {
        repositoryManager = NoteRepositoryManager.getInstance();
    }


    public boolean doValidation(String subjectName, String unitName, String topicName, String doubt) {

        if (!isNetworkAvailable()) {
            Utility.showToast(Utility.getContext(), Constants.PLEASE_CHECK_INTERNET, false);
            return false;
        }
        if (TextUtils.isEmpty(subjectName)) {
            error.postValue(new Error(R.id.subjectInputLayout, "Please enter subject name"));
            return false;
        }

        if (TextUtils.isEmpty(unitName)) {
            error.postValue(new Error(R.id.unitInputLayout, "Please enter unit name"));
            return false;
        }

        if (TextUtils.isEmpty(topicName)) {
            error.postValue(new Error(R.id.topicInputLayout, "Please enter topic name"));
            return false;
        }

        if (TextUtils.isEmpty(doubt)) {
            error.postValue(new Error(R.id.doubtInputLayout, "Please enter your doubt here"));
            return false;
        }


        return true;
    }


    public boolean doValidation(String doubt) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return false;
        }

        if (TextUtils.isEmpty(doubt)) {
            message.postValue(new Message("Please add doubt description", 2));
            return false;
        }

        return true;
    }

}
