package com.frost.leap.viewmodels.profile;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.R;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.SupportInfo;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 01-10-2019.
 * <p>
 * FROST
 */
public class SubmitRequestViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;
    private MutableLiveData<SupportInfo> liveData;

    private String issueType;
    private String course;
    private String description;
    private MenuItem menuItem = null;


    public SubmitRequestViewModel() {
        super();
        liveData = new MutableLiveData<>();
        repositoryManager = UserRepositoryManager.getInstance();
    }


    public class NetworkTag {
        public static final int FETCH_SUPPORT_TERMS = 1;
        public static final int ADD_FRESH_DESK_TICKET = 2;
    }


    public LiveData<SupportInfo> updateSupportInfo() {
        return liveData;
    }


    public void fetchSupportTerms() {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.FETCH_SUPPORT_TERMS, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTag.FETCH_SUPPORT_TERMS, NetworkStatus.IN_PROCESS));

        Observer<Response<SupportInfo>> observer = new Observer<Response<SupportInfo>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<SupportInfo> response) {
                if (response != null && response.code() == 200) {
                    liveData.postValue(response.body());
                    networkCall.postValue(new NetworkCall(NetworkTag.FETCH_SUPPORT_TERMS, NetworkStatus.SUCCESS));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.FETCH_SUPPORT_TERMS, NetworkStatus.FAIL));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.FETCH_SUPPORT_TERMS, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.getSupportInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);


    }


    public boolean doValidation(String description, MenuItem menuItem, boolean isOptional, String issueType, String course
            , String smartPhoneModel, String andoridVersion) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return false;
        }

        if (menuItem != null) {

            this.menuItem = menuItem;

            if (menuItem.getIssueType().equals("Issue Type")) {
//                message.postValue(new Message("Please choose issue type", 2));
                error.postValue(new Error(R.id.tlissueTypeSpinner, "Please choose issue type"));
                return false;
            }

            if (menuItem.getIssueTopic().equals("Issue Topic")) {
//                message.postValue(new Message("Please choose issue topic", 2));
                error.postValue(new Error(R.id.tlissueTopicSpinner, "Please choose issue topic"));
                return false;
            }

            if (menuItem.getIssueType().equals("Technical Issues")) {
                if (TextUtils.isEmpty(smartPhoneModel)) {
//                    message.postValue(new Message("Please add Smart Phone Model", 2));
                    error.postValue(new Error(R.id.tlSmartPhoneModel, "Please add Smart Phone Model"));
                    return false;

                }
                if (TextUtils.isEmpty(andoridVersion)) {
//                    message.postValue(new Message("Please Android Version", 2));
                    error.postValue(new Error(R.id.tlAndroidVersion, "Please enter Android Version"));
                    return false;
                }
            }
            if (TextUtils.isEmpty(description)) {
//                message.postValue(new Message("Please add message", 2));
                error.postValue(new Error(R.id.tlMessage, "Please add message"));
                return false;
            }
            if (!isOptional)
                if (course.equals("Choose Course")) {
//                    message.postValue(new Message("Please choose course", 2));
                    error.postValue(new Error(R.id.tlcourseSpinner, "Please choose course"));
                    return false;
                }

        } else {

            if (issueType.equals("Issue Type")) {
//                message.postValue(new Message("Please choose issue type", 2));
                error.postValue(new Error(R.id.tlissueTypeSpinner, "Please choose issue type"));
                return false;
            }
            if (TextUtils.isEmpty(description.trim())) {
//                message.postValue(new Message("Please add message", 2));
                error.postValue(new Error(R.id.tlMessage, "Please add message"));
                return false;
            }
            if (!isOptional)
                if (course.equals("Choose Course")) {
//                    message.postValue(new Message("Please choose course", 2));
                    error.postValue(new Error(R.id.tlcourseSpinner, "Please choose course"));
                    return false;
                }
        }

        this.issueType = issueType;
        this.description = description.trim();
        this.course = course;

        return true;
    }


    public void addFreshDeskTicket(List<GalleryFile> list) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTag.ADD_FRESH_DESK_TICKET, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTag.ADD_FRESH_DESK_TICKET, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                if (response.code() == 201 || response.code() == 200) {
                    message.postValue(new Message("Ticket added, we will back you soon", 1));
                    networkCall.postValue(new NetworkCall(NetworkTag.ADD_FRESH_DESK_TICKET, NetworkStatus.SUCCESS));
                } else {
                    message.postValue(new Message("Failed to add fresh desk ticket", 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.ADD_FRESH_DESK_TICKET, NetworkStatus.FAIL));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.ADD_FRESH_DESK_TICKET, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.addFreshDeskTicket(issueType, description, course, list, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);


    }


}
