package com.frost.leap.viewmodels.quiz;


import android.os.Bundle;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import apprepos.quiz.QuizRepositoryManager;
import apprepos.quiz.model.QuizQuestionsModel;
import apprepos.quiz.model.QuizReportModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 06-09-2019.
 * <p>
 * Frost
 */
public class QuizViewModel extends BaseViewModel {
    private QuizRepositoryManager repositoryManager;
    private MutableLiveData<QuizQuestionsModel> quizQuestionsModelMutableLiveData;

    public QuizViewModel() {
        super();
        repositoryManager = QuizRepositoryManager.getInstance();
        quizQuestionsModelMutableLiveData = new MutableLiveData<>();
    }


    public class NetworkTags {
        public static final int QUIZ_QUESTIONS = 1;
        public static final int SUBMIT_QUIZ = 2;
        public static final int UPDATE_QUIZ_STATUS = 3;

    }

    public LiveData<QuizQuestionsModel> updateQuizQuestions() {
        return quizQuestionsModelMutableLiveData;
    }

    public void getQuizQuestions(String quizId) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.IN_PROCESS));
        Observer<Response<QuizQuestionsModel>> observer = new Observer<Response<QuizQuestionsModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<QuizQuestionsModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        response.body().setTopicId("topicId");
                        quizQuestionsModelMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.NO_DATA));
                        message.postValue(new Message(Constants.NO_DATA_AVAILABLE, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    message.postValue(new Message("Token Expired", 0));

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.QUIZ_QUESTIONS, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = QuizRepositoryManager.getInstance();

        repositoryManager.getQuizQuestions(quizId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);

    }

    public void submitQuiz(QuizQuestionsModel quizQuestionsModel) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.SUBMIT_QUIZ, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.SUBMIT_QUIZ, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                if (response != null && response.code() == 200) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<QuizReportModel>() {
                    }.getType();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("QuizReportModel", gson.fromJson(response.body().get("quizReport"), type));
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBMIT_QUIZ, bundle, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 401) {
                    message.postValue(new Message("Token Expired", 0));
                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBMIT_QUIZ, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.SUBMIT_QUIZ, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = QuizRepositoryManager.getInstance();

        repositoryManager.submitQuiz(quizQuestionsModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void updateQuizStatus(String subjectId, String unitId, String topicId, String quizId) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observable = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.NO_DATA));
                        message.postValue(new Message(Constants.NO_DATA_AVAILABLE, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    message.postValue(new Message("Token Expired", 0));

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_QUIZ_STATUS, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = QuizRepositoryManager.getInstance();


        repositoryManager.updateStatusQuiz(subjectId, unitId, topicId, quizId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observable);

    }


}
