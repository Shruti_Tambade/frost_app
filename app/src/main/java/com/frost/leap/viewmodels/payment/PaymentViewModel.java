package com.frost.leap.viewmodels.payment;

import android.os.Bundle;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.store.StoreRepositoryManager;
import apprepos.store.model.CouponModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 10-10-2019.
 * <p>
 * FROST
 */
public class PaymentViewModel extends BaseViewModel {

    private StoreRepositoryManager repositoryManager;
    private MutableLiveData<CouponModel> couponModelMutableLiveData;

    public PaymentViewModel() {
        super();
        repositoryManager = StoreRepositoryManager.getInstance();
        couponModelMutableLiveData = new MutableLiveData<>();
    }

    public class NetworkTag {
        public static final int GENERATE_ORDER = 1;
        public static final int UPDATE_ORDER_STATUS = 2;
        public static final int GET_COUPON_INFO = 3;
    }

    public LiveData<CouponModel> updateCouponData() {
        return couponModelMutableLiveData;
    }


    public CatalogueModel getStoreCatalogue() {
        return ShareDataManager.getInstance().getStoreOverviewData().getCatalogueModel();
    }

    public void generateOrder(String catalogueId, String name, String itemPriceId, String couponId) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {

                    Bundle bundle = new Bundle();
                    JSONObject paymentObject = new JSONObject();
                    try {
                        paymentObject.put("order_id", response.body().get("orderId").getAsString());
                        paymentObject.put("customer_id", response.body().get("customerId").getAsString());
                        paymentObject.put("description", name);
                        bundle.putString("Json", paymentObject.toString());
                        bundle.putString("OrderId", response.body().get("orderId").getAsString());
                        networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, bundle, NetworkStatus.SUCCESS));
                    } catch (Exception e) {
                        e.printStackTrace();
                        networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, NetworkStatus.FAIL));
                        message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            generateOrder(catalogueId, name, itemPriceId, couponId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                networkCall.postValue(new NetworkCall(NetworkTag.GENERATE_ORDER, NetworkStatus.ERROR));
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        repositoryManager.generateOrder(catalogueId, name, itemPriceId, couponId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void updateOrderStatus(String paymentId, String orderId) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.UPDATE_ORDER_STATUS, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.UPDATE_ORDER_STATUS, NetworkStatus.SUCCESS));
                    message.postValue(new Message("Payment is successful", 2));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTag.UPDATE_ORDER_STATUS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateOrderStatus(paymentId, orderId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTag.UPDATE_ORDER_STATUS, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.UPDATE_ORDER_STATUS, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                networkCall.postValue(new NetworkCall(NetworkTag.UPDATE_ORDER_STATUS, NetworkStatus.ERROR));
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        repositoryManager.updateOrderStatus(orderId, paymentId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }

    public void getCouponResponse(String catalogueId, String couponName) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.IN_PROCESS));
        Observer<Response<CouponModel>> observer = new Observer<Response<CouponModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<CouponModel> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.DONE));
                    if (response.body() != null) {
                        couponModelMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.SUCCESS));
                        message.postValue(new Message("Coupon Applied Successfully", 1));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.NO_DATA));
                        message.postValue(new Message("Invalid coupon code", 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            getCouponResponse(catalogueId, couponName);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                networkCall.postValue(new NetworkCall(NetworkTag.GET_COUPON_INFO, NetworkStatus.ERROR));
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        repositoryManager.getCouponInfo(catalogueId, couponName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    public String getHlsVideoByCatalogueId(String catalogueId, int position) {

        if (catalogueId == null)
            return null;

        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        JsonObject jsonObject = repositoryManager.getHlsVideos();
        if (jsonObject == null)
            return null;

        if (jsonObject.has(catalogueId)) {
            if (jsonObject.get(catalogueId).isJsonArray() && jsonObject.get(catalogueId).getAsJsonArray().size() > position) {
                return jsonObject.get(catalogueId).getAsJsonArray().get(position).getAsString();
            } else {
                return null;
            }

        } else {
            return null;
        }


    }

}
