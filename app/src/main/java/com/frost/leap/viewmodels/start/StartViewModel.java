package com.frost.leap.viewmodels.start;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.R;
import com.frost.leap.components.start.VerifyType;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;


/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 27-09-2019.
 * <p>
 * FROST
 */

public class StartViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;
    private String mobileNumber;
    private String otpCode;
    private MutableLiveData<Boolean> clearLiveData;

    public StartViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
        clearLiveData = new MutableLiveData<>();
    }


    public class NetworkTag {
        public static final int REQUEST_LOGIN = 1;

        public static final int VERIFY_LOGIN_OTP = 2;

        public static final int USER_DETAILS = 3;

        public static final int REGISTER_USER = 4;

        public static final int CHECK_USER_EXIST = 5;
    }

    public LiveData<Boolean> clearUserData() {
        return clearLiveData;
    }


    // SIGN IN
    public boolean doMobileValidation(String mobileNumber) {

        if (TextUtils.isEmpty(mobileNumber)) {
            error.postValue(new Error(R.id.tlPhoneNumber, "Phone number cannot be empty"));
            return false;
        }


        if (!Utility.isValidMobile(mobileNumber)) {
            error.postValue(new Error(R.id.tlPhoneNumber, "The phone number you have entered is invalid"));
            return false;
        }
        this.mobileNumber = mobileNumber;
        return true;
    }


    public void requestLogin() {

        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOGIN, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOGIN, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {


                if (response != null && response.code() == 200) {

                    String otpWord = response.headers().get("otplength");
                    if (otpWord != null) {
                        repositoryManager.setOtpWordCount(otpWord);
                    }
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOGIN, NetworkStatus.SUCCESS));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOGIN, NetworkStatus.FAIL));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOGIN, NetworkStatus.FAIL));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();

                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOGIN, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.requestLogin(mobileNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    public boolean doOTPValidation(String otpCode) {
        if (TextUtils.isEmpty(otpCode)) {
            message.postValue(new Message("Verification code cannot be empty", 3));
            return false;
        } else if (otpCode.length() < 6) {
            message.postValue(new Message("Please enter a valid 6 digit verification code", 3));
            return false;

        }

        this.otpCode = otpCode;
        return true;
    }


    public void verifyLoginOTP(String value) {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    User user = null;
                    if ((user = repositoryManager.getUser()) != null) {
                        if (user.getCredentials().getMobile() != null && !user.getCredentials().getMobile().equals(value)) {
                            deleteUserData();
                            clearLiveData.postValue(true);
                        }
                    }
                    repositoryManager.saveUserInfo(response.headers().get(Constants.AUTHORIZATION), response.body());
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.UNAUTHORIZED));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.FAIL));
                } else { //
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.FAIL));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();

                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_LOGIN_OTP, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.verifyLoginOTP(value, otpCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }


    public void resendOTP(VerifyType verifyType, String value) {
        switch (verifyType) {
            case LOGIN:
                this.mobileNumber = value;
                requestLogin();
                break;
        }
    }


    // REGISTER
    public void registerUser(User user) {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    deleteUserData();
                    clearLiveData.postValue(true);
                    repositoryManager.saveUserInfo(response.headers().get(Constants.AUTHORIZATION), response.body());
                    networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.UNAUTHORIZED));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.FAIL));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.FAIL));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();

                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.REGISTER_USER, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.registerUser(user, otpCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void getUserDetails() {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.IN_PROCESS));

        Observer<Response<User>> observer = new Observer<Response<User>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<User> response) {
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        repositoryManager.saveUserDetails(response.body());

                        Bundle bundle = new Bundle();
                        bundle.putParcelable("User", response.body());

                        networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, bundle, NetworkStatus.SUCCESS));
                    } else {
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                        networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.FAIL));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    message.postValue(new Message("Token Expired", 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.FAIL));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.FAIL));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.USER_DETAILS, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.getUserDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public void checkUserExist(User user) {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.FAIL));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.FAIL));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.FAIL));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();

                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.CHECK_USER_EXIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.checkUserExist(user.getCredentials().getEmail(), user.getCredentials().getMobile())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    public int getOtpWordCount() {
        return repositoryManager.getOtpWordCount();
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }


    private boolean getData() {
        return true;
    }


}
