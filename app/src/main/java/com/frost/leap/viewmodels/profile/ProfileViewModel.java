package com.frost.leap.viewmodels.profile;

import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.viewmodels.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class ProfileViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;

    public UserRepositoryManager getRepositoryManager() {
        return repositoryManager;
    }

    public ProfileViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
    }

    public List<MenuItem> generateProfileMenu() {
        List<MenuItem> list = new ArrayList<>();
        list.add(null);

        list.add(new MenuItem(8, "Help", "We are here to help", "Support"));
        list.add(new MenuItem(1, "Profile Settings", "Update and modify your profile", "Personal"));
        list.add(new MenuItem(11, "Additional Settings", "More information about you", null));
        list.add(new MenuItem(2, "Subscription Details", "Download invoices", "Orders & Billing"));
//        list.add(new MenuItem(3, "Notifications", "Change your notification settings", "Application"));
//        list.add(new MenuItem(4, "Data Usage", "Control data usage", null));
//        list.add(new MenuItem(5, "Display Theme", "Manage theme", null));
//        list.add(new MenuItem(6, "Linked Devices", "Mange & Control", "Application"));
        list.add(new MenuItem(12, "In-App Download Settings", "Update and modify default settings", "Application"));
        list.add(new MenuItem(7, "About Deep Learn", "Terms & policies", "General"));
//        list.add(new MenuItem(9, "FAQ", "Frequently asked questions", null));
        list.add(new MenuItem(10, "Logout", "Logout of the application", null));

        return list;
    }


    public User getUser() {
        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        return repositoryManager.getUser();
    }


    public void setSecurityLevel(String level){
        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.setSecurityLevel(level);
    }


}
