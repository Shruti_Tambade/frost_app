package com.frost.leap.viewmodels.pdf;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Logger;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 10-06-2020.
 * <p>
 * Frost
 */
public class PdfViewModel extends BaseViewModel {

    private Disposable disposable;
    private MutableLiveData<File> fileMutableLiveData;

    public PdfViewModel() {
        super();
        fileMutableLiveData = new MutableLiveData<>();
    }

    public class NetworkTags {
        public static final int FILE_DOWNLOAD = 1;
    }

    public LiveData<File> getPdfFile() {
        return fileMutableLiveData;
    }

    public void downloadPdfFile(String pdfLink, File file) {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTags.FILE_DOWNLOAD, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.FILE_DOWNLOAD, NetworkStatus.IN_PROCESS));
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Observable<File> observable = Observable.create(emitter ->
        {
            try {
                URL url = new URL(pdfLink);// Create Download URl
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                // Open Url Connection
                httpURLConnection.setRequestMethod("GET");
                //Set Request Method to "GET" since we are getting data
                httpURLConnection.connect();//connect the URL Connection
                //If Connection response is not OK then show Logs
                if (httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Logger.d("FILE DOWNLOAD", "Server returned HTTP " + httpURLConnection.getResponseCode() + " " + httpURLConnection.getResponseMessage());
                    emitter.onError(new Exception());
                    return;
                }
                FileOutputStream fos = new FileOutputStream(file, false);//Get OutputStream for NewFile Location
                InputStream is = httpURLConnection.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[Constants.BUFFER_SIZE];//Set buffer type
                int length = 0;

                while ((length = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, length);//Write new file
                }
                fos.close();
                is.close();
                fos.flush();
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                emitter.onNext(file);
                emitter.onComplete();

            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }
        });

        Observer<File> observer = new Observer<File>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(File file) {
                if (file != null && file.exists()) {
                    networkCall.postValue(new NetworkCall(NetworkTags.FILE_DOWNLOAD, NetworkStatus.SUCCESS));
                    fileMutableLiveData.postValue(file);
                }
            }

            @Override
            public void onError(Throwable e) {
                networkCall.postValue(new NetworkCall(NetworkTags.FILE_DOWNLOAD, NetworkStatus.ERROR));
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);


    }


    public void clearAll() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }


}
