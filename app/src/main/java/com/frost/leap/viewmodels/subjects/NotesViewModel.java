package com.frost.leap.viewmodels.subjects;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import apprepos.subject.NoteRepositoryManager;
import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;

/**
 * Created by CHENNA RAO on 2020-01-03.
 * Frost Interactive
 */
public class NotesViewModel extends BaseViewModel {

    private MutableLiveData<List<Note>> notesLiveData;
    public List<Note> noteList;
    private NoteRepositoryManager repositoryManager;
    private MutableLiveData<List<SubjectInfo>> subjectsInfoLiveData;
    private Disposable disposable;
    public boolean hasFilters = false;
    public String filterSubjectId, filterUnitId, filterTopicId;

    public NotesViewModel() {
        super();
        notesLiveData = new MutableLiveData<>();
        subjectsInfoLiveData = new MutableLiveData<>();
        repositoryManager = NoteRepositoryManager.getInstance();
    }

    public static final class NetworkTags {

        public static final int NOTES_LIST = 1;

        public static final int SUBJECTS_INFO_LIST = 2;
    }

    public LiveData<List<Note>> updateNotesList() {
        return notesLiveData;
    }


    public LiveData<List<SubjectInfo>> updateSubjectsInfo() {
        return subjectsInfoLiveData;
    }

    public void fetchNotes(List<String> studentSubjectIds) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.IN_PROCESS));
        Observer<Response<List<Note>>> observer = new Observer<Response<List<Note>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @SuppressLint("StaticFieldLeak")
            @Override
            public void onNext(Response<List<Note>> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            noteList = response.body();
                            if (hasFilters) {
                                filterNotes(filterSubjectId, filterUnitId, filterTopicId);
                                return;
                            }

                            notesLiveData.postValue(noteList);
                            networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.SUCCESS));

                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.NO_DATA));
                            message.postValue(new Message("No notes added", 2));
                        }
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchNotes(studentSubjectIds);
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.SERVER_ERROR));
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                        }
                    });

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = NoteRepositoryManager.getInstance();

        JsonObject jsonObject = new JsonObject();

        jsonObject.add("studentSubjectIds", new Gson().toJsonTree(studentSubjectIds));

        repositoryManager.getUserNotes(jsonObject)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void fetchSubjectsInfo(String catalogueId) {

        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_INFO_LIST, NetworkStatus.NO_INTERNET));
            return;
        }

        if (disposable != null && !disposable.isDisposed())
            disposable.dispose();

        Observer<Response<List<SubjectInfo>>> observer = new Observer<Response<List<SubjectInfo>>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(Response<List<SubjectInfo>> response) {
                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_INFO_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    subjectsInfoLiveData.postValue(response.body());
                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_INFO_LIST, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_INFO_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = NoteRepositoryManager.getInstance();


        repositoryManager.getCatalogueSubjectsInfo(catalogueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    @SuppressLint("StaticFieldLeak")
    public void filterNotes(String filterSubjectId, String filterUnitId, String filterTopicId) {
        hasFilters = true;
        if (filterSubjectId == null) {
            clearAllFilters();
            return;
        }
        this.filterSubjectId = filterSubjectId;
        this.filterUnitId = filterUnitId;
        this.filterTopicId = filterTopicId;

        new AsyncTask<Void, Void, List<Note>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.IN_PROCESS));
            }

            @Override
            protected List<Note> doInBackground(Void... data) {
                List<Note> list = new ArrayList<>();
                for (int i = 0; i < noteList.size(); i++) {

                    if (filterSubjectId.equals(noteList.get(i).getStudentSubjectId())) {

                        if (filterUnitId == null) {
                            list.add(noteList.get(i));
                        } else {
                            if (filterUnitId.equals(noteList.get(i).getStudentUnitId())) {
                                if (filterTopicId == null) {
                                    list.add(noteList.get(i));
                                } else {
                                    if (filterTopicId.equals(noteList.get(i).getStudentTopicId())) {
                                        list.add(noteList.get(i));
                                    }
                                }
                            }
                        }
                    }
                }

                return list;
            }

            @Override
            protected void onPostExecute(List<Note> notes) {
                super.onPostExecute(notes);
                notesLiveData.postValue(notes);
                networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.SUCCESS));
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void clearAllFilters() {
        hasFilters = false;
        filterSubjectId = null;
        filterUnitId = null;
        filterTopicId = null;

        notesLiveData.postValue(noteList);
        networkCall.postValue(new NetworkCall(NetworkTags.NOTES_LIST, NetworkStatus.SUCCESS));
    }


    public void updateJsonObject(Note note, boolean delete) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... data) {

                for (int i = 0; i < noteList.size(); i++) {
                    if (note.getStudentNoteId().equals(noteList.get(i).getStudentNoteId())) {
                        if (delete) {
                            noteList.remove(i);
                            return null;
                        } else {
                            noteList.set(i, note);
                            return null;

                        }
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void data) {
                super.onPostExecute(data);
            }
        }.execute();
    }

}
