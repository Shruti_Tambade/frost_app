package com.frost.leap.viewmodels.start;

import android.text.TextUtils;

import com.frost.leap.R;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import apprepos.user.UserRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 30-09-2019.
 * <p>
 * FROST
 */
public class LostMobileViewModel extends BaseViewModel {

    private String userId;
    private String mobileNumber;
    private String email;
    private String otpCode;
    private UserRepositoryManager repositoryManager;

    public LostMobileViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
    }


    public class NetworkTag {
        public static final int REQUEST_LOST_MOBILE = 1;

        public static final int RESEND_EMAIL_OTP = 2;

        public static final int VERIFY_EMAIL_OTP = 3;

        public static final int REQUEST_ALTERNATIVE_NUMBER = 4;

        public static final int REQUEST_ALTERNATIVE_NUMBER_AGAIN = 5;

        public static final int VERIFY_MOBILE_OTP = 6;

    }


    public boolean doEmailValidation(String email) {
        if (TextUtils.isEmpty(email)) {
            error.postValue(new Error(R.id.tlEmailAddress, "Email address cannot be empty"));
            return false;
        }
        if (!Utility.isValidEmail(email)) {
            error.postValue(new Error(R.id.tlEmailAddress, "Please enter a valid email address"));
            return false;
        }
        this.email = email;
        return true;
    }


    public boolean doMobileValidation(String mobileNumber) {

        if (TextUtils.isEmpty(mobileNumber)) {
            error.postValue(new Error(R.id.tlPhoneNumber, "Phone number cannot be empty"));
            return false;
        }
        if (!Utility.isValidMobile(mobileNumber)) {
            error.postValue(new Error(R.id.tlPhoneNumber, "The phone number you have entered is invalid"));
            return false;
        }

        this.mobileNumber = mobileNumber;
        return true;

    }

    public boolean doOTPValidation(String otpCode) {
        if (TextUtils.isEmpty(otpCode)) {
            message.postValue(new Message("Verification code cannot be empty", 3));
            return false;
        } else if (otpCode.length() < 6) {
            message.postValue(new Message("Please enter a valid 6 digit verification code", 3));
            return false;
        }

        this.otpCode = otpCode;
        return true;
    }


    public void requestLostPhone() {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.FAIL));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.ERROR));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_LOST_MOBILE, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.requestLostPhone(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void resentEmailOtp(String email) {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    String errorMessage = null;
                    message.postValue(new Message(errorMessage = Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.FAIL));
                    Mixpanel.verificationOTPResendRecovery(email, errorMessage);
                } else if (response.code() == 401) {
                    String errorMessage = null;
                    message.postValue(new Message(errorMessage = Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.FAIL));
                    Mixpanel.verificationOTPResendRecovery(email, errorMessage);
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.RESEND_EMAIL_OTP, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.requestLostPhone(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void verifyEmailOtp(String email) {

        final String[] errorMessage = {""};

        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(errorMessage[0] = Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.FAIL));
                    Mixpanel.recoveryEmailOTPVerfication(email, errorMessage[0]);
                } else if (response.code() == 401) {
                    message.postValue(new Message(errorMessage[0] = Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.FAIL));
                    Mixpanel.recoveryEmailOTPVerfication(email, errorMessage[0]);
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_EMAIL_OTP, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.verifyEmailOtp(email, otpCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void requestAlternativeNumber() {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    userId = response.body().get("message").getAsString();
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.FAIL));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.ERROR));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.updateAlternativeNumber(email, mobileNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void requestAlternativeNumberAgain() {
        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.FAIL));
                } else if (response.code() == 401) {
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.ERROR));
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.updateAlternativeNumber(email, mobileNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    public void verifyMobileOtp() {

        final String[] errorMessage = {""};

        if (!isNetworkAvailable()) {
            networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.NO_INTERNET));
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.IN_PROCESS));


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.SUCCESS));
                } else if (response != null && response.code() == 500) {
                    message.postValue(new Message(errorMessage[0] = Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.FAIL));
                    Mixpanel.recoveryMobileOTPVerfication(email, mobileNumber, errorMessage[0]);

                } else if (response.code() == 401) {
                    message.postValue(new Message(errorMessage[0] = Utility.showErrorMessage(response.errorBody()), 0));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.FAIL));
                    Mixpanel.recoveryMobileOTPVerfication(email, mobileNumber, errorMessage[0]);
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTag.VERIFY_MOBILE_OTP, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.verifyAlternativeNumberOTP(mobileNumber, userId, otpCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


}
