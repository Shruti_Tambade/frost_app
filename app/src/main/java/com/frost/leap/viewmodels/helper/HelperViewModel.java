package com.frost.leap.viewmodels.helper;

import android.net.Uri;

import com.frost.leap.applications.HelperApplication;
import com.frost.leap.viewmodels.BaseViewModel;
import com.google.android.exoplayer2.offline.Download;

import java.util.HashMap;

import apprepos.topics.TopicRepositoryManager;
import apprepos.user.UserRepositoryManager;

public class HelperViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;
    private boolean isLoaded = false;


    public HelperViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
    }


    public boolean isLogin() {
        return repositoryManager.isLogin();
    }

    public boolean clearUserDownloads() {
        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();
        if (repositoryManager.getUser().getStudentId() == null) {
            HashMap<Uri, Download> downloadHashMap = HelperApplication.getInstance().getDownloadTracker().getAllDownloads();
            if (downloadHashMap != null && downloadHashMap.size() > 0) {
                return true;
            }
        }

        return false;
    }


    public boolean getAskMeEveryTime() {
        return repositoryManager.getAskMeEveryTime();
    }


    public void setAskMeEveryTime(boolean value) {
        repositoryManager.setAskMeEveryTime(value);
    }


    public int getSelectedDownloadResolution() {
        return repositoryManager.getDownloadResolution();
    }


    public void setSelectedDownloadResolution(int quality) {
        repositoryManager.setDownloadResolution(quality);
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
    }


    public void MigrationLicenseToDB(){
        TopicRepositoryManager.getInstance().migrationLicensesToDB();
    }
}
