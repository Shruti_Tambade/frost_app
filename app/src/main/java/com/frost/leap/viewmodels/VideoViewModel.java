package com.frost.leap.viewmodels;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.source.dash.DashUtil;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.upstream.ByteArrayDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.JsonObject;

import java.io.IOException;

import apprepos.video.model.video.DashUrlsModel;
import apprepos.video.model.video.PageContentDataModel;
import apprepos.video.VideoRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 9/27/2019.
 */
public class VideoViewModel extends BaseViewModel {

    private VideoRepositoryManager repositoryManager;
    private MutableLiveData<PageContentDataModel> pageContentDataMutableLiveData;
    private MutableLiveData<String> tokenLiveData;

    public VideoViewModel() {
        super();
        pageContentDataMutableLiveData = new MutableLiveData<>();
        repositoryManager = VideoRepositoryManager.getInstance();
        tokenLiveData = new MutableLiveData<>();
    }

    public static final class NetworkTags {

        public static final int VIDEO_URL = 1;
        public static final int VIDEO_STREAM_UPDATE = 2;
        public static final int XML_TOKEN = 3;
    }

    public LiveData<PageContentDataModel> updateVideoUrl() {
        return pageContentDataMutableLiveData;
    }

    public void updateVideoStream(String studentSubjectId,
                                  String unitId,
                                  String topicId,
                                  int videoCompletion,
                                  String subTopicId) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
//                        message.postValue(new Message(response.message(), 2));
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = VideoRepositoryManager.getInstance();

        repositoryManager.updateVideoStream(studentSubjectId, unitId, topicId, videoCompletion, subTopicId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);

    }


    public void fetchVideoUrl(String pageContentId) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.IN_PROCESS));
        Observer<Response<PageContentDataModel>> observer = new Observer<Response<PageContentDataModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<PageContentDataModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        pageContentDataMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
//                    Toast.makeText(Utility.getContext(), "Token expired", Toast.LENGTH_SHORT).show();
                    Utility.showToast(Utility.getContext(), "Token expired", false);

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = VideoRepositoryManager.getInstance();

        repositoryManager.getVideoUrl(pageContentId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);

    }

    public void fetchXmlToken() {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        tokenLiveData.postValue(response.body().get("authToken").getAsString());
                        Bundle bundle = new Bundle();
                        bundle.putString("CDATA", response.body().get("authToken").getAsString());
                        networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, bundle, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
//                    Toast.makeText(Utility.getContext(), "Token expired", Toast.LENGTH_SHORT).show();
                    Utility.showToast(Utility.getContext(), "Token expired", false);
                    networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = VideoRepositoryManager.getInstance();

        repositoryManager.getXmlToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }




}
