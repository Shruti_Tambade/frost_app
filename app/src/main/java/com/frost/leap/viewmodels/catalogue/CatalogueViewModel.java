package com.frost.leap.viewmodels.catalogue;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.util.List;

import apprepos.catalogue.CatalogueRepositoryManager;
import apprepos.catalogue.model.CatalogueModel;
import apprepos.user.UserRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;


/**
 * Created by Chenna Rao on 8/22/2019.
 */
public class CatalogueViewModel extends BaseViewModel {

    private MutableLiveData<List<CatalogueModel>> catalogueLiveData;
    private CatalogueRepositoryManager catalogueRepository;

    public CatalogueViewModel() {
        super();
        catalogueLiveData = new MutableLiveData<>();
        catalogueRepository = CatalogueRepositoryManager.getInstance();
    }

    public CatalogueRepositoryManager getRepository() {
        return catalogueRepository;
    }

    public UserRepositoryManager getUserRepository() {
        return UserRepositoryManager.getInstance();
    }


    public static final class NetworkTags {

        public static final int CATALOGUE_LIST = 1;
        public static final int TOPICS_LIST = 2;
    }

    public LiveData<List<CatalogueModel>> updateCatalogueData() {
        return catalogueLiveData;
    }

    public void fetchCatalogues() {

        if (!isNetworkAvailable()) {
            if (catalogueRepository == null)
                catalogueRepository = CatalogueRepositoryManager.getInstance();
            catalogueRepository.getOfflineCatalogues()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(offlineCatalogues -> {
                        if (offlineCatalogues != null && offlineCatalogues.size() > 0) {
                            catalogueLiveData.postValue(offlineCatalogues);
                            return;
                        }
                        message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
                        networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.NO_INTERNET));

                    });
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.IN_PROCESS));
        Observer<Response<List<CatalogueModel>>> observer = new Observer<Response<List<CatalogueModel>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<CatalogueModel>> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        catalogueRepository.updateUserCatalogues(response.body());
                        if (response.body().size() > 0) {
                            catalogueLiveData.postValue(response.body());
                            networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.SUCCESS));
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.NO_DATA));
                            message.postValue(new Message("No Catalogues", 2));
                        }
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchCatalogues();
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.SERVER_ERROR));
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                        }
                    });

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response != null ? response.errorBody() : null), 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (catalogueRepository == null)
            catalogueRepository = CatalogueRepositoryManager.getInstance();

        catalogueRepository.getCatalogues()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }


}
