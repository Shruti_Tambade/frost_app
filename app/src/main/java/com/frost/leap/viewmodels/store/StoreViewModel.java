package com.frost.leap.viewmodels.store;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.catalogue.model.SubCategoryModel;
import apprepos.store.StoreRepositoryManager;
import apprepos.store.model.Banner;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.store.model.SubscriptionModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 9/30/2019.
 */
public class StoreViewModel extends BaseViewModel {

    private MutableLiveData<List<SubCategoryModel>> catalogueLiveData;
    private MutableLiveData<SubscriptionModel> subscriptionModelMutableLiveData;
    private StoreRepositoryManager repositoryManager;
    private MutableLiveData<CatalogueOverviewModel> catalogueOverviewModelMutableLiveData;

    public StoreViewModel() {
        super();
        catalogueLiveData = new MutableLiveData<>();
        subscriptionModelMutableLiveData = new MutableLiveData<>();
        catalogueOverviewModelMutableLiveData = new MutableLiveData<>();
        repositoryManager = StoreRepositoryManager.getInstance();
    }

    public StoreRepositoryManager getRepository() {
        return repositoryManager;
    }


    public CatalogueModel getStoreCatalogue() {
        return ShareDataManager.getInstance().getStoreOverviewData().getCatalogueModel();
    }


    public String getStoreCatalogueName() {
        return ShareDataManager.getInstance().getStoreOverviewData().getCatalogueName();
    }

    public LiveData<CatalogueOverviewModel> updateSubjectsData() {
        return catalogueOverviewModelMutableLiveData;
    }

    public static final class NetworkTags {

        public static final int STORE_CATALOGUES = 1;

        public static final int SUBJECTS_LIST = 2;

        public static final int CATALOGUE_SUBSCRIPTION_STATUS = 3;
    }

    public LiveData<List<SubCategoryModel>> updateStoreData() {
        return catalogueLiveData;
    }

    public LiveData<SubscriptionModel> getSubscription() {
        return subscriptionModelMutableLiveData;
    }

    public void fetchStoreData() {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.IN_PROCESS));
        Observer<Response<List<SubCategoryModel>>> observer = new Observer<Response<List<SubCategoryModel>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<SubCategoryModel>> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            catalogueLiveData.postValue(response.body());
                            networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.SUCCESS));
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.NO_DATA));
                            message.postValue(new Message("No Catalogues", 2));
                        }
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchStoreData();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.STORE_CATALOGUES, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        repositoryManager.getStoreCatalogues()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void fetchSubjects(String studentCatalogueId) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.IN_PROCESS));
        Observer<Response<CatalogueOverviewModel>> observer = new Observer<Response<CatalogueOverviewModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<CatalogueOverviewModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        catalogueOverviewModelMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchSubjects(studentCatalogueId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
//                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        repositoryManager.getStoreSubjects(studentCatalogueId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void fetchCatalogueSubscriptionStatus(String catalogueId, String studentId) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.IN_PROCESS));
        Observer<Response<SubscriptionModel>> observer = new Observer<Response<SubscriptionModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Response<SubscriptionModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        subscriptionModelMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchCatalogueSubscriptionStatus(catalogueId, studentId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.CATALOGUE_SUBSCRIPTION_STATUS, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {
            }
        };

        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();

        repositoryManager.getSubscriptionStatus(catalogueId, studentId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);

    }


    public Banner getStoreBanner() {
        if (repositoryManager == null)
            repositoryManager = StoreRepositoryManager.getInstance();


        return repositoryManager.getStoreBanner();
    }
}
