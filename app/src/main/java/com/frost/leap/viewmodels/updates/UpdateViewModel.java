package com.frost.leap.viewmodels.updates;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import java.util.List;

import apprepos.updates.UpdateRepositoryManager;
import apprepos.updates.model.UpdateModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Logger;
import supporters.utils.Utility;


/**
 * Created by Chenna Rao on 06/01/2020.
 */
public class UpdateViewModel extends BaseViewModel {

    private MutableLiveData<List<UpdateModel>> updateLieveData;
    private MutableLiveData<UpdateModel> updateModelMutableLiveData;
    private UpdateRepositoryManager updateRepositoryManager;

    public UpdateViewModel() {
        super();
        updateLieveData = new MutableLiveData<>();
        updateModelMutableLiveData = new MutableLiveData<>();
        updateRepositoryManager = UpdateRepositoryManager.getInstance();
    }


    public static final class NetworkTags {

        public static final int UPDATES_LIST = 1;
        public static final int GET_DATA_BY_UPDATEID = 2;
        public static final int JOIN_LIVE_SESSION = 3;
    }

    public LiveData<List<UpdateModel>> getUpdatesList() {
        return updateLieveData;
    }

    public LiveData<UpdateModel> getUpdateDataByID() {
        return updateModelMutableLiveData;
    }

    public void fetchUpdateDateByID(String updateId) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.IN_PROCESS));
        Observer<Response<UpdateModel>> observer = new Observer<Response<UpdateModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<UpdateModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        updateModelMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.SUCCESS));

                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchUpdateDateByID(updateId);
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.SERVER_ERROR));
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                        }
                    });

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response != null ? response.errorBody() : null), 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                Logger.d("Erroe", e.getMessage());
                networkCall.postValue(new NetworkCall(NetworkTags.GET_DATA_BY_UPDATEID, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (updateRepositoryManager == null)
            updateRepositoryManager = UpdateRepositoryManager.getInstance();

        updateRepositoryManager.getUpdateDataByID(updateId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void fetchUpdatesList(List<String> catalogueIDS, String date, String endDate) {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.IN_PROCESS));
        Observer<Response<List<UpdateModel>>> observer = new Observer<Response<List<UpdateModel>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<UpdateModel>> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            updateLieveData.postValue(response.body());
                            networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.SUCCESS));
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.NO_DATA));
                            message.postValue(new Message("No Updates", 2));
                        }
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchUpdatesList(catalogueIDS, date, endDate);
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.SERVER_ERROR));
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                        }
                    });

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response != null ? response.errorBody() : null), 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                Logger.d("Error", e.getMessage());
                networkCall.postValue(new NetworkCall(NetworkTags.UPDATES_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (updateRepositoryManager == null)
            updateRepositoryManager = UpdateRepositoryManager.getInstance();

        updateRepositoryManager.getUpdatesList(catalogueIDS, date, endDate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void joinUpdate(String updateId, String choiceType) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            joinUpdate(updateId, choiceType);
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.SERVER_ERROR));
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                        }
                    });

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response != null ? response.errorBody() : null), 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                Logger.d("Error", e.getMessage());
                networkCall.postValue(new NetworkCall(NetworkTags.JOIN_LIVE_SESSION, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (updateRepositoryManager == null)
            updateRepositoryManager = UpdateRepositoryManager.getInstance();

        updateRepositoryManager.joinMeeting(updateId, choiceType)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }


}
