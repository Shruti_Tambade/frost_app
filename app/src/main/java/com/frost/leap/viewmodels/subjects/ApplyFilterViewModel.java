package com.frost.leap.viewmodels.subjects;

import android.os.AsyncTask;
import android.util.ArraySet;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 07-01-2020.
 * <p>
 * FROST
 */
public class ApplyFilterViewModel extends BaseViewModel {

    private List<String> subjects, units, topics;
    private Set<String> subjectIds, unitIds, topicIds;
    public List<String> sIds, uIds, tIds;

    private MutableLiveData<List<String>> subjectsLiveData;
    private MutableLiveData<List<String>> unitsLiveData;
    private MutableLiveData<List<String>> topicsLiveData;
    private MutableLiveData<Boolean> showLoader;
    private List<SubjectInfo> filterSubjectList;

    public ApplyFilterViewModel() {
        subjectsLiveData = new MutableLiveData<>();
        unitsLiveData = new MutableLiveData<>();
        topicsLiveData = new MutableLiveData<>();
        showLoader = new MutableLiveData<>();
    }


    public LiveData<Boolean> showLoader() {
        return showLoader;
    }

    public LiveData<List<String>> updateSubjects() {
        return subjectsLiveData;
    }

    public LiveData<List<String>> updateUnits() {
        return unitsLiveData;
    }

    public LiveData<List<String>> updateTopics() {
        return topicsLiveData;
    }

    public void getSubjects(List<Note> noteList, List<SubjectInfo> list) {
        new AsyncTask<Void, Void, List<String>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoader.postValue(true);
            }

            @Override
            protected List<String> doInBackground(Void... voids) {

                subjectIds = new ArraySet<>();
                for (int i = 0; i < noteList.size(); i++) {
                    if (!subjectIds.contains(noteList.get(i).getStudentSubjectId())) {
                        subjectIds.add(noteList.get(i).getStudentSubjectId());
                    }
                }
                subjects = new ArrayList<>();
                filterSubjectList = new ArrayList<>();
                sIds = new ArrayList<>();
                for (SubjectInfo subjectInfo : list) {
                    if (subjectIds.contains(subjectInfo.getStudentSubjectId())) {
                        filterSubjectList.add(subjectInfo);
                        subjects.add(subjectInfo.getSubjectName());
                        sIds.add(subjectInfo.getStudentSubjectId());
                    }
                }
                return subjects;
            }

            @Override
            protected void onPostExecute(List<String> result) {
                super.onPostExecute(result);
                showLoader.postValue(false);
                subjects = result;
                subjectsLiveData.postValue(result);
            }
        }.execute();
    }

    public void getUnits(List<Note> noteList, int position) {
        new AsyncTask<Void, Void, List<String>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoader.postValue(true);
            }

            @Override
            protected List<String> doInBackground(Void... voids) {

                units = new ArrayList<>();
                uIds = new ArrayList<>();
                unitIds = new ArraySet<>();
                for (int i = 0; i < noteList.size(); i++) {
                    if (sIds.get(position).equals(noteList.get(i).getStudentSubjectId())
                            &&
                            !unitIds.contains(noteList.get(i).getStudentUnitId())) {
                        unitIds.add(noteList.get(i).getStudentUnitId());
                    }
                }
                for (SubjectInfo.UnitInfo unitInfo : filterSubjectList.get(position).getUnitNames()) {
                    if (unitIds.contains(unitInfo.getStudentUnitId())) {
                        uIds.add(unitInfo.getStudentUnitId());
                        units.add(unitInfo.getUnitName());
                    }
                }
                return units;
            }

            @Override
            protected void onPostExecute(List<String> result) {
                super.onPostExecute(result);
                showLoader.postValue(false);
                units = result;
                unitsLiveData.postValue(result);
            }
        }.execute();


    }

    public void getTopics(List<Note> noteList, int sPosition, int uPosition) {
        new AsyncTask<Void, Void, List<String>>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                showLoader.postValue(true);
            }

            @Override
            protected List<String> doInBackground(Void... voids) {

                topics = new ArrayList<>();
                tIds = new ArrayList<>();
                topicIds = new ArraySet<>();

                for (int i = 0; i < noteList.size(); i++) {

                    if (sIds.get(sPosition).equals(noteList.get(i).getStudentSubjectId())
                            &&
                            uIds.get(uPosition).equals(noteList.get(i).getStudentUnitId())
                            &&
                            !topicIds.contains(noteList.get(i).getStudentTopicId())
                    ) {
                        topicIds.add(noteList.get(i).getStudentTopicId());
                        tIds.add(noteList.get(i).getStudentTopicId());
                        topics.add(noteList.get(i).getTopicName());
                    }
                }

                return topics;
            }

            @Override
            protected void onPostExecute(List<String> result) {
                super.onPostExecute(result);
                showLoader.postValue(false);
                topics = result;
                topicsLiveData.postValue(topics);
            }
        }.execute();
    }


}
