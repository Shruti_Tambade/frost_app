package com.frost.leap.viewmodels.topics;


import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.android.exoplayer2.drm.DeepLearnDrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.source.dash.DashUtil;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.upstream.ByteArrayDataSource;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import apprepos.topics.TopicRepositoryManager;
import apprepos.topics.model.TopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.user.UserRepositoryManager;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.model.video.ContentModel;
import apprepos.video.model.video.DashUrlsModel;
import apprepos.video.model.video.OfflineProgressModel;
import apprepos.video.model.video.PageContentDataModel;
import apprepos.video.model.video.VideoDataModel;
import apprepos.video.model.video.VideoDetailsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public class TopicsViewModel extends BaseViewModel {

    private MutableLiveData<TopicModel> topicLiveData;
    private MutableLiveData<String> tokenLiveData;
    private MutableLiveData<PageContentDataModel> pageContentDataMutableLiveData;
    private MutableLiveData<RatingModel> ratingModelMutableLiveData;
    private TopicRepositoryManager repositoryManager;
    private DashUrlDAO dashUrlDAO;


    public TopicsViewModel() {
        super();
        topicLiveData = new MutableLiveData<>();
        pageContentDataMutableLiveData = new MutableLiveData<>();
        ratingModelMutableLiveData = new MutableLiveData<>();
        tokenLiveData = new MutableLiveData<>();
        repositoryManager = TopicRepositoryManager.getInstance();
    }


    public static final class NetworkTags {

        public static final int TOPICS_LIST = 1;
        public static final int VIDEO_URL = 2;
        public static final int VIDEO_STREAM_UPDATE = 3;
        public static final int XML_TOKEN = 4;
        public static final int SUBTOPIC_RATING = 5;
    }

    public LiveData<TopicModel> updateTopics() {
        return topicLiveData;
    }

    public LiveData<String> updateToken() {
        return tokenLiveData;
    }

    public LiveData<RatingModel> updateSubTopicRating() {
        return ratingModelMutableLiveData;
    }

    public LiveData<PageContentDataModel> updateVideoUrl() {
        return pageContentDataMutableLiveData;
    }

    public void fetchTopics(String unitId) {

        if (!isNetworkAvailable()) {

            repositoryManager.getOfflineUnitTopic(unitId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<TopicModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(TopicModel topicModel) {
                            if (topicModel != null) {
                                topicLiveData.postValue(topicModel);
                                networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.SUCCESS));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
                            networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.NO_INTERNET));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.IN_PROCESS));
        Observer<Response<TopicModel>> observer = new Observer<Response<TopicModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<TopicModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        repositoryManager.updateUnitTopic(unitId, response.body());
                        topicLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchTopics(unitId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.TOPICS_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.getTopics(unitId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void updateVideoStream(String studentSubjectId, String unitId,
                                  String topicId, int videoCompletion, String subTopicId) {

        if (!isNetworkAvailable()) {
            //message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
//                        message.postValue(new Message(response.message(), 2));
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));
//                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateVideoStream(studentSubjectId, unitId,
                                    topicId, videoCompletion, subTopicId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
//                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_STREAM_UPDATE, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.updateVideoStream(studentSubjectId, unitId, topicId, videoCompletion, subTopicId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);


    }

    public void fetchVideoUrl(String pageContentId) {
        if (dashUrlDAO == null)
            dashUrlDAO = repositoryManager.getDashUrlDAO();

        Observer<DashUrlsModel> observer = new Observer<DashUrlsModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(DashUrlsModel dashUrlsModel) {
                verifyAndFetchVideoUrl(dashUrlsModel, pageContentId);
            }

            @Override
            public void onError(Throwable e) {
                Logger.d("VIDEO URL", "NOT FOUND IN LOCAL DB");
                verifyAndFetchVideoUrl(null, pageContentId);
            }

            @Override
            public void onComplete() {

            }
        };

        Observable<DashUrlsModel> observable = Observable.create(emitter -> {
            emitter.onNext(dashUrlDAO.findPageContentUrl(pageContentId));
            emitter.onComplete();
        });
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }


    public void verifyAndFetchVideoUrl(DashUrlsModel dashUrlsModel, String pageContentId) {
        if (dashUrlsModel != null) {
            ShareDataManager.getInstance().setDashUrlsModel(dashUrlsModel);
            if (dashUrlsModel.getDashUrl() != null) {
                PageContentDataModel pageContentDataModel = new PageContentDataModel();
                ContentModel contentModel = new ContentModel();

                List<VideoDataModel> videoDataModelArrayList = new ArrayList<>();
                VideoDataModel videoDataModel = new VideoDataModel();

                VideoDetailsModel videoDataModel1 = new VideoDetailsModel();
                videoDataModel1.setVideoUrl(dashUrlsModel.getDashUrl());

                videoDataModel.setVideoDetails(videoDataModel1);
                videoDataModelArrayList.add(videoDataModel);
                contentModel.setVideoData(videoDataModelArrayList);
                pageContentDataModel.setContent(contentModel);

                pageContentDataMutableLiveData.postValue(pageContentDataModel);
                Logger.d("VIDEO URL", "FROM LOCAL DB");
                return;
            }
        }

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.IN_PROCESS));
        Observer<Response<PageContentDataModel>> observer = new Observer<Response<PageContentDataModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<PageContentDataModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        pageContentDataMutableLiveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            verifyAndFetchVideoUrl(dashUrlsModel, pageContentId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.VIDEO_URL, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.getVideoUrl(pageContentId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void fetchXmlToken() {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        tokenLiveData.postValue(response.body().get("authToken").getAsString());
                        Bundle bundle = new Bundle();
                        bundle.putString("CDATA", response.body().get("authToken").getAsString());
                        networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, bundle, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchXmlToken();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.XML_TOKEN, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.getXmlToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void fetchSubtopicsRating(String subTopicId) {

        if (!isNetworkAvailable()) {
            //message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
            networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.IN_PROCESS));
        Observer<Response<ResponseBody>> observer = new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<ResponseBody> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {

                    if (response.body() != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<RatingModel>() {
                        }.getType();
                        try {
                            RatingModel ratingModel = gson.fromJson(response.body().string(), type);
                            ratingModelMutableLiveData.postValue(ratingModel);
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.SUCCESS));

                        } catch (Exception e) {
                            e.printStackTrace();
//                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.ERROR));
                        }
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.NO_DATA));
//                        message.postValue(new Message(Constants.NO_DATA_AVAILABLE, 2));
                    }

                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.FAIL));
//                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchSubtopicsRating(subTopicId);
                        } else {
//                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.ERROR));
                        }
                    });
                } else {
//                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
//                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.SUBTOPIC_RATING, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.getSubTopicRating(subTopicId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);

    }


    public DeepLearnDrmSessionManager<ExoMediaCrypto> generateDRMManager(String agent, Uri uri, String securityLevel) {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        return repositoryManager.generateDRMManager(agent, uri, securityLevel);
    }


    public void deleteOfflineLicense(String url) {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.deleteOfflineLicense(url);
    }

    public void hasOfflineData(String unitId) {
        repositoryManager.hasOfflineDataOfTopic(unitId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(exist -> {
                    if (!exist) {
//                        message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 3));
                    }
                });
    }


    public String getAuthToken() {
        return repositoryManager.getAuthToken();
    }


    public int getAutoResolution() {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();
        return repositoryManager.getAutoResolutions();
    }


    public DashManifest getDashManifest(Uri uri) {
        DashUrlsModel dashUrlsModel = ShareDataManager.getInstance().getDashUrlsModel();
        if (dashUrlsModel == null)
            return null;
        if (TextUtils.isEmpty(dashUrlsModel.getManifestData())) {
            return null;
        }

        if (!dashUrlsModel.getDashUrl().equalsIgnoreCase(uri.toString()))
            return null;

        try {
            return DashUtil.loadManifest(new ByteArrayDataSource(dashUrlsModel.getManifestData().getBytes()), uri);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void setEmptySecurityLevel() {
        UserRepositoryManager.getInstance().setSecurityLevel("");
    }


    public void pushToOfflineProgress(String subTopicId, String topicId, String unitId, String subjectId, long videoCompletion) {
        OfflineProgressModel offlineProgressModel = new OfflineProgressModel();
        offlineProgressModel.setStudentSubTopicId(subTopicId);
        offlineProgressModel.setStudentTopicId(topicId);
        offlineProgressModel.setStudentUnitId(unitId);
        offlineProgressModel.setStudentSubjectId(subjectId);
        offlineProgressModel.setVideoCompletion((double) videoCompletion / 1000);

        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();


        repositoryManager.pushToOfflineProgress(offlineProgressModel);
    }

}
