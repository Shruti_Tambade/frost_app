package com.frost.leap.viewmodels.topics;

import android.net.Uri;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.applications.HelperApplication;
import com.frost.leap.services.video.DownloadTracker;
import com.frost.leap.viewmodels.BaseViewModel;
import com.google.android.exoplayer2.offline.Download;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import apprepos.topics.TopicRepositoryManager;
import apprepos.topics.dao.IDownloadSubTopicDAO;
import apprepos.topics.model.DownloadSubTopicModel;
import apprepos.topics.model.StudentContentModel;
import apprepos.topics.model.StudentTopicsModel;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.utils.Logger;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 29-01-2020.
 * <p>S
 * FROST
 */
public class DownloadViewModel extends BaseViewModel {

    private MutableLiveData<DashUrlsModel> liveData;
    private MutableLiveData<List<StudentTopicsModel>> liveDataTopics;
    private MutableLiveData<List<StudentTopicsModel>> liveOfflineTopics;
    private TopicRepositoryManager repositoryManager;
    private DashUrlDAO dashUrlDAO;
    private IDownloadSubTopicDAO downloadSubTopicDAO;
    private boolean hasChange = false;
    private AsyncTask asyncTask;
    private List<StudentTopicsModel> topicsModelList;
    private Disposable disposable;
    private AsyncTask offlineAsyncTask;


    public DownloadViewModel() {
        super();
        repositoryManager = TopicRepositoryManager.getInstance();
        dashUrlDAO = repositoryManager.getDashUrlDAO();
        downloadSubTopicDAO = repositoryManager.getDownloadSubTopicDAO();
        liveData = new MutableLiveData<>();
        liveDataTopics = new MutableLiveData<>();
        liveOfflineTopics = new MutableLiveData<>();
    }


    public LiveData<DashUrlsModel> getVideoUrl() {
        return liveData;
    }

    public LiveData<List<StudentTopicsModel>> updateTopics() {
        return liveDataTopics;
    }

    public LiveData<List<StudentTopicsModel>> updateOfflineTopics() {
        return liveOfflineTopics;
    }


    public void fetchVideoUrl(String pageContentId) {

        Observer<DashUrlsModel> observer = new Observer<DashUrlsModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(DashUrlsModel dashUrlsModel) {
                Logger.d("VIDEO URL", "FOUND IN LOCAL DB");
                liveData.setValue(dashUrlsModel);
            }

            @Override
            public void onError(Throwable e) {
                Logger.d("VIDEO URL", "NOT FOUND IN LOCAL DB");
            }

            @Override
            public void onComplete() {

            }
        };
        Observable<DashUrlsModel> observable = Observable.create(emitter -> {
            emitter.onNext(dashUrlDAO.findPageContentUrl(pageContentId));
            emitter.onComplete();
        });
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }

    public void updateOfflineVideosInfo(List<StudentTopicsModel> studentTopicsModelList, boolean showOnlyOffline) {

        if (asyncTask != null && !asyncTask.isCancelled()) {
            asyncTask.cancel(true);
        }
        asyncTask = new AsyncTask<Void, Void, List<StudentTopicsModel>>() {

            @Override
            protected List<StudentTopicsModel> doInBackground(Void... voids) {
                if (studentTopicsModelList == null)
                    return null;
                DownloadTracker downloadTracker = HelperApplication.getInstance().getDownloadTracker();
                hasChange = false;
                try {

                    for (int i = 0; i < studentTopicsModelList.size(); i++) {
                        if (studentTopicsModelList.get(i).getStudentContents() != null) {

                            for (int j = 0; j < studentTopicsModelList.get(i).getStudentContents().size(); j++) {
                                if (studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic() != null) {
                                    try {
                                        DashUrlsModel dashUrlsModel = dashUrlDAO.findPageContentUrl(studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic().getPageContentId());
                                        if (dashUrlsModel != null) {
                                            if (downloadTracker.getDownload(Uri.parse(dashUrlsModel.getDashUrl())) != null) {
                                                hasChange = true;
                                                Download download = downloadTracker.getDownload(Uri.parse(dashUrlsModel.getDashUrl()));
                                                if (studentTopicsModelList.get(i).getStudentContents().get(j).getPercentage() == download.getPercentDownloaded()) {
                                                    hasChange = false;
                                                }
                                                studentTopicsModelList.get(i).getStudentContents().get(j).setInProcess(download.state == Download.STATE_COMPLETED ? false : true);
                                                studentTopicsModelList.get(i).getStudentContents().get(j).setDownloaded(download.state == Download.STATE_COMPLETED ? true : false);
                                                studentTopicsModelList.get(i).getStudentContents().get(j).setFailed(download.state == Download.STATE_FAILED ? true : false);
                                                studentTopicsModelList.get(i).getStudentContents().get(j).setPercentage(download.getPercentDownloaded() == 0 ? 0.5f : (download.state == Download.STATE_COMPLETED ? 100 : download.getPercentDownloaded()));
                                            } else {
                                                if (studentTopicsModelList.get(i).getStudentContents().get(j).getPercentage() > 0) {
                                                    if (studentTopicsModelList.get(i).getStudentContents().get(j).getPercentage() == 0.5f) {
                                                        if (studentTopicsModelList.get(i).getStudentContents().get(j).isInProcess()
                                                                && !studentTopicsModelList.get(i).getStudentContents().get(j).isDownloaded()) {

                                                        }
                                                    } else {
                                                        hasChange = true;
                                                        studentTopicsModelList.get(i).getStudentContents().get(j).setInProcess(false);
                                                        studentTopicsModelList.get(i).getStudentContents().get(j).setDownloaded(false);
                                                        studentTopicsModelList.get(i).getStudentContents().get(j).setFailed(false);
                                                        studentTopicsModelList.get(i).getStudentContents().get(j).setPercentage(0);
                                                    }
                                                }
                                            }
                                        } else {

                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                    if (!showOnlyOffline) {
                        topicsModelList = new ArrayList<>();
                        topicsModelList.addAll(studentTopicsModelList);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return studentTopicsModelList;
            }


            @Override
            protected void onPostExecute(List<StudentTopicsModel> studentTopicsModels) {
                super.onPostExecute(studentTopicsModels);
                if (studentTopicsModels == null)
                    return;

                if (hasChange) {
                    liveDataTopics.postValue(studentTopicsModels);
                }


            }
        }.execute();
    }

    public void showOnlyDownloadedVideosInfo(String unitId) {

        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        Observer<List<DownloadSubTopicModel>> observer = new Observer<List<DownloadSubTopicModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(List<DownloadSubTopicModel> result) {
                Logger.d("OFFLINE", "AVAILABLE");
                filterDownloadVideos(result);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                Logger.d("OFFLINE", "NOT AVAILABLE");
                liveOfflineTopics.postValue(new ArrayList<>());
            }

            @Override
            public void onComplete() {

            }
        };
        Observable<List<DownloadSubTopicModel>> observable = Observable.create(emitter ->
        {
            emitter.onNext(downloadSubTopicDAO.getDownloadSubTopicByUnitId(unitId));
            emitter.onComplete();
        });
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }


    public void filterDownloadVideos(List<DownloadSubTopicModel> downloadSubTopicModels) {

        if (downloadSubTopicModels != null && downloadSubTopicModels.size() > 0) {
            if (offlineAsyncTask != null && !offlineAsyncTask.isCancelled()) {
                offlineAsyncTask.cancel(true);
            }
            if (asyncTask != null && !asyncTask.isCancelled()) {
                asyncTask.cancel(true);
            }
            offlineAsyncTask = new AsyncTask<Void, Void, List<StudentTopicsModel>>() {

                @Override
                protected List<StudentTopicsModel> doInBackground(Void... voids) {
                    if (topicsModelList == null)
                        return new ArrayList<>();

                    List<StudentTopicsModel> offlineList = new ArrayList<>();

                    try {

                        for (StudentTopicsModel studentTopicsModel : topicsModelList) {
                            for (DownloadSubTopicModel downloadSubTopicModel : downloadSubTopicModels) {
                                List<StudentContentModel> studentContentModels = new ArrayList<>();
                                if (studentTopicsModel.getStudentTopicId().equals(downloadSubTopicModel.getTopicId())) {
                                    for (StudentContentModel contentModel : studentTopicsModel.getStudentContents()) {
                                        if (downloadSubTopicModel.getSubTopicId().equals(contentModel.getSubTopic().getStudentSubTopicId())) {
                                            studentContentModels.add(contentModel);
                                            break;
                                        }
                                    }
                                    boolean isExist = false;
                                    if (offlineList.size() > 0)
                                        for (int c = 0; c < offlineList.size(); c++) {
                                            if (offlineList.get(c).getTopicId().equals(studentTopicsModel.getTopicId())) {
                                                offlineList.get(c).getStudentContents().add(studentContentModels.get(0));
                                                isExist = true;
                                                break;
                                            }
                                        }
                                    if (!isExist) {
                                        StudentTopicsModel topicsModel = new StudentTopicsModel();
                                        topicsModel.setStudentTopicId(studentTopicsModel.getStudentTopicId());
                                        topicsModel.setTopicId(studentTopicsModel.getTopicId());
                                        topicsModel.setTopicName(studentTopicsModel.getTopicName());
                                        topicsModel.setTotalDurationInSce(studentTopicsModel.getTotalDurationInSce());
                                        topicsModel.setCompletionPercentage(studentTopicsModel.getCompletionPercentage());
                                        topicsModel.setLastWatched(studentTopicsModel.getLastWatched());
                                        topicsModel.setOffline(studentTopicsModel.isOffline());
                                        topicsModel.setStudentContents(studentContentModels);
                                        offlineList.add(topicsModel);
                                    }

                                }

                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return offlineList;
                }

                @Override
                protected void onPostExecute(List<StudentTopicsModel> list) {
                    super.onPostExecute(list);
                    liveOfflineTopics.postValue(list);
                }
            }.execute();
        } else {
            liveOfflineTopics.postValue(new ArrayList<>());
        }

    }

    public void cancelUpdate() {
        if (asyncTask != null && !asyncTask.isCancelled()) {
            asyncTask.cancel(true);
        }
    }

    public String getUserId() {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();
        return repositoryManager.getUserId();
    }


    public void deleteLicenseUrl(String url) {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.deleteOfflineLicense(url);
    }


    public void addDownloadSubTopic(DownloadSubTopicModel downloadSubTopicModel) {

        if (downloadSubTopicModel == null)
            return;

        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();

        repositoryManager.insertOrUpdateDownloadSubTopic(downloadSubTopicModel);
    }


    public List<StudentTopicsModel> getTopicsModelList() {
        return topicsModelList;
    }

    public void storeTopicModelList(List<StudentTopicsModel> studentTopicsModelList) {
        topicsModelList = new ArrayList<>();
        topicsModelList.addAll(studentTopicsModelList);
    }

    public void stopOfflineData() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        if (offlineAsyncTask != null && !offlineAsyncTask.isCancelled()) {
            offlineAsyncTask.cancel(true);
        }
    }

    public void pushToNotify(DashUrlsModel dashUrlsModel, String catalogueName, String subjectName, String unitName, String topicName, String subTopicName) {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("courseName", catalogueName);
        jsonObject.addProperty("subjectName", subjectName);
        jsonObject.addProperty("unitName", unitName);
        jsonObject.addProperty("topicName", topicName);
        jsonObject.addProperty("subTopicName", subTopicName);
        jsonObject.addProperty("pageContentId", dashUrlsModel.getPageContentId());
        repositoryManager.pushToNotifyDownloadedSuccessToMixPanel(dashUrlsModel.getDashUrl(), jsonObject);
    }


    public void popNotify(String url) {
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();


        repositoryManager.deleteNotifyDownload(url);
    }

    public boolean getAskMeEveryTime() {
        return repositoryManager.getAskMeEveryTime();
    }

    public int getDownloadResolution() {
        return repositoryManager.getDownloadResolution();
    }
}
