package com.frost.leap.viewmodels.profile;

import android.os.Bundle;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.AdditionalDetails;
import apprepos.user.model.Address;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 30-12-2019.
 * <p>
 * FROST
 */
public class AdditionalViewModel extends BaseViewModel {
    private UserRepositoryManager userRepositoryManager;
    private MutableLiveData<AdditionalDetails> mutableLiveData;
    private Disposable uploadDisposable = null;

    public AdditionalViewModel() {
        userRepositoryManager = UserRepositoryManager.getInstance();
        mutableLiveData = new MutableLiveData<>();
    }


    public class NetworkTags {
        public static final int GET_ADDITIONAL_DETAILS = 1;
        public static final int PUSH_PROFILE_PIC_S3 = 2;

    }


    public LiveData<AdditionalDetails> updateAdditionalDetails() {
        return mutableLiveData;
    }


    public void fetchAdditionalDetails() {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.IN_PROCESS));

        Observer<Response<AdditionalDetails>> observer = new Observer<Response<AdditionalDetails>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<AdditionalDetails> response) {
                if (response != null && response.code() == 200) {
                    if (response.body() == null)
                        mutableLiveData.postValue(new AdditionalDetails());
                    else {
                        mutableLiveData.postValue(response.body());
                    }
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchAdditionalDetails();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();


        userRepositoryManager.getAdditionalDetails()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }


    // REFRESH TOKEN ADDED
    public void uploadProofPic(GalleryFile galleryFile) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.NO_INTERNET));
            return;
        }
        if (uploadDisposable != null && !uploadDisposable.isDisposed())
            uploadDisposable.dispose();

        networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                uploadDisposable = d;
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    Bundle bundle = new Bundle();
                    bundle.putString("ImageUrl", response.body().get("message").getAsString());
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, bundle, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            uploadProofPic(galleryFile);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        userRepositoryManager.pushProfilePicToS3(galleryFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public List<MenuItem> generateAdditionalSettingsMenu(AdditionalDetails additionalDetails) {
        List<MenuItem> list = new ArrayList<>();


        if (additionalDetails == null) {
            additionalDetails = new AdditionalDetails();
        }

        list.add(new MenuItem(1, "Father Name", additionalDetails.getFatherName() == null ? "Update the name of your father" : additionalDetails.getFatherName(), null, true));
        list.add(new MenuItem(2, "Highest Qualification", additionalDetails.getHighestQualification() == null ? "Add your highest academic qualification" : additionalDetails.getHighestQualification(), null, true));
        list.add(new MenuItem(3, "College/University Name", additionalDetails.getCollegeName() == null ? "Update your college/university name" : additionalDetails.getCollegeName(), null, true));
        list.add(new MenuItem(4, "Current Employment Status", additionalDetails.getEmploymentStatus() == null ? "Update your employment status" : Utility.getCamelCase(additionalDetails.getEmploymentStatus()), null, true));
        list.add(new MenuItem(5, "Current Goal", additionalDetails.getCurrentGoal() == null ? "Add your current goal" : additionalDetails.getCurrentGoal(), null, true));
        list.add(new MenuItem(6, "Delivery Address", additionalDetails.getAddress() == null ? "Add your delivery address" : getAddressDetails(additionalDetails.getAddress()), null, true));
        list.add(new MenuItem(7, "ID Proof", additionalDetails.getProofFront() == null ? "Upload your ID proof" : "Aadhar Card", null, true));

        return list;
    }



    public String getAddressDetails(Address address){
        if(address==null){
            return "";
        }


        return address.getAddress()+ " " + address.getCity() + " " + address.getState() + " " + address.getPin();
    }


}
