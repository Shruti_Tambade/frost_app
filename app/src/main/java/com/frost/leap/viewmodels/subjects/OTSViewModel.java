package com.frost.leap.viewmodels.subjects;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.R;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Error;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.payment.PaymentViewModel;
import com.google.gson.JsonObject;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.AdditionalDetails;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 29-10-2019.
 * <p>
 * FROST
 */
public class OTSViewModel extends BaseViewModel {

    private UserRepositoryManager repositoryManager;
    private Disposable uploadDisposable;
    public String userPicUrl = null;
    public String proofFrontSideUrl = null;
    public String proofBackSideUrl = null;
    private String fatherName = null;
    private String qualification = null;
    private String university = null;
    private String goal = null;
    private String companyName = null;
    private String password = null;
    private String address = null;
    private String city = null;
    private String state = null;
    private String pincode = null;
    private boolean isDelivered;
    private int value = 0;
    private MutableLiveData<AdditionalDetails> mutableLiveData;


    public OTSViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
        mutableLiveData = new MutableLiveData<>();

    }

    public boolean validationFirst() {
        if (userPicUrl == null) {
            message.postValue(new Message("Please upload the profile pic", 2));
            return false;
        }
        return true;
    }


    public boolean validationSecond(String fatherName, String qualification, int value, String companyName, String goal, String university, String password) {
        if (TextUtils.isEmpty(fatherName)) {
            error.postValue(new Error(R.id.tlFatherName, "Father name should not be empty"));
            return false;
        }
        if (fatherName.trim().length() < 3) {
            error.postValue(new Error(R.id.tlFatherName, "Please enter valid father name"));
            return false;
        }
        if (TextUtils.isEmpty(qualification)) {
//            message.postValue(new Message("Please select the high qualification", 2));
            error.postValue(new Error(R.id.qualificationTextInputLayout, "Please select the high qualification"));
            return false;
        }
        if (TextUtils.isEmpty(university)) {
            error.postValue(new Error(R.id.tlUniversityName, "University should not be empty"));
            return false;
        }
        if (value == 2) {
            if (TextUtils.isEmpty(companyName)) {
                error.postValue(new Error(R.id.tlCompanyName, "Company name should not be empty"));
                return false;
            }
        }
        if (TextUtils.isEmpty(goal)) {
//            message.postValue(new Message("Please select the current goal", 2));
            error.postValue(new Error(R.id.goalTextInputLayout, "Please select the current goal"));
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            error.postValue(new Error(R.id.tlPassword, "Password should not be empty"));
            return false;
        }
        if (password.length() < 6) {
            error.postValue(new Error(R.id.tlPassword, "Password should be minimum 6 characters"));
            return false;
        }
        this.fatherName = fatherName.trim();
        this.qualification = qualification;
        this.value = value;
        this.goal = goal;
        this.companyName = companyName;
        this.university = university;
        this.password = password;

        return true;
    }


    public boolean validationThree(boolean isDelivered, String address, String city, String state, String pincode) {

        if (TextUtils.isEmpty(address)) {
            error.postValue(new Error(R.id.tlAddress, "Address should not be empty"));
            return false;
        }

        if (TextUtils.isEmpty(city)) {
            error.postValue(new Error(R.id.tlCity, "City should not be empty"));
            return false;
        }

        if (TextUtils.isEmpty(state)) {
            error.postValue(new Error(R.id.tlState, "State should not be empty"));
            return false;
        }

        if (TextUtils.isEmpty(pincode)) {
            error.postValue(new Error(R.id.tlPinCode, "Pincode should not be empty"));
            return false;
        }

        if (pincode.length() < 6) {
            error.postValue(new Error(R.id.tlPinCode, "Pincode should be minimum 6 digits"));
            return false;
        }

        if (proofFrontSideUrl == null) {
            message.postValue(new Message("Please upload the front side of ID Proof", 2));
            return false;
        }

        if (proofBackSideUrl == null) {
            message.postValue(new Message("Please upload the back side of ID Proof", 2));
            return false;
        }


        this.address = address;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.isDelivered = isDelivered;

        return true;

    }


    public static final class NetworkTags {
        public static final int PUSH_USER_PROFILE_PIC = 1;
        public static final int PUSH_PROOF_FRONT_SIDE = 2;
        public static final int PUSH_PROOF_BACK_SIDE = 3;
        public static final int UPDATE_OTS_DETAILS = 4;
        public static final int GET_ADDITIONAL_DETAILS = 5;
    }

    public String getUserEmail() {
        return repositoryManager.getUser().getCredentials().getEmail();
    }

    public void uploadProfilePic(GalleryFile galleryFile) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, NetworkStatus.NO_INTERNET));
            return;
        }
        if (uploadDisposable != null && !uploadDisposable.isDisposed())
            uploadDisposable.dispose();

        networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                uploadDisposable = d;
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    Bundle bundle = new Bundle();
                    bundle.putString("ImageUrl", userPicUrl = response.body().get("message").getAsString());
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, bundle, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            uploadProfilePic(galleryFile);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.PUSH_USER_PROFILE_PIC, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.pushImageToS3(galleryFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


    public void uploadProofFrontSidePic(GalleryFile galleryFile) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                uploadDisposable = d;
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    Bundle bundle = new Bundle();
                    bundle.putString("ImageUrl", proofFrontSideUrl = response.body().get("message").getAsString());
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, bundle, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            uploadProofFrontSidePic(galleryFile);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_FRONT_SIDE, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.pushImageToS3(galleryFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


    public void uploadProofBackSidePic(GalleryFile galleryFile) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                uploadDisposable = d;
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    Bundle bundle = new Bundle();
                    bundle.putString("ImageUrl", proofBackSideUrl = response.body().get("message").getAsString());
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, bundle, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            uploadProofBackSidePic(galleryFile);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROOF_BACK_SIDE, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.pushImageToS3(galleryFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


    public void updateOTSDetails(String studentCatalogueId) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    Utility.showToast(Utility.getContext(), Utility.getMessage(response.body()), false);
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateOTSDetails(studentCatalogueId);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_OTS_DETAILS, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("fatherName", fatherName);
        jsonObject.addProperty("studentPhoto", userPicUrl);
        jsonObject.addProperty("highestQualification", qualification);
        jsonObject.addProperty("collegeName", university);
        jsonObject.addProperty("universityName", university);
        jsonObject.addProperty("deliveryInCollege", isDelivered);


        jsonObject.addProperty("employmentStatus", value == 1 ? "STUDENT" : (value == 2 ? "EMPLOYED" : "UNEMPLOYED"));
        jsonObject.addProperty("companyName", companyName);
        jsonObject.addProperty("currentGoal", goal);

        jsonObject.addProperty("proofFront", proofFrontSideUrl);
        jsonObject.addProperty("proofBack", proofBackSideUrl);
        jsonObject.addProperty("studentId", repositoryManager.getUser().getStudentId());
        jsonObject.addProperty("studentCatalogueId", studentCatalogueId);


        JsonObject addressJsonObject = new JsonObject();
        addressJsonObject.addProperty("address", address);
        addressJsonObject.addProperty("city", city);
        addressJsonObject.addProperty("state", state);
        addressJsonObject.addProperty("pin", pincode);

        JsonObject otsJsonObject = new JsonObject();
        otsJsonObject.addProperty("orgCode", Constants.ORG_CODE);
        otsJsonObject.addProperty("packageGroupCode", Constants.PACKAGE_GROUP_CODE);
        otsJsonObject.addProperty("packageCode", Constants.PACKAGE_CODE);
        otsJsonObject.addProperty("studentTypeCode", Constants.STUDENT_TYPE_CODE);
        otsJsonObject.addProperty("email", getUserEmail());
        otsJsonObject.addProperty("password", password);


        jsonObject.add("address", addressJsonObject);
        jsonObject.add("otsRequest", otsJsonObject);


        repositoryManager.updateOTSDetails(jsonObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }


    public void fetchAdditionalDetails() {

        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.IN_PROCESS));

        Observer<Response<AdditionalDetails>> observer = new Observer<Response<AdditionalDetails>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<AdditionalDetails> response) {
                if (response != null && response.code() == 200) {
                    if (response.body() == null)
                        mutableLiveData.postValue(null);
                    else {
                        mutableLiveData.postValue(response.body());
                    }
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchAdditionalDetails();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.GET_ADDITIONAL_DETAILS, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.getAdditionalDetails()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }

    public LiveData<AdditionalDetails> updateAdditionalDetails() {
        return mutableLiveData;
    }


    public String getProfilePic() {
        return repositoryManager.getUser().getProfileInfo().getImage();
    }


}
