package com.frost.leap.viewmodels.quiz;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import apprepos.quiz.QuizRepositoryManager;
import apprepos.quiz.model.QuizQuestionsModel;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public class QuizReviewViewModel extends BaseViewModel {

    private QuizRepositoryManager repositoryManager;
    private MutableLiveData<QuizQuestionsModel> liveData;

    public QuizReviewViewModel() {
        super();
        repositoryManager = QuizRepositoryManager.getInstance();
        liveData = new MutableLiveData<>();
    }


    public class NetworkTags {
        public static final int PREVIEW_QUIZ = 1;
    }

    public LiveData<QuizQuestionsModel> updateQuiz() {
        return liveData;
    }

    public void getQuizDetails(String quizId) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.NO_INTERNET));
            return;
        }

        networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.IN_PROCESS));
        Observer<Response<QuizQuestionsModel>> observer = new Observer<Response<QuizQuestionsModel>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<QuizQuestionsModel> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        liveData.postValue(response.body());
                        networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.SUCCESS));
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.NO_DATA));
                        message.postValue(new Message(Constants.NO_DATA_AVAILABLE, 2));
                    }

                } else if (response != null && response.code() == 401) {
                    message.postValue(new Message("Token Expired", 0));

                } else {
                    networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.ERROR));
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.PREVIEW_QUIZ, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = QuizRepositoryManager.getInstance();

        repositoryManager.getPreviewQuiz(quizId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


}
