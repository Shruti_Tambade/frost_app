package com.frost.leap.viewmodels.profile;


import android.os.Bundle;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class ProfileSettingsViewModel extends BaseViewModel {
    private UserRepositoryManager repositoryManager;
    private MutableLiveData<User> liveData;
    private Disposable uploadDisposable = null;

    public ProfileSettingsViewModel() {
        super();
        repositoryManager = UserRepositoryManager.getInstance();
        liveData = new MutableLiveData<>();
    }

    public class NetworkTags {
        public static final int GET_USER_DETAILS = 1;
        public static final int PUSH_PROFILE_PIC_S3 = 2;
        public static final int UPDATE_PROFILE = 3;
        public static final int UPDATE_PASSWORD = 4;
        public static final int UPDATE_MOBILE = 5;
        public static final int VERIFY_OTP = 6;
    }

    public LiveData<User> updateUser() {
        return liveData;
    }


    public boolean doValidation(String oldPassword, String password, String confirmPassword) {
        if (TextUtils.isEmpty(oldPassword)) {
            message.postValue(new Message("Old password cannot be empty", 3));
            return false;
        } else if (TextUtils.isEmpty(password)) {
            message.postValue(new Message("New password cannot be empty", 3));
            return false;
        } else if (TextUtils.isEmpty(confirmPassword)) {
            message.postValue(new Message("Confirm password cannot be empty", 3));
            return false;
        } else if (password.length() < 6) {
            message.postValue(new Message("Password should have atleast 6 characters", 3));
            return false;
        }
//        else if (!Utility.isValidPassword(password)) {
//            message.postValue(new Message("It expects at least 1 small-case letter, 1 Capital letter, 1 digit, 1 special character and the length should be between 8-15 characters. ", 2));
//            return false;
//        }
        else if (!password.equals(confirmPassword)) {
            message.postValue(new Message("Password and confirm password should be same", 3));
            return false;
        } else if (oldPassword.equals(confirmPassword)) {
            message.postValue(new Message("Old Password and new password should not be same", 3));
            return false;
        }
        return true;
    }

    public boolean doValidationMobileUpdate(String mobileNumber, String password) {
        if (TextUtils.isEmpty(mobileNumber)) {
            message.postValue(new Message("Phone number cannot be empty", 3));
            return false;
        } else if (TextUtils.isEmpty(password)) {
            message.postValue(new Message("Password cannot be empty", 3));
            return false;
        } else if (!Utility.isValidMobile(mobileNumber)) {
            message.postValue(new Message("The phone number you have entered is invalid", 3));
            return false;
        }
        return true;
    }

    public boolean doValidationOTP(String otp) {
        if (TextUtils.isEmpty(otp)) {
            message.postValue(new Message("Verification code cannot be empty", 3));
            return false;
        } else if (otp.length() != 6) {
            message.postValue(new Message("Please enter a valid 6 digit verification code", 3));
            return false;
        }
        return true;
    }

    public List<MenuItem> generateProfileSettingsMenu(User user) {
        List<MenuItem> list = new ArrayList<>();
        list.add(null);
        if (user.getProfileInfo() == null || user.getCredentials() == null) {
            return list;
        }
        list.add(new MenuItem(1, "Name", user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName(), null, true));
        list.add(new MenuItem(2, "DOB", Utility.getUTCDateToNormal(user.getProfileInfo().getDateOfBirth()), null, false));
        list.add(new MenuItem(3, "Gender", user.getProfileInfo().getGender(), null, true));
        list.add(new MenuItem(4, "Phone", "+" + user.getCredentials().getCountryCode() + " " + user.getCredentials().getMobile(), null, true));
        list.add(new MenuItem(5, "Email", user.getCredentials().getEmail(), null, false));
        list.add(new MenuItem(6, "Password", "Change your password", null, true));
//        list.add(new MenuItem(7, "Addition Details", "Change your ots-details", null, true));
//        list.add(new MenuItem(7, "Logout", "Logout account", null, false));
        return list;
    }

    // REFRESH TOKEN ADDED
    public void fetchUserDetails() {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.GET_USER_DETAILS, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.GET_USER_DETAILS, NetworkStatus.IN_PROCESS));

        Observer<Response<User>> observer = new Observer<Response<User>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<User> response) {
                if (response != null && response.code() == 200) {
                    repositoryManager.saveUserDetails(response.body());
                    liveData.postValue(response.body());
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_USER_DETAILS, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchUserDetails();
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.GET_USER_DETAILS, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.GET_USER_DETAILS, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.GET_USER_DETAILS, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();


        repositoryManager.getUserDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    // REFRESH TOKEN ADDED
    public void uploadProfilePic(GalleryFile galleryFile) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.NO_INTERNET));
            return;
        }
        if (uploadDisposable != null && !uploadDisposable.isDisposed())
            uploadDisposable.dispose();

        networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.IN_PROCESS));

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                uploadDisposable = d;
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    message.postValue(new Message("Profile pic updated successfully", 1));
                    Bundle bundle = new Bundle();
                    bundle.putString("ImageUrl", response.body().get("message").getAsString());
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, bundle, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));

                    Mixpanel.updateProfilePic(Utility.showErrorMessage(response.errorBody()));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            uploadProfilePic(galleryFile);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.ERROR));
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.PUSH_PROFILE_PIC_S3, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };


        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.pushProfilePicToS3(galleryFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    // REFRESH TOKEN ADDED
    public void updatePassword(User user, String oldPassword, String password) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    message.postValue(new Message(Utility.getMessage(response.body()), 1));
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updatePassword(user, oldPassword, password);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.updateUserPassword(user, oldPassword, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    // REFRESH TOKEN ADDED
    public void updateMobileNumber(String mobileNumber, String password) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_PASSWORD, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_MOBILE, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    message.postValue(new Message(Utility.getMessage(response.body()), 1));
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_MOBILE, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_MOBILE, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateMobileNumber(mobileNumber, password);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_MOBILE, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_MOBILE, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.UPDATE_MOBILE, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.updateUserMobile(mobileNumber, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    // REFRESH TOKEN ADDED
    public void verifyOTP(String mobileNumber, String otpCode) {
        if (!isNetworkAvailable()) {
            message.postValue(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
            networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, NetworkStatus.NO_INTERNET));
            return;
        }
        networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, NetworkStatus.IN_PROCESS));
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    message.postValue(new Message(Utility.getMessage(response.body()), 1));
                    Bundle bundle = new Bundle();
                    repositoryManager.updateUserNumber(mobileNumber);
                    bundle.putString("Mobile", mobileNumber);
                    networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, bundle, NetworkStatus.SUCCESS));
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, NetworkStatus.FAIL));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            verifyOTP(mobileNumber, otpCode);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.VERIFY_OTP, NetworkStatus.ERROR));
            }

            @Override
            public void onComplete() {

            }
        };

        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        repositoryManager.verifyOTP(mobileNumber, otpCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }


    public String getUserToken() {
        if (repositoryManager == null)
            repositoryManager = UserRepositoryManager.getInstance();

        return repositoryManager.getUserToken();
    }

}
