package com.frost.leap.viewmodels.subjects;


import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;

import java.util.ArrayList;
import java.util.List;

import apprepos.subject.SubjectRepositoryManager;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;
import apprepos.topics.model.DownloadSubTopicModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.constants.NetworkStatus;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public class SubjectViewModel extends BaseViewModel {

    private MutableLiveData<List<SubjectModel>> subjectLiveData;
    private MutableLiveData<List<SubjectModel>> liveOfflineSubjects;
    private SubjectRepositoryManager subjectRepositoryManager;
    private List<SubjectModel> subjectModelList;
    private Disposable disposable;
    private AsyncTask offlineSubjectsAsyncTask;

    public SubjectViewModel() {
        super();
        subjectLiveData = new MutableLiveData<>();
        liveOfflineSubjects = new MutableLiveData<>();
        subjectRepositoryManager = SubjectRepositoryManager.getInstance();
    }

    public static final class NetworkTags {

        public static final int SUBJECTS_LIST = 1;
    }

    public LiveData<List<SubjectModel>> updateSubjectsData() {
        return subjectLiveData;
    }

    public LiveData<List<SubjectModel>> updateOfflineSubjectsData() {
        return liveOfflineSubjects;
    }

    public void fetchSubjects(String studentCatalogueId, boolean isRefresh) {

        if (subjectRepositoryManager == null)
            subjectRepositoryManager = SubjectRepositoryManager.getInstance();

        if (!isNetworkAvailable()) {
            subjectRepositoryManager.getOfflineCatalogueSubjects(studentCatalogueId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<SubjectModel>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(List<SubjectModel> offlineSubjects) {
                            if (offlineSubjects != null && offlineSubjects.size() > 0) {
                                subjectModelList = new ArrayList<>();
                                subjectModelList.addAll(offlineSubjects);
                                subjectLiveData.postValue(offlineSubjects);
                            } else {
                                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.NO_DATA));
                                message.postValue(new Message("No Subjects", 2));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.NO_INTERNET));
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
            return;
        }
        if (isRefresh) {
            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.IN_PROCESS));
        }
        Observer<Response<List<SubjectModel>>> observer = new Observer<Response<List<SubjectModel>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<SubjectModel>> response) {

                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.DONE));
                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            if (isRefresh || subjectModelList == null || subjectModelList.size() == 0) {
                                subjectLiveData.postValue(response.body());
                                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.SUCCESS));
                            }
                        } else {
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.NO_DATA));
                            message.postValue(new Message("No Subjects", 2));
                        }
                        subjectModelList = new ArrayList<>();
                        subjectModelList.addAll(response.body());
                        subjectRepositoryManager.updateCatalogueSubjects(response.body(), studentCatalogueId);
                    } else {
                        networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
                        message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    }
                } else if (response.code() == 500) {
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.SERVER_ERROR));
                    message.postValue(new Message(Utility.showErrorMessage(response.errorBody()), 2));
                } else if (response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            fetchSubjects(studentCatalogueId, false);
                        } else {
                            message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                            networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
                        }
                    });
                } else {
                    message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                    networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));
                }
            }

            @Override
            public void onError(Throwable e) {
                message.postValue(new Message(Constants.SOMETHING_WENT_WRONG, 2));
                networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.ERROR));

            }

            @Override
            public void onComplete() {

            }
        };

        subjectRepositoryManager.getSubjects(studentCatalogueId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }


    public void getDownloadSubjects(String catalogueId) {

        if (subjectRepositoryManager == null)
            subjectRepositoryManager = SubjectRepositoryManager.getInstance();

        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        Observer<List<DownloadSubTopicModel>> observer = new Observer<List<DownloadSubTopicModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(List<DownloadSubTopicModel> result) {
                Logger.d("OFFLINE", "AVAILABLE");
                filterDownloadSubjects(result);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                Logger.d("OFFLINE", "NOT AVAILABLE");
                liveOfflineSubjects.postValue(new ArrayList<>());
            }

            @Override
            public void onComplete() {

            }
        };
        Observable<List<DownloadSubTopicModel>> observable = Observable.create(emitter ->
        {
            try {
                emitter.onNext(subjectRepositoryManager.getDownloadSubTopicsDAO().getDownloadSubTopicByCatalogueId(catalogueId));
                emitter.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }
        });
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);


    }

    private void filterDownloadSubjects(List<DownloadSubTopicModel> downloadSubTopicModels) {
        if (offlineSubjectsAsyncTask != null && !offlineSubjectsAsyncTask.isCancelled()) {
            offlineSubjectsAsyncTask.cancel(true);
        }

        offlineSubjectsAsyncTask = new AsyncTask<Void, Void, List<SubjectModel>>() {

            @Override
            protected List<SubjectModel> doInBackground(Void... voids) {

                if (subjectModelList == null)
                    return new ArrayList<>();

                List<SubjectModel> offlineList = new ArrayList<>();

                try {

                    for (SubjectModel subjectModel : subjectModelList) {
                        for (DownloadSubTopicModel downloadSubTopicModel : downloadSubTopicModels) {
                            List<UnitModel> unitModels = new ArrayList<>();
                            if (subjectModel.getStudentSubjectId().equals(downloadSubTopicModel.getSubjectId())) {
                                for (UnitModel unitModel : subjectModel.getStudentUnits()) {
                                    if (downloadSubTopicModel.getUnitId().equals(unitModel.getStudentUnitId())) {
                                        unitModels.add(unitModel);
                                        break;
                                    }
                                }
                                boolean isExist = false;
                                if (offlineList.size() > 0)
                                    for (int c = 0; c < offlineList.size(); c++) {
                                        boolean add = true;
                                        if (offlineList.get(c).getStudentSubjectId().equals(subjectModel.getStudentSubjectId())) {

                                            for (UnitModel existUnitModel : offlineList.get(c).getStudentUnits()) {
                                                if (existUnitModel.getStudentUnitId().equals(unitModels.get(0).getStudentUnitId())) {
                                                    add = false;
                                                    break;
                                                }
                                            }
                                            if (add) {
                                                offlineList.get(c).getStudentUnits().add(unitModels.get(0));
                                            }
                                            isExist = true;
                                            break;
                                        }
                                    }
                                if (!isExist) {
                                    SubjectModel subject = new SubjectModel();
                                    subject.setStudentId(subjectModel.getStudentId());
                                    subject.setStudentCatalogueId(subjectModel.getStudentCatalogueId());
                                    subject.setStudentSubjectId(subjectModel.getStudentSubjectId());
                                    subject.setSubjectName(subjectModel.getSubjectName());
                                    subject.setSubjectId(subjectModel.getSubjectId());


                                    subject.setTotalDurationInSce(subjectModel.getTotalDurationInSce());
                                    subject.setCompletionPercentage(subjectModel.getCompletionPercentage());
                                    subject.setStudentUnits(unitModels);
                                    subject.setSelect(subjectModel.isSelect());
                                    subject.setOffline(subjectModel.isOffline());
                                    offlineList.add(subject);
                                }

                            }

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return offlineList;
            }

            @Override
            protected void onPostExecute(List<SubjectModel> list) {
                super.onPostExecute(list);
                liveOfflineSubjects.postValue(list);
            }
        }.execute();

    }

    public void updateSubjects(List<SubjectModel> list) {
        subjectModelList = new ArrayList<>();
        subjectModelList.addAll(list);
    }

    public List<SubjectModel> getSubjectList() {
        return subjectModelList == null ? new ArrayList<>() : subjectModelList;
    }

    public void stopOfflineData() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        if (offlineSubjectsAsyncTask != null && !offlineSubjectsAsyncTask.isCancelled()) {
            offlineSubjectsAsyncTask.cancel(true);
        }
    }

    public void getOfflineSubjects(String studentCatalogueId) {
        if (subjectRepositoryManager == null)
            subjectRepositoryManager = SubjectRepositoryManager.getInstance();

        subjectRepositoryManager.getOfflineCatalogueSubjects(studentCatalogueId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<SubjectModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<SubjectModel> offlineSubjects) {
                        subjectModelList = new ArrayList<>();
                        subjectModelList.addAll(offlineSubjects);
                        subjectLiveData.postValue(offlineSubjects);
                    }

                    @Override
                    public void onError(Throwable e) {
                        networkCall.postValue(new NetworkCall(NetworkTags.SUBJECTS_LIST, NetworkStatus.IN_PROCESS));
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
