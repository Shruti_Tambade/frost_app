package com.frost.leap.activities;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.frost.leap.R;
import com.frost.leap.components.media.CustomMediaFragment;
import com.frost.leap.components.media.constants.MediaType;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.pdf.PDFFileViewFragment;
import com.frost.leap.components.profile.HelperFragment;
import com.frost.leap.components.profile.QueryFragment;
import com.frost.leap.components.profile.about.AboutFragment;
import com.frost.leap.components.profile.additional.AdditionalSettingsFragment;
import com.frost.leap.components.profile.additional.ImageViewFragment;
import com.frost.leap.components.profile.app_settings.AppSettingsFragment;
import com.frost.leap.components.profile.billing.BillingHistoryFragment;
import com.frost.leap.components.profile.billing.InvoiceDetailsFragment;
import com.frost.leap.components.profile.data_usage.DataUsageFragment;
import com.frost.leap.components.profile.devices.LinkedDevicesFragment;
import com.frost.leap.components.profile.notifications.ProfileNotificationsFragment;
import com.frost.leap.components.profile.settings.ProfileSettingsFragment;
import com.frost.leap.components.profile.support.SubmitRequestFragment;
import com.frost.leap.components.profile.theme.DisplayThemeFragment;
import com.frost.leap.components.quiz.QuizFragment;
import com.frost.leap.components.quiz.QuizOverViewFragment;
import com.frost.leap.components.quiz.QuizResultFragment;
import com.frost.leap.components.quiz.QuizReviewFragment;
import com.frost.leap.components.store_overview.OverviewFragment;
import com.frost.leap.components.store_overview.StoreOverviewFragment;
import com.frost.leap.components.subject.CatalogueOverviewFragment;
import com.frost.leap.components.subject.ots.ClaimFormFragment;
import com.frost.leap.components.updates.AllUpdatesFragment;
import com.frost.leap.components.updates.DoubtClearingFragment;
import com.frost.leap.components.videoplayer.VideoPlayerFragment;
import com.frost.leap.components.web.HelperWebFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.ActivityHelperBinding;
import com.frost.leap.fragments.generic.DummyFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Logger;
import supporters.utils.Utility;

import static com.yalantis.ucrop.UCrop.EXTRA_ASPECT_RATIO_X;
import static com.yalantis.ucrop.UCrop.EXTRA_ASPECT_RATIO_Y;
import static com.yalantis.ucrop.UCrop.EXTRA_INPUT_URI;
import static com.yalantis.ucrop.UCrop.EXTRA_OUTPUT_URI;

public class HelperActivity extends BaseActivity implements PaymentResultListener {


    private ActivityHelperBinding activityHelperBinding;
    private BottomSheetBehavior bottomSheetBehavior;
    private String redirectMethod = null;
    public MenuItem menuDone, menupay;
    private File imageFile = null;
    public TextView tvGet;

    @Override
    public int layout() {
        return R.layout.activity_helper;
    }

    @Override
    public void setUp() {
        setUpView();
        setUpFragment();
    }

    private void setUpView() {

        activityHelperBinding = (ActivityHelperBinding) baseDataBinding;
        setUpSnackBarView(activityHelperBinding.coordinatorLayout);
        setSupportActionBar(activityHelperBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);

        activityHelperBinding.fragmentLayoutBottomSheet.setOnClickListener(v -> {

        });
        activityHelperBinding.frameLayoutHelper.setOnClickListener(v -> {

        });

        bottomSheetBehavior = BottomSheetBehavior.from(activityHelperBinding.fragmentLayoutBottomSheet);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN || newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutBottom, DummyFragment.newInstance(null, null)).commit();
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    activityHelperBinding.frameLayoutBottom.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        activityHelperBinding.llClose.setOnClickListener(click -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        });

        Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 5);
    }

    private void setUpFragment() {
        Intent intent = getIntent();
        int fragmentKey = intent.getIntExtra(Constants.FRAGMENT_KEY, 0);
        Logger.d(Constants.FRAGMENT_KEY, "" + fragmentKey);
        Fragment fragment = null;
        switch (fragmentKey) {

            case AppController.CATALOGUE_OVERVIEW:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                fragment = CatalogueOverviewFragment.newInstance(null, null);
                break;

            case AppController.QUIZ_OVERVIEW:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                fragment = QuizOverViewFragment.newInstance(intent.getParcelableExtra("QuizConstants"),
                        intent.getStringExtra("SubjectId"),
                        intent.getStringExtra("UnitId"),
                        intent.getStringExtra("TopicId"));
                break;

            case AppController.QUIZ_START:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                fragment = QuizFragment.newInstance(intent.getParcelableExtra("QuizQuestionsModel"),
                        intent.getStringExtra("SubjectId"),
                        intent.getStringExtra("UnitId"),
                        intent.getStringExtra("TopicId"), true);
                break;

            case AppController.QUIZ_REVIEW:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                fragment = QuizReviewFragment.newInstance(intent.getStringExtra("StudentQuizId"));
                break;

            case AppController.QUIZ_RESULT:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                fragment = QuizResultFragment.newInstance(intent.getParcelableExtra("QuizQuestionsModel"),
                        intent.getStringExtra("SubjectId"),
                        intent.getStringExtra("UnitId"),
                        intent.getStringExtra("TopicId"));
                break;

            case AppController.PROFILE_SETTINGS:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Profile Settings");
                fragment = ProfileSettingsFragment.newInstance(null, null);
                break;

            case AppController.ADDITIONAL_SETTINGS:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Additional Settings");
                fragment = AdditionalSettingsFragment.newInstance(null, null);
                break;

            case AppController.BILLING_HISTORY:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Subscription Details");
                fragment = BillingHistoryFragment.newInstance(null, null);
                break;

            case AppController.DEFAULT_APP_SETTINGS:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("In-App Download Settings");
                fragment = AppSettingsFragment.newInstance(null, null);
                break;

            case AppController.LINKED_DEVICES:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Linked Devices");
                fragment = LinkedDevicesFragment.newInstance(null, null);
                break;

            case AppController.ABOUT_DEEP_LEARN:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("About Deep Learn");
                fragment = AboutFragment.newInstance(null, null);
                break;

            case AppController.PROFILE_NOTIFICATIONS:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 0);
                changeTitle("Notifications");
                fragment = ProfileNotificationsFragment.newInstance(null, null);
                break;

            case AppController.DATA_USAGE:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 0);
                changeTitle("Data Usagge");
                fragment = DataUsageFragment.newInstance(null, null);
                break;

            case AppController.DISPLAY_THEME:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 0);
                changeTitle("Display Theme");
                fragment = DisplayThemeFragment.newInstance(null, null);
                break;


            case AppController.SUBMIT_REQUEST:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Submit a Request");
                fragment = SubmitRequestFragment.newInstance(getIntent().getParcelableExtra("Issue_Item"),
                        getIntent().getBooleanExtra("Optional", false));
                break;


            case AppController.CLAIM_FORM:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Additional Details");
                fragment = ClaimFormFragment.newInstance(getIntent().getStringExtra("StudentCatalogueId"), null);
                break;

            case AppController.INVOICE_DETAILS:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);

                if (Build.VERSION.SDK_INT == 21 || Build.VERSION.SDK_INT == 22) {
                    Uri uri = Utility.getUriFromInvoice(getIntent().getParcelableExtra("Invoice"));
                    if (uri == null) {
                        Utility.showToast(activity(), "Unable to fetch details of that invoice", false);
                        activity().finish();
                        return;
                    }
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(browserIntent);
                    activity().finish();
                    return;
                }
                changeTitle("Invoice Preview");
                fragment = InvoiceDetailsFragment.newInstance(getIntent().getParcelableExtra("Invoice"), null);
                break;

            case AppController.STORE_OVERVIEW:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 0);
                changeTitle(intent.getStringExtra("CATALOGUE_OVERVIEW"));
                fragment = StoreOverviewFragment.newInstance();
                break;

            case AppController.GALLERY_FRAGMENT:
                if (Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()) && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
                    changeTitle("Gallery");

                    fragment = CustomMediaFragment.newInstance(
                            getIntent().getParcelableArrayListExtra("GalleryFileList"),
                            getIntent().getParcelableArrayListExtra("MediaList"),
                            getIntent().getIntExtra("Type", 1));
                } else {
                    redirectMethod = "setUpFragment";
                    if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity()))
                        Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
                    else if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()))
                        Utility.raisePermissionRequest(Permission.READ_STORAGE, activity());
                    else {

                    }
                }
                break;


            case AppController.WEB_OVERVIEW:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle(getIntent().getStringExtra(Constants.TITLE));
                if (Build.VERSION.SDK_INT == 21 || Build.VERSION.SDK_INT == 22) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(getIntent().getStringExtra("Url")));
                    startActivity(browserIntent);
                    activity().finish();
                    return;
                }
                fragment = HelperWebFragment.newInstance(getIntent().getStringExtra("Url"));
                break;
//            case AppController.PDF_OVERVIEW:
//                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
//                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
//                changeTitle(getIntent().getStringExtra(Constants.TITLE));
//                fragment = PdfViewerFragment.newInstance(getIntent().getStringExtra("Url"));
//                break;

            case AppController.PDF_OVERVIEW:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle(getIntent().getStringExtra(Constants.TITLE));
                fragment = PDFFileViewFragment.newInstance(getIntent().getStringExtra("Url"));
                break;

            case AppController.VIDEO_PLAYER:
                activityHelperBinding.appBarLayout.setVisibility(View.GONE);
                fragment = VideoPlayerFragment.newInstance(getIntent().getStringExtra("VIDEO_URL"));
                break;

            case AppController.HELP_PAGE:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Help");
                fragment = HelperFragment.newInstance(null, null);
                break;

            case AppController.QUERY_PAGE:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle(getIntent().getStringExtra("TITLE_NAME"));
                fragment = QueryFragment.newInstance(getIntent().getStringExtra("TITLE_NAME"), null);
                break;

            case AppController.DOUBT_CLEARING_PAGE:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Back");
                fragment = DoubtClearingFragment.newInstance(getIntent().getParcelableExtra("Update_Item"), null);
                break;


            case AppController.ALL_UPDATES_PAGE:
                activityHelperBinding.appBarLayout.setVisibility(View.VISIBLE);
                Utility.setAppBarElevation(activityHelperBinding.appBarLayout, 2);
                changeTitle("Back");
                fragment = AllUpdatesFragment.newInstance(getIntent().getStringArrayListExtra("CatalogueIDS"), null);
                break;


        }
        if (fragment != null) // add for 5 only
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void beforeSetUp() {

    }

    @Override
    public void changeTitle(String title) {
        activityHelperBinding.tvTitle.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int fragmentKey = getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0);
        switch (fragmentKey) {

            case AppController.GALLERY_FRAGMENT:
                if (getIntent().getBooleanExtra("SinglePick", false)) {
                    return true;
                }
                getMenuInflater().inflate(R.menu.gallery_menu, menu);
                menuDone = menu.findItem(R.id.menu_done);
                menuDone.setVisible(false);
                break;

            case AppController.STORE_OVERVIEW:
                getMenuInflater().inflate(R.menu.menu_catalogue_overview, menu);
                menupay = menu.findItem(R.id.menu_pay);
                menupay.setVisible(false);

                View actionView = menupay.getActionView();
                tvGet = actionView.findViewById(R.id.tvGet);

                tvGet.setOnClickListener(v -> {
                    gotoPayment();
                });
                break;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int fragmentKey = getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0);

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_camera:
                openCameraSnap();
                break;
            case R.id.menu_done:
                if (fragmentKey == AppController.GALLERY_FRAGMENT) {
                    sendMedia();
                }
                break;
            case R.id.menu_pay:
                gotoPayment();
                break;
        }
        return true;
    }

    private void gotoPayment() {

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof StoreOverviewFragment) {
                StoreOverviewFragment storeOverviewFragment = (StoreOverviewFragment) fragment;
                storeOverviewFragment.changeTab(0);
                for (Fragment childFragment : fragment.getChildFragmentManager().getFragments()) {
                    if (childFragment instanceof OverviewFragment) {
                        OverviewFragment overviewFragment = (OverviewFragment) childFragment;
                        overviewFragment.gotoPayment();
                    }
                }
            }
        }
    }


    private void reDirect(String redirectMethod) {
        if (redirectMethod == null) {
            return;
        }

        switch (redirectMethod) {
            case "openCameraSnap":
                openCameraSnap();
                break;
            case "setUpFragment":
                setUpFragment();
                break;
            case "openCustomGallery":
                openCustomGallery();
                break;

        }
    }

    public String getMediaTitle(Integer type) {
        if (type == 2) {
            return "Audios";
        } else if (type == 3) {
            return "Videos";
        } else if (type == 1) {
            return "Gallery";
        } else if (type == 0) {
            return "Documents";
        } else {
            return "Media";
        }
    }

    public void sendMedia() {

        if (getSupportFragmentManager().getFragments().get(0) instanceof CustomMediaFragment) {
            CustomMediaFragment customMediaFragment = (CustomMediaFragment) getSupportFragmentManager().getFragments().get(0);
            customMediaFragment.sendSelectedGalleryFiles();
        }
    }

    public void openCustomGallery() {
        if (Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()) && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
            if (getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0) == AppController.PROFILE_SETTINGS) {
                Intent intent = new Intent(activity(), HelperActivity.class);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.GALLERY_FRAGMENT);// gallery
                intent.putParcelableArrayListExtra("MediaList", null);
                intent.putParcelableArrayListExtra("GalleryFileList", null);
                startActivityForResult(intent, Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY"));
                return;
            } else if (getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0) == AppController.CLAIM_FORM) {
                Intent intent = new Intent(activity(), HelperActivity.class);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.GALLERY_FRAGMENT);// gallery
                intent.putParcelableArrayListExtra("MediaList", null);
                intent.putParcelableArrayListExtra("GalleryFileList", null);
                startActivityForResult(intent, Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY"));
                return;
            } else if (getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0) == AppController.ADDITIONAL_SETTINGS) {
                Intent intent = new Intent(activity(), HelperActivity.class);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.GALLERY_FRAGMENT);// gallery
                intent.putParcelableArrayListExtra("MediaList", null);
                intent.putParcelableArrayListExtra("GalleryFileList", null);
                startActivityForResult(intent, Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY"));
                return;
            } else if (getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0) == AppController.SUBMIT_REQUEST) {

                if (getSupportFragmentManager().getFragments().get(0) instanceof SubmitRequestFragment) {

                    SubmitRequestFragment submitRequestFragment = (SubmitRequestFragment) getSupportFragmentManager().getFragments().get(0);
                    Intent intent = new Intent(activity(), HelperActivity.class);
                    intent.putExtra(Constants.FRAGMENT_KEY, AppController.GALLERY_FRAGMENT);// gallery
                    intent.putParcelableArrayListExtra("MediaList", null);
                    intent.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) submitRequestFragment.getSelectedGallery());
                    startActivityForResult(intent, Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY"));
                }
                return;
            }
        } else {
            redirectMethod = "openCustomGallery";
            if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
            else if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.READ_STORAGE, activity());
            else {

            }
        }
    }

    public void openCameraSnap() {
        if (Utility.checkPermissionRequest(Permission.CAMERA, activity())) {
            Intent snapIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (snapIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = Utility.createImageFile(activity());

//                    Continue only if the File was successfully created
                    if (photoFile != null) {
                        imageFile = photoFile;
                        Uri photoUri = FileProvider.getUriForFile(activity(), Constants.FILE_PROVIDER_AUTHORITY, photoFile);
                        snapIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        snapIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                snapIntent.setClipData(ClipData.newRawUri("", photoUri));
                            }
                            snapIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                        startActivityForResult(snapIntent, Utility.generateRequestCodes().get("SNAP_FROM_CAMERA"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        } else {
            redirectMethod = "openCameraSnap";
            Utility.raisePermissionRequest(Permission.CAMERA, activity());
        }
    }

    private void addPictureToGallery() {

        if (imageFile != null) {
            MediaScannerConnection.scanFile(
                    getApplicationContext(),
                    new String[]{imageFile.getAbsolutePath()},
                    new String[]{"image/jpeg"},
                    (path, uri) -> refreshCustomGallery());
        } else {
            Utility.showToast(activity(), Constants.SOMETHING_WENT_WRONG, false);
        }
    }

    public void refreshCustomGallery() {
        int fragmentKey = getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0);
        switch (fragmentKey) {
            case AppController.GALLERY_FRAGMENT:
                if (getSupportFragmentManager().getFragments().get(0) instanceof CustomMediaFragment) {
                    CustomMediaFragment customMediaFragment = (CustomMediaFragment) getSupportFragmentManager().getFragments().get(0);
                    customMediaFragment.refresh();
                }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logger.d("Request Code", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("READ_STORAGE_REQUEST")) {
            if (Utility.checkPermissionRequest(Permission.READ_STORAGE, activity())) {
                reDirect(redirectMethod);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        Utility.showToast(activity(), Constants.GIVE_ALL_PERMISSIONS, false);
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                }
                showSnackBar("Permission denied", 2);
            }
        } else if (requestCode == Utility.generateRequestCodes().get("WRITE_STORAGE_REQUEST")) {
            if (Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
                if (getIntent().getIntExtra(Constants.FRAGMENT_KEY, 0) == AppController.DEFAULT_APP_SETTINGS) {
                    if (getSupportFragmentManager().getFragments().get(0) instanceof AppSettingsFragment) {
                        AppSettingsFragment appSettingsFragment = (AppSettingsFragment) getSupportFragmentManager().getFragments().get(0);
                        appSettingsFragment.setExternalStorage();
                    }
                    return;
                }
                reDirect(redirectMethod);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Utility.showToast(activity(), Constants.GIVE_ALL_PERMISSIONS, false);
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                }
                showSnackBar("Permission denied", 2);
            }
        } else if (requestCode == Utility.generateRequestCodes().get("CAMERA_REQUEST")) {
            if (Utility.checkPermissionRequest(Permission.CAMERA, activity())) {
                reDirect(redirectMethod);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                        Utility.showToast(activity(), Constants.GIVE_ALL_PERMISSIONS, false);
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                }
                Utility.showToast(activity(), "Permission denied", false);
            }
        } else {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST_CODE", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY")) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                if (getSupportFragmentManager().getFragments().get(0) instanceof ProfileSettingsFragment) {
                    List<GalleryFile> galleryFileList = data.getParcelableArrayListExtra("GalleryFileList");
                    if (galleryFileList != null && galleryFileList.size() == 0)
                        return;


                    Intent mCropIntent = new Intent(activity(), CustomImageCropActivity.class);
                    Bundle mCropOptionsBundle = new Bundle();
                    mCropOptionsBundle.putParcelable(EXTRA_INPUT_URI, galleryFileList.get(0).uri);
                    mCropOptionsBundle.putParcelable(EXTRA_OUTPUT_URI, Uri.fromFile(new File(getCacheDir(), Constants.EDITED_PROFILE_PIC + ".jpg")));
                    mCropOptionsBundle.putFloat(EXTRA_ASPECT_RATIO_X, 1);
                    mCropOptionsBundle.putFloat(EXTRA_ASPECT_RATIO_Y, 1);
                    mCropIntent.putExtras(mCropOptionsBundle);
                    startActivityForResult(mCropIntent, Utility.generateRequestCodes().get("IMAGE_CROP"));

                } else if (getSupportFragmentManager().getFragments().get(0) instanceof ClaimFormFragment) {
                    List<GalleryFile> galleryFileList = data.getParcelableArrayListExtra("GalleryFileList");
                    if (galleryFileList != null && galleryFileList.size() == 0)
                        return;


                    ClaimFormFragment claimFormFragment = (ClaimFormFragment) getSupportFragmentManager().getFragments().get(0);

                    if (!claimFormFragment.allowCrop) {
                        if (claimFormFragment.isFront) {
                            claimFormFragment.updateFront(galleryFileList);
                        } else
                            claimFormFragment.updateBack(galleryFileList);

                        return;
                    }

                    Intent mCropIntent = new Intent(activity(), CustomImageCropActivity.class);
                    Bundle mCropOptionsBundle = new Bundle();
                    mCropOptionsBundle.putParcelable(EXTRA_INPUT_URI, galleryFileList.get(0).uri);
                    mCropOptionsBundle.putParcelable(EXTRA_OUTPUT_URI, Uri.fromFile(new File(getCacheDir(), Constants.EDITED_PROFILE_PIC + ".jpg")));
                    mCropOptionsBundle.putFloat(EXTRA_ASPECT_RATIO_X, 1);
                    mCropOptionsBundle.putFloat(EXTRA_ASPECT_RATIO_Y, 1);
                    mCropIntent.putExtras(mCropOptionsBundle);
                    startActivityForResult(mCropIntent, Utility.generateRequestCodes().get("IMAGE_CROP"));

                } else if (getSupportFragmentManager().getFragments().get(0) instanceof SubmitRequestFragment) {
                    List<GalleryFile> galleryFileList = data.getParcelableArrayListExtra("GalleryFileList");
                    if (galleryFileList != null && galleryFileList.size() == 0)
                        return;
                    SubmitRequestFragment submitRequestFragment = (SubmitRequestFragment) getSupportFragmentManager().getFragments().get(0);
                    submitRequestFragment.updateGalleryFiles(galleryFileList);
                } else if (getSupportFragmentManager().getFragments().get(0) instanceof AdditionalSettingsFragment) {
                    List<GalleryFile> galleryFileList = data.getParcelableArrayListExtra("GalleryFileList");
                    if (galleryFileList != null && galleryFileList.size() == 0)
                        return;
                    AdditionalSettingsFragment additionalSettingsFragment = (AdditionalSettingsFragment) getSupportFragmentManager().getFragments().get(0);
                    additionalSettingsFragment.updateProofPic(galleryFileList);
                }

            }
        } else if (requestCode == Utility.generateRequestCodes().get("SNAP_FROM_CAMERA")) {
            if (resultCode == RESULT_OK) {
                addPictureToGallery();
            }
        } else if (requestCode == Utility.generateRequestCodes().get("IMAGE_CROP")) {
            if (data != null && resultCode == RESULT_OK) {
                if (getSupportFragmentManager().getFragments().get(0) instanceof ProfileSettingsFragment) {
                    GalleryFile galleryFile = new GalleryFile();
                    galleryFile.mimeType = "image/jpeg";
                    galleryFile.mediaType = MediaType.IMAGE;
                    galleryFile.uri = data.getParcelableExtra("OUTPUT_URI");
                    galleryFile.ratio = 1;
                    galleryFile.size = Constants.SAMPLE_FILE_SIZE;


                    ProfileSettingsFragment profileSettingsFragment = (ProfileSettingsFragment) getSupportFragmentManager().getFragments().get(0);
                    profileSettingsFragment.updateProfilePic(Arrays.asList(galleryFile));


                } else if (getSupportFragmentManager().getFragments().get(0) instanceof ClaimFormFragment) {

                    GalleryFile galleryFile = new GalleryFile();
                    galleryFile.mimeType = "image/jpeg";
                    galleryFile.mediaType = MediaType.IMAGE;
                    galleryFile.uri = data.getParcelableExtra("OUTPUT_URI");
                    galleryFile.ratio = 1;
                    galleryFile.size = Constants.SAMPLE_FILE_SIZE;

                    ClaimFormFragment claimFormFragment = (ClaimFormFragment) getSupportFragmentManager().getFragments().get(0);
                    claimFormFragment.updateProfilePic(Arrays.asList(galleryFile));
                }
            }
        } else {

        }
    }

    @Override
    public void onBackPressed() {

        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        } else if (activityHelperBinding.frameLayoutHelper.getVisibility() == View.VISIBLE) {
            activityHelperBinding.frameLayoutHelper.setVisibility(View.GONE);
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutHelper, DummyFragment.newInstance(null, null)).commit();
            return;
        } else {

            if (getIntent().getIntExtra(Constants.FRAGMENT_KEY, -1) == AppController.CLAIM_FORM) {
                if (getSupportFragmentManager().getFragments().get(0) instanceof ClaimFormFragment) {
                    ClaimFormFragment claimFormFragment = (ClaimFormFragment) getSupportFragmentManager().getFragments().get(0);
                    claimFormFragment.onBackPressed();
                }
            } else {
                finish();
            }
        }
    }


    public void restartApp() {
        Utility.changeTheme(activity());
    }

    public void changeTab() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof StoreOverviewFragment) {

                StoreOverviewFragment storeOverviewFragment = (StoreOverviewFragment) fragment;
                storeOverviewFragment.changeTab(1);
            }
        }
    }


    public void startPayment(JSONObject jsonObject) {


        final Checkout checkout = new Checkout();
        try {
//            JSONObject options = new JSONObject();
//            options.put("currency", "INR");
//            options.put("amount", "100.0");
//            options.put("payment_capture", "1");
//            options.put("description", "ESE - General Studies and Engineering Aptitude");
//            options.put("currency", "INR");
//            options.put("order_id","order_EeihNVe7wt2LP7");
//            options.put("customer_id","cust_D0xmKLwFg5haTo");
//            JSONObject preFill = new JSONObject();
//            preFill.put("email", "gokulkalagara@gmail.com");
//            preFill.put("contact", "7207824353");
//            options.put("prefill", preFill);
            checkout.open(activity(), jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String paymentId) {
        Logger.d("PAYMENT ID", paymentId);
        if (getSupportFragmentManager().getFragments().get(0) instanceof StoreOverviewFragment) {
            StoreOverviewFragment storeOverviewFragment = (StoreOverviewFragment) getSupportFragmentManager().getFragments().get(0);
            storeOverviewFragment.updateOrderStatus(paymentId, true);
        }
    }


    @Override
    public void onPaymentError(int i, String s) {
        Logger.d("PAYMENT ERROR", s + "  " + i);
        showSnackBar("Payment failed", 0);

        if (getSupportFragmentManager().getFragments().get(0) instanceof StoreOverviewFragment) {
            StoreOverviewFragment storeOverviewFragment = (StoreOverviewFragment) getSupportFragmentManager().getFragments().get(0);
            storeOverviewFragment.updateOrderStatus(null, false);
        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    public void openFullImageView(Uri uri) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutBottom, ImageViewFragment.newInstance(uri)).commit();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
        } catch (IllegalStateException e) {
            try {
                Field f = Activity.class.getDeclaredField("mCalled");
                f.setAccessible(true);
                f.set(this, true);
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException e1) {
            }
        }
    }

//    @Override
//    public void onPaymentSuccess(String paymentId, PaymentData paymentData) {
//        Logger.d("Payment Id", paymentId);
//        Logger.d("Order Id", paymentData.getOrderId());
//        Logger.d("Payment Signature", paymentData.getSignature());
//        Toast.makeText(activity(), "SUCCESS" + paymentData.getSignature(), Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onPaymentError(int i, String s, PaymentData paymentData) {
//        Toast.makeText(activity(), "DATA " + paymentData.getUserEmail(), Toast.LENGTH_SHORT).show();
//    }
}


