package com.frost.leap.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.frost.leap.BuildConfig;
import com.frost.leap.R;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.controllers.AppController;
import com.frost.leap.interfaces.IActivity;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.services.HelperIntentService;
import com.frost.leap.services.video.DownloadLicenseService;
import com.frost.leap.viewmodels.BaseViewModel;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.scottyab.rootbeer.RootBeer;

import java.io.File;
import java.lang.reflect.Type;

import apprepos.topics.TopicRepositoryManager;
import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import storage.AppSharedPreferences;
import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 08-05-2019.
 * <p>
 * Frost
 */
public abstract class BaseActivity extends AppCompatActivity implements IActivity, ICustomAlertDialog {

    public ViewDataBinding baseDataBinding;
    private View snackBarView;
    private AppSharedPreferences appSharedPreferences;
    public MixpanelAPI mixpanel;

    @LayoutRes
    public abstract int layout();

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(activity(), R.color.black));
        }

        appSharedPreferences = AppSharedPreferences.getInstance();
        try {
            if (appSharedPreferences.getBoolean(Constants.IS_DARK_MODE)) {
                setTheme(R.style.darktheme);
            } else {
                setTheme(R.style.Lighttheme);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onCreate(savedInstanceState);
        beforeSetUp();
        baseDataBinding = DataBindingUtil.setContentView(activity(), layout());
        setUp();

        //MixPanel Initialization

        if (mixpanel == null)
            mixpanel = MixpanelAPI.getInstance(activity(), Constants.MIXPANEL_TOKEN);

        setPeopleInfoToMixPanel();
        verifyIsRooted();
        getRemoteConfigInfo();
    }

    private void setPeopleInfoToMixPanel() {

        Gson gson = new Gson();
        Type type = new TypeToken<User>() {
        }.getType();

        if (appSharedPreferences.getString(Constants.UserInfo.USER_DATA) != null) {
            User user = gson.fromJson(appSharedPreferences.getString(Constants.UserInfo.USER_DATA), type);
            Mixpanel.loginVerify(true, "Successfully Login", user, "", true);
        }

    }

    private void getRemoteConfigInfo() {

        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.reset();
        firebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(this, task ->
        {
            if (task.isSuccessful()) {
                UserRepositoryManager.getInstance().setForceSecurityLevel(firebaseRemoteConfig.getString("forceSecurityLevel"));
                UserRepositoryManager.getInstance().saveStoreBanner(firebaseRemoteConfig.getString("appStoreBanner"));
                UserRepositoryManager.getInstance().allowLicenseCache(firebaseRemoteConfig.getString("allowLicenseCache"));
                UserRepositoryManager.getInstance().storeUpdateDetails(firebaseRemoteConfig.getString("androidUpdateVersionDetails"));
                UserRepositoryManager.getInstance().saveCredentials(firebaseRemoteConfig.getString("firebaseCredentials"));
                UserRepositoryManager.getInstance().savePhoneSupport(firebaseRemoteConfig.getString("phoneSupport"));
                UserRepositoryManager.getInstance().saveHlsVideos(firebaseRemoteConfig.getString("hlsVideos"));
                UserRepositoryManager.getInstance().saveAutoResolutions(firebaseRemoteConfig.getString("autoResolutions"));
                TopicRepositoryManager.getInstance().saveDRMTags(firebaseRemoteConfig.getString("drmLicenseUrl"), firebaseRemoteConfig.getString("drmOfflineLicenseUrl"));
                UserRepositoryManager.getInstance().saveRSAPublicKey(firebaseRemoteConfig.getString("rsaPublicKey"));
                if (layout() == R.layout.activity_helper) {
                    return;
                }
//                 CHECKING FOR ANY UPDATE
                if (BuildConfig.BUILD_TYPE.equals("debug")) {
                    return;
                }

                int currentVersion = (int) firebaseRemoteConfig.getLong("androidVersionCode");

                if (currentVersion > BuildConfig.VERSION_CODE) {
                    String updateType = firebaseRemoteConfig.getString("androidUpdateType");
                    if (updateType != null) {
                        showUpdateApplication(updateType);
                    }

                } else {

                }
            } else {
                UserRepositoryManager.getInstance().saveDRMTagsFromLocal();
            }
        });
    }

    public abstract void setUp();

    public abstract void beforeSetUp();

    public void setUpSnackBarView(View view) {
        this.snackBarView = view;
    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(activity(), snackBarView, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return this;
    }


    private void verifyIsRooted() {
        RootBeer rootBeer = new RootBeer(activity());
        if (rootBeer.isRooted()) {
            if (rootBeer.isRootedWithBusyBoxCheck())
                showRootSecurityAlert();
        }
    }

    private void showRootSecurityAlert() {
        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity());
        builder.setTitle("Security Alert");
        builder.setMessage("We not allowing to access application in root devices due to security issues. We are sorry for inconvenience");
        builder.setCancelable(false);
        builder.setPositiveButton("CLOSE", (dialog, which) -> {
            finish();
        });
        builder.show();
    }


    public void showUpdateApplication(String updateType) {

        Utility.requestDialogAlert(activity(), this,
                "We're Better Than Ever",
                updateType.equals("HARD") ? "Please download the latest update to continue using the application "
                        : "Please download the latest update to get the best experience",
                1439,
                "UPDATE",
//                BuildConfig.DEBUG ? true : !isCancel,
                updateType.equals("HARD") ? true : false,
                updateType.equals("HARD") ? true : false);
    }


    public void doLogout(BaseViewModel viewModel) {

        Mixpanel.clickLogout();

        Intent intentService = new Intent(activity(), HelperIntentService.class);
        intentService.putExtra(Constants.ACTION_KEY, AppController.ACTION_LOGOUT);
        intentService.putExtra(Constants.TOKEN, viewModel.getUserToken());
        activity().startService(intentService);

        viewModel.clearLoginDetails();
        //deleteDashUrls();


        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("login", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void deleteDashUrls() {
        TopicRepositoryManager.getInstance().clearAllDashUrls();
    }


    public void deleteOfflineVideo() {
        try {
            TopicRepositoryManager.getInstance().clearAllDownloadSubTopics();
            HelperApplication.getInstance().getDownloadManager().pauseDownloads();
            HelperApplication.getInstance().getDownloadManager().removeAllDownloads();
            File dir = new File(HelperApplication.getInstance().getDownloadDirectory(), HelperApplication.DOWNLOAD_CONTENT_DIRECTORY);
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    if (new File(dir, children[i]).isDirectory()) {
                        File subDir = new File(dir, children[i]);
                        String[] childrenSubDir = subDir.list();
                        for (int j = 0; j < childrenSubDir.length; j++) {
                            new File(subDir, childrenSubDir[j]).delete();
                        }
                        new File(dir, children[i]).delete();
                    } else
                        new File(dir, children[i]).delete();
                }
            }
            Intent intent = new Intent(activity(), DownloadLicenseService.class);
            intent.putExtra("CancelAll", true);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearAllExoFiles() {
        try {

            File dir = null;
            if (Utility.isExternalStorageWritable(activity()) && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, getApplicationContext())) {
                File[] externalStorageVolumes =
                        ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
                dir = new File(externalStorageVolumes[1], HelperApplication.DOWNLOAD_CONTENT_DIRECTORY);
                if (dir.isDirectory()) {
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++) {
                        if (new File(dir, children[i]).isDirectory()) {
                            File subDir = new File(dir, children[i]);
                            String[] childrenSubDir = subDir.list();
                            for (int j = 0; i < childrenSubDir.length; j++) {
                                new File(subDir, childrenSubDir[j]).delete();
                            }
                            new File(dir, children[i]).delete();
                        } else
                            new File(dir, children[i]).delete();
                    }
                }
            }
            dir = getExternalFilesDir(null);
            if (dir == null) {
                dir = getFilesDir();
            }
            dir = new File(dir, HelperApplication.DOWNLOAD_CONTENT_DIRECTORY);
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    if (new File(dir, children[i]).isDirectory()) {
                        File subDir = new File(dir, children[i]);
                        String[] childrenSubDir = subDir.list();
                        for (int j = 0; j < childrenSubDir.length; j++) {
                            new File(subDir, childrenSubDir[j]).delete();
                        }
                        new File(dir, children[i]).delete();
                    } else
                        new File(dir, children[i]).delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void gotoPlayStore() {
        String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    public void doPositiveAction(int id) {
        switch (id) {
            case 1439:
                gotoPlayStore();
                break;
        }
    }

    @Override
    public void doNegativeAction() {

    }

    @Override
    protected void onDestroy() {
        if (mixpanel != null) {
            mixpanel.flush();
        }

        super.onDestroy();
    }

    //setting animation programatically
    /*@Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }*/
}
