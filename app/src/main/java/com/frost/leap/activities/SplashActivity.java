package com.frost.leap.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.components.start.LostPhoneFragment;
import com.frost.leap.components.start.SignInFragment;
import com.frost.leap.components.start.SignUpFragment;
import com.frost.leap.components.start.VerifyOtpFragment;
import com.frost.leap.components.start.VerifyType;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.ActivitySplashBinding;
import com.frost.leap.services.video.VideoDownloadService;
import com.frost.leap.viewmodels.helper.HelperViewModel;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.Credentials;
import com.google.android.gms.auth.api.credentials.HintRequest;

import java.lang.reflect.Field;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import apprepos.updates.model.UpdateModel;
import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import io.reactivex.disposables.Disposable;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

public class SplashActivity extends BaseActivity {


    private ActivitySplashBinding activityBinding;
    private HelperViewModel viewModel;
    private UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();

    @Override
    public int layout() {
        return R.layout.activity_splash;
    }

    @Override
    public void setUp() {
        activityBinding = (ActivitySplashBinding) baseDataBinding;
        setUpSnackBarView(activityBinding.coordinatorLayout);
        setUpFragment();
    }


    private void setUpFragment() {

//        if (!userRepositoryManager.appSharedPreferences.getBoolean(Constants.IS_FIRST_LOGIN)) {
//
////            Utility.showToast(activity(), "Fist App Open", true);
//            userRepositoryManager.appSharedPreferences.setBoolean(Constants.IS_FIRST_LOGIN, true);
//            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, IntroductionFragment.newInstance(null, null)).addToBackStack("IntroductionFragment").commit();
//
//        } else {
////            Utility.showToast(activity(), "Second App Open", true);
//            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, IntroductionFragment.newInstance(null, null)).addToBackStack("IntroductionFragment").commit();
////            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, SignInFragment.newInstance(null, null)).addToBackStack("SignInFragment").commit();
//        }

        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, SignInFragment.newInstance(null, null)).addToBackStack("SignInFragment").commit();
    }

    @Override
    public void beforeSetUp() {
        setTheme(R.style.SplashTheme);
        Logger.d("HashKey: ", Utility.getAppSignature());
        // This code requires one time to get Hash keys do comment and share key
        viewModel = new ViewModelProvider(this).get(HelperViewModel.class);
        disableSSLCertificateVerify();
        if (viewModel.isLogin()) {
            if (getIntent() != null && getIntent().hasExtra("UpdateId") && getIntent().getStringExtra("UpdateId") != null) {
                redirectToUpdatePage(getIntent().getStringExtra("UpdateId"));
                getIntent().removeExtra("UpdateId");
                return;
            }
            crossCheckPendingDownloads();
            gotoHome(false);
        } else {
            if (viewModel.clearUserDownloads()) {
                deleteDashUrls();
                deleteOfflineVideo();
            }
        }

    }

    private void redirectToUpdatePage(String updateID) {

        UpdateModel updateModel = new UpdateModel();
        updateModel.setUpdateId(updateID);

        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.DOUBT_CLEARING_PAGE);
        intent.putExtra("Update_Item", updateModel);
        startActivity(intent);
        finish();

    }


    @Override
    public void changeTitle(String title) {

    }

    private static void disableSSLCertificateVerify() {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> {
                if (session.isValid()) {
                    return true;
                }
                return false;
            });
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    public void crossCheckPendingDownloads() {
        try {
            VideoDownloadService.notify = false;
            Intent intent = new Intent(activity(), VideoDownloadService.class);
            startService(intent);
        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    public void gotoSignUp() {
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, SignUpFragment.newInstance(null, null)).addToBackStack("SignUpFragment").commit();
    }

    public void gotoVerifyOtp(String key, String value, VerifyType verifyType, User user) {
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, VerifyOtpFragment.newInstance(key, value, verifyType, user)).addToBackStack("VerifyOtpFragment").commit();
    }

    public void gotoLostPhone() {
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, LostPhoneFragment.newInstance(null, null)).addToBackStack("LostPhoneFragment").commit();
    }

    public void clearAndGotoSignUp() {
        onBackPressed();
        gotoSignUp();
    }

    private Disposable disposable;
    Runnable runnable = null;

    public void gotoHome(boolean signUp) {
        Intent intent = new Intent(activity(), MainActivity.class);
        intent.putExtra("SignUp", signUp);
        startActivity(intent);
        activity().finish();
    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        } else {
            super.onBackPressed();
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                if (getSupportFragmentManager().getFragments().get(0) instanceof SignInFragment) {
                    SignInFragment signInFragment = (SignInFragment) getSupportFragmentManager().getFragments().get(0);
                    signInFragment.setFocus(true);
                }
            }
        }
    }


    public void requestMobileHint() {
        try {
            HintRequest hintRequest = new HintRequest.Builder()
                    .setPhoneNumberIdentifierSupported(true)
                    .build();
            PendingIntent intent = Credentials.getClient(activity()).getHintPickerIntent(hintRequest);
            startIntentSenderForResult(intent.getIntentSender(), Utility.generateRequestCodes().get("REQUEST_HINT_MOBILE"), null, 0, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logger.d("Request Code", "" + requestCode);
//        if (requestCode == Utility.generateRequestCodes().get("CALL_PHONE")) {
//            if (Utility.checkPermissionRequest(Permission.CALL, activity())) {
//                if(getSupportFragmentManager().getFragments().size()>0){
//                    for (Fragment fragment: getSupportFragmentManager().getFragments()){
//                        if(fragment instanceof SignInFragment){
//                            SignInFragment signInFragment = (SignInFragment) fragment;
//                            signInFragment.flashCallVerification();
//                            break;
//                        }
//                    }
//                }
//            }
//            else {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    if (!shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
//                        Utility.showToast(activity(), Constants.GIVE_ALL_PERMISSIONS, false);
//                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        Uri uri = Uri.fromParts("package", getPackageName(), null);
//                        intent.setData(uri);
//                        startActivity(intent);
//                    }
//                }
//                showSnackBar("Permission denied", 2);
//            }
//        }
    }

    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utility.generateRequestCodes().get("REQUEST_HINT_MOBILE")) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                Logger.d("KEY Id", credential.getId());
                if (getSupportFragmentManager().getFragments().size() > 0)
                    if (getSupportFragmentManager().getFragments().get(0) instanceof SignInFragment) {
                        SignInFragment signInFragment = (SignInFragment) getSupportFragmentManager().getFragments().get(0);
                        signInFragment.updateMobileNumber(credential.getId());
                    }
            }
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
        } catch (IllegalStateException e) {
            try {
                Field f = Activity.class.getDeclaredField("mCalled");
                f.setAccessible(true);
                f.set(this, true);
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException e1) {
            }
        }
    }

}
