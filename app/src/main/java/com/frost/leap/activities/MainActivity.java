package com.frost.leap.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.components.catalogue.CatalogueFragment;
import com.frost.leap.components.subject.CatalogueOverviewFragment;
import com.frost.leap.components.subject.askexpert.BSDAskDoubtFragment;
import com.frost.leap.components.subject.notes.BSDAddNoteFragment;
import com.frost.leap.components.subject.notes.BSDApplyFilterFragment;
import com.frost.leap.components.subject.notes.INotesFilter;
import com.frost.leap.components.topics.BSDDownloadRequestFragment;
import com.frost.leap.components.topics.BSDVideoRatingFragment;
import com.frost.leap.components.topics.BSDVideoReportFragment;
import com.frost.leap.components.topics.UnitOverViewFragment;
import com.frost.leap.components.topics.UnitProviderFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.ActivityMainBinding;
import com.frost.leap.fragments.generic.DummyFragment;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.services.HelperIntentService;
import com.frost.leap.services.models.Report;
import com.frost.leap.services.video.LicenseExpiryIntentService;
import com.frost.leap.viewmodels.helper.HelperViewModel;
import com.frost.leap.viewpager.HomeViewPagerAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.firebase.perf.metrics.Trace;

import java.lang.reflect.Field;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;
import apprepos.topics.model.QuizModel;
import apprepos.topics.model.StudentTopicsModel;
import apprepos.topics.model.SubTopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;
import supporters.constants.Constants;
import supporters.customviews.badge.Badge;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

public class MainActivity extends BaseActivity {

    public ActivityMainBinding activityBinding;
    private BottomSheetBehavior bottomSheetBehavior;
    private String redirectMethod = null;
    private Badge cartBadge = null;
    private boolean isTransitionExpand = true;
    private int height;
    private UnitProviderFragment unitProviderFragment;
    private HelperViewModel viewModel;
    private String storeCatalogueId = null, dashboardCatalogueId = null;
    private String appPath;
    private Trace mainActivitySetUpTrace;

    @Override
    public int layout() {
        return R.layout.activity_main;
    }

    @Override
    @AddTrace(name = "MainActivitySetUp", enabled = true)
    public void setUp() {


        activityBinding = (ActivityMainBinding) baseDataBinding;
        setUpSnackBarView(activityBinding.coordinatorLayout);

        // Code to handle the deeplink
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();

        activityBinding.coordinatorLayout.post(() -> {
            height = activityBinding.coordinatorLayout.getHeight();
        });

        activityBinding.includedAppBarLayout.includedSubLayout.bottomCard.setTranslationY(0);
        activityBinding.includedAppBarLayout.appBarLayout.setVisibility(View.GONE);

        activityBinding.includedAppBarLayout.includedSubLayout.navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
//        activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("⬤");
//        activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("");
//        activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("");

//        activityBinding.includedAppBarLayout.includedSubLayout.navigation.setItemIconTintList(Utility.generateColors(AppSharedPreferences.getInstance().getBoolean(Constants.IS_DARK_MODE)));

//        activityBinding.includedAppBarLayout.includedSubLayout.navigation.setItemBackground(0,R.drawable.rectangle_coupon);

        bottomSheetBehavior = BottomSheetBehavior.from(activityBinding.fragmentLayoutBottomSheet);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Logger.d("Bottom_Sheet", "" + newState);
                Utility.hideKeyboard(activity());
                if (newState == BottomSheetBehavior.STATE_HIDDEN || newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    try {
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutBottom, DummyFragment.newInstance(null, null)).commit();
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        activityBinding.frameLayoutBottom.setLayoutParams(layoutParams);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        activityBinding.fragmentLayoutBottomSheet.setOnClickListener(v -> {

        });

        if (appLinkAction == Intent.ACTION_VIEW) {
            Logger.d("AppLink Action " + appLinkAction);
            Logger.d("AppLink Data " + appLinkIntent.getData());
            Uri appLinkData = appLinkIntent.getData();
            String appHost = appLinkData.getHost();
            appPath = appLinkData.getEncodedPath();

            if (appPath.equals("/page1"))
                dashboardCatalogueId = appLinkData.getQueryParameter("id");
            else if (appPath.equals("/page2"))
                storeCatalogueId = appLinkData.getQueryParameter("id");

            Logger.d("AppPath " + appPath);
            Logger.d("AppID " + appPath);
        }


        setUpViewPager(dashboardCatalogueId, storeCatalogueId);

        if (appLinkAction == Intent.ACTION_VIEW) {
            if (viewModel.isLogin()) {
                if (appPath.equals("/page1")) {
                    //do some action after the deeplink has been resolved
                    goToHome(0);
                }
                if (appPath.equals("/page2")) {
                    //do some action after the deeplink has been resolved
                    goToHome(1);
                }
                if (appPath.equals("/page3")) {
                    //do some action after the deeplink has been resolved
                    goToHome(2);
                }
            } else {
                doLogout(viewModel);
                return;
            }
        }

        //cacheLicense();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            activityBinding.videoFrameLayout.setOnTouchListener((view, motionEvent) -> true);
        } else if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            // Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            activityBinding.videoFrameLayout.setOnTouchListener((view, motionEvent) -> false);
        }
    }

    private void setUpViewPager(String dashboardCatalogueId, String storeCatalogueId) {

        activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setPagingEnabled(false);
        activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setOffscreenPageLimit(3);
        activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setAdapter(new HomeViewPagerAdapter(getSupportFragmentManager(), storeCatalogueId, dashboardCatalogueId));

        if (getIntent().getBooleanExtra("SignUp", false)) {
            goToHome(1);
        }
    }

    BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    Utility.hideKeyboard(activity());
                    item.setChecked(true);

                    switch (item.getItemId()) {

                        case R.id.navigation_home:
                            activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setCurrentItem(0, false);
                            Mixpanel.clickTabs(1);
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("⬤");
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("");
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("");
                            break;
                        case R.id.navigation_cart:
                            activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setCurrentItem(1, false);
                            Mixpanel.clickTabs(2);
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("");
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("⬤");
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("");
                            break;
                        case R.id.navigation_profile:
                            activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setCurrentItem(2, false);
                            Mixpanel.clickTabs(3);
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("");
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("");
//                            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("⬤");
                            break;

                    }
                    return false;
                }
            };


    @Override
    public void beforeSetUp() {
        viewModel = new ViewModelProvider(this).get(HelperViewModel.class);
        viewModel.MigrationLicenseToDB();
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            Utility.hideKeyboard(activity());
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        } else if (activityBinding.videoFrameLayout.getVisibility() == View.VISIBLE) {

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof UnitProviderFragment) {

                    UnitProviderFragment unitProviderFragment = (UnitProviderFragment) fragment;


                    if (this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {

                        ((UnitProviderFragment) fragment).controlScreenOrientation(-1);

                    } else {
                        if (unitProviderFragment.binding.skeltonView.getVisibility() == View.VISIBLE) {
                            activityBinding.videoFrameLayout.setVisibility(View.GONE);
                            unitProviderFragment.binding.skeltonView.setVisibility(View.GONE);
                            super.onBackPressed();

                        } else {
                            if (isTransitionExpand) {
                                unitProviderFragment.endTransaction();

                            } else {

                                for (Fragment fragment1 : getSupportFragmentManager().getFragments()) {
                                    if (fragment1 instanceof CatalogueOverviewFragment) {
                                        removeFragment(fragment1);
                                        changeMotion(true);
                                        activityBinding.frameLayoutHelper.setVisibility(View.GONE);
                                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutHelper, DummyFragment.newInstance(null, null)).commit();
                                        break;
                                    }

                                }
                            }
                        }
                    }
                }
            }

            return;

        } else if (activityBinding.frameLayoutHelper.getVisibility() == View.VISIBLE) {
            if (getSupportFragmentManager().getFragments() == null && getSupportFragmentManager().getFragments().size() == 0) {
                super.onBackPressed();
                return;
            } else {
                activityBinding.frameLayoutHelper.setVisibility(View.GONE);
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutHelper, DummyFragment.newInstance(null, null)).commit();
            }
            return;
        } else if (activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.getCurrentItem() != 0) {
            goToHome(0);
            return;
        } else {
            finish();
        }
    }


    private void updateDeviceDetails() {
        try {
            Intent intent = new Intent(activity(), HelperIntentService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_DEVICE_DETAILS);
            intent.putExtra(Constants.RECEIVER, new DeviceResultReceiver(new Handler()));
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void goToHome(int position) {

        if (position == 0) {
            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
            activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setCurrentItem(0, false);
            Mixpanel.clickTabs(1);
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("⬤");
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("");
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("");
        } else if (position == 1) {
            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().findItem(R.id.navigation_cart).setChecked(true);
            activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setCurrentItem(1, false);
            Mixpanel.clickTabs(2);
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("");
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("⬤");
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("");
        } else if (position == 2) {
            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().findItem(R.id.navigation_profile).setChecked(true);
            activityBinding.includedAppBarLayout.includedSubLayout.swipeDisableViewPager.setCurrentItem(2, false);
            Mixpanel.clickTabs(3);
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(0).setTitle("");
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(1).setTitle("");
//            activityBinding.includedAppBarLayout.includedSubLayout.navigation.getMenu().getItem(2).setTitle("⬤");
        }
    }

    public void restartApp(boolean isDark) {

        Utility.changeTheme(activity());
    }


    public void openUnitPage(UnitModel unitModel, CatalogueModel catalogueModel, SubjectModel subjectModel) {
        if (unitProviderFragment != null) {
            unitProviderFragment.releasePlayer();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.videoFrameLayout, unitProviderFragment = UnitProviderFragment.newInstance(unitModel, catalogueModel, subjectModel)).addToBackStack("UnitProviderFragment").commit();
        activityBinding.videoFrameLayout.setVisibility(View.VISIBLE);
    }

    public void openCatalogOverview(CatalogueModel catalogueModel) {
        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayoutHelper, CatalogueOverviewFragment.newInstance(catalogueModel, null)).addToBackStack("CatalogueOverviewFragment").commit();
            activityBinding.frameLayoutHelper.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void playSubTopicVideo(SubTopicModel subTopic, StudentTopicsModel studentTopicsModel) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof UnitProviderFragment) {
                UnitProviderFragment unitProviderFragment = (UnitProviderFragment) fragment;
                unitProviderFragment.playSubTopicVideo(subTopic, studentTopicsModel);
            }
        }
    }

    public void pausePlayVideo(boolean isPlay) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof UnitProviderFragment) {
                UnitProviderFragment unitProviderFragment = (UnitProviderFragment) fragment;
                unitProviderFragment.playPauseVideo(isPlay);
            }
        }
    }

    public void openQuizPage(String subjectId, String unitId, String topicId, QuizModel quiz) {

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof UnitProviderFragment) {
                UnitProviderFragment unitProviderFragment = (UnitProviderFragment) fragment;
                unitProviderFragment.openQuiz(quiz, subjectId, unitId, topicId);
            }
        }
    }

    public void removeFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (fragment instanceof UnitProviderFragment) {
            for (Fragment frag : getSupportFragmentManager().getFragments()) {
                if (frag instanceof UnitProviderFragment) {
                    fragmentTransaction.remove(frag);
                    activityBinding.videoFrameLayout.setVisibility(View.INVISIBLE);
                }
            }
        } else if (fragment instanceof CatalogueOverviewFragment) {
            fragmentTransaction.remove(fragment);
        }

        fragmentTransaction.commit();
    }


    public void changeMotion(boolean change) {

        for (Fragment frag : getSupportFragmentManager().getFragments()) {
            if (frag instanceof UnitProviderFragment) {
                ((UnitProviderFragment) frag).changeMotion(change);
                break;
            }
        }


    }


    private void reDirect(String redirectMethod) {

    }

    public void passTransitionType(boolean transitionType) {

        this.isTransitionExpand = transitionType;

    }

    public void openQuiz(QuizModel quiz, String subjectId, String unitId, String topicId) {

        // getChildFragmentManager().beginTransaction().replace(R.id.topFrameLayout, QuizOverViewFragment.newInstance(quiz, subjectId, unitId, topicId)).commit();
//        binding.controllerLayout.setVisibility(View.GONE);
//        binding.controllerLayout.setAlpha(0);
//        binding.videoProgressBar.setVisibility(View.GONE);
//        binding.videoProgressBar.setAlpha(0);
//        binding.topContainer.setVisibility(View.GONE);
//        binding.topContainer.setOnTouchListener((v, event) -> true);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST_CODE", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("UPDATE_STORE_COURSE") && resultCode == Activity.RESULT_OK) {
            goToHome(0);
            if (data.getBooleanExtra("OpenCatalogue", false)) {
                if (ShareDataManager.getInstance().getStoreOverviewData().getCatalogueModel() != null) {
                    CatalogueModel catalogueModel = ShareDataManager.getInstance().getStoreOverviewData().getCatalogueModel();

                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        if (fragment instanceof CatalogueFragment) {
                            CatalogueFragment catalogueFragment = (CatalogueFragment) fragment;
                            catalogueFragment.checkCatalogueId(catalogueModel);
                            break;
                        }
                    }
                }
                return;
            }
            showSnackBar("Payment is successful", 1);
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof CatalogueFragment) {
                    CatalogueFragment catalogueFragment = (CatalogueFragment) fragment;
                    catalogueFragment.refreshCatalogues();
                    break;
                }
            }

        } else if (requestCode == Utility.generateRequestCodes().get("UPDATE_UPDATE_CARDS") && resultCode == Activity.RESULT_OK) {
            if (data.getParcelableExtra("UpdateCard") != null) {
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    if (fragment instanceof CatalogueFragment) {
//                        CatalogueFragment catalogueFragment = (CatalogueFragment) fragment;
//                        catalogueFragment.checkCatalogueId(data.getParcelableExtra("UpdateCard"));
                        break;
                    }
                }
            }
        }
    }

    public void redirectToCatalogueOverview(CatalogueModel catalogueModel) {
        openCatalogOverview(catalogueModel);
    }

    public void openRatingPage(String catalogueId, String catalogueName
            , String subjectId, String subjectName, String unitName
            , String topicId, String topicName
            , String studentSubTopicId, String subTopiName, String subTopicId, RatingModel ratingModel) {

        RatingModel intentData = new RatingModel();

        if (ratingModel != null) {
            intentData = ratingModel;
            intentData.setUnitName(unitName);
            intentData.setSubTopicName(subTopiName);
        } else {
            intentData.setUnitName(unitName);
            intentData.setSubTopicName(subTopiName);
            intentData.setCatalogueId(catalogueId);
            intentData.setCatalogueName(catalogueName);
            intentData.setSubjectId(subjectId);
            intentData.setSubjectName(subjectName);
            intentData.setUnitName(unitName);
            intentData.setTopicId(topicId);
            intentData.setTopicName(topicName);
            intentData.setStudentSubTopicId(studentSubTopicId);
            intentData.setSubTopicId(subTopicId);
        }

//        BSDVideoRatingFragment.newInstance(intentData, null).show(getSupportFragmentManager(), "BSDVideoRatingFragment");

        BSDVideoRatingFragment bsdVideoRatingFragment =
                BSDVideoRatingFragment.newInstance(intentData, null);
        bsdVideoRatingFragment.show(getSupportFragmentManager(),
                BSDVideoRatingFragment.TAG);

    }

    public void openReportPage(String catalogueName, String subjectName, String unitName
            , String topicName
            , String subTopiName) {

        Report intentData = new Report();

        intentData.setCourse(catalogueName);
        intentData.setSubject(subjectName);
        intentData.setUnit(unitName);
        intentData.setTopic(topicName);
        intentData.setSubTopic(subTopiName);

        BSDVideoReportFragment bsdVideoReportFragment =
                BSDVideoReportFragment.newInstance(intentData, null);
        bsdVideoReportFragment.show(getSupportFragmentManager(),
                BSDVideoRatingFragment.TAG);

    }

    public void updateRating(RatingModel ratingModel) {

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof UnitProviderFragment) {
                for (Fragment fragment1 : fragment.getChildFragmentManager().getFragments())
                    if (fragment1 instanceof UnitOverViewFragment) {
                        UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment1;
                        unitOverViewFragment.updateRating(ratingModel);
                        break;
                    }
            }
        }
    }

    public void askExpert(String catalogueName, String subjectName, String unitName, String topicName, String subTopicName) {
        BSDAskDoubtFragment bsdAskDoubtFragment = BSDAskDoubtFragment.newInstance(catalogueName, subjectName, unitName, topicName, subTopicName, height);
        bsdAskDoubtFragment.setCancelable(false);
        bsdAskDoubtFragment.show(getSupportFragmentManager(), "BSDAskDoubtFragment");
    }

    public void updateNote(Note note, int position, String noteTitle) {
        BSDAddNoteFragment bsdAddNoteFragment = BSDAddNoteFragment.newInstance(note, position, height, noteTitle);
        bsdAddNoteFragment.setCancelable(false);
        bsdAddNoteFragment.show(getSupportFragmentManager(), "BSDAddNoteFragment");
    }


    public void requestDownload(BSDDownloadRequestFragment fragment) {
        fragment.show(getSupportFragmentManager(), "BSDDownloadRequestFragment");
    }


    public void openFilters(String catalogueName, String subjectId, String unitId, String topicId, INotesFilter iNotesFilter, List<SubjectInfo> subjectInfos, List<Note> noteList) {
        BSDApplyFilterFragment.newInstance(subjectId, unitId, topicId, iNotesFilter, subjectInfos, noteList, catalogueName)
                .show(getSupportFragmentManager(), BSDApplyFilterFragment.TAG);
    }


    public class DeviceResultReceiver extends ResultReceiver {
        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public DeviceResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            // Display the address string or an error message sent from the intent service.
            if (resultCode == Utility.generateRequestCodes().get("UPDATE_DEVICE_DETAILS")) {
                //showUpdateApplication(resultData.getBoolean("IsCancel", true));
            }
        }

    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onStop() {
        super.onStop();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

//    private void cacheLicense() {
//        try {
//            Intent intent = new Intent(activity(), CacheLicenseIntentService.class);
//            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_CACHE_PENDING_LICENSE);
//            startService(intent);
//        } catch (
//                Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void verifyDownloadLicense() {
        try {
            Intent intent = new Intent(activity(), LicenseExpiryIntentService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_LICENCE_VERIFY);
            startService(intent);
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
            if (!viewModel.isLoaded()) {
                viewModel.setLoaded(true);
                updateDeviceDetails();
                Mixpanel.installedApplications();
                if (HelperApplication.getInstance().getDownloadTracker().getAllDownloads() != null
                        && HelperApplication.getInstance().getDownloadTracker().getAllDownloads().size() > 0)
                    verifyDownloadLicense();
            }

        } catch (IllegalStateException e) {
            try {
                Field f = Activity.class.getDeclaredField("mCalled");
                f.setAccessible(true);
                f.set(this, true);
            } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException e1) {
            }
        }
    }
}
