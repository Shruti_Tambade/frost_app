package com.frost.leap.viewpager;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.frost.leap.components.catalogue.CatalogueFragment;
import com.frost.leap.components.profile.ProfileFragment;
import com.frost.leap.components.store.StoreFragment;

public class HomeViewPagerAdapter extends FragmentPagerAdapter {
    private int type = 1;
    private String storeCatalogueId, dashboardCatalogueId;

    public HomeViewPagerAdapter(FragmentManager fm, String storeCatalogueId, String dashboardCatalogueId) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.storeCatalogueId = storeCatalogueId;
        this.dashboardCatalogueId = dashboardCatalogueId;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (type == 1) {

            switch (position) {
                case 0:
                    fragment = CatalogueFragment.newInstance(dashboardCatalogueId, null);
                    break;
                case 1:
                    fragment = StoreFragment.newInstance(storeCatalogueId, null);
                    break;
                case 2:
                    fragment = ProfileFragment.newInstance(null, null);
                    break;
            }
            return fragment;
        } else {
            return fragment;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

}
