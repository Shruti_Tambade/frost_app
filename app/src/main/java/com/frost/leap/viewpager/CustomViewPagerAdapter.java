package com.frost.leap.viewpager;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.frost.leap.components.store_overview.CurriculumFragment;
import com.frost.leap.components.store_overview.OverviewFragment;
import com.frost.leap.components.subject.SubjectsFragment;
import com.frost.leap.components.subject.askexpert.AskExpertFragment;
import com.frost.leap.components.subject.notes.NotesFragment;
import com.frost.leap.components.subject.ots.OTSFragment;
import com.frost.leap.components.updates.ArticlesFragment;
import com.frost.leap.components.updates.UpdatesFragment;
import com.frost.leap.fragments.generic.DummyFragment;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao on 05-08-19.
 */

public class CustomViewPagerAdapter extends FragmentPagerAdapter {

    private List<String> titlesList;
    private int type;
    private Bundle bundle;
    private CatalogueModel catalogueModel;
    private ArrayList<String> activeInactiveCatalogueIDs = new ArrayList<>();
    private ArrayList<String> activeCatalogueIds = new ArrayList<>();


    public CustomViewPagerAdapter(FragmentManager fm, List<String> titlesList, int type, CatalogueModel catalogueModel) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.titlesList = titlesList;
        this.type = type;
        this.catalogueModel = catalogueModel;
    }

    public CustomViewPagerAdapter(FragmentManager fm, List<String> titlesList, ArrayList<String> activeInactiveCatalogueIDs, ArrayList<String> activeCatalogueIds, int type) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.titlesList = titlesList;
        this.type = type;
        this.activeInactiveCatalogueIDs = activeInactiveCatalogueIDs;
        this.activeCatalogueIds = activeCatalogueIds;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (type == 1) {
            switch (position) {
            }
            return fragment;
        } else if (type == 2) {
            switch (position) {
                case 0:
                    Constants.PACKAGE_CODE = catalogueModel.getPackageCode();
                    Constants.PACKAGE_GROUP_CODE = catalogueModel.getPackageGroupCode();
                    fragment = SubjectsFragment.newInstance(catalogueModel, null);
                    break;
                case 1:
                    fragment = NotesFragment.newInstance(catalogueModel, null);
                    break;
                case 2:
                    fragment = AskExpertFragment.newInstance(catalogueModel, null);
                    break;
                default:
                    Constants.PACKAGE_CODE = catalogueModel.getPackageCode();
                    Constants.PACKAGE_GROUP_CODE = catalogueModel.getPackageGroupCode();
                    fragment = OTSFragment.newInstance(catalogueModel.isOts(), catalogueModel.getStudentCatalogueId());
            }

            return fragment;
        } else if (type == 3) {
            switch (position) {
                case 0:
                    fragment = DummyFragment.newInstance(null, null);
                    break;
                default:
                    fragment = DummyFragment.newInstance(null, null);
                    break;
            }

            return fragment;
        } else if (type == 4) {
            switch (position) {
                case 0:
                    fragment = OverviewFragment.newInstance(null, null);
                    break;
                default:
                    fragment = CurriculumFragment.newInstance(null, null);
                    break;
            }

            return fragment;
        } else if (type == 5) {
            switch (position) {
                case 0:
                    fragment = UpdatesFragment.newInstance(activeInactiveCatalogueIDs, activeCatalogueIds);
                    break;
                default:
                    fragment = ArticlesFragment.newInstance(null, null);
                    break;
            }

            return fragment;
        } else {
            return fragment;
        }
    }

    @Override
    public int getCount() {
        return titlesList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titlesList.get(position);
    }


    public void updateCatalogueOverview(CatalogueModel catalogueModel, List<String> titlesList, int type) {
        this.catalogueModel = catalogueModel;
        this.titlesList = titlesList;
        this.type = type;
        notifyDataSetChanged();
    }
}
