package com.frost.leap.components.profile.billing;


import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentBillingHistoryBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.profile.InvoiceViewModel;

import java.util.Arrays;

import apprepos.user.model.Invoice;
import supporters.constants.Constants;
import supporters.constants.RController;

/**
 * A simple {@link } subclass.
 * Use the {@link BillingHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BillingHistoryFragment extends BaseFragment implements IInvoiceAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentBillingHistoryBinding binding;
    private InvoiceViewModel viewModel;


    public BillingHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BillingHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BillingHistoryFragment newInstance(String param1, String param2) {
        BillingHistoryFragment fragment = new BillingHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(InvoiceViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_billing_history;
    }

    @Override
    public void setUp() {
        binding = (FragmentBillingHistoryBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchInvoiceHistory();
        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case InvoiceViewModel.NetworkTags.INVOICE_HISTORY:
                    updateUiForInvoices(networkCall);
                    break;
            }
        });

        viewModel.updateInvoices().observe(this, invoices -> {
            binding.recyclerView.setAdapter(new InvoiceAdapter(invoices, RController.DONE, activity(), this));
        });


        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value,viewModel);
        });

        viewModel.fetchInvoiceHistory();

    }

    private void updateUiForInvoices(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;
            case IN_PROCESS:
                binding.recyclerView.setAdapter(new InvoiceAdapter(Arrays.asList(null, null), RController.LOADING, activity(), this));
                break;
            case NO_DATA:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), "Your billing history is empty", R.drawable.no_data_available, 1));
                break;
            case FAIL:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void downloadInvoice(Invoice invoice, int position) {

        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.INVOICE_DETAILS);
        intent.putExtra("Invoice", invoice);
        startActivity(intent);

    }

    @Override
    public void viewInvoice(Invoice invoice, int position) {
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.INVOICE_DETAILS);
        intent.putExtra("Invoice", invoice);
        startActivity(intent);
    }
}
