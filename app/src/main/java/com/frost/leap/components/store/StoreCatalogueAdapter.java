package com.frost.leap.components.store;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.frost.leap.R;
import com.frost.leap.databinding.ItemStoreCatalogBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 9/30/2019.
 */
public class StoreCatalogueAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<CatalogueModel> catalogueModelList;
    private LinearLayout.LayoutParams imageParams;
    private RController rController;
    private IStoreAdapter iStoreAdapter;
    private String subCategoryName;

    public StoreCatalogueAdapter(IStoreAdapter iStoreAdapter, Context context, List<CatalogueModel> catalogueModelList, RController rController, String subCategoryName) {
        this.context = context;
        this.catalogueModelList = catalogueModelList;
        this.rController = rController;
        this.iStoreAdapter = iStoreAdapter;
        this.subCategoryName = subCategoryName;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new StoreCatalogItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_store_catalog, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof StoreCatalogItemViewHolder) {
            StoreCatalogItemViewHolder storeCatalogItemViewHolder = (StoreCatalogItemViewHolder) holder;


//
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(
                    Utility.toSdp(R.dimen._20sdp),
                    Utility.toSdp(R.dimen._3sdp),
                    Utility.toSdp(R.dimen._20sdp),
                    position == catalogueModelList.size() - 1 ? Utility.toSdp(R.dimen._15sdp) : 0);
            storeCatalogItemViewHolder.itemView.setLayoutParams(params);

            storeCatalogItemViewHolder.bindData(position);

            storeCatalogItemViewHolder.binding.cardView.setOnClickListener(v -> {
                iStoreAdapter.clickToCatalogueOverview(catalogueModelList.get(position), position, subCategoryName);
            });
        }
    }

    @Override
    public int getItemCount() {
        return catalogueModelList.size();
    }

    public class StoreCatalogItemViewHolder extends RecyclerView.ViewHolder {

        private ItemStoreCatalogBinding binding;

        public StoreCatalogItemViewHolder(ItemStoreCatalogBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position) {

            try {
                String[] catalogueColor;
                if (catalogueModelList.get(position).getCatalogueImageColor() != null && !catalogueModelList.get(position).getCatalogueImageColor().isEmpty()) {
                    catalogueColor = catalogueModelList.get(position).getCatalogueImageColor().split("#");
                    Logger.d("CATALOGUE_COLOR", catalogueModelList.get(position).getCatalogueImageColor().split("#")[1]);
                    binding.imgCover.setColorFilter(Color.parseColor("#" + catalogueColor[1]));
                } else {
                    binding.imgCover.setColorFilter(Color.parseColor("#c9d6e8"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (catalogueModelList.get(position).getCatalogueName() != null && !catalogueModelList.get(position).getCatalogueName().isEmpty()) {
                binding.tvCatalogName.setText(catalogueModelList.get(position).getCatalogueName());
            }

            if (catalogueModelList.get(position).getCatalogueImage() != null && !catalogueModelList.get(position).getCatalogueImage().isEmpty())
                Glide.with(context)
                        .load(catalogueModelList.get(position).getCatalogueImage())
                        .circleCrop()
                        .into(binding.ivCatalogPhoto);

//            if (catalogueModelList.get(position).getTotalDurationInHours() != null)
//                binding.tvTotalVideos.setText(String.valueOf(catalogueModelList.get(position).getTotalDurationInHours()) + " Hours");

        }
    }
}
