package com.frost.leap.components.profile.settings;

import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.viewmodels.models.Message;

import apprepos.user.model.User;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 13-09-2019.
 * <p>
 * Frost
 */
public interface IProfileSettingsAdapter {
     void onProfileSettingsClick(MenuItem menuItem, int position);

     void onChangeProfilePic();

     void updateUserDetails(User user, int position);

     void sendMessage(Message message);

     void changePassword(int position, String oldPassword, String password, String confirmPassword);

     void requestUpdateMobileNumber(int position, String mobileNumber, String password);

     void verifyOtp(int position, String mobileNumber, String otp);

     void gotoPrivacyPolicies();
}
