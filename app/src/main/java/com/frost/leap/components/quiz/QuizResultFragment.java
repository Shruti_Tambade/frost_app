package com.frost.leap.components.quiz;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;

import apprepos.quiz.model.QuizQuestionsModel;

import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentQuizResultBinding;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;

import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link QuizResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizResultFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String subjectId, unitId, topicId;

    private FragmentQuizResultBinding binding;
    private Typeface tfLight;
    private QuizQuestionsModel quizQuestionsModel;

    public QuizResultFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static QuizResultFragment newInstance(QuizQuestionsModel quizQuestionsModel, String subjectId, String unitId, String topicId) {
        QuizResultFragment fragment = new QuizResultFragment();
        Bundle args = new Bundle();
        args.putParcelable("QuizQuestionsModel", quizQuestionsModel);
        args.putString("SubjectId", subjectId);
        args.putString("UnitId", unitId);
        args.putString("TopicId", topicId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subjectId = getArguments().getString("SubjectId");
            unitId = getArguments().getString("UnitId");
            topicId = getArguments().getString("TopicId");
            quizQuestionsModel = getArguments().getParcelable("QuizQuestionsModel");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_quiz_result;
    }

    @Override
    public void setUp() {

        binding = (FragmentQuizResultBinding) getBaseDataBinding();
        binding.tvQuizType.setText("QuizConstants - " + quizQuestionsModel.getQuizName());
        setUpSnackBarView(binding.constraintLayout);
        tfLight = Utility.getTypeface(18, activity());
        setUpPieChart();
    }

    private void setUpPieChart() {


//        binding.piechart.setMinimumHeight((Utility.getScreenWidth(activity()) * 70) / 100);
        binding.rlLayout.setMinimumHeight(Utility.getScreenWidth(activity()));

        /*
         * First add Entry values in array
         * */
        ArrayList<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<Integer>();


        if (quizQuestionsModel.getQuizReport().getNoOfCorrectAnswers() != 0) {
            colors.add(ContextCompat.getColor(activity(), R.color.correct_answer));
            entries.add(new PieEntry(quizQuestionsModel.getQuizReport().getNoOfCorrectAnswers(), "Correct", ContextCompat.getDrawable(activity(), R.drawable.circle_white), 1));
        }
        if (quizQuestionsModel.getQuizReport().getNoOfWrongAnswers() - (quizQuestionsModel.getQuizReport().getNoOfQuestions() - quizQuestionsModel.getQuizReport().getNoOfQuestionsAttempted()) != 0) {
            colors.add(ContextCompat.getColor(activity(), R.color.wrong_answer));
            entries.add(new PieEntry(quizQuestionsModel.getQuizReport().getNoOfWrongAnswers() - (quizQuestionsModel.getQuizReport().getNoOfQuestions() - quizQuestionsModel.getQuizReport().getNoOfQuestionsAttempted()), "Wrong", ContextCompat.getDrawable(activity(), R.drawable.circle_white), 0));
        }
        if (quizQuestionsModel.getQuizReport().getNoOfQuestions() - quizQuestionsModel.getQuizReport().getNoOfQuestionsAttempted() != 0) {
            colors.add(ContextCompat.getColor(activity(), R.color.un_answered));
            entries.add(new PieEntry(quizQuestionsModel.getQuizReport().getNoOfQuestions() - quizQuestionsModel.getQuizReport().getNoOfQuestionsAttempted(), "Un Attempt", ContextCompat.getDrawable(activity(), R.drawable.circle_white), 2));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");
//        dataSet.setSliceSpace(0f);


        dataSet.setDrawIcons(false);
        dataSet.setColors(colors);
        dataSet.setValueTextSize(15f);
        dataSet.setDrawIcons(true);

        // enable rotation of the chart by touch
        binding.piechart.setRotationEnabled(false);
        binding.piechart.getDescription().setEnabled(false);
        binding.piechart.setCenterTextTypeface(tfLight);
        //
        binding.piechart.setCenterText(generateCenterSpannableText());
        // enable touch listner of the chart
        binding.piechart.setHighlightPerTapEnabled(false);
//        shows all DataSet labels and colors outside of the chart
        binding.piechart.getLegend().setEnabled(false);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(binding.piechart));
        data.setValueTextSize(15f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(tfLight);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                binding.piechart.setData(data);
                binding.piechart.highlightValue(0, 0, false);
            }
        }, 100);

        binding.tvReviewQuiz.setOnClickListener(v -> {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUIZ_REVIEW);
            intent.putExtra("StudentQuizId", quizQuestionsModel.getStudentQuizId());
            startActivity(intent);
        });

        binding.tvRetakeQuiz.setOnClickListener(v -> {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUIZ_START);
            intent.putExtra("QuizQuestionsModel", quizQuestionsModel);
            intent.putExtra("SubjectId", subjectId);
            intent.putExtra("UnitId", unitId);
            intent.putExtra("TopicId", topicId);
            startActivity(intent);
            activity().finish();
        });

        binding.tvContinueQuiz.setOnClickListener(v -> {
            activity().finish();
        });

    }

    @Override
    public void changeTitle(String title) {

    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString(Utility.roundTwoDecimals(quizQuestionsModel.getQuizReport().getScorePercentage()) + " %\nNot bad!");
        s.setSpan(new RelativeSizeSpan(2.3f), 0, s.length(), 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(activity().getResources().getColor(R.color.blue)), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(0.5f), 4, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(activity().getResources().getColor(R.color.blue)), 0, s.length(), 0);
        return s;
    }
}
