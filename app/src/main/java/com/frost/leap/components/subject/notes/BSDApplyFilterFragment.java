package com.frost.leap.components.subject.notes;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.frost.leap.R;
import com.frost.leap.databinding.FragmentBsdApplyFilterBinding;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.subjects.ApplyFilterViewModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonObject;

import java.util.List;

import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 06-01-2020.
 * <p>
 * FROST
 */
public class BSDApplyFilterFragment extends BottomSheetDialogFragment implements IFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private FragmentBsdApplyFilterBinding binding;
    private String subjectId, unitId, topicId, catalogueName;
    private String oldSubjectId, oldUnitId, oldTopicId;
    private INotesFilter iNotesFilter;
    private List<SubjectInfo> subjectInfoList;
    private String value = "All Subject Notes";
    public List<String> subjects, units, topics;
    private List<Note> noteList;
    private ApplyFilterViewModel viewModel;
    private int subjectPosition, unitPosition, topicPosition;


    public static final String TAG = "BSDApplyFilterFragment";


    public BSDApplyFilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p>
     * Parameter 1.
     *
     * @return A new instance of fragment BSDAddNoteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BSDApplyFilterFragment newInstance() {
        BSDApplyFilterFragment fragment = new BSDApplyFilterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static BSDApplyFilterFragment newInstance(String subjectId, String unitId, String topicId,
                                                     INotesFilter iNotesFilter,
                                                     List<SubjectInfo> subjectInfos,
                                                     List<Note> noteList, String catalogueName) {
        BSDApplyFilterFragment fragment = new BSDApplyFilterFragment();
        Bundle args = new Bundle();
        args.putString("SubjectId", subjectId);
        args.putString("UnitId", unitId);
        args.putString("TopicId", topicId);
        args.putString("CatalogueName", catalogueName);
        fragment.setArguments(args);

        fragment.iNotesFilter = iNotesFilter;
        fragment.subjectInfoList = subjectInfos;
        fragment.noteList = noteList;

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
        viewModel = new ViewModelProvider(this).get(ApplyFilterViewModel.class);
        if (getArguments() != null) {
            subjectId = getArguments().getString("SubjectId");
            unitId = getArguments().getString("UnitId");
            topicId = getArguments().getString("TopicId");
            catalogueName = getArguments().getString("CatalogueName");
            oldSubjectId = subjectId;
            oldUnitId = unitId;
            oldTopicId = topicId;
        }
//        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bsd_apply_filter, container, false);

        setUp();
        return binding.getRoot();
    }

    private void setUp() {

        activity().overridePendingTransition(R.anim.slide_up, R.anim.slide_down);

        Logger.d("width", "" + Utility.getScreenWidth(activity()));

//        int spinnerDropDownWIdth = ((Utility.pxToDp(Utility.getScreenWidth(activity()))) - (Utility.pxToDp(Utility.dpSize(activity(), 20))));
//        binding.subjectSpinner.setDropDownWidth(Utility.dpToPx(spinnerDropDownWIdth));
//        binding.unitSpinner.setDropDownWidth(Utility.dpToPx(spinnerDropDownWIdth));
//        binding.topicSpinner.setDropDownWidth(Utility.dpToPx(spinnerDropDownWIdth));

        binding.progressBar.setVisibility(View.GONE);
        binding.rlSubjects.setVisibility(View.GONE);
        binding.rlUnits.setVisibility(View.GONE);
        binding.rlTopics.setVisibility(View.GONE);
        binding.tvApply.setVisibility(View.GONE);

        if (subjectId == null) {
            binding.tvCancel.setVisibility(View.GONE);
        }

        binding.tvCancel.setOnClickListener(v -> {
            iNotesFilter.clearFilters();
            dismiss();
        });


        binding.tvApply.setOnClickListener(v -> {
            iNotesFilter.applyFilters(subjectId, unitId, topicId, value);
            Mixpanel.notesFilter(catalogueName);
            dismiss();
        });


        viewModel.showLoader().observe(this, loader -> {
            binding.progressBar.setVisibility(loader ? View.VISIBLE : View.GONE);
        });


        viewModel.updateSubjects().observe(this, subjects -> {
            this.subjects = subjects;
            this.subjects.add(0, "Choose subject");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(activity(),
                    R.layout.spinner_item, subjects);
            adapter.setDropDownViewResource(R.layout.dropdown_item);
            binding.subjectSpinner.setAdapter(adapter);
            binding.rlSubjects.setVisibility(View.VISIBLE);

            binding.subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    subjectPosition = position;
                    ((TextView) view).setTextColor(ContextCompat.getColor(activity(), R.color.normal_text_color)); //Change selected text color
                    binding.tvApply.setVisibility(View.GONE);
                    if (subjectPosition != 0) {
                        binding.tvApply.setVisibility(View.VISIBLE);
                        value = subjects.get(subjectPosition);
                        viewModel.getUnits(noteList, position - 1);
                        subjectId = viewModel.sIds.get(position - 1);
                        unitId = null;
                        topicId = null;

                    } else {

                        binding.rlUnits.setVisibility(View.GONE);
                        binding.rlTopics.setVisibility(View.GONE);
                        subjectId = null;
                        unitId = null;
                        topicId = null;

                        if (oldSubjectId != null) {
                            for (int i = 0; i < viewModel.sIds.size(); i++) {
                                if (viewModel.sIds.get(i).equals(oldSubjectId)) {
                                    oldSubjectId = null;
                                    binding.subjectSpinner.setSelection(i + 1);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        });

        viewModel.updateUnits().observe(this, units -> {
            this.units = units;
            this.units.add(0, "Choose Unit");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(activity(),
                    R.layout.spinner_item, units);
            adapter.setDropDownViewResource(R.layout.dropdown_item);
            binding.unitSpinner.setAdapter(adapter);
            binding.rlUnits.setVisibility(View.VISIBLE);
            binding.unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    unitPosition = position;
                    ((TextView) view).setTextColor(ContextCompat.getColor(activity(), R.color.normal_text_color)); //Change selected text color

                    if (unitPosition != 0) {
                        value = subjects.get(subjectPosition) + " > " + units.get(unitPosition);
                        binding.tvApply.setVisibility(View.VISIBLE);
                        unitId = viewModel.uIds.get(position - 1);
                        viewModel.getTopics(noteList, subjectPosition - 1, unitPosition - 1);
                        topicId = null;
                    } else {
                        unitId = null;
                        topicId = null;
                        binding.rlTopics.setVisibility(View.GONE);

                        if (oldUnitId != null) {
                            for (int i = 0; i < viewModel.uIds.size(); i++) {
                                if (viewModel.uIds.get(i).equals(oldUnitId)) {
                                    oldUnitId = null;
                                    binding.unitSpinner.setSelection(i + 1);
                                }
                            }
                        }
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });

        viewModel.updateTopics().observe(this, topics -> {
            this.topics = topics;
            this.topics.add(0, "Choose Topic");

            ArrayAdapter<String> adapter = new ArrayAdapter<>(activity(),
                    R.layout.spinner_item, topics);
            adapter.setDropDownViewResource(R.layout.dropdown_item);
            binding.topicSpinner.setAdapter(adapter);
            binding.rlTopics.setVisibility(View.VISIBLE);
            binding.topicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    topicPosition = position;
                    ((TextView) view).setTextColor(ContextCompat.getColor(activity(), R.color.normal_text_color)); //Change selected text color

                    if (topicPosition != 0) {
                        value = subjects.get(subjectPosition) + " > " + units.get(unitPosition) + " > " + topics.get(topicPosition);
                        binding.tvApply.setVisibility(View.VISIBLE);

                        topicId = viewModel.tIds.get(position - 1);
                    } else {
                        topicId = null;

                        if (oldTopicId != null) {
                            for (int i = 0; i < viewModel.tIds.size(); i++) {
                                if (viewModel.tIds.get(i).equals(oldTopicId)) {
                                    oldTopicId = null;
                                    binding.topicSpinner.setSelection(i + 1);
                                }
                            }
                        }
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        });

        viewModel.getSubjects(noteList, subjectInfoList);


    }


    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(activity(), binding.coordinatorLayout, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return getActivity();
    }


//    @Override
//    public int getTheme() {
//        return R.style.BaseBottomSheetDialog;
//    }
//
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
//        if (dialog == null)
//            return super.onCreateDialog(savedInstanceState);
//
//        dialog.setOnShowListener(bottomSheetDialog -> {
//            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
//            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
//            // Right here!
//            BottomSheetBehavior.from(bottomSheet)
//                    .setState(BottomSheetBehavior.STATE_EXPANDED);
//        });
//
//        return dialog;
//    }
}