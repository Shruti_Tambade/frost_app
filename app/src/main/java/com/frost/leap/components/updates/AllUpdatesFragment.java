package com.frost.leap.components.updates;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentAllUpdatesBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.updates.UpdateViewModel;
import com.google.android.material.datepicker.MaterialDatePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import apprepos.updates.model.UpdateModel;
import supporters.constants.Constants;
import supporters.constants.RController;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AllUpdatesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllUpdatesFragment extends BaseFragment implements IUpdateAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static int sizeOfDates = 0;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentAllUpdatesBinding binding;
    private CatalogueUpdatesAdapter catalogueUpdatesAdapter;

    private UpdateViewModel viewModel;

    private String startDate, endDate;
    private List<String> activeInactiveCatalogueIDs = new ArrayList<>();
    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat sdf;
    private String currentdate;

    private List<UpdateModel> list;
    private int currentPosition = -1;

    private Pair<Long, Long> seletedDate = null;

    public AllUpdatesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AllUpdatesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AllUpdatesFragment newInstance(ArrayList<String> param1, String param2) {
        AllUpdatesFragment fragment = new AllUpdatesFragment();

        fragment.activeInactiveCatalogueIDs = param1;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UpdateViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_all_updates;
    }

    @Override
    public void setUp() {

        binding = (FragmentAllUpdatesBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);


        binding.ivSelecteDate.setVisibility(View.GONE);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setNestedScrollingEnabled(false);

        openCalender();

        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchUpdatesList(activeInactiveCatalogueIDs, startDate, endDate);

        });

        binding.liveSessionCard.setOnClickListener(v -> {

            MaterialDatePicker.Builder<Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker().setTheme(R.style.ThemeOverlay_MaterialComponents_MaterialCalendar);

            if (seletedDate != null) {

                builder.setSelection(seletedDate);
            }

            MaterialDatePicker<Pair<Long, Long>> picker = builder.build();

            //FullScreenMode
//            MaterialDatePicker<Pair<Long, Long>> datePickerDialog = MaterialDatePicker.Builder.dateRangePicker().setTheme(R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen).build();
//
//            if (seletedDate != null)
//                builder.dateRangePicker().setSelection(seletedDate);

            picker.show(getParentFragmentManager(), picker.toString());

            picker.addOnPositiveButtonClickListener(selection -> {
                this.seletedDate = selection;
                simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                sdf = new SimpleDateFormat("MMM d");

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(selection.first);
                Date start = calendar.getTime();
                startDate = simpleDateFormat.format(start);
                currentdate = sdf.format(start);

                calendar.setTimeInMillis(selection.second);
                Date end = calendar.getTime();
                endDate = simpleDateFormat.format(end);
                currentdate = currentdate + " - " + sdf.format(end);


                binding.tvDate.setText("" + currentdate);
                viewModel.fetchUpdatesList(activeInactiveCatalogueIDs, startDate, endDate);
            });
        });
        binding.ivSelecteDate.setOnClickListener(v -> {

            MaterialDatePicker.Builder<Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker().setTheme(R.style.ThemeOverlay_MaterialComponents_MaterialCalendar);

            if (seletedDate != null) {
                builder.setSelection(seletedDate);
            }

            MaterialDatePicker<Pair<Long, Long>> picker = builder.build();

            //FullScreenMode
//            MaterialDatePicker<Pair<Long, Long>> datePickerDialog = MaterialDatePicker.Builder.dateRangePicker().setTheme(R.style.ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen).build();

//            if (seletedDate != null)
//                builder.dateRangePicker().setSelection(seletedDate);

            picker.show(getParentFragmentManager(), picker.toString());

            picker.addOnPositiveButtonClickListener(selection -> {

                this.seletedDate = selection;
                simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                sdf = new SimpleDateFormat("MMM d");

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(selection.first);
                Date start = calendar.getTime();
                startDate = simpleDateFormat.format(start);
                currentdate = sdf.format(start);

                calendar.setTimeInMillis(selection.second);
                Date end = calendar.getTime();
                endDate = simpleDateFormat.format(end);
                currentdate = currentdate + " - " + sdf.format(end);


                binding.tvDate.setText("" + currentdate);
                viewModel.fetchUpdatesList(activeInactiveCatalogueIDs, startDate, endDate);
            });


        });

        viewModel.getMessage().observe(this, message -> {
//            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {

            switch (networkCall.getNetworkTag()) {
                case UpdateViewModel.NetworkTags.UPDATES_LIST:
                    updateUIForUpdatesList(networkCall);
                    break;
            }

        });

        viewModel.getUpdatesList().observe(this, list -> {
            if (list != null) {
                this.list = list;
                binding.recyclerView.setAdapter(catalogueUpdatesAdapter = new CatalogueUpdatesAdapter(activity(), this, list, activeInactiveCatalogueIDs, true, RController.DONE));
            }
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        ShareDataManager.getInstance().getAllUpdatesLiveData().observe(this, updateModel -> {

            if (updateModel != null && list != null && currentPosition != -1
                    && list.size() > currentPosition && catalogueUpdatesAdapter != null) {
                if (updateModel.getUpdateId().equals(list.get(currentPosition).getUpdateId())) {
                    list.get(currentPosition).setChoiceType(updateModel.getChoiceType());
                    catalogueUpdatesAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void updateUIForUpdatesList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), "Looks like your device isn't connected to the internet right now.", R.drawable.cloud_icon, 1, false));
                break;

            case IN_PROCESS:
                binding.recyclerView.setAdapter(catalogueUpdatesAdapter = new CatalogueUpdatesAdapter(activity(), this, Arrays.asList(null, null), activeInactiveCatalogueIDs, true, RController.LOADING));
                break;

            case SUCCESS:
                break;

            case DONE:
                break;

            case NO_DATA:
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), "No new updates to show you. You’re all caught up!", R.drawable.update_icon, 3, false));

                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 1, false));
                break;

            case SERVER_ERROR:
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 1, false));
                break;

            case UNAUTHORIZED:
                break;
        }
    }

    private void openCalender() {

        sdf = new SimpleDateFormat("MMM d");
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        currentdate = sdf.format(new Date());
        startDate = simpleDateFormat.format(new Date());
        endDate = simpleDateFormat.format(new Date());

        binding.tvDate.setText("" + currentdate);

        viewModel.fetchUpdatesList(activeInactiveCatalogueIDs, startDate, endDate);
        binding.ivSelecteDate.setVisibility(View.VISIBLE);

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void redirectToDoubtPage(UpdateModel updateModel, int position) {

        this.currentPosition = position;
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.DOUBT_CLEARING_PAGE);
        intent.putExtra("Update_Item", updateModel);
        intent.putExtra("TAG", 2);
        startActivity(intent);

    }

    @Override
    public void retryUpdateAPI() {
        if (!isNetworkAvailable()) {
            return;
        }
        viewModel.fetchUpdatesList(activeInactiveCatalogueIDs, startDate, endDate);
    }

    @Override
    public void redirectTOAllUpdates() {

    }

    @Override
    public void openPDF(String pdfFile, String title) {
//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, title);
//        intent.putExtra("Url", pdfFile);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(pdfFile), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(pdfFile), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdfFile));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }
}
