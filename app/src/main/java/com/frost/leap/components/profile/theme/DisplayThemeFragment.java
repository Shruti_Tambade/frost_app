package com.frost.leap.components.profile.theme;


import android.os.Bundle;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentDisplayThemeBinding;

import apprepos.user.UserRepositoryManager;
import supporters.constants.Constants;

/**
 * A simple {@link } subclass.
 * Use the {@link DisplayThemeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DisplayThemeFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentDisplayThemeBinding binding;
    private UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();


    public DisplayThemeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DisplayThemeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DisplayThemeFragment newInstance(String param1, String param2) {
        DisplayThemeFragment fragment = new DisplayThemeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_display_theme;
    }

    @Override
    public void setUp() {

        binding = (FragmentDisplayThemeBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        binding.swtDarkMode.setChecked(userRepositoryManager.appSharedPreferences.getBoolean(Constants.IS_DARK_MODE));

        binding.swtDarkMode.setOnCheckedChangeListener((buttonView, isChecked) -> {

            userRepositoryManager.appSharedPreferences.setBoolean(Constants.IS_DARK_MODE, isChecked);
            if (activity() instanceof HelperActivity) {
                ((HelperActivity) activity()).restartApp();
            }
        });

        binding.swtAutoDarkMode.setOnCheckedChangeListener((buttonView, isChecked) -> {

            userRepositoryManager.appSharedPreferences.setBoolean(Constants.AUTO_DARK_MODE, isChecked);


        });
    }

    @Override
    public void changeTitle(String title) {

    }
}
