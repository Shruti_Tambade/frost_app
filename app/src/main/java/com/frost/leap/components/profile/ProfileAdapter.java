package com.frost.leap.components.profile;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.databinding.ItemMenuProfileSettingBinding;
import com.frost.leap.databinding.ItemProfileHeaderBinding;

import org.w3c.dom.Text;

import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<MenuItem> list;
    private IProfileAdapter iProfileAdapter;
    private Context context;
    private User user;

    public ProfileAdapter(Context context, User user, List<MenuItem> list, IProfileAdapter iProfileAdapter, UserRepositoryManager repositoryManager) {
        this.list = list;
        this.user = user;
        this.context = context;
        this.iProfileAdapter = iProfileAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        if (type == 0) {
            return new HeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                    R.layout.item_profile_header, viewGroup, false));
        } else
            return new MenuItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                    R.layout.item_menu_profile_setting, viewGroup, false));
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? 0 : 1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.bindData();
            headerViewHolder.itemView.setOnClickListener(v -> {
                iProfileAdapter.onProfileClick();
            });
        } else if (viewHolder instanceof MenuItemViewHolder) {
            MenuItemViewHolder menuItemViewHolder = (MenuItemViewHolder) viewHolder;
            menuItemViewHolder.bindData(list.get(i), i);
            menuItemViewHolder.binding.llFooter.setOnClickListener(v -> {
                iProfileAdapter.onMenuItemClick(list.get(i), i);
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        private ItemProfileHeaderBinding binding;

        public HeaderViewHolder(ItemProfileHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {

            if (user != null && user.getProfileInfo() != null) {
                binding.tvUserName.setText(Utility.getCamelCase(user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName()));
                if (TextUtils.isEmpty(user.getProfileInfo().getImage()))
                {
                    binding.tvShortName.setVisibility(View.VISIBLE);
                    binding.tvShortName.setText(Utility.getShortName(user.getProfileInfo().getFirstName(),
                            user.getProfileInfo().getLastName()));
                } else {
                    binding.tvShortName.setVisibility(View.GONE);
                    binding.imgUser.setImageURI(Uri.parse(TextUtils.isEmpty(user.getProfileInfo().getImage()) ? Constants.AVATAR_IMAGE : user.getProfileInfo().getImage()));
                }
            }
            if (user != null && user.getCredentials() != null) {
                binding.tvEmail.setText(user.getCredentials().getEmail() != null ? user.getCredentials().getEmail().toLowerCase() : "");
            }

        }
    }


    public class MenuItemViewHolder extends RecyclerView.ViewHolder {
        private ItemMenuProfileSettingBinding binding;

        public MenuItemViewHolder(ItemMenuProfileSettingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(MenuItem menuItem, int position) {
            binding.llHeader.setVisibility(menuItem.getHeader() == null ? View.GONE : View.VISIBLE);
            binding.tvHeader.setText(menuItem.getHeader() == null ? "" : menuItem.getHeader());
            binding.tvTitle.setText(menuItem.getTitle());
            binding.tvDescription.setText(menuItem.getDescription());


            if (position == list.size() - 1) {
                binding.tvTitle.setTextColor(Color.parseColor("#e2452a"));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, Utility.dpSize(context, 120));
                binding.getRoot().setLayoutParams(params);
            } else {
                binding.tvTitle.setTextColor(Utility.getColorsFromAttrs(context, R.attr.text_color));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, Utility.dpSize(context, 0));
                binding.getRoot().setLayoutParams(params);
            }

        }
    }


    public void updateDetails(User user) {
        this.user = user;
        notifyItemChanged(0);
    }


}
