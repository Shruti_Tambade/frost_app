package com.frost.leap.components.profile.additional;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.databinding.ItemAddProofBinding;
import com.frost.leap.databinding.ItemChangeCurrentGoalBinding;
import com.frost.leap.databinding.ItemEditAddressBinding;
import com.frost.leap.databinding.ItemEditFatherNameBinding;
import com.frost.leap.databinding.ItemEmployeeStatusBinding;
import com.frost.leap.databinding.ItemHighQualificationBinding;
import com.frost.leap.databinding.ItemMenuProfileBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;
import com.frost.leap.viewmodels.models.Message;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.model.AdditionalDetails;
import apprepos.user.model.Address;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 30-12-2019.
 * <p>
 * FROST
 */
public class AdditionalSettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private AdditionalDetails additionalDetails;
    private List<MenuItem> list;
    private RController rController;
    private IAdditionalSettingsAdapter iAdditionalSettingsAdapter;
    private Context context;
    private int previous = -1;
    private int currentGoalIndex = -1;
    private Uri front, back;
    private String imageNameFront = null, imageNameBack = null, imageName = null;
    private boolean isFront = false;
    private boolean deliveryInCollege = false;

    public AdditionalSettingsAdapter(AdditionalDetails additionalDetails, List<MenuItem> list, RController rController, IAdditionalSettingsAdapter iAdditionalSettingsAdapter, Context context) {
        this.additionalDetails = additionalDetails;
        this.list = list;
        this.rController = rController;
        this.iAdditionalSettingsAdapter = iAdditionalSettingsAdapter;
        this.context = context;
        if (additionalDetails == null)
            return;
        if (additionalDetails.getProofFront() != null)
            this.front = Uri.parse(additionalDetails.getProofFront());

        if (additionalDetails.getProofBack() != null)
            this.back = Uri.parse(additionalDetails.getProofBack());

    }


    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? 0 : getExactType(position);
    }

    private int getExactType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skeleton_view, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else {
            if (viewType == 0) {
                return new AdditionalSettingsItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_menu_profile, viewGroup, false));
            } else {
                switch (viewType) {

                    case 1:
                        return new EditFatherNameViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_father_name, viewGroup, false));
                    case 2:
                        return new EditHighestQualificationViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_high_qualification, viewGroup, false));
                    case 3:
                        return new EditCollegeNameViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_father_name, viewGroup, false));
                    case 4:
                        return new EditEmployeeStatusViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_employee_status, viewGroup, false));
                    case 5:
                        return new ChangeCurrentGoalViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_change_current_goal, viewGroup, false));
                    case 6:
                        return new EditAddressViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_address, viewGroup, false));
                    case 7:
                        return new EditProofViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_add_proof, viewGroup, false));

                    default:
                        return new EditFatherNameViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_father_name, viewGroup, false));

                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof AdditionalSettingsItemViewHolder) {
            AdditionalSettingsItemViewHolder menuItemViewHolder = (AdditionalSettingsItemViewHolder) viewHolder;
            menuItemViewHolder.bindData(list.get(position), position);
            menuItemViewHolder.binding.llFooter.setOnClickListener(v -> {
                iAdditionalSettingsAdapter.onAdditionalSettingsClick(list.get(position), position);
            });
        } else if (viewHolder instanceof EditFatherNameViewHolder) {
            EditFatherNameViewHolder editFatherNameViewHolder = (EditFatherNameViewHolder) viewHolder;
            editFatherNameViewHolder.bindData();

            editFatherNameViewHolder.binding.tvSave.setOnClickListener(v -> {


                if (TextUtils.isEmpty(editFatherNameViewHolder.binding.etFatherName.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("Father name not be empty", 2));
                    return;
                }
                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setFatherName(editFatherNameViewHolder.binding.etFatherName.getText().toString());
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(additionalDetails.getFatherName(), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Father name is updated", 1));
                Mixpanel.updateFatherName(additionalDetails.getFatherName(), "SUCCESS");
            });
        } else if (viewHolder instanceof EditHighestQualificationViewHolder) {
            EditHighestQualificationViewHolder editHighestQualificationViewHolder = (EditHighestQualificationViewHolder) viewHolder;
            editHighestQualificationViewHolder.bindData();

            editHighestQualificationViewHolder.binding.llBtech.setOnClickListener(v -> {
                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setHighestQualification("B.Tech");
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(additionalDetails.getHighestQualification(), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Highest qualification is updated", 1));
                Mixpanel.updateHigherQualification(additionalDetails.getHighestQualification(), "SUCCESS");
            });

            editHighestQualificationViewHolder.binding.llMtech.setOnClickListener(v -> {
                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setHighestQualification("M.Tech");
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(additionalDetails.getHighestQualification(), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Highest qualification is updated", 1));
                Mixpanel.updateHigherQualification(additionalDetails.getHighestQualification(), "SUCCESS");
            });
        } else if (viewHolder instanceof EditCollegeNameViewHolder) {
            EditCollegeNameViewHolder editCollegeNameViewHolder = (EditCollegeNameViewHolder) viewHolder;
            editCollegeNameViewHolder.bindData();

            editCollegeNameViewHolder.binding.tvSave.setOnClickListener(v -> {

                if (TextUtils.isEmpty(editCollegeNameViewHolder.binding.etFatherName.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("College name not be empty", 2));
                    return;
                }
                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setCollegeName(editCollegeNameViewHolder.binding.etFatherName.getText().toString());
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(additionalDetails.getCollegeName(), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("College name is updated", 1));
                Mixpanel.updateUniversityName(additionalDetails.getCollegeName(), "SUCCESS");

            });
        } else if (viewHolder instanceof EditEmployeeStatusViewHolder) {
            EditEmployeeStatusViewHolder editEmployeeStatusViewHolder = (EditEmployeeStatusViewHolder) viewHolder;
            editEmployeeStatusViewHolder.bindData();

            editEmployeeStatusViewHolder.binding.llStudent.setOnClickListener(v -> {

                editEmployeeStatusViewHolder.binding.etCompanyName.setVisibility(View.GONE);
                editEmployeeStatusViewHolder.binding.tvSave.setVisibility(View.GONE);

                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setEmploymentStatus("STUDENT");
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(Utility.getCamelCase(additionalDetails.getEmploymentStatus()), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Employee status is updated", 1));
                Mixpanel.updateEmploymentStatus(additionalDetails.getEmploymentStatus(), "", "SUCCESS");
            });

            editEmployeeStatusViewHolder.binding.llEmployee.setOnClickListener(v -> {

                editEmployeeStatusViewHolder.binding.etCompanyName.setVisibility(View.VISIBLE);
                editEmployeeStatusViewHolder.binding.tvSave.setVisibility(View.VISIBLE);

                editEmployeeStatusViewHolder.binding.imgStudent.setImageResource(R.drawable.ic_radio_button_unchecked);
                editEmployeeStatusViewHolder.binding.imgUnEmployee.setImageResource(R.drawable.ic_radio_button_unchecked);
                editEmployeeStatusViewHolder.binding.imgEmployee.setImageResource(R.drawable.ic_radio_button_checked);


            });

            editEmployeeStatusViewHolder.binding.llUnEmployee.setOnClickListener(v -> {

                editEmployeeStatusViewHolder.binding.etCompanyName.setVisibility(View.GONE);
                editEmployeeStatusViewHolder.binding.tvSave.setVisibility(View.GONE);

                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setEmploymentStatus("UNEMPLOYED");
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(Utility.getCamelCase(additionalDetails.getEmploymentStatus()), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Employee status is updated", 1));
                Mixpanel.updateEmploymentStatus(additionalDetails.getEmploymentStatus(), "", "SUCCESS");
            });


            editEmployeeStatusViewHolder.binding.tvSave.setOnClickListener(v -> {

                if (TextUtils.isEmpty(editEmployeeStatusViewHolder.binding.etCompanyName.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("Company name not be empty", 2));
                    return;
                }
                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                additionalDetails.setEmploymentStatus("EMPLOYED");
                additionalDetails.setCompanyName(editEmployeeStatusViewHolder.binding.etCompanyName.getText().toString());
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(Utility.getCamelCase(additionalDetails.getEmploymentStatus()), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Employee status is updated", 1));
                Mixpanel.updateEmploymentStatus(additionalDetails.getEmploymentStatus(), additionalDetails.getCompanyName(), "SUCCESS");
            });

        } else if (viewHolder instanceof ChangeCurrentGoalViewHolder) {

            ChangeCurrentGoalViewHolder changeCurrentGoalViewHolder = (ChangeCurrentGoalViewHolder) viewHolder;
            changeCurrentGoalViewHolder.bindData();
            changeCurrentGoalViewHolder.binding.currentGoalSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int value, long id) {
                    currentGoalIndex = value;
                    ((TextView) view).setTextColor(ContextCompat.getColor(context, R.color.normal_text_color)); //Change selected text color

                    if (currentGoalIndex == 0)
                        return;

                    if (!Utility.isNetworkAvailable(context)) {
                        iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                        return;
                    }

                    additionalDetails.setCurrentGoal(Constants.CURRENT_GOALS.get(currentGoalIndex - 1));
                    iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                    updateValue(Utility.getCamelCase(additionalDetails.getCurrentGoal()), position - 1);
                    deleteSubItem(position - 1);
                    iAdditionalSettingsAdapter.sendMessage(new Message("Current goal is updated", 1));
                    Mixpanel.updateCurrentGoal(additionalDetails.getCurrentGoal(), "SUCCESS");

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else if (viewHolder instanceof EditAddressViewHolder) {
            EditAddressViewHolder editAddressViewHolder = (EditAddressViewHolder) viewHolder;
            editAddressViewHolder.bindData();

            editAddressViewHolder.binding.llYes.setOnClickListener(v -> {
                editAddressViewHolder.binding.imgYes.setImageResource(R.drawable.ic_radio_button_checked);
                editAddressViewHolder.binding.imgNo.setImageResource(R.drawable.ic_radio_button_unchecked);
                deliveryInCollege = true;

            });

            editAddressViewHolder.binding.llNo.setOnClickListener(v -> {
                editAddressViewHolder.binding.imgYes.setImageResource(R.drawable.ic_radio_button_unchecked);
                editAddressViewHolder.binding.imgNo.setImageResource(R.drawable.ic_radio_button_checked);
                deliveryInCollege = false;

            });


            editAddressViewHolder.binding.tvSave.setOnClickListener(v -> {
                if (TextUtils.isEmpty(editAddressViewHolder.binding.etAddress.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("Address should not be empty", 2));
                    return;
                }
                if (TextUtils.isEmpty(editAddressViewHolder.binding.etCity.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("City should not be empty", 2));
                    return;
                }
                if (TextUtils.isEmpty(editAddressViewHolder.binding.etState.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("State should not be empty", 2));
                    return;
                }
                if (TextUtils.isEmpty(editAddressViewHolder.binding.etPinCode.getText().toString())) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("Pincode should not be empty", 2));
                    return;
                }

                if (!Utility.isNetworkAvailable(context)) {
                    iAdditionalSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }


                Address address = new Address();
                address.setAddress(editAddressViewHolder.binding.etAddress.getText().toString());
                address.setCity(editAddressViewHolder.binding.etCity.getText().toString());
                address.setState(editAddressViewHolder.binding.etState.getText().toString());
                address.setPin(editAddressViewHolder.binding.etPinCode.getText().toString());


                additionalDetails.setAddress(address);
                additionalDetails.setDeliveryInCollege(deliveryInCollege);
                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                updateValue(address.getAddress() + " " + address.getCity() + " " + address.getState() + " " + address.getPin(), position - 1);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("Delievery address is updated", 1));
                Mixpanel.updateDeliveryAddress(deliveryInCollege);
            });

        } else if (viewHolder instanceof EditProofViewHolder) {
            EditProofViewHolder editProofViewHolder = (EditProofViewHolder) viewHolder;
            editProofViewHolder.bindData(position);

            editProofViewHolder.binding.tvEdit.setOnClickListener(v -> {
                iAdditionalSettingsAdapter.changeProofFrontSide();
            });


            editProofViewHolder.binding.tvPreview.setOnClickListener(v -> {
                iAdditionalSettingsAdapter.previewImage(front);
            });


            editProofViewHolder.binding.tvEditBack.setOnClickListener(v -> {
                iAdditionalSettingsAdapter.changeProofBackSide();
            });

            editProofViewHolder.binding.tvPreviewBack.setOnClickListener(v -> {
                iAdditionalSettingsAdapter.previewImage(back);
            });

            editProofViewHolder.binding.tvPrivacy.setOnClickListener(v -> {
                iAdditionalSettingsAdapter.gotoPrivacyPolicies();
            });


            editProofViewHolder.binding.tvSave.setOnClickListener(v ->
            {
                if (front == null) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("ID Proof front side is not selected", 2));
                    return;
                }
                if (back == null) {
                    iAdditionalSettingsAdapter.sendMessage(new Message("ID Proof back side is not selected", 2));
                    return;
                }


                iAdditionalSettingsAdapter.updateAdditionalDetails(additionalDetails, position);
                deleteSubItem(position - 1);
                iAdditionalSettingsAdapter.sendMessage(new Message("ID Proof is updated", 1));
                Mixpanel.updateIDProofUpdate();
            });
        }
    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING ? 1 : list.size();
    }


    public class AdditionalSettingsItemViewHolder extends RecyclerView.ViewHolder {
        private ItemMenuProfileBinding binding;

        public AdditionalSettingsItemViewHolder(ItemMenuProfileBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(MenuItem menuItem, int position) {
            binding.llHeader.setVisibility(menuItem.getHeader() == null ? View.GONE : View.VISIBLE);
            binding.tvHeader.setText(menuItem.getHeader() == null ? "" : menuItem.getHeader());
            binding.tvTitle.setText(menuItem.getTitle());
            binding.tvDescription.setText(menuItem.getDescription());
            binding.imgGo.setRotation(!menuItem.isOpen() ? 0 : 180);
            binding.imgGo.setRotation(menuItem.isExpand() ? binding.imgGo.getRotation() : -90);
            binding.viewBottom.setVisibility(menuItem.isOpen() ? View.GONE : View.VISIBLE);

            if (position == list.size() - 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, Utility.dpSize(context, 120));
                binding.getRoot().setLayoutParams(params);
            } else {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, Utility.dpSize(context, 0));
                binding.getRoot().setLayoutParams(params);
            }
        }
    }


    private class EditFatherNameViewHolder extends RecyclerView.ViewHolder {

        private ItemEditFatherNameBinding binding;

        public EditFatherNameViewHolder(@NonNull ItemEditFatherNameBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            if (additionalDetails == null)
                return;

            binding.etFatherName.setText(Utility.getCamelCase(additionalDetails.getFatherName()));
        }
    }


    private class EditHighestQualificationViewHolder extends RecyclerView.ViewHolder {
        private ItemHighQualificationBinding binding;

        public EditHighestQualificationViewHolder(ItemHighQualificationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            if (additionalDetails == null)
                return;

            if (additionalDetails.getHighestQualification() != null) {
                binding.imgBtech.setImageResource(additionalDetails.getHighestQualification().equalsIgnoreCase("B.Tech") ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
                binding.imgMtech.setImageResource(additionalDetails.getHighestQualification().equalsIgnoreCase("M.Tech") ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
            }
        }
    }


    private class EditEmployeeStatusViewHolder extends RecyclerView.ViewHolder {
        private ItemEmployeeStatusBinding binding;

        public EditEmployeeStatusViewHolder(ItemEmployeeStatusBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            if (additionalDetails == null)
                return;
            if (additionalDetails.getEmploymentStatus() != null) {
                binding.imgStudent.setImageResource(additionalDetails.getEmploymentStatus().equalsIgnoreCase("STUDENT") ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
                binding.imgUnEmployee.setImageResource(additionalDetails.getEmploymentStatus().equalsIgnoreCase("UNEMPLOYED") ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
                if (additionalDetails.getEmploymentStatus().equalsIgnoreCase("EMPLOYED")) {
                    binding.imgEmployee.setImageResource(R.drawable.ic_radio_button_checked);
                    binding.etCompanyName.setVisibility(View.VISIBLE);
                    binding.etCompanyName.setText(additionalDetails.getCompanyName());
                    binding.tvSave.setVisibility(View.VISIBLE);
                } else {
                    binding.imgEmployee.setImageResource(R.drawable.ic_radio_button_unchecked);
                    binding.etCompanyName.setVisibility(View.GONE);
                    binding.tvSave.setVisibility(View.GONE);
                }
            }

        }
    }


    private class EditCollegeNameViewHolder extends RecyclerView.ViewHolder {

        private ItemEditFatherNameBinding binding;

        public EditCollegeNameViewHolder(@NonNull ItemEditFatherNameBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.etFatherName.setHint("College Name");
        }

        public void bindData() {
            if (additionalDetails == null)
                return;

            binding.etFatherName.setText(Utility.getCamelCase(additionalDetails.getCollegeName()));
        }
    }


    private class ChangeCurrentGoalViewHolder extends RecyclerView.ViewHolder {
        private ItemChangeCurrentGoalBinding binding;

        public ChangeCurrentGoalViewHolder(@NonNull ItemChangeCurrentGoalBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            List<String> goals = new ArrayList<>();
            goals.add("Choose your goal");
            goals.addAll(Constants.CURRENT_GOALS);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    R.layout.spinner_item, goals);
            adapter.setDropDownViewResource(R.layout.dropdown_item);
            binding.currentGoalSpinner.setAdapter(adapter);
        }
    }


    private class EditAddressViewHolder extends RecyclerView.ViewHolder {
        private ItemEditAddressBinding binding;

        public EditAddressViewHolder(@NonNull ItemEditAddressBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            deliveryInCollege = additionalDetails.isDeliveryInCollege();

            binding.imgYes.setImageResource(additionalDetails.isDeliveryInCollege() ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
            binding.imgNo.setImageResource(!additionalDetails.isDeliveryInCollege() ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);

            if (additionalDetails.getAddress() == null) {
                return;
            }

            binding.etAddress.setText(additionalDetails.getAddress().getAddress());
            binding.etCity.setText(additionalDetails.getAddress().getCity());
            binding.etState.setText(additionalDetails.getAddress().getState());
            binding.etPinCode.setText(additionalDetails.getAddress().getPin());


        }
    }


    private class EditProofViewHolder extends RecyclerView.ViewHolder {
        private ItemAddProofBinding binding;

        public EditProofViewHolder(@NonNull ItemAddProofBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(int position) {

            if (front == null) {
                binding.tvEdit.setText("UPLOAD");
                binding.tvPreview.setVisibility(View.GONE);
            } else {
                binding.tvEdit.setText("EDIT");
                binding.tvPreview.setVisibility(View.VISIBLE);
            }


            if (back == null) {
                binding.tvEditBack.setText("UPLOAD");
                binding.tvPreviewBack.setVisibility(View.GONE);
            } else {
                binding.tvEditBack.setText("EDIT");
                binding.tvPreviewBack.setVisibility(View.VISIBLE);
            }

            if (imageName != null && !imageName.isEmpty()) {

                if (imageNameFront != null && !imageNameFront.isEmpty())
                    binding.tvFront.setText(imageNameFront + ".jpg");


                if (imageNameBack != null && !imageNameBack.isEmpty())
                    binding.tvBack.setText(imageNameBack + ".jpg");

            } else {
                if (front != null) {
                    if (additionalDetails.getProofFront().contains("---")) {
                        String[] separated = additionalDetails.getProofFront().split("---");
                        Logger.d("Image_Split_Front", separated[1]);

                        binding.tvFront.setText(separated[1]);
                    } else {

                        binding.tvFront.setText("Image_" + (position + 1) + ".jpg");
                    }

                    imageNameFront = binding.tvFront.getText().toString();
                }

                if (back != null) {
                    if (additionalDetails.getProofBack().contains("---")) {
                        String[] separated = additionalDetails.getProofBack().split("---");
                        Logger.d("Image_Split_Back", separated[1]);

                        binding.tvBack.setText(separated[1]);
                    } else {
                        binding.tvBack.setText("Image -" + (position + 2) + ".jpg");
                    }

                    imageNameBack = binding.tvBack.getText().toString();
                }
            }
        }
    }


    public void addSubItem(int position) {

        if (previous != -1) {
            list.remove(previous + 1);
            list.get(previous).setOpen(false);
            if (position > previous) {
                position = position - 1;
            }
            notifyItemRemoved(previous + 1);
            notifyItemChanged(previous);
        }
        list.get(position).setOpen(true);
        list.add(position + 1, null);
        previous = position;


        notifyItemInserted(position + 1);
        notifyItemChanged(position);
        notifyItemRangeChanged(0, list.size());
    }

    public void deleteSubItem(int position) {
        //iProfileSettingsAdapter.sendMessage(new Message("remove position "+position,2));
        list.remove(position + 1);
        list.get(position).setOpen(false);
        if (previous == position) {
            previous = -1;
        }
        notifyItemRemoved(position + 1);
        notifyItemChanged(position);
        notifyItemRangeChanged(0, list.size());
        //notifyDataSetChanged();

    }

    public void updateValue(String value, int position) {
        if (list.get(position) instanceof MenuItem) {
            list.get(position).setDescription(value);
        }
    }

    public void updateProof(Uri uri, boolean isFront, String imageName) {
        this.imageName = imageName;
        this.isFront = isFront;
        if (isFront) {
            this.front = uri;
            this.imageNameFront = imageName;
        } else {
            this.back = uri;
            this.imageNameBack = imageName;
        }
        notifyItemChanged(list.size() - 1);
    }
}
