package com.frost.leap.components.subject;


import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentCatalogueOverviewBinding;
import com.frost.leap.viewpager.CustomViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import java.math.BigDecimal;

import apprepos.catalogue.model.CatalogueModel;
import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link CatalogueOverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CatalogueOverviewFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "catalog";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private FragmentCatalogueOverviewBinding binding;
    private CatalogueModel catalogueModel = null;
    private CustomViewPagerAdapter customViewPagerAdapter;


    public CatalogueOverviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CatalogueOverviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CatalogueOverviewFragment newInstance(CatalogueModel param1, String param2) {
        CatalogueOverviewFragment fragment = new CatalogueOverviewFragment();
        Bundle args = new Bundle();
        fragment.catalogueModel = param1;
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int layout() {
        return R.layout.fragment_catalogue_overview;
    }


    @Override
    public void setUp() {
        binding = (FragmentCatalogueOverviewBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        if (catalogueModel == null) {
            activity().finish();
            return;
        }

        if (catalogueModel.isOts()) {
            binding.viewPager.setOffscreenPageLimit(Constants.CATALOGUE_OTS_CLAIM_CATEGORIES.size());
            binding.tabLayout.setupWithViewPager(binding.viewPager);
            binding.viewPager.setAdapter(customViewPagerAdapter = new CustomViewPagerAdapter(getChildFragmentManager(), Constants.CATALOGUE_OTS_CLAIM_CATEGORIES, 2, catalogueModel));
            Utility.changeTabsFont(binding.tabLayout);
        } else {
            binding.viewPager.setOffscreenPageLimit(Constants.CATALOGUE_CATEGORIES.size());
            binding.tabLayout.setupWithViewPager(binding.viewPager);
            binding.viewPager.setAdapter(customViewPagerAdapter = new CustomViewPagerAdapter(getChildFragmentManager(), Constants.CATALOGUE_CATEGORIES, 2, catalogueModel));
            Utility.changeTabsFont(binding.tabLayout);
        }


        if (catalogueModel != null) {
            Glide.with(activity())
                    .load(catalogueModel.getCatalogueImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.ivCatalogPhoto);

            if (catalogueModel.getTotalDurationInSce() != null)
                binding.tvCatalogueHours.setText(Utility.splitToComponentTimes(BigDecimal.valueOf(catalogueModel.getTotalDurationInSce())));


            if (catalogueModel.getCompletionPercentage() != null && catalogueModel.getCompletionPercentage() > 0) {

                binding.progressBar.setProgress(catalogueModel.getCompletionPercentage().intValue());
                binding.tvCataloguePercentage.setText(catalogueModel.getCompletionPercentage().intValue() + "%");
            }

            if (catalogueModel.getCatalogueName() != null && !catalogueModel.getCatalogueName().isEmpty())
                binding.tvCatalogName.setText(catalogueModel.getCatalogueName());
        }

        binding.ivClose.setOnClickListener(v -> {
            if (activity() instanceof MainActivity) {
                ((MainActivity) activity()).removeFragment(this);
                //MiniView Padding
                ((MainActivity) activity()).changeMotion(true);
            }
        });

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 1:
                        Mixpanel.notesTab(catalogueModel.getCatalogueName());
                        break;
                    case 2:
                        Mixpanel.otsTab(catalogueModel.getCatalogueName());
                        break;
                    case 3:
                        Mixpanel.askExpertTab(catalogueModel.getCatalogueName());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    @Override
    public void changeTitle(String title) {

    }


    public void updateOtsDetails() {
        catalogueModel.setOts(true);
        if (customViewPagerAdapter != null)
            customViewPagerAdapter.updateCatalogueOverview(catalogueModel, Constants.CATALOGUE_OTS_CLAIM_CATEGORIES, 2);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
