package com.frost.leap.components.quiz;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemQuizAnswerBinding;

import java.util.List;

import apprepos.quiz.model.OptionModel;
import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/3/2019.
 */
public class QuizAnswersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int lastCheckedPos = -1;
    private Context context;
    private List<OptionModel> optionModelList;
    private boolean isPreview = false;

    public QuizAnswersAdapter(Context context, List<OptionModel> optionModelList) {

        this.context = context;
        this.optionModelList = optionModelList;
    }

    public QuizAnswersAdapter(Context context, List<OptionModel> optionModelList, boolean isPreview) {

        this.context = context;
        this.optionModelList = optionModelList;
        this.isPreview = isPreview;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new QuizAnswersViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_quiz_answer, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof QuizAnswersViewHolder) {
            QuizAnswersViewHolder quizAnswersViewHolder = (QuizAnswersViewHolder) holder;

            quizAnswersViewHolder.itemView.setOnClickListener(v -> {


            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(
                    Utility.dpSize(context, 15),
                    Utility.dpSize(context, 15),
                    Utility.dpSize(context, 15),
                    Utility.dpSize(context, position == optionModelList.size() - 1 ? 15 : 0));

            quizAnswersViewHolder.itemView.setLayoutParams(params);
            quizAnswersViewHolder.bindData(position);

            quizAnswersViewHolder.itemView.setOnClickListener(v -> {

                if (isPreview) return;

                if (lastCheckedPos >= 0) {
                    optionModelList.get(lastCheckedPos).setTicked(false);
                    optionModelList.get(lastCheckedPos).isSelected = false;
                    optionModelList.get(position).isSelected = true;
                } else optionModelList.get(position).isSelected = true;

                lastCheckedPos = position;
                optionModelList.get(lastCheckedPos).setTicked(true);
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemCount() {
        return optionModelList.size();
    }

    private class QuizAnswersViewHolder extends RecyclerView.ViewHolder {

        private ItemQuizAnswerBinding binding;

        public QuizAnswersViewHolder(ItemQuizAnswerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(int position) {

            if (!isPreview) {
                if (optionModelList.get(position).isSelected) {
                    binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_dark_blue);
                    binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.white));
                    binding.ivImage.setImageResource(R.drawable.ic_check_white);
                    binding.tvIsCorrect.setVisibility(View.GONE);
                    binding.ivImage.setVisibility(View.VISIBLE);
                } else {
                    binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_blue);
                    binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.normal_text_color));
                    binding.tvIsCorrect.setVisibility(View.GONE);
                    binding.ivImage.setVisibility(View.GONE);
                }
                binding.tvAnswer.setText(Html.fromHtml(optionModelList.get(position).getOption()));

                return;
            }

            if (optionModelList.get(position).isTicked()) {
                if (optionModelList.get(position).isCorrect()) {
                    binding.tvIsCorrect.setText("Your Answer");
                    binding.tvIsCorrect.setVisibility(View.VISIBLE);
                    binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.white));
                    binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_green);
                    binding.ivImage.setImageResource(R.drawable.ic_right_mark_icon);
                    binding.ivImage.setVisibility(View.VISIBLE);
                } else {
                    binding.tvIsCorrect.setText("Your Answer");
                    binding.tvIsCorrect.setVisibility(View.VISIBLE);
                    binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_red);
                    binding.ivImage.setImageResource(R.drawable.ic_cross_red_icon);
                    binding.ivImage.setVisibility(View.VISIBLE);
                }
                binding.tvAnswer.setText(Html.fromHtml(optionModelList.get(position).getOption()));

            } else {
                if (optionModelList.get(position).isCorrect()) {
                    binding.tvIsCorrect.setText("Correct Answer");
                    binding.tvIsCorrect.setVisibility(View.VISIBLE);
                    binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.white));
                    binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_green);
                    binding.ivImage.setImageResource(R.drawable.ic_right_mark_icon);
                    binding.ivImage.setVisibility(View.VISIBLE);
                } else {
                    binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_blue);
                    binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.normal_text_color));
                    binding.tvIsCorrect.setVisibility(View.GONE);
                    binding.ivImage.setVisibility(View.GONE);
                }
                binding.tvAnswer.setText(Html.fromHtml(optionModelList.get(position).getOption()));

            }

//            if (position == 0) {
//                binding.tvAnswer.setText("Node.JS is a JavaScript based framework.");
//                binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_dark_blue);
//                binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.white));
//                binding.ivImage.setImageResource(R.drawable.ic_check_white);
//                binding.tvIsCorrect.setVisibility(View.GONE);
//                binding.ivImage.setVisibility(View.VISIBLE);
//
//            } else if (position == 1) {
//
//                binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_red);
//                binding.tvIsCorrect.setText("Your Answer");
//                binding.tvIsCorrect.setVisibility(View.VISIBLE);
//                binding.ivImage.setImageResource(R.drawable.ic_cross_red_icon);
//                binding.ivImage.setVisibility(View.VISIBLE);
//
//            } else if (position == 2) {
//
//                binding.tvAnswer.setText("None of above");
//                binding.constraintLayout.setBackgroundResource(R.drawable.quiz_border_corner_light_green);
//                binding.tvIsCorrect.setText("Correct Answer");
//                binding.tvIsCorrect.setTextColor(ContextCompat.getColor(context, R.color.white));
//                binding.tvAnswer.setTextColor(ContextCompat.getColor(context, R.color.white));
//                binding.tvIsCorrect.setVisibility(View.VISIBLE);
//                binding.ivImage.setImageResource(R.drawable.ic_right_mark_icon);
//                binding.ivImage.setVisibility(View.VISIBLE);
//
//
//            } else {
//
//                binding.ivImage.setVisibility(View.GONE);
//                binding.tvIsCorrect.setVisibility(View.GONE);
//            }
        }
    }
}
