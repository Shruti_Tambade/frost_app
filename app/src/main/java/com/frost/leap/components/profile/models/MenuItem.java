package com.frost.leap.components.profile.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class MenuItem implements Parcelable {
    private int id;
    private String title;
    private String description;
    private int image;
    private String header;
    private boolean isExpand = false;
    private boolean isOpen = false;

    private boolean isContact = false;
    private String contactMessage;
    private String issueType;
    private String issueTopic;
    private String phoneModel;
    private String androidVersion;

    public MenuItem() {
    }

    public MenuItem(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public MenuItem(int id, String title, String description, String header) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.header = header;
    }

    public MenuItem(int id, String title, String description, boolean isContact, String header, String contactMessage, String issueType, String issueTopic) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.header = header;
        this.isContact = isContact;
        this.contactMessage = contactMessage;
        this.issueType = issueType;
        this.issueTopic = issueTopic;
    }

//    public MenuItem(int id, String title, String description, boolean isContact, String header,String contactMessage) {
//        this.id = id;
//        this.title = title;
//        this.description = description;
//        this.header = header;
//        this.isContact = isContact;
//        this.contactMessage = contactMessage;
//    }

    public MenuItem(int id, String title, String description, String header, boolean isExpand) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.header = header;
        this.isExpand = isExpand;
    }

    protected MenuItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        image = in.readInt();
        header = in.readString();
        isExpand = in.readByte() != 0;
        isOpen = in.readByte() != 0;
        isContact = in.readByte() != 0;
        contactMessage = in.readString();
        issueType = in.readString();
        issueTopic = in.readString();
        phoneModel = in.readString();
        androidVersion = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(image);
        dest.writeString(header);
        dest.writeByte((byte) (isExpand ? 1 : 0));
        dest.writeByte((byte) (isOpen ? 1 : 0));
        dest.writeByte((byte) (isContact ? 1 : 0));
        dest.writeString(contactMessage);
        dest.writeString(issueType);
        dest.writeString(issueTopic);
        dest.writeString(phoneModel);
        dest.writeString(androidVersion);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MenuItem> CREATOR = new Creator<MenuItem>() {
        @Override
        public MenuItem createFromParcel(Parcel in) {
            return new MenuItem(in);
        }

        @Override
        public MenuItem[] newArray(int size) {
            return new MenuItem[size];
        }
    };

    public boolean isContact() {
        return isContact;
    }

    public void setContact(boolean contact) {
        isContact = contact;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public String getContactMessage() {
        return contactMessage;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public String getIssueTopic() {
        return issueTopic;
    }

    public void setIssueTopic(String issueTopic) {
        this.issueTopic = issueTopic;
    }

    public void setContactMessage(String contactMessage) {
        this.contactMessage = contactMessage;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }
}
