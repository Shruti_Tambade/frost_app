package com.frost.leap.components.profile;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentHelperBinding;

import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelperFragment extends BaseFragment implements IProfileAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentHelperBinding binding;
    private HelperAdapter helpAdapter;

    public HelperFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HelperFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelperFragment newInstance(String param1, String param2) {
        HelperFragment fragment = new HelperFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public int layout() {
        return R.layout.fragment_helper;
    }

    @Override
    public void setUp() {
        binding = (FragmentHelperBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));

        binding.recyclerView.getItemAnimator().setChangeDuration(0);
        binding.recyclerView.setAdapter(helpAdapter = new HelperAdapter(activity(), Utility.generateSupportItems(), this));
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onProfileClick() {

    }

    @Override
    public void onMenuItemClick(MenuItem menuItem, int position) {
        if (menuItem == null)
            return;

        Intent intent = new Intent(activity(), HelperActivity.class);
        switch (menuItem.getId()) {
            case 1:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Subsciption Related");
                startActivity(intent);
                break;
            case 2:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Online Test Series");
                startActivity(intent);
                break;
            case 3:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Study Material");
                startActivity(intent);
                break;
            case 4:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Device and Region Compatibility");
                startActivity(intent);
                break;
            case 5:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - General Issues");
                startActivity(intent);
                break;
            case 6:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Reporting Content Issues");
                startActivity(intent);
                break;
            case 7:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - New Content Requests");
                startActivity(intent);
                break;
            case 8:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Payment General Queries");
                startActivity(intent);
                break;
            case 9:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Payment Issues");
                startActivity(intent);
                break;
            case 10:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - General Queries");
                startActivity(intent);
                break;
            case 11:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Video Player and Streaming Related");
                startActivity(intent);
                break;
            case 12:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - Feature Requests");
                startActivity(intent);
                break;
            case 13:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Help - New Course Requests");
                startActivity(intent);
                break;
        }
    }
}
