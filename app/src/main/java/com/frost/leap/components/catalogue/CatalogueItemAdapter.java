package com.frost.leap.components.catalogue;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.databinding.ItemDashboardActivityBinding;
import com.frost.leap.databinding.ItemDashboardCatalogListBinding;
import com.frost.leap.databinding.ItemStoreHeaderBinding;
import com.frost.leap.databinding.ItemUpdateListBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.user.UserRepositoryManager;
import storage.AppSharedPreferences;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.customviews.views.LinearLayoutManagerWithSmoothScroller;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/10/2019.
 */
public class CatalogueItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<CatalogueModel> catalogueModelList;
    private RController rController;
    private ICatalogueAdapter iCatalogueAdapter;

    public CatalogueItemAdapter(ICatalogueAdapter iCatalogueAdapter, Context context, List<CatalogueModel> catalogueModelList, RController rController) {
        this.context = context;
        this.catalogueModelList = catalogueModelList;
        this.rController = rController;
        this.iCatalogueAdapter = iCatalogueAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skelton_catalogue, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else
//            return new CatalogueItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
//                    R.layout.item_dashboard_catalog_list, viewGroup, false));

            if (viewType == 0) {
                return new CatalogueHeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_store_header, viewGroup, false));
            } else
                return new CatalogueItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_dashboard_catalog_list, viewGroup, false));

//            else
//                return new CatalogueActivityViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
//                        R.layout.item_dashboard_activity, viewGroup, false));
//                return new CatalogueUpdateViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
//                        R.layout.item_update_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CatalogueItemViewHolder) {

            CatalogueItemViewHolder catalogueItemViewHolder = (CatalogueItemViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
//            params.setMargins(0, 0, 0, Utility.dpSize(context, position == catalogueModelList.size() ? 35 : 0));
            catalogueItemViewHolder.itemView.setLayoutParams(params);

            catalogueItemViewHolder.bindData(position);

        } else if (holder instanceof CatalogueHeaderViewHolder) {
            CatalogueHeaderViewHolder catalogueHeaderViewHolder = (CatalogueHeaderViewHolder) holder;

            catalogueHeaderViewHolder.bindData();

        } else if (holder instanceof CatalogueActivityViewHolder) {
            CatalogueActivityViewHolder catalogueActivityViewHolder = (CatalogueActivityViewHolder) holder;

            int width = (int) (Utility.getScreenWidth(context) * 0.57);
            int height = (int) (width * 2.2);

            Logger.d(" HEIGHT ", +height + " WIDTH " + width);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    height);
            catalogueActivityViewHolder.itemView.setLayoutParams(params);

            catalogueActivityViewHolder.bindData();
        } else if (holder instanceof CatalogueUpdateViewHolder) {
            CatalogueUpdateViewHolder catalogueUpdateViewHolder = (CatalogueUpdateViewHolder) holder;

            catalogueUpdateViewHolder.bindData(position);
        }
    }

//    @Override
//    public int getItemCount() {
//        return rController == RController.LOADING
//                ? 1
//                : 1;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING
                ? 2
                : 2;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class CatalogueItemViewHolder extends RecyclerView.ViewHolder {
        private ItemDashboardCatalogListBinding binding;

        public CatalogueItemViewHolder(ItemDashboardCatalogListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context,
                    LinearLayoutManager.HORIZONTAL, false));

        }

        public void bindData(int position) {

//            binding.imageView2.setVisibility(position - 1 == 0 ? View.VISIBLE : View.GONE);

            binding.recyclerView.setAdapter(new CatalogAdapter(context, catalogueModelList, iCatalogueAdapter));
        }
    }

    private class CatalogueActivityViewHolder extends RecyclerView.ViewHolder {

        private ItemDashboardActivityBinding binding;

        public CatalogueActivityViewHolder(ItemDashboardActivityBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public void bindData() {

            binding.chart.setViewPortOffsets(0, 0, 0, 0);
            binding.chart.setBackgroundColor(Color.parseColor(AppSharedPreferences.getInstance().getBoolean(Constants.IS_DARK_MODE) ? "#32333D" : "#FFFFFF"));

            // no description text
            binding.chart.getDescription().setEnabled(false);
            // enable touch gestures
            binding.chart.setTouchEnabled(true);
            // enable scaling and dragging
            binding.chart.setDragEnabled(true);
            binding.chart.setScaleEnabled(true);
            // if disabled, scaling can be done on x- and y-axis separately
            binding.chart.setPinchZoom(false);
            binding.chart.setDrawGridBackground(false);
            binding.chart.setMaxHighlightDistance(300);
            XAxis x = binding.chart.getXAxis();
            x.setEnabled(false);
            x.setTextColor(Color.BLACK);

            YAxis y = binding.chart.getAxisLeft();
            y.setLabelCount(4, false);
            y.setTextColor(Color.BLACK);
            y.setTextSize(0);
            y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
            y.setDrawGridLines(false);


            binding.chart.getAxisRight().setEnabled(false);

            CustomMarkerView mv = new CustomMarkerView(context, R.layout.marker_layout);
            mv.setChartView(binding.chart);
            binding.chart.setMarker(mv);

            setDrawDashLines(y);
            setDataPresent(20, 250, binding.chart);
            setDataPrevious(20, 150, binding.chart);
            binding.chart.animateXY(2000, 2000);
            // don't forget to refresh the drawing
            binding.chart.invalidate();
            binding.chart.setPinchZoom(false);
            binding.chart.setDoubleTapToZoomEnabled(false);
            //binding.chart.zoomToCenter(0,0);

            ScaleAnimation expandAnimation = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.0f);
            expandAnimation.setDuration(400);

            ScaleAnimation collapsAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.0f);
            collapsAnimation.setDuration(400);


            binding.dailyBtn.setOnClickListener(v -> {
                binding.dailyBtn.setText("Daily");
                binding.weeklyBtn.setText("W");
                binding.monthlyBtn.setText("M");
                binding.yearlyBtn.setText("Y");

                binding.dailyBtn.setBackgroundResource(R.drawable.red_border_radius_22);
                binding.weeklyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.monthlyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.yearlyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);

                binding.dailyBtn.setTextColor(ContextCompat.getColor(context, R.color.white));
                binding.weeklyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.monthlyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.yearlyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));

                //binding.dailyBtn.startAnimation(expandAnimation);
                TransitionManager.beginDelayedTransition(binding.rootLayout);

                binding.dailyBtn.setPadding(Utility.dpSize(context, 45), 0, Utility.dpSize(context, 45), 0);
                binding.weeklyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.monthlyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.yearlyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);

            });

            binding.weeklyBtn.setOnClickListener(v -> {
                binding.dailyBtn.setText("D");
                binding.weeklyBtn.setText("Weekly");
                binding.monthlyBtn.setText("M");
                binding.yearlyBtn.setText("Y");

                binding.dailyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.weeklyBtn.setBackgroundResource(R.drawable.red_border_radius_22);
                binding.monthlyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.yearlyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);

                binding.dailyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.weeklyBtn.setTextColor(ContextCompat.getColor(context, R.color.white));
                binding.monthlyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.yearlyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));

                //binding.weeklyBtn.startAnimation(expandAnimation);
                TransitionManager.beginDelayedTransition(binding.rootLayout);

                binding.dailyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.weeklyBtn.setPadding(Utility.dpSize(context, 45), 0, Utility.dpSize(context, 45), 0);
                binding.monthlyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.yearlyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
            });

            binding.monthlyBtn.setOnClickListener(v -> {
                binding.dailyBtn.setText("D");
                binding.weeklyBtn.setText("W");
                binding.monthlyBtn.setText("Monthly");
                binding.yearlyBtn.setText("Y");

                binding.dailyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.weeklyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.monthlyBtn.setBackgroundResource(R.drawable.red_border_radius_22);
                binding.yearlyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);

                binding.dailyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.weeklyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.monthlyBtn.setTextColor(ContextCompat.getColor(context, R.color.white));
                binding.yearlyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));

                //binding.monthlyBtn.startAnimation(expandAnimation);
                TransitionManager.beginDelayedTransition(binding.rootLayout);

                binding.dailyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.weeklyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.monthlyBtn.setPadding(Utility.dpSize(context, 45), 0, Utility.dpSize(context, 45), 0);
                binding.yearlyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
            });

            binding.yearlyBtn.setOnClickListener(v -> {
                binding.dailyBtn.setText("D");
                binding.weeklyBtn.setText("W");
                binding.monthlyBtn.setText("M");
                binding.yearlyBtn.setText("Yearly");

                binding.dailyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.weeklyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.monthlyBtn.setBackgroundResource(R.drawable.light_gray_corner_radius_22);
                binding.yearlyBtn.setBackgroundResource(R.drawable.red_border_radius_22);

                binding.dailyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.weeklyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.monthlyBtn.setTextColor(ContextCompat.getColor(context, R.color.black));
                binding.yearlyBtn.setTextColor(ContextCompat.getColor(context, R.color.white));

                //binding.yearlyBtn.startAnimation(expandAnimation);
                TransitionManager.beginDelayedTransition(binding.rootLayout);

                binding.dailyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.weeklyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.monthlyBtn.setPadding(Utility.dpSize(context, 15), 0, Utility.dpSize(context, 15), 0);
                binding.yearlyBtn.setPadding(Utility.dpSize(context, 45), 0, Utility.dpSize(context, 45), 0);
            });

            binding.tvWeek.setOnClickListener(v -> {

                binding.tvDaily.setText("D");
                binding.tvWeek.setText("WEEKly");
                binding.tvMonth.setText("M");
                binding.tvYear.setText("Y");

                binding.tvDaily.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvWeek.setTextColor(ContextCompat.getColor(context, R.color.blue));
                binding.tvMonth.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvYear.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));

                binding.tvDaily.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvWeek.setBackgroundResource(R.drawable.underline_bottom);
                binding.tvMonth.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvYear.setBackgroundResource(R.drawable.underline_bottom_gray);

            });

            binding.tvDaily.setOnClickListener(v -> {

                binding.tvDaily.setText("Daily");
                binding.tvWeek.setText("W");
                binding.tvMonth.setText("M");
                binding.tvYear.setText("Y");

                binding.tvDaily.setTextColor(ContextCompat.getColor(context, R.color.blue));
                binding.tvWeek.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvMonth.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvYear.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));


                binding.tvDaily.setBackgroundResource(R.drawable.underline_bottom);
                binding.tvWeek.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvMonth.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvYear.setBackgroundResource(R.drawable.underline_bottom_gray);

            });

            binding.tvMonth.setOnClickListener(v -> {

                binding.tvDaily.setText("D");
                binding.tvWeek.setText("W");
                binding.tvMonth.setText("Monthly");
                binding.tvYear.setText("Y");

                binding.tvDaily.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvWeek.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvMonth.setTextColor(ContextCompat.getColor(context, R.color.blue));
                binding.tvYear.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));


                binding.tvDaily.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvWeek.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvMonth.setBackgroundResource(R.drawable.underline_bottom);
                binding.tvYear.setBackgroundResource(R.drawable.underline_bottom_gray);

            });

            binding.tvYear.setOnClickListener(v -> {

                binding.tvDaily.setText("D");
                binding.tvWeek.setText("W");
                binding.tvMonth.setText("M");
                binding.tvYear.setText("Yearly");

                binding.tvDaily.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvWeek.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvMonth.setTextColor(ContextCompat.getColor(context, R.color.l_card_background_color));
                binding.tvYear.setTextColor(ContextCompat.getColor(context, R.color.blue));


                binding.tvDaily.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvWeek.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvMonth.setBackgroundResource(R.drawable.underline_bottom_gray);
                binding.tvYear.setBackgroundResource(R.drawable.underline_bottom);

            });

        }

    }

    private class CatalogueHeaderViewHolder extends RecyclerView.ViewHolder {

        private ItemStoreHeaderBinding binding;

        public CatalogueHeaderViewHolder(ItemStoreHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {

            binding.ivSearch.setText("Dashboard");

//            if (repositoryManager.appSharedPreferences.getInstance().getBoolean(Constants.IS_DARK_MODE)) {
//                binding.ivDarkMode.setImageResource(R.drawable.ic_light_inactive);
//            } else {
//                binding.ivDarkMode.setImageResource(R.drawable.ic_dark__inactive);
//            }
//
//            binding.ivDarkMode.setOnClickListener(v -> {
//
//                if (repositoryManager.appSharedPreferences.getInstance().getBoolean(Constants.IS_DARK_MODE)) {
//                    repositoryManager.appSharedPreferences.getInstance().setBoolean(Constants.IS_DARK_MODE, false);
//                    if (context instanceof MainActivity) {
//                        ((MainActivity) context).restartApp(true);
//                    }
//                } else {
//                    repositoryManager.appSharedPreferences.getInstance().setBoolean(Constants.IS_DARK_MODE, true);
//                    if (context instanceof MainActivity) {
//                        ((MainActivity) context).restartApp(false);
//                    }
//                }
//            });
        }
    }

    private void setDrawDashLines(YAxis yAxis) {

        // // Create Limit Lines // //
        LimitLine llXAxis = new LimitLine(9f, "");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(20f, 30f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        LimitLine ll1 = new LimitLine(50f, "");
        ll1.setLineWidth(1.5f);
        ll1.setLineColor(Color.parseColor("#BCBCBC"));
        ll1.enableDashedLine(5f, 30f, 0f);
        ll1.setTextSize(10f);

        LimitLine ll2 = new LimitLine(150f, "");
        ll2.setLineWidth(1.5f);
        ll2.setLineColor(Color.parseColor("#BCBCBC"));
        ll2.enableDashedLine(5f, 30f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);
        // draw limit lines behind data instead of on top
        yAxis.setDrawLimitLinesBehindData(true);

        // add limit lines
        yAxis.addLimitLine(ll1);
        yAxis.addLimitLine(ll2);
    }

    private void setDataPresent(int count, int range, LineChart chart) {
        ArrayList<Entry> values = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            float val = (float) (Math.random() * (range + 1)) + 5;
            values.add(new Entry(i, val));
        }
        LineDataSet presentSet;
        presentSet = new LineDataSet(values, "Current");
        presentSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        presentSet.setCubicIntensity(0.2f);
        presentSet.setDrawFilled(true);
        presentSet.setDrawCircles(false);
        presentSet.setLineWidth(0f);
        presentSet.setCircleRadius(4f);
        presentSet.setCircleColor(Color.parseColor("#FFDEEFFD"));
        presentSet.setHighLightColor(Color.parseColor("#FFDEEFFD"));
        presentSet.setColor(Color.parseColor("#FFDEEFFD"));
//        presentSet.enableDashedLine(10f, 5f, 0f);

        if (Utils.getSDKInt() >= 18) {
            // drawables only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.fade_gray);
            presentSet.setFillDrawable(drawable);
        } else {
            presentSet.setFillColor(Color.parseColor("#FFDEEFFD"));
        }

        presentSet.setFillAlpha(100);
        presentSet.setDrawHorizontalHighlightIndicator(false);
        presentSet.setFillFormatter((dataSet, dataProvider) -> chart.getAxisLeft().getAxisMinimum());
        // create a data object with the data sets
        LineData data = new LineData(presentSet);
        data.setValueTextSize(0f);
        data.setDrawValues(false);

        // set data
        chart.setData(data);
    }

    private void setDataPrevious(int count, int range, LineChart chart) {
        ArrayList<Entry> values = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            float val = (float) (Math.random() * (range + 1)) - 45;
            values.add(new Entry(i, val));
        }

        LineDataSet previousSet;

        // create a dataset and give it a type
        previousSet = new LineDataSet(values, "Previous");

        previousSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//        previousSet.setCubicIntensity(0.2f);
        previousSet.setDrawFilled(true);
        previousSet.setDrawCircles(false);
        previousSet.setLineWidth(1.8f);
//        previousSet.setCircleRadius(10f);
        previousSet.setCircleColor(Color.parseColor("#FF3B8DF4"));
        previousSet.setHighLightColor(Color.parseColor("#FF3B8DF4"));
        previousSet.setColor(Color.parseColor("#FF3B8DF4"));
//        previousSet.enableDashedLine(10f, 5f, 0f);

//        if (Utils.getSDKInt() >= 18) {
//            // drawables only supported on api level 18 and above
//            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.fade_gray);
//            previousSet.setFillDrawable(drawable);
//        } else {
        previousSet.setFillColor(Color.parseColor("#FF3B8DF4"));
//        }
        previousSet.setFillAlpha(1000);
        previousSet.setDrawHorizontalHighlightIndicator(false);
        previousSet.setFillFormatter((dataSet, dataProvider) -> chart.getAxisLeft().getAxisMinimum());


        chart.getLineData().addDataSet(previousSet);
        chart.getLineData().getDataSetByIndex(1).setDrawValues(false);
    }

    private class CatalogueUpdateViewHolder extends RecyclerView.ViewHolder {

        private ItemUpdateListBinding binding;

        public CatalogueUpdateViewHolder(ItemUpdateListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }

        public void bindData(int position) {

            binding.tvPastUpdatesContent.setOnClickListener(v -> {

            });

            binding.recyclerView.setVisibility(View.GONE);
//            binding.recyclerView.setAdapter(new CatalogueUpdatesAdapter(context));
        }
    }
}
