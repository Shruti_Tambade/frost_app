package com.frost.leap.components.pdf;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentPDFFileViewBinding;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PDFFileViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PDFFileViewFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String myPdfUrl;

    private FragmentPDFFileViewBinding binding;
    private boolean isPDFView = false;


    public PDFFileViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PDFFileViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PDFFileViewFragment newInstance(String url) {
        PDFFileViewFragment fragment = new PDFFileViewFragment();
        Bundle args = new Bundle();
        args.putString("Url", url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getString("Url") != null)
                myPdfUrl = getArguments().getString("Url");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_p_d_f_file_view;
    }

    @Override
    public void setUp() {
        binding = (FragmentPDFFileViewBinding) getBaseDataBinding();

        final ProgressDialog progressDialog = new ProgressDialog(activity());
        progressDialog.setMessage("Loading Data...");
        progressDialog.setCancelable(false);
        binding.webView.requestFocus();
        binding.webView.getSettings().setJavaScriptEnabled(true);
//        String myPdfUrl = "http://www.africau.edu/images/default/sample.pdf";
//        String myPdfUrl = "https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fmechanical-engineering%2FGATE-ME.pdf?alt=media";
//        String url = "https://docs.google.com/viewer?embedded = true&url = " + myPdfUrl;
        String url = null;
        try {
            url = "https://docs.google.com/gview?embedded=true&url=" + URLEncoder.encode(myPdfUrl, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        binding.webView.loadUrl(url);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setLoadWithOverviewMode(true);
        binding.webView.getSettings().setUseWideViewPort(true);

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                isPDFView = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (!isPDFView)
                    binding.webView.loadUrl(url);
            }
        });
        binding.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress == 100) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void changeTitle(String title) {

    }
}
