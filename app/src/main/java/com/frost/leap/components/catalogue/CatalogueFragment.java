package com.frost.leap.components.catalogue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentCatalogueBinding;
import com.frost.leap.generic.EmptyCatalogueDataAdapter;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.services.FirebasePushService;
import com.frost.leap.services.LeadSquaredIntentService;
import com.frost.leap.viewmodels.catalogue.CatalogueViewModel;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewpager.CustomViewPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CatalogueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CatalogueFragment extends BaseFragment implements ICatalogueAdapter, ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentCatalogueBinding binding;
    private CatalogueViewModel viewModel;
    private List<CatalogueModel> catalogueModelList = new ArrayList<>();

    private CatalogueItemAdapter adapter;
    private String catalogueId = null;
    private String email;
    private ArrayList<String> activeInactiveCatalogueIDs = new ArrayList<>();
    private ArrayList<String> activeCatalogueIds = new ArrayList<>();
    private boolean noCatalogues;
    private CustomViewPagerAdapter customViewPagerAdapter;

    public CatalogueFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CatalogueFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CatalogueFragment newInstance(String param1, String param2) {
        CatalogueFragment fragment = new CatalogueFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CatalogueViewModel.class);
        if (getArguments() != null) {
            catalogueId = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_catalogue;
    }

    @Override
    public void setUp() {

        binding = (FragmentCatalogueBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);
        binding.viewPager.setOffscreenPageLimit(Constants.DASHBOARD_TABS.size());
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        binding.swipeRefreshLayout.setVisibility(View.GONE);

        email = viewModel.getUserRepository().appSharedPreferences.getString(Constants.UserInfo.EMAIL);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.emptyRecyclerview.setLayoutManager(new LinearLayoutManager(activity()));

        binding.recyclerView.setNestedScrollingEnabled(false);
        binding.emptyRecyclerview.setNestedScrollingEnabled(false);


        binding.tvReview.setOnClickListener(v -> {
            gotoTermsAndPolicies();
        });


        binding.tvAccept.setOnClickListener(v -> {
            acceptTerms();
        });

        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setEnabled(false);
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchCatalogues();
        });

        viewModel.getMessage().observe(this, message -> {
            if (message.getType() == 10) {
                showToast(message.getMessage(), false);
                return;
            }
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {

            switch (networkCall.getNetworkTag()) {
                case CatalogueViewModel.NetworkTags.CATALOGUE_LIST:
                    updateUIForCatalogueList(networkCall);
                    break;
            }

        });

        viewModel.updateCatalogueData().observe(this, list -> {

            if (noCatalogues) {
                binding.swipeRefreshLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                binding.recyclerView.setVisibility(View.GONE);
                binding.tabLayout.setVisibility(View.GONE);
                binding.viewPager.setVisibility(View.GONE);
            }
            noCatalogues = false;
            binding.swipeRefreshLayout.setEnabled(false);
            binding.swipeRefreshLayout.setVisibility(View.GONE);
            List<CatalogueModel> activeCatalogueList = new ArrayList<>();
            List<CatalogueModel> inActiveCatalogueList = new ArrayList<>();

            activeCatalogueIds = new ArrayList<>();
            activeInactiveCatalogueIDs = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {
                activeInactiveCatalogueIDs.add(list.get(i).getCatalogueId());
                if (list.get(i).getCatalogueStatus().equals("ACTIVE")) {
                    activeCatalogueList.add(list.get(i));
                    activeCatalogueIds.add(list.get(i).getCatalogueId());
                } else {
                    inActiveCatalogueList.add(list.get(i));
                }
            }

//            settingViewPager
            Logger.d("ACTIVE : -", "" + activeCatalogueIds.size() + " : INACTIVE_ACTIVE : -" + activeInactiveCatalogueIDs.size());
            binding.viewPager.setAdapter(customViewPagerAdapter = new CustomViewPagerAdapter(getChildFragmentManager(), Constants.DASHBOARD_TABS, activeInactiveCatalogueIDs, activeCatalogueIds, 5));
            Utility.changeTabsFont(binding.tabLayout);

            if (inActiveCatalogueList != null && inActiveCatalogueList.size() > 0)
                activeCatalogueList.addAll(inActiveCatalogueList);

            binding.swipeRefreshLayout.setEnabled(false);
            this.catalogueModelList = activeCatalogueList;
            binding.recyclerView.setFocusable(false);
            binding.recyclerView.setAdapter(adapter = new CatalogueItemAdapter(this, activity(), activeCatalogueList, RController.DONE));
            binding.recyclerView.setHasFixedSize(true);

            if (catalogueId != null)
                redirectToCatalogueOverView(catalogueId, activeCatalogueList);

            //Mixpanel
            List<String> cataloguesNames = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                cataloguesNames.add(list.get(i).getCatalogueName());
            }
            Mixpanel.dashboardView("Premium", cataloguesNames);

            //Validating LeadSquared Data
            Intent intent = new Intent(activity(), LeadSquaredIntentService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.GET_LEAD_BY_EMAILID);
            activity().startService(intent);
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });
        ShareDataManager.getInstance().isAcceptTerms().observe(this, isAccept -> {
            binding.llTerms.setVisibility(isAccept ? View.GONE : View.VISIBLE);
            if (noCatalogues) {
                if (binding.llTerms.getVisibility() == View.VISIBLE) {
                    binding.recyclerView.setVisibility(View.GONE);
                    binding.tabLayout.setVisibility(View.GONE);
                    binding.viewPager.setVisibility(View.GONE);
                    binding.llTerms.post(() -> {
                        binding.swipeRefreshLayout.setLayoutParams(getLayoutParams());
                    });
                }
            }
        });

    }

    private void gotoTermsAndPolicies() {
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.ABOUT_DEEP_LEARN);
        startActivity(intent);
    }


    private void acceptTerms() {
        if (!viewModel.isNetworkAvailable()) {
            Utility.showToast(activity(), Constants.PLEASE_CHECK_INTERNET, false);
            return;
        }
        try {
            binding.llTerms.setVisibility(View.GONE);
            Intent intent = new Intent(activity(), FirebasePushService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_ACCEPT_TERMS);
            activity().startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void redirectToCatalogueOverView(String catalogueId, List<CatalogueModel> activeCatalogueList) {

        for (int i = 0; i < activeCatalogueList.size(); i++) {

            if (activeCatalogueList.get(i).getCatalogueId().equals(catalogueId)) {
                openCatalogueOverview(activeCatalogueList.get(i), i);
                break;
            }
        }

    }

    private void updateUIForCatalogueList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.swipeRefreshLayout.setVisibility(View.VISIBLE);
                binding.emptyRecyclerview.setAdapter(new EmptyCatalogueDataAdapter(this, activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0, email));
                break;

            case IN_PROCESS:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.recyclerView.setAdapter(adapter = new CatalogueItemAdapter(this, activity(), Arrays.asList(null, null), RController.LOADING));
                break;

            case SUCCESS:
                break;

            case DONE:
                break;

            case NO_DATA:
                noCatalogues = true;
                if (binding.llTerms.getVisibility() == View.VISIBLE) {
                    if (binding.llTerms.getVisibility() == View.VISIBLE) {
                        binding.recyclerView.setVisibility(View.GONE);
                        binding.viewPager.setVisibility(View.GONE);
                        binding.tabLayout.setVisibility(View.GONE);
                        binding.llTerms.post(() -> {
                            binding.swipeRefreshLayout.setLayoutParams(getLayoutParams());
                        });
                    }
                }
                Mixpanel.dashboardView("Free", null);
                binding.swipeRefreshLayout.setEnabled(true);
                binding.swipeRefreshLayout.setVisibility(View.VISIBLE);
                binding.emptyRecyclerview.setAdapter(new EmptyCatalogueDataAdapter(this, activity(), "No subscribed catalogues", R.drawable.no_catalog_image_light, 2, email));
                break;

            case ERROR:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.swipeRefreshLayout.setVisibility(View.VISIBLE);
                binding.emptyRecyclerview.setAdapter(new EmptyCatalogueDataAdapter(this, activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0, email));
                break;

            case SERVER_ERROR:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.swipeRefreshLayout.setVisibility(View.VISIBLE);
                binding.emptyRecyclerview.setAdapter(new EmptyCatalogueDataAdapter(this, activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 0, email));
                break;

            case UNAUTHORIZED:
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    public void refreshCatalogues() {
        binding.swipeRefreshLayout.setEnabled(true);
        viewModel.fetchCatalogues();
    }

    public void checkCatalogueId(CatalogueModel catalogueModel) {

        if (catalogueModelList != null) {
            for (int f = 0; f < catalogueModelList.size(); f++) {
                if (catalogueModelList.get(f).getCatalogueId().equals(catalogueModel.getCatalogueId())) {
                    catalogueModel.setStudentCatalogueId(catalogueModelList.get(f).getStudentCatalogueId());
                    catalogueModel.setTotalDurationInSce(catalogueModelList.get(f).getTotalDurationInSce());
                    ((MainActivity) activity()).redirectToCatalogueOverview(catalogueModelList.get(f));
                    break;
                }
            }
        } else {
            viewModel.fetchCatalogues();
        }
    }

    @Override
    public void openCatalogueOverview(CatalogueModel catalogueModel, int position) {
        Mixpanel.aceessCourse(catalogueModel.getCatalogueName(), "Active");

        if (catalogueModel.getTotalDurationInSce() != null && catalogueModel.getTotalDurationInSce() > 0) {
            ((MainActivity) activity()).openCatalogOverview(catalogueModelList.get(position));
        } else {

            Utility.requestDialogAlert(activity(), this,
                    null,
                    "We're Actively Working To Get The Class Live Soon. Stay Tuned!",
                    1439,
                    "BACK",
                    false,
                    true);
        }
    }

    @Override
    public void openStoreOverview(CatalogueModel catalogueModel, int position) {
        Mixpanel.aceessCourse(catalogueModel.getCatalogueName(), "Inactive");
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.STORE_OVERVIEW);
        intent.putExtra("CATALOGUE_OVERVIEW", catalogueModel.getCatalogueName() != null ? catalogueModel.getCatalogueName() : "Catalogue Overview");
        ShareDataManager.getInstance().getStoreOverviewData().setCatalogueModel(catalogueModel);
        activity().startActivityForResult(intent, Utility.generateRequestCodes().get("UPDATE_STORE_COURSE"));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST_CODE", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("UPDATE_UPDATE_CARDS") && resultCode == Activity.RESULT_OK) {
            if (data.getParcelableExtra("UpdateCard") != null) {
                for (Fragment fragment : getChildFragmentManager().getFragments()) {
                    if (fragment instanceof CatalogueFragment) {
//                        CatalogueFragment catalogueFragment = (CatalogueFragment) fragment;
//                        catalogueFragment.checkCatalogueId(data.getParcelableExtra("UpdateCard"));
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void changeMotion(boolean isMotion) {
        //MiniView Padding
        ((MainActivity) activity()).changeMotion(isMotion);
    }

    @Override
    public void gotoStore() {
        ((MainActivity) activity()).goToHome(1);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (adapter == null) {
            viewModel.fetchCatalogues();
            isTermsAccepted();
        }
    }

    @Override
    public void doPositiveAction(int id) {
        switch (id) {
            case 1:
                break;
        }
    }

    @Override
    public void doNegativeAction() {

    }


    private void isTermsAccepted() {
        try {
            Intent intent = new Intent(activity(), FirebasePushService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_IS_ACCEPT_TERMS);
            activity().startService(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private RelativeLayout.LayoutParams getLayoutParams() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(0, binding.llTerms.getHeight(), 0, 0);
        return params;
    }


}
