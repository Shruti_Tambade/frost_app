package com.frost.leap.components.updates;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentDoubtClearingBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.updates.UpdateViewModel;

import apprepos.updates.model.UpdateModel;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DoubtClearingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoubtClearingFragment extends BaseFragment implements IUpdateAdapter, ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Handler handler;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentDoubtClearingBinding binding;

    private UpdateModel updateModel;
    private UpdateViewModel viewModel;
    private String choiceType = "YES";


    private AnimatedVectorDrawable animation;

    public DoubtClearingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoubtClearingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoubtClearingFragment newInstance(UpdateModel param1, String param2) {
        DoubtClearingFragment fragment = new DoubtClearingFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UpdateViewModel.class);
        if (getArguments() != null) {
            updateModel = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_doubt_clearing;
    }

    @Override
    public void setUp() {
        binding = (FragmentDoubtClearingBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {

            switch (networkCall.getNetworkTag()) {
                case UpdateViewModel.NetworkTags.GET_DATA_BY_UPDATEID:
                    updateUIForUPDATEID(networkCall);
                    break;
                case UpdateViewModel.NetworkTags.JOIN_LIVE_SESSION:
                    updateUIForJOINLIveSession(networkCall);
                    break;
            }

        });

        viewModel.getUpdateDataByID().observe(this, data -> {
            if (data != null) {
                this.updateModel = data;
                if (updateModel.getUpdateType().equals("LIVE_SESSION")) {
                    binding.frameLayout.setVisibility(View.VISIBLE);
                    appendData(updateModel);
                } else if (updateModel.getUpdateType().equals("TEST")) {
                    binding.frameLayout.setVisibility(View.VISIBLE);
                    appendData(updateModel);
                } else {
                    gotoPDFViewer(updateModel);
                }
            }
        });


        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        if (updateModel.getUpdateTitle() == null) {
            if (Utility.isNetworkAvailable(activity())) {
                viewModel.fetchUpdateDateByID(updateModel.getUpdateId());
                binding.frameLayout.setVisibility(View.VISIBLE);
            } else {

                binding.frameLayout.setVisibility(View.GONE);
                final Dialog dialog = new Dialog(activity());
                dialog.setContentView(R.layout.dailog_no_internet);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().getAttributes().windowAnimations = R.style.CustomDialogStyle;
                dialog.setCancelable(false);
                dialog.show();

                TextView tvTitle = dialog.findViewById(R.id.tvTitle);
                TextView tvBack = dialog.findViewById(R.id.tvBack);
                TextView tvRetry = dialog.findViewById(R.id.tvRetry);

                tvTitle.setText("There is no internet connection please check it !");

                tvBack.setOnClickListener(v -> {
                    dialog.dismiss();
                    activity().finish();
                });

                tvRetry.setOnClickListener(v -> {
                    if (Utility.isNetworkAvailable(activity())) {
                        viewModel.fetchUpdateDateByID(updateModel.getUpdateId());
                        dialog.dismiss();
                    }

                });
            }
        } else
            appendData(updateModel);

    }


    private void appendData(UpdateModel updateModel) {
        try {
            if (updateModel != null && updateModel.getUpdateType() != null) {

                long currentTimeMillis = System.currentTimeMillis();
                long liveSessionStartTimeMillis = Utility.getTimeInMillis(updateModel.getEffectiveDate(), updateModel.getStartTime());
                long liveSessionEndTimeMillis = Utility.getTimeInMillis(updateModel.getEffectiveDate(), updateModel.getEndTime());

                if (updateModel.getUpdateType().equals("TEST")) {
                    binding.ivPlayIcon.setImageResource(R.drawable.circle_white);
                    binding.tvJoin.setBackgroundResource(R.drawable.corner_radius_blue_10);
                    binding.tvJoin.setText("OPEN OTS");
                    binding.tvInstructor.setVisibility(View.GONE);
                    binding.recyclerView.setVisibility(View.VISIBLE);

                    binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
                    binding.recyclerView.setAdapter(new TestAdapter(activity(), this, updateModel.getTestList()));


                    binding.tvMessageContent.setVisibility(View.GONE);
                    binding.ivMessageIcon.setVisibility(View.GONE);
                    binding.radioGroup.setVisibility(View.GONE);
                }

                if (currentTimeMillis > liveSessionEndTimeMillis) {
                    binding.tvJoin.setVisibility(View.GONE);
                    binding.tvMessageContent.setVisibility(View.GONE);
                    binding.ivMessageIcon.setVisibility(View.GONE);
                    binding.radioGroup.setVisibility(View.GONE);
                } else if (currentTimeMillis > liveSessionStartTimeMillis - 10 * 60 * 1000) {
                    binding.tvJoin.setBackgroundResource(R.drawable.corner_radius_blue_10);
                    if (!updateModel.getUpdateType().equals("TEST"))
                        binding.tvJoin.setText("JOIN");

                } else {
                    binding.tvJoin.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
                    if (!updateModel.getUpdateType().equals("TEST"))
                        binding.tvJoin.setText("JOIN");
                }

                if (updateModel.getChoiceType() != null && !updateModel.getChoiceType().isEmpty()) {
                    switch (updateModel.getChoiceType()) {
                        case "YES":
                            binding.tvYes.setChecked(true);
                            break;
                        case "NO":
                            binding.tvNo.setChecked(true);
                            break;
                        case "MAYBE":
                            binding.tvMayBe.setChecked(true);
                            break;
                    }
                }

                if (updateModel.getEffectiveDate() != null && !updateModel.getEffectiveDate().isEmpty())
                    binding.tvDate.setText("" + Utility.getUpdatePageDate(updateModel.getEffectiveDate(), updateModel)
                            + " | " + Utility.getTimeInAMPM(updateModel.getStartTime())
                            + " - " + Utility.getTimeInAMPM(updateModel.getEndTime()));

                if (updateModel.getInstructorName() != null && !updateModel.getInstructorName().isEmpty())
                    binding.tvInstructor.setText("Instructor: " + updateModel.getInstructorName());

                if (updateModel.getCatalogueList() != null) {
                    String catalogueName = "";
                    for (int i = 0; i < updateModel.getCatalogueList().size(); i++) {
                        catalogueName = catalogueName + (i == 0 ? "" : "\n") + updateModel.getCatalogueList().get(i).getCatalogueName();
                    }
                    binding.tvCatalogueName.setText(catalogueName);
                }

                if (updateModel.getSubjectName() != null && !updateModel.getSubjectName().isEmpty())
                    binding.tvTopicName.setText(updateModel.getSubjectName());

                if (updateModel.getSessionDescription() != null && !updateModel.getSessionDescription().isEmpty())
                    binding.tvSessionDescription.setText(updateModel.getSessionDescription());

                if (updateModel.getUpdateTitle() != null && !updateModel.getUpdateTitle().isEmpty())
                    binding.tvTitle.setText(updateModel.getUpdateTitle());

                binding.tvJoin.setOnClickListener(v -> {

                    if (updateModel != null && updateModel.getUpdateType() != null) {

                        long cTimeMillis = System.currentTimeMillis();
                        long startTimeMillis = Utility.getTimeInMillis(updateModel.getEffectiveDate(), updateModel.getStartTime());
                        long endTimeMillis = Utility.getTimeInMillis(updateModel.getEffectiveDate(), updateModel.getEndTime());

                        if (cTimeMillis > endTimeMillis) {

                        } else if (cTimeMillis > startTimeMillis - 10 * 60 * 1000) {

                            if (updateModel.getUpdateType().equals("LIVE_SESSION")) {

                                String url = updateModel.getJoinLink();
                                if (!url.startsWith("http://") && !url.startsWith("https://"))
                                    url = "http://" + url;
                                try {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    if (intent.resolveActivity(activity().getPackageManager()) != null) {
                                        startActivity(intent);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                ClipboardManager clipboardManager = (ClipboardManager) activity().getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("text", url);
                                clipboardManager.setPrimaryClip(clipData);
//                                Utility.showToast(activity(), "Live session url is copied to clipboard", false);

                            } else if (updateModel.getUpdateType().equals("TEST")) {

                                try {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.OTS_LOGIN_URL));
                                    startActivity(browserIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                ClipboardManager clipboardManager = (ClipboardManager) activity().getSystemService(CLIPBOARD_SERVICE);
                                ClipData clipData = ClipData.newPlainText("text", Constants.OTS_LOGIN_URL);
                                clipboardManager.setPrimaryClip(clipData);
//                                Utility.showToast(activity(), "OTS url is copied to clipboard", false);

                            }

                        } else {

                        }

                    }


                });

                binding.radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                    switch (checkedId) {
                        case R.id.tvYes:
                            choiceType = "YES";
                            viewModel.joinUpdate(updateModel.getUpdateId(), choiceType);
                            break;
                        case R.id.tvNo:
                            choiceType = "NO";
                            viewModel.joinUpdate(updateModel.getUpdateId(), choiceType);
                            break;
                        case R.id.tvMayBe:
                            choiceType = "MAYBE";
                            viewModel.joinUpdate(updateModel.getUpdateId(), choiceType);
                            break;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUIForUPDATEID(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.frameLayout.setVisibility(View.GONE);
                break;
            case IN_PROCESS:
                binding.frameLayout.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.VISIBLE);
                break;

            case SUCCESS:
            case DONE:
            case NO_DATA:
            case ERROR:
            case SERVER_ERROR:
                binding.progressBar.setVisibility(View.GONE);
                break;

        }
    }

    private void updateUIForJOINLIveSession(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                break;
            case IN_PROCESS:
                break;
            case SUCCESS:
                updateModel.setChoiceType(choiceType);
                if (activity().getIntent().getIntExtra("TAG", 0) == 1) {
                    ShareDataManager.getInstance().setDashBoardUpdateData(updateModel);
                } else if (activity().getIntent().getIntExtra("TAG", 0) == 2) {
                    ShareDataManager.getInstance().setAllUpdatesData(updateModel);
                }
                break;

            case DONE:
            case ERROR:
            case SERVER_ERROR:
            case UNAUTHORIZED:
            case NO_DATA:
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }


    @Override
    public void onResume() {
        super.onResume();
        if (handler == null) {
            handler = new Handler();
            setRunnable();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (handler != null)
            handler.removeCallbacksAndMessages(null);
        handler = null;
    }


    private void setRunnable() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Logger.d("HANDLER IS RUNNING");
                if (updateModel != null && updateModel.getStartTime() != null) {
                    long currentTimeMillis = System.currentTimeMillis();
                    long liveSessionStartTimeMillis = Utility.getTimeInMillis(updateModel.getEffectiveDate(), updateModel.getStartTime());
                    long liveSessionEndTimeMillis = Utility.getTimeInMillis(updateModel.getEffectiveDate(), updateModel.getEndTime());
                    binding.tvJoin.setVisibility(View.VISIBLE);

                    if (!updateModel.getUpdateType().equals("TEST")) {
                        binding.tvMessageContent.setVisibility(View.VISIBLE);
                        binding.ivMessageIcon.setVisibility(View.VISIBLE);
                        binding.radioGroup.setVisibility(View.VISIBLE);
                    }

                    if (currentTimeMillis > liveSessionEndTimeMillis) {
                        binding.tvJoin.setVisibility(View.GONE);

                        if (!updateModel.getUpdateType().equals("TEST")) {
                            binding.tvMessageContent.setVisibility(View.GONE);
                            binding.ivMessageIcon.setVisibility(View.GONE);
                            binding.radioGroup.setVisibility(View.GONE);
                        }
                    } else if (currentTimeMillis > liveSessionStartTimeMillis - 10 * 60 * 1000) {
                        if (!updateModel.getUpdateType().equals("TEST")) {
                            binding.tvJoin.setBackgroundResource(R.drawable.corner_radius_blue_10);
                            binding.tvJoin.setText("JOIN");
                        }
                    } else {
                        if (!updateModel.getUpdateType().equals("TEST")) {
                            binding.tvJoin.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
                            binding.tvJoin.setText("JOIN");
                        }
                    }
                }

                if (handler != null)
                    handler.postDelayed(this, 10000);
            }
        });

    }

    @Override
    public void redirectToDoubtPage(UpdateModel updateModel, int position) {

    }

    @Override
    public void retryUpdateAPI() {

    }

    @Override
    public void redirectTOAllUpdates() {

    }

    @Override
    public void openPDF(String pdfFile, String title) {

    }


    @Override
    public void doPositiveAction(int id) {
        switch (id) {
            case 1439:
                Toast.makeText(activity(), "Yes you click me", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void doNegativeAction() {

    }


    private void gotoPDFViewer(UpdateModel updateModel) {
        try {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(updateModel.getPdfLink()), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(updateModel.getPdfLink()), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateModel.getPdfLink()));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }

        activity().finish();
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        Drawable d = binding.ivNotificationIcon.getDrawable();
//        if (d instanceof AnimatedVectorDrawable) {
//
//            Log.d("testanim", "onCreate: instancefound" + d.toString());
//            animation = (AnimatedVectorDrawable) d;
//            animation.start();
//        }
//    }
}
