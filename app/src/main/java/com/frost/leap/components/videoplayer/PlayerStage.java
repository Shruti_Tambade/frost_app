package com.frost.leap.components.videoplayer;

public enum PlayerStage {
    // Screen
    NORMAL,
    FULL_SCREEN,
    DEMO_FULL_SCREEN,
    MINI_VIEW,
    NORMAL_END,
    FULL_SCREEN_END,
    DEMO_FULL_SCREEN_END
}
