package com.frost.leap.components.media.interfaces;




import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.media.models.Media;

import java.util.ArrayList;


public interface IMediaPickerAdapter {

    public void addMedia();

    public void removeMedia(GalleryFile media, int position);

    public void openMediaSlider(ArrayList<Media> mediaArrayList, int position);
}
