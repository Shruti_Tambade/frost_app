package com.frost.leap.components.videoplayer;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.frost.leap.BuildConfig;
import com.frost.leap.R;
import com.frost.leap.activities.BaseActivity;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.topics.UnitProviderFragment;
import com.frost.leap.databinding.FragmentVideoPlayerBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.BaseViewModel;
import com.frost.leap.viewmodels.topics.TopicsViewModel;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadHelper;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;
import com.google.firebase.perf.metrics.AddTrace;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

import supporters.constants.Constants;
import supporters.constants.PlayBackSpeed;
import supporters.constants.VideoQuality;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;


public class VideoPlayerFragment extends BaseFragment
        implements ISpeedController, IVideoResolutionController, IHeadSetsController {


    private boolean isTouchEnable = false;
    private static final String VIDEO_URL = "video_url", TAG = "VideoPlayerFragment";
    private boolean actionFwdBwd = false;
    private boolean onTouch = false;
    private DataSource.Factory dataSourceFactory;
    private String videoUrl = null;
    private Handler updateHandler;
    private Handler autoPlayHandler;
    private boolean initial = false;
    private FragmentVideoPlayerBinding videoPlayerBinding;
    private UnitProviderFragment unitProviderFragment;
    private float previousVolume = 1f;

    private TelephonyManager telephonyManager;
    private AudioManager mAudioManager;
    private MediaReceiver mediaReceiver = new MediaReceiver();
    private ComponentName mRemoteControlResponder;
    public static IHeadSetsController iHeadSetsController;
    private NetworkReceiver networkReceiver;
    private boolean state = true;

    private boolean isBuffering = false;

    //VideoPlayer
    private int counter = 5;
    private SimpleExoPlayer player;
    private long playbackPosition = 0;
    private int currentWindow = 0;
    private boolean playWhenReady = true;
    private DefaultTrackSelector trackSelector;
    private TopicsViewModel viewModel;
    private String title = null;
    private String videoPath = null;
    private int currentBitrate = 1;
    private int resolution = 0;
    private WeakReference<VideoPlayerFragment> videoPlayerFragmentWeakReference;
    public boolean onPlay = false;
    public boolean fromOffline = false;
    public BandwidthMeter bandwidthMeter;

    private PlayBackSpeed playBackSpeed = PlayBackSpeed.SP1X;
    private VideoQuality videoQuality = VideoQuality.AUTO;
    private Runnable autoHide;
    private boolean previousSeek = false;
    private boolean isVideoEnded = false;
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;

    public VideoPlayerFragment() {
        // Required empty public constructor
    }

    public static VideoPlayerFragment newInstance(String videoUrl) {

        VideoPlayerFragment fragment = new VideoPlayerFragment();
        Bundle args = new Bundle();
        args.putString(VIDEO_URL, videoUrl);
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scaleGestureDetector = new ScaleGestureDetector(activity(), new ScaleListener());
        gestureDetector = new GestureDetector(activity(), new SingleTapGesture());

        activity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        videoPlayerFragmentWeakReference = new WeakReference<>(this);
        networkReceiver = new NetworkReceiver();
        mAudioManager = (AudioManager) activity().getSystemService(Context.AUDIO_SERVICE);
        dataSourceFactory = buildDataSourceFactory();
        iHeadSetsController = this;
        viewModel = new ViewModelProvider(this).get(TopicsViewModel.class);
        unitProviderFragment = (UnitProviderFragment) getParentFragment();
        telephonyManager = (TelephonyManager) activity().getSystemService(Service.TELEPHONY_SERVICE);
        mRemoteControlResponder = new ComponentName(activity().getPackageName(), HeadSetActionReceiver.class.getName());
        if (getArguments() != null) {
            videoPath = getArguments().getString(VIDEO_URL);
        }
    }


    @Override
    public int layout() {
        return R.layout.fragment_video_player;
    }


    @Override
    public void setUp() {
        videoPlayerBinding = (FragmentVideoPlayerBinding) getBaseDataBinding();


        if (activity() instanceof HelperActivity) {
            setUpForDemoVideos();
        } else {

        }
        setUpUiElements();

    }


    private void setUpForDemoVideos() {
        controlScreenOrientation(-1);
        LinearLayout.MarginLayoutParams params = (LinearLayout.MarginLayoutParams)
                videoPlayerBinding.llBottomLayout.getLayoutParams();
        params.leftMargin = Utility.toSdp(R.dimen._15sdp);
        params.rightMargin = Utility.toSdp(R.dimen._15sdp);
        params.bottomMargin = Utility.toSdp(R.dimen._20sdp);
        videoPlayerBinding.llBottomLayout.setLayoutParams(params);
        videoPlayerBinding.deepLogo.setVisibility(View.VISIBLE);
        videoPlayerBinding.imgFullScreen.setVisibility(View.GONE);
        videoPlayerBinding.imgForward.setVisibility(View.VISIBLE);
        videoPlayerBinding.imgBackward.setVisibility(View.VISIBLE);
        videoPlayerBinding.playerView.setResizeMode(ShareDataManager.getInstance().getAspectRatio());
        videoPlayerBinding.imgClose.setImageResource(R.drawable.ic_close_black_24dp);
        videoPlayerBinding.llPrevious.setVisibility(View.GONE);
        videoPlayerBinding.llNext.setVisibility(View.GONE);
        videoPlayerBinding.llAutoPlay.setVisibility(View.GONE);
        videoPlayerBinding.llReport.setVisibility(View.GONE);
        videoPlayerBinding.llFooter.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utility.toSdp(R.dimen._15sdp)));

        LinearLayout.LayoutParams playParams = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._80sdp), Utility.toSdp(R.dimen._80sdp));
        playParams.setMargins(Utility.toSdp(R.dimen._60sdp), 0, Utility.toSdp(R.dimen._60sdp), 0);
        videoPlayerBinding.imgPlay.setLayoutParams(playParams);
        videoPlayerBinding.imgAction.setLayoutParams(playParams);

        videoPlayerBinding.imgPlay.setVisibility(isBuffering ? View.INVISIBLE : View.VISIBLE);

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._70sdp), Utility.toSdp(R.dimen._70sdp));
        videoPlayerBinding.imgBackward.setLayoutParams(params1);
        videoPlayerBinding.imgForward.setLayoutParams(params1);
        videoPlayerBinding.imgBackward.setVisibility(View.VISIBLE);
        videoPlayerBinding.imgForward.setVisibility(View.VISIBLE);

        RelativeLayout.LayoutParams progressParams = new RelativeLayout.LayoutParams(Utility.toSdp(R.dimen._85sdp), R.dimen._85sdp);
        progressParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressParams.addRule(RelativeLayout.CENTER_VERTICAL);
        progressParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        videoPlayerBinding.progressBar.setLayoutParams(progressParams);
    }


    private void setUpUiElements() {

        ((SurfaceView) videoPlayerBinding.playerView.getVideoSurfaceView()).setSecure(BuildConfig.DEBUG ? true : true);

        ((SurfaceView) videoPlayerBinding.playerView.getVideoSurfaceView()).
                getHolder().setFixedSize(Utility.getScreenWidth(activity()), Utility.getScreenWidth(activity()) * 9 / 16);

        onPlay = true;
        if (videoPath != null) {
            playVideo(videoPath);
        }
        inTouchMode(false);
        addClicksEvents();
    }

    private void addClicksEvents() {

        // Resolution text
        ShareDataManager.getInstance().getLiveDataResolution().observe(this, resolutions -> {
            if (videoPlayerBinding != null) {
                videoPlayerBinding.tvQuality.setText("" + resolutions);
            }
        });
        // Play
        videoPlayerBinding.imgPlay.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            if (isVideoEnded) {
                onVideoEnded(false);
                isVideoEnded = false;
                playVideoFromStart();
            } else {
                if (isPlaying()) {
                    videoPlayerBinding.imgPlay.setImageResource(R.drawable.ic_play_arrow_black);
                    pauseVideo();
                    showCustomController();
                } else {
                    videoPlayerBinding.imgPlay.setImageResource(R.drawable.ic_pause_black);
                    playVideo();
                    showCustomController();
                }
            }

        });

        // mute
        videoPlayerBinding.imgSound.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            if (player == null)
                return;

            player.setVolume(player.getVolume() == 0f ? 1f : 0f);
            previousVolume = player.getVolume();
            videoPlayerBinding.imgSound.setImageResource(player.getVolume() == 0f ?
                    R.drawable.ic_volume_off : R.drawable.ic_volume_up);

        });

        // full screen
        videoPlayerBinding.imgFullScreen.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            controlScreenOrientation(-1);
        });

        // << Backward
        videoPlayerBinding.imgBackward.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            videoBackward(false);
            if (player != null && player.getPlayWhenReady())
                hideAuto();

        });

        // >> Forward
        videoPlayerBinding.imgForward.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            videoForward(false);
            if (player != null && player.getPlayWhenReady())
                hideAuto();
        });

        // Video settings resolutions and speed
        videoPlayerBinding.videoSettings.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            videoPlayerBinding.rlVideoSettings.startAnimation(animation);
            inVideoSettings();
        });

        // close in HelperActivity, back to normal in MainActivity
        videoPlayerBinding.imgClose.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            if (activity() instanceof HelperActivity && videoPlayerBinding.imgClose.getVisibility() == View.VISIBLE) {
                activity().onBackPressed();
                releasePlayer();
            } else {
                activity().onBackPressed();
            }
        });

        // swipe down
        videoPlayerBinding.llSwipeDown.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            if (unitProviderFragment != null)
                unitProviderFragment.endTransaction();
        });

        // report
        videoPlayerBinding.llReport.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            clearAutoPlay();
            if (unitProviderFragment != null)
                unitProviderFragment.reportAboutVideo();
        });


        // play next video
        videoPlayerBinding.llNext.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            pauseVideo();
            showCustomController();
            autoPlayIn5Seconds(false);
            if (unitProviderFragment != null)
                unitProviderFragment.autoPlayNext();
        });

        videoPlayerBinding.imgNext.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            pauseVideo();
            showCustomController();
            autoPlayIn5Seconds(false);
            if (unitProviderFragment != null)
                unitProviderFragment.autoPlayNext();
        });


        // play previous video
        videoPlayerBinding.llPrevious.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            pauseVideo();
            showCustomController();
            autoPlayIn5Seconds(false);
            if (unitProviderFragment != null)
                unitProviderFragment.autoPlayPrevious();
        });

        // play previous video
        videoPlayerBinding.imgPrevious.setOnClickListener(v -> {
            Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_affect);
            v.startAnimation(animation);
            pauseVideo();
            showCustomController();
            autoPlayIn5Seconds(false);
            if (unitProviderFragment != null)
                unitProviderFragment.autoPlayPrevious();
        });


        // Tap action
        ShareDataManager.getInstance().getTapLiveData().observe(this, tap -> {
            if (tap == 1) {
                tapAction();
            } else if (tap == 2) {
                videoForward(true);
            } else if (tap == -2) {
                videoBackward(true);
            }
        });


    }

    public void clearAutoPlay() {
        if (isVideoEnded) {
            autoPlayIn5Seconds(false);
        }
    }

    private void updateVideoDetails() {

        if (updateHandler != null) {
            updateHandler.removeCallbacksAndMessages(null);
            updateHandler = null;
        }

        updateHandler = new Handler();
        activity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (player != null) {
                    if (!isBuffering)
                        updateVideoStream(player.getCurrentPosition(), player.getPlaybackState(), playWhenReady);
                    else {
                        if (videoPlayerBinding.videoProgressBar.getProgress() == 100)
                            updateVideoStream(player.getDuration(), Player.STATE_ENDED, playWhenReady);
                    }

                    if (player.getCurrentPosition() <= 0)
                        videoPlayerBinding.tvTimeLimit.setText("00:00 / " + getTimeFromMillis(player.getDuration()));
                    else if (player.getCurrentPosition() >= player.getDuration())
                        videoPlayerBinding.tvTimeLimit.setText(getTimeFromMillis(player.getDuration()) + " / " + getTimeFromMillis(player.getDuration()));
                    else
                        videoPlayerBinding.tvTimeLimit.setText(getTimeFromMillis(player.getCurrentPosition()) + " / " + getTimeFromMillis(player.getDuration()));


                    // updating seekBar Details
                    int mCurrentPosition = (int) (player.getCurrentPosition() / 1000);
                    int bufferPosition = (int) (player.getBufferedPosition() / 1000);

                    videoPlayerBinding.videoProgressBar.setSecondaryProgress(bufferPosition);
                    videoPlayerBinding.videoProgressBar.setProgress(mCurrentPosition);

                    if (unitProviderFragment != null) {
                        unitProviderFragment.binding.videoProgressBar.setSecondaryProgress(bufferPosition);
                        unitProviderFragment.binding.videoProgressBar.setProgress(mCurrentPosition);
                    }


                    if (player.getPlaybackState() == Player.STATE_ENDED) {
                        clearHandlers();
                    } else {
                        videoPlayerBinding.llAutoPlay.setVisibility(View.INVISIBLE);
                    }
                    if (updateHandler != null)
                        updateHandler.postDelayed(this, 1000);
                } else {
                    updateHandler = null;
                }
            }
        });
    }

    private void updateVideoStream(long currentPosition, int playbackState, boolean playWhenReady) {
        if (unitProviderFragment != null) {
            unitProviderFragment.updateVideoStream(currentPosition, playbackState, playWhenReady);
        }
    }

    private void playVideoFromStart() {
        if (player != null) {
            isVideoEnded = false;
            player.seekTo(0);
            player.setPlayWhenReady(true);
        }
    }

    public void hideAuto() {
        if (autoHide != null) {
            videoPlayerBinding.controllerLayout.removeCallbacks(autoHide);
        }

        if (autoHide == null) {
            autoHide = () -> {
                if (videoPlayerBinding.controllerLayout.getVisibility() == View.GONE) {
                    onTouch = false;
                } else
                    onTouch = true;


                if (unitProviderFragment != null) {
                    unitProviderFragment.hideSeekThumb();
                }

                if (activity() == null) {
                    videoPlayerBinding.controllerLayout.setVisibility(View.GONE);
                    return;
                }

                videoPlayerBinding.controllerLayout.clearAnimation();
                if (videoPlayerBinding.controllerLayout.getVisibility() == View.VISIBLE) {
                    Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_controllers_fade);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            videoPlayerBinding.controllerLayout.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    videoPlayerBinding.controllerLayout.startAnimation(animation);
                }


            };
        }

        videoPlayerBinding.controllerLayout.postDelayed(autoHide, 3000);
    }


    private void showCustomController() {
        try {
            if (isPlaying()) {
                onTouch = false;
                videoPlayerBinding.controllerLayout.clearAnimation();
                if (videoPlayerBinding.controllerLayout.getVisibility() != View.VISIBLE) {
                    videoPlayerBinding.controllerLayout.setVisibility(View.VISIBLE);
                    Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_controller_affects);
                    videoPlayerBinding.controllerLayout.startAnimation(animation);
                }
                if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                    videoPlayerBinding.imgClose.setVisibility(View.VISIBLE);
                    videoPlayerBinding.llSwipeDown.setVisibility(View.GONE);
                } else {
                    videoPlayerBinding.imgClose.setVisibility(View.GONE);
                    videoPlayerBinding.llSwipeDown.setVisibility(View.VISIBLE);
                }
                videoPlayerBinding.videoProgressBar.getThumb().mutate().setAlpha(255);
                if (videoPlayerBinding.controllerLayout.getAlpha() != 1.0f)
                    videoPlayerBinding.controllerLayout.setAlpha(1.0f);

                hideAuto();
            } else {

                if (autoHide != null) {
                    videoPlayerBinding.controllerLayout.removeCallbacks(autoHide);
                }
                videoPlayerBinding.controllerLayout.clearAnimation();
                if (videoPlayerBinding.controllerLayout.getVisibility() != View.VISIBLE) {
                    videoPlayerBinding.controllerLayout.setVisibility(View.VISIBLE);
                    Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_controller_affects);
                    videoPlayerBinding.controllerLayout.startAnimation(animation);
                }
                if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                    videoPlayerBinding.imgClose.setVisibility(View.VISIBLE);
                    videoPlayerBinding.llSwipeDown.setVisibility(View.GONE);
                } else {
                    videoPlayerBinding.imgClose.setVisibility(View.GONE);
                    videoPlayerBinding.llSwipeDown.setVisibility(View.VISIBLE);
                }
                videoPlayerBinding.videoProgressBar.getThumb().mutate().setAlpha(255);
                if (videoPlayerBinding.controllerLayout.getAlpha() != 1.0f)
                    videoPlayerBinding.controllerLayout.setAlpha(1.0f);
                onTouch = false;
                if (player != null && player.getPlayWhenReady() && !isVideoEnded)
                    hideAuto();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (unitProviderFragment != null) {
            unitProviderFragment.showSeekThumb();
        }
    }

    public void hideCustomController() {
        try {
            if (videoPlayerBinding.controllerLayout.getVisibility() == View.GONE && !onTouch) {
                showCustomController();
            } else {
                onTouch = true;
                if (autoHide != null)
                    videoPlayerBinding.controllerLayout.removeCallbacks(autoHide);

                videoPlayerBinding.controllerLayout.clearAnimation();

                if (unitProviderFragment != null) {
                    unitProviderFragment.hideSeekThumb();
                }

                if (videoPlayerBinding.controllerLayout.getVisibility() == View.VISIBLE) {
                    if (activity() == null) {
                        videoPlayerBinding.controllerLayout.setVisibility(View.GONE);
                        return;
                    }
                    Animation animation = AnimationUtils.loadAnimation(activity(), R.anim.alpha_controllers_fade);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            videoPlayerBinding.controllerLayout.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    videoPlayerBinding.controllerLayout.startAnimation(animation);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showCustomSeekBar() {
        videoPlayerBinding.videoProgressBar.setVisibility(View.VISIBLE);
        videoPlayerBinding.videoProgressBar.getThumb().mutate().setAlpha(255);
        applySeekBar(videoPlayerBinding.videoProgressBar);
    }

    private void hideCustomSeekBar() {
        videoPlayerBinding.videoProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        iHeadSetsController = this;
        if (player == null && videoUrl != null) {
            initializePlayer(videoUrl, false, null);
        }


        if (telephonyManager != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
            setPlayControllers();
        }

        if (player != null && videoUrl != null) {
            player.setPlayWhenReady(state);
            setPlayControllers();
        }

        controlReceivers(true);
        new Handler().postDelayed(() -> {
            decorateScreenUi();
        }, 1000);
        decorateScreenUi();

        updateVideoDetails();
    }


    @Override
    public void onPause() {
        super.onPause();
        iHeadSetsController = null;
        if (player == null) {

        } else {
            // updating the control's
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            setPlayControllers();

            if (Util.SDK_INT < 23) {
                releasePlayer();
            } else {
                state = player.getPlayWhenReady();
                player.setPlayWhenReady(false);
            }
        }

        controlReceivers(false);

        clearHandlers();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (player == null) {

        } else {
            if (mAudioManager != null)
                mAudioManager.abandonAudioFocus(focusChangeListener);
            pauseVideo();
            if (Util.SDK_INT < 23) {
                releasePlayer();
            }
        }

        setPlayControllers();
        if (telephonyManager != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        controlReceivers(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controlReceivers(false);
        releasePlayer();
        clearHandlers();

        if (autoPlayHandler != null)
            autoPlayHandler.removeCallbacksAndMessages(null);
    }

    @AddTrace(name = "InitializePlayer", enabled = true)
    public void initializePlayer(String videoUrl, boolean force, String securityLevel) {
        initial = true;

        if (videoPlayerBinding == null)
            return;

        if (mAudioManager == null)
            mAudioManager = (AudioManager) activity().getSystemService(Context.AUDIO_SERVICE);

        int result = mAudioManager.requestAudioFocus(focusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        isBuffering = true;
        fromOffline = false;
        videoPlayerBinding.progressBar.setVisibility(View.VISIBLE);
        if (force) {
            if (unitProviderFragment != null) {
                unitProviderFragment.hideSeekThumb();
            }
            clearHandlers();
            autoPlayIn5Seconds(false);
            currentWindow = 0;
            playbackPosition = 0;
            isVideoEnded = false;
            onVideoEnded(false);
            videoPlayerBinding.networkErrorMsg.setVisibility(View.GONE);
            videoPlayerBinding.tvTimeLimit.setText("00:00 / 00:00");
//             playing from previous position
            if (activity() instanceof MainActivity)
                playbackPosition = ShareDataManager.getInstance().getSubTopicCurrentPosition();
        }
        if (TextUtils.isEmpty(videoUrl)) {
            if (player != null)
                releasePlayer();
            showToast("Video link not available", false);
            return;
        }

        if (videoUrl != null) {
//            videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/14d3b2f8-ebcc-4738-ba2e-711bd0166e2c/dash/6e8252ab-18de-4499-a90c-ed568fb2f4d6-1.2 problems on limits(1).mpd";
//            videoUrl =  "https://d2jx687vmgx9ce.cloudfront.net/62823f88-227e-4fbf-9d36-91704dc42563/dash/0240d8b8-4f32-490f-a2d8-05d5944bf453-1.1%20Introduction%20to%20Project-Management.mpd";
//            videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/238dcce7-6cbb-4f18-9bed-f3d11333cfc0/dash/e921f4d4-b892-411b-be12-07615fade913-Force.mpd";
//            videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/c6aa2b70-86a6-4c48-95b9-d1bd8ea68148/dash/74577549-8cba-467d-a2f3-78abc9b4c44b-01.%20Scope%20of%20the%20Subject.mpd";
//            videoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
//            videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/0338c7d5-485b-430d-8406-9c13dd8a8cf1/dash/e11bbda1-28b5-47f7-966e-c335b8a8af00-Introduction Compressed Version.mpd";
//            videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/37bc2ea3-1069-463a-a0c2-dedd8a0991ba/dash/488c6371-8220-4d2a-9a62-f9eba6da9649-3.1%20first%20fundamental%20theorem%20of%20integral%20calculums.mpd";
//            videoUrl = "https://d3ble58c9xsklv.cloudfront.net/out/v1/e63de0e107ca40a3bddf9b93086cb680/eceda4fdcdfa48ad99c93390d9f2eb2f/13f0461f8a4342d792b79a1d23a429d0/index.m3u8";
//            videoUrl = "http://ftp.itec.aau.at/datasets/DASHDataset2014/BigBuckBunny/15sec/BigBuckBunny_15s_simple_2014_05_09.mpd";
//            videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/0338c7d5-485b-430d-8406-9c13dd8a8cf1/dash/e11bbda1-28b5-47f7-966e-c335b8a8af00-Introduction Compressed Version.mpd";
//            videoUrl = " https://d2jx687vmgx9ce.cloudfront.net/1f63c4ee-0cc1-4b85-95b2-9ad6af426966/dash/761949e0-2ed3-4b81-8419-414a445f3fc4-1.1 Number System Intro.mpd";
//            https://d2gd7uae3xw47.cloudfront.net/large_video/dash-audio/6.8 Column Formulas.mpd
//            videoUrl = "https://d2gd7uae3xw47.cloudfront.net/large_video/hls-audio/6.8 Column Formulas.m3u8";
//            videoUrl = "https://d2gd7uae3xw47.cloudfront.net/large_video/dash-audio/6.8 Column Formulas.mpd";
//            videoUrl = "https://d3ble58c9xsklv.cloudfront.net/out/v1/b39579f9a9c24cc68b1b999e5822fac4/5b9fe6875a6e4bb1a679c02132370331/947a5178adb041bc94d043b9b1f38f32/index.mpd";
//            videoUrl = "https://d2gd7uae3xw47.cloudfront.net/subjects/4c72fee1-348c-4ac8-b79b-9afa8452d3f6/aaa2e095-6ed7-499e-94b8-6349eb6e3456/manifest.mpd";
//            videoUrl = "https://d3ble58c9xsklv.cloudfront.net/out/v1/b39579f9a9c24cc68b1b999e5822fac4/5b9fe6875a6e4bb1a679c02132370331/947a5178adb041bc94d043b9b1f38f32/index.mpd";
            //videoUrl = "https://d2jx687vmgx9ce.cloudfront.net/d3ca648e-5ecf-43c8-8294-79047a95f700/dash/df7e614f-eb5c-4ed1-8c9c-145e92f741f4-1.3.2 Addition rule for arbitrary events.mpd";
            Logger.d("VIDEO URL", videoUrl);
            Uri uri = Uri.parse(videoUrl);
            if (player == null) {

                videoPlayerBinding.playerView.setUseController(false);

//                ((SurfaceView) videoPlayerBinding.playerView.getVideoSurfaceView()).
//                        getHolder().setFixedSize(Utility.getScreenWidth(activity()),
//                        Utility.getVideoPlayerHeight(activity()));
                bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
                trackSelector = new DefaultTrackSelector(activity(), videoTrackSelectionFactory);

                videoPlayerBinding.playerView.requestFocus();
                setVideoQuality(VideoQuality.AUTO, 0, 0);
//                        Utility.getBitrate(viewModel.getVideoQualityValue()),
//                        viewModel.getVideoQualityValue());

                SimpleExoPlayer.Builder builder = new SimpleExoPlayer.Builder(activity())
                        .setBandwidthMeter(new DefaultBandwidthMeter.Builder(activity())
                                .build())
                        .setTrackSelector(trackSelector)
                        .setLoadControl(getCustomLoadController());
                player = builder.build();
                player.setHandleAudioBecomingNoisy(true);
                videoPlayerBinding.playerView.setPlayer(player);
                player.setPlayWhenReady(playWhenReady);

                previousSeek = true;
                videoSetup(uri, securityLevel);
            } else {
                videoSetup(uri, securityLevel);
            }
        }

        if (playBackSpeed != null)
            setPlaybackSpeed(playBackSpeed);


        if (player != null) {
            player.setVolume(previousVolume);
            videoPlayerBinding.imgSound.setImageResource(player.getVolume() == 0f ?
                    R.drawable.ic_volume_off : R.drawable.ic_volume_up);
        }
    }

    private DefaultLoadControl getCustomLoadController() {

        DefaultLoadControl defaultLoadControl = new DefaultLoadControl.Builder()
                .setBufferDurationsMs(
                        2 * 60 * 1000, // this is it!
                        7 * 60 * 1000,
                        DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_MS,
                        DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_AFTER_REBUFFER_MS
                )
                .setBackBuffer(2 * 60 * 1000, false)
                .createDefaultLoadControl();

        return defaultLoadControl;
    }

    public void updateUiForRefreshToken(int value, BaseViewModel viewModel) {

        switch (value) {

            case -1: // ERROR
                break;

            case 0: // PROCESSING TOKEN
                break;

            case 1: // TOKEN REFRESHED
                break;

            case 2: // UNAUTHORIZED
                ((BaseActivity) activity()).doLogout(viewModel);
                break;

            default:
                break;
        }
    }

    @AddTrace(name = "videoSetUp", enabled = true)
    private void videoSetup(Uri uri, String securityLevel) {
        MediaSource mediaSource = buildMediaSource(uri, securityLevel);
        if (fromOffline)
            setVideoQuality(VideoQuality.AUTO, 0, 0);
        if (mediaSource == null) {
            player.release();
            Utility.showToast(activity(), "unsupported video format. try another one", false);
            return;
        }
        player.prepare(mediaSource);
        player.seekTo(currentWindow, fromOffline && !viewModel.isNetworkAvailable() ? 0 : playbackPosition);
        player.setPlayWhenReady(true);

        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (unitProviderFragment != null) {
                    applySeekBar(unitProviderFragment.getSeekBar());
                    Logger.d("Updated Video Stream", "" + player.getContentPosition());
                }
                applySeekBar(videoPlayerBinding.videoProgressBar);
                switch (playbackState) {
                    case Player.STATE_IDLE:
                        videoPlayerBinding.controllerLayout.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            videoPlayerBinding.playerView.setShowBuffering(Math.toIntExact(player.getCurrentPosition() * 100 / player.getDuration()));
                        }
                        break;
                    case Player.STATE_BUFFERING:
                        isBuffering = true;
                        videoPlayerBinding.progressBar.setVisibility(View.VISIBLE);
                        videoPlayerBinding.imgPlay.setVisibility(View.INVISIBLE);
                        //hideCustomController();
                        break;
                    case Player.STATE_READY:
                        if (videoPlayerBinding.imgPlay.getVisibility() != View.VISIBLE) {
                            videoPlayerBinding.imgPlay.setVisibility(View.VISIBLE);
                        }

                        autoPlayIn5Seconds(false);
                        isBuffering = false;
                        videoPlayerBinding.controllerLayout.setBackgroundColor(ContextCompat.getColor(activity(), R.color.controllers_background));
                        videoPlayerBinding.llAutoPlay.setVisibility(View.INVISIBLE);
                        videoPlayerBinding.progressBar.setVisibility(View.INVISIBLE);
                        videoPlayerBinding.imgFullScreen.setVisibility(View.VISIBLE);
                        if (activity() instanceof HelperActivity) {
                            videoPlayerBinding.llSwipeDown.setVisibility(View.GONE);
                            videoPlayerBinding.videoSettings.setVisibility(View.VISIBLE);
                            videoPlayerBinding.imgFullScreen.setVisibility(View.GONE);
                            videoPlayerBinding.imgClose.setVisibility(View.VISIBLE);
                            showCustomSeekBar();
                        } else {
                            onTouch = true;
                            if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                                videoPlayerBinding.imgClose.setVisibility(View.VISIBLE);
                                videoPlayerBinding.llFooter.setVisibility(View.VISIBLE);
                                videoPlayerBinding.imgFullScreen.setImageResource(R.drawable.ic_fullscreen_exit_black_24dp);
                            } else {
                                videoPlayerBinding.imgClose.setVisibility(View.GONE);
                                videoPlayerBinding.llFooter.setVisibility(View.GONE);
                                videoPlayerBinding.imgFullScreen.setImageResource(R.drawable.ic_fullscreen_black_24dp);
                            }
                        }
                        setPlayControllers();

                        if (updateHandler == null) {
                            updateVideoDetails();
                        }

                        if (unitProviderFragment != null)
                            unitProviderFragment.setControls();
                        if (player.getPlayWhenReady()) {
                            if (mAudioManager != null) {
                                mAudioManager.requestAudioFocus(focusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                                mAudioManager.requestAudioFocus(focusChangeListener, AudioManager.STREAM_SYSTEM, AudioManager.AUDIOFOCUS_GAIN);
                            }
                        }
                        break;
                    case Player.STATE_ENDED:
                        if (videoPlayerBinding.imgPlay.getVisibility() != View.VISIBLE) {
                            videoPlayerBinding.imgPlay.setVisibility(View.VISIBLE);
                        }
                        videoPlayerBinding.progressBar.setVisibility(View.INVISIBLE);
                        videoPlayerBinding.tvTimeLimit.setText(getTimeFromMillis(player.getDuration()) + " / " + getTimeFromMillis(player.getDuration()));
                        isVideoEnded = true;
                        if (activity() instanceof MainActivity && ShareDataManager.getInstance().getHasNext()) {
                            videoPlayerBinding.llAutoPlay.setVisibility(View.VISIBLE);
                            autoPlayIn5Seconds(true);
                        }
                        onVideoEnded(isVideoEnded);
                        updateVideoStream(player.getCurrentPosition(), Player.STATE_ENDED, false);
                        break;
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                //Utility.showToast(activity(), error.getMessage(), false);
                error.printStackTrace();
                Mixpanel.addingExoPlayerError(uri.toString(), error, fromOffline);
                if (error.getMessage() == null) {
                    releasePlayer();
                    return;
                }
                Logger.d("ExoPlaybackException TYPE", "" + error.getMessage());
                if (error.getMessage().contains("Failed to get key request") || error.getMessage().contains("Crypto key not available")) {
                    if (fromOffline) {
                        HelperApplication.getInstance().getDownloadTracker().deleteDownload(uri);
                        ShareDataManager.getInstance().getDashUrlsModel().setLicense(null);
                        viewModel.deleteOfflineLicense(uri.toString());
                        releasePlayer();
                        initializePlayer(uri.toString(), false, null);
                    } else {
                        ShareDataManager.getInstance().getDashUrlsModel().setLicense(null);
                        releasePlayer();
                        initializePlayer(uri.toString(), false, null);
                    }
                    return;
                }
                if (error.getMessage().contains("Decoder init failed:")) {
                    releasePlayer();
                    initializePlayer(uri.toString(), false, null);
                    return;
                }
                if (error.getMessage().contains("Failed to restore keys: DRM vendor-defined error") && fromOffline) {
                    releasePlayer();
                    initializePlayer(uri.toString(), false, "");
                    return;
                }
                if (error.getMessage().toLowerCase().contains("failed to restore keys") && fromOffline) {
                    releasePlayer();
                    return;
                }
                if (error.getMessage().toLowerCase().contains("failed to set property")) {
                    releasePlayer();
                    viewModel.setEmptySecurityLevel();
                    initializePlayer(uri.toString(), false, "");
                    return;
                }
                //videoPlayerBinding.progressBar.setVisibility(View.VISIBLE);
                videoPlayerBinding.controllerLayout.setVisibility(View.INVISIBLE);
                playbackPosition = player.getCurrentPosition();
                currentWindow = player.getCurrentWindowIndex();
                state = player.getPlayWhenReady();
                new Handler().postDelayed(() -> {
                    if (!Utility.isNetworkAvailable(activity())) {
                        videoPlayerBinding.progressBar.setVisibility(View.INVISIBLE);
                        videoPlayerBinding.networkErrorMsg.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                                RelativeLayout.LayoutParams.MATCH_PARENT));
                        videoPlayerBinding.networkErrorMsg.setVisibility(View.VISIBLE);
                    }
                }, 1000);
            }


            @Override
            public void onLoadingChanged(boolean isLoading) {
                if (isLoading) {
                    if (isPlaying()) {
                        isBuffering = false;
                        videoPlayerBinding.progressBar.setVisibility(View.INVISIBLE);
                    } else {
                        isBuffering = isLoading;
                        videoPlayerBinding.progressBar.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        if (fromOffline) {
//            player.addAnalyticsListener(new EventLogger(trackSelector));
//            player.addAnalyticsListener(new PushEventLogger(trackSelector));
            player.addAnalyticsListener(new AnalyticsListener() {
                @Override
                public void onTracksChanged(EventTime eventTime, TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                    MappingTrackSelector.MappedTrackInfo mappedTrackInfo =
                            trackSelector != null ? trackSelector.getCurrentMappedTrackInfo() : null;
                    if (mappedTrackInfo == null) {
                        return;
                    }
                    try {
                        for (int i = 0; i < mappedTrackInfo.getTrackGroups(0).length; i++) {
                            TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(i);
                            for (int j = 0; j < trackGroupArray.get(i).length; j++) {
                                Logger.d("Download Resolution ", "" + trackGroupArray.get(i).getFormat(j).height);
                                videoPlayerBinding.tvQuality.setText("" + trackGroupArray.get(i).getFormat(j).height);
                                return;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });


        } else {
//            player.addAnalyticsListener(new PushEventLogger(trackSelector));
            player.addAnalyticsListener(new DeepLearnEventLogger(trackSelector));
        }

    }

    public void onVideoEnded(boolean state) {

        try {

            if (state) {
                if (autoHide != null)
                    videoPlayerBinding.controllerLayout.removeCallbacks(autoHide);

                videoPlayerBinding.llSwipeDown.setVisibility(View.INVISIBLE);
                videoPlayerBinding.imgForward.setVisibility(View.INVISIBLE);
                videoPlayerBinding.imgBackward.setVisibility(View.INVISIBLE);
                if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                    videoPlayerBinding.imgPrevious.setVisibility(View.GONE);
                    videoPlayerBinding.imgNext.setVisibility(View.GONE);
                } else {
                    videoPlayerBinding.imgPrevious.setVisibility(View.VISIBLE);
                    videoPlayerBinding.imgNext.setVisibility(View.VISIBLE);
                }
                if (activity() instanceof HelperActivity) {
                    videoPlayerBinding.controllerLayout.setVisibility(View.VISIBLE);
                    videoPlayerBinding.imgFullScreen.setVisibility(View.GONE);
                }
                videoPlayerBinding.tvTimeLimit.setVisibility(View.VISIBLE);
                videoPlayerBinding.videoSettings.setVisibility(View.VISIBLE);
                videoPlayerBinding.imgPlay.setImageResource(R.drawable.ic_replay_black);
                if (unitProviderFragment != null) {
                    if (unitProviderFragment.binding.unitProviderMotion.getCurrentState() == R.id.collapsed) {
                        hideCustomController();
                    } else {
                        showCustomController();
                    }
                } else
                    showCustomController();
                videoPlayerBinding.controllerLayout.setBackgroundColor(ContextCompat.getColor(activity(), R.color.black));

            } else {

                if (activity() instanceof MainActivity) {
                    autoPlayIn5Seconds(false);
                }
                videoPlayerBinding.controllerLayout.setVisibility(View.INVISIBLE);
                videoPlayerBinding.llSwipeDown.setVisibility(View.VISIBLE);
                videoPlayerBinding.imgForward.setVisibility(View.VISIBLE);
                videoPlayerBinding.imgBackward.setVisibility(View.VISIBLE);
                if (activity() instanceof MainActivity)
                    videoPlayerBinding.imgFullScreen.setVisibility(View.VISIBLE);
                videoPlayerBinding.tvTimeLimit.setVisibility(View.VISIBLE);
                videoPlayerBinding.videoSettings.setVisibility(View.VISIBLE);
                videoPlayerBinding.imgPlay.setImageResource(R.drawable.ic_pause_black);

                if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ||
                        activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                    videoPlayerBinding.imgPrevious.setVisibility(View.GONE); // RECHANGE
                    videoPlayerBinding.imgNext.setVisibility(View.GONE); // RECHANGE
                    showCustomSeekBar();
                } else {
                    videoPlayerBinding.imgPrevious.setVisibility(View.VISIBLE); // RECHANGE
                    videoPlayerBinding.imgNext.setVisibility(View.VISIBLE); // RECHANGE
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private MediaSource buildMediaSource(Uri uri, String securityLevel) {

        DownloadRequest downloadRequest = ((HelperApplication) getActivity().getApplication()).getDownloadTracker().getDownloadRequest(uri);
        if (downloadRequest != null) {
            if (!viewModel.isNetworkAvailable()) {
                fromOffline = true;
                return DownloadHelper.createMediaSource(downloadRequest, dataSourceFactory,
                        viewModel.generateDRMManager(Constants.APP_NAME, uri, securityLevel));
            } else {
                if (((HelperApplication) getActivity().getApplication()).getDownloadTracker().getDownload(uri).state == Download.STATE_COMPLETED) {
                    fromOffline = true;
                    return DownloadHelper.createMediaSource(downloadRequest, dataSourceFactory,
                            viewModel.generateDRMManager(Constants.APP_NAME, uri, securityLevel));

                }
            }
        }

        fromOffline = false;
        int type = Util.inferContentType(uri);
        Log.d(VideoPlayerFragment.this.getClass().getName(), String.valueOf(type));
        switch (type) {
            case C.TYPE_DASH:
                DashMediaSource.Factory dashFactory = new DashMediaSource.Factory(dataSourceFactory)
                        .setDrmSessionManager(viewModel.generateDRMManager(Constants.APP_NAME, uri, securityLevel));

                if (viewModel.getDashManifest(uri) == null) {
                    return dashFactory.createMediaSource(uri);
                }
                return dashFactory.createMediaSource(viewModel.getDashManifest(uri));
            case C.TYPE_SS:
                return new SsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw null;
        }
    }


    private DataSource.Factory buildDataSourceFactory() {
        return ((HelperApplication) getActivity().getApplication()).buildDataSourceFactory();
    }


    public void playVideo(String videoUrl) {
        if (videoUrl == null)
            return;

        this.videoUrl = videoUrl;
        initializePlayer(videoUrl, false, null);
    }


    public void playVideo() {
        if (player != null) {
            player.setPlayWhenReady(true);
        }
    }

    public void pauseVideo() {
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    private void videoForward(boolean animate) {
        if (videoPlayerBinding != null && videoPlayerBinding.networkErrorMsg.getVisibility() != View.VISIBLE
                && !videoPlayerBinding.tvTimeLimit.getText().equals("00:00 / 00:00")
                && !isVideoEnded) {
            if (player != null) {
                long contentPosition = player.getContentPosition();
                player.seekTo(contentPosition + 10000 >= player.getDuration() ? player.getDuration() : contentPosition + 10000);

                if (animate) {
                    videoPlayerBinding.imgAction.setImageResource(R.drawable.ic_forward);
                    ValueAnimator fadeAnim1 = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "alpha", 1f, 0.75f);
                    ValueAnimator fadeAnim2 = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "alpha", 0.75f, 0f);
                    fadeAnim1.setDuration(250);
                    fadeAnim2.setDuration(250);


                    ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "scaleX", 0.5f, 1);
                    ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "scaleY", 0.5f, 1);
                    scaleDownX.setDuration(200);
                    scaleDownY.setDuration(200);

                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.playTogether(scaleDownX, scaleDownY);
                    animatorSet.playSequentially(fadeAnim1, fadeAnim2);
                    animatorSet.start();
                }

            }
        }
    }

    private void videoBackward(boolean animate) {
        if (videoPlayerBinding != null && videoPlayerBinding.networkErrorMsg.getVisibility() != View.VISIBLE
                && !videoPlayerBinding.tvTimeLimit.getText().equals("00:00 / 00:00")
                && !isVideoEnded) {
            if (player != null) {
                long contentPosition = player.getContentPosition();
                player.seekTo(contentPosition - 10000 <= 0 ? 0 : contentPosition - 10000);

                if (animate) {
                    videoPlayerBinding.imgAction.setImageResource(R.drawable.ic_backward);

                    ValueAnimator fadeAnim1 = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "alpha", 1f, 0.75f);
                    ValueAnimator fadeAnim2 = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "alpha", 0.75f, 0f);
                    fadeAnim1.setDuration(250);
                    fadeAnim2.setDuration(250);

                    ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "scaleX", 0.5f, 1);
                    ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(videoPlayerBinding.imgAction, "scaleY", 0.5f, 1);
                    scaleDownX.setDuration(200);
                    scaleDownY.setDuration(200);

                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.playTogether(scaleDownX, scaleDownY);
                    animatorSet.playSequentially(fadeAnim1, fadeAnim2);
                    animatorSet.start();
                }
            }
        }
    }

    public boolean isPlaying() {
        if (player != null)
            return player.getPlaybackState() == Player.STATE_READY && player.getPlayWhenReady();
        else
            return false;

    }

    public void releasePlayer() {
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }

    private void applySeekBar(SeekBar seekBar) {
        if (player != null) {
            seekBar.setMax((int) (player.getDuration() / 1000));
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (player != null && fromUser) {
                    showCustomController();
                    player.seekTo(progress * 1000);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                if (isVideoEnded) {
                    isVideoEnded = false;
                    onVideoEnded(isVideoEnded);
                }

                seekBar.getThumb().mutate().setAlpha(255);
                if (isBuffering) {
                    hideCustomController();
                }

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (isVideoEnded) {
                    isVideoEnded = false;
                    onVideoEnded(isVideoEnded);
                }
                if (isBuffering) {
                    hideCustomController();
                    seekBar.getThumb().mutate().setAlpha(0);
                }
            }
        });

    }


    public void inVideoSettings() {

        BSDVideoActionFragment bsdAskDoubtFragment =
                BSDVideoActionFragment.newInstance(playBackSpeed, this, videoQuality, this);
        bsdAskDoubtFragment.show(getParentFragmentManager(),
                BSDVideoActionFragment.TAG);
    }


    @Override
    public void setPlaybackSpeed(PlayBackSpeed playbackSpeed) {
        this.playBackSpeed = playbackSpeed;
        if (player != null) {
            PlaybackParameters params = null;

            switch (playbackSpeed) {
                case SPp25X:
                    params = new PlaybackParameters(0.25f);

                    break;

                case SPp5X:
                    params = new PlaybackParameters(0.5f);

                    break;

                case SPp75X:
                    params = new PlaybackParameters(0.75f);

                    break;

                case SP1X:
                    params = new PlaybackParameters(1f);

                    break;

                case SP1p25X:
                    params = new PlaybackParameters(1.25f);

                    break;

                case SP1p5X:
                    params = new PlaybackParameters(1.5f);

                    break;

                case SP1p75X:
                    params = new PlaybackParameters(1.75f);
                    break;

                case SP2X:
                    params = new PlaybackParameters(2f);
                    break;
            }

            if (params != null)
                player.setPlaybackParameters(params);
        }

    }

    @Override
    public void updateVideoFocus() {

    }

    @Override
    public void setVideoQuality(VideoQuality videoQuality, int bitrate, int resolution) {
        Log.d(TAG, "setVideoQuality: " + videoQuality + " bitrate " + " resolution " + resolution);
        this.videoQuality = videoQuality;
        this.currentBitrate = bitrate;
        if (trackSelector == null) {
            Log.d(TAG, "setVideoQuality: trackSelector == null");
            return;
        }else {
            Log.d(TAG, "setVideoQuality: trackSelector != null");
        }
        if (bitrate == 0) {
            Log.d(TAG, "setVideoQuality: bitrate == 0");
            resolution = viewModel.getAutoResolution();
        }else {
            Log.d(TAG, "setVideoQuality: bitrate != 0");
        }
        if (bitrate > 0) {
            Log.d(TAG, "setVideoQuality: bitrate > 0 if condition");
            if (fromOffline) {
                Log.d(TAG, "setVideoQuality: bitrate > 0 fromOffline If condition");
                showToast("Playing from offline", false);
                return;
            }else {
                Log.d(TAG, "setVideoQuality: bitrate > 0 fromOffline else condition");
                DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
                        .clearSelectionOverrides()
                        .clearVideoSizeConstraints()
                        .clearViewportSizeConstraints()
                        .build();
                trackSelector.setParameters(parameters);
                parameters = trackSelector.buildUponParameters()
//                    .setForceHighestSupportedBitrate(true)
                        .setMaxVideoSize(resolution * 16 / 9, resolution)
                        .build();
                trackSelector.setParameters(parameters);
            }
//            trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoBitrate(model.getBitrate()).setMaxVideoSize(model.getWidth(), videoQualityList.get(positionInList).getHeight()));
            Log.d(TAG, "setVideoQuality: bitrate > 0 outer condition");
            videoPlayerBinding.tvQuality.setText("" + resolution);
        } else {
            Log.d(TAG, "setVideoQuality: bitrate > 0 else condition");
            if (fromOffline) {
                Log.d(TAG, "setVideoQuality: bitrate > 0 else condition fromOffline if condition");
                DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
                        .build();
                trackSelector.setParameters(parameters);
                return;
            } else {
                Log.d(TAG, "setVideoQuality: bitrate > 0 else condition fromOffline else condition");
//                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
//                trackSelector = new DefaultTrackSelector(activity(), videoTrackSelectionFactory);
//                videoPlayerBinding.playerView.requestFocus();
                DefaultTrackSelector.Parameters parameters = trackSelector.buildUponParameters()
                        .clearSelectionOverrides()
                        .clearVideoSizeConstraints()
                        .clearViewportSizeConstraints()
                        .build();
                trackSelector.setParameters(parameters);
//                TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
//                trackSelector = new DefaultTrackSelector(activity(), videoTrackSelectionFactory);
            }
//            videoPlayerBinding.tvQuality.setText("" + resolution);
        }
    }
    public void setPlayControllers() {
        if (isPlaying()) {
            videoPlayerBinding.imgPlay.setImageResource(R.drawable.ic_pause_black);
        } else {
            videoPlayerBinding.imgPlay.setImageResource(R.drawable.ic_play_arrow_black);
        }

        if (unitProviderFragment != null) {
            if (isPlaying())
                unitProviderFragment.setPauseIcon();
            else
                unitProviderFragment.setPlayIcon();
        }
    }


    public String getTimeFromMillis(long millis) {
        if (millis <= 0 || player == null) {
            return "00:00";
        }
        if (millis >= 60 * 60 * 1000 || player.getDuration() >= 60 * 60 * 1000) {
            return String.format(TimeUnit.MILLISECONDS.toHours(millis) > 99 ? "%d:%02d:%02d" : "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
        }

        return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
    }

    private AudioManager.OnAudioFocusChangeListener focusChangeListener =
            focusChange ->
            {
                switch (focusChange) {
                    case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):

                        break;
                    case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):

                        break;

                    case (AudioManager.AUDIOFOCUS_LOSS):
                        //for pause
                        pauseVideo();
                        break;

                    case (AudioManager.AUDIOFOCUS_GAIN):
                        //for play
                        playVideo();
                        break;

                }
            };


    private PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                Logger.d("RINGING", "RINGING");
                //Incoming call
                pauseVideo();
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                //Not in call
                //playVideo();
                Logger.d("RINGING", "IDLE");
                if (isPlaying())
                    pauseVideo();
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                //A call is dialing, active or on hold
                pauseVideo();
                Logger.d("RINGING", "OFFHOOK");
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                //A call is dialing, active or on hold
                pauseVideo();
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    };


    private void whenOnline() {
        Log.d(TAG, "whenOnline: videoQuality " + videoQuality);
        if (videoQuality == VideoQuality.AUTO) {
//            if (!fromOffline)
//                setVideoQuality(VideoQuality.AUTO, 0, 0);
            Log.d(TAG, "whenOnline: videoQuality " + videoQuality + " VideoQuality.AUTO If condition");
        } else {
            Log.d(TAG, "whenOnline: videoQuality " + videoQuality + " VideoQuality.AUTO Else condition");
        }
        if (player != null && videoPlayerBinding.networkErrorMsg.getVisibility() == View.VISIBLE) {
            videoPlayerBinding.networkErrorMsg.setVisibility(View.GONE);
            player.seekTo(currentWindow, playbackPosition);
            showCustomController();
            player.setPlayWhenReady(state);
            player.retry();
            Log.d(TAG, "whenOnline: videoQuality " + videoQuality + " VideoQuality.AUTO If networkErrorMsg condition");
        } else {
            Log.d(TAG, "whenOnline: videoQuality " + videoQuality + " VideoQuality.AUTO Else networkErrorMsg condition");
        }
    }

    public void whenOffline() {
        if (isPlaying()) {
            activity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        playbackPosition = player.getContentPosition();
                        currentWindow = player.getCurrentWindowIndex();
                    } catch (Exception e) {
                        Logger.d("" + e);
                    }
                    new Handler().postDelayed(this, 500);
                }
            });

        }
    }

    @Override
    public void applyAction() {
        if (player == null)
            return;

        if (isPlaying()) {
            pauseVideo();
        } else {
            playVideo();
        }
        if (!isTouchEnable && !isBuffering)
            if (videoPlayerBinding != null)
                showCustomController();
    }

    @SuppressLint("SourceLockedOrientationActivity")
    public void controlScreenOrientation(int orientation) {
        activity().setRequestedOrientation((activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ||
                activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE)
                ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                : (android.provider.Settings.System.getInt(activity().getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0) == 1
                ? getScreenOrientation(orientation)
                : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE));

        if (unitProviderFragment != null) {
            if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {

                adjustControllers(true);
                videoPlayerBinding.imgNext.setVisibility(View.GONE);
                videoPlayerBinding.imgPrevious.setVisibility(View.GONE);
                videoPlayerBinding.llFooter.setVisibility(View.VISIBLE);
                videoPlayerBinding.llSwipeDown.setVisibility(View.GONE);
                videoPlayerBinding.imgClose.setVisibility(View.VISIBLE);
                unitProviderFragment.enableFullScreen();
                showCustomSeekBar();


                videoPlayerBinding.imgFullScreen.setImageResource(R.drawable.ic_fullscreen_exit_black_24dp);

                if (isVideoEnded)
                    onVideoEnded(true);


            } else {

                adjustControllers(false);
                videoPlayerBinding.imgNext.setVisibility(View.VISIBLE); // RECHANGE
                videoPlayerBinding.imgPrevious.setVisibility(View.VISIBLE); // RECHANGE
                activity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                videoPlayerBinding.llFooter.setVisibility(View.GONE);
                videoPlayerBinding.llSwipeDown.setVisibility(View.VISIBLE);
                videoPlayerBinding.imgClose.setVisibility(View.GONE);
                unitProviderFragment.disableFullScreen();
                hideCustomSeekBar();

                videoPlayerBinding.imgFullScreen.setImageResource(R.drawable.ic_fullscreen_black_24dp);


                if (isVideoEnded)
                    onVideoEnded(true);
            }
        }

        if (videoPlayerBinding.controllerLayout.getVisibility() == View.VISIBLE) {
            showCustomController();
        }

        decorateScreenUi();
    }

    public int getScreenOrientation(int orientation) {
        if (orientation == -1) {
            return ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }
        return ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
    }

    private void decorateScreenUi() {
        if (activity() == null) {
            return;
        }
        if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                View decorView = activity().getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                View decorView = activity().getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        }


    }


    public void inTouchMode(boolean inTouch) {
        isTouchEnable = inTouch;
        if (videoPlayerBinding == null)
            return;


        if (inTouch) {
            //do nothing
            videoPlayerBinding.playerView.setOnTouchListener((v, event) -> false);
            if (unitProviderFragment != null) {
                unitProviderFragment.hideSeekThumb();
            }
            videoPlayerBinding.controllerLayout.clearAnimation();
            videoPlayerBinding.controllerLayout.setVisibility(View.GONE);
            onTouch = true;
        } else {

            videoPlayerBinding.playerView.setOnTouchListener((view, motionEvent) ->
            {
                if (activity() != null) {
                    if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                        gestureDetector.onTouchEvent(motionEvent);
                        scaleGestureDetector.onTouchEvent(motionEvent);
                        return true;
                    } else {
                        return false;
                    }
                } else
                    return tapAction();


            });
        }
    }

    public void controlNetwork(boolean collapse) {
        if (videoPlayerBinding != null && videoPlayerBinding.networkErrorMsg != null)
            videoPlayerBinding.networkErrorMsg.setAlpha(collapse ? 0 : 1);
    }


    public class MediaReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                if (isPlaying()) {
                    pauseVideo();
                    showCustomController();
                }
            }
        }
    }


    public class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if (Utility.isNetworkAvailable(context)) {
                whenOnline();
            }
        }

    }


    private void registerMediaReceiver() {
        try {
            if (mediaReceiver != null)
                activity().registerReceiver(mediaReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unRegisterMediaReceiver() {
        try {
            LocalBroadcastManager.getInstance(activity()).unregisterReceiver(mediaReceiver);
            activity().unregisterReceiver(mediaReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerNetworkBroadCast() {
        try {
            if (networkReceiver != null) {
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
                activity().registerReceiver(networkReceiver, intentFilter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void unregisterNetworkBroadCast() {
        try {
            if (networkReceiver != null) {
                LocalBroadcastManager.getInstance(activity()).unregisterReceiver(networkReceiver);
                activity().unregisterReceiver(networkReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void controlReceivers(boolean allow) {
        if (allow) {
            registerMediaReceiver();
            registerNetworkBroadCast();
            if (mAudioManager != null)
                mAudioManager.registerMediaButtonEventReceiver(mRemoteControlResponder);

        } else {
            unRegisterMediaReceiver();
            unregisterNetworkBroadCast();
            if (mAudioManager != null)
                mAudioManager.unregisterMediaButtonEventReceiver(mRemoteControlResponder);

        }
    }

    public PlayerView getPlayerView() {
        return videoPlayerBinding == null ? null : videoPlayerBinding.playerView;
    }

    public SimpleExoPlayer getExoPlayer() {
        return player;
    }

    public void clearHandlers() {
        if (updateHandler != null) {
            updateHandler.removeCallbacksAndMessages(null);
        }
        updateHandler = null;
    }


    private void autoPlayIn5Seconds(boolean enable) {
        if (!enable) {
            videoPlayerBinding.llAutoPlay.setVisibility(View.GONE);
            if (autoPlayHandler != null) {
                autoPlayHandler.removeCallbacksAndMessages(null);
            }
            return;
        }

        if (autoPlayHandler != null) {
            autoPlayHandler.removeCallbacksAndMessages(null);
        }
        autoPlayHandler = new Handler();
        counter = 5;
        videoPlayerBinding.progressBarCounter.setProgress(0);
        videoPlayerBinding.tvCounter.setText("" + counter);
        autoPlayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Logger.d("RUNNING", "AUTO PLAY");
                if (counter <= 1) {
                    if (autoPlayHandler != null)
                        autoPlayHandler.removeCallbacksAndMessages(null);

                    videoPlayerBinding.llAutoPlay.setVisibility(View.GONE);
                    if (player != null && player.getPlaybackState() == Player.STATE_ENDED) {
                        if (unitProviderFragment != null)
                            unitProviderFragment.autoPlayNext();
                    }
                    counter = 5;
                    return;
                }
                counter--;
                videoPlayerBinding.progressBarCounter.setProgress(5 - counter);
                videoPlayerBinding.tvCounter.setText("" + counter);
                if (autoPlayHandler != null)
                    autoPlayHandler.postDelayed(this, 1000);
            }
        }, 1000);

    }

    public void updateAsView(boolean fullScreen) {
        if (videoPlayerBinding == null)
            return;

        if (fullScreen && unitProviderFragment != null) {
            videoPlayerBinding.llFooter.setVisibility(View.VISIBLE);
            videoPlayerBinding.llSwipeDown.setVisibility(View.GONE);
            videoPlayerBinding.imgClose.setVisibility(View.VISIBLE);
            videoPlayerBinding.imgNext.setVisibility(View.GONE); // RECHANGE
            videoPlayerBinding.imgPrevious.setVisibility(View.GONE); // RECHANGE
            if (unitProviderFragment != null)
                unitProviderFragment.enableFullScreen();
            showCustomSeekBar();

            videoPlayerBinding.imgFullScreen.setImageResource(R.drawable.ic_fullscreen_exit_black_24dp);

            adjustControllers(true);
        } else {
            videoPlayerBinding.llSwipeDown.setVisibility(View.VISIBLE);
            videoPlayerBinding.imgForward.setVisibility(View.VISIBLE);
            videoPlayerBinding.imgBackward.setVisibility(View.VISIBLE);
            videoPlayerBinding.imgNext.setVisibility(View.VISIBLE); // RE-CHANGE
            videoPlayerBinding.imgPrevious.setVisibility(View.VISIBLE); // RE-CHANGE

            adjustControllers(false);
        }
    }

    private void adjustControllers(boolean fullScreen) {
        if (fullScreen) {
            videoPlayerBinding.controllerLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

            LinearLayout.MarginLayoutParams llBottomParams = (LinearLayout.MarginLayoutParams) videoPlayerBinding.llBottomLayout.getLayoutParams();
            llBottomParams.leftMargin = Utility.toSdp(R.dimen._15sdp);
            llBottomParams.rightMargin = Utility.toSdp(R.dimen._15sdp);
            llBottomParams.bottomMargin = Utility.toSdp(R.dimen._11sdp);
            videoPlayerBinding.llBottomLayout.setLayoutParams(llBottomParams);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._80sdp), Utility.toSdp(R.dimen._80sdp));
            params.setMargins(Utility.toSdp(R.dimen._60sdp), 0, Utility.toSdp(R.dimen._60sdp), 0);
            videoPlayerBinding.imgPlay.setLayoutParams(params);
            videoPlayerBinding.imgAction.setLayoutParams(params);

            videoPlayerBinding.imgPlay.setVisibility(isBuffering ? View.INVISIBLE : View.VISIBLE);

            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._70sdp), Utility.toSdp(R.dimen._70sdp));
            videoPlayerBinding.imgBackward.setLayoutParams(params1);
            videoPlayerBinding.imgForward.setLayoutParams(params1);
            videoPlayerBinding.imgBackward.setVisibility(View.VISIBLE);
            videoPlayerBinding.imgForward.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(Utility.toSdp(R.dimen._35sdp), Utility.toSdp(R.dimen._35sdp));
            videoPlayerBinding.videoSettings.setLayoutParams(params2);

            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._35sdp), Utility.toSdp(R.dimen._35sdp));
            videoPlayerBinding.imgFullScreen.setLayoutParams(params3);
            videoPlayerBinding.imgSound.setLayoutParams(params3);

            RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params4.addRule(RelativeLayout.CENTER_VERTICAL);
            params4.setMargins(Utility.toSdp(R.dimen._15sdp), Utility.toSdp(R.dimen._15sdp), Utility.toSdp(R.dimen._15sdp), Utility.toSdp(R.dimen._15sdp));
            videoPlayerBinding.llAutoPlay.setLayoutParams(params4);

            RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(Utility.toSdp(R.dimen._85sdp), R.dimen._85sdp);
            params5.addRule(RelativeLayout.CENTER_IN_PARENT);
            params5.addRule(RelativeLayout.CENTER_VERTICAL);
            params5.addRule(RelativeLayout.CENTER_HORIZONTAL);
            videoPlayerBinding.progressBar.setLayoutParams(params5);


            RelativeLayout.LayoutParams paramsQuality = new RelativeLayout.LayoutParams(Utility.toSdp(R.dimen._25sdp), RelativeLayout.LayoutParams.WRAP_CONTENT);
            paramsQuality.setMargins(Utility.toSdp(R.dimen._18sdp), Utility.toSdp(R.dimen._5sdp), 0, 0);
            videoPlayerBinding.tvQuality.setLayoutParams(paramsQuality);

        } else {


            videoPlayerBinding.controllerLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

            LinearLayout.MarginLayoutParams llBottomParams = (LinearLayout.MarginLayoutParams) videoPlayerBinding.llBottomLayout.getLayoutParams();
            llBottomParams.leftMargin = 0;
            llBottomParams.rightMargin = Utility.toSdp(R.dimen._5sdp);
            llBottomParams.bottomMargin = Utility.toSdp(R.dimen._7sdp);
            videoPlayerBinding.llBottomLayout.setLayoutParams(llBottomParams);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._50sdp), Utility.toSdp(R.dimen._50sdp));
            videoPlayerBinding.imgPlay.setLayoutParams(params);
            videoPlayerBinding.imgAction.setLayoutParams(params);

            videoPlayerBinding.imgPlay.setVisibility(isBuffering ? View.INVISIBLE : View.VISIBLE);

            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._50sdp), Utility.toSdp(R.dimen._50sdp));
            videoPlayerBinding.imgBackward.setLayoutParams(params1);
            videoPlayerBinding.imgForward.setLayoutParams(params1);
            videoPlayerBinding.imgBackward.setVisibility(View.VISIBLE);
            videoPlayerBinding.imgForward.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(Utility.toSdp(R.dimen._30sdp), Utility.toSdp(R.dimen._30sdp));
            videoPlayerBinding.videoSettings.setLayoutParams(params2);

            LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(Utility.toSdp(R.dimen._30sdp), Utility.toSdp(R.dimen._30sdp));
            videoPlayerBinding.imgFullScreen.setLayoutParams(params3);
            videoPlayerBinding.imgSound.setLayoutParams(params3);

            RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params4.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params4.setMargins(Utility.toSdp(R.dimen._10sdp), Utility.toSdp(R.dimen._10sdp), Utility.toSdp(R.dimen._10sdp), Utility.toSdp(R.dimen._10sdp));
            videoPlayerBinding.llAutoPlay.setLayoutParams(params4);

            RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(Utility.toSdp(R.dimen._80sdp), R.dimen._80sdp);
            params5.addRule(RelativeLayout.CENTER_IN_PARENT);
            params5.addRule(RelativeLayout.CENTER_VERTICAL);
            params5.addRule(RelativeLayout.CENTER_HORIZONTAL);
            videoPlayerBinding.progressBar.setLayoutParams(params5);

            RelativeLayout.LayoutParams paramsQuality = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            paramsQuality.setMargins(Utility.toSdp(R.dimen._15sdp), Utility.toSdp(R.dimen._5sdp), 0, 0);
            videoPlayerBinding.tvQuality.setLayoutParams(paramsQuality);

        }
    }


    private boolean tapAction() {
        if (videoPlayerBinding == null)
            return false;

        if (player == null)
            return false;

        if (videoPlayerBinding.tvTimeLimit.getText().equals("00:00 / 00:00"))
            return false;

        if (!isBuffering) {
            if (onTouch) {
                if (videoPlayerBinding.networkErrorMsg.getVisibility() == View.VISIBLE) {
                    if (isVideoEnded) {
                        onVideoEnded(isVideoEnded);
                    } else {
                        hideCustomController();
                    }
                    onTouch = true;
                    new Handler().postDelayed(() -> {
                        if (Utility.isNetworkAvailable(activity())) {
                            Utility.showToast(activity(), "Connected to internet", false);
                            whenOnline();
                        } else {
                            if (isVideoEnded) {
                                onVideoEnded(isVideoEnded);
                            } else {
                                hideCustomController();
                            }
                            videoPlayerBinding.networkErrorMsg.setVisibility(View.VISIBLE);
                            videoPlayerBinding.progressBar.setVisibility(View.INVISIBLE);
                        }
                    }, 500);
                } else {
                    if (isVideoEnded) {
                        onVideoEnded(isVideoEnded);
                    } else {
                        if (videoPlayerBinding.controllerLayout.getVisibility() == View.VISIBLE) {
                            hideCustomController();
                        } else
                            showCustomController();
                    }
                }

            } else {
                if (isVideoEnded) {
                    onVideoEnded(isVideoEnded);
                } else {
                    hideCustomController();
                }
            }
        } else {
            if (player != null && !videoPlayerBinding.tvTimeLimit.getText().equals("00:00 / 00:00")) {
                if (isVideoEnded) {
                    onVideoEnded(isVideoEnded);
                    return false;
                }
                if (!isPlaying() && videoPlayerBinding.controllerLayout.getVisibility() == View.VISIBLE) {
                    hideCustomController();
                } else
                    showCustomController();
            }
        }
        return false;
    }

    // ZOOM SCALE

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private float zoomScale = 1;

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {

            zoomScale *= scaleGestureDetector.getScaleFactor();
            zoomScale = Math.max(1f, Math.min(zoomScale, 2.0f));
            Logger.d("SCALE", "" + zoomScale);
            getPlayerView().setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            if (zoomScale > 1.4) {
                getPlayerView().setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
                ShareDataManager.getInstance().setAspectRatio(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
            } else {
                getPlayerView().setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                ShareDataManager.getInstance().setAspectRatio(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            }
            return true;
        }
    }

    private class SingleTapGesture extends GestureDetector.SimpleOnGestureListener {

        private long previousTime;

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            previousTime = System.currentTimeMillis();
            if (activity() == null)
                return super.onDoubleTap(e);


            if (e.getX() < (Utility.getScreenWidth(activity()) / 2) - 250) {
                videoBackward(true);
            } else if ((Utility.getScreenWidth(activity()) / 2 + 250) < e.getX()) {
                videoForward(true);
            } else {

            }
            return super.onDoubleTap(e);
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (previousTime >= System.currentTimeMillis() - 800) {
                return false;
            }
            return tapAction();
        }

        public void onLongPress(MotionEvent ev) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                float distanceY) {

            return false;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {

            return false;
        }
    }


}
