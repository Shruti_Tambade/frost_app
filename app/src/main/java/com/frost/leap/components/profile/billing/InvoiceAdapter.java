package com.frost.leap.components.profile.billing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemInvoiceBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.user.model.Invoice;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class InvoiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Invoice> list;
    private RController rController;
    private Context context;
    private IInvoiceAdapter iInvoiceAdapter;

    public InvoiceAdapter(List<Invoice> list, RController rController, Context context, IInvoiceAdapter iInvoiceAdapter) {
        this.list = list;
        this.rController = rController;
        this.context = context;
        this.iInvoiceAdapter = iInvoiceAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skeleton_item_invoice, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else
            return new InvoiceViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_invoice, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (rController != RController.LOADING) {
            if (viewHolder instanceof InvoiceViewHolder) {
                InvoiceViewHolder invoiceViewHolder = (InvoiceViewHolder) viewHolder;
                invoiceViewHolder.bindData(list.get(i), i);
                invoiceViewHolder.binding.imgDownload.setOnClickListener(v -> {
                    iInvoiceAdapter.downloadInvoice(list.get(i), i);
                });

                invoiceViewHolder.itemView.setOnClickListener(v -> {
                    iInvoiceAdapter.viewInvoice(list.get(i), i);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING ? 15 : list.size();
    }


    public class InvoiceViewHolder extends RecyclerView.ViewHolder {
        private ItemInvoiceBinding binding;

        public InvoiceViewHolder(ItemInvoiceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(Invoice invoice, int position) {
            binding.tvName.setText(invoice.getOrderId());
            String day = (invoice.getOrderDate().getDayOfMonth() < 10 ? "0" : "") + invoice.getOrderDate().getDayOfMonth();
            String month = (invoice.getOrderDate().getMonthValue() < 10 ? "0" : "") + invoice.getOrderDate().getMonthValue();
            binding.tvDate.setText(day + "/" + month + "/" + invoice.getOrderDate().getYear());
            binding.tvPrice.setText("₹ " + Utility.roundTwoDecimals((invoice.getAmount() - invoice.getFee()) / 100) + "/-");

            binding.imgDownload.setOnClickListener(v -> {
                iInvoiceAdapter.downloadInvoice(invoice, position);
            });
        }

    }

}
