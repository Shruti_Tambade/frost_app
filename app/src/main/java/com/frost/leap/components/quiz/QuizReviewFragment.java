package com.frost.leap.components.quiz;


import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentQuizReviewBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.quiz.QuizReviewViewModel;

import apprepos.quiz.model.QuizQuestionsModel;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link QuizReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizReviewFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String quizId;
    private FragmentQuizReviewBinding binding;
    private int secondaryProgress = 0, currentQuestionNumber = 0;
    private QuizQuestionsModel quizQuestionsModel;
    private QuizReviewViewModel viewModel;

    public QuizReviewFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static QuizReviewFragment newInstance(String quizId) {
        QuizReviewFragment fragment = new QuizReviewFragment();
        Bundle args = new Bundle();
        args.putString("StudentQuizId", quizId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(QuizReviewViewModel.class);
        if (getArguments() != null) {
            quizId = getArguments().getString("StudentQuizId");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_quiz_review;
    }

    @Override
    public void setUp() {

        binding = (FragmentQuizReviewBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        binding.ivClose.setOnClickListener(v -> {
            activity().finish();
        });

        binding.ivBackQuestion.setOnClickListener(v -> {
            binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() - 1);
        });

        binding.tvNextQuestion.setOnClickListener(v -> {

            if (binding.viewPager.getCurrentItem() + 1 == quizQuestionsModel.getQuestions().size()) {
                activity().finish();
            } else {
                binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() + 1);

            }
        });

        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int currentPosition) {

                currentQuestionNumber = currentPosition;
                binding.ivBackQuestion.setVisibility(currentPosition == 0 ? View.GONE : View.VISIBLE);
                binding.progressBar.setSecondaryProgress(currentPosition + 1 == quizQuestionsModel.getQuestions().size() ? 100 : (currentPosition + 1) * secondaryProgress);
                binding.tvNextQuestion.setText(currentPosition + 1 == quizQuestionsModel.getQuestions().size()
                        ? "Close" : "Next Question");
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case QuizReviewViewModel.NetworkTags.PREVIEW_QUIZ:
                    updateUiForPreviewQuiz(networkCall);
                    break;
            }
        });

        viewModel.updateQuiz().observe(this, quizQuestionsModel -> {
            this.quizQuestionsModel = quizQuestionsModel;
            binding.spnLoader.setVisibility(View.GONE);
            if (quizQuestionsModel.getQuestions() != null && quizQuestionsModel.getQuestions().size() > 0) {
                binding.tvQuizType.setText("QuizConstants . " + Utility.getCamelCase(quizQuestionsModel.getQuizName()));
                binding.viewPager.setOffscreenPageLimit(quizQuestionsModel.getQuestions().size());
                binding.viewPager.setPagingEnabled(false);


                binding.viewPager.setAdapter(new QuizViewPagerAdapter(activity(), quizQuestionsModel.getQuestions().size(), quizQuestionsModel, true));
                binding.viewPager.setClipToPadding(false);

                secondaryProgress = (100 / quizQuestionsModel.getQuestions().size());
                binding.progressBar.setSecondaryProgress(secondaryProgress);

                if (quizQuestionsModel.getQuestions().size() <= 1)
                    binding.tvNextQuestion.setText("Close");
                else
                    binding.tvNextQuestion.setText("Next question");

            }
            binding.constraintLayout.setVisibility(View.VISIBLE);
        });

        binding.tvRetry.setOnClickListener(v -> {
            binding.constraintLayout.setVisibility(View.GONE);
            binding.llRetry.setVisibility(View.GONE);
            viewModel.getQuizDetails(quizId);
        });

        viewModel.getQuizDetails(quizId);

    }

    private void updateUiForPreviewQuiz(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.tvMessage.setText(Constants.PLEASE_CHECK_INTERNET);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;

            case IN_PROCESS:
                binding.llRetry.setVisibility(View.GONE);
                binding.spnLoader.setVisibility(View.VISIBLE);
                break;

            case NO_DATA:
                binding.tvMessage.setText(Constants.NO_DATA_AVAILABLE);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;

            case FAIL:
                binding.tvMessage.setText(Constants.SOMETHING_WENT_WRONG);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;

            case ERROR:
                binding.tvMessage.setText(Constants.SOMETHING_WENT_WRONG);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }
}
