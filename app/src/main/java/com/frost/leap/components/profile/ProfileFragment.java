package com.frost.leap.components.profile;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.BaseActivity;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentProfileBinding;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.viewmodels.profile.ProfileViewModel;

import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends BaseFragment implements IProfileAdapter, ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentProfileBinding binding;
    private ProfileViewModel viewModel;
    private ProfileAdapter profileAdapter;
    private int counter;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_profile;
    }


    @Override
    public void setUp() {
        binding = (FragmentProfileBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));


        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        binding.recyclerView.getItemAnimator().setChangeDuration(0);

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (profileAdapter == null)
            binding.recyclerView.setAdapter(profileAdapter = new ProfileAdapter(activity(), viewModel.getUser(), viewModel.generateProfileMenu(), this, viewModel.getRepositoryManager()));

    }

    @Override
    public void onProfileClick() {

        counter++;
        if (counter % 7 == 0) {
            counter = 0;
            changeSecurityLevel();
        }
    }

    @Override
    public void onMenuItemClick(MenuItem menuItem, int position) {
        if (menuItem == null)
            return;

        Intent intent = new Intent(activity(), HelperActivity.class);
        switch (menuItem.getId()) {
            case 1:
                Mixpanel.settingPageCLickEvents(1);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.PROFILE_SETTINGS);
                startActivityForResult(intent, Utility.generateRequestCodes().get("UPDATE_PROFILE"));
                break;
            case 2:
                Mixpanel.settingPageCLickEvents(3);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.BILLING_HISTORY);
                startActivity(intent);
                break;
            case 3:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.PROFILE_NOTIFICATIONS);
                startActivity(intent);
                break;
            case 4:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.DATA_USAGE);
                startActivity(intent);
                break;
            case 5:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.DISPLAY_THEME);
                startActivity(intent);
                break;
            case 6:
                Mixpanel.settingPageCLickEvents(4);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.LINKED_DEVICES);
                startActivity(intent);
                break;
            case 7:
                Mixpanel.settingPageCLickEvents(5);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.ABOUT_DEEP_LEARN);
                startActivity(intent);
                break;
            case 8:
                Mixpanel.settingPageCLickEvents(6);
//                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.HELP_PAGE);
                startActivity(intent);
                break;
            case 9:
                Mixpanel.settingPageCLickEvents(7);
                gotoFaqs();
                break;
            case 10:
                //Do you want to logout from the application?
                Utility.requestDialog(activity(), this, "Logout?", "Do you want to logout from the application?", 1);
                break;
            case 11:
                Mixpanel.settingPageCLickEvents(2);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.ADDITIONAL_SETTINGS);
                startActivity(intent);
                break;

            case 12:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.DEFAULT_APP_SETTINGS);
                startActivity(intent);
                break;
        }
    }

    private void gotoFaqs() {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(Constants.UserConstants.FAQS));
        startActivity(browserIntent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST_CODE", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("UPDATE_PROFILE")) {
            if (profileAdapter != null)
                profileAdapter.updateDetails(viewModel.getUser());
        }

    }

    @Override
    public void doPositiveAction(int id) {
        switch (id) {
            case 1:
                ((BaseActivity) activity()).doLogout(viewModel);
                break;
        }
    }

    @Override
    public void doNegativeAction() {

    }


    private void changeSecurityLevel() {
        LinearLayout linearLayout = new LinearLayout(activity());
        EditText inputEditTextField = new EditText(activity());
        linearLayout.addView(inputEditTextField);
        inputEditTextField.setInputType(EditorInfo.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        inputEditTextField.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputEditTextField.setHint("Enter the security level");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utility.toSdp(R.dimen._45sdp));
        params.setMargins(Utility.toSdp(R.dimen._18sdp), 0, Utility.toSdp(R.dimen._18sdp), 0);
        inputEditTextField.setPadding(15, 15, 15, 15);
        inputEditTextField.setLayoutParams(params);
        inputEditTextField.setMaxLines(1);
        AlertDialog dialog = new AlertDialog.Builder(activity())
                .setTitle("Set Security Level")
                .setCancelable(false)
                .setView(linearLayout)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String securityLevel = "" + inputEditTextField.getText().toString();
                        securityLevel.trim();
                        viewModel.setSecurityLevel(securityLevel);
                        dialogInterface.dismiss();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        dialog.show();
    }
}
