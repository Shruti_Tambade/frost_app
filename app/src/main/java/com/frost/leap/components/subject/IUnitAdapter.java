package com.frost.leap.components.subject;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 16-10-2019.
 * <p>
 * FROST
 */
public interface IUnitAdapter
{
    public void gotoClaimStudyMaterial();
    public void showOnlyOfflineDownloads(boolean show);
    public void onUnitClicked(int subjectPostion, int unitPosition, UnitModel unitModel, CatalogueModel catalogueModel, SubjectModel subjectModel, int lastPosition, List<UnitModel> lastWatchedUnitModel);
}
