package com.frost.leap.components.store_overview;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemEnrollBinding;

import java.util.List;

import apprepos.store.model.PriceModel;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/28/2019.
 */
public class EnrollAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<PriceModel> list;
    private IStoreOverviewAdapter iStoreOverviewAdapter;
    private int lastPosition = 0;

    public EnrollAdapter(Context context, List<PriceModel> list, IStoreOverviewAdapter iStoreOverviewAdapter) {

        this.context = context;
        this.list = list;
        this.iStoreOverviewAdapter = iStoreOverviewAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new EnrollViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_enroll, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof EnrollViewHolder) {
            EnrollViewHolder enrollViewHolder = (EnrollViewHolder) holder;

            enrollViewHolder.bindData(position);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class EnrollViewHolder extends RecyclerView.ViewHolder {

        private ItemEnrollBinding binding;

        public EnrollViewHolder(ItemEnrollBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bindData(int position) {

//            if (list.get(position).getFixed())
//                binding.tvEnrollname.setText(Html.fromHtml("Valid till " + "<b><b>" + Utility.getDateFomat(list.get(position).getSubscriptionType(), 6) + "</b></b>"));
//            else
//                binding.tvEnrollname.setText(Html.fromHtml(list.get(position).getSubscriptionType() + "<b><b> Months </b></b>"));

            binding.tvEnrollname.setText(list.get(position).getLabel());

            binding.checkbox.setChecked(list.get(position).isCheched());

            if (position == 0)
                iStoreOverviewAdapter.setSubscriptionName(binding.tvEnrollname.getText().toString());

            binding.checkbox.setOnClickListener(v -> {
                iStoreOverviewAdapter.clickPrice(position, list.get(position), lastPosition);
                lastPosition = position;
            });

        }
    }
}
