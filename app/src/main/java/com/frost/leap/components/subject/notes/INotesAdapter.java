package com.frost.leap.components.subject.notes;

import apprepos.subject.model.Note;

/**
 * Created by CHENNA RAO on 2020-01-03.
 * Frost Interactive
 */
public interface INotesAdapter {

    public void deleteNote(Note note, int position);

    public void editNote(Note note, int position);

    public void sortNotes();

    public void applyFilter();

    void applySort(boolean hasSort);
}
