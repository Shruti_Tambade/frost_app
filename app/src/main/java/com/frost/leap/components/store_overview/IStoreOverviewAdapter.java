package com.frost.leap.components.store_overview;

import apprepos.store.model.PriceModel;

/**
 * Created by Chenna Rao on 10/17/2019.
 */
public interface IStoreOverviewAdapter {

    void playPreviewVideo(int position, String videoUrl);

    void clickPrice(int position, PriceModel priceModel, int lastPosition);

    void setSubscriptionName(String subScriptionType);
}
