package com.frost.leap.components.catalogue;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.frost.leap.R;
import com.frost.leap.databinding.ItemDashboardCatalogBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.math.BigDecimal;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 7/10/2019.
 */
public class CatalogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<CatalogueModel> catalogueModelList;
    private ICatalogueAdapter iCatalogueAdapter;
    private int width = 0;

    public CatalogAdapter(Context context, List<CatalogueModel> catalogueModelList, ICatalogueAdapter iCatalogueAdapter) {
        this.context = context;
        this.catalogueModelList = catalogueModelList;
//        this.catalogueModelList = Utility.getCatalogueList3();
        width = (int) (Utility.getScreenWidth(context) * 0.75);
        this.iCatalogueAdapter = iCatalogueAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new DashBoardCatalogItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_dashboard_catalog, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof DashBoardCatalogItemViewHolder) {
            DashBoardCatalogItemViewHolder dashBoardCatalogItemViewHolder = (DashBoardCatalogItemViewHolder) holder;

//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                    width,
//                    LinearLayout.LayoutParams.MATCH_PARENT);
//            params.setMargins(
//                    Utility.dpSize(context, position == 0 ? 35 : 10),
//                    Utility.dpSize(context, 20),
//                    Utility.dpSize(context, catalogueModelList.size() - 1 == position ? 35 : 10),
//                    Utility.dpSize(context, 15));

            int width = (int) (Utility.getScreenWidth(context) * 0.57);
            int height = (int) (width * 1.7);

            Logger.d(" HEIGHT ", +height + " WIDTH " + width);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    width,
                    height);

            params.setMargins(
                    Utility.dpSize(context, position == 0 ? 25 : 0),
                    Utility.dpSize(context, 5),
                    Utility.dpSize(context, catalogueModelList.size() - 1 == position ? 25 : 10),
                    Utility.dpSize(context, 0));

            dashBoardCatalogItemViewHolder.itemView.setLayoutParams(params);

//            int width = (int) (Utility.getScreenWidth(context) * 0.57);
//            int height = (int) (width * 1.7);
//
//            Logger.d(" HEIGHT ", +height + " WIDTH " + width);
//
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                    width,
//                    height);
//
//            params.setMargins(
//                    Utility.dpSize(context, position == 0 ? 35 : 10),
//                    Utility.dpSize(context, 15),
//                    Utility.dpSize(context, catalogueModelList.size() - 1 == position ? 35 : 10),
//                    Utility.dpSize(context, 0));
//            dashBoardCatalogItemViewHolder.itemView.setLayoutParams(params);

            dashBoardCatalogItemViewHolder.bindData(position);

            dashBoardCatalogItemViewHolder.binding.cardView.setOnClickListener(v -> {
                if (catalogueModelList.get(position).getCatalogueStatus() != null && !catalogueModelList.get(position).getCatalogueStatus().isEmpty())
                    if (catalogueModelList.get(position).getCatalogueStatus().equals("ACTIVE")) {
                        // If Catalogue is Not Expired will show complete Catalogue Data
                        iCatalogueAdapter.openCatalogueOverview(catalogueModelList.get(position), position);
                        iCatalogueAdapter.changeMotion(false);
                    } else {
                        // If Catalogue is Expired will navigate to Store page to Buy the catalogue again
                        iCatalogueAdapter.openStoreOverview(catalogueModelList.get(position), position);
                    }
            });

        }
    }

    @Override
    public int getItemCount() {
        return catalogueModelList.size();
    }

    public class DashBoardCatalogItemViewHolder extends RecyclerView.ViewHolder {

        private ItemDashboardCatalogBinding binding;

        public DashBoardCatalogItemViewHolder(ItemDashboardCatalogBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position) {

            try {
                String[] catalogueColor;
                if (catalogueModelList.get(position).getCatalogueImageColor() != null && !catalogueModelList.get(position).getCatalogueImageColor().isEmpty()) {
                    catalogueColor = catalogueModelList.get(position).getCatalogueImageColor().split("#");
                    Logger.d("CATALOGUE_COLOR", catalogueModelList.get(position).getCatalogueImageColor().split("#")[1]);
                    binding.cardView.setCardBackgroundColor(Color.parseColor("#" + catalogueColor[1]));
                    binding.tvHours1.setTextColor(Color.parseColor("#" + catalogueColor[1]));

//                    binding.cardView.setCardBackgroundColor(Color.parseColor("#" + "C7B199"));
//                    binding.tvHours1.setTextColor(Color.parseColor("#" + "C7B199"));
                } else {
                    binding.cardView.setCardBackgroundColor(Color.parseColor("#" + "C9D7E9"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (catalogueModelList.get(position).getCatalogueName() != null && !catalogueModelList.get(position).getCatalogueName().isEmpty()) {
                binding.tvCatalogName.setText(catalogueModelList.get(position).getCatalogueName());
                binding.tvCatalogName1.setText(catalogueModelList.get(position).getCatalogueName());
            }
            binding.cardView.setCardElevation(1);

            if (catalogueModelList.get(position).getCatalogueImage() != null && !catalogueModelList.get(position).getCatalogueImage().isEmpty())
                Glide.with(context)
                        .load(catalogueModelList.get(position).getCatalogueImage())
//                        .load("https://res.cloudinary.com/chinna972/image/upload/v1594721743/Leap/icon.png")
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(binding.ivCatalogPhoto);

            if (catalogueModelList.get(position).getTotalDurationInSce() != null) {
                binding.tvHours.setText(Utility.splitToComponentTimes(BigDecimal.valueOf(catalogueModelList.get(position).getTotalDurationInSce())));
            }

            if (catalogueModelList.get(position).getCatalogueStatus() != null && !catalogueModelList.get(position).getCatalogueStatus().isEmpty())
                if (!catalogueModelList.get(position).getCatalogueStatus().equals("ACTIVE")) {
                    binding.rlExpiry.setVisibility(View.VISIBLE);
                    binding.tvHours.setVisibility(View.GONE);
                    binding.tvCatalogName.setVisibility(View.GONE);
                } else {
                    binding.rlExpiry.setVisibility(View.GONE);
                    binding.tvHours.setVisibility(View.VISIBLE);
                    binding.tvCatalogName.setVisibility(View.VISIBLE);
                }

        }
    }
}
