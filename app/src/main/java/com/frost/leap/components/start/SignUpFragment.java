package com.frost.leap.components.start;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.activities.SplashActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentSignUpBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.start.SignUpViewModel;
import com.google.android.material.textfield.TextInputLayout;

import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SignUpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignUpFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentSignUpBinding binding;
    private SignUpViewModel viewModel;

    public SignUpFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUpFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_sign_up;
    }


    @Override
    public void setUp() {
        binding = (FragmentSignUpBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.tvTerms.setText(Html.fromHtml(getString(R.string.terms_conditions)));
        binding.chCheckBox.setChecked(true);

        binding.rlLayout.setBackgroundResource(R.drawable.corner_radius_light_blue_10);

        String text = "By signing up, you agree to the Terms of Use and Privacy Policy";
        SpannableString ss = new SpannableString(text);

        ClickableSpan termsSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                gotoTermsAndConditions();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#007AFF"));
                ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                ds.setUnderlineText(true);
            }
        };

        ClickableSpan privacySpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                gotoPrivacyPolicy();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#007AFF"));
                ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                ds.setUnderlineText(true);
            }
        };

        ss.setSpan(termsSpan, 32, 44, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(privacySpan, 49, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        binding.tvTerms.setText(ss);
        binding.tvTerms.setMovementMethod(LinkMovementMethod.getInstance());


        binding.frameLayout.setOnClickListener(v -> {

        });

        editTextListners();

        binding.chCheckBox.setTypeface(Utility.getTypeface(5, activity()));
//        binding.tvLogin.setOnClickListener(v -> {
//            activity().onBackPressed();
//        });

        binding.imgClose.setOnClickListener(v -> {
            activity().onBackPressed();
        });


        binding.tvSignUp.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());

            if (viewModel.doValidationSignUp(
                    binding.etFirstName.getText().toString(),
                    binding.etLastName.getText().toString(),
                    binding.etEmailAddress.getText().toString(),
                    binding.etPhoneNumber.getText().toString(),
                    binding.etPassword.getText().toString(),
                    binding.etConfirmPassword.getText().toString(),
                    binding.chCheckBox.isChecked()
            )) {
                viewModel.checkUserExist();
            }


        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case SignUpViewModel.NetworkTag.CHECK_USER_EXIST:
                    updateUiForCheckUserExist(networkCall);
                    break;
            }
        });


        viewModel.getError().observe(this, error -> {
            if (error == null)
                return;

            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setError(error.getMessage());
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setEndIconTintList(AppCompatResources.getColorStateList(activity(), R.color.password_toggle_red_color));

        });

    }

    private void editTextListners() {

        binding.etFirstName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlFirstName.setErrorEnabled(false);
                    binding.tlFirstName.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etLastName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlLastName.setErrorEnabled(false);
                    binding.tlLastName.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etEmailAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlEmailAddress.setErrorEnabled(false);
                    binding.tlEmailAddress.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlPassword.setErrorEnabled(false);
                    binding.tlPassword.setEndIconTintList(AppCompatResources.getColorStateList(activity(), R.color.normal_text_color));
                    binding.tlPassword.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etConfirmPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlConfirmPassword.setErrorEnabled(false);
                    binding.tlConfirmPassword.setEndIconTintList(AppCompatResources.getColorStateList(activity(), R.color.normal_text_color));
                    binding.tlConfirmPassword.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etPhoneNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlPhoneNumber.setErrorEnabled(false);
                    binding.tlPhoneNumber.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                    binding.rlLayout.setBackgroundResource(R.drawable.corner_radius_blue_10);
                } else {
                    binding.rlLayout.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
                }
            }
        });

    }


    private void updateUiForCheckUserExist(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSignUp.setVisibility(View.VISIBLE);
                break;
            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvSignUp.setVisibility(View.GONE);
                break;
            case SUCCESS:
                Mixpanel.createSignUp(false, networkCall.getBundle().getParcelable("User"), true, "");
                gotoVerifyOTP(networkCall.getBundle().getParcelable("User"));
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSignUp.setVisibility(View.VISIBLE);
                break;
            case FAIL:
                Mixpanel.createSignUp(false, networkCall.getBundle().getParcelable("User"), false, "");
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSignUp.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSignUp.setVisibility(View.VISIBLE);
                break;

        }
    }

    private void gotoVerifyOTP(User user) {
        if (activity() instanceof SplashActivity) {
            ((SplashActivity) activity()).gotoVerifyOtp("Mobile",
                    binding.etPhoneNumber.getText().toString(),
                    VerifyType.REGISTER, user);
        }
    }

    private void gotoTermsAndConditions() {

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.TERMS_AND_USES_URL), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.TERMS_AND_USES_URL), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UserConstants.TERMS_AND_USES_URL));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }

    private void gotoPrivacyPolicy() {

//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, "Terms of Use");
//        intent.putExtra("Url", Constants.UserConstants.TERMS_AND_USES_URL);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }


    @Override
    public void changeTitle(String title) {

    }
}
