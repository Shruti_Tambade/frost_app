package com.frost.leap.components.media;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.media.adapter.CustomMediaAdapter;
import com.frost.leap.components.media.constants.MediaType;
import com.frost.leap.components.media.interfaces.ICustomMediaAdapter;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.media.models.Media;
import com.frost.leap.databinding.FragmentGallaryBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.interfaces.IFragment;

import java.util.ArrayList;
import java.util.List;

import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CustomMediaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CustomMediaFragment extends Fragment implements IFragment, ICustomMediaAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    List<GalleryFile> list = new ArrayList<>();
    public List<GalleryFile> selectedItems = new ArrayList<>();

    CustomMediaAdapter customMediaAdapter;

    public int mediaPickedOption = 1;

    int count = 0;
    long size = 0;

    public boolean isPick = false;

    public String cameraMediaName = null;

    public boolean isSizeLimitExceed = false;

    public boolean isCountLimitExceed = false;

    String fetchQuery = null;

    int existingMedia = 0;

    private FragmentGallaryBinding customMediaBinding;

    private AsyncTask loadMediaFromDevice;


    public CustomMediaFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CustomMediaFragment newInstance(List<GalleryFile> list, List<Media> mediaList, int type) {
        CustomMediaFragment fragment = new CustomMediaFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("GalleryFileList", (ArrayList<? extends Parcelable>) list);
        args.putParcelableArrayList("MediaList", (ArrayList<? extends Parcelable>) mediaList);
        args.putInt("Type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mediaPickedOption = getArguments().getInt("Type", 1);
            selectedItems = getArguments().getParcelableArrayList("GalleryFileList");
            if (selectedItems == null) {
                selectedItems = new ArrayList<>();
            }
            if (getArguments().getParcelableArrayList("MediaList") != null) {
                List<Media> mediaList = getArguments().getParcelableArrayList("MediaList");
                if (mediaList != null && mediaList.size() > 0) {
                    existingMedia = mediaList.size();
                }
                Logger.d("Existed media size", "" + existingMedia);
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        customMediaBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallary, container, false);

        setUp();
        return customMediaBinding.getRoot();
    }

    private void setUp() {

        addPickerType(mediaPickedOption);
        customMediaBinding.recyclerView.setLayoutManager(new GridLayoutManager(activity(), 3));
        loadMediaFromDevice = new LoadMediaFromDevice().execute();
    }


    public void refresh() {

        activity().runOnUiThread(() -> {
            isPick = true;
            loadMediaFromDevice = new LoadMediaFromDevice().execute();
        });
    }

    public void reload() {
        activity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                addPickerType(mediaPickedOption);
                loadMediaFromDevice = new LoadMediaFromDevice().execute();
            }
        });
    }

    private void addPickerType(int value) {
        switch (value) {
            case 0: // both images and videos
                fetchQuery = MediaStore.Files.FileColumns.MEDIA_TYPE + " = 1 OR " + MediaStore.Files.FileColumns.MEDIA_TYPE + " = 3";
                break;

            case 1: // only images
                fetchQuery = MediaStore.Files.FileColumns.MEDIA_TYPE + " = 1";
                break;

            case 2: // only audio
                fetchQuery = MediaStore.Files.FileColumns.MEDIA_TYPE + " = 2";
                break;

            case 3: // only videos
                fetchQuery = MediaStore.Files.FileColumns.MEDIA_TYPE + " = 3";
                break;

        }
    }

    @Override
    public void changeTitle(String title) {
        ((HelperActivity) activity()).changeTitle(title);
    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(activity(), customMediaBinding.coordinatorLayout, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    public void updateAdapter() {
        if (list != null && list.size() > 0) {
            customMediaBinding.recyclerView.setLayoutManager(new GridLayoutManager(activity(), 3));
            customMediaBinding.recyclerView.setAdapter(customMediaAdapter = new CustomMediaAdapter(activity(), list, this));
        } else {
            customMediaBinding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
            customMediaBinding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), "Media items are not available", R.drawable.no_data_available, 1));
        }
    }

    @Override
    public void click(GalleryFile galleryFile, int position) {

        Logger.d("Count", "" + count);
        Logger.d("Gallery File", galleryFile.name);
        if (size <= Constants.MEDIA_SIZE_LIMIT_IN_BYES) {
            if (!galleryFile.isSelect) {
                if (size + Long.parseLong(galleryFile.size) >= Constants.MEDIA_SIZE_LIMIT_IN_BYES) {
                    showSnackBar("You can select upto " + Constants.MEDIA_SIZE_LIMIT_IN_BYES / (1024 * 1024) + " MB", 2);
                    return;
                }
            }
        }
        if (count + existingMedia == Constants.GALLERY_LIMIT) {
            if (!galleryFile.isSelect) {
                showSnackBar("You can select " + (Constants.GALLERY_LIMIT - existingMedia) + " items only", 2);
                return;
            }
        }


        list.get(position).isSelect = !galleryFile.isSelect;
        customMediaAdapter.notifyItemChanged(position);
        for (GalleryFile item : selectedItems) {
            if (item.id == galleryFile.id) {
                selectedItems.remove(item);
                updateData();
                return;
            }
        }
        selectedItems.add(list.get(position));
        updateData();


        // inside single click

    }


    @Override
    public void longPressed(GalleryFile galleryFile, int position) {

        Logger.d("Count", "" + count);
        Logger.d("Gallery File", galleryFile.name);
        if (size <= Constants.MEDIA_SIZE_LIMIT_IN_BYES) {
            if (!galleryFile.isSelect) {
                if (size + Long.parseLong(galleryFile.size) >= Constants.MEDIA_SIZE_LIMIT_IN_BYES) {
                    showSnackBar("You can select upto " + Constants.MEDIA_SIZE_LIMIT_IN_BYES / (1024 * 1024) + " MB", 2);
                    return;
                }
            }
        }
        if (count + existingMedia == Constants.GALLERY_LIMIT) {
            if (!galleryFile.isSelect) {
                showSnackBar("You can select " + (Constants.GALLERY_LIMIT - existingMedia) + " items only", 2);
                return;
            }
        }


        list.get(position).isSelect = !galleryFile.isSelect;
        customMediaAdapter.notifyItemChanged(position);
        for (GalleryFile item : selectedItems) {
            if (item.id == galleryFile.id) {
                selectedItems.remove(item);
                updateData();
                return;
            }
        }
        selectedItems.add(list.get(position));
        updateData();


        // inside single click

    }

    private String getMimeType(String actualMimeType, MediaType mediaType) {
        if (mediaType == MediaType.AUDIO) {
            return "audio/mp3";
        } else if (mediaType == MediaType.VIDEO) {
            return "video/mp4";
        } else if (mediaType == MediaType.IMAGE) {
            return "image/jpeg";
        } else if (mediaType == MediaType.FILE) {
            return "doc/pdf";
        } else {
            return "image/jpeg";
        }
    }

    public class LoadMediaFromDevice extends AsyncTask<Void, Void, List<GalleryFile>> {

        List<GalleryFile> result;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            result = new ArrayList<>();
            customMediaBinding.progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<GalleryFile> doInBackground(Void... params) {

            if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity())
                    || !Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {

                return new ArrayList<>();
            }

            String[] projection = {
                    MediaStore.Files.FileColumns._ID,
                    MediaStore.Files.FileColumns.DISPLAY_NAME,
                    MediaStore.Files.FileColumns.SIZE,
                    MediaStore.Files.FileColumns.MEDIA_TYPE,
                    MediaStore.Files.FileColumns.MIME_TYPE,
                    MediaStore.Files.FileColumns.DATA,
                    MediaStore.Files.FileColumns.TITLE,
                    MediaStore.Audio.AudioColumns.DURATION
            };
            // Create the cursor pointing to the SDCard
            Cursor cursor;
            if (mediaPickedOption == 0) {
                String selectionMimeType = MediaStore.Files.FileColumns.MIME_TYPE + "=? OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=? OR " + MediaStore.Files.FileColumns.MIME_TYPE + "=?";
                String[] selectionArgsPdf = new String[]{MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf"), MimeTypeMap.getSingleton().getMimeTypeFromExtension("doc"), MimeTypeMap.getSingleton().getMimeTypeFromExtension("docx")};
                cursor = activity().getContentResolver().query(MediaStore.Files.getContentUri("external"), projection, selectionMimeType,
                        selectionArgsPdf, MediaStore.Files.FileColumns._ID + " DESC");
            } else {
                cursor = activity().getContentResolver().query(MediaStore.Files.getContentUri("external"),
                        projection,
                        fetchQuery,
                        null,
                        MediaStore.Files.FileColumns._ID + " DESC");
            }

            if (cursor == null)
                return result;
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                Uri uri = null;
                Uri thumbnailUri = null;


                if (cursor.getString(3).equals("0")) {//File
                    uri = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, "" + cursor.getInt(0));
                    //thumbnailUri = Uri.withAppendedPath(MediaStore.VideoConstants.Thumbnails.EXTERNAL_CONTENT_URI,""+cursor.getInt(0));
                } else if (cursor.getString(3).equals("1")) {//Image
                    uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + cursor.getInt(0));
                    thumbnailUri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + cursor.getInt(0));
                } else if (cursor.getString(3).equals("2")) {//Audio
                    uri = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, "" + cursor.getInt(0));
                    //thumbnailUri = Uri.withAppendedPath(MediaStore.Audio.Albums.ALBUM_ART,""+cursor.getInt(0));
                } else if (cursor.getString(3).equals("3")) {//VideoConstants
                    uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, "" + cursor.getInt(0));
                    thumbnailUri = Uri.withAppendedPath(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, "" + cursor.getInt(0));
                }

                GalleryFile galleryFile = new GalleryFile();
                galleryFile.id = cursor.getInt(0);
                galleryFile.name = cursor.getString(6); //cursor.getString(1);
                galleryFile.mediaType = cursor.getString(3).equals("0") ? MediaType.FILE : cursor.getString(3).equals("1") ? MediaType.IMAGE : cursor.getString(3).equals("2") ? MediaType.AUDIO : MediaType.VIDEO;
                galleryFile.mimeType = getMimeType(cursor.getString(4), galleryFile.mediaType);
                galleryFile.uri = uri;
                String fullPath = cursor.getString(5);
                galleryFile.caption = fullPath.substring(fullPath.lastIndexOf("/") + 1);
                galleryFile.thumbnailUri = thumbnailUri;
                galleryFile.size = cursor.getString(2) == null ? Constants.SAMPLE_FILE_SIZE : cursor.getString(2);
                galleryFile.dataFilePath = fullPath;


                if (i == 0 && isPick) {
                    if (selectedItems.size() + existingMedia == Constants.GALLERY_LIMIT) {
                        isCountLimitExceed = true;
                    } else if (checkSizeLimit(galleryFile)) {
                        isSizeLimitExceed = true;
                    } else {
                        selectedItems.add(galleryFile);
                        //addFacePoints(selectedItems.size()-1,galleryFile.uri);
                    }
                    isPick = false;
                }

                if (selectedItems != null && selectedItems.size() > 0) {
                    for (int j = 0; j < selectedItems.size(); j++) {
                        if (galleryFile.id == selectedItems.get(j).id) {
                            galleryFile.isSelect = true;
                            break;
                        }
                    }
                }

                Logger.d("SIZE ", galleryFile.size);

                result.add(galleryFile);
            }


            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            return result;
        }


        @Override
        protected void onPostExecute(List<GalleryFile> result) {
            customMediaBinding.progressBar.setVisibility(View.GONE);
            list = result;
            updateAdapter();
            updateData();
            if (isSizeLimitExceed) {
                showSnackBar("You can select upto " + Constants.MEDIA_SIZE_LIMIT_IN_BYES / (1024 * 1024) + " MB", 2);
                isSizeLimitExceed = false;
            }
            if (isCountLimitExceed) {
                showSnackBar("You can select " + Constants.GALLERY_LIMIT + " items only", 2);
                isCountLimitExceed = false;
            }
        }
    }


    public boolean checkSizeLimit(GalleryFile galleryFile) {
        long selectedFilesSize = 0;
        for (int i = 0; i < selectedItems.size(); i++) {
            selectedFilesSize += Long.parseLong(selectedItems.get(i).size);
        }
        return selectedFilesSize + Long.parseLong(galleryFile.size) >= Constants.MEDIA_SIZE_LIMIT_IN_BYES ? true : false;
    }

    public void updateData() {
        if (selectedItems != null && selectedItems.size() > 0) {
            count = 0;
            size = 0;
            for (int i = 0; i < selectedItems.size(); i++) {
                count++;
                size += Long.parseLong(selectedItems.get(i).size);
            }

            Logger.d("Total Size", "" + size);
            Logger.d("Count", "" + count);


            changeTitle(count == 0 ? ((HelperActivity) activity()).getMediaTitle(mediaPickedOption) : count + " Selected");
            if (((HelperActivity) activity()).menuDone != null)
                ((HelperActivity) activity()).menuDone.setVisible(count == 0 ? false : true);
            if (count == Constants.GALLERY_LIMIT && Constants.GALLERY_LIMIT == 1) {
                ((HelperActivity) activity()).sendMedia();
            }
        } else {
            count = 0;
            size = 0;
            changeTitle(((HelperActivity) activity()).getMediaTitle(mediaPickedOption));
            if (((HelperActivity) activity()).menuDone != null)
                ((HelperActivity) activity()).menuDone.setVisible(false);
        }
    }

    public void sendSelectedGalleryFiles() {
        if (selectedItems != null && selectedItems.size() > 0) {
            Intent returnData = new Intent(activity(), HelperActivity.class);
            returnData.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) selectedItems);
            activity().setResult(Activity.RESULT_OK, returnData);
            activity().finish();
        } else {
            showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadMediaFromDevice != null) {
            loadMediaFromDevice.cancel(true);
        }
    }
}
