package com.frost.leap.components.start;

public enum VerifyType
{
    LOGIN,
    REGISTER,
    LOST_PASSWORD
}
