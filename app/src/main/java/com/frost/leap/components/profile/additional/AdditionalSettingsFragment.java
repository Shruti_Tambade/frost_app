package com.frost.leap.components.profile.additional;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentAdditionalSettingsBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.services.HelperService;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.profile.AdditionalViewModel;

import java.util.Arrays;
import java.util.List;

import apprepos.user.model.AdditionalDetails;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AdditionalSettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdditionalSettingsFragment extends BaseFragment implements IAdditionalSettingsAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private AdditionalViewModel viewModel;
    private FragmentAdditionalSettingsBinding binding;
    private AdditionalDetails additionalDetails;
    private AdditionalSettingsAdapter adapter;
    private boolean isFront = true;
    private GalleryFile galleryFile;
    private String proofFront, proofBack;

    public AdditionalSettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AdditionalSettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AdditionalSettingsFragment newInstance(String param1, String param2) {
        AdditionalSettingsFragment fragment = new AdditionalSettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(AdditionalViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_additional_settings;
    }


    @Override
    public void setUp() {
        binding = (FragmentAdditionalSettingsBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);

            viewModel.fetchAdditionalDetails();
        });


        viewModel.getMessage().observe(this, message ->
        {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case AdditionalViewModel.NetworkTags.GET_ADDITIONAL_DETAILS:
                    updateUiForAdditionalDetails(networkCall);
                    break;

                case AdditionalViewModel.NetworkTags.PUSH_PROFILE_PIC_S3:
                    updateUiForPushToS3(networkCall);
                    break;
            }
        });


        viewModel.updateAdditionalDetails().observe(this, additionalDetails -> {
            this.additionalDetails = additionalDetails;
            binding.swipeRefreshLayout.setEnabled(false);
            binding.recyclerView.setAdapter(adapter = new AdditionalSettingsAdapter(additionalDetails,
                    viewModel.generateAdditionalSettingsMenu(additionalDetails),
                    RController.DONE,
                    this,
                    activity()
            ));
        });


        viewModel.fetchAdditionalDetails();
    }

    private void updateUiForPushToS3(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;

            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                if (networkCall.getBundle() != null) {
                    if (isFront)
                        proofFront = networkCall.getBundle().getString("ImageUrl");
                    else
                        proofBack = networkCall.getBundle().getString("ImageUrl");
                }
                break;

            case FAIL:
                closeProgressDialog();
                break;
            case ERROR:
                closeProgressDialog();
                break;

        }
    }


    @Override
    public void changeTitle(String title) {

    }


    private void updateUiForAdditionalDetails(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;
            case IN_PROCESS:
                binding.recyclerView.setAdapter(new AdditionalSettingsAdapter(null, Arrays.asList(null, null), RController.LOADING, this, activity()));
                break;
            case FAIL:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;
        }
    }


    public void updateProofPic(List<GalleryFile> galleryFileList) {
        if (!viewModel.isNetworkAvailable()) {
            showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
            return;
        }

        Logger.d("FILE URI", galleryFileList.get(0).uri.getPath() + "MIMET : -" + galleryFileList.get(0).name);
        galleryFile = galleryFileList.get(0);
        if (adapter != null)
            adapter.updateProof(galleryFileList.get(0).uri, isFront, galleryFileList.get(0).name);

        viewModel.uploadProofPic(galleryFile);
    }

    @Override
    public void onAdditionalSettingsClick(MenuItem menuItem, int position) {
        if (menuItem == null || adapter == null)
            return;

        if (!menuItem.isOpen() && menuItem.isExpand()) {
            adapter.addSubItem(position);
        } else if (menuItem.isOpen() && menuItem.isExpand()) {
            adapter.deleteSubItem(position);
        }
    }


    @Override
    public void gotoPrivacyPolicies() {
//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, "Privacy Policies");
//        intent.putExtra("Url", Constants.UserConstants.PRIVACY_POLICIES_URL);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }

    @Override
    public void updateAdditionalDetails(AdditionalDetails additionalDetails, int position) {
        Utility.hideKeyboard(activity());
        this.additionalDetails = additionalDetails;


        if (proofFront != null)
            this.additionalDetails.setProofFront(proofFront);
        if (proofBack != null)
            this.additionalDetails.setProofBack(proofBack);

        updateDetails(this.additionalDetails);
    }


    private void updateDetails(AdditionalDetails additionalDetails) {
        Intent intent = new Intent(activity(), HelperService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_ADDITIONAL_DETAILS);
        intent.putExtra("AdditionalDetails", additionalDetails);
        activity().startService(intent);
    }


    @Override
    public void changeProofFrontSide() {
        if (activity() instanceof HelperActivity) {
            Constants.GALLERY_LIMIT = 1;
            isFront = true;
            ((HelperActivity) activity()).openCustomGallery();
        }
    }

    @Override
    public void changeProofBackSide() {
        if (activity() instanceof HelperActivity) {
            Constants.GALLERY_LIMIT = 1;
            isFront = false;
            ((HelperActivity) activity()).openCustomGallery();
        }
    }

    @Override
    public void previewImage(Uri uri) {
        if (uri == null) return;
        if (activity() instanceof HelperActivity) {
            ((HelperActivity) activity()).openFullImageView(uri);
        }
    }

    @Override
    public void sendMessage(Message message) {
        showSnackBar(message.getMessage(), message.getType());
    }
}
