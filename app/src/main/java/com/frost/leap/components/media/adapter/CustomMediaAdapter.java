package com.frost.leap.components.media.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.frost.leap.R;
import com.frost.leap.components.media.constants.MediaType;
import com.frost.leap.components.media.interfaces.ICustomMediaAdapter;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.databinding.GalleryItemBinding;

import java.util.List;

import supporters.utils.Utility;


public class CustomMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<GalleryFile> list;
    Context context;
    ICustomMediaAdapter iCustomMediaAdapter;


    public int getMediaType(MediaType mediaType) {
        switch (mediaType) {
            case IMAGE:
                return R.drawable.ic_image;
            case VIDEO:
                return R.drawable.ic_video;
            case AUDIO:
                return R.drawable.ic_audiotrack;
            case FILE:
                return R.drawable.ic_document;
        }

        return R.drawable.ic_image;
    }


    public CustomMediaAdapter(Context context, List<GalleryFile> list, ICustomMediaAdapter iCustomMediaAdapter) {
        this.list = list;
        this.context = context;
        this.iCustomMediaAdapter = iCustomMediaAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GalleryItemBinding galleryItemBinding;
        galleryItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.gallery_item, parent, false);
        return new CustomMediaViewHolder(galleryItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomMediaViewHolder) {
            CustomMediaViewHolder customMediaViewHolder = (CustomMediaViewHolder) holder;
            int paddingImage = 100;
            if (list.get(position).mediaType == MediaType.AUDIO) {
                customMediaViewHolder.galleryItemBinding.imgGallery.setImageResource(R.drawable.mp3);
                customMediaViewHolder.galleryItemBinding.imgGallery.setPadding(paddingImage, paddingImage, paddingImage, paddingImage);
            } else if (list.get(position).mediaType == MediaType.FILE) {
                if (list.get(position).caption != null && list.get(position).caption.contains(".pdf")) {
                    customMediaViewHolder.galleryItemBinding.imgGallery.setImageResource(R.drawable.pdf);
                    customMediaViewHolder.galleryItemBinding.imgGallery.setPadding(paddingImage, paddingImage, paddingImage, paddingImage);
                } else {
                    customMediaViewHolder.galleryItemBinding.imgGallery.setImageResource(R.drawable.docs);
                    customMediaViewHolder.galleryItemBinding.imgGallery.setPadding(paddingImage, paddingImage, paddingImage, paddingImage);
                }
            } else {


                Glide.with(context)
                        .load(list.get(position).uri)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_photo)
                                .centerCrop().skipMemoryCache(true))
                        .into(customMediaViewHolder.galleryItemBinding.imgGallery);

            }


            customMediaViewHolder.galleryItemBinding.tvMediaName.setVisibility(list.get(position).mediaType == MediaType.AUDIO ? View.VISIBLE : list.get(position).mediaType == MediaType.FILE ? View.VISIBLE : View.GONE);
            customMediaViewHolder.galleryItemBinding.tvMediaName.setText(list.get(position).name);

            customMediaViewHolder.galleryItemBinding.imgTag.setVisibility(list.get(position).mediaType == MediaType.IMAGE ? View.GONE : View.VISIBLE);

            customMediaViewHolder.galleryItemBinding.imgTag.setImageResource(getMediaType(list.get(position).mediaType));
            customMediaViewHolder.galleryItemBinding.rlSelect.setVisibility(list.get(position).isSelect ? View.VISIBLE : View.GONE);

            customMediaViewHolder.itemView.setOnClickListener(v ->
            {
                iCustomMediaAdapter.click(list.get(position), position);

            });

            customMediaViewHolder.itemView.setOnLongClickListener(v -> {
                iCustomMediaAdapter.longPressed(list.get(position), position);
                return false;
            });

        }

        holder.itemView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utility.getScreenWidth(context) / 3));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CustomMediaViewHolder extends RecyclerView.ViewHolder {

        private GalleryItemBinding galleryItemBinding;

        public CustomMediaViewHolder(GalleryItemBinding galleryItemBinding) {
            super(galleryItemBinding.getRoot());
            this.galleryItemBinding = galleryItemBinding;
        }
    }
}
