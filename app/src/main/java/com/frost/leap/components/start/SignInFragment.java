package com.frost.leap.components.start;


import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.activities.SplashActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentSignInBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.start.StartViewModel;
import com.google.android.material.textfield.TextInputLayout;

import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SignInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignInFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentSignInBinding binding;
    private StartViewModel viewModel;
    private int counter = 0;

    public SignInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignInFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(StartViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_sign_in;
    }


    public void setFocus(boolean focus) {
        binding.etPhoneNumber.setFocusable(focus);
        binding.etPhoneNumber.setFocusableInTouchMode(focus);
    }

    @Override
    public void setUp() {

        Utility.deleteCache(activity());
        binding = (FragmentSignInBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.tvLostPhone.setText(Html.fromHtml(getString(R.string.lost_phone)));
        binding.etCountryCode.setEnabled(false);
        binding.etCountryCode.setFocusable(false);
        binding.etCountryCode.setFocusableInTouchMode(false);

        binding.etPhoneNumber.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utility.hideKeyboard(activity());
                if (viewModel.doMobileValidation(binding.etPhoneNumber.getText().toString())) {
                    //flashCallVerification();
                    viewModel.requestLogin();
                }
                return true;
            }
            return false;
        });
        binding.tvLogin.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            if (viewModel.doMobileValidation(binding.etPhoneNumber.getText().toString())) {
                viewModel.requestLogin();
                //flashCallVerification();
            }
        });


        binding.etPhoneNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlPhoneNumber.setErrorEnabled(false);
                    binding.tlPhoneNumber.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));

                }
            }
        });

        binding.tvLostPhone.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            gotoLostPhone();
            binding.tlPhoneNumber.setErrorEnabled(false);

            Mixpanel.recoveryButtonClick();
        });

        binding.tvSignUp.setOnClickListener(v -> {
            gotoSignUp();
            binding.tlPhoneNumber.setErrorEnabled(false);

            Mixpanel.signupButtonClick();
        });

        binding.etPhoneNumber.setOnClickListener(v -> {

            binding.etPhoneNumber.setFocusable(true);
            binding.etPhoneNumber.setFocusableInTouchMode(true);
            binding.etPhoneNumber.requestFocus();

            //ShowKeyBoard
            InputMethodManager imm = (InputMethodManager) activity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(binding.etPhoneNumber, InputMethodManager.SHOW_IMPLICIT);
        });


        binding.imgApp.setOnClickListener(v -> {
            counter++;
            if (counter % 7 == 0) {
                counter = 0;
                showSnackBar(Constants.APP_INFO, 2);
            }
        });

        binding.llLayout.setOnClickListener(v -> {
            binding.etPhoneNumber.setFocusable(false);
            binding.etPhoneNumber.setFocusableInTouchMode(false);
            binding.tlPhoneNumber.setErrorEnabled(false);

            //HideKeyBoard
            InputMethodManager imm = (InputMethodManager) activity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(binding.etPhoneNumber.getWindowToken(), 0);
            binding.tlPhoneNumber.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));

        });


        viewModel.getError().observe(this, error -> {
            if (error == null)
                return;

            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setError(error.getMessage());
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));

        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case StartViewModel.NetworkTag.REQUEST_LOGIN:
                    updateUiForRequestLogin(networkCall);
                    break;
            }
        });


        if (activity() instanceof SplashActivity)
            ((SplashActivity) activity()).requestMobileHint();


    }


//    public void flashCallVerification() {
//        if (!Utility.checkPermissionRequest(Permission.CALL, activity()) && !Utility.checkPermissionRequest(Permission.PHONE_STATE, activity())) {
//            Utility.raisePermissionRequest(Permission.CALL, activity());
//            return;
//        }
//        if (!isNetworkAvailable()) {
//            showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
//            return;
//        }
//        Config config = SinchVerification.config().applicationKey("dfd90634-2b62-4b4c-bae8-7d0f0ce71a68").context(activity().getApplicationContext()).build();
//        VerificationListener listener = new PhoneVerificationListener();
//
//        String defaultRegion = PhoneNumberUtils.getDefaultCountryIso(activity());
//        String phoneNumberInE164 = PhoneNumberUtils.formatNumberToE164(binding.etPhoneNumber.getText().toString(), defaultRegion);
//
//        verification = SinchVerification.createFlashCallVerification(config, phoneNumberInE164, listener);
//        verification.initiate();
//        showProgressDialog();
//
//    }


//    private class PhoneVerificationListener implements VerificationListener {
//
//
//        @Override
//        public void onInitiated(InitiationResult initiationResult) {
//            showProgressDialog();
//        }
//
//        @Override
//        public void onInitiationFailed(Exception e) {
//            closeProgressDialog();
//            e.printStackTrace();
//        }
//
//        @Override
//        public void onVerified() {
//            closeProgressDialog();
//            new AlertDialog.Builder(activity())
//                    .setTitle("Verification")
//                    .setMessage("+91 " + binding.etPhoneNumber.getText().toString()  +" is verified Successful!")
//                    .setPositiveButton("Done", (dialog, whichButton) -> dialog.cancel())
//                    .show();
//        }
//
//        @Override
//        public void onVerificationFailed(Exception e) {
//
//            if (e instanceof InvalidInputException) {
//                // Incorrect number or code provided, ask the user to input number again.
//            } else if (e instanceof CodeInterceptionException) {
//                // Intercepting the verification code automatically failed, try to verify manually.
//            } else if (e instanceof IncorrectCodeException) {
//                // The verification code provided was incorrect. Ask the user to type it again.
//            } else if (e instanceof ServiceErrorException) {
//                // Another service error, please report the error message to the developers.
//            } else {
//                // Other system error.
//            }
//            closeProgressDialog();
//            e.printStackTrace();
//
//        }
//
//        @Override
//        public void onVerificationFallback() {
//
//        }
//    }


    private void updateUiForRequestLogin(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvLogin.setVisibility(View.VISIBLE);
                break;
            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvLogin.setVisibility(View.GONE);
                break;
            case SUCCESS:
                gotoVerifyOTP();
                Mixpanel.clickLogin(binding.etPhoneNumber.getText().toString(), true);
                binding.llLoader.setVisibility(View.GONE);
                binding.tvLogin.setVisibility(View.VISIBLE);
                break;
            case FAIL:
                Mixpanel.clickLogin(binding.etPhoneNumber.getText().toString(), false);
                binding.llLoader.setVisibility(View.GONE);
                binding.tvLogin.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvLogin.setVisibility(View.VISIBLE);
                break;

        }
    }

    private void gotoVerifyOTP() {
        setFocus(false);
        if (activity() instanceof SplashActivity) {
            ((SplashActivity) activity()).gotoVerifyOtp("Mobile",
                    binding.etPhoneNumber.getText().toString(),
                    VerifyType.LOGIN, null);
        }
    }

    private void gotoSignUp() {
        setFocus(false);
        if (activity() instanceof SplashActivity) {
            ((SplashActivity) activity()).gotoSignUp();
        }
    }

    private void gotoLostPhone() {
        setFocus(false);
        if (activity() instanceof SplashActivity) {
            ((SplashActivity) activity()).gotoLostPhone();
        }
    }

    @Override
    public void changeTitle(String title) {

    }


    public void updateMobileNumber(String mobileNumber) {
        if (mobileNumber != null) {
            mobileNumber = mobileNumber.replaceAll("\\+91", "");
            binding.etPhoneNumber.setText(mobileNumber);
        }
    }


}
