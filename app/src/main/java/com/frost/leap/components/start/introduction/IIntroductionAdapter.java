package com.frost.leap.components.start.introduction;

public interface IIntroductionAdapter {

    void signInPage();
    void tryitNowPage();
}
