package com.frost.leap.components.profile.billing;


import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AlertDialog;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentInvoiceDetailsBinding;

import apprepos.user.model.Invoice;
import supporters.constants.Constants;

/**
 * A simple {@link } subclass.
 * Use the {@link InvoiceDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InvoiceDetailsFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Invoice invoice;
    private FragmentInvoiceDetailsBinding binding;

    public InvoiceDetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static InvoiceDetailsFragment newInstance(Invoice invoice, String param2) {
        InvoiceDetailsFragment fragment = new InvoiceDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable("Invoice", invoice);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            invoice = getArguments().getParcelable("Invoice");
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_invoice_details;
    }

    @Override
    public void setUp() {
        binding = (FragmentInvoiceDetailsBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        if (invoice == null || invoice.getOrderId() == null)
            return;


        binding.webView.getSettings().setLoadsImagesAutomatically(true);
        binding.webView.getSettings().setAllowContentAccess(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);

        binding.webView.loadUrl(Constants.UserConstants.INVOICE_PREVIEW_URL + invoice.getOrderId());

        binding.webView.setWebViewClient(new AppWebViewClients());

        binding.tvRetry.setOnClickListener(v -> {
            reload();
        });


        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            reload();
        });

        binding.swipeRefreshLayout.setEnabled(false);
    }


    public void reload() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.tvPlaceHolderWeb.setVisibility(View.VISIBLE);
        binding.webView.setVisibility(View.VISIBLE);
        binding.rlRetry.setVisibility(View.GONE);
        binding.webView.getSettings().setLoadsImagesAutomatically(true);
        binding.webView.getSettings().setAllowContentAccess(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.loadUrl(Constants.UserConstants.INVOICE_PREVIEW_URL + invoice.getOrderId());
        binding.webView.setWebViewClient(new AppWebViewClients());

    }

    public class AppWebViewClients extends WebViewClient {
        public boolean error = false;

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (error) {
                binding.progressBar.setVisibility(View.GONE);
                binding.tvPlaceHolderWeb.setVisibility(View.VISIBLE);
                binding.rlRetry.setVisibility(View.VISIBLE);
                binding.swipeRefreshLayout.setRefreshing(false);
            } else {
                binding.progressBar.setVisibility(View.GONE);
                binding.tvPlaceHolderWeb.setVisibility(View.GONE);
                binding.rlRetry.setVisibility(View.GONE);
                binding.swipeRefreshLayout.setRefreshing(false);

            }

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            this.error = true;
            super.onReceivedError(view, request, error);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            error = false;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity());
            builder.setTitle("Security Alert");
            builder.setMessage("Due to invalid ssl certificates");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }


    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
