package com.frost.leap.components.profile.support;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.components.profile.models.Type;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentSubmitRequestBinding;
import com.frost.leap.services.HelperService;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.profile.SubmitRequestViewModel;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.model.SupportInfo;
import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubmitRequestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubmitRequestFragment extends BaseFragment implements IPickedItemAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentSubmitRequestBinding binding;
    private SubmitRequestViewModel viewModel;
    private List<GalleryFile> list;
    //    private SupportInfo supportInfo;
    private int issueType = 0;
    private int courseType = 0;
    private int topicsType = 0;
    private PickedItemAdapter pickedItemAdapter;

    private MenuItem menuItem = null;
    private boolean isOptional = false;

    private List<String> coursesList = new ArrayList<>();
    private List<String> topicList = new ArrayList<>();
    private List<Type> typeList = new ArrayList<>();
    private List<String> typeItemsList = new ArrayList<>();

    private String issueTypeString = "", courseTypeString = "";

    private String type = "", topic = "";

    public SubmitRequestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubmitRequestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SubmitRequestFragment newInstance(MenuItem param1, boolean param2) {
        SubmitRequestFragment fragment = new SubmitRequestFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(SubmitRequestViewModel.class);
        if (getArguments() != null) {
            menuItem = getArguments().getParcelable(ARG_PARAM1);
            isOptional = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_submit_request;
    }


    @Override
    public void setUp() {
        binding = (FragmentSubmitRequestBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.rlRetry.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.GONE);
        binding.scrollView.setVisibility(View.VISIBLE);

        coursesList = Utility.getCoursesList(isOptional);
        typeList = Utility.getTypeList();

        binding.issueTopicSpinner.setTag(binding.issueTopicSpinner.getKeyListener());
        binding.issueTopicSpinner.setKeyListener(null);

        binding.courseSpinner.setTag(binding.issueTopicSpinner.getKeyListener());
        binding.courseSpinner.setKeyListener(null);

        for (int i = 0; i < typeList.size(); i++) {
            typeItemsList.add(typeList.get(i).getTypeName());
        }

//        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity(),
//                R.layout.spinner_item, typeItemsList);
//        adapter.setDropDownViewResource(R.layout.dropdown_item);
//        binding.issueTypeSpinner.setAdapter(adapter);

//        ArrayAdapter<String> adapterCourse = new ArrayAdapter<>(activity(),
//                R.layout.spinner_item, coursesList);
//        adapterCourse.setDropDownViewResource(R.layout.dropdown_item);
//        binding.courseSpinner.setAdapter(adapterCourse);

        if (isOptional) {
            binding.courseSpinner.setText("Choose Course");
            binding.courseSpinner.setSelection(binding.issueTopicSpinner.length());
            binding.tlcourseSpinner.setHelperText("Optional");
            courseTypeString = "Choose Course (optional)";
        } else {
            binding.courseSpinner.setText("Choose Course");
            binding.courseSpinner.setSelection(binding.issueTopicSpinner.length());
            courseTypeString = "Choose Course";
        }

        ArrayAdapter<String> courseSpinner = new ArrayAdapter<String>
                (activity(), R.layout.dropdown_item, coursesList);
        //Getting the instance of AutoCompleteTextView
        binding.courseSpinner.setThreshold(1);//will start working from first character
        binding.courseSpinner.setAdapter(courseSpinner);//setting the adapter data into the AutoCompleteTextView
        binding.courseSpinner.setTextColor(Color.BLACK);

        addSpinnerListener();

        if (menuItem != null) {

            type = menuItem.getIssueType();
            topic = menuItem.getIssueTopic();


            if (menuItem.getContactMessage() != null && !menuItem.getContactMessage().isEmpty()) {
                binding.tvIssueMessage.setText(menuItem.getContactMessage());
                binding.etIssueMessage.setVisibility(View.VISIBLE);
            }

            for (int j = 0; j < typeItemsList.size(); j++) {
                if (typeItemsList.get(j).equals(menuItem.getIssueType())) {
//                    binding.issueTypeSpinner.setSelection(j);
                    issueTypeString = menuItem.getIssueType();
                    binding.issueTypeSpinner.setText(menuItem.getIssueType());
                    binding.issueTypeSpinner.setSelection(binding.issueTypeSpinner.length());

                    issueTypeString = typeItemsList.get(j);

                    if (menuItem != null)
                        menuItem.setIssueType(typeItemsList.get(j));

                    topicList = typeList.get(j).getTopicsList();

                    if (menuItem != null) {
                        addTopicSpinner();
                    }
                }
            }

            ArrayAdapter<String> issueTypeSpinner = new ArrayAdapter<String>
                    (activity(), R.layout.dropdown_item, typeItemsList);
            //Getting the instance of AutoCompleteTextView
            binding.issueTypeSpinner.setThreshold(1);//will start working from first character
            binding.issueTypeSpinner.setAdapter(issueTypeSpinner);//setting the adapter data into the AutoCompleteTextView
            binding.issueTypeSpinner.setTextColor(Color.BLACK);

            binding.issueTypeSpinner.setTag(binding.issueTypeSpinner.getKeyListener());
            binding.issueTypeSpinner.setKeyListener(null);


            if (!TextUtils.isEmpty(menuItem.getIssueType()))
                if (menuItem.getIssueType().equals("Technical Issues")) {
                    binding.tlAndroidVersion.setVisibility(View.VISIBLE);
                    binding.tlSmartPhoneModel.setVisibility(View.VISIBLE);
                }
        }

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity(), RecyclerView.HORIZONTAL, false));


        binding.tvMediaUpload.setOnClickListener(v -> {
            if (activity() instanceof HelperActivity) {
                Constants.GALLERY_LIMIT = 5;
                ((HelperActivity) activity()).openCustomGallery();
            }
        });

        binding.rlRetry.setOnClickListener(v -> {
            viewModel.fetchSupportTerms();
        });


        binding.tvSubmit.setOnClickListener(v -> {

            if (viewModel.doValidation(binding.etMessage.getText().toString(), menuItem, isOptional, issueTypeString, courseTypeString
                    , binding.etSmartPhoneModel.getText().toString(), binding.etAndroidVersion.getText().toString())) {
                //viewModel.addFreshDeskTicket(list);

                if (menuItem != null) {
                    if (!type.equals(issueTypeString) && !topicList.equals(courseTypeString))
                        menuItem.setContactMessage("");

                    if (!TextUtils.isEmpty(menuItem.getIssueType()))
                        if (menuItem.getIssueType().equals("Technical Issues")) {
                            menuItem.setPhoneModel(binding.etSmartPhoneModel.getText().toString());
                            menuItem.setAndroidVersion(binding.etAndroidVersion.getText().toString());
                        }
                }

                addFreshDeskTicket();
//                Utility.showToast(activity(), "Ticket", false);

                Mixpanel.submitTicket(courseTypeString.isEmpty() ? "" : courseTypeString, issueTypeString, menuItem, binding.etMessage.getText().toString());
            }
        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getError().observe(this, error -> {
            if (error == null)
                return;
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setError(error.getMessage());
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));
            binding.scrollView.scrollTo(Math.round(binding.getRoot().findViewById(error.getId()).getX()),
                    Math.round(binding.getRoot().findViewById(error.getId()).getY()));
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {

            switch (networkCall.getNetworkTag()) {
                case SubmitRequestViewModel.NetworkTag.FETCH_SUPPORT_TERMS:
                    updateUiForSubmitRequest(networkCall);
                    break;
                case SubmitRequestViewModel.NetworkTag.ADD_FRESH_DESK_TICKET:
                    updateUiForAddTicket(networkCall);
                    break;
            }

        });


//        viewModel.fetchSupportTerms();
        editTextChangeListner();

    }

    private void editTextChangeListner() {

        binding.etMessage.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlMessage.setErrorEnabled(false);
                    binding.tlMessage.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etAndroidVersion.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlAndroidVersion.setErrorEnabled(false);
                    binding.tlAndroidVersion.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etSmartPhoneModel.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlSmartPhoneModel.setErrorEnabled(false);
                    binding.tlSmartPhoneModel.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));

                }
            }
        });
    }

    private void addIssueTopic() {

    }

    private void addFreshDeskTicket() {

        Intent intent = new Intent(activity(), HelperService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_ADD_FRESH_DESK_TICKET);

        intent.putExtra("MenuItem", menuItem);

        intent.putExtra("IssueType", issueTypeString);
        intent.putExtra("Message", binding.etMessage.getText().toString().trim());
        intent.putExtra("CourseType", courseTypeString.equals("") ? null : courseTypeString);
        intent.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) list);

        if (menuItem != null)
            intent.putExtra("MenuItem", menuItem);

        activity().startService(intent);

        Utility.showToast(activity(), "Your ticket is added, we will get back to you soon", true);
        activity().finish();

    }

    private void addSpinnerListener() {

        binding.issueTypeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                binding.tlissueTypeSpinner.setErrorEnabled(false);
                issueTypeString = typeItemsList.get(position);

                if (menuItem != null)
                    menuItem.setIssueType(typeItemsList.get(position));

                topicList = typeList.get(position).getTopicsList();

                if (menuItem != null) {
                    addTopicSpinner();
                }
            }
        });

        binding.courseSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                courseTypeString = coursesList.get(position);
            }
        });

        binding.issueTopicSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                binding.tlissueTopicSpinner.setErrorEnabled(false);

                if (menuItem != null) {
                    menuItem.setIssueTopic(topicList.get(position));

                    if (!TextUtils.isEmpty(menuItem.getIssueType()))
                        if (menuItem.getIssueType().equals("Technical Issues")) {
                            binding.tlAndroidVersion.setVisibility(View.VISIBLE);
                            binding.tlSmartPhoneModel.setVisibility(View.VISIBLE);
                        } else {
                            binding.tlAndroidVersion.setVisibility(View.GONE);
                            binding.tlSmartPhoneModel.setVisibility(View.GONE);
                        }

                    if (!TextUtils.isEmpty(menuItem.getIssueType())) {
                        if (menuItem.getIssueType().equals("Technical Issues")) {
                            binding.tlAndroidVersion.setVisibility(View.VISIBLE);
                            binding.tlSmartPhoneModel.setVisibility(View.VISIBLE);
                        }
                    } else {
                        binding.tlAndroidVersion.setVisibility(View.GONE);
                        binding.tlSmartPhoneModel.setVisibility(View.GONE);
                    }
                }

                if (type.equals(issueTypeString) && topic.equals(topicList.get(position)))
                    binding.etIssueMessage.setVisibility(menuItem.getContactMessage().isEmpty() ? View.GONE : View.VISIBLE);
                else
                    binding.etIssueMessage.setVisibility(View.GONE);

            }
        });
    }

    private void addTopicSpinner() {

        if (menuItem.getIssueTopic() != null && !menuItem.getIssueTopic().isEmpty()) {
//                binding.etIssuetype.setText(queryType);

//            ArrayAdapter<String> adapterIssueTopic = new ArrayAdapter<>(activity(),
//                    R.layout.spinner_item, topicList);
//            adapterIssueTopic.setDropDownViewResource(R.layout.dropdown_item);
//            binding.issueTopicSpinner.setAdapter(adapterIssueTopic);

            boolean isTopicEqual = false;

            for (int i = 0; i < topicList.size(); i++) {
                if (menuItem.getIssueTopic().equals(topicList.get(i))) {
//                    binding.issueTopicSpinner.setSelection(i);
                    binding.issueTopicSpinner.setText(topicList.get(i));
                    binding.issueTopicSpinner.setSelection(binding.issueTopicSpinner.length());
                    menuItem.setIssueTopic(topicList.get(i));

                    if (type.equals(issueTypeString) && topic.equals(topicList.get(i)))
                        binding.etIssueMessage.setVisibility(menuItem.getContactMessage().isEmpty() ? View.GONE : View.VISIBLE);
                    else
                        binding.etIssueMessage.setVisibility(View.GONE);

                    isTopicEqual = true;

                    break;
                } else {
                    isTopicEqual = false;
                    if (type.equals(issueTypeString) && topic.equals(topicList.get(0)))
                        binding.etIssueMessage.setVisibility(menuItem.getContactMessage().isEmpty() ? View.GONE : View.VISIBLE);
                    else
                        binding.etIssueMessage.setVisibility(View.GONE);
                }
            }

            if (!isTopicEqual) {
                menuItem.setIssueTopic(topicList.get(0));
                binding.issueTopicSpinner.setText(topicList.get(0));
                binding.issueTopicSpinner.setSelection(binding.issueTopicSpinner.length());
            }

            ArrayAdapter<String> isstTopicSpinner = new ArrayAdapter<String>
                    (activity(), R.layout.dropdown_item, topicList);
            //Getting the instance of AutoCompleteTextView
            binding.issueTopicSpinner.setThreshold(1);//will start working from first character
            binding.issueTopicSpinner.setAdapter(isstTopicSpinner);//setting the adapter data into the AutoCompleteTextView
            binding.issueTopicSpinner.setTextColor(Color.BLACK);

            binding.tlissueTopicSpinner.setVisibility(View.VISIBLE);
        }
    }

    private void updateUiForAddTicket(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvSubmit.setVisibility(View.GONE);
                break;


            case SUCCESS:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);
                break;

            case FAIL:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);
                break;

            case ERROR:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateUiForSubmitRequest(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.tvMessage.setText(Constants.PLEASE_CHECK_INTERNET);
                binding.rlRetry.setVisibility(View.VISIBLE);
                binding.progressBar.setVisibility(View.GONE);
                binding.scrollView.setVisibility(View.GONE);
                break;

            case IN_PROCESS:
                binding.rlRetry.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.VISIBLE);
                binding.scrollView.setVisibility(View.GONE);
                break;

            case FAIL:
                binding.tvMessage.setText(Constants.SOMETHING_WENT_WRONG);
                binding.rlRetry.setVisibility(View.VISIBLE);
                binding.progressBar.setVisibility(View.GONE);
                binding.scrollView.setVisibility(View.GONE);
                break;

            case SUCCESS:
                binding.rlRetry.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.GONE);
                binding.scrollView.setVisibility(View.VISIBLE);
                break;

            case ERROR:
                binding.tvMessage.setText(Constants.SOMETHING_WENT_WRONG);
                binding.rlRetry.setVisibility(View.VISIBLE);
                binding.progressBar.setVisibility(View.GONE);
                binding.scrollView.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    public void changeTitle(String title) {

    }


    public void updateGalleryFiles(List<GalleryFile> list) {
        if (list != null && list.size() > 0) {
            this.list = list;
            binding.recyclerView.setAdapter(pickedItemAdapter = new PickedItemAdapter(list, activity(), this));
        }
    }

    @Override
    public void deleteItem(GalleryFile galleryFile, int position) {
        if (list != null && list.size() > position) {
            list.remove(position);
            if (pickedItemAdapter != null)
                pickedItemAdapter.notifyDataSetChanged();
        }
    }

    public List<GalleryFile> getSelectedGallery() {
        return list;
    }
}
