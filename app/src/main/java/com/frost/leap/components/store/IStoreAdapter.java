package com.frost.leap.components.store;

import apprepos.catalogue.model.CatalogueModel;

/**
 * Created by Chenna Rao on 10/1/2019.
 */
public interface IStoreAdapter {

    void clickToCatalogueOverview(CatalogueModel catalogueModel, int position, String subCategoryName);

    void enableMike();
}
