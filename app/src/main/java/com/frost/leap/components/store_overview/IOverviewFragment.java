package com.frost.leap.components.store_overview;

import apprepos.store.model.InstructorsModel;

/**
 * Created by Chenna Rao on 10/18/2019.
 */
public interface IOverviewFragment {

    void navigateToInstructorProfile(InstructorsModel instructorsModel);
}
