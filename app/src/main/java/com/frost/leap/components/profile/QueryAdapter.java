package com.frost.leap.components.profile;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.databinding.ItemMenuQueryBinding;
import com.frost.leap.services.HelperService;

import java.util.List;

import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 16-04-2020.
 * Frost
 */
public class QueryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MenuItem> list;
    private IQueryAdpater iQueryAdpater;
    private Context context;
    private String type;

    public QueryAdapter(Context context, List<MenuItem> list, IQueryAdpater iQueryAdpater, String type) {
        this.list = list;
        this.context = context;
        this.iQueryAdpater = iQueryAdpater;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        return new MenuItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_menu_query, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof MenuItemViewHolder) {
            MenuItemViewHolder menuItemViewHolder = (MenuItemViewHolder) viewHolder;
            menuItemViewHolder.bindData(list.get(i), i);
            menuItemViewHolder.binding.llFooter.setOnClickListener(v -> {

                if (type.equals("Phone")) {

                    Toast.makeText(context, "Call Button", Toast.LENGTH_SHORT).show();

                } else {
                    if (list.get(i).getTitle().equals("Phone Support") || list.get(i).getTitle().equals("Email Support"))
                        iQueryAdpater.onMenuClick(list.get(i), i, "Menu");
                    else
                        iQueryAdpater.clickforDescription(list.get(i), i);
                }

            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MenuItemViewHolder extends RecyclerView.ViewHolder {
        private ItemMenuQueryBinding binding;

        public MenuItemViewHolder(ItemMenuQueryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(MenuItem menuItem, int i) {

            if (menuItem.getTitle().equals("Phone Support") || menuItem.getTitle().equals("Email Support"))
                binding.imgGo.setVisibility(View.GONE);
            else
                binding.imgGo.setVisibility(View.VISIBLE);

            binding.llFooter.setVisibility(type.equals("Phone") ? View.GONE : View.VISIBLE);
            binding.llPhoneLayout.setVisibility(type.equals("Phone") ? View.VISIBLE : View.GONE);

            binding.llHeader.setVisibility(menuItem.getHeader() == null ? View.GONE : View.VISIBLE);
            binding.tvHeader.setText(menuItem.getHeader() == null ? "" : menuItem.getHeader());

            if (type.equals("Phone"))
                binding.tvPhoneNumber.setText("(+91) " + menuItem.getTitle());
            else
                binding.tvTitle.setText(Html.fromHtml(menuItem.getTitle()));

            if (type.equals("Phone"))
                binding.tvCity.setText(menuItem.getDescription());
            else {
                if (menuItem.getId() == 10 || menuItem.getId() == 11 || menuItem.getId() == 12 || menuItem.getId() == 13)
                    spannableClick(menuItem, i, binding);
                else
                    binding.tvDescription.setText(menuItem.getDescription());

            }

            binding.llDocumentLayout.setVisibility(menuItem.getId() == 15 ? menuItem.isExpand() ? View.VISIBLE : View.GONE : View.GONE);

            binding.tvDescription.setVisibility(menuItem.isExpand() ? View.VISIBLE : View.GONE);

            binding.imgGo.animate().rotation(menuItem.isExpand()
                    ? -180
                    : -0)
                    .start();

            binding.tvTitle.setTextColor(menuItem.isExpand()
                    ? Color.parseColor("#2c8cf4")
                    : Utility.getColorsFromAttrs(context, R.attr.text_color));

//            binding.tvTitle.setTextColor(Utility.getColorsFromAttrs(context, R.attr.text_color));

            binding.tvContactUsDetails.setVisibility(menuItem.isExpand() ? menuItem.isContact() ? View.VISIBLE : View.GONE : View.GONE);

            if (menuItem.getTitle().equals("Phone Support") || menuItem.getTitle().equals("Email Support")) {
                binding.tvDescription.setVisibility(View.VISIBLE);
            }

            binding.tvHeader.setTextColor(Color.parseColor("#80000000"));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, Utility.dpSize(context, i == list.size() - 1 ? 120 : 0));
            binding.getRoot().setLayoutParams(params);

            binding.tvContactUsDetails.setOnClickListener(v -> {
                iQueryAdpater.onMenuClick(menuItem, i, "ContactUs");
            });

            binding.llPhoneLayout.setOnClickListener(v -> {

                iQueryAdpater.clicktoCall(menuItem, i);

            });

            binding.tvGATECivil.setOnClickListener(v -> {

                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fcivil-engineering%2FGATE-CE.pdf?alt=media"
                        , "GATE Civil Engineering");

            });

            binding.tvGATEMehanical.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fmechanical-engineering%2FGATE-ME.pdf?alt=media",
                        "GATE Mechanical Engineering");
            });

            binding.tvGATEElectrical.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Felectrical-engineering%2FGATE-EE.pdf?alt=media"
                        , "GATE Electrical Engineering");

            });

            binding.tvGATEElectronics.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Felectronics-and-communication-engineering%2FGATE-ECE.pdf?alt=media"
                        , "GATE Electronics Engineering");

            });

            binding.tvGATEInstrumentation.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("", "GATE Civil Engineering");

            });

            binding.tvGATECSE.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fcomputer-science-engineering%2FGATE-CSIT.pdf?alt=media"
                        , "GATE Computer Science and Engineering");

            });

            binding.tvESECivil.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fcivil-engineering%2FESE-CE.pdf?alt=media"
                        , "ESE Civil Engineering");

            });

            binding.tvESEMechanical.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fmechanical-engineering%2FESE-ME.pdf?alt=media"
                        , "ESE Mechanical Engineering");

            });

            binding.tvESEElectrical.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Felectrical-engineering%2FESE-EE.pdf?alt=media"
                        , "ESE Electrical Engineering");

            });

            binding.tvESEElectronics.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Felectronics-and-communication-engineering%2FESE-ECE.pdf?alt=media"
                        , "ESE Electronics Engineering");

            });

            binding.tvESEGeneralStudies.setOnClickListener(v -> {
                iQueryAdpater.redirectTOPDFPage("https://firebasestorage.googleapis.com/v0/b/leap-81741.appspot.com/o/docs%2Fresources%2Fgeneral-studies-and-engineering-aptitude%2FESE-GS.pdf?alt=media"
                        , "ESE General Studies");

            });

        }
    }

    private void spannableClick(MenuItem menuItem, int i, ItemMenuQueryBinding binding) {

        String text = menuItem.getDescription();
        SpannableString ss = new SpannableString(text);

        ClickableSpan otsSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                iQueryAdpater.redirecttoOTSPage(menuItem, i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#007AFF"));
//                ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                ds.setUnderlineText(true);
            }
        };

        ClickableSpan otsApplicationSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                iQueryAdpater.redirecttoOTSAppPage(menuItem, i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#007AFF"));
//                ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                ds.setUnderlineText(true);
            }
        };

        ClickableSpan aceWebsiteSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                iQueryAdpater.redirecttoACEWEBPage(menuItem, i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#007AFF"));
//                ds.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                ds.setUnderlineText(true);
            }
        };

        if (menuItem.getId() == 10)
            ss.setSpan(otsSpan, 215, 229, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        else if (menuItem.getId() == 11)
            ss.setSpan(otsSpan, 219, 233, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        else if (menuItem.getId() == 12) {
            ss.setSpan(otsSpan, 50, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(otsApplicationSpan, 122, 135, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (menuItem.getId() == 13)
            ss.setSpan(aceWebsiteSpan, 134, 148, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        binding.tvDescription.setText(ss);
        binding.tvDescription.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
