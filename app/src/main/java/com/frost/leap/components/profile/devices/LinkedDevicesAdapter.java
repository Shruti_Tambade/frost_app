package com.frost.leap.components.profile.devices;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemLinkedDeviceBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.user.model.LinkedDevice;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public class LinkedDevicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<LinkedDevice> list;
    private RController rController;
    private Context context;

    public LinkedDevicesAdapter(Context context, List<LinkedDevice> list, RController rController) {
        this.list = list;
        this.context = context;
        this.rController = rController;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skeleton_item_linked_device, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else
            return new LinkedDeviceViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_linked_device, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (rController != RController.LOADING) {
            if (viewHolder instanceof LinkedDeviceViewHolder) {
                LinkedDeviceViewHolder linkedDeviceViewHolder = (LinkedDeviceViewHolder) viewHolder;
                linkedDeviceViewHolder.bindData(list.get(position), position);
                linkedDeviceViewHolder.itemView.setOnClickListener(v -> {
                });

            }
        }
    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING ? 10 : list.size();
    }

    public class LinkedDeviceViewHolder extends RecyclerView.ViewHolder {
        private ItemLinkedDeviceBinding binding;

        public LinkedDeviceViewHolder(ItemLinkedDeviceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(LinkedDevice linkedDevice, int position) {
            switch (linkedDevice.getDeviceType()) {
                case "Personal computer":
                    binding.imgResource.setImageResource(("" + linkedDevice.getDeviceOs()).equalsIgnoreCase("WINDOWS")
                            ? R.drawable.ic_window : linkedDevice.getDeviceOs().equalsIgnoreCase("Linux") ? R.drawable.ic_linux : R.drawable.ic_mac_book);
                    break;
                case "Smartphone":
                    binding.imgResource.setImageResource(("" + linkedDevice.getDeviceOs()).equalsIgnoreCase("ANDROID")
                            ? R.drawable.ic_android : R.drawable.ic_iphone);
                    break;
                case "Tablet":
                    binding.imgResource.setImageResource(("" + linkedDevice.getDeviceOs()).equalsIgnoreCase("ANDROID")
                            ? R.drawable.ic_android_tablet : R.drawable.ic_ios_tablet);
                    break;
                default:
                    binding.imgResource.setImageResource(R.drawable.ic_guest);
                    break;
            }

            binding.tvName.setText(linkedDevice.getDeviceOs());
            binding.tvDate.setText(Utility.makeJSDateReadableOther(linkedDevice.getLastLoginTime()));
            binding.tvStatus.setText(linkedDevice.isActive() ? "Active" : "Inactive");
            if (linkedDevice.isActive()) {
                binding.tvStatus.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                binding.tvStatus.setTextColor(Utility.getColorsFromAttrs(context, R.attr.text_color_1));
            }

            binding.llStatus.setBackgroundResource(linkedDevice.isActive() ? R.drawable.corner_radius_green_3 : R.drawable.corner_radius_light_3);
        }
    }
}
