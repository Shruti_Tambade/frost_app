package com.frost.leap.components.player;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentDlVideoPlayerBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DlVideoPlayerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DlVideoPlayerFragment extends BaseFragment implements IDlVideoPlayerFragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentDlVideoPlayerBinding binding;

    public DlVideoPlayerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DlVideoPlayerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DlVideoPlayerFragment newInstance(String param1, String param2) {
        DlVideoPlayerFragment fragment = new DlVideoPlayerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_dl_video_player;
    }


    @Override
    public void setUp() {
        binding = (FragmentDlVideoPlayerBinding) getBaseDataBinding();
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initializePlayer() {

    }

    public void addPlayerListeners() {

    }

    public void setUpVideoUrl() {

    }

    public void setUpPlaylist() {

    }

    public void buildMediaSource() {

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void releasePlayer() {

    }

    public void removeListeners() {

    }

    public void clearVideoHandlers() {

    }

    public void clearOtherHandlers() {

    }


    @Override
    public void playVideo() {

    }

    @Override
    public void pauseVideo() {

    }

    @Override
    public void backToOnline() {

    }

    @Override
    public void showControllers() {

    }

    @Override
    public void hideControllers() {

    }

    @Override
    public void doForward() {

    }

    @Override
    public void doBackward() {

    }

    @Override
    public void playNextVideo() {

    }

    @Override
    public void playPreviousVideo() {

    }

    @Override
    public void rePlayVideo() {

    }

    @Override
    public void reInitializePlayer() {

    }
}
