package com.frost.leap.components.start.introduction;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.start.SignInFragment;
import com.frost.leap.databinding.FragmentIntroductionBinding;

import java.util.Timer;
import java.util.TimerTask;

import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IntroductionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IntroductionFragment extends BaseFragment implements IIntroductionAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentIntroductionBinding binding;

    private int currentPage = 0;
    private Timer timer;
    private final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.


    public IntroductionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment IntroductionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IntroductionFragment newInstance(String param1, String param2) {
        IntroductionFragment fragment = new IntroductionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_introduction;
    }

    @Override
    public void setUp() {

        binding = (FragmentIntroductionBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.viewPager.setAdapter(new IntroductionAdapter(activity(), this));
        binding.viewPager.setClipToPadding(false);
        binding.viewPager.setOffscreenPageLimit(Utility.getBenefitsList().size());

//        binding.benefitViewPager.setCurrentItem(1);

        binding.tabLayout.setupWithViewPager(binding.viewPager, true);

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab1 = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p1 = (ViewGroup.MarginLayoutParams) tab1.getLayoutParams();
            p1.setMargins(0, 0, 9, 0);
            tab1.requestLayout();
        }

        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == Utility.getIntroductionDetails().size()) {
                    currentPage = 0;
                }
                binding.viewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void signInPage() {

    }

    @Override
    public void tryitNowPage() {

    }
}
