package com.frost.leap.components.start;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.activities.BaseActivity;
import com.frost.leap.activities.SplashActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentVerifyOtpBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.start.StartViewModel;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;

import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VerifyOtpFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VerifyOtpFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String key, value;
    private VerifyType verifyType;
    private StartViewModel viewModel;
    private FragmentVerifyOtpBinding binding;
    private CountDownTimer countDownTimer;
    private boolean isResend = false;
    private TextView[] textViews;
    private User user;
    private OTPBroadCastReceiver broadcastReceiver;
    private int wordCount = -1;

    public VerifyOtpFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VerifyOtpFragment newInstance(String key, String value, VerifyType verifyType) {
        VerifyOtpFragment fragment = new VerifyOtpFragment();
        Bundle args = new Bundle();
        args.putString("Key", key);
        args.putString("Value", value);
        args.putInt("VerifyType", verifyType.ordinal());
        fragment.setArguments(args);
        return fragment;
    }


    public static VerifyOtpFragment newInstance(String key, String value, VerifyType verifyType, User user) {
        VerifyOtpFragment fragment = new VerifyOtpFragment();
        Bundle args = new Bundle();
        args.putString("Key", key);
        args.putString("Value", value);
        args.putInt("VerifyType", verifyType.ordinal());
        args.putParcelable("User", user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(StartViewModel.class);
        if (getArguments() != null) {
            key = getArguments().getString("Key");
            value = getArguments().getString("Value");
            verifyType = VerifyType.values()[getArguments().getInt("VerifyType")];
            user = getArguments().getParcelable("User");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_verify_otp;
    }


    @Override
    public void setUp() {
        broadcastReceiver = new OTPBroadCastReceiver();
        registerReceiver();
        setOtpListener();
        binding = (FragmentVerifyOtpBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);
        textViews = new TextView[]{binding.textView1, binding.textView2, binding.textView3, binding.textView4, binding.textView5, binding.textView6};

        String data = "";
        if (key.equals("Mobile")) {
            data = " " + "+91" + " " + value;
        } else {
            data = " " + value;
        }
        final SpannableString text = new SpannableString("Please enter the verification code sent to " + data);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.black)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvHeading.setText(text);


//        if (verifyType == VerifyType.LOGIN) {
//            binding.baseEditText.setFocusable(false);
//            binding.baseEditText.setFocusableInTouchMode(false);
//        }


        binding.imgClose.setOnClickListener(v -> {
            activity().onBackPressed();
        });

        binding.baseEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable value) {
                updateUI(value.toString());
            }
        });

        binding.tvVerify.setOnClickListener(v -> {
            doAction();
        });


        binding.tvResendOtp.setOnClickListener(v -> {
            countDown();
            isResend = true;
            if (verifyType == VerifyType.REGISTER) {
                viewModel.checkUserExist(user);
                return;
            }
            viewModel.resendOTP(verifyType, value);
        });


        viewModel.clearUserData().observe(this, clear -> {
            if (clear) {
                if (activity() != null) {
                    ((BaseActivity) activity()).deleteDashUrls();
                    ((BaseActivity) activity()).deleteOfflineVideo();
                }
            }
        });

        viewModel.getMessage().observe(this, message ->
        {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case StartViewModel.NetworkTag.REQUEST_LOGIN:
                    updateUiForRequestLogin(networkCall);
                    break;
                case StartViewModel.NetworkTag.VERIFY_LOGIN_OTP:
                    updateUiForVerifyOtp(networkCall);
                    break;
                case StartViewModel.NetworkTag.USER_DETAILS:
                    updateUiForUserDetails(networkCall);
                    break;
                case StartViewModel.NetworkTag.REGISTER_USER:
                    updateUiForRegisterUser(networkCall);
                    break;
                case StartViewModel.NetworkTag.CHECK_USER_EXIST:
                    updateUiForCheckUserExist(networkCall);
                    break;


            }
        });

        countDown();
        isResend = true;
    }

    private void doAction() {
        Utility.hideKeyboard(activity());
        if (viewModel.doOTPValidation(binding.baseEditText.getText().toString())) {
            switch (verifyType) {
                case LOGIN:
                    viewModel.verifyLoginOTP(value);
                    break;
                case REGISTER:
                    viewModel.registerUser(user);
                    break;
            }
        }
    }

    private void updateUiForCheckUserExist(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case FAIL:
                Mixpanel.signupResendOTP(user, false);
                break;
            case IN_PROCESS:
                break;
            case SUCCESS:
                Mixpanel.signupResendOTP(user, true);
                showSnackBar("Resent OTP Successfully", 1);
                break;
            case ERROR:
                Mixpanel.signupResendOTP(user, false);
                break;
        }
    }

    private void updateUiForRegisterUser(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvVerify.setVisibility(View.GONE);
                break;

            case SUCCESS:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                viewModel.getUserDetails();
                break;

            case FAIL:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                Mixpanel.createSignUp(true, user, false, "Signup Failure");
                break;

            case UNAUTHORIZED:
                Mixpanel.createSignUp(true, user, false, "Entered Invalid OTP");
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case ERROR:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                Mixpanel.createSignUp(true, user, false, "Signup Failure");
                break;
        }
    }


    @Override
    public void changeTitle(String title) {

    }


    public void countDown() {
        binding.tvResendOtp.setEnabled(false);
        binding.tvResendOtp.setTextColor(activity().getResources().getColor(R.color.light_gray_text));
        countDownTimer = new CountDownTimer(Constants.COUNT_DOWN_TIME, Constants.COUNT_DOWN_INTERVAL) {

            public void onTick(long millisUntilFinished) {
                binding.timer.setVisibility(View.VISIBLE);

                binding.timer.setText("00 : " +
                        ((millisUntilFinished / 1000) < 10
                                ? "0" + (millisUntilFinished / 1000)
                                : millisUntilFinished / 1000));

            }

            public void onFinish() {
                if (activity() == null)
                    return;

                binding.tvResendOtp.setTextColor(ContextCompat.getColor(activity(), R.color.charcoal_gray));
                binding.tvResendOtp.setEnabled(true);
                binding.timer.setVisibility(View.GONE);
            }

        }.start();
    }

    public void updateUI(final String value) {
        if (value.isEmpty()) {
            for (int i = 0; i < textViews.length; i++) {
                textViews[i].setText(" ");
                textViews[i].setBackgroundResource(R.drawable.ic_tv_light_bottom);
            }
            return;
        } else {
            for (int i = 0; i < textViews.length; i++) {
                textViews[i].setText(value.length() > i ? "" + value.charAt(i) : " ");
                textViews[i].setBackgroundResource(value.length() > i ? R.drawable.ic_tv_dark_bottom : R.drawable.ic_tv_light_bottom);
//                textViews[i].setBackgroundResource(value.length() > i ? R.drawable.ic_tv_light_bottom : R.drawable.ic_tv_light_bottom);

            }
        }
    }


    private void updateUiForVerifyOtp(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvVerify.setVisibility(View.GONE);
                break;

            case SUCCESS:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                viewModel.getUserDetails();
                break;

            case FAIL:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                Mixpanel.loginVerify(false, "LogIn Failure", null, value, false);
                break;

            case UNAUTHORIZED:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                Mixpanel.loginVerify(false, "Entered Invalid OTP", null, value, false);
                break;


            case ERROR:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                Mixpanel.loginVerify(false, "LogIn Failure", null, value, false);
                break;
        }
    }

    private void updateUiForUserDetails(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvVerify.setVisibility(View.GONE);
                break;

            case SUCCESS:
                switch (verifyType) {
                    case LOGIN:
                        Mixpanel.loginVerify(true, "Successfully Login", networkCall.getBundle().getParcelable("User"), value, false);
                        break;
                    case REGISTER:
                        Mixpanel.createSignUp(true, networkCall.getBundle().getParcelable("User"), false, "Successfull Signup");
                        break;
                }
                gotoHome();
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case FAIL:
                switch (verifyType) {
                    case LOGIN:
                        Mixpanel.loginVerify(false, "Entered Invalid OTP", null, value, false);
                        break;
                    case REGISTER:
                        Mixpanel.createSignUp(true, user, false, "Signup Failure");
                        break;
                }
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case ERROR:
                switch (verifyType) {
                    case LOGIN:
                        Mixpanel.loginVerify(false, "Entered Invalid OTP", null, value, false);
                        break;
                    case REGISTER:
                        Mixpanel.createSignUp(true, user, false, "Signup Failure");
                        break;
                }
                binding.llLoader.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void gotoHome() {
        if (activity() instanceof SplashActivity) {
            ((SplashActivity) activity()).gotoHome(verifyType == VerifyType.REGISTER ? true : false);
        }
    }

    private void updateUiForRequestLogin(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case FAIL:
                Mixpanel.loginVerifyResend(value, false);

                break;
            case IN_PROCESS:

                break;
            case SUCCESS:
                Mixpanel.loginVerifyResend(value, true);
                showSnackBar("Resent OTP Successfully", 1);
                break;
            case ERROR:
                Mixpanel.loginVerifyResend(value, false);
                break;
        }
    }


    public void setOtpListener() {
        SmsRetrieverClient client = SmsRetriever.getClient(activity());
        // Starts SmsRetriever, waits for ONE matching SMS message until timeout
        // (5 minutes).
        Task<Void> task = client.startSmsRetriever();

        // Listen for success start Task
        task.addOnSuccessListener(aVoid -> {

        });

        //Listen failure of the start Task.
        task.addOnFailureListener(e -> {

        });
    }


    private void registerReceiver() {

        try {
            if (broadcastReceiver == null)
                return;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_UPDATE_OTP);
            activity().registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    public class OTPBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent data) {
            if (data.getBooleanExtra("otp", false)) {
                updateOTP(data.getStringExtra("message"));
            }
        }
    }


    public void updateOTP(String msg) {
        Logger.d("MESSAGE", msg);
        wordCount = viewModel.getOtpWordCount();
        if (wordCount == -1) {
            return;
        }
        final String arg[] = msg.split(" ");
        if (arg.length > wordCount && arg[wordCount - 1].length() == 6) {
            try {
                int otp = Integer.parseInt(arg[wordCount - 1]);
                binding.baseEditText.setText("" + otp);
                doAction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();

        if (broadcastReceiver != null) {
            activity().unregisterReceiver(broadcastReceiver);
        }
    }
}
