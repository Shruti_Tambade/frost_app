package com.frost.leap.components.media.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.frost.leap.components.media.constants.MediaType;


public class GalleryFile implements Parcelable {

    public int id;
    public String name;
    public MediaType mediaType;//it will not add automatically, you have define manually
    public String mimeType;
    public Uri uri;
    public Uri thumbnailUri;
    public String size;
    public String dataFilePath;
    public double ratio = 1;
    public boolean isSelect = false;
    public boolean isCamera = false;
    public String caption = "";

    public GalleryFile() {

    }


    protected GalleryFile(Parcel in) {
        id = in.readInt();
        name = in.readString();
        mimeType = in.readString();
        uri = in.readParcelable(Uri.class.getClassLoader());
        thumbnailUri = in.readParcelable(Uri.class.getClassLoader());
        size = in.readString();
        dataFilePath = in.readString();
        ratio = in.readDouble();
        isSelect = in.readByte() != 0;
        isCamera = in.readByte() != 0;
        caption = in.readString();

        try {
            mediaType= MediaType.valueOf(in.readString());
        } catch (IllegalArgumentException x) {
            mediaType = null;
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(mimeType);
        dest.writeParcelable(uri, flags);
        dest.writeParcelable(thumbnailUri, flags);
        dest.writeString(size);
        dest.writeString(dataFilePath);
        dest.writeDouble(ratio);
        dest.writeByte((byte) (isSelect ? 1 : 0));
        dest.writeByte((byte) (isCamera ? 1 : 0));
        dest.writeString(caption);
        dest.writeString((mediaType == null) ? "" : mediaType.name());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GalleryFile> CREATOR = new Creator<GalleryFile>() {
        @Override
        public GalleryFile createFromParcel(Parcel in) {
            return new GalleryFile(in);
        }

        @Override
        public GalleryFile[] newArray(int size) {
            return new GalleryFile[size];
        }
    };
}
