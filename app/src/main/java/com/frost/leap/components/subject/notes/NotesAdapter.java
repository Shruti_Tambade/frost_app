package com.frost.leap.components.subject.notes;

import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemNoteHeaderBinding;
import com.frost.leap.databinding.ItemNotesBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.subject.model.Note;
import supporters.constants.RController;
import supporters.customviews.views.MySpannable;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 02-01-2020.
 * <p>
 * FROST
 */
public class NotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private RController rController;
    private List<Note> list;
    private INotesAdapter iNotesAdapter;
    private String value;
    private boolean hasFilter;
    private boolean hasSort;
    private String topicName;
    private int filterCount;

    public NotesAdapter(Context context, String value, RController rController, List<Note> list, INotesAdapter iNotesAdapter, boolean hasFilter, int filterCount, boolean hasSort) {
        this.context = context;
        this.value = value;
        this.rController = rController;
        this.list = list;
        this.iNotesAdapter = iNotesAdapter;
        this.hasFilter = hasFilter;
        this.hasSort = hasSort;
        this.filterCount = filterCount;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skelton_subject, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else {
            if (viewType == 0) {
                return new HeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_note_header, viewGroup, false));
            } else
                return new NotesViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_notes, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NotesViewHolder) {
            NotesViewHolder notesViewHolder = (NotesViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 7));
            notesViewHolder.itemView.setLayoutParams(params);

            notesViewHolder.bindData(list.get(position - 1), position - 1);


            notesViewHolder.binding.ivDelete.setOnClickListener(v -> {
                iNotesAdapter.deleteNote(list.get(position - 1), position - 1);
            });


            notesViewHolder.binding.ivEditNotes.setOnClickListener(v -> {
                iNotesAdapter.editNote(list.get(position - 1), position - 1);
            });

            notesViewHolder.binding.constraintLayout9.setOnClickListener(v -> {

            });

        } else if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.binding.tvTitle.setText(value);
            headerViewHolder.binding.ivFilter.setColorFilter(ContextCompat.getColor(context, hasFilter ? R.color.new_primary_blue : R.color.black));
            headerViewHolder.binding.ivSort.setColorFilter(ContextCompat.getColor(context, hasSort ? R.color.new_primary_blue : R.color.black));

            if (filterCount == 0) {
                headerViewHolder.binding.tvFilterCount.setVisibility(View.GONE);
            } else {
                headerViewHolder.binding.tvFilterCount.setVisibility(View.VISIBLE);
                headerViewHolder.binding.tvFilterCount.setText("" + filterCount);
            }

            headerViewHolder.binding.rlFilters.setOnClickListener(v -> {
                iNotesAdapter.applyFilter();
            });

            headerViewHolder.binding.ivSort.setOnClickListener(v -> {
                iNotesAdapter.applySort(hasSort ? false : true);
            });
        }

    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING
                ? 15
                : list.size() + 1;
    }

    public void changeSort(boolean hasSort) {
        topicName = null;
        this.hasSort = hasSort;
        notifyDataSetChanged();
    }

    private class NotesViewHolder extends RecyclerView.ViewHolder {

        private ItemNotesBinding binding;

        public NotesViewHolder(ItemNotesBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bindData(Note note, int position) {

            if (note.getNote() != null && !note.getNote().isEmpty()) {

                binding.tvNote.setText(note.getNote());

//                if (!note.isShowMore()) {
//                    Logger.d("TEXT_LENGTH" + binding.tvNote.length());
//                    if (binding.tvNote.length() >= 350)
//                        makeTextViewResizable(binding.tvNote, 3, ".. See More", true, note);
//                } else {
//                    makeTextViewResizable(binding.tvNote, -1, "Show Less", false, note);
//                }
            }

            if (note.getSubTopicName() != null && !note.getSubTopicName().isEmpty())
                binding.tvSubTopicName.setText(note.getSubTopicName());

            if (note.getCreatedDate() != null && !note.getCreatedDate().isEmpty())
                binding.tvCreatedDate.setText(Utility.makeJSDateReadableOther(note.getCreatedDate()));

//            if (topicName != null && !topicName.isEmpty()) {
//                if (topicName.equals(note.getTopicName())) {
//                    binding.tvCreatedDate.setVisibility(View.GONE);
//                    binding.tvSubTopicName.setVisibility(View.GONE);
//                } else {
//                    binding.tvCreatedDate.setVisibility(View.VISIBLE);
//                    binding.tvTopicName.setVisibility(View.VISIBLE);
//                    topicName = note.getTopicName();
//                }
//            } else {
//                topicName = note.getTopicName();
//            }


            if (note.getTimestampInSecond() >= 0 && note.getVideoLengthInSeconds() >= 0) {

                String timeStamp = (String) ((int) (note.getTimestampInSecond() / 60) + ":" + (long) Utility.roundTwoDecimals(note.getTimestampInSecond() % 60));
                String videoLength = (String) ((int) (note.getVideoLengthInSeconds() / 60) + ":" + (long) Utility.roundTwoDecimals(note.getVideoLengthInSeconds() % 60));

                Logger.d("NOTE_TIME", timeStamp + "FUll_VIDEO_TIME : " + videoLength);

                binding.tvDuration.setText(timeStamp + " / " + videoLength);

            }


        }
    }


    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        private ItemNoteHeaderBinding binding;

        public HeaderViewHolder(@NonNull ItemNoteHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {

        }
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore, Note note) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(note, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(note, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(note, Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(Note note, final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    note.setShowMore(viewMore);
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Show Less", false, note);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. See More", true, note);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);
        }
        return ssb;

    }
}
