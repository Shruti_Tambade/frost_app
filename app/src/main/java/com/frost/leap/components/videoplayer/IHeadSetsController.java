package com.frost.leap.components.videoplayer;

/**
 * Created by R!ZWAN SHEIKH on 2019-10-01.
 * <p>
 * Frost
 */
public interface IHeadSetsController {
    void applyAction();
}
