package com.frost.leap.components.pdf;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentPdfViewerBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.pdf.PdfViewModel;

import java.io.File;

import supporters.constants.Constants;
import supporters.utils.Logger;

import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PdfViewerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PdfViewerFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentPdfViewerBinding binding;
    private PdfViewModel viewModel;
    private String url;

    public PdfViewerFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PdfViewerFragment newInstance(String url) {
        PdfViewerFragment fragment = new PdfViewerFragment();
        Bundle args = new Bundle();
        args.putString("Url", url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(PdfViewModel.class);
        if (getArguments() != null) {
            if (getArguments().getString("Url") != null)
                url = getArguments().getString("Url");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_pdf_viewer;
    }


    @Override
    public void setUp() {
        binding = (FragmentPdfViewerBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);


        if (url == null) {
            showSnackBar("Link is not available", 2);
            return;
        }

        binding.tvRetry.setOnClickListener(v -> {
            viewModel.downloadPdfFile(url, new File(getCacheDir(), Constants.CACHE_PDF_FILE));
        });


        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case PdfViewModel.NetworkTags.FILE_DOWNLOAD:
                    updateUiForPdfFile(networkCall);
                    break;
            }
        });

        viewModel.getPdfFile().observe(this, file -> {

//            binding.pdfView.fromUri(Uri.fromFile(new File(getCacheDir(), Constants.CACHE_PDF_FILE)))
//                    .enableSwipe(true) // allows to block changing pages using swipe
//                    .enableDoubletap(true)
//                    .defaultPage(0)
//                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
//                    .password(null)
//                    .scrollHandle(null)
//                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
//                    // spacing between pages in dp. To define spacing color, set view background
//                    .spacing(15)
//                    .load();
        });
        Logger.d("PDF URL", url);

        viewModel.downloadPdfFile(url, new File(getCacheDir(), Constants.CACHE_PDF_FILE));

    }

    private void updateUiForPdfFile(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.rlRetry.setVisibility(View.VISIBLE);
                binding.progressBar.setVisibility(View.GONE);
                break;
            case IN_PROCESS:
                binding.rlRetry.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.VISIBLE);
                break;
            case ERROR:
            case SUCCESS:
                binding.rlRetry.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }


    @Override
    public void onDestroy() {
        if (viewModel != null)
            viewModel.clearAll();
        super.onDestroy();
    }
}
