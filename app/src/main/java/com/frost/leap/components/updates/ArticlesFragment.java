package com.frost.leap.components.updates;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentArticlesBinding;

import apprepos.updates.model.UpdateModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArticlesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticlesFragment extends BaseFragment implements IUpdateAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EmptyUpdateAdapter emptyUpdateAdapter;
    private FragmentArticlesBinding binding;

    public ArticlesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ArticlesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticlesFragment newInstance(String param1, String param2) {
        ArticlesFragment fragment = new ArticlesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_articles;
    }


    @Override
    public void setUp() {

        binding = (FragmentArticlesBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));

//        binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), "No new articles to show you. You’re all caught up!", R.drawable.update_icon, 1, false));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (emptyUpdateAdapter == null)
            binding.recyclerView.setAdapter(emptyUpdateAdapter = new EmptyUpdateAdapter(this, activity(), "No new articles to show you. You’re all caught up!", R.drawable.ic_article, 2, false));

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void redirectToDoubtPage(UpdateModel updateModel,int position) {

    }

    @Override
    public void retryUpdateAPI() {

    }

    @Override
    public void redirectTOAllUpdates() {

    }

    @Override
    public void openPDF(String pdfFile, String title) {

    }
}
