package com.frost.leap.components.media.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chenna Rao on 14-03-2019.
 **/

public class Media implements Parcelable, Cloneable
{

    @SerializedName("mediaId")
    public String id;

    @SerializedName("_id")
    public String _id;

    @SerializedName("mediaCreditValue")
    public String mediaCreditValue;

    @SerializedName("mediaType")
    public String type = "image";

    public String url;

    @SerializedName("mediaRatio")
    public double ratio;

    @SerializedName("mediaSize")
    public double sizeKB;

    public Uri uri;

    public Uri thumbnailUri;

    public String mimeType;

    public String name;

    public String dataFilePath;

    public String caption = "";


    public Media()
    {

    }


    protected Media(Parcel in) {
        id = in.readString();
        _id = in.readString();
        mediaCreditValue = in.readString();
        type = in.readString();
        url = in.readString();
        ratio = in.readDouble();
        sizeKB = in.readDouble();
        uri = in.readParcelable(Uri.class.getClassLoader());
        thumbnailUri = in.readParcelable(Uri.class.getClassLoader());
        mimeType = in.readString();
        name = in.readString();
        dataFilePath = in.readString();
        caption = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(_id);
        dest.writeString(mediaCreditValue);
        dest.writeString(type);
        dest.writeString(url);
        dest.writeDouble(ratio);
        dest.writeDouble(sizeKB);
        dest.writeParcelable(uri, flags);
        dest.writeParcelable(thumbnailUri, flags);
        dest.writeString(mimeType);
        dest.writeString(name);
        dest.writeString(dataFilePath);
        dest.writeString(caption);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };
}
