package com.frost.leap.components.store_overview;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.frost.leap.R;
import com.frost.leap.databinding.FragmentVideoOverviewBinding;

import apprepos.store.model.CatalogueOverviewModel;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/17/2019.
 */
public class PreviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private CatalogueOverviewModel catalogueOverviewModel;
    private IStoreOverviewAdapter iStoreOverviewAdapter;

    public PreviewAdapter(IStoreOverviewAdapter iStoreOverviewAdapter, Context context, CatalogueOverviewModel catalogueOverviewModel) {
        this.context = context;
        this.iStoreOverviewAdapter = iStoreOverviewAdapter;
        this.catalogueOverviewModel = catalogueOverviewModel;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new PreviewViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.fragment_video_overview, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PreviewViewHolder) {
            PreviewViewHolder previewViewHolder = (PreviewViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    Utility.getScreenWidth(context) - Utility.dpSize(context, 75),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            params.setMargins(
                    Utility.dpSize(context, position == 0 ? 35 : 10),
                    Utility.dpSize(context, 5),
                    Utility.dpSize(context, catalogueOverviewModel.getVideoPaths().size() - 1 == position ? 35 : 5),
                    Utility.dpSize(context, 5));
            previewViewHolder.itemView.setLayoutParams(params);

            previewViewHolder.itemView.setOnClickListener(v -> {
                iStoreOverviewAdapter.playPreviewVideo(position, catalogueOverviewModel.getVideoPaths().get(position).getVideoUrl());
            });

            previewViewHolder.bindData(position);
        }

    }

    @Override
    public int getItemCount() {
        return catalogueOverviewModel.getVideoPaths().size();
    }

    private class PreviewViewHolder extends RecyclerView.ViewHolder {

        private FragmentVideoOverviewBinding binding;

        public PreviewViewHolder(FragmentVideoOverviewBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bindData(int position) {

            if (catalogueOverviewModel.getVideoPaths().get(position).getThumbnail() != null && !catalogueOverviewModel.getVideoPaths().get(position).getThumbnail().isEmpty()) {

                if (Build.VERSION.SDK_INT > 23) {

////                    ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(catalogueOverviewModel.getVideoPaths().get(position).getThumbnail()))
////                            .setPostprocessor(new IterativeBoxBlurPostProcessor(5))
////                            .build();
////
////                    DraweeController controller = Fresco.newDraweeControllerBuilder()
////                            .setImageRequest(request)
////                            .setOldController(binding.ivPreviewImage.getController())
////                            .build();
//                    binding.ivPreviewImage.setController(controller);
                    binding.ivPreviewImage.setImageURI(Uri.parse(catalogueOverviewModel.getVideoPaths().get(position).getThumbnail()));

                } else {

                    Glide.with(context)
                            .load(catalogueOverviewModel.getVideoPaths().get(position).getThumbnail())
                            .into(binding.ivPreviewImage);
                }
            }
        }
    }
}
