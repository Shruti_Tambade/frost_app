package com.frost.leap.components.player;

public interface IDlVideoPlayerFragment {

    void playVideo();

    void pauseVideo();

    void backToOnline();

    void showControllers();

    void hideControllers();

    void doForward();

    void doBackward();

    void playNextVideo();

    void playPreviousVideo();

    void rePlayVideo();

    void reInitializePlayer();


}