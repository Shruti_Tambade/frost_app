package com.frost.leap.components.profile;

import com.frost.leap.components.profile.models.MenuItem;

/**
 * Created by Chenna Rao on 16-04-2020.
 * <p>
 * Frost
 */
public interface IQueryAdpater {

    void onMenuClick(MenuItem menuItem, int position, String clickType);

    void clickContactUs(MenuItem menuItem, int position);

    void clickforDescription(MenuItem menuItem, int position);

    void clicktoCall(MenuItem menuItem, int position);

    void redirecttoOTSPage(MenuItem menuItem, int position);

    void redirecttoOTSAppPage(MenuItem menuItem, int position);

    void redirecttoACEWEBPage(MenuItem menuItem, int position);

    void redirectTOPDFPage(String pdfFile, String title);
}
