package com.frost.leap.components.topics;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.quiz.QuizOverViewFragment;
import com.frost.leap.components.videoplayer.VideoPlayerFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentUnitProviderBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.services.HelperIntentService;
import com.frost.leap.services.PushIntentService;
import com.frost.leap.services.video.CachePlaylistLicenseIntentService;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.topics.TopicsViewModel;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;
import apprepos.topics.model.QuizModel;
import apprepos.topics.model.StudentTopicsModel;
import apprepos.topics.model.SubTopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.video.model.video.PageContentDataModel;
import supporters.constants.Constants;
import supporters.constants.PlayBackSpeed;
import supporters.constants.VideoPlayerState;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link UnitProviderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UnitProviderFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "units";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "subject";
    private boolean changeMotion = false;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentUnitProviderBinding binding;
    private UnitModel unitModel;
    private CatalogueModel catalogueModel;
    private SubjectModel subjectModel;
    private TopicsViewModel viewModel;
    private StudentTopicsModel studentTopicsModel;
    private SubTopicModel subTopicModel;
    private int period = 0;
    private PageContentDataModel pageContentData;
    private RatingModel ratingModel;
    private String videoUrl = null;

    private VideoPlayerFragment videoPlayerFragment;
    private VideoPlayerState videoPlayerState = VideoPlayerState.IN_PORTRAIT_MODE;
    private List<StudentTopicsModel> studentTopicsModelList = new ArrayList<>();

    private OrientationEventListener orientationEventListener;
    private PlayBackSpeed playBackSpeed = PlayBackSpeed.SP1X;
    private boolean isCompleted = false;
    private GestureDetector gestureDetector;

    public UnitProviderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1       Parameter 1.
     * @param param2       Parameter 2.
     * @param subjectModel
     * @return A new instance of fragment UnitProviderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UnitProviderFragment newInstance(UnitModel param1, CatalogueModel param2, SubjectModel subjectModel) {
        UnitProviderFragment fragment = new UnitProviderFragment();
        fragment.unitModel = param1;
        fragment.catalogueModel = param2;
        fragment.subjectModel = subjectModel;
        return fragment;
    }

    public static UnitProviderFragment newInstance() {
        return new UnitProviderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(TopicsViewModel.class);

    }


    @Override
    public int layout() {
        return R.layout.fragment_unit_provider;
    }

    @Override
    public void setUp() {
        gestureDetector = new GestureDetector(activity(), new SingleTapGesture());

        binding = (FragmentUnitProviderBinding) getBaseDataBinding();
        setUpSnackBarView(binding.rootlayout);

        if (unitModel == null) {
            activity().finish();
            return;
        }
//        binding.unitProviderMotion.getConstraintSet(R.id.topContainer).
//                setMargin(R.id.topContainer, ConstraintSet.BOTTOM, Utility.toSdp(R.dimen.bottom_margin));

        if (activity() instanceof MainActivity) {
            ((MainActivity) activity()).passTransitionType(true);
        }

        //Show SkeltonView before data Loading
        binding.skeltonView.setVisibility(View.VISIBLE);
        binding.unitProviderMotion.setVisibility(View.GONE);

        if (!viewModel.isNetworkAvailable())
            viewModel.hasOfflineData(unitModel.getStudentUnitId());

        binding.videoProgressBar.setProgress(0);

        binding.videoProgressBar.setMax(100);

        binding.videoProgressBar.setPadding(0, 0, 0, 0);

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case TopicsViewModel.NetworkTags.VIDEO_URL:
                    updateUiForVideoUrl(networkCall);
                    break;
            }
        });


        viewModel.updateTopics().observe(this, list ->
        {
            this.studentTopicsModelList = list.getStudentTopicsModels();

            // SkeltonView Disable
            binding.skeltonView.setVisibility(View.GONE);
            binding.unitProviderMotion.setVisibility(View.VISIBLE);

            appendPageContentIdList(list.getStudentTopicsModels());

            if (list != null) {
                if (list.getSubTopicModel() != null) {
                    binding.titleText.setText(list.getSubTopicModel().getSubTopicName());
                } else {
                    if (list.getStudentTopicsModels() != null && list.getStudentTopicsModels().size() > 0) {
                        if (list.getStudentTopicsModels().get(0).getStudentContents() != null)
                            for (int i = 0; i < list.getStudentTopicsModels().get(0).getStudentContents().size(); i++) {
                                if (list.getStudentTopicsModels().get(0).getStudentContents().get(i).getContentType().equals("SUBTOPIC")) {
                                    binding.titleText.setText(list.getStudentTopicsModels().get(0).getStudentContents().get(i).getSubTopic().getSubTopicName());
                                    break;
                                }
                            }
                    }
                }
            }

            //getChildFragmentManager().beginTransaction().replace(R.id.bottomFrameLayout, DummyFragment.newInstance(null, null)).commit();
            getChildFragmentManager().beginTransaction().replace(R.id.bottomFrameLayout, UnitOverViewFragment.newInstance(unitModel, catalogueModel, subjectModel, list)).commit();
            getChildFragmentManager().beginTransaction().replace(R.id.topFrameLayout, videoPlayerFragment = VideoPlayerFragment.newInstance(null)).commit();

        });

        viewModel.updateVideoUrl().observe(this, pageContentData -> {
            this.pageContentData = pageContentData;
            videoUrl = pageContentData.getContent().getVideoData().get(0).getVideoDetails().getVideoUrl();
            playVideoUrl(videoUrl);
        });

        viewModel.updateSubTopicRating().observe(this, ratingModelData -> {
            this.ratingModel = ratingModelData;
            for (Fragment fragment : getChildFragmentManager().getFragments()) {
                if (fragment instanceof UnitOverViewFragment) {
                    UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment;
                    unitOverViewFragment.updateRating(ratingModelData);
                    break;
                }
            }
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        viewModel.fetchTopics(unitModel.getStudentUnitId());

        binding.videoProgressBar.getThumb().mutate().setAlpha(0);

        binding.unitProviderMotion.addGestureDetector(gestureDetector);
        binding.unitProviderMotion.setTransitionListener(new MotionLayout.TransitionListener() {
            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {
                Logger.d("onTransitionStarted", i + " " + i1);
                binding.topContainer.setOnTouchListener((v, event) -> true);
                if (videoPlayerFragment != null) {
//                    videoPlayerFragment.hideCustomController();
                }
            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
                Logger.d("onTransitionChange", i + " " + i1 + " " + v);

                if (activity() instanceof MainActivity) {
                    ((MainActivity) activity()).activityBinding.includedAppBarLayout.includedSubLayout.bottomMotion.setProgress(Math.abs(v));
                }

                Logger.d("Progress " + v);

                if (Math.abs(v) >= 0.50) {

                    LinearLayout.MarginLayoutParams params = (LinearLayout.MarginLayoutParams)
                            binding.linearLayout.getLayoutParams();

                    params.leftMargin = binding.topFrameLayout.getRight();

                    if (videoPlayerFragment != null)
                        videoPlayerFragment.controlNetwork(true);

                }


                if (videoPlayerFragment != null) {
                    videoPlayerFragment.hideCustomController();
                }
            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {
                Logger.d("onTransitionCompleted", "" + i);
                binding.topContainer.setOnTouchListener((v, event) -> false);

                switch (i) {
                    case R.id.expanded:
                        inExpandedMode();
                        if (activity() instanceof MainActivity) {
                            ((MainActivity) activity()).passTransitionType(true);
                        }


                        break;
                    case R.id.collapsed:
                        inCollapsedMode();
                        if (activity() instanceof MainActivity) {
                            ((MainActivity) activity()).passTransitionType(false);
                        }
                        break;
                }

            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {
                Logger.d("onTransitionCompleted", "" + i + " " + b + " " + v);

            }
        });

        orientationEventListener = new OrientationEventListener(activity(), SensorManager.SENSOR_DELAY_NORMAL) {

            @SuppressLint("SourceLockedOrientationActivity")
            @Override
            public void onOrientationChanged(int orientation) {
                Logger.d("Sensor", "Orientation changed to " + orientation);
                try {

                    if (android.provider.Settings.System.getInt(activity().getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                        if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                            if (orientation > 70 && orientation < 110) {
                                binding.videoProgressBar.setAlpha(0);

                                controlScreenOrientation(0);
                            } else if (orientation > 270 && orientation < 310) {
                                binding.videoProgressBar.setAlpha(0);

                                controlScreenOrientation(-1);
                            }
                        } else {
                            if (orientation > 0 && orientation < 40) {
                                if (videoPlayerFragment != null)
                                    videoPlayerFragment.hideCustomController();
                                controlScreenOrientation(-1);
                            }
                            if (orientation > 80 && orientation < 120) {
                                activity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                            } else if (orientation > 265 && orientation < 295) {
                                activity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                            }

                        }
                    } else {

                        if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
                            if (orientation > 80 && orientation < 120) {
                                activity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                            } else if (orientation > 265 && orientation < 295) {
                                activity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        ;

        if (orientationEventListener.canDetectOrientation() == true) {
            Logger.d("Sensor",
                    "Can detect orientation");
            orientationEventListener.enable();
        } else {
            Logger.d("Sensor",
                    "Cannot detect orientation");
            orientationEventListener.disable();
        }


        /*binding.videoSettings.setOnClickListener(v -> {
            onSettingsClicked();
        });*/


    }

    private void updateUiForVideoUrl(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                if (videoPlayerFragment != null)
                    videoPlayerFragment.releasePlayer();
                break;
            case IN_PROCESS:
                if (videoPlayerFragment != null)
                    videoPlayerFragment.releasePlayer();
                break;
        }
    }

    private void appendPageContentIdList(List<StudentTopicsModel> studentTopicsModels) {

        List pageContenIdsList = new ArrayList<>();

        for (int i = 0; i < studentTopicsModels.size(); i++) {
            for (int j = 0; j < studentTopicsModels.get(i).getStudentContents().size(); j++) {
                if (studentTopicsModels.get(i).getStudentContents().get(j).getContentType().equals("SUBTOPIC")) {
                    pageContenIdsList.add(studentTopicsModels.get(i).getStudentContents().get(j).getSubTopic().getPageContentId());
                }
            }
        }

        //Post PAGECONTENT IDS in Background Service
        Intent intent = new Intent(activity(), PushIntentService.class);
        intent.putExtra(Constants.RECEIVER, new LicenseResultReceiver(new Handler()));
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_GET_VIDEO_URLS);
        intent.putStringArrayListExtra("VIDEO_URLS", (ArrayList<String>) pageContenIdsList);
        activity().startService(intent);
    }

    public void controlScreenOrientation(int orientation) {
        if (videoPlayerFragment != null)
            videoPlayerFragment.controlScreenOrientation(orientation);
    }

    private void inExpandedMode() {

        binding.videoProgressBar.setPadding(0, 0, 0, 0);

        binding.videoProgressBar.getThumb().mutate().setAlpha(0);

        binding.videoProgressBar.setOnTouchListener((v, event) -> false);

        if (videoPlayerFragment != null) {
            //videoPlayerFragment.hideCustomController();
            videoPlayerFragment.inTouchMode(false);
            videoPlayerFragment.controlNetwork(false);

        }

        if (orientationEventListener.canDetectOrientation() == true) {
            Logger.d("Sensor",
                    "Can detect orientation");
            orientationEventListener.enable();
        }

    }


    private void inCollapsedMode() {

        binding.linearLayout.setVisibility(View.VISIBLE);
        binding.videoProgressBar.getThumb().mutate().setAlpha(0);
        binding.videoProgressBar.setOnTouchListener((v, event) -> true);

        if (videoPlayerFragment != null) {
            videoPlayerFragment.inTouchMode(true);
            videoPlayerFragment.hideCustomController();
            videoPlayerFragment.controlNetwork(true);
        }


        LinearLayout.MarginLayoutParams params = (LinearLayout.MarginLayoutParams)
                binding.linearLayout.getLayoutParams();

        params.leftMargin = binding.topFrameLayout.getRight();


        if (videoPlayerFragment != null)
            if (videoPlayerFragment.isPlaying()) {
                setPauseIcon();
            } else {
                setPlayIcon();
            }


        binding.play.setOnClickListener(v -> {
            if (videoPlayerFragment != null) {
                if (videoPlayerFragment.isPlaying()) {
                    binding.play.setImageResource(R.drawable.ic_play_arrow_black);
                    videoPlayerFragment.pauseVideo();
                } else {
                    binding.play.setImageResource(R.drawable.ic_pause_black);
                    videoPlayerFragment.playVideo();
                }
            }
            hideSeekThumb();

        });

        binding.cancel.setOnClickListener(v -> {
            releasePlayer();

            if (activity() instanceof MainActivity) {
                ((MainActivity) activity()).removeFragment(this);
            }
        });


        if (orientationEventListener != null)
            orientationEventListener.disable();


    }


    private void playVideoUrl(String videoUrl) {
        //Enabling Motion for the Video
        period = 0;
        releasePlayer();

        binding.videoProgressBar.setVisibility(View.GONE);
        binding.videoProgressBar.setProgress(0);
        binding.topContainer.setOnTouchListener((v, event) -> false);
        if (videoPlayerFragment == null)
            getChildFragmentManager().beginTransaction().replace(R.id.topFrameLayout,
                    videoPlayerFragment = VideoPlayerFragment.newInstance(videoUrl)).commit();
        else {
            videoPlayerFragment.initializePlayer(videoUrl, true, null);
        }
        binding.topContainer.setVisibility(View.VISIBLE);
        if (activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || activity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            new Handler().post(() -> {
                videoPlayerFragment.updateAsView(true);
            });
        } else {
            binding.videoProgressBar.setVisibility(View.VISIBLE);
            binding.videoProgressBar.setAlpha(1);
            new Handler().post(() -> {
                videoPlayerFragment.updateAsView(false);
            });
            if (binding.unitProviderMotion.getCurrentState() == R.id.collapsed) {
                inCollapsedMode();
            }

        }
    }

    @Override
    public void changeTitle(String title) {

    }

    public void playSubTopicVideo(SubTopicModel subTopic, StudentTopicsModel studentTopicsModel) {

        ShareDataManager.getInstance().setSubTopicPercentage(subTopic == null ? 0 : subTopic.getVideoCompletionPercentage());
        ShareDataManager.getInstance().setSubTopicCurrentPosition(subTopic == null ? 0 : (subTopic.getVideoCompletionPercentage() > 99 ? 0 : (long) (subTopic.getTotalVideoDuration() * subTopic.getVideoCompletionPercentage() / 100 * 1000)));
        viewModel.fetchVideoUrl(subTopic.getPageContentId());
        viewModel.fetchSubtopicsRating(subTopic.getStudentSubTopicId());

        binding.titleText.setText(subTopic.getSubTopicName());

        if (studentTopicsModel != null) {

            this.studentTopicsModel = studentTopicsModel;
            this.subTopicModel = subTopic;

            viewModel.updateVideoStream(subjectModel.getStudentSubjectId(), unitModel.getStudentUnitId(), studentTopicsModel.getStudentTopicId(), 0, subTopic.getStudentSubTopicId());
            Logger.d("Student_SubjectID :" + subjectModel.getStudentSubjectId());
            Logger.d("Student_UnitID :" + unitModel.getStudentUnitId());
            Logger.d("Student_TopicID :" + studentTopicsModel.getStudentTopicId());
            Logger.d("Student_SubTopicID :" + subTopic.getStudentSubTopicId());
        }

        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (fragment instanceof UnitOverViewFragment) {
                UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment;
                if (studentTopicsModel != null)
                    unitOverViewFragment.playSubTopicVideo(subTopic, studentTopicsModel);
                else
                    unitOverViewFragment.playSubTopicVideo(subTopic, subTopic.getTopicName(), studentTopicsModel.getStudentTopicId());
            }
        }

    }

    public void endTransaction() {
        binding.unitProviderMotion.post(() -> {
            binding.unitProviderMotion.transitionToEnd();
        });
    }

    public void startTransaction() {
        binding.unitProviderMotion.post(() -> {
            binding.unitProviderMotion.transitionToStart();
        });
    }

    public void enableFullScreen() {

        if (videoPlayerFragment != null && videoPlayerFragment.getPlayerView() != null) {
            videoPlayerFragment.getPlayerView().setResizeMode(ShareDataManager.getInstance().getAspectRatio());
        }

        binding.unitProviderMotion.loadLayoutDescription(R.xml.fullscreen_scene);
        binding.videoProgressBar.setOnTouchListener((v, event) -> true);
        binding.videoProgressBar.setAlpha(0);

    }

    public void disableFullScreen() {
        if (videoPlayerFragment != null && videoPlayerFragment.getPlayerView() != null)
            videoPlayerFragment.getPlayerView().setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);

        binding.videoProgressBar.setOnTouchListener((v, event) -> false);
        binding.videoProgressBar.setAlpha(1);
        binding.unitProviderMotion.post(() -> {
            binding.unitProviderMotion.loadLayoutDescription(R.xml.unit_provider_scene);
            if (changeMotion) {
                binding.unitProviderMotion.getConstraintSet(R.id.topContainer).
                        setMargin(R.id.topContainer, ConstraintSet.BOTTOM, Utility.toSdp(R.dimen.bottom_margin));
                binding.topContainer.requestLayout();
            }
        });

    }


    public void setPlayIcon() {
        binding.play.setImageResource(R.drawable.ic_play_arrow_black);
    }

    public void setPauseIcon() {
        binding.play.setImageResource(R.drawable.ic_pause_black);
    }


    public void setControls() {

        if (videoPlayerFragment != null)
            if (videoPlayerFragment.isPlaying()) {
                setPauseIcon();
            } else {
                setPlayIcon();
            }
    }


    public SeekBar getSeekBar() {
        return binding.videoProgressBar;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void changeMotion(boolean change) {
        if (change) {
            changeMotion = true;
            binding.unitProviderMotion.getConstraintSet(R.id.topContainer).
                    setMargin(R.id.topContainer, ConstraintSet.BOTTOM, Utility.toSdp(R.dimen.bottom_margin));
            binding.topContainer.requestLayout();

        } else {
            changeMotion = false;
            binding.unitProviderMotion.getConstraintSet(R.id.topContainer).
                    setMargin(R.id.topContainer, ConstraintSet.BOTTOM, Utility.toSdp(R.dimen._15sdp));
            binding.topContainer.requestLayout();
        }
    }


    public void openQuiz(QuizModel quiz, String subjectId, String unitId, String topicId) {

        //Disable MotionLayout for Quiz
        binding.topContainer.setOnTouchListener((v, event) -> true);

        getChildFragmentManager().beginTransaction().replace(R.id.topFrameLayout, QuizOverViewFragment.newInstance(quiz, subjectId, unitId, topicId)).commit();
        binding.videoProgressBar.setVisibility(View.GONE);
        binding.videoProgressBar.setAlpha(0);
    }


    public void showSeekThumb() {
        binding.videoProgressBar.getThumb().mutate().setAlpha(255);
    }

    public void hideSeekThumb() {
        binding.videoProgressBar.getThumb().mutate().setAlpha(0);
    }


    public void updateVideoStream(long currentPosition, int playerState, boolean playWhenReady) {

        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (fragment instanceof UnitOverViewFragment) {
                UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment;
                unitOverViewFragment.updateVideoStream(currentPosition);
                break;
            }

        }


        if (studentTopicsModel != null && subTopicModel != null && playerState != Player.STATE_IDLE) {
            if (period % 13 == 0 || playerState == Player.STATE_ENDED) {
                try {


                    if (playerState == Player.STATE_ENDED) {
                        Logger.d("STATE", "Player.STATE_ENDED");
                        if (isCompleted) {
                            return;
                        }
                        isCompleted = true;

                    } else {
                        isCompleted = false;
                    }

                    if (!viewModel.isNetworkAvailable()) {
                        return;
                    }

                    Logger.d("ACTION_UPDATE_VIDEO_STREAM", "Updating Sub-Topic Progress");

                    Intent intent = new Intent(activity(), HelperIntentService.class);
                    intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_VIDEO_STREAM);
                    intent.putExtra("StudentSubjectId", subjectModel.getStudentSubjectId());
                    intent.putExtra("UnitId", unitModel.getStudentUnitId());
                    intent.putExtra("TopicId", studentTopicsModel.getStudentTopicId());
                    intent.putExtra("SubTopicId", subTopicModel.getStudentSubTopicId());
                    intent.putExtra("VideoCompletion", currentPosition);
                    activity().startService(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                period = 0;
            }
        }

        period++;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (orientationEventListener != null)
            orientationEventListener.disable();
    }

    public void playPauseVideo(boolean isPlay) {

        if (videoPlayerFragment != null) {
            if (!isPlay) {
                binding.play.setImageResource(R.drawable.ic_play_arrow_black);
                videoPlayerFragment.pauseVideo();

            } else {
                binding.play.setImageResource(R.drawable.ic_pause_black);
                videoPlayerFragment.playVideo();
            }
        }
    }

    public boolean isPlaying() {
        if (videoPlayerFragment != null) {
            return videoPlayerFragment.isPlaying();
        }
        return false;
    }

    public void releasePlayer() {
        if (videoPlayerFragment != null) {
            videoPlayerFragment.releasePlayer();
            videoPlayerFragment.clearHandlers();
        }
    }

    public void autoPlayNext() {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (fragment instanceof UnitOverViewFragment) {
                UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment;
                unitOverViewFragment.autoPlayNext();
                break;
            }
        }
    }

    public void autoPlayPrevious() {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (fragment instanceof UnitOverViewFragment) {
                UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment;
                unitOverViewFragment.autoPlayPrevious();
                break;
            }
        }
    }


    public void reportAboutVideo() {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            if (fragment instanceof UnitOverViewFragment) {
                UnitOverViewFragment unitOverViewFragment = (UnitOverViewFragment) fragment;
                unitOverViewFragment.reportPage();
                break;
            }
        }
    }

    public void clearAutoPlay() {
        if (videoPlayerFragment != null)
            videoPlayerFragment.clearAutoPlay();
    }


    public void updateFullScreenScene() {
        binding.unitProviderMotion.loadLayoutDescription(R.xml.fullscreen_scene);
    }

    private class LicenseResultReceiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public LicenseResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == 1) {
                try {
                    Intent intent = new Intent(activity(), CachePlaylistLicenseIntentService.class);
                    intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_CACHE_PLAYLIST_URLS_LICENSE);
                    intent.putExtra("PageContentIds", resultData.getStringArrayList("PageContentIds"));
                    activity().startService(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private class SingleTapGesture extends GestureDetector.SimpleOnGestureListener {

        private long previousTime;

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            if (activity() == null)
                return super.onDoubleTap(e);
            if (binding.unitProviderMotion.getProgress() > 0)
                return super.onDoubleTap(e);

            previousTime = System.currentTimeMillis();

            if (e.getX() < (Utility.getScreenWidth(activity()) / 2) - 250) {
                ShareDataManager.getInstance().setTapLiveData(-2);
            } else if ((Utility.getScreenWidth(activity()) / 2 + 250) < e.getX()) {
                ShareDataManager.getInstance().setTapLiveData(2);
            }
            return super.onDoubleTap(e);
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (binding.unitProviderMotion.getProgress() > 0)
                return super.onSingleTapConfirmed(e);

            if (previousTime >= System.currentTimeMillis() - 800) {
                return false;
            }

            ShareDataManager.getInstance().setTapLiveData(1);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent ev) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                float distanceY) {

            return false;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {

            return false;
        }
    }

}



