package com.frost.leap.components.subject.ots;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.catalogue.CatalogueFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentOtsBinding;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OTSFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OTSFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentOtsBinding binding;
    private boolean isOTSDone = false;
    private String studentCatalogueId = null;

    public OTSFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static OTSFragment newInstance(boolean isOTSDone, String studentCatalogueId) {
        OTSFragment fragment = new OTSFragment();
        Bundle args = new Bundle();
        args.putBoolean("IsOTSDone", isOTSDone);
        args.putString("StudentCatalogueId", studentCatalogueId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isOTSDone = getArguments().getBoolean("IsOTSDone", false);
            studentCatalogueId = getArguments().getString("StudentCatalogueId");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_ots;
    }


    @Override
    public void setUp() {
        binding = (FragmentOtsBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);


        binding.tvOTSLogin.setVisibility(isOTSDone ? View.VISIBLE : View.GONE);
        binding.tvClaimStudyMaterial.setVisibility(!isOTSDone ? View.VISIBLE : View.GONE);

        binding.tvClaimStudyMaterial.setOnClickListener(v -> {
            gotoClaimStudyMaterial();
        });


        binding.tvOTSLogin.setOnClickListener(v -> {
            gotoOTSLogin();
        });


    }

    private void gotoOTSLogin() {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.OTS_LOGIN_URL));
            startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
            ClipboardManager clipboardManager = (ClipboardManager) activity().getSystemService(CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("text", Constants.OTS_LOGIN_URL);
            clipboardManager.setPrimaryClip(clipData);
//            Utility.showToast(activity(), "OTS url is copied to clipboard", false);
        }
    }

    private void gotoClaimStudyMaterial() {
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.CLAIM_FORM);
        intent.putExtra("StudentCatalogueId", studentCatalogueId);
        startActivityForResult(intent, Utility.generateRequestCodes().get("OTS_REGISTRATION"));
    }

    @Override
    public void changeTitle(String title) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST CODE", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("OTS_REGISTRATION")) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                isOTSDone = true;
                binding.tvOTSLogin.setVisibility(isOTSDone ? View.VISIBLE : View.GONE);
                binding.tvClaimStudyMaterial.setVisibility(!isOTSDone ? View.VISIBLE : View.GONE);

                if (activity() instanceof MainActivity) {
                    List<Fragment> list = ((MainActivity) activity()).getSupportFragmentManager().getFragments();
                    for (Fragment fragment : list) {
                        if (fragment instanceof CatalogueFragment) {
                            CatalogueFragment catalogueFragment = (CatalogueFragment) fragment;
                            catalogueFragment.refreshCatalogues();
                            break;
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onDestroyView() {
        if (binding != null)
            binding.unbind();
        super.onDestroyView();
    }
}
