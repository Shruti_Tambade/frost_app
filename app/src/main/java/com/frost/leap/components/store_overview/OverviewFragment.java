package com.frost.leap.components.store_overview;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentOverviewBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.payment.PaymentViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.store.model.CouponModel;
import apprepos.store.model.FeaturesModel;
import apprepos.store.model.InstructorsModel;
import apprepos.store.model.PriceModel;
import apprepos.store.model.SubscriptionModel;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.customviews.views.MySpannable;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverviewFragment extends BaseFragment implements IStoreOverviewAdapter, IOverviewFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentOverviewBinding binding;
    private boolean isCheckedDiscount = false;
    private CatalogueOverviewModel catalogueOverviewModel;
    private PaymentViewModel viewModel;
    private String orderId;
    private boolean isPayment = true;
    private CouponModel couponModel;
    private PreviewAdapter previewAdapter;

    private EnrollAdapter enrollAdapter;
    private int pricePosition = 0;
    private boolean isDiscount = false;
    private String subScriptionType = "";

    private String courseName, coursePrice, apppliedCoupon, purChasedPrice;

    public OverviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OverviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OverviewFragment newInstance(String param1, String param2) {
        OverviewFragment fragment = new OverviewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(PaymentViewModel.class);
        catalogueOverviewModel = ((StoreOverviewFragment) getParentFragment()).catalogueOverviewModel;

    }

    @Override
    public int layout() {
        return R.layout.fragment_overview;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setUp() {
        binding = (FragmentOverviewBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        if (catalogueOverviewModel == null) {
            return;
        }

        binding.enrollRecyclerview.setNestedScrollingEnabled(false);

        binding.tvLearnDescription.setText(catalogueOverviewModel.getCatalogueDescription());

        Logger.d("TEXT_LENGTH" + binding.tvLearnDescription.length());
        if (binding.tvLearnDescription.length() >= 550)
            makeTextViewResizable(binding.tvLearnDescription, 5, ".. See More", true);

        if (catalogueOverviewModel.getPrices() != null && catalogueOverviewModel.getPrices().size() > 0) {

            binding.enrollRecyclerview.setLayoutManager(new LinearLayoutManager(activity()));
            binding.enrollRecyclerview.setAdapter(enrollAdapter = new EnrollAdapter(activity(), catalogueOverviewModel.getPrices(), this));

            catalogueOverviewModel.getPrices().get(0).setCheched(true);
            enrollAdapter.notifyDataSetChanged();


            binding.tvOriginalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(0).getActualPrice());
        }

        if (catalogueOverviewModel.getPrices() != null && catalogueOverviewModel.getPrices().size() > 0) {
            if (catalogueOverviewModel.getPrices().get(0).getDiscount() > 0) {
                isDiscount = true;

                binding.discountLayout.setVisibility(View.VISIBLE);
                binding.tvDiscountPercentage.setText(new DecimalFormat("##.##").format(catalogueOverviewModel.getPrices().get(0).getDiscount()) + "% Discount" + " - " + catalogueOverviewModel.getPrices().get(0).getDiscountName());
                binding.tvtotalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(0).getFinalPrice());
                binding.tvOriginalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(0).getActualPrice());
                binding.tvDiscountPrice.setText("- ₹ " + new DecimalFormat("##.##").format((catalogueOverviewModel.getPrices().get(0).getActualPrice() - catalogueOverviewModel.getPrices().get(0).getFinalPrice())));

                binding.totalView.setVisibility(View.VISIBLE);
//            binding.tvProceedToPay.setText("Buy");

            } else {
                binding.discountLayout.setVisibility(View.GONE);
//            binding.tvProceedToPay.setText("Buy     |     " + catalogueOverviewModel.getPrices().get(0).getFinalPrice());
                binding.tvtotalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(0).getFinalPrice());
                binding.totalView.setVisibility(View.GONE);
            }
        }


        /**
         * Preview ViewPager with Dots
         */
//        binding.previewViewPager.setPageMargin(35);
//        binding.previewViewPager.setAdapter(new StoreViewPagerAdapter(activity(), catalogueOverviewModel.getVideoPaths().size(), catalogueOverviewModel, 0));
//        binding.previewViewPager.setClipToPadding(false);
//        binding.previewViewPager.setOffscreenPageLimit(catalogueOverviewModel.getVideoPaths().size());
//        binding.previewViewPager.setPadding(95, 0, 95, 0);

        binding.previewRecyclerview.setLayoutManager(new LinearLayoutManager(activity(), LinearLayoutManager.HORIZONTAL, false));
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(binding.previewRecyclerview);
        binding.previewRecyclerview.setAdapter(previewAdapter = new PreviewAdapter(this, activity(), catalogueOverviewModel));
        binding.tabLayout.setupWithViewPager(binding.previewViewPager, true);

//        binding.previewRecyclerview.smoothScrollToPosition(1);

        for (int i = 0; i < binding.tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) binding.tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(Utility.dpSize(activity(), 35), 0, 9, 0);
            tab.requestLayout();
        }

        /**
         * BenefitModel ViewPager with Dots
         */
        binding.benefitViewPager.setPageMargin(Utility.dpSize(activity(), 15));
        binding.benefitViewPager.setAdapter(new StoreViewPagerAdapter(this, activity(), Utility.getBenefitsList().size(), 1, Utility.getBenefitsList()));
        binding.benefitViewPager.setClipToPadding(false);
        binding.benefitViewPager.setOffscreenPageLimit(Utility.getBenefitsList().size());
        binding.benefitViewPager.setPadding(Utility.dpSize(activity(), 35), 0, Utility.dpSize(activity(), 35), 0);

//        binding.benefitViewPager.setCurrentItem(1);

        binding.benefitTablayout.setupWithViewPager(binding.benefitViewPager, true);

        for (int i = 0; i < binding.benefitTablayout.getTabCount(); i++) {
            View tab1 = ((ViewGroup) binding.benefitTablayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p1 = (ViewGroup.MarginLayoutParams) tab1.getLayoutParams();
            p1.setMargins(0, 0, 9, 0);
            tab1.requestLayout();
        }

        /**
         * Instructors ViewPager with Dots
         */

        if (catalogueOverviewModel.getInstructors() != null && catalogueOverviewModel.getInstructors().size() > 0) {
            int value;
            if (catalogueOverviewModel.getInstructors().size() % 3 == 0)
                value = 0;
            else
                value = 1;

            List<List<InstructorsModel>> instructorsList = new ArrayList<>();
            List<InstructorsModel> instructorsModels = catalogueOverviewModel.getInstructors();
            int j = 0;
            int k = 0;

            for (int i = 0; i < (catalogueOverviewModel.getInstructors().size() / 3) + value; i++) {
                List<InstructorsModel> instructors = new ArrayList<>();
                outerloop:
                for (j = j; j < instructorsModels.size(); j++) {
                    if (k == 3) {
                        k = 0;
                        break;
                    }
                    instructors.add(instructorsModels.get(j));
                    k++;
                }
                instructorsList.add(instructors);
            }

            // update size
            ViewGroup.LayoutParams layoutParams = binding.instructorsViewPager.getLayoutParams();
            layoutParams.width = Utility.getScreenWidth(activity());
            layoutParams.height = catalogueOverviewModel.getInstructors().size() * Utility.toSdp(R.dimen._87sdp);
            binding.instructorsViewPager.setLayoutParams(layoutParams);

            binding.instructorsViewPager.setPageMargin(15);
            binding.instructorsViewPager.setAdapter(new StoreViewPagerAdapter(this, activity(), (catalogueOverviewModel.getInstructors().size() / 3) + value, 2, instructorsList));
            binding.instructorsViewPager.setClipToPadding(false);
            binding.instructorsViewPager.setOffscreenPageLimit((catalogueOverviewModel.getInstructors().size() / 3) + value);
            binding.instructorsViewPager.setPadding(55, 0, 85, 0);

            binding.instructorsTabLayout.setupWithViewPager(binding.instructorsViewPager, true);

            for (int i = 0; i < binding.instructorsTabLayout.getTabCount(); i++) {
                View tab1 = ((ViewGroup) binding.instructorsTabLayout.getChildAt(0)).getChildAt(i);
                ViewGroup.MarginLayoutParams p1 = (ViewGroup.MarginLayoutParams) tab1.getLayoutParams();
                p1.setMargins(0, 0, 9, 0);
                tab1.requestLayout();
            }
        } else {
            binding.instructorsViewPager.setVisibility(View.GONE);
            binding.instructorsTabLayout.setVisibility(View.GONE);
            binding.tvInstructors.setVisibility(View.GONE);
        }

        /**
         * Setting the Features Data
         */
        int value1;
        if (Utility.getFeaturesList().size() % 3 == 0)
            value1 = 0;
        else
            value1 = 1;

        List<List<FeaturesModel>> featuresModelArrayList = new ArrayList<>();
        List<FeaturesModel> featuresModelList = Utility.getFeaturesList();
        int j1 = 0;
        int k1 = 0;

        for (int i = 0; i < (Utility.getFeaturesList().size() / 3) + value1; i++) {
            List<FeaturesModel> featuresModels = new ArrayList<>();
            outerloop:
            for (j1 = j1; j1 < featuresModelList.size(); j1++) {
                if (k1 == 3) {
                    k1 = 0;
                    break;
                }
                featuresModels.add(featuresModelList.get(j1));
                k1++;
            }
            featuresModelArrayList.add(featuresModels);
        }

        binding.featureViewPager.setAdapter(new StoreViewPagerAdapter(this, activity(), (Utility.getFeaturesList().size() / 3) + value1, 3, featuresModelArrayList));
        binding.featureViewPager.setClipToPadding(false);
        binding.featureViewPager.setOffscreenPageLimit((Utility.getFeaturesList().size() / 3) + value1);

        binding.featureTablayout.setupWithViewPager(binding.featureViewPager, true);

        for (int i = 0; i < binding.featureTablayout.getTabCount(); i++) {
            View tab1 = ((ViewGroup) binding.featureTablayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p1 = (ViewGroup.MarginLayoutParams) tab1.getLayoutParams();
            p1.setMargins(0, 0, 9, 0);
            tab1.requestLayout();
        }


        binding.tvViewCurriculum.setOnClickListener(v -> {
            ((HelperActivity) activity()).changeTab();
            Mixpanel.clickCurriculum(catalogueOverviewModel.getCatalogueName(), false);
        });


        binding.tvProceedToPay.setOnClickListener(v -> {

            if (isPayment) {

                this.courseName = catalogueOverviewModel.getCatalogueName();
                this.coursePrice = binding.tvOriginalPrice.getText().toString();
                this.purChasedPrice = binding.tvtotalPrice.getText().toString();

                if (couponModel != null) {
                    viewModel.generateOrder(catalogueOverviewModel.getCatalogueId(),
                            catalogueOverviewModel.getCatalogueName(),
                            catalogueOverviewModel.getPrices().get(pricePosition).getPriceId(), couponModel.getCouponId());


                    this.apppliedCoupon = binding.etCoupon.getText().toString();
                    Mixpanel.clickCompleteOrder(catalogueOverviewModel.getCatalogueName(), subScriptionType, binding.tvtotalPrice.getText().toString(), binding.etCoupon.getText().toString());

                } else {
                    viewModel.generateOrder(catalogueOverviewModel.getCatalogueId(),
                            catalogueOverviewModel.getCatalogueName(),
                            catalogueOverviewModel.getPrices().get(pricePosition).getPriceId(), null);

                    Mixpanel.clickCompleteOrder(catalogueOverviewModel.getCatalogueName(), subScriptionType, binding.tvtotalPrice.getText().toString(), null);
                }
            } else {

                Mixpanel.ClickGetStarted(catalogueOverviewModel.getCatalogueName());
                gotoCatalogueOverview();
            }
        });


        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case PaymentViewModel.NetworkTag.GENERATE_ORDER:
                    updateUiForGenerateOrder(networkCall);
                    break;

                case PaymentViewModel.NetworkTag.UPDATE_ORDER_STATUS:
                    updateUiForOrderStatus(networkCall);
                    break;

                case PaymentViewModel.NetworkTag.GET_COUPON_INFO:
                    updateUiForApplyCoupon(networkCall);
                    break;
            }
        });


        viewModel.controlRefreshToken().observe(this, data -> {
            updateUiForRefreshToken(data, viewModel);
        });

        viewModel.updateCouponData().observe(this, data -> {

            this.couponModel = data;

            if (!isDiscount)
                binding.totalView.setVisibility(View.VISIBLE);


            binding.llCouponLayout.setVisibility(View.VISIBLE);
            binding.tvApply.setText("REMOVE");
            binding.etCoupon.setEnabled(false);
            binding.tvApply.setTextColor(activity().getResources().getColor(R.color.other_red));

            if (couponModel.getActual()) {
                binding.tvDiscountPercentage.setVisibility(View.GONE);
                binding.tvDiscountPrice.setVisibility(View.GONE);
            } else {
                binding.tvDiscountPercentage.setVisibility(View.VISIBLE);
                binding.tvDiscountPrice.setVisibility(View.VISIBLE);
            }

            if (couponModel.getAmount()) {
                binding.tvCouponName.setText("Coupon Applied ");
                binding.tvCouponDiscount.setText("- ₹" + couponModel.getCouponValue());
                if (couponModel.getActual()) {
                    binding.tvtotalPrice.setText("₹ " + (catalogueOverviewModel.getPrices().get(pricePosition).getActualPrice() - couponModel.getCouponValue()));
                } else {
                    binding.tvtotalPrice.setText("₹ " + (catalogueOverviewModel.getPrices().get(pricePosition).getFinalPrice() - couponModel.getCouponValue()));
                }
            } else {
                binding.tvCouponName.setText("Coupon Applied (" + couponModel.getCouponValue() + "%)");
                if (couponModel.getActual()) {
                    binding.tvtotalPrice.setText("₹ " + (catalogueOverviewModel.getPrices().get(pricePosition).getActualPrice()
                            - (((catalogueOverviewModel.getPrices().get(pricePosition).getActualPrice() * couponModel.getCouponValue()) / 100))));

                    binding.tvCouponDiscount.setText("- ₹" + (((catalogueOverviewModel.getPrices().get(pricePosition).getActualPrice() * couponModel.getCouponValue()) / 100)));
                } else {
                    binding.tvtotalPrice.setText("₹ " + (catalogueOverviewModel.getPrices().get(pricePosition).getFinalPrice()
                            - (((catalogueOverviewModel.getPrices().get(pricePosition).getFinalPrice() * couponModel.getCouponValue()) / 100))));

                    binding.tvCouponDiscount.setText("- ₹" + (((catalogueOverviewModel.getPrices().get(pricePosition).getFinalPrice() * couponModel.getCouponValue()) / 100)));
                }
            }

        });

        binding.tvApply.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());

            if (binding.tvApply.getText().toString().equals("Apply") || binding.tvApply.getText().toString().equals("APPLY")) {
                if (!TextUtils.isEmpty(binding.etCoupon.getText().toString()))
                    viewModel.getCouponResponse(catalogueOverviewModel.getCatalogueId(), binding.etCoupon.getText().toString().toUpperCase());
                else {
                    showSnackBar("Please enter Coupon Code", 2);
                }
            } else if (binding.tvApply.getText().toString().equals("Remove") || binding.tvApply.getText().toString().equals("REMOVE")) {

                binding.etCoupon.setEnabled(true);
                removeLayout();
                Mixpanel.applyOrRemoveCoupon(catalogueOverviewModel.getCatalogueName(), binding.etCoupon.getText().toString().toUpperCase(), false, false);
            }
        });
    }

    private void gotoCatalogueOverview() {

        CatalogueModel catalogueModel = new CatalogueModel();
        catalogueModel.setCatalogueName(catalogueOverviewModel.getCatalogueName());
        catalogueModel.setCatalogueId(catalogueOverviewModel.getCatalogueId());
        catalogueModel.setCatalogueDescription(catalogueOverviewModel.getCatalogueDescription());
        catalogueModel.setCatalogueImage(catalogueOverviewModel.getCatalogueImage());

        ShareDataManager.getInstance().getStoreOverviewData().setCatalogueModel(catalogueModel);
        Intent returnData = new Intent();
        returnData.putExtra("OpenCatalogue", true);
        activity().setResult(Activity.RESULT_OK, returnData);
        activity().finish();
    }

    private void removeLayout() {

        couponModel = null;
        binding.etCoupon.setText("");
        binding.llCouponLayout.setVisibility(View.GONE);

        if (!isDiscount)
            binding.totalView.setVisibility(View.GONE);

        binding.tvCouponName.setText("");
        binding.tvCouponDiscount.setText("");
        binding.tvApply.setText("APPLY");
        binding.tvApply.setTextColor(activity().getResources().getColor(R.color.blue));
        binding.tvtotalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(pricePosition).getFinalPrice());

        binding.tvDiscountPercentage.setVisibility(View.VISIBLE);
        binding.tvDiscountPrice.setVisibility(View.VISIBLE);
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                            viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "Show Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);
        }
        return ssb;

    }

    private void updateUiForGenerateOrder(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                if (networkCall.getBundle() != null) {
                    orderId = networkCall.getBundle().getString("OrderId");
                    if (activity() instanceof HelperActivity) {
                        try {
                            ((HelperActivity) (activity())).startPayment(new JSONObject(networkCall.getBundle().getString("Json")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case FAIL:
                closeProgressDialog();
                break;

            case ERROR:
                closeProgressDialog();
                break;
        }
    }

    private void updateUiForOrderStatus(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {

            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                Intent returnData = new Intent();
                returnData.putExtra("Payment", true);
                activity().setResult(Activity.RESULT_OK, returnData);
                activity().finish();
                break;

            case FAIL:
                closeProgressDialog();
                break;

            case ERROR:
                closeProgressDialog();
                break;
        }
    }

    private void updateUiForApplyCoupon(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {

            case IN_PROCESS:
                binding.couponProgressBar.setVisibility(View.VISIBLE);
                binding.tvApply.setVisibility(View.GONE);
                break;

            case DONE:
                binding.couponProgressBar.setVisibility(View.GONE);
                binding.tvApply.setVisibility(View.VISIBLE);
                break;

            case SUCCESS:
                binding.couponProgressBar.setVisibility(View.GONE);
                binding.tvApply.setVisibility(View.VISIBLE);
                Mixpanel.applyOrRemoveCoupon(catalogueOverviewModel.getCatalogueName(), binding.etCoupon.getText().toString().toUpperCase(), true, true);
                break;

            case NO_DATA:
                binding.couponProgressBar.setVisibility(View.GONE);
                binding.tvApply.setVisibility(View.VISIBLE);
                break;

            case FAIL:
                binding.couponProgressBar.setVisibility(View.GONE);
                binding.tvApply.setVisibility(View.VISIBLE);
                Animation shake = AnimationUtils.loadAnimation(activity(), R.anim.shake);
                binding.etCoupon.startAnimation(shake);
                setVibration();
                Mixpanel.applyOrRemoveCoupon(catalogueOverviewModel.getCatalogueName(), binding.etCoupon.getText().toString().toUpperCase(), true, false);
                break;

            case ERROR:
                binding.couponProgressBar.setVisibility(View.GONE);
                binding.tvApply.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setVibration() {

        int DURATION = 500; // you can change this according to your need
        if (Build.VERSION.SDK_INT >= 26) {
            ((Vibrator) activity().getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(DURATION, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            ((Vibrator) activity().getSystemService(VIBRATOR_SERVICE)).vibrate(DURATION);
        }
    }


    public void updateOrderStatus(String paymentId) {
        viewModel.updateOrderStatus(paymentId, orderId);
    }

    @Override
    public void changeTitle(String title) {

    }

    public void updateSubscriptionStatus(SubscriptionModel subscriptionModel) {

        if (subscriptionModel == null)
            return;

        if (subscriptionModel.isExists()) {
            if (subscriptionModel.getCatalogueStatus().equals("ACTIVE")) {
                isPayment = false;
                binding.durationView.setVisibility(View.GONE);
                binding.featureView.setVisibility(View.GONE);
                binding.tvDuration.setVisibility(View.GONE);
                binding.durationLayout.setVisibility(View.GONE);
                binding.subTotalLayout.setVisibility(View.GONE);
                binding.llApplyCouponLayout.setVisibility(View.GONE);
                binding.durationView1.setVisibility(View.GONE);
                binding.totallayout.setVisibility(View.GONE);
                binding.tvProceedToPay.setText("Get Started");
                if (((HelperActivity) activity()).tvGet != null) {
                    ((HelperActivity) activity()).tvGet.setText("Access");

                    ((HelperActivity) activity()).menupay.setTitle("Access");
                    ((HelperActivity) activity()).menupay.setVisible(true);
                }
            } else if (subscriptionModel.getCatalogueStatus().equals("EXPIRED")) {
                isPayment = true;
                if (((HelperActivity) activity()).tvGet != null) {
                    ((HelperActivity) activity()).tvGet.setText("GET   ");

                    ((HelperActivity) activity()).menupay.setTitle("GET");
                    ((HelperActivity) activity()).menupay.setVisible(true);
                }
            }
        } else {
            isPayment = true;
            if (((HelperActivity) activity()).tvGet != null) {
                ((HelperActivity) activity()).tvGet.setText("GET");

                ((HelperActivity) activity()).menupay.setTitle("GET   ");
                ((HelperActivity) activity()).menupay.setVisible(true);
            }
        }
    }

    @Override
    public void playPreviewVideo(int position, String videoUrl) {
        String hlsVideoUrl = null;
        if (catalogueOverviewModel != null) {
            hlsVideoUrl = viewModel.getHlsVideoByCatalogueId(catalogueOverviewModel.getCatalogueId(), position);
        }

        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.VIDEO_PLAYER);
        intent.putExtra("VIDEO_URL", hlsVideoUrl == null ? videoUrl : hlsVideoUrl);
        activity().startActivity(intent);
    }

    @Override
    public void clickPrice(int position, PriceModel priceModel, int lastPosition) {
        binding.etCoupon.setEnabled(true);
        removeLayout();
        this.pricePosition = position;

        catalogueOverviewModel.getPrices().get(lastPosition).setCheched(false);
        catalogueOverviewModel.getPrices().get(position).setCheched(true);
        enrollAdapter.notifyDataSetChanged();
        binding.tvOriginalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(position).getActualPrice());

        if (catalogueOverviewModel.getPrices().get(position).getDiscount() > 0) {

            binding.discountLayout.setVisibility(View.VISIBLE);
            binding.tvDiscountPercentage.setText(new DecimalFormat("##.##").format(catalogueOverviewModel.getPrices().get(position).getDiscount()) + "% Discount" + " - " + catalogueOverviewModel.getPrices().get(position).getDiscountName());
            binding.tvtotalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(position).getFinalPrice());
            binding.tvDiscountPrice.setText("- ₹ " + new DecimalFormat("##.##").format((catalogueOverviewModel.getPrices().get(position).getActualPrice() - catalogueOverviewModel.getPrices().get(position).getFinalPrice())));

            binding.totalView.setVisibility(View.VISIBLE);
//            binding.tvProceedToPay.setText("Buy");

        } else {
            binding.discountLayout.setVisibility(View.GONE);
//            binding.tvProceedToPay.setText("Buy     |     " + catalogueOverviewModel.getPrices().get(position).getFinalPrice());
            binding.tvtotalPrice.setText("₹ " + catalogueOverviewModel.getPrices().get(position).getFinalPrice());
            binding.totalView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSubscriptionName(String subScriptionType) {

        this.subScriptionType = subScriptionType;
    }

    @Override
    public void navigateToInstructorProfile(InstructorsModel instructorsModel) {

//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, instructorsModel.getInstructorName());
//        intent.putExtra("Url", instructorsModel.getResume());
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(instructorsModel.getResume()), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(instructorsModel.getResume()), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(instructorsModel.getResume()));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }

    public void gotoPayment() {

        if (!isPayment) {
            gotoCatalogueOverview();
        } else
            binding.nestedScrollview.fullScroll(View.FOCUS_DOWN);
    }

    public void updateMixPanel(boolean isPaymentSuccess) {

        Mixpanel.orderComplete(courseName, subScriptionType, coursePrice, apppliedCoupon, purChasedPrice, isPaymentSuccess);
    }
}
