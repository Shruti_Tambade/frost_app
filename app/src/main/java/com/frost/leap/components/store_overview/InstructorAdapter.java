package com.frost.leap.components.store_overview;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemInstructorBinding;

import java.util.List;

import apprepos.store.model.InstructorsModel;

/**
 * Created by Chenna Rao on 10/3/2019.
 */
public class InstructorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<InstructorsModel> list;
    private IOverviewFragment iOverviewFragment;

    public InstructorAdapter(IOverviewFragment iOverviewFragment, Context context, List<InstructorsModel> instructorsModel) {
        this.context = context;
        this.list = instructorsModel;
        this.iOverviewFragment = iOverviewFragment;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new InstructorViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_instructor, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof InstructorViewHolder) {
            InstructorViewHolder instructorViewHolder = (InstructorViewHolder) holder;

            instructorViewHolder.bindData(position);

            instructorViewHolder.binding.tvInstructorProfile1.setOnClickListener(v -> {

                iOverviewFragment.navigateToInstructorProfile(list.get(position));
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class InstructorViewHolder extends RecyclerView.ViewHolder {

        private ItemInstructorBinding binding;

        public InstructorViewHolder(ItemInstructorBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position) {

            binding.tvInstructorName1.setText(list.get(position).getInstructorName());
            binding.tvInstructorDescription.setText(list.get(position).getAbout());
            binding.ivInstructor1.setImageURI(Uri.parse(list.get(position).getInstructorImage()));
        }
    }
}
