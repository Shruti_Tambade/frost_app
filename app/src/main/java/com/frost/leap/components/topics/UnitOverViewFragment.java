package com.frost.leap.components.topics;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewAnimationUtils;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentUnitOverViewBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.services.HelperService;
import com.frost.leap.services.video.DownloadLicenseService;
import com.frost.leap.services.video.DownloadTracker;
import com.frost.leap.viewmodels.topics.DownloadViewModel;
import com.google.android.exoplayer2.offline.Download;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;
import apprepos.topics.model.DownloadSubTopicModel;
import apprepos.topics.model.QuizModel;
import apprepos.topics.model.StudentContentModel;
import apprepos.topics.model.StudentTopicsModel;
import apprepos.topics.model.SubTopicModel;
import apprepos.topics.model.TopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link UnitOverViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UnitOverViewFragment extends BaseFragment implements ISubTopicsAdapter, ICustomAlertDialog, DownloadTracker.Listener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "units";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "subject";
    private static final String ARG_PARAM4 = "topic";
    private boolean updateData = true;
    private boolean showOnlyDownloaded = false;

    // TODO: Rename and change types of parametersf
    private String mParam1;
    private String mParam2;

    private String downloadTopicName;
    private String downloadSubTopicName;
    private Handler handler;
    private FragmentUnitOverViewBinding binding;
    private UnitModel unitModel;
    private String title;
    private int downloadTopicPosition, downloadSubTopicPosition;
    private DownloadSubTopicModel downloadSubTopicModel;

    private List<StudentTopicsModel> studentTopicsModelList = new ArrayList<>();
    private StudentTopicsModel studentTopicsModel;
    private CatalogueModel catalogueModel;
    private SubjectModel subjectModel;
    private TopicModel topicModel;
    private DownloadViewModel viewModel;

    private SubTopicModel lastWatchedSubTopicModel;
    private SubTopicModel currentSubTopicModel;
    private TopicsAdapter adapter;
    private boolean isLastWatchedQuiz = false;
    private QuizModel lastWatchedQuizModel;

    private TopicsBroadcastReceiver broadcastReceiver;
    private int topicPosition = 0, subTopicPosition = 0;
    private String topicId;
    private int offset = 0;

    private boolean isClicked = true;
    private boolean isFABOpen = false;

    private DownloadTracker downloadTracker;
    private int height;
    private long currentVideoPlaybackPosition = 0;
    private boolean isNetwork = false;

    public UnitOverViewFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static UnitOverViewFragment newInstance(String param1, String param2) {
        UnitOverViewFragment fragment = new UnitOverViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public static UnitOverViewFragment newInstance(UnitModel unitModel, CatalogueModel catalogueModel, SubjectModel subjectModel, TopicModel topicModel) {
        UnitOverViewFragment fragment = new UnitOverViewFragment();

        fragment.unitModel = unitModel;
        fragment.catalogueModel = catalogueModel;
        fragment.subjectModel = subjectModel;
        fragment.topicModel = topicModel;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DownloadViewModel.class);
        isNetwork = viewModel.isNetworkAvailable();
        broadcastReceiver = new TopicsBroadcastReceiver();
        downloadTracker = HelperApplication.getInstance().getDownloadTracker();

    }

    @Override
    public int layout() {
        return R.layout.fragment_unit_over_view;
    }


    @Override
    public void setUp() {
        binding = (FragmentUnitOverViewBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.llraiseComplaintFab.setVisibility(View.GONE);
        binding.lladdNoteFab.setVisibility(View.GONE);

        binding.coordinatorLayout.post(() -> {
            height = binding.coordinatorLayout.getHeight();
        });

        if (topicModel == null) {
            return;
        }

        if (topicModel.getStudentTopicsModels() == null || topicModel.getStudentTopicsModels().size() == 0) {
            binding.fabLayout.setVisibility(View.GONE);
            showErrorDialog();
        }

//        if ((currentSubTopicModel == null || currentSubTopicModel.getSubTopicId() == null)) {
//            showErrorDailog();
//        }

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        ((SimpleItemAnimator) binding.recyclerView.getItemAnimator()).setChangeDuration(0);
        binding.recyclerView.setItemAnimator(null);
        if (topicModel != null) {

            this.studentTopicsModelList = topicModel.getStudentTopicsModels();

            binding.recyclerView.setAdapter(adapter = new TopicsAdapter(activity(), this, RController.DONE, studentTopicsModelList));

            binding.recyclerView.setHasFixedSize(true);

            if (topicModel.getSubTopicModel() != null) {
                lastWatchedSubTopicModel = topicModel.getSubTopicModel();
                Logger.d("LAST SUBTOPIC", String.valueOf(topicModel.getSubTopicModel()));
                adapter.changeHeaderTitle(topicModel.getSubTopicModel());

                try {

                    for (int i = 0; i < studentTopicsModelList.size(); i++) {
                        if (lastWatchedSubTopicModel.getStudentTopicId().equals(studentTopicsModelList.get(i).getStudentTopicId())) {
                            topicPosition = i;
                            for (int j = 0; j < studentTopicsModelList.get(i).getStudentContents().size(); j++) {
                                if (lastWatchedSubTopicModel.getSubTopicId().equals(studentTopicsModelList.get(i).
                                        getStudentContents().get(j).getSubTopic().getSubTopicId())) {
                                    subTopicPosition = j;
                                    break;
                                }
                            }
                            int finalI = i;
                            new Handler().postDelayed(() -> binding.recyclerView.smoothScrollToPosition(finalI == 0 ? finalI : finalI + 1), 500);

                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {

                if (studentTopicsModelList != null && studentTopicsModelList.size() > 0)
                    if (studentTopicsModelList.get(0).getStudentContents() != null && studentTopicsModelList.get(0).getStudentContents().size() > 0) {

                        if (studentTopicsModelList.get(0).getStudentContents().get(0).getContentType().equals("SUBTOPIC")) {
                            studentTopicsModel = studentTopicsModelList.get(0);
                            adapter.changeHeaderTitle(studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic());
                            isLastWatchedQuiz = false;
                            Logger.d("lastQuiz", String.valueOf(studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic()));
                            lastWatchedSubTopicModel = studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic();
                            studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic().setLastWatched(true);
                            adapter.notifyDataSetChanged();

                        } else if (studentTopicsModelList.get(0).getStudentContents().get(0).getContentType().equals("QUIZ")) {

                            isLastWatchedQuiz = true;
                            Logger.d("lastQuiz", String.valueOf(studentTopicsModelList.get(0).getStudentContents().get(0).getQuiz()));
                            lastWatchedQuizModel = studentTopicsModelList.get(0).getStudentContents().get(0).getQuiz();
                            studentTopicsModelList.get(0).getStudentContents().get(0).getQuiz().setLastWatched(true);
                            adapter.notifyDataSetChanged();

                        }
                    } else
                        showSnackBar("VideoConstants details not available", 2);
                else
                    showSnackBar("VideoConstants details not available", 2);

            }

        }

        if (topicModel.getSubTopicModel() != null) {

            playSubTopicVideo(topicModel.getSubTopicModel(), topicModel.getSubTopicModel().getTopicName(), topicModel.getSubTopicModel().getStudentTopicId());
            StudentTopicsModel studentTopicsModel = new StudentTopicsModel();
            studentTopicsModel.setStudentTopicId(topicModel.getSubTopicModel().getStudentTopicId());

            ((MainActivity) activity()).playSubTopicVideo(updateCurrentSubTopic(topicModel.getSubTopicModel()), studentTopicsModel);

            Mixpanel.accessSubTopics(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName(), topicModel.getSubTopicModel().getTopicName(), topicModel.getSubTopicModel().getSubTopicName());

//            adapter.setSubCatContent(subjectModel.getSubjectName() + "/"
//                    + unitModel.getUnitName() + "/"
//                    + topicModel.getSubTopicModel().getTopicName() + "/"
//                    + topicModel.getSubTopicModel().getSubTopicName());

        } else {
            if (topicModel.getStudentTopicsModels() != null && topicModel.getStudentTopicsModels().size() > 0) {
                outerloop:
                for (int j = 0; j < topicModel.getStudentTopicsModels().size(); j++) {

                    for (int i = 0; i < topicModel.getStudentTopicsModels().get(j).getStudentContents().size(); i++) {

                        if (topicModel.getStudentTopicsModels().get(j).getStudentContents().get(i).getContentType().equals("SUBTOPIC")) {
                            topicPosition = j;
                            subTopicPosition = i;

                            ((MainActivity) activity()).playSubTopicVideo(updateCurrentSubTopic(topicModel.getStudentTopicsModels().get(j).getStudentContents().get(i).getSubTopic())
                                    , topicModel.getStudentTopicsModels().get(j));
                            playSubTopicVideo(topicModel.getStudentTopicsModels().get(j).getStudentContents().get(i).getSubTopic(), topicModel.getStudentTopicsModels().get(j).getTopicName()
                                    , topicModel.getStudentTopicsModels().get(j).getStudentTopicId());

                            Mixpanel.accessSubTopics(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName()
                                    , topicModel.getStudentTopicsModels().get(j).getTopicName()
                                    , topicModel.getStudentTopicsModels().get(j).getStudentContents().get(subTopicPosition).getSubTopic().getSubTopicName());

                            break outerloop;
                        } else {
                            ((MainActivity) activity()).openQuizPage(subjectModel.getStudentSubjectId(), unitModel.getStudentUnitId()
                                    , topicModel.getStudentTopicsModels().get(j).getStudentTopicId()
                                    , topicModel.getStudentTopicsModels().get(j).getStudentContents().get(i).getQuiz());

//                            adapter.setSubCatContent(subjectModel.getSubjectName() + "/"
//                                    + unitModel.getUnitName() + "/"
//                                    + topicModel.getStudentTopicsModels().get(j).getTopicName() + "/"
//                                    + topicModel.getStudentTopicsModels().get(j).getStudentContents().get(i).getQuiz().getName());

                            break outerloop;
                        }
                        //End I loop
                    }
                    //End J Loop
                }
            }
        }


        viewModel.storeTopicModelList(studentTopicsModelList);
        viewModel.updateOfflineVideosInfo(studentTopicsModelList, showOnlyDownloaded);


        binding.openFab.setOnClickListener(view -> {
            if (!isFABOpen) {
                showFABMenu();
                binding.openFab.setImageResource(R.drawable.ic_notes_cross_icon);
            } else {
                closeFABMenu();
                binding.openFab.setImageResource(R.drawable.ic_add_note_icon);
            }
        });

        binding.addNoteLayout.setOnClickListener(v -> {

        });

        binding.addNoteFab.setOnClickListener(view -> {

            if (currentSubTopicModel == null) {
                return;
            }

            if (currentSubTopicModel.getSubTopicId() != null) {
                Note note = new Note();

                note.setCatalogueName(catalogueModel.getCatalogueName());
                note.setSubjectName(subjectModel.getSubjectName());
                note.setUnitName(unitModel.getUnitName());

                note.setStudentId(viewModel.getUserId());
                note.setStudentSubjectId(subjectModel.getStudentSubjectId());
                note.setStudentUnitId(unitModel.getStudentUnitId());
                note.setTimestampInSecond(currentVideoPlaybackPosition / 1000);

                note.setStudentTopicId(currentSubTopicModel.getStudentTopicId());
                note.setTopicName(currentSubTopicModel.getTopicName());
                note.setStudentSubtopicId(currentSubTopicModel.getSubTopicId());
                note.setSubTopicName(currentSubTopicModel.getSubTopicName());
                note.setVideoLengthInSeconds(currentSubTopicModel.getTotalVideoDuration());

                if (activity() instanceof MainActivity) {
                    ((MainActivity) activity()).updateNote(note, 0, catalogueModel.getCatalogueName() + "<br/><br/> <b> <font color ='black'>" + currentSubTopicModel.getSubTopicName() + "</font></b>");
                }

                Mixpanel.accessTools(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName(), currentSubTopicModel.getTopicName(), currentSubTopicModel.getSubTopicName(), "Add Note");
                closeFABMenu();
                binding.openFab.setImageResource(R.drawable.ic_add_note_icon);

                ((MainActivity) activity()).pausePlayVideo(false);
            }

        });

        binding.raiseComplaintFab.setOnClickListener(view -> {

            if (currentSubTopicModel == null) {
                return;
            }

            if (currentSubTopicModel.getSubTopicId() != null) {

                if (activity() instanceof MainActivity) {
                    ((MainActivity) activity()).askExpert(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName(), currentSubTopicModel.getTopicName(), currentSubTopicModel.getSubTopicName());
                }

                Mixpanel.accessTools(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName(), currentSubTopicModel.getTopicName(), currentSubTopicModel.getSubTopicName(), "Ask an Expert");
                closeFABMenu();
                binding.openFab.setImageResource(R.drawable.ic_add_note_icon);

                ((MainActivity) activity()).pausePlayVideo(false);
            }
        });


        viewModel.getVideoUrl().observe(this, dashUrlsModel -> {
            if (dashUrlsModel == null) {
                return;
            }
            if (TextUtils.isEmpty(dashUrlsModel.getDashUrl())) {
                showToast("Video link not available", false);
                return;
            }

//            dashUrlsModel.setDashUrl("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
//            dashUrlsModel.setDashUrl("https://d2jx687vmgx9ce.cloudfront.net/d3ca648e-5ecf-43c8-8294-79047a95f700/dash/df7e614f-eb5c-4ed1-8c9c-145e92f741f4-1.3.2 Addition rule for arbitrary events.mpd");

            if (downloadSubTopicModel != null)
                downloadSubTopicModel.setVideoUrl(dashUrlsModel.getDashUrl());
            verifyAndDownload(dashUrlsModel, title);
        });

        viewModel.updateTopics().observe(this, studentTopicsModels -> {
            this.studentTopicsModelList = studentTopicsModels;
            try {
                if (updateData) {
                    adapter.notifyDataSetChanged();
                } else
                    updateData = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        viewModel.updateOfflineTopics().observe(this, studentTopicsModels -> {
            binding.progressBar.setVisibility(View.GONE);
            this.studentTopicsModelList = studentTopicsModels;
            if (adapter != null)
                adapter.updateList(studentTopicsModelList);
            if (studentTopicsModelList != null && studentTopicsModelList.size() > 0 && !isNetwork) {
                isNetwork = true;
                isClicked = true;
                if (studentTopicsModelList.get(0).getStudentContents() != null && studentTopicsModelList.get(0).getStudentContents().size() > 0) {
                    if (currentSubTopicModel != null && !currentSubTopicModel.getSubTopicId().equals(studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic().getSubTopicId())) {
                        onSubTopicClick(studentTopicsModelList.get(0), studentTopicsModelList.get(0).getStudentContents().get(0), 0, 0);
                    } else {
                        topicPosition = 0;
                        subTopicPosition = 0;
                        updateNext();
                    }
                }
            }
        });
        updateNext();
    }

    private void showErrorDialog() {

        Utility.requestDialogAlert(activity(), this,
                null,
                "We're Actively Working To Get The Class Live Soon. Stay Tuned!",
                1439,
                "BACK",
                false,
                false);
    }

    private void verifyAndDownload(DashUrlsModel dashUrlsModel, String title) {
        Uri uri = Uri.parse(dashUrlsModel.getDashUrl());
        if (downloadTracker.isDownloaded(uri) && downloadTracker.getDownload(uri).state == Download.STATE_COMPLETED) {
            Utility.requestDialog(activity(), new ICustomAlertDialog() {
                        @Override
                        public void doPositiveAction(int id) {
                            viewModel.popNotify(dashUrlsModel.getDashUrl());
                            Mixpanel.cancelDownloadedVideo(
                                    catalogueModel.getCatalogueName(),
                                    subjectModel.getSubjectName(),
                                    unitModel.getUnitName(), downloadTopicName, downloadSubTopicName
                            );

                            downloadTracker.deleteDownload(uri);
                            viewModel.deleteLicenseUrl(uri.toString());
                            if (studentTopicsModelList == null)
                                return;

                            cancelUpdate();
                            updateData = false;

                            try {
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setPercentage(0);
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setDownloaded(false);
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setInProcess(false);
                                adapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void doNegativeAction() {

                        }
                    },
                    "Delete",
                    "Do you want to delete, please confirm",
                    71);

            return;
        }
        if (downloadTracker.getDownload(uri) != null && downloadTracker.getDownload(uri).state == Download.STATE_FAILED) {
            Download download = downloadTracker.getDownload(uri);
            Utility.requestDialog(activity(), new ICustomAlertDialog() {
                        @Override
                        public void doPositiveAction(int id) {


                            if (!viewModel.isNetworkAvailable()) {
                                showToast(Constants.PLEASE_CHECK_INTERNET, false);
                                return;
                            }

                            viewModel.pushToNotify(dashUrlsModel,
                                    catalogueModel.getCatalogueName(),
                                    subjectModel.getSubjectName(),
                                    unitModel.getUnitName(),
                                    downloadTopicName, downloadSubTopicName);


                            viewModel.addDownloadSubTopic(downloadSubTopicModel);
                            downloadLicense(dashUrlsModel);
                            downloadTracker.retryDownload(activity(), download);

                            if (studentTopicsModelList == null)
                                return;

                            cancelUpdate();
                            updateData = false;
                            try {
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setPercentage(downloadTracker.getDownload(uri).getPercentDownloaded());
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setDownloaded(true);
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setInProcess(true);
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setFailed(false);
                                adapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void doNegativeAction() {

                        }
                    },
                    "Resume Download",
                    "It downloaded " + (int) download.getPercentDownloaded() + "%, Do you want to resume please confirm?",
                    72);

            return;
        }
        if (downloadTracker.getDownload(uri) != null) {
            Download download = downloadTracker.getDownload(uri);
            Utility.requestDialog(activity(), new ICustomAlertDialog() {
                        @Override
                        public void doPositiveAction(int id) {

                            viewModel.popNotify(dashUrlsModel.getDashUrl());
                            Mixpanel.cancelDownloadedVideo(
                                    catalogueModel.getCatalogueName(),
                                    subjectModel.getSubjectName(),
                                    unitModel.getUnitName(), downloadTopicName, downloadSubTopicName
                            );

                            downloadTracker.deleteDownload(uri);
                            viewModel.deleteLicenseUrl(uri.toString());

                            if (studentTopicsModelList == null)
                                return;

                            cancelUpdate();
                            updateData = false;
                            try {
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setPercentage(0);
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setDownloaded(false);
                                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setInProcess(false);
                                adapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void doNegativeAction() {

                        }
                    },
                    "Delete",
                    "Already in queue, downloaded " + (int) download.getPercentDownloaded() + "%, Do you want to delete please confirm?",
                    72);

            return;
        }

        if (!viewModel.getAskMeEveryTime()) {
//            HelperApplication.getInstance().getDownloadManager();
//            if (HelperApplication.getInstance().getDownloadManager().getCurrentDownloads().size() > 10) {
//                showToast("10 items already in queue. Try Again", false);
//                return;
//            }
            viewModel.pushToNotify(dashUrlsModel,
                    catalogueModel.getCatalogueName(),
                    subjectModel.getSubjectName(),
                    unitModel.getUnitName(),
                    downloadTopicName, downloadSubTopicName);


            Mixpanel.clickVideoDownloadButton(
                    catalogueModel.getCatalogueName(),
                    subjectModel.getSubjectName(),
                    unitModel.getUnitName(), downloadTopicName, downloadSubTopicName, "" + viewModel.getDownloadResolution()
            );

            if (!viewModel.isNetworkAvailable()) {
                showToast(Constants.PLEASE_CHECK_INTERNET, false);
                return;
            }
            Logger.d("DOWNLOAD URL", uri.toString());

            if (activity() == null)
                return;


            downloadTracker.toggleDownload(
                    ((MainActivity) activity()).getSupportFragmentManager(),
                    title,
                    uri,
                    null,
                    viewModel.getDownloadResolution(),
                    HelperApplication.getInstance()
                            .buildRenderersFactory(true));

            viewModel.addDownloadSubTopic(downloadSubTopicModel);

            downloadLicense(dashUrlsModel);

            if (studentTopicsModelList == null)
                return;

            cancelUpdate();

            updateData = false;

            try {
                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setPercentage(0.5f);
                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setDownloaded(false);
                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setInProcess(true);
                adapter.notifyDataSetChanged();
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            return;
        }

        BSDDownloadRequestFragment bsdDownloadRequestFragment = BSDDownloadRequestFragment.newInstance(quality -> {
//            HelperApplication.getInstance().getDownloadManager();
//            if (HelperApplication.getInstance().getDownloadManager().getCurrentDownloads().size() > 10) {
//                showToast("10 items already in queue. Try Again", false);
//                return;
//            }

            viewModel.pushToNotify(dashUrlsModel,
                    catalogueModel.getCatalogueName(),
                    subjectModel.getSubjectName(),
                    unitModel.getUnitName(),
                    downloadTopicName, downloadSubTopicName);


            Mixpanel.clickVideoDownloadButton(
                    catalogueModel.getCatalogueName(),
                    subjectModel.getSubjectName(),
                    unitModel.getUnitName(), downloadTopicName, downloadSubTopicName, "" + quality
            );

            if (!viewModel.isNetworkAvailable()) {
                showToast(Constants.PLEASE_CHECK_INTERNET, false);
                return;
            }

            Logger.d("DOWNLOAD URL", uri.toString());

            if (activity() == null)
                return;

            downloadTracker.toggleDownload(
                    ((MainActivity) activity()).getSupportFragmentManager(),
                    title,
                    uri,
                    null,
                    quality,
                    HelperApplication.getInstance()
                            .buildRenderersFactory(true));

            viewModel.addDownloadSubTopic(downloadSubTopicModel);

            downloadLicense(dashUrlsModel);

            if (studentTopicsModelList == null)
                return;

            cancelUpdate();
            updateData = false;

            try {
                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setPercentage(0.5f);
                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setDownloaded(false);
                studentTopicsModelList.get(downloadTopicPosition).getStudentContents().get(downloadSubTopicPosition).setInProcess(true);
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        if (activity() instanceof MainActivity) {
            ((MainActivity) activity()).requestDownload(bsdDownloadRequestFragment);
        }

    }


    private void downloadLicense(DashUrlsModel dashUrlsModel) {
        Intent intent = new Intent(activity(), DownloadLicenseService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_DOWNLOAD_DRM_LICENSE);
        intent.putExtra("VideoUrl", dashUrlsModel.getDashUrl());
        intent.putExtra("PageContentId", dashUrlsModel.getPageContentId());
        activity().startService(intent);
    }

//    public void askExpert(String catalogueName, String subjectName) {
//        BSDAskDoubtFragment.newInstance(catalogueName, subjectName, height).show(getSupportFragmentManager(), "BSDAskDoubtFragment");
//    }
//
//    public void updateNote(Note note, int position) {
//        BSDAddNoteFragment.newInstance(note, position, height).show(getSupportFragmentManager(), "BSDAskDoubtFragment");
//    }

    private void showFABMenu() {

        binding.llraiseComplaintFab.setVisibility(View.VISIBLE);
        binding.lladdNoteFab.setVisibility(View.VISIBLE);

        isFABOpen = true;
        binding.lladdNoteFab.animate().translationY(-Utility.toSdp(R.dimen._62sdp));
        binding.llraiseComplaintFab.animate().translationY(-Utility.toSdp(R.dimen._115sdp));

        binding.tvAddNote.setVisibility(View.VISIBLE);
        binding.tvAskExpert.setVisibility(View.VISIBLE);

        showWithRevealEffect(binding.addNoteLayout);

    }

    private void closeFABMenu() {

        binding.llraiseComplaintFab.setVisibility(View.GONE);
        binding.lladdNoteFab.setVisibility(View.GONE);

        isFABOpen = false;
        binding.lladdNoteFab.animate().translationY(0);
        binding.llraiseComplaintFab.animate().translationY(0);

        binding.tvAddNote.setVisibility(View.GONE);
        binding.tvAskExpert.setVisibility(View.GONE);

        hideWithRevealEffect(binding.addNoteLayout);
    }

    private void showWithRevealEffect(final View view) {

//        int cx = view.getWidth() / 2;
//        int cy = view.getHeight() / 2;

        int cx = binding.coordinatorLayout.getWidth();
        int cy = binding.coordinatorLayout.getHeight();

        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(binding.coordinatorLayout.getWidth(), binding.coordinatorLayout.getHeight());

        // create the animator for this view (the start radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);

//        anim.setDuration(500);

            // make the view visible and start the animation
            anim.start();
        }
        view.setVisibility(View.VISIBLE);
    }

    private void hideWithRevealEffect(final View view) {

//        int cx = view.getWidth() / 2;
//        int cy = view.getHeight() / 2;

        int cx = binding.coordinatorLayout.getWidth();
        int cy = binding.coordinatorLayout.getHeight();

        // get the initial radius for the clipping circle
        float initialRadius = (float) Math.hypot(binding.coordinatorLayout.getWidth(), binding.coordinatorLayout.getHeight());

        // create the animation (the final radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);

            // make the view invisible when the animation is done
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.INVISIBLE);
                }
            });

            // start the animation
            anim.start();
        } else {

            view.setVisibility(View.INVISIBLE);
        }
    }

    public void playSubTopicVideo(SubTopicModel subTopic, String topicName, String topicId) {

        this.topicId = topicId;
        try {
//            binding.tvSubTopicName.setText(subTopic.getSubTopicName())); //collapsed view title
//            binding.tvDescription.setText(Utility.getTextCase(subTopic.getDescription()));
//
//            binding.tvCategoryName.setText(catalogueModel.getCatalogueName()));
//            adapter.setSubCatContent(subjectModel.getSubjectName() + "/"
//                    + unitModel.getUnitName() + "/"
//                    + topicName + "/"
//                    + subTopic.getSubTopicName());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void playSubTopicVideo(SubTopicModel subTopic, StudentTopicsModel studentTopicsModel) {

        this.topicId = studentTopicsModel.getStudentTopicId();

        //binding.progressBar.setVisibility(View.VISIBLE);
        if (studentTopicsModel != null) {
//            binding.tvSubTopicName.setText(subTopic.getSubTopicName()));
//            binding.tvDescription.setText(Utility.getTextCase(subTopic.getDescription()));
//            binding.tvCategoryName.setText(catalogueModel.getCatalogueName()));
//            adapter.setSubCatContent(subjectModel.getSubjectName() + "/"
//                    + unitModel.getUnitName() + "/"
//                    + studentTopicsModel.getTopicName() + "/"
//                    + subTopic.getSubTopicName());
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public Activity activity() {
        return getActivity();
    }

    @Override
    public void onSubTopicClick(StudentTopicsModel topicModel, StudentContentModel
            studentContentModel, int position, int topicPosition) {

        if (isClicked) {
            isClicked = false;
            this.topicPosition = topicPosition;
            this.subTopicPosition = position;
            this.topicId = topicModel.getTopicId();
            updateNext();

            if (activity() instanceof MainActivity) {
                if (studentContentModel.getContentType().equals("SUBTOPIC")) {
                    studentTopicsModel = topicModel;

                    updateCurrentSubTopic(studentContentModel.getSubTopic());

                    Mixpanel.accessSubTopics(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName()
                            , studentContentModel.getSubTopic().getTopicName(), studentContentModel.getSubTopic().getSubTopicName());

                    ((MainActivity) activity()).playSubTopicVideo(updateCurrentSubTopic(studentContentModel.getSubTopic()), topicModel);
                    adapter.changeHeaderTitle(studentContentModel.getSubTopic());
                    if (isLastWatchedQuiz) {
                        if (lastWatchedQuizModel != null) {
                            lastWatchedQuizModel.setLastWatched(false);
                            studentContentModel.getSubTopic().setLastWatched(true);
                            adapter.notifyDataSetChanged();
                            new Handler().post(() -> {
                                isClicked = true;
                            });
                        }
                    } else {
                        if (lastWatchedSubTopicModel != null) {
                            lastWatchedSubTopicModel.setLastWatched(false);
                            studentContentModel.getSubTopic().setLastWatched(true);

                            this.lastWatchedSubTopicModel = studentContentModel.getSubTopic();
                            adapter.notifyDataSetChanged();
                            new Handler().post(() -> {
                                isClicked = true;
                            });
                        }
                    }

                } else {
//                    ((MainActivity) activity()).openQuizPage(subjectModel.getStudentSubjectId(), unitModel.getStudentUnitId(), topicModel.getStudentTopicId(), studentContentModel.getQuiz());
//                    if (isLastWatchedQuiz) {
//                        if (lastWatchedQuizModel != null) {
//                            lastWatchedQuizModel.setLastWatched(false);
//                            studentContentModel.getQuiz().setLastWatched(true);
//                            adapter.notifyDataSetChanged();
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    isClicked = true;
//                                }
//                            }, 500);
//                        }
//                    } else {
//                        if (lastWatchedSubTopicModel != null) {
//                            lastWatchedSubTopicModel.setLastWatched(false);
//                            studentContentModel.getQuiz().setLastWatched(true);
//                            adapter.notifyDataSetChanged();
//                            new Handler().postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    isClicked = true;
//                                }
//                            }, 500);
//                        }
//                    }
                }
            }

        }
    }


    @Override
    public void downloadSubTopic(StudentTopicsModel topicModel, StudentContentModel studentContentModel, int position, int topicPosition) {

        if (studentContentModel.getSubTopic() == null)
            return;


        downloadSubTopicModel = new DownloadSubTopicModel(catalogueModel.getStudentCatalogueId(),
                subjectModel.getStudentSubjectId(),
                unitModel.getStudentUnitId(),
                topicModel.getStudentTopicId(),
                studentContentModel.getSubTopic().getStudentSubTopicId(),
                studentContentModel.getSubTopic().getPageContentId(),
                position
        );

        this.downloadTopicName = topicModel.getTopicName();
        this.downloadSubTopicName = studentContentModel.getSubTopic().getSubTopicName();

        this.downloadTopicPosition = topicPosition;
        this.downloadSubTopicPosition = position;
        title = studentContentModel.getSubTopic().getSubTopicName();
        viewModel.fetchVideoUrl(studentContentModel.getSubTopic().getPageContentId());
    }


    private SubTopicModel updateCurrentSubTopic(SubTopicModel subTopic) {
        currentSubTopicModel = new SubTopicModel();
        currentSubTopicModel.setStudentTopicId(subTopic.getStudentTopicId());
        currentSubTopicModel.setTopicName(subTopic.getTopicName());
        currentSubTopicModel.setSubTopicId(subTopic.getSubTopicId());
        currentSubTopicModel.setSubTopicName(subTopic.getSubTopicName());
        currentSubTopicModel.setTotalVideoDuration(subTopic.getTotalVideoDuration());
        currentSubTopicModel.setVideoCompletionInSeconds(subTopic.getVideoCompletionInSeconds());

        return subTopic;
    }

    public void updateVideoStream(long currentPosition) {
        this.currentVideoPlaybackPosition = currentPosition;
    }

    @Override
    public void doPositiveAction(int id) {
        switch (id) {
            case 1439:
                if (activity() != null) {
                    if (activity() instanceof MainActivity) {
                        if (getParentFragment() == null)
                            return;
                        ((MainActivity) activity()).removeFragment(getParentFragment());
                    }
                }
                break;
        }
    }

    @Override
    public void doNegativeAction() {

    }


    public class TopicsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getBooleanExtra("Update", false)) {
                if (topicId == null || adapter == null) {
                    return;
                }

                Observable<Integer> observable = Observable.create(emitter ->
                {
                    try {
                        if (studentTopicsModelList == null) {
                            emitter.onNext(-1);
                            emitter.onComplete();
                            return;
                        }
                        String topicId = intent.getStringExtra("TopicId");
                        String subTopicId = intent.getStringExtra("SubTopicId");
                        double topicCompletionPercentage = intent.getDoubleExtra("TopicCompletionPercentage", 0);
                        double subTopicCompletionPercentage = intent.getDoubleExtra("SubTopicCompletionPercentage", 0);

                        for (int i = 0; i < studentTopicsModelList.size(); i++) {
                            if (studentTopicsModelList.get(i).getStudentTopicId().equals(topicId)) {
                                studentTopicsModelList.get(i).setCompletionPercentage(topicCompletionPercentage);
                                List<StudentContentModel> subTopics = studentTopicsModelList.get(i).getStudentContents();
                                if (subTopics == null) {
                                    emitter.onNext(i);
                                    emitter.onComplete();
                                    return;
                                }
                                for (int j = 0; j < subTopics.size(); j++) {
                                    if (subTopics.get(j).getSubTopic().getStudentSubTopicId().equals(subTopicId)) {
                                        studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic().setVideoCompletionPercentage(subTopicCompletionPercentage);
                                        emitter.onNext(i);
                                        emitter.onComplete();
                                        return;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        emitter.onError(e);
                    }
                    emitter.onComplete();

                });
                observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Integer>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Integer position) {
                                if (position != null && position != -1) {
                                    if (adapter != null)
                                        adapter.notifyItemChanged(position + 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            }
        }
    }


    @Override
    public void lastWatchedVideoPosition(int position, SubTopicModel subTopic) {
        lastWatchedSubTopicModel = subTopic;
        isLastWatchedQuiz = false;
    }

    @Override
    public void lastWatchedQuizPosition(int position, QuizModel quiz) {
        lastWatchedQuizModel = quiz;
        isLastWatchedQuiz = true;

    }

    @Override
    public void ratingPage(RatingModel ratingModel) {

        if (topicModel.getSubTopicModel() != null) {
            if (lastWatchedSubTopicModel == null)
                return;
            ((MainActivity) activity()).openRatingPage(catalogueModel.getCatalogueId(), catalogueModel.getCatalogueName()
                    , subjectModel.getSubjectId(), subjectModel.getSubjectName(), unitModel.getUnitName()
                    , lastWatchedSubTopicModel.getTopicId(), lastWatchedSubTopicModel.getTopicName()
                    , lastWatchedSubTopicModel.getStudentSubTopicId(), lastWatchedSubTopicModel.getSubTopicName(), lastWatchedSubTopicModel.getSubTopicId(), ratingModel);
        } else {
            if (studentTopicsModel != null)
                ((MainActivity) activity()).openRatingPage(catalogueModel.getCatalogueId(), catalogueModel.getCatalogueName()
                        , subjectModel.getSubjectId(), subjectModel.getSubjectName(), unitModel.getUnitName()
                        , studentTopicsModel.getTopicId(), studentTopicsModel.getTopicName()
                        , studentTopicsModel.getStudentContents().get(0).getSubTopic().getStudentSubTopicId(), studentTopicsModel.getStudentContents().get(0).getSubTopic().getSubTopicName(),
                        studentTopicsModel.getStudentContents().get(0).getSubTopic().getSubTopicId(), ratingModel);
        }
    }

    @Override
    public void reportPage() {

        if (getParentFragment() != null) {
            if (getParentFragment() instanceof UnitProviderFragment)
                ((UnitProviderFragment) getParentFragment()).clearAutoPlay();
        }
        if (topicModel.getSubTopicModel() != null) {
            if (lastWatchedSubTopicModel == null)
                return;
            ((MainActivity) activity()).openReportPage(catalogueModel.getCatalogueName()
                    , subjectModel.getSubjectName()
                    , unitModel.getUnitName()
                    , lastWatchedSubTopicModel.getTopicName()
                    , lastWatchedSubTopicModel.getSubTopicName());
        } else {
            if (studentTopicsModel != null)
                ((MainActivity) activity()).openReportPage(catalogueModel.getCatalogueName()
                        , subjectModel.getSubjectName()
                        , unitModel.getUnitName()
                        , studentTopicsModel.getTopicName()
                        , studentTopicsModel.getStudentContents().get(0).getSubTopic().getSubTopicName());
        }

    }

    @Override
    public void applyRating(int ratingValue, RatingModel ratingModel, String topicName, String subTopicName) {

        if (ratingModel != null) {
            ratingModel.setRating(ratingValue);
            //Post Rating Model in Background Service
            Intent intent = new Intent(activity(), HelperService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_RATING);
            intent.putExtra("RATING", ratingModel);
            activity().startService(intent);

            Mixpanel.addRating(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName(), topicName, subTopicName, ratingValue);

            adapter.updateRating(ratingModel);
        } else {
            if (lastWatchedSubTopicModel == null)
                return;

            ratingModel = new RatingModel();
            ratingModel.setRating(ratingValue);
            ratingModel.setCatalogueId(catalogueModel.getCatalogueId());
            ratingModel.setCatalogueName(catalogueModel.getCatalogueId());
            ratingModel.setSubjectId(subjectModel.getSubjectId());
            ratingModel.setSubjectName(subjectModel.getSubjectName());
            ratingModel.setTopicId(lastWatchedSubTopicModel.getTopicId());
            ratingModel.setTopicName(lastWatchedSubTopicModel.getTopicName());
            ratingModel.setStudentSubTopicId(lastWatchedSubTopicModel.getStudentSubTopicId());
            ratingModel.setSubTopicId(lastWatchedSubTopicModel.getSubTopicId());

            //Post Rating Model in Background Service
            Intent intent = new Intent(activity(), HelperService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_RATING);
            intent.putExtra("RATING", ratingModel);
            activity().startService(intent);

            Mixpanel.addRating(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName(), topicName, subTopicName, ratingValue);
            adapter.updateRating(ratingModel);
        }
    }

    @Override
    public void showOnlyDownloads(boolean show) {
        this.showOnlyDownloaded = show;
        if (show) {
            binding.progressBar.setVisibility(View.VISIBLE);
            viewModel.showOnlyDownloadedVideosInfo(unitModel.getStudentUnitId());
        } else {
            binding.progressBar.setVisibility(View.GONE);
            viewModel.stopOfflineData();
            studentTopicsModelList = viewModel.getTopicsModelList();
            adapter.updateList(studentTopicsModelList);
        }
    }

    public void updateRating(RatingModel ratingModelData) {

        if (adapter != null)
            adapter.updateRating(ratingModelData);
    }

    @Override
    public void onStart() {
        super.onStart();
        registerReceiver();
        downloadTracker.addListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshOfflineData();
    }

    private void registerReceiver() {

        try {
            if (broadcastReceiver == null)
                return;

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_UPDATE_TOPIC_PROGRESS);
            activity().registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }

        downloadTracker.removeListener(this);

        if (broadcastReceiver == null)
            return;
        activity().unregisterReceiver(broadcastReceiver);


    }


    @Override
    public void onDownloadsChanged() {
        Logger.d("DOWNLOAD", "CHANGED " + System.currentTimeMillis());

    }

    @Override
    public void onProgressChanged(int id, int percentage) {

    }


    private void refreshOfflineData() {

        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
        handler = new Handler();
        handler.post(new RefreshRunnable(this));
    }

    private class RefreshRunnable implements Runnable {

        private WeakReference<UnitOverViewFragment> unitOverViewFragmentWeakReference;

        private RefreshRunnable(UnitOverViewFragment unitOverViewFragment) {
            unitOverViewFragmentWeakReference = new WeakReference<>(unitOverViewFragment);
        }

        @Override
        public void run() {
            Logger.d("RUNNING", "REFRESH " + System.currentTimeMillis() / 1000);

            if (handler != null)
                handler.postDelayed(this, 5000);
            if (!unitOverViewFragmentWeakReference.get().viewModel.isNetworkAvailable()) {
                return;
            }
            if (unitOverViewFragmentWeakReference.get().adapter == null) {
                return;
            }

            if (unitOverViewFragmentWeakReference.get().studentTopicsModelList == null) {
                return;
            }

            try {

                adapter.updateList(studentTopicsModelList);
                unitOverViewFragmentWeakReference.get().viewModel.updateOfflineVideosInfo(studentTopicsModelList, showOnlyDownloaded);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void autoPlayNext() {

        try {
            if (studentTopicsModelList != null
                    && studentTopicsModelList.size() > topicPosition
                    && studentTopicsModelList.get(topicPosition).getStudentContents() != null
                    && studentTopicsModelList.get(topicPosition).getStudentContents().size() > subTopicPosition) {
                if (studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition).getSubTopic().getSubTopicId().equals(currentSubTopicModel.getSubTopicId())) { // MATCH IN LIST
                    if (subTopicPosition == studentTopicsModelList.get(topicPosition).getStudentContents().size() - 1) {
                        if (topicPosition == studentTopicsModelList.size() - 1) {
                            Utility.showToast(activity(), "topics are not available", false);
                        } else {
                            if (studentTopicsModelList.get(topicPosition + 1).getStudentContents() != null) {
                                onSubTopicClick(studentTopicsModelList.get(topicPosition + 1), studentTopicsModelList.get(topicPosition + 1).getStudentContents().get(0), 0, topicPosition + 1);
                            } else {
                                Utility.showToast(activity(), "topics are not available", false);
                            }
                        }
                    } else {
                        onSubTopicClick(studentTopicsModelList.get(topicPosition), studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition + 1), subTopicPosition + 1, topicPosition);
                    }

                } else { // UN MATCH LIST
                    for (int i = 0; i < studentTopicsModelList.size(); i++) {
                        if (studentTopicsModelList.get(i).getTopicId().equals(studentTopicsModel.getTopicId())) {
                            if (studentTopicsModelList.get(i).getStudentContents() != null && studentTopicsModelList.get(i).getStudentContents().size() > 0) {
                                for (int j = 0; j < studentTopicsModelList.get(i).getStudentContents().size(); j++) {
                                    if (studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic().getSubTopicId().equals(currentSubTopicModel.getSubTopicId())) {
                                        topicPosition = i;
                                        subTopicPosition = j;
                                        autoPlayNext();
                                        return;
                                    }
                                    if (j == studentTopicsModelList.get(i).getStudentContents().size() - 1) {
                                        onSubTopicClick(studentTopicsModelList.get(i), studentTopicsModelList.get(i).getStudentContents().get(0), 0, i);
                                        return;
                                    }

                                }
                            }
                        }
                        if (i == studentTopicsModelList.size() - 1) {
                            onSubTopicClick(studentTopicsModelList.get(0), studentTopicsModelList.get(0).getStudentContents().get(0), 0, 0);
                        }
                    }
                }
            } else if (studentTopicsModelList.size() == 0) {
                Utility.showToast(activity(), "topics are not available", false);
            } else {
                if (studentTopicsModelList != null && studentTopicsModelList.size() > 0) {
                    for (int i = 0; i < studentTopicsModelList.size(); i++) {
                        if (studentTopicsModelList.get(i).getTopicId().equals(studentTopicsModel.getTopicId())) {
                            if (studentTopicsModelList.get(i).getStudentContents() != null && studentTopicsModelList.get(i).getStudentContents().size() > 0) {
                                for (int j = 0; j < studentTopicsModelList.get(i).getStudentContents().size(); j++) {
                                    if (studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic().getSubTopicId().equals(currentSubTopicModel.getSubTopicId())) {
                                        topicPosition = i;
                                        subTopicPosition = j;
                                        autoPlayNext();
                                        return;
                                    }
                                    if (j == studentTopicsModelList.get(i).getStudentContents().size() - 1) {
                                        onSubTopicClick(studentTopicsModelList.get(i), studentTopicsModelList.get(i).getStudentContents().get(j), j, i);
                                        return;
                                    }

                                }
                            }
                        }
                        if (i == studentTopicsModelList.size() - 1) {
                            Utility.showToast(activity(), "topics are not available", false);
                        }
                    }

                } else {
                    Utility.showToast(activity(), "topics are not available", false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.showToast(activity(), Constants.SOMETHING_WENT_WRONG, false);
        }
    }

    public void autoPlayPrevious() {

        try {
            if (studentTopicsModelList != null
                    && studentTopicsModelList.size() > topicPosition
                    && studentTopicsModelList.get(topicPosition).getStudentContents() != null
                    && studentTopicsModelList.get(topicPosition).getStudentContents().size() > subTopicPosition) {
                if (studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition).getSubTopic().getSubTopicId().equals(currentSubTopicModel.getSubTopicId())) { // MATCH IN LIST
                    if (subTopicPosition == 0) {
                        if (topicPosition == 0) {
                            Utility.showToast(activity(), "topics are not available", false);
                        } else {
                            if (studentTopicsModelList.get(topicPosition - 1).getStudentContents() != null) {
                                onSubTopicClick(studentTopicsModelList.get(topicPosition - 1),
                                        studentTopicsModelList.get(topicPosition - 1).getStudentContents().get(studentTopicsModelList.get(topicPosition - 1).getStudentContents().size() - 1),
                                        studentTopicsModelList.get(topicPosition - 1).getStudentContents().size() - 1,
                                        topicPosition - 1);
                            } else {
                                Utility.showToast(activity(), "topics are not available", false);
                            }
                        }
                    } else {
                        onSubTopicClick(studentTopicsModelList.get(topicPosition), studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition - 1), subTopicPosition - 1, topicPosition);
                    }

                } else { // UN MATCH LIST
                    for (int i = 0; i < studentTopicsModelList.size(); i++) {
                        if (studentTopicsModelList.get(i).getTopicId().equals(studentTopicsModel.getTopicId())) {
                            if (studentTopicsModelList.get(i).getStudentContents() != null && studentTopicsModelList.get(i).getStudentContents().size() > 0) {
                                for (int j = 0; j < studentTopicsModelList.get(i).getStudentContents().size(); j++) {
                                    if (studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic().getSubTopicId().equals(currentSubTopicModel.getSubTopicId())) {
                                        topicPosition = i;
                                        subTopicPosition = j;
                                        autoPlayPrevious();
                                        return;
                                    }
                                    if (j == studentTopicsModelList.get(i).getStudentContents().size() - 1) {
                                        onSubTopicClick(studentTopicsModelList.get(i), studentTopicsModelList.get(i).getStudentContents().get(0), 0, i);
                                        return;
                                    }

                                }
                            }
                        }
                        if (i == studentTopicsModelList.size() - 1) {
                            onSubTopicClick(studentTopicsModelList.get(0), studentTopicsModelList.get(0).getStudentContents().get(0), 0, 0);
                        }
                    }
                }
            } else if (studentTopicsModelList.size() == 0) {
                Utility.showToast(activity(), "topics are not available", false);
            } else {
                if (studentTopicsModelList != null && studentTopicsModelList.size() > 0) {
                    for (int i = 0; i < studentTopicsModelList.size(); i++) {
                        if (studentTopicsModelList.get(i).getTopicId().equals(studentTopicsModel.getTopicId())) {
                            if (studentTopicsModelList.get(i).getStudentContents() != null && studentTopicsModelList.get(i).getStudentContents().size() > 0) {
                                for (int j = 0; j < studentTopicsModelList.get(i).getStudentContents().size(); j++) {
                                    if (studentTopicsModelList.get(i).getStudentContents().get(j).getSubTopic().getSubTopicId().equals(currentSubTopicModel.getSubTopicId())) {
                                        topicPosition = i;
                                        subTopicPosition = j;
                                        autoPlayPrevious();
                                        return;
                                    }
                                    if (j == studentTopicsModelList.get(i).getStudentContents().size() - 1) {
                                        onSubTopicClick(studentTopicsModelList.get(i), studentTopicsModelList.get(i).getStudentContents().get(0), 0, i);
                                        return;
                                    }

                                }
                            }
                        }
                        if (i == studentTopicsModelList.size() - 1) {
                            Utility.showToast(activity(), "topics are not available", false);
                        }
                    }

                } else {
                    Utility.showToast(activity(), "topics are not available", false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.showToast(activity(), Constants.SOMETHING_WENT_WRONG, false);
        }
    }


    private void updateNext() {
        if (studentTopicsModelList != null && studentTopicsModelList.size() > 0) {
            if (topicPosition < studentTopicsModelList.size() && subTopicPosition < studentTopicsModelList.get(topicPosition).getStudentContents().size()) {
                if (topicPosition == studentTopicsModelList.size() - 1 && subTopicPosition == studentTopicsModelList.get(topicPosition).getStudentContents().size() - 1) {
                    ShareDataManager.getInstance().setHasNext(false);
                } else {
                    ShareDataManager.getInstance().setHasNext(true);
                }
            } else {
                ShareDataManager.getInstance().setHasNext(false);
            }
        } else {
            ShareDataManager.getInstance().setHasNext(false);
        }
    }


    private void cancelUpdate() {
        viewModel.cancelUpdate();
    }


}