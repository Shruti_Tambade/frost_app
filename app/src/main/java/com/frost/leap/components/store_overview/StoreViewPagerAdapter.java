package com.frost.leap.components.store_overview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.postprocessors.IterativeBoxBlurPostProcessor;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentBenefitBinding;
import com.frost.leap.databinding.FragmentInstructorsBinding;
import com.frost.leap.databinding.FragmentVideoOverviewBinding;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.benefit.BenefitModel;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.store.model.FeaturesModel;
import apprepos.store.model.InstructorsModel;
import supporters.constants.Constants;
import supporters.utils.Utility;


/**
 * Created by Chenna Rao on 10/1/2019.
 */
public class StoreViewPagerAdapter extends PagerAdapter {

    private int size;
    private LayoutInflater inflater;
    private FragmentVideoOverviewBinding binding;
    private FragmentBenefitBinding fragmentBenefitBinding;
    private FragmentInstructorsBinding fragmentInstructorsBinding;
    private Context context;
    private int type = 0;
    //    private List<String> thumbnail;isViewFromObject
    private CatalogueOverviewModel catalogueOverviewModel;
    public ViewDataBinding baseDataBinding;
    private List<List<InstructorsModel>> instructorsList;
    private List<List<FeaturesModel>> featuresModelArrayList;
    private List<BenefitModel> benefitModelArrayList = new ArrayList<>();
    private IOverviewFragment iOverviewFragment;

    public StoreViewPagerAdapter(IOverviewFragment iOverviewFragment, Context context, int size, int type, Object object) {
        this.context = context;
        this.size = size;
        this.type = type;
        this.iOverviewFragment = iOverviewFragment;
        this.instructorsList = (List<List<InstructorsModel>>) object;
        this.featuresModelArrayList = (List<List<FeaturesModel>>) object;
        this.benefitModelArrayList = (List<BenefitModel>) object;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public StoreViewPagerAdapter(Context context, int size, Object object, int type) {
        this.context = context;
        this.size = size;
        this.type = type;
        this.catalogueOverviewModel = (CatalogueOverviewModel) object;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        if (type == 0) {
            baseDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_overview, container, false);
            binding = (FragmentVideoOverviewBinding) baseDataBinding;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {

                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(catalogueOverviewModel.getVideoPaths().get(position).getThumbnail()))
                        .setPostprocessor(new IterativeBoxBlurPostProcessor(5))
                        .build();

                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(binding.ivPreviewImage.getController())
                        .build();
                binding.ivPreviewImage.setController(controller);

            } else {

                Glide.with(context)
                        .load(catalogueOverviewModel.getVideoPaths().get(position).getThumbnail())
                        .into(binding.ivPreviewImage);
            }

            binding.ivPreviewImage.setOnClickListener(v -> {

                Intent intent = new Intent(context, HelperActivity.class);
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.VIDEO_PLAYER);
                intent.putExtra("VIDEO_URL", catalogueOverviewModel.getVideoPaths().get(position));
                context.startActivity(intent);

            });

        } else if (type == 1) {

            baseDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_benefit, container, false);
            fragmentBenefitBinding = (FragmentBenefitBinding) baseDataBinding;

            fragmentBenefitBinding.ivImage.setImageResource(benefitModelArrayList.get(position).getImage());
            fragmentBenefitBinding.tvTitle.setText(benefitModelArrayList.get(position).getName());
            fragmentBenefitBinding.tvDescription.setText(benefitModelArrayList.get(position).getDescription());

        } else if (type == 2) {

            baseDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_instructors, container, false);
            fragmentInstructorsBinding = (FragmentInstructorsBinding) baseDataBinding;

            List<InstructorsModel> instructorsModel = instructorsList.get(position);
            fragmentInstructorsBinding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            fragmentInstructorsBinding.recyclerView.setAdapter(new InstructorAdapter(iOverviewFragment, context, instructorsModel));
            fragmentInstructorsBinding.recyclerView.setHasFixedSize(true);
            fragmentInstructorsBinding.recyclerView.setNestedScrollingEnabled(false);

        } else if (type == 3) {

            baseDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_instructors, container, false);
            fragmentInstructorsBinding = (FragmentInstructorsBinding) baseDataBinding;

            List<FeaturesModel> featuresModels = featuresModelArrayList.get(position);
            fragmentInstructorsBinding.recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
            fragmentInstructorsBinding.recyclerView.setAdapter(new FeaturesAdapter(context, featuresModels));
            fragmentInstructorsBinding.recyclerView.setHasFixedSize(true);
            fragmentInstructorsBinding.recyclerView.setNestedScrollingEnabled(false);
        }

        container.addView(baseDataBinding.getRoot());
        return baseDataBinding.getRoot();
    }

    @Override
    public int getCount() {
        return size;
    }
}
