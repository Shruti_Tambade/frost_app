package com.frost.leap.components.profile.support;

import com.frost.leap.components.media.models.GalleryFile;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 03-10-2019.
 * <p>
 * FROST
 */
public interface IPickedItemAdapter
{
    public void deleteItem(GalleryFile galleryFile, int position);
}
