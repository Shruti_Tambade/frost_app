package com.frost.leap.components.updates;

import apprepos.updates.model.UpdateModel;

public interface IUpdateAdapter {

    void redirectToDoubtPage(UpdateModel updateModel, int position);

    void retryUpdateAPI();

    void redirectTOAllUpdates();

    void openPDF(String pdfFile, String title);
}
