package com.frost.leap.components.updates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemAllUpdatesCardBinding;
import com.frost.leap.databinding.ItemGeneralCardBinding;
import com.frost.leap.databinding.ItemImportantCardBinding;
import com.frost.leap.databinding.ItemLiveSessionCardBinding;
import com.frost.leap.databinding.ItemTechnicalCardBinding;
import com.frost.leap.databinding.ItemTestCardBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.updates.model.UpdateModel;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 11/05/2020.
 */
public class CatalogueUpdatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private boolean isLive = true;
    private IUpdateAdapter iUpdateAdapter;
    private List<UpdateModel> list;
    private List<String> catalogueIDS;
    private boolean isAllUpdates;
    private RController rController;

    public CatalogueUpdatesAdapter(Context context, IUpdateAdapter iUpdateAdapter, List<UpdateModel> list, List<String> catalogueIDS, boolean isAllUpdates, RController rController) {
        this.context = context;
        this.iUpdateAdapter = iUpdateAdapter;
        this.list = list;
        this.catalogueIDS = catalogueIDS;
        this.isAllUpdates = isAllUpdates;
        this.rController = rController;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skelton_update_card, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else {
            switch (type) {
                case 0:
                    return new LiveSessionCardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_live_session_card, viewGroup, false));
                case 1:
                    return new TestCardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_test_card, viewGroup, false));
                case 2:
                    return new GeneralCardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_general_card, viewGroup, false));
                case 3:
                    return new ImportantViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_important_card, viewGroup, false));
                case 4:
                    return new TechnicalCardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_technical_card, viewGroup, false));
                case 5:
                    return new AllUpdatesCardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_all_updates_card, viewGroup, false));
                default:
                    return new LiveSessionCardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                            R.layout.item_live_session_card, viewGroup, false));

            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SkeletonViewHolder) {
            SkeletonViewHolder skeletonViewHolder = (SkeletonViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params.setMargins(
                    Utility.dpSize(context, isAllUpdates ? 15 : 20),
                    Utility.dpSize(context, position == 0 ? 20 : 10),
                    Utility.dpSize(context, 18),
                    Utility.dpSize(context, position == 8 - 1 ? 15 : 0));
            skeletonViewHolder.itemView.setLayoutParams(params);


        } else if (holder instanceof LiveSessionCardViewHolder) {
            LiveSessionCardViewHolder liveSessionCardViewHolder = (LiveSessionCardViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!isAllUpdates) {
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 20 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0));
            } else
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 0 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == list.size() - 1 ? 35 : 0));

            liveSessionCardViewHolder.itemView.setLayoutParams(params);

            liveSessionCardViewHolder.bindData(position, list.get(position));

        } else if (holder instanceof TestCardViewHolder) {
            TestCardViewHolder testCardViewHolder = (TestCardViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!isAllUpdates) {
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 20 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0));
            } else
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 0 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == list.size() - 1 ? 35 : 0));
            testCardViewHolder.itemView.setLayoutParams(params);

            testCardViewHolder.bindData(position, list.get(position));

        } else if (holder instanceof GeneralCardViewHolder) {
            GeneralCardViewHolder generalCardViewHolder = (GeneralCardViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!isAllUpdates) {
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 20 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0));
            } else
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 0 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == list.size() - 1 ? 35 : 0));
            generalCardViewHolder.itemView.setLayoutParams(params);

            generalCardViewHolder.bindData(position, list.get(position));

        } else if (holder instanceof ImportantViewHolder) {
            ImportantViewHolder importantViewHolder = (ImportantViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!isAllUpdates) {
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 20 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0));
            } else
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 0 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == list.size() - 1 ? 35 : 0));
            importantViewHolder.itemView.setLayoutParams(params);

            importantViewHolder.bindData(position, list.get(position));

        } else if (holder instanceof TechnicalCardViewHolder) {
            TechnicalCardViewHolder technicalCardViewHolder = (TechnicalCardViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if (!isAllUpdates) {
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 20 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0));
            } else
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == 0 ? 0 : 3),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == list.size() - 1 ? 35 : 0));
            technicalCardViewHolder.itemView.setLayoutParams(params);

            technicalCardViewHolder.bindData(position, list.get(position));

        } else if (holder instanceof AllUpdatesCardViewHolder) {
            AllUpdatesCardViewHolder allUpdatesCardViewHolder = (AllUpdatesCardViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            params.setMargins(
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 5),
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 35));
            allUpdatesCardViewHolder.itemView.setLayoutParams(params);

            allUpdatesCardViewHolder.bindData();

        }
    }

    @Override
    public int getItemViewType(int position) {
        return rController == RController.DONE
                ? isAllUpdates ? getExactType(position)
                : position == list.size() ? 5 : getExactType(position)
                : position;
    }

    private int getExactType(int position) {
        switch (list.get(position).getUpdateType()) {
            case "LIVE_SESSION":
                return 0;
            case "TEST":
                return 1;
            case "GENERAL":
                return 2;
            case "IMPORTANT":
                return 3;
            case "TECHNICAL":
                return 4;

            default:
                return 0;
        }
    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING
                ? 8
                : isAllUpdates ? list.size() : list.size() + 1;
    }

    public class LiveSessionCardViewHolder extends RecyclerView.ViewHolder {

        private ItemLiveSessionCardBinding binding;

        public LiveSessionCardViewHolder(ItemLiveSessionCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position, UpdateModel updateModel) {

            binding.liveSessionCard.setOnClickListener(v -> {
                iUpdateAdapter.redirectToDoubtPage(updateModel, position);
            });

            binding.liveSessionCard.setVisibility(View.VISIBLE);

            binding.llSessionLive.setVisibility(updateModel.isLiveNow() ? View.VISIBLE : View.GONE);
            binding.tvSessionDate.setVisibility(updateModel.isLiveNow() ? View.GONE : View.VISIBLE);

            if (updateModel.getUpdateTitle() != null && !updateModel.getUpdateTitle().isEmpty())
                binding.tvSessionTitle.setText(updateModel.getUpdateTitle());

            if (updateModel.getUpdateLabel() != null && !updateModel.getUpdateLabel().isEmpty())
                binding.tvSessionLable.setText(updateModel.getUpdateLabel());

            if (updateModel.getEffectiveDate() != null && !updateModel.getEffectiveDate().isEmpty())
                binding.tvSessionDate.setText(Utility.getUpdatePageDate(updateModel.getEffectiveDate(), updateModel));

            if (updateModel.getCatalogueList() != null) {
                int idSize = 0;
                if (updateModel.getCatalogueList().size() > 1) {
                    for (int i = 0; i < updateModel.getCatalogueList().size(); i++) {
                        for (int j = 0; j < catalogueIDS.size(); j++) {
                            if (updateModel.getCatalogueList().get(i).getCatalogueId().equals(catalogueIDS.get(j))) {
                                idSize++;
                                binding.tvSesionCatalogueName.setText(updateModel.getCatalogueList().get(i).getCatalogueName());
                            }
                        }
                    }

                    if (idSize == 1) {
                        binding.tvSesionCatalogueName.setVisibility(View.VISIBLE);
                    } else {
                        binding.tvSesionCatalogueName.setVisibility(View.GONE);
                    }

                } else if (updateModel.getCatalogueList().get(0).getCatalogueName() != null && !updateModel.getCatalogueList().get(0).getCatalogueName().isEmpty())
                    binding.tvSesionCatalogueName.setText(updateModel.getCatalogueList().get(0).getCatalogueName());
            }

            if (updateModel.getStartTime() != null && !updateModel.getStartTime().isEmpty())
                binding.tvSessionStartTIme.setText(Utility.getTimeInAMPM(updateModel.getStartTime()));

            if (updateModel.getEndTime() != null && !updateModel.getEndTime().isEmpty())
                binding.tvSessionEndTime.setText(Utility.getTimeInAMPM(updateModel.getEndTime()));

        }
    }

    public class TestCardViewHolder extends RecyclerView.ViewHolder {

        private ItemTestCardBinding binding;

        public TestCardViewHolder(ItemTestCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }

        public void bindData(int position, UpdateModel updateModel) {

            binding.testNumberAbility.setOnClickListener(v -> {
                iUpdateAdapter.redirectToDoubtPage(updateModel, position);
            });

            if (updateModel.getUpdateTitle() != null && !updateModel.getUpdateTitle().isEmpty())
                binding.tvTestTitle.setText(updateModel.getUpdateTitle());

            if (updateModel.getCatalogueList() != null)
                if (updateModel.getCatalogueList().get(0).getCatalogueName() != null && !updateModel.getCatalogueList().get(0).getCatalogueName().isEmpty())
                    binding.tvTestCatalogueName.setText(updateModel.getCatalogueList().get(0).getCatalogueName());

            if (updateModel.getEffectiveDate() != null && !updateModel.getEffectiveDate().isEmpty())
                binding.tvTestDate.setText("" + Utility.getUpdatePageDate(updateModel.getEffectiveDate(), updateModel)
                        + " | " + Utility.getTimeInAMPM(updateModel.getStartTime())
                        + " - " + Utility.getTimeInAMPM(updateModel.getEndTime()));

            if (list.get(position).getTestList() != null)
                binding.recyclerView.setAdapter(new TestAdapter(context, iUpdateAdapter, list.get(position).getTestList()));

        }
    }

    public class GeneralCardViewHolder extends RecyclerView.ViewHolder {

        private ItemGeneralCardBinding binding;

        public GeneralCardViewHolder(ItemGeneralCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position, UpdateModel updateModel) {

            binding.generalUpdateCard.setOnClickListener(v -> {
                iUpdateAdapter.openPDF(updateModel.getPdfLink(), "General Update");

            });

            if (updateModel.getUpdateTitle() != null && !updateModel.getUpdateTitle().isEmpty())
                binding.tvGeneralTItle.setText(updateModel.getUpdateTitle());

            if (updateModel.getUpdateLabel() != null && !updateModel.getUpdateLabel().isEmpty())
                binding.tvGeneralLable.setText(updateModel.getUpdateLabel());

            if (updateModel.getEffectiveDate() != null && !updateModel.getEffectiveDate().isEmpty())
                binding.tvGeneralDate.setText(Utility.getUpdatePageDate(updateModel.getEffectiveDate(), updateModel));

        }
    }

    public class ImportantViewHolder extends RecyclerView.ViewHolder {

        private ItemImportantCardBinding binding;

        public ImportantViewHolder(ItemImportantCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position, UpdateModel updateModel) {


            binding.importantCard.setOnClickListener(v -> {
                iUpdateAdapter.openPDF(updateModel.getPdfLink(), "Important Message");
            });

            if (updateModel.getUpdateTitle() != null && !updateModel.getUpdateTitle().isEmpty())
                binding.tvImportantTitle.setText(updateModel.getUpdateTitle());

            if (updateModel.getUpdateLabel() != null && !updateModel.getUpdateLabel().isEmpty())
                binding.tvImportantLable.setText(updateModel.getUpdateLabel());

            if (updateModel.getEffectiveDate() != null && !updateModel.getEffectiveDate().isEmpty())
                binding.tvImportantDate.setText(Utility.getUpdatePageDate(updateModel.getEffectiveDate(), updateModel));


        }
    }

    public class TechnicalCardViewHolder extends RecyclerView.ViewHolder {

        private ItemTechnicalCardBinding binding;

        public TechnicalCardViewHolder(ItemTechnicalCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position, UpdateModel updateModel) {

            binding.technicalCard.setOnClickListener(v -> {
                iUpdateAdapter.openPDF(updateModel.getPdfLink(), "Technical");

            });

            if (updateModel.getUpdateTitle() != null && !updateModel.getUpdateTitle().isEmpty())
                binding.tvTechnicalTitle.setText(updateModel.getUpdateTitle());

            if (updateModel.getUpdateLabel() != null && !updateModel.getUpdateLabel().isEmpty())
                binding.tvTechnicalLable.setText(updateModel.getUpdateLabel());

            if (updateModel.getEffectiveDate() != null && !updateModel.getEffectiveDate().isEmpty())
                binding.tvTechnicalDate.setText(Utility.getUpdatePageDate(updateModel.getEffectiveDate(), updateModel));

        }
    }

    public class AllUpdatesCardViewHolder extends RecyclerView.ViewHolder {

        private ItemAllUpdatesCardBinding binding;

        public AllUpdatesCardViewHolder(ItemAllUpdatesCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData() {

            binding.tvPastUpdatesContent.setOnClickListener(v -> {
                iUpdateAdapter.redirectTOAllUpdates();
            });
        }
    }


}
