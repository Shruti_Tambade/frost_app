package com.frost.leap.components.quiz;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentQuizOverViewBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.quiz.QuizViewModel;

import apprepos.quiz.model.QuizQuestionsModel;
import apprepos.topics.model.QuizModel;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link QuizOverViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizOverViewFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentQuizOverViewBinding binding;
    private QuizModel quiz;
    private String subjectId, unitId, topicId;
    private QuizViewModel viewModel;
    private QuizQuestionsModel quizQuestionsModel;

    public QuizOverViewFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static QuizOverViewFragment newInstance(QuizModel quiz, String subjectId, String unitId, String topicId) {
        QuizOverViewFragment fragment = new QuizOverViewFragment();
        Bundle args = new Bundle();
        args.putString("SubjectId", subjectId);
        args.putString("UnitId", unitId);
        args.putString("TopicId", topicId);
        fragment.quiz = quiz;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(QuizViewModel.class);
        if (getArguments() != null) {
            subjectId = getArguments().getString("SubjectId");
            unitId = getArguments().getString("UnitId");
            topicId = getArguments().getString("TopicId");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_quiz_over_view;
    }


    @Override
    public void setUp() {
        binding = (FragmentQuizOverViewBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.llQuiz.setVisibility(View.GONE);
        binding.llQuizReview.setVisibility(View.GONE);
        binding.llRetry.setVisibility(View.GONE);


        binding.tvSkip.setOnClickListener(v -> {
            showSnackBar("quiz skip", 2);
        });

        binding.llShowResult.setOnClickListener(v -> {
            gotoQuizResult();
        });

        binding.llStartQuiz.setOnClickListener(v -> {
            gotoQuizStart();
        });

        binding.llRetakeQuiz.setOnClickListener(v -> {
            gotoQuizStart();
        });

        binding.llReviewQuiz.setOnClickListener(v -> {
            gotoQuizReview();
        });

        binding.tvRetry.setOnClickListener(v -> {
            binding.llQuiz.setVisibility(View.GONE);
            binding.llQuizReview.setVisibility(View.GONE);
            binding.llRetry.setVisibility(View.GONE);
            viewModel.getQuizQuestions( quiz.getQuizId());

        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case QuizViewModel.NetworkTags.QUIZ_QUESTIONS:
                    updateUIForQuizQuestionsList(networkCall);
                    break;
            }
        });

        viewModel.updateQuizQuestions().observe(this, data -> {
            binding.spnLoader.setVisibility(View.GONE);
            this.quizQuestionsModel = data;
            if (quizQuestionsModel.getCompleted()) {
                binding.llQuiz.setVisibility(View.GONE);
                binding.tvResult.setText(Utility.roundTwoDecimals(quizQuestionsModel.getQuizReport().getScorePercentage()) + " %");
                binding.llQuizReview.setVisibility(View.VISIBLE);
            } else {
                binding.llQuiz.setVisibility(View.VISIBLE);
                binding.llQuizReview.setVisibility(View.GONE);
            }

        });


        viewModel.getQuizQuestions( quiz.getQuizId());
    }

    private void updateUIForQuizQuestionsList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.tvMessage.setText(Constants.PLEASE_CHECK_INTERNET);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;

            case IN_PROCESS:
                binding.llRetry.setVisibility(View.GONE);
                binding.spnLoader.setVisibility(View.VISIBLE);
                break;

            case NO_DATA:
                binding.tvMessage.setText(Constants.NO_DATA_AVAILABLE);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;

            case FAIL:
                binding.tvMessage.setText(Constants.SOMETHING_WENT_WRONG);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;

            case ERROR:
                binding.tvMessage.setText(Constants.SOMETHING_WENT_WRONG);
                binding.llRetry.setVisibility(View.VISIBLE);
                binding.spnLoader.setVisibility(View.GONE);
                break;
        }
    }

    private void gotoQuizReview() {
        try {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUIZ_REVIEW);
            intent.putExtra("StudentQuizId", quizQuestionsModel.getStudentQuizId());
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoQuizStart() {
        try {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUIZ_START);
            intent.putExtra("QuizQuestionsModel", quizQuestionsModel);
            intent.putExtra("SubjectId", subjectId);
            intent.putExtra("UnitId", unitId);
            intent.putExtra("TopicId", topicId);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gotoQuizResult() {
        try {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUIZ_RESULT);
            intent.putExtra("QuizQuestionsModel", quizQuestionsModel);
            intent.putExtra("SubjectId", subjectId);
            intent.putExtra("UnitId", unitId);
            intent.putExtra("TopicId", topicId);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeTitle(String title) {

    }
}
