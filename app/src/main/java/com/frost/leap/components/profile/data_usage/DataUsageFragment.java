package com.frost.leap.components.profile.data_usage;


import android.os.Bundle;
import android.view.View;

import androidx.transition.TransitionManager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentDataUsageBinding;

import apprepos.user.UserRepositoryManager;
import supporters.constants.Constants;

/**
 * A simple {@link} subclass.
 * Use the {@link DataUsageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DataUsageFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentDataUsageBinding binding;
    private boolean isVideoQualityLayout = false;

    private UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();

    public DataUsageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DataUsageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DataUsageFragment newInstance(String param1, String param2) {
        DataUsageFragment fragment = new DataUsageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_data_usage;
    }

    @Override
    public void setUp() {
        binding = (FragmentDataUsageBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        binding.tvArrow3.setOnClickListener(v -> {

            if (isVideoQualityLayout) {
                isVideoQualityLayout = false;
                binding.tvArrow3.animate().rotation(180).start();
                binding.videoLayout.setVisibility(View.GONE);

            } else {
                isVideoQualityLayout = true;
                binding.tvArrow3.animate().rotation(0).start();
                binding.videoLayout.setVisibility(View.VISIBLE);
            }
            TransitionManager.beginDelayedTransition(binding.videoLayout);
        });

        binding.checkCellular.setOnClickListener(v -> {

            binding.checkCellular.setChecked(true);
            binding.checkOnlyWifi.setChecked(false);
            binding.checkNever.setChecked(false);
            userRepositoryManager.appSharedPreferences.setString(Constants.VIDEO_QUALITY, "cellular");

            binding.tvHighQualityVideo.setText("on cellular or Wif-Fi");
        });

        binding.checkOnlyWifi.setOnClickListener(v -> {

            binding.checkCellular.setChecked(false);
            binding.checkOnlyWifi.setChecked(true);
            binding.checkNever.setChecked(false);
            userRepositoryManager.appSharedPreferences.setString(Constants.VIDEO_QUALITY, "wifi");

            binding.tvHighQualityVideo.setText("on Wif-Fi");
        });

        binding.checkNever.setOnClickListener(v -> {

            binding.checkCellular.setChecked(false);
            binding.checkOnlyWifi.setChecked(false);
            binding.checkNever.setChecked(true);
            userRepositoryManager.appSharedPreferences.setString(Constants.VIDEO_QUALITY, "never");

            binding.tvHighQualityVideo.setText("never");
        });

        if (userRepositoryManager.appSharedPreferences.getString(Constants.VIDEO_QUALITY) != null) {
            switch (userRepositoryManager.appSharedPreferences.getString(Constants.VIDEO_QUALITY)) {
                case "cellular":
                    binding.checkCellular.setChecked(true);
                    binding.checkOnlyWifi.setChecked(false);
                    binding.checkNever.setChecked(false);
                    binding.tvHighQualityVideo.setText("on cellular or Wif-Fi");
                    break;
                case "wifi":
                    binding.checkCellular.setChecked(false);
                    binding.checkOnlyWifi.setChecked(true);
                    binding.checkNever.setChecked(false);
                    binding.tvHighQualityVideo.setText("on Wif-Fi");
                    break;
                case "never":
                    binding.checkCellular.setChecked(false);
                    binding.checkOnlyWifi.setChecked(false);
                    binding.checkNever.setChecked(true);
                    binding.tvHighQualityVideo.setText("never");
                    break;
            }
        }
    }

    @Override
    public void changeTitle(String title) {

    }
}
