package com.frost.leap.components.web;


import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentHelperWebBinding;

import supporters.utils.Logger;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelperWebFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelperWebFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String url;
    private FragmentHelperWebBinding binding;

    public HelperWebFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HelperWebFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HelperWebFragment newInstance(String param1, String param2) {
        HelperWebFragment fragment = new HelperWebFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public static HelperWebFragment newInstance(String url) {
        HelperWebFragment fragment = new HelperWebFragment();
        Bundle args = new Bundle();
        args.putString("Url", url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString("Url");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_helper_web;
    }


    @Override
    public void setUp() {
        binding = (FragmentHelperWebBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        if (url == null)
            return;
        Logger.d("URL", url);

        binding.webView.getSettings().setLoadsImagesAutomatically(true);
        binding.webView.getSettings().setAllowContentAccess(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.loadUrl(url);
        binding.webView.setWebViewClient(new AppWebViewClients());

        binding.tvRetry.setOnClickListener(v -> {
            reload();
        });


        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            reload();
        });

        binding.swipeRefreshLayout.setEnabled(false);
    }


    public void reload() {
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.tvPlaceHolderWeb.setVisibility(View.VISIBLE);
        binding.webView.setVisibility(View.VISIBLE);
        binding.rlRetry.setVisibility(View.GONE);
        binding.webView.getSettings().setLoadsImagesAutomatically(true);
        binding.webView.getSettings().setAllowContentAccess(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.loadUrl(url);
        binding.webView.setWebViewClient(new AppWebViewClients());

    }

    public class AppWebViewClients extends WebViewClient {
        public boolean error = false;

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (error) {
                binding.progressBar.setVisibility(View.GONE);
                binding.tvPlaceHolderWeb.setVisibility(View.VISIBLE);
                binding.rlRetry.setVisibility(View.VISIBLE);
                binding.swipeRefreshLayout.setRefreshing(false);
            } else {
                binding.progressBar.setVisibility(View.GONE);
                binding.tvPlaceHolderWeb.setVisibility(View.GONE);
                binding.rlRetry.setVisibility(View.GONE);
                binding.swipeRefreshLayout.setRefreshing(false);

            }

        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            this.error = true;
            super.onReceivedError(view, request, error);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            error = false;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }


        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity());
            builder.setTitle("Security Alert");
            builder.setMessage("Due to invalid ssl certificates");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    @Override
    public void changeTitle(String title) {

    }
}
