package com.frost.leap.components.store;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemStoreCatalogueListBinding;
import com.frost.leap.databinding.ItemStoreSearchBinding;
import com.frost.leap.databinding.SkeltonCataloguePreviewBinding;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.catalogue.model.SubCategoryModel;
import apprepos.store.model.Banner;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/11/2019.
 */
public class StoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Disposable searchDisposable;
    private Context context;
    private boolean isDark = false;
    private RController rController;
    private List<SubCategoryModel> list;
    private List<SubCategoryModel> filterList;
    private IStoreAdapter iStoreAdapter;
    private StoreCatalogueAdapter storeCatalogueAdapter;
    private Banner banner;
    private TextWatcher textWatcher;

    private String mikeText = null;

    public StoreAdapter(IStoreAdapter iStoreAdapter, Context context, RController rController, List<SubCategoryModel> list, Banner banner) {
        this.context = context;
        this.rController = rController;
        this.list = list;
        filterList = new ArrayList<>();
        filterList.addAll(list);
        this.iStoreAdapter = iStoreAdapter;
        this.banner = banner;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if (rController == RController.LOADING) {
            return new SkeletonTypeViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                    R.layout.skelton_catalogue_preview, viewGroup, false));

        } else if (rController == RController.DONE) {
            if (viewType == 0) {
                return new StoreSearchHeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_store_search, viewGroup, false));
            } else
                return new StoreItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_store_catalogue_list, viewGroup, false));
        } else
            return new StoreItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                    R.layout.item_store_catalogue_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SkeletonTypeViewHolder) {
            SkeletonTypeViewHolder skeletonTypeViewHolder = (SkeletonTypeViewHolder) holder;


        } else if (holder instanceof StoreSearchHeaderViewHolder) {
            StoreSearchHeaderViewHolder storeHeaderViewHolder = (StoreSearchHeaderViewHolder) holder;
            storeHeaderViewHolder.binding.rlHeader.setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, Utility.getScreenWidth(context) * 8 / 16));
            storeHeaderViewHolder.bindData(position);

            storeHeaderViewHolder.binding.imgBackground.setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, Utility.getScreenWidth(context) * 10 / 16));
            storeHeaderViewHolder.bindData(position);

        } else if (holder instanceof StoreItemViewHolder) {
            StoreItemViewHolder storeItemViewHolder = (StoreItemViewHolder) holder;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, Utility.dpSize(context, position == filterList.size() ? 25 : 0));
            storeItemViewHolder.itemView.setLayoutParams(params);

            storeItemViewHolder.bindData(position);
        }

    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING ? 2 : filterList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setMikeText(String mikeText) {
        this.mikeText = mikeText;
        filterCatalogues(mikeText.toLowerCase());
    }

    private class StoreItemViewHolder extends RecyclerView.ViewHolder {
        private ItemStoreCatalogueListBinding binding;

        public StoreItemViewHolder(ItemStoreCatalogueListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerView.setHasFixedSize(true);
            binding.recyclerView.setNestedScrollingEnabled(false);
        }

        public void bindData(int position) {

            if (filterList.get(position - 1).getSubCategoryName() != null && !filterList.get(position - 1).getSubCategoryName().isEmpty())
                binding.tvFeatureTitle.setText(filterList.get(position - 1).getSubCategoryName());

            binding.recyclerView.setAdapter(storeCatalogueAdapter = new StoreCatalogueAdapter(iStoreAdapter, context, filterList.get(position - 1).getCatalogues(), RController.DONE, list.get(position - 1).getSubCategoryName()));
        }
    }

    private class StoreSearchHeaderViewHolder extends RecyclerView.ViewHolder {

        private ItemStoreSearchBinding binding;

        public StoreSearchHeaderViewHolder(ItemStoreSearchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.etSearch.addTextChangedListener(textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s != null && s.length() > 0) {
                        binding.imgClear.setVisibility(View.VISIBLE);
                        String query = s.toString();
                        if (list != null) {
                            filterCatalogues(query.toLowerCase());
                        }
                    } else {
                        binding.imgClear.setVisibility(View.GONE);
                        filterCatalogues(null);
                        mikeText = null;
                    }

                }
            });


            binding.imgClear.setOnClickListener(v -> {
                binding.imgClear.setVisibility(View.GONE);
                binding.etSearch.setText("");
                filterCatalogues(null);
            });

        }

        public void bindData(int position) {

            if (textWatcher != null && mikeText != null) {
                binding.etSearch.removeTextChangedListener(textWatcher);
                binding.etSearch.setText(mikeText);
                binding.imgClear.setVisibility(View.VISIBLE);
                mikeText = null;
                binding.etSearch.addTextChangedListener(textWatcher);
            }

            binding.imgMic.setOnClickListener(v -> {
                iStoreAdapter.enableMike();
            });

            if (banner != null && banner.isUpdate()) {
                binding.imgCover.setImageURI(Uri.parse(banner.getImage()));
                binding.tvTag.setText(banner.getMessage());
                binding.tvTag.setTextColor(Color.parseColor(banner.getTextColor()));
                binding.tvTitle.setTextColor(Color.parseColor(banner.getTextColor()));
                binding.imgBackground.setColorFilter(Color.parseColor(banner.getBgColor()));
            } else {
                binding.imgCover.setActualImageResource(R.drawable.store_header_icon);
                binding.imgCover.setImageResource(R.drawable.store_header_icon);
                binding.tvTag.setText(Constants.APP_TAG_MESSAGE);
                binding.tvTag.setTextColor(Color.parseColor("#000000"));
                binding.tvTitle.setTextColor(Color.parseColor("#000000"));
                binding.imgBackground.setColorFilter(ContextCompat.getColor(context, R.color.store_shadow_color));
            }
        }

    }

    private void filterCatalogues(String query) {
        int size = getItemCount();
        if (query == null || query.length() == 0) {
            filterList = new ArrayList<>();
            filterList.addAll(list);
            notifyItemRangeChanged(1, getItemCount());
        } else {
            if (searchDisposable != null && !searchDisposable.isDisposed()) {
                searchDisposable.dispose();
            }
            Observable<Boolean> observable = Observable.create(emitter -> {
                filterList = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    SubCategoryModel subCategoryModel = list.get(i).getClone();
                    if (subCategoryModel != null) {
                        List<CatalogueModel> catalogueModels = new ArrayList<>();
                        for (int j = 0; j < subCategoryModel.getCatalogues().size(); j++) {
                            if (subCategoryModel.getCatalogues().get(j).getCatalogueName().toLowerCase().contains(query))
                                catalogueModels.add(subCategoryModel.getCatalogues().get(j));
                        }
                        if (catalogueModels.size() > 0) {
                            subCategoryModel.setCatalogues(catalogueModels);
                            filterList.add(subCategoryModel);
                        }
                    }
                }
                emitter.onNext(true);
                emitter.onComplete();
            });
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            searchDisposable = d;

                        }

                        @Override
                        public void onNext(Boolean result) {
                            if (result) {
                                notifyItemRangeRemoved(1, size);
                                if (filterList.size() > 0) {
                                    notifyItemRangeInserted(1, getItemCount());
                                }
                            }

                            if (mikeText != null)
                                notifyItemChanged(0);
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });

        }

    }


    private class SkeletonTypeViewHolder extends RecyclerView.ViewHolder {

        private SkeltonCataloguePreviewBinding binding;

        SkeletonTypeViewHolder(SkeltonCataloguePreviewBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }


    }


    public void updateBanner(Banner latestBanner) {
        if (latestBanner == null)
            return;
        if (rController == RController.DONE) {
            if (banner == null) {
                this.banner = latestBanner;
                notifyItemChanged(0);
            } else if (latestBanner.isUpdate() != banner.isUpdate()) {
                this.banner = latestBanner;
                notifyItemChanged(0);
            } else if (latestBanner.isUpdate()) {
                if (!banner.getImage().equalsIgnoreCase(latestBanner.getImage())
                        || !banner.getBgColor().equalsIgnoreCase(latestBanner.getBgColor())
                        || !banner.getMessage().equalsIgnoreCase(latestBanner.getMessage())
                        || !banner.getTextColor().equalsIgnoreCase(latestBanner.getTextColor())) {
                    this.banner = latestBanner;
                    notifyItemChanged(0);
                }
            }
        }
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }
}
