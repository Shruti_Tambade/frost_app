package com.frost.leap.components.topics;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemSubtopicBinding;

import java.util.List;

import apprepos.topics.model.StudentContentModel;
import apprepos.topics.model.StudentTopicsModel;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 20-08-2019.
 * <p>
 * Frost
 */
public class SubTopicAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StudentContentModel> list;
    private Context context;
    private int subtopicNumber = 0, quizNumber = 0;
    private StudentTopicsModel studentTopicsModel;
    private boolean lastWatched;
    private ISubTopicsAdapter iSubTopicsAdapter;
    private int topicPosition;

    public SubTopicAdapter(int topicPosition, StudentTopicsModel studentTopicsModel, ISubTopicsAdapter iSubTopicsAdapter, List<StudentContentModel> list, Context context, boolean lastWatched) {
        this.topicPosition = topicPosition;
        this.list = list;
        this.context = context;
        this.studentTopicsModel = studentTopicsModel;
        this.lastWatched = lastWatched;
        this.iSubTopicsAdapter = iSubTopicsAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SubTopicViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_subtopic, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof SubTopicViewHolder) {
            SubTopicViewHolder subTopicViewHolder = (SubTopicViewHolder) viewHolder;
            subTopicViewHolder.bindData(list.get(position), position);


            subTopicViewHolder.binding.rlDownload.setOnClickListener(v -> {
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.alpha_affect);
                v.startAnimation(animation);
                iSubTopicsAdapter.downloadSubTopic(studentTopicsModel, list.get(position), position, topicPosition);
            });

            subTopicViewHolder.itemView.setOnClickListener(v -> {
                if (list.get(position).getSubTopic() == null)
                    return;
                this.notifyItemChanged(position);
                iSubTopicsAdapter.onSubTopicClick(studentTopicsModel, list.get(position), position, topicPosition);
            });

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SubTopicViewHolder extends RecyclerView.ViewHolder {

        private ItemSubtopicBinding binding;

        public SubTopicViewHolder(ItemSubtopicBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(StudentContentModel studentContentModel, int position) {

            if (studentContentModel == null)
                return;

            if (list.get(position).getContentType().equals("SUBTOPIC")) {
                binding.ivDownload.setVisibility(View.VISIBLE);

                binding.tvTotalVideoDuaration.setText(Utility.calculateTimeDuration(studentContentModel.getSubTopic().getTotalVideoDuration()));
//                android.R.drawable.stat_sys_download

                int percentageValue = 0;

                if (studentContentModel.getSubTopic().getVideoCompletionPercentage() != null) {
                    percentageValue = studentContentModel.getSubTopic().getVideoCompletionPercentage().intValue();

                    if ((studentContentModel.getSubTopic().getVideoCompletionPercentage() > 0 && studentContentModel.getSubTopic().getVideoCompletionPercentage() < 100))
                        binding.videoProgressBar.setProgress(percentageValue + 1);
                    else
                        binding.videoProgressBar.setProgress(percentageValue);

                    binding.ivPlayIcon.setVisibility(studentContentModel.getSubTopic().isLastWatched()
                            ? View.VISIBLE
                            : studentContentModel.getSubTopic().getVideoCompletionPercentage() > 99 && studentContentModel.getSubTopic().getVideoCompletionPercentage() <= 100 ? View.VISIBLE : View.GONE);

                    binding.ivPlayIcon.setImageResource(studentContentModel.getSubTopic().getVideoCompletionPercentage() > 99 && studentContentModel.getSubTopic().getVideoCompletionPercentage() <= 100
                            ? R.drawable.ic_progress_done
                            : R.drawable.ic_playing);

                    binding.videoProgressBar.setVisibility(studentContentModel.getSubTopic().isLastWatched()
                            ? View.GONE
                            : studentContentModel.getSubTopic().getVideoCompletionPercentage() > 99 && studentContentModel.getSubTopic().getVideoCompletionPercentage() <= 100 ? View.GONE : View.VISIBLE);


                }

                binding.ivDownload.setImageResource(
                        list.get(position).isFailed() ? R.drawable.ic_info_outline : (
                                list.get(position).getPercentage() > 0 && list.get(position).getPercentage() < 100
                                        ? R.drawable.ic_transparent_icon :
                                        list.get(position).isDownloaded() ? R.drawable.ic_offline : R.drawable.ic_download_icon));

                binding.ivDownload.setColorFilter(ContextCompat.getColor(context,
                        list.get(position).isDownloaded() ? R.color.new_primary_blue : R.color.black));

                binding.progressBar.setSecondaryProgress((int) list.get(position).getPercentage());
                if (list.get(position).getPercentage() > 0 && list.get(position).getPercentage() < 100) {
                    binding.progressBar.setVisibility(View.VISIBLE);
                    binding.ivDownload.setColorFilter(ContextCompat.getColor(context, list.get(position).isFailed()
                            ? R.color.black : R.color.white));
                    binding.ivDownload.setPadding(0, 0, 0, 0);
                    binding.progressBar.setVisibility(list.get(position).isFailed() ? View.GONE : View.VISIBLE);

                } else {
                    binding.progressBar.setVisibility(View.GONE);
                    binding.ivDownload.setPadding(0, 0, 0, 0);
                }


                binding.tvUnitName.setText(list.get(position).getSubTopic().getSubTopicName());

                if (studentContentModel.getSubTopic() != null && studentContentModel.getSubTopic().isLastWatched()) {
                    binding.llLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.last_played_background_color));
                    binding.tvUnitName.setTextColor(ContextCompat.getColor(context, R.color.text_color));
                    iSubTopicsAdapter.lastWatchedVideoPosition(position, studentContentModel.getSubTopic());
                } else
                    binding.llLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));

            } else if (list.get(position).getContentType().equals("QUIZ")) {
                binding.ivDownload.setVisibility(View.GONE);
                binding.progressBar.setVisibility(View.GONE);
                binding.tvUnitName.setText(list.get(position).getQuiz().getName());

                binding.tvTotalVideoDuaration.setText(Utility.calculateTimeDuration(studentContentModel.getSubTopic().getTotalVideoDuration()));

                if (list.get(position).getQuiz() != null && list.get(position).getQuiz().getLastWatched()) {
                    binding.llLayout.setBackgroundResource(R.drawable.item_gradient_last_played);
                    binding.tvUnitName.setTextColor(context.getResources().getColor(R.color.text_color));
                    iSubTopicsAdapter.lastWatchedQuizPosition(position, list.get(position).getQuiz());
                } else
                    binding.llLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));

            }
        }
    }
}
