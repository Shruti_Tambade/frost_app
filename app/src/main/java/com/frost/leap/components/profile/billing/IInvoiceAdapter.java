package com.frost.leap.components.profile.billing;

import apprepos.user.model.Invoice;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public interface IInvoiceAdapter {
    public void downloadInvoice(Invoice invoice, int position);
    public void viewInvoice(Invoice invoice, int position);
}
