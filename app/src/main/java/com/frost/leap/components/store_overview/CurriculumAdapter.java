package com.frost.leap.components.store_overview;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.subject.UnitsAdapter;
import com.frost.leap.databinding.ItemCurriculumBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.store.model.StudentSubjectModel;
import apprepos.subject.model.SubjectModel;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/1/2019.
 */
public class CurriculumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private RController rController;
    private List<StudentSubjectModel> studentSubjectModelList;


    public CurriculumAdapter(Context context, RController rController, List<StudentSubjectModel> studentSubjects) {
        this.context = context;
        this.rController = rController;
        this.studentSubjectModelList = studentSubjects;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skelton_subject, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else
            return new CurriculumViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_curriculum, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CurriculumViewHolder) {
            CurriculumViewHolder curriculumViewHolder = (CurriculumViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 3),
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, position == studentSubjectModelList.size() - 1 ? 110 : 0));

            curriculumViewHolder.itemView.setLayoutParams(params);

            curriculumViewHolder.bindData(position);
        }

    }

    @Override
    public int getItemCount() {
        return studentSubjectModelList.size();
    }

    private class CurriculumViewHolder extends RecyclerView.ViewHolder {

        private ItemCurriculumBinding binding;

        public CurriculumViewHolder(ItemCurriculumBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerView.setHasFixedSize(true);
        }

        public void bindData(int position) {

            if (studentSubjectModelList.get(position).getSubjectName() != null && !studentSubjectModelList.get(position).getSubjectName().isEmpty())
                binding.tvSubjectName.setText(studentSubjectModelList.get(position).getSubjectName());

            if (studentSubjectModelList.get(position).getStudentUnits() != null && studentSubjectModelList.get(position).getStudentUnits().size() > 0) {
                binding.recyclerView.setAdapter(new CurriculumUnitAdapter(context, studentSubjectModelList.get(position).getStudentUnits(), 0));
            }

            binding.recyclerView.setVisibility(studentSubjectModelList.get(position).isSelect()
                    ? View.VISIBLE
                    : View.GONE);

            binding.imgOptions.animate().rotation(studentSubjectModelList.get(position).isSelect()
                    ? 180
                    : 0)
                    .start();

            binding.llLayout.setOnClickListener(v -> {
                studentSubjectModelList.get(position).setSelect(!studentSubjectModelList.get(position).isSelect());
                notifyDataSetChanged();
            });
        }
    }
}
