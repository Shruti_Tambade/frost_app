package com.frost.leap.components.subject.resources;

import apprepos.subject.model.ResourceModel;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/17/2019.
 */
public interface IResourceAdapter {

    public void downloadResource(ResourceModel resourceModel, int position);
    public void viewResource(ResourceModel resourceModel, int position);
}
