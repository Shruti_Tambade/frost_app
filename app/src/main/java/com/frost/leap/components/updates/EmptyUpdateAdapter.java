package com.frost.leap.components.updates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.EmptyUpdateItemBinding;


public class EmptyUpdateAdapter extends RecyclerView.Adapter<EmptyUpdateAdapter.EmptyViewHolder> {

    private Context context;
    private String content;
    private int imageId;
    private int type;
    private IUpdateAdapter iUpdateAdapter;
    private boolean isRetry;


    public EmptyUpdateAdapter(IUpdateAdapter iUpdateAdapter, Context context, String content, int imageId, int type, boolean isRetry) {
        this.context = context;
        this.content = content;
        this.imageId = imageId;
        this.type = type;
        this.iUpdateAdapter = iUpdateAdapter;
        this.isRetry = isRetry;
    }

    @Override
    public EmptyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new EmptyViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.empty_update_item, parent, false));
    }

    @Override
    public void onBindViewHolder(EmptyViewHolder holder, int position) {

        holder.bindData();

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {

        private EmptyUpdateItemBinding binding;

        public EmptyViewHolder(EmptyUpdateItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        public void bindData() {
            binding.tvTitle.setText(content);

            binding.tvRetry.setVisibility(isRetry ? View.GONE : View.VISIBLE);

            binding.tvRetry.setOnClickListener(v -> {
                iUpdateAdapter.retryUpdateAPI();
            });

            if (type == 1) {
                binding.ivInternetImage.setVisibility(View.VISIBLE);
                binding.ivNoData.setVisibility(View.GONE);
                binding.ivInternetImage.setImageResource(imageId);
            } else if (type == 2) {

                binding.ivInternetImage.setVisibility(View.GONE);
                binding.ivNoData.setVisibility(View.VISIBLE);
                binding.ivNoData.setImageResource(imageId);

            } else if (type == 3) {

                binding.ivInternetImage.setVisibility(View.GONE);
                binding.ivNoData.setVisibility(View.VISIBLE);
                binding.ivNoData.setImageResource(imageId);
                binding.tvRetry.setText("Swipe to refresh");
            }
        }
    }
}
