package com.frost.leap.components.store_overview;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentCurriulumBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.store.StoreViewModel;
import com.frost.leap.viewmodels.subjects.SubjectViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.store.model.CatalogueOverviewModel;
import apprepos.subject.model.SubjectModel;
import supporters.constants.Constants;
import supporters.constants.RController;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CurriculumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurriculumFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentCurriulumBinding binding;
    private CatalogueOverviewModel catalogueOverviewModel;

    private List<SubjectModel> subjectModelList;
    private CurriculumAdapter adapter;

    public CurriculumFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CurriculumFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CurriculumFragment newInstance(String param1, String param2) {
        CurriculumFragment fragment = new CurriculumFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catalogueOverviewModel = ((StoreOverviewFragment) getParentFragment()).catalogueOverviewModel;
    }

    @Override
    public int layout() {
        return R.layout.fragment_curriulum;
    }

    @Override
    public void setUp() {
        binding = (FragmentCurriulumBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setFocusable(false);

        if(catalogueOverviewModel==null)
            return;

        binding.recyclerView.setAdapter(adapter = new CurriculumAdapter(activity(), RController.DONE,
                catalogueOverviewModel.getStudentSubjects()== null
                ? new ArrayList<>()
                : catalogueOverviewModel.getStudentSubjects()));
        binding.recyclerView.setHasFixedSize(true);

    }

    @Override
    public void changeTitle(String title) {

    }
}
