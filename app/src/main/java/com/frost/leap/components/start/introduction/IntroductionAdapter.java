package com.frost.leap.components.start.introduction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager.widget.PagerAdapter;

import com.frost.leap.R;
import com.frost.leap.components.start.SignInFragment;
import com.frost.leap.databinding.ItemIntroductionViewBinding;

import java.util.ArrayList;
import java.util.List;

import apprepos.introduction.model.IntroductionModel;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 06/05/2020.
 */
public class IntroductionAdapter extends PagerAdapter {

    private Context context;
    private List<IntroductionModel> list = new ArrayList<>();
    private ItemIntroductionViewBinding binding;
    public ViewDataBinding baseDataBinding;
    private LayoutInflater inflater;
    private IIntroductionAdapter iIntroductionAdapter;

    public IntroductionAdapter(Context context, IIntroductionAdapter iIntroductionAdapter) {
        this.context = context;
        this.iIntroductionAdapter = iIntroductionAdapter;
        this.list = Utility.getIntroductionDetails();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        baseDataBinding = DataBindingUtil.inflate(inflater, R.layout.item_introduction_view, container, false);
        binding = (ItemIntroductionViewBinding) baseDataBinding;

        binding.tvTitle.setText(list.get(position).getTitle());
        binding.tvDescription.setText(list.get(position).getDescription());

        binding.ivCoverPhoto.setImageResource(list.get(position).getImage());

        container.addView(baseDataBinding.getRoot());
        return baseDataBinding.getRoot();
    }

    @Override
    public int getCount() {
        return list.size();
    }
}