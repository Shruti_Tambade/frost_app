package com.frost.leap.components.topics;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 31-01-2020.
 * <p>
 * FROST
 */
public interface IBSDDownloadRequestFragment {
    public void download(int quality);
}
