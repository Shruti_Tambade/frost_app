package com.frost.leap.components.subject.notes;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.frost.leap.R;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentBsdaddNoteBinding;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.services.ActionIntentService;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import apprepos.subject.model.Note;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BSDAddNoteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BSDAddNoteFragment extends BottomSheetDialogFragment implements IFragment, ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private FragmentBsdaddNoteBinding binding;
    private Note note;
    private int position;
    private int height = -1;
    private String noteTitle;

    public BSDAddNoteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p>
     * Parameter 1.
     *
     * @return A new instance of fragment BSDAddNoteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BSDAddNoteFragment newInstance(Note note, int position, int height, String heading) {
        BSDAddNoteFragment fragment = new BSDAddNoteFragment();
        Bundle args = new Bundle();
        args.putParcelable("Note", note);
        args.putInt("Position", position);
        args.putInt("Height", height);
        args.putString("Heading", heading);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            note = getArguments().getParcelable("Note");
            position = getArguments().getInt("Position", -1);
            height = getArguments().getInt("Height", -1);
            noteTitle = getArguments().getString("Heading");

            Logger.d("Note_StudentID", note.getStudentId());
            Logger.d("Note_SubjectID", note.getStudentSubjectId());
            Logger.d("Note_UnitID", note.getStudentUnitId());
            Logger.d("Note_TopicId", note.getStudentTopicId());
            Logger.d("Note_TopicName", note.getTopicName());
            Logger.d("Note_SubTopicId", note.getStudentSubtopicId());
            Logger.d("Note_SubTopicNmae", note.getSubTopicName());
            Logger.d("Note_Video_Legth", String.valueOf(note.getVideoLengthInSeconds()));
            Logger.d("Note_timestampInSecond", String.valueOf(note.getTimestampInSecond()));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bsdadd_note, container, false);

        setUp();
        return binding.getRoot();
    }

    private void setUp() {

        binding.frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));

        binding.tvCancel.setOnClickListener(v -> {
            if (binding.tvSave.getText().equals("Update")) {
                Mixpanel.editNote(note.getCatalogueName(), "Cancelled");
                dismiss();
            } else {
                if (!TextUtils.isEmpty(binding.etNote.getText().toString())) {
                    cancelAlert();
                    return;
                }
                Mixpanel.cancelNote(note.getCatalogueName(), note.getSubjectName(), note.getUnitName(), note.getTopicName(), note.getSubTopicName());
                dismiss();
            }
        });


        binding.tvSave.setOnClickListener(v -> {
            if (!Utility.isNetworkAvailable(activity())) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            if (TextUtils.isEmpty(binding.etNote.getText().toString())) {
                showSnackBar("Note cannot be empty", 2);
                return;
            }
            if (note.getStudentNoteId() != null) {
                updateNote(binding.etNote.getText().toString());
            } else {
                createNote(binding.etNote.getText().toString());
            }

        });

        if (note.getStudentNoteId() != null) {
            binding.tvSave.setText("Update");
            binding.etNote.setText(note.getNote());
        }

        if (!noteTitle.isEmpty())
            binding.tvInfo.setText(Html.fromHtml(noteTitle));
        else {
            binding.tvInfo.setText(Html.fromHtml(note.getTopicName() + "<br/><br/> <b> <font color ='black'>" + note.getSubTopicName() + "</font></b>"));
        }

    }

    private void createNote(String noteText) {
        note.setNote(noteText);
        Intent intent = new Intent(activity(), ActionIntentService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_CREATE_NOTE);
        intent.putExtra("Note", note);
        activity().startService(intent);

        Mixpanel.addNote(note.getCatalogueName(), note.getSubjectName(), note.getUnitName(), note.getTopicName(), note.getSubTopicName());
        dismiss();
    }

    private void updateNote(String noteText) {

        note.setNote(noteText);

        Intent intent = new Intent(activity(), ActionIntentService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_NOTE);
        intent.putExtra("Note", note);
        activity().startService(intent);

        Intent broadCastIntent = new Intent();
        broadCastIntent.putExtra("Update", true);
        broadCastIntent.putExtra("Note", note);
        broadCastIntent.putExtra("Position", position);
        broadCastIntent.setAction(Constants.ACTION_UPDATE_NOTES);
        activity().sendBroadcast(broadCastIntent);

        Mixpanel.editNote(note.getCatalogueName(), "Updated");

        dismiss();
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(activity(), binding.coordinatorLayout, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        if (dialog == null)
            return super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(bottomSheetDialog -> {
            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            // Right here!
            BottomSheetBehavior.from(bottomSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });


        dialog.setOnKeyListener((dialog1, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (note.getStudentNoteId() == null) {
                        if (!TextUtils.isEmpty(binding.etNote.getText().toString())) {
                            cancelAlert();
                            return true;
                        }
                        dialog.dismiss();
                        return true;
                    }
                    dialog.dismiss();
                    return true;
                }
            }
            return true;
        });

        return dialog;
    }


    public void cancelAlert() {
        Utility.requestDialog(activity(), this, "Cancel", "Do you want to cancel?", 9001);
    }


    @Override
    public void doPositiveAction(int id) {
        if (id == 9001) {
            Mixpanel.cancelNote(note.getCatalogueName(), note.getSubjectName(), note.getUnitName(), note.getTopicName(), note.getSubTopicName());
            this.dismiss();
        }
    }

    @Override
    public void doNegativeAction() {

    }
}
