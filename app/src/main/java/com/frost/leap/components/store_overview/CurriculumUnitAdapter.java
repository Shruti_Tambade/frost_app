package com.frost.leap.components.store_overview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemCurriculumUnitBinding;

import java.util.List;

import apprepos.store.model.StudentTopicModel;
import apprepos.store.model.StudentUnitModel;
import apprepos.topics.model.StudentContentModel;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/9/2019.
 */
public class CurriculumUnitAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<StudentUnitModel> studentUnitModelList;
    private List<StudentTopicModel> studentTopicModelList;
    private List<StudentContentModel> studentContentModelList;
    private int type;

    public CurriculumUnitAdapter(Context context, List<StudentUnitModel> studentUnitModelList, int type) {

        this.context = context;
        this.studentUnitModelList = studentUnitModelList;
        this.type = type;

    }

    public CurriculumUnitAdapter(List<StudentTopicModel> studentTopicModelList, Context context, int type) {

        this.context = context;
        this.studentTopicModelList = studentTopicModelList;
        this.type = type;
    }

    public CurriculumUnitAdapter(int type, List<StudentContentModel> studentContentModelList, Context context) {
        this.context = context;
        this.studentContentModelList = studentContentModelList;
        this.type = type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new CurriculumUnitViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_curriculum_unit, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CurriculumUnitViewHolder) {
            CurriculumUnitViewHolder curriculumUnitViewHolder = (CurriculumUnitViewHolder) holder;

            if (type == 0) {
                curriculumUnitViewHolder.itemView.setPadding(25, 0, 15, 0);
            } else if (type == 1) {
                curriculumUnitViewHolder.itemView.setPadding(35, 0, 15, 0);
            } else if (type == 2) {
                curriculumUnitViewHolder.itemView.setPadding(45, 0, 15, 0);
            }

            curriculumUnitViewHolder.bindData(position);
        }

    }

    @Override
    public int getItemCount() {
        return type == 0
                ? studentUnitModelList.size() : type == 1
                ? studentTopicModelList.size() : type == 2
                ? studentContentModelList.size() : 0;
    }

    private class CurriculumUnitViewHolder extends RecyclerView.ViewHolder {

        private ItemCurriculumUnitBinding binding;

        public CurriculumUnitViewHolder(ItemCurriculumUnitBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            binding.recyclerView.setHasFixedSize(true);
        }

        public void bindData(int position) {

            if (type == 0) {

                if (studentUnitModelList.get(position).getUnitName() != null && !studentUnitModelList.get(position).getUnitName().isEmpty())
                    binding.tvName.setText(studentUnitModelList.get(position).getUnitName());

                binding.recyclerView.setVisibility(View.GONE);

//                if (studentUnitModelList.get(position).getStudentTopics() != null && studentUnitModelList.get(position).getStudentTopics().size() > 0) {
//                    binding.recyclerView.setAdapter(new CurriculumUnitAdapter(studentUnitModelList.get(position).getStudentTopics(), context, 1));
//                }

            } else if (type == 1) {

                if (studentTopicModelList.get(position).getTopicName() != null && !studentTopicModelList.get(position).getTopicName().isEmpty())
                    binding.tvName.setText(studentTopicModelList.get(position).getTopicName());

                if (studentTopicModelList.get(position).getStudentContents() != null && studentTopicModelList.get(position).getStudentContents().size() > 0) {
                    binding.recyclerView.setAdapter(new CurriculumUnitAdapter(2, studentTopicModelList.get(position).getStudentContents(), context));
                }
            } else if (type == 2) {

                if (studentContentModelList.get(position).getContentType().equals("SUBTOPIC")) {
                    if (studentContentModelList.get(position).getSubTopic().getSubTopicName() != null && !studentContentModelList.get(position).getSubTopic().getSubTopicName().isEmpty())
                        binding.tvName.setText(studentContentModelList.get(position).getSubTopic().getSubTopicName());
                } else if (studentContentModelList.get(position).getContentType().equals("QUIZ")) {
                    if (studentContentModelList.get(position).getQuiz().getName() != null && !studentContentModelList.get(position).getQuiz().getName().isEmpty())
                        binding.tvName.setText(studentContentModelList.get(position).getQuiz().getName());
                }
            }
        }
    }
}
