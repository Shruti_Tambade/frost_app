package com.frost.leap.components.subject.askexpert;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.support.IPickedItemAdapter;
import com.frost.leap.components.profile.support.PickedItemAdapter;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentAskExpertBinding;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.services.FreshDeskForegroundService;
import com.frost.leap.services.HelperService;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.subjects.AskExpertViewModel;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AskExpertFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AskExpertFragment extends BaseFragment implements IFragment, IPickedItemAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private FragmentAskExpertBinding binding;
    private List<GalleryFile> list;
    private PickedItemAdapter pickedItemAdapter;
    private AskExpertViewModel viewModel;
    private CatalogueModel catalogueModel;

    public AskExpertFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static AskExpertFragment newInstance(CatalogueModel catalogueModel, String param2) {
        AskExpertFragment fragment = new AskExpertFragment();
        Bundle args = new Bundle();
        fragment.catalogueModel = catalogueModel;
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(AskExpertViewModel.class);

    }

    @Override
    public int layout() {
        return R.layout.fragment_ask_expert;
    }

    @Override
    public void setUp() {


        if (catalogueModel == null)
            return;

        binding = (FragmentAskExpertBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.tvSubmit.setBackgroundResource(R.drawable.corner_radius_light_blue_10);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        ((SimpleItemAnimator) binding.recyclerView.getItemAnimator()).setChangeDuration(0L);

        binding.llSelectMedia.setOnClickListener(v -> {
            if (activity() instanceof MainActivity) {
                Constants.GALLERY_LIMIT = 5;
                openCustomGallery();
            }
        });

        binding.tvSubmit.setOnClickListener(v -> {

            Utility.hideKeyboard(activity());

            if (viewModel.doValidation(binding.etSubjectName.getText().toString(),
                    binding.etUnitName.getText().toString(),
                    binding.etTopiName.getText().toString(),
                    binding.etDoubt.getText().toString())) {
                askAnExpert();
            }
        });


        binding.etSubjectName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.subjectInputLayout.setErrorEnabled(false);
                    binding.subjectInputLayout.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etUnitName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.unitInputLayout.setErrorEnabled(false);
                    binding.unitInputLayout.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etTopiName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.topicInputLayout.setErrorEnabled(false);
                    binding.topicInputLayout.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etDoubt.setOnTouchListener((view, event) -> {
            // TODO Auto-generated method stub
            if (view.getId() == R.id.etDoubt) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        });

        binding.etDoubt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tvSubmit.setBackgroundResource(R.drawable.corner_radius_blue_10);
                    binding.doubtInputLayout.setErrorEnabled(false);
                    binding.doubtInputLayout.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                } else
                    binding.tvSubmit.setBackgroundResource(R.drawable.corner_radius_light_blue_10);

            }
        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getError().observe(this, error -> {
            if (error == null)
                return;

            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setError(error.getMessage());
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));
            (binding.getRoot().findViewById(error.getId())).requestFocus();
        });


    }

    private void openCustomGallery() {

        if (Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()) && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.GALLERY_FRAGMENT);// gallery
            intent.putParcelableArrayListExtra("MediaList", null);
            intent.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) getSelectedGallery());
            startActivityForResult(intent, Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY"));
            return;

        } else {
            if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
            else if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.READ_STORAGE, activity());
            else {

            }
        }
    }


    public void updateGalleryFiles(List<GalleryFile> list) {
        if (list != null && list.size() > 0) {
            this.list = list;
            binding.recyclerView.setAdapter(pickedItemAdapter = new PickedItemAdapter(list, activity(), this, 1));

            binding.llSelectMedia.setText("Upload More");

            if (list.size() == 1)
                binding.tvUploadTitle.setText("" + list.size() + " File in attachment");
            else
                binding.tvUploadTitle.setText("" + list.size() + " Files in attachments");
        }
    }

    @Override
    public void deleteItem(GalleryFile galleryFile, int position) {
        if (list != null && list.size() > position) {
            list.remove(position);
            if (pickedItemAdapter != null)
                pickedItemAdapter.notifyDataSetChanged();

            pickedItemAdapter.notifyItemRemoved(position);
            pickedItemAdapter.notifyItemRangeChanged(position, pickedItemAdapter.getItemCount());

            if (list.size() == 1)
                binding.tvUploadTitle.setText("" + list.size() + " File in attachment");
            else
                binding.tvUploadTitle.setText("" + list.size() + " Files in attachments");

            if (list.size() == 0) {
                binding.tvUploadTitle.setText("Upload Files Here");
                binding.llSelectMedia.setText("Upload");
            }

        }
    }

    public List<GalleryFile> getSelectedGallery() {
        return list;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST CODE", "" + requestCode);

        if (resultCode == Activity.RESULT_OK && data != null) {
            List<GalleryFile> galleryFileList = data.getParcelableArrayListExtra("GalleryFileList");
            if (galleryFileList != null && galleryFileList.size() == 0)
                return;
            updateGalleryFiles(galleryFileList);
        }
    }

    private void askAnExpert() {

        Intent intent = new Intent(activity(), FreshDeskForegroundService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_ASK_AN_EXPERT);
        intent.putExtra("IssueType", "Ask an Expert");
        intent.putExtra("Message", binding.etDoubt.getText().toString().trim());
        intent.putExtra("SubjectInfo", catalogueModel.getCatalogueName());
        intent.putExtra("SubjectName", binding.etSubjectName.getText().toString());
        intent.putExtra("UnitName", binding.etUnitName.getText().toString());
        intent.putExtra("TopicName", binding.etTopiName.getText().toString());
        intent.putExtra("SubTopicName", "");
        intent.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) list);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity().startForegroundService(intent);
        } else {
            activity().startService(intent);
        }

        Mixpanel.subtmitQuestionTab(catalogueModel.getCatalogueName(), binding.etSubjectName.getText().toString(), binding.etUnitName.getText().toString(), binding.etTopiName.getText().toString(),
                binding.etDoubt.getText().toString().trim(), list != null ? true : false);

        //Utility.showToast(activity(), "Your ticket is added, we will get back to you soon", false);
        binding.subjectInputLayout.requestFocus();
        binding.scrollView.scrollTo(Math.round(binding.subjectInputLayout.getX()),
                Math.round(binding.subjectInputLayout.getY()));
        reLoadFragment();

    }

    private void reLoadFragment() {

        binding.etSubjectName.setText("");
        binding.etUnitName.setText("");
        binding.etTopiName.setText("");
        binding.etDoubt.setText("");
        binding.tvUploadTitle.setText("Upload Files Here");
        binding.llSelectMedia.setText("Upload");

        if (list != null && list.size() > 0) {
            list.removeAll(list);
            pickedItemAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void changeTitle(String title) {

    }
}

