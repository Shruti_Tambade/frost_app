package com.frost.leap.components.topics;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentTopicsBinding;
import com.frost.leap.viewmodels.topics.TopicsViewModel;

import java.util.ArrayList;
import java.util.List;

import apprepos.subject.model.UnitModel;
import apprepos.topics.model.QuizModel;
import apprepos.topics.model.StudentContentModel;
import apprepos.topics.model.StudentTopicsModel;
import apprepos.topics.model.SubTopicModel;
import apprepos.topics.model.TopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;

/**
 * A simple {@link } subclass.
 * Use the {@link TopicsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TopicsFragment extends BaseFragment implements ISubTopicsAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "unit";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "topic";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String subjectId;

    private FragmentTopicsBinding binding;
    private TopicsViewModel viewModel;

    private List<StudentTopicsModel> studentTopicsModelList = new ArrayList<>();
    private UnitModel unitModel;
    private TopicsAdapter adapter;
    private TopicModel topicModel;
    private SubTopicModel lastWatchedSubTopicModel;
    private QuizModel lastWatchedQuizModel;
    private boolean isLastWatchedQuiz = false;
    private TopicsBroadcastReceiver broadcastReceiver;
    private int topicPosition = 0, subTopicPosition = 0;
    private String topicId, subTopicId;
    private int offset = 0;

    public TopicsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TopicsFragment newInstance() {
        TopicsFragment fragment = new TopicsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(TopicsViewModel.class);
        broadcastReceiver = new TopicsBroadcastReceiver();
        if (getArguments() != null) {
            unitModel = getArguments().getParcelable(ARG_PARAM1);
            subjectId = getArguments().getString(ARG_PARAM2);
            topicModel = getArguments().getParcelable(ARG_PARAM3);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_topics;
    }

    @Override
    public void setUp() {
        binding = (FragmentTopicsBinding) getBaseDataBinding();
        // setUpSnackBarView(binding.coordinatorLayout);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setNestedScrollingEnabled(false);

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        if (topicModel != null) {

            this.studentTopicsModelList = topicModel.getStudentTopicsModels();
            binding.recyclerView.setAdapter(adapter = new TopicsAdapter(activity(), this, RController.DONE, studentTopicsModelList));

            if (topicModel.getSubTopicModel() != null) {

                lastWatchedSubTopicModel = topicModel.getSubTopicModel();
                Logger.d("lastQuiz", String.valueOf(topicModel.getSubTopicModel()));
                for (int i = 0; i < studentTopicsModelList.size(); i++) {
                    if (lastWatchedSubTopicModel.getStudentTopicId().equals(studentTopicsModelList.get(i).getStudentTopicId())) {
                        int finalI = i;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.recyclerView.smoothScrollToPosition(finalI == 0 ? finalI : finalI + 1);
                            }
                        }, 500);
                        break;
                    }
                }


            } else {

                if (studentTopicsModelList != null && studentTopicsModelList.size() > 0)
                    if (studentTopicsModelList.get(0).getStudentContents() != null && studentTopicsModelList.get(0).getStudentContents().size() > 0) {

                        if (studentTopicsModelList.get(0).getStudentContents().get(0).getContentType().equals("SUBTOPIC")) {

                            isLastWatchedQuiz = false;
                            Logger.d("lastQuiz", String.valueOf(studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic()));
                            lastWatchedSubTopicModel = studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic();
                            studentTopicsModelList.get(0).getStudentContents().get(0).getSubTopic().setLastWatched(true);
                            adapter.notifyDataSetChanged();

                        } else if (studentTopicsModelList.get(0).getStudentContents().get(0).getContentType().equals("QUIZ")) {

                            isLastWatchedQuiz = true;
                            Logger.d("lastQuiz", String.valueOf(studentTopicsModelList.get(0).getStudentContents().get(0).getQuiz()));
                            lastWatchedQuizModel = studentTopicsModelList.get(0).getStudentContents().get(0).getQuiz();
                            studentTopicsModelList.get(0).getStudentContents().get(0).getQuiz().setLastWatched(true);
                            adapter.notifyDataSetChanged();

                        }
                    } else
                        showSnackBar("VideoConstants details not available", 2);
                else
                    showSnackBar("VideoConstants details not available", 2);

            }
        }

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onSubTopicClick(StudentTopicsModel topicModel, StudentContentModel studentContentModel, int position, int topicPosition) {
        this.topicPosition = topicPosition;
        this.subTopicPosition = position;
        this.topicId = topicModel.getTopicId();
        this.subTopicId = studentContentModel.getSubTopic().getSubTopicId();

        if (activity() instanceof MainActivity)
            if (studentContentModel.getContentType().equals("SUBTOPIC")) {
                ((MainActivity) activity()).playSubTopicVideo(studentContentModel.getSubTopic(), topicModel);
                if (isLastWatchedQuiz) {
                    if (lastWatchedQuizModel != null) {
                        lastWatchedQuizModel.setLastWatched(false);
                        studentContentModel.getSubTopic().setLastWatched(true);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    if (lastWatchedSubTopicModel != null) {
                        lastWatchedSubTopicModel.setLastWatched(false);
                        studentContentModel.getSubTopic().setLastWatched(true);
                        adapter.notifyDataSetChanged();
                    }
                }

            } else {
                ((MainActivity) activity()).openQuizPage(subjectId, unitModel.getStudentUnitId(), topicModel.getStudentTopicId(), studentContentModel.getQuiz());
                if (isLastWatchedQuiz) {
                    if (lastWatchedQuizModel != null) {
                        lastWatchedQuizModel.setLastWatched(false);
                        studentContentModel.getQuiz().setLastWatched(true);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    if (lastWatchedSubTopicModel != null) {
                        lastWatchedSubTopicModel.setLastWatched(false);
                        studentContentModel.getQuiz().setLastWatched(true);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
    }

    @Override
    public void downloadSubTopic(StudentTopicsModel topicModel, StudentContentModel studentContentModel, int position, int topicPosition) {

    }

    @Override
    public void lastWatchedVideoPosition(int position, SubTopicModel subTopic) {
        lastWatchedSubTopicModel = subTopic;
        isLastWatchedQuiz = false;

    }

    @Override
    public void lastWatchedQuizPosition(int position, QuizModel quiz) {
        lastWatchedQuizModel = quiz;
        isLastWatchedQuiz = true;

    }

    @Override
    public void ratingPage(RatingModel ratingModel) {

    }

    @Override
    public void applyRating(int ratingValue, RatingModel ratingModel, String topicName, String subTopicName) {

    }

    @Override
    public void showOnlyDownloads(boolean show) {

    }

    @Override
    public void reportPage() {

    }


    public class TopicsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("Update", false)) {
                if (topicId == null || adapter == null) {
                    return;
                }
                try {

                    if (topicId.equals(intent.getStringExtra("TopicId")) && subTopicId.equals(intent.getStringExtra("SubTopicId"))) {
                        if (topicPosition < studentTopicsModelList.size()) {
                            if (studentTopicsModelList.get(topicPosition).getCompletionPercentage() == 100) {
                                return;
                            }
                            studentTopicsModelList.get(topicPosition).setCompletionPercentage(intent.getDoubleExtra("TopicCompletionPercentage", studentTopicsModelList.get(topicPosition).getCompletionPercentage()));
                            if (subTopicPosition < studentTopicsModelList.get(topicPosition).getStudentContents().size()) {
                                if (studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition).getSubTopic().getVideoCompletionPercentage() == 100) {
                                    adapter.notifyDataSetChanged();
                                    return;
                                }
                                studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition).getSubTopic().setVideoCompletionPercentage(intent.getDoubleExtra(
                                        "SubTopicCompletionPercentage",
                                        studentTopicsModelList.get(topicPosition).getStudentContents().get(subTopicPosition).getSubTopic().getVideoCompletionPercentage()
                                        )
                                );
                            }
                            adapter.notifyItemChanged(topicPosition + 1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        registerReceiver();
    }


    private void registerReceiver() {

        try {
            if (broadcastReceiver == null)
                return;

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_UPDATE_TOPIC_PROGRESS);
            activity().registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (broadcastReceiver == null)
            return;

        activity().unregisterReceiver(broadcastReceiver);

    }


}