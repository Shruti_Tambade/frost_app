package com.frost.leap.components.profile.additional;

import android.net.Uri;

import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.viewmodels.models.Message;

import apprepos.user.model.AdditionalDetails;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 30-12-2019.
 * <p>
 * FROST
 */
public interface IAdditionalSettingsAdapter {

    void onAdditionalSettingsClick(MenuItem menuItem, int position);

    void changeProofFrontSide();

    void changeProofBackSide();

    void previewImage(Uri uri);

    void sendMessage(Message message);

    void gotoPrivacyPolicies();

    void updateAdditionalDetails(AdditionalDetails additionalDetails,int position);


}
