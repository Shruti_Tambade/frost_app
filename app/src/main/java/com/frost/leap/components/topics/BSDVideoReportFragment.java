package com.frost.leap.components.topics;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.frost.leap.R;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentBSDVideoReportBinding;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.services.FirebasePushService;
import com.frost.leap.services.HelperService;
import com.frost.leap.services.models.Report;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BSDVideoReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BSDVideoReportFragment extends BottomSheetDialogFragment implements IFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentBSDVideoReportBinding binding;
    private Report report;

    public BSDVideoReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BSDVideoReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BSDVideoReportFragment newInstance(Report param1, String param2) {
        BSDVideoReportFragment fragment = new BSDVideoReportFragment();
        Bundle args = new Bundle();
        fragment.report = param1;
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
        if (getArguments() != null) {
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_b_s_d_video_report, container, false);

        setUp();
        return binding.getRoot();
    }

    private void setUp() {

//        activity().overridePendingTransition(R.anim.slide_up, R.anim.slide_down);

        binding.llEditingIssue.setOnClickListener(v -> {

            startService(binding.tvEditingIssue.getText().toString());

        });
        binding.llVideoMissing.setOnClickListener(v -> {

            startService(binding.tvVideoMissing.getText().toString());

        });
        binding.llVoiceClear.setOnClickListener(v -> {

            startService(binding.tvVoiceClear.getText().toString());

        });
        binding.llVideoAbruptly.setOnClickListener(v -> {

            startService(binding.tvVideoAbruptly.getText().toString());

        });
        binding.llCorrelation.setOnClickListener(v -> {

            startService(binding.tvCorrelation.getText().toString());

        });
        binding.llIncorrectContent.setOnClickListener(v -> {

            startService(binding.tvIncorrectContent.getText().toString());

        });
        binding.llOtherIssue.setOnClickListener(v -> {

            final Dialog dialog = new Dialog(activity());
            dialog.setContentView(R.layout.dailog_other_issue);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().getAttributes().windowAnimations = R.style.CustomDialogStyle;
            dialog.setCancelable(false);
            dialog.show();

            EditText etReportReason = dialog.findViewById(R.id.etReportReason);
            TextView tvSubmitReport = dialog.findViewById(R.id.tvSubmitReport);
            TextView tvCancel = dialog.findViewById(R.id.tvCancel);

            tvCancel.setOnClickListener(view -> {
                dialog.dismiss();
            });

            tvSubmitReport.setOnClickListener(view -> {
                if (etReportReason.getText().toString() != null && !etReportReason.getText().toString().isEmpty()) {
                    startService(etReportReason.getText().toString());
                }
                dialog.dismiss();
            });

        });
    }

    private void startService(String issue) {

        report.setIssue(issue);

        try {

            Intent intent = new Intent(activity(), FirebasePushService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_REPORT_VIDEO);
            intent.putExtra("Report", report);
            activity().startService(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }

        dismiss();
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

    }

    @Override
    public Activity activity() {
        return getActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        decorateScreenUi();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        if (dialog == null)
            return super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(bottomSheetDialog -> {
            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            // Right here!
            BottomSheetBehavior.from(bottomSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        return dialog;
    }


    public void decorateScreenUi() {

        if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                View decorView = activity().getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                View decorView = activity().getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        }


    }

}
