package com.frost.leap.components.topics;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentVideoRatingBinding;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.services.HelperService;
import com.frost.leap.viewmodels.topics.TopicsViewModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.topics.model.reviewmodel.RatingQuestionModel;
import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link BSDVideoRatingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

@SuppressWarnings("ConstantConditions")
public class BSDVideoRatingFragment extends BottomSheetDialogFragment implements IFragment {
    public static final String TAG = "BSDVideoRatingFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RatingModel ratingModel;
    private TopicsViewModel viewModel;

    private FragmentVideoRatingBinding binding;
    private List<RatingQuestionModel> ratingQuestionModelList = new ArrayList<>();
    private boolean isConceptUnlike = false, isConceptlike = false, isConceptMostlike = false;
    private boolean isPresentationUnlike = false, isPresentationlike = false, isPresentationMostlike = false;
    private boolean isExplainedUnlike = false, isExplainedlike = false, isExplainedMostlike = false;

    private String clarity = "N/A", presenetation = "N/A", depth = "N/A";

    public BSDVideoRatingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BSDVideoRatingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BSDVideoRatingFragment newInstance(RatingModel param1, String param2) {
        BSDVideoRatingFragment fragment = new BSDVideoRatingFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
        if (getArguments() != null) {
            ratingModel = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_rating, container, false);

        setUp();
        return binding.getRoot();
    }

    private void setUp() {

        activity().overridePendingTransition(R.anim.slide_up, R.anim.slide_down);

        binding.ratingBar.setStepSize(1);

        if (ratingModel != null) {

            if (ratingModel.getRating() > 0)
                binding.ratingBar.setRating(ratingModel.getRating());

            if (ratingModel.getQuestions() != null && ratingModel.getQuestions().size() > 0) {

                for (int i = 0; i < ratingModel.getQuestions().size(); i++) {
                    if (binding.textView1.getText().toString().equals(ratingModel.getQuestions().get(i).getQuestion())) {
                        if (ratingModel.getQuestions().get(i).getAnswer() == 1) {
                            isConceptUnlike = true;
                            binding.tvUnlikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvUnlikely.setTextColor(activity().getResources().getColor(R.color.white));
                        } else if (ratingModel.getQuestions().get(i).getAnswer() == 2) {
                            isConceptlike = true;
                            binding.tvLikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvLikely.setTextColor(activity().getResources().getColor(R.color.white));
                        } else if (ratingModel.getQuestions().get(i).getAnswer() == 3) {
                            isConceptMostlike = true;
                            binding.tvMostLikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvMostLikely.setTextColor(activity().getResources().getColor(R.color.white));
                        }

                    } else if (binding.textView2.getText().toString().equals(ratingModel.getQuestions().get(i).getQuestion())) {
                        if (ratingModel.getQuestions().get(i).getAnswer() == 1) {
                            isPresentationUnlike = true;
                            binding.tvPresentationUnlikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvPresentationUnlikely.setTextColor(activity().getResources().getColor(R.color.white));
                        } else if (ratingModel.getQuestions().get(i).getAnswer() == 2) {
                            isPresentationlike = true;
                            binding.tvPresentationLikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvPresentationLikely.setTextColor(activity().getResources().getColor(R.color.white));
                        } else if (ratingModel.getQuestions().get(i).getAnswer() == 3) {
                            isPresentationMostlike = true;
                            binding.tvPresentationMostLikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvPresentationMostLikely.setTextColor(activity().getResources().getColor(R.color.white));
                        }

                    } else if (binding.textView3.getText().toString().equals(ratingModel.getQuestions().get(i).getQuestion())) {
                        if (ratingModel.getQuestions().get(i).getAnswer() == 1) {
                            isExplainedUnlike = true;
                            binding.tvConceptUnlikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvConceptUnlikely.setTextColor(activity().getResources().getColor(R.color.white));
                        } else if (ratingModel.getQuestions().get(i).getAnswer() == 2) {
                            isExplainedlike = true;
                            binding.tvConceptLikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvConceptLikely.setTextColor(activity().getResources().getColor(R.color.white));
                        } else if (ratingModel.getQuestions().get(i).getAnswer() == 3) {
                            isExplainedMostlike = true;
                            binding.tvConceptMostLikely.setBackgroundResource(R.drawable.red_border_radius_22);
                            binding.tvConceptMostLikely.setTextColor(activity().getResources().getColor(R.color.white));
                        }

                    }
                }
            }

            if (ratingModel.getFeedBackk() != null && !ratingModel.getFeedBackk().isEmpty()) {
                binding.etReview.setText(ratingModel.getFeedBackk());
            }
        }

        binding.tvUnlikely.setOnClickListener(v -> {

            binding.tvUnlikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvLikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvMostLikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isConceptUnlike = true;
            isConceptlike = false;
            isConceptMostlike = false;


            binding.tvUnlikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvMostLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvLikely.setOnClickListener(v -> {

            binding.tvLikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvUnlikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvMostLikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isConceptlike = true;
            isConceptMostlike = false;
            isConceptUnlike = false;

            binding.tvLikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvUnlikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvMostLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvMostLikely.setOnClickListener(v -> {

            binding.tvMostLikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvLikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvUnlikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isConceptlike = false;
            isConceptMostlike = true;
            isConceptUnlike = false;

            binding.tvMostLikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvUnlikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        //Presentation
        binding.tvPresentationUnlikely.setOnClickListener(v -> {

            binding.tvPresentationUnlikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvPresentationLikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvPresentationMostLikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isPresentationlike = false;
            isPresentationUnlike = true;
            isPresentationMostlike = false;


            binding.tvPresentationUnlikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvPresentationLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvPresentationMostLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvPresentationLikely.setOnClickListener(v -> {

            binding.tvPresentationLikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvPresentationUnlikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvPresentationMostLikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isPresentationlike = true;
            isPresentationUnlike = false;
            isPresentationMostlike = false;


            binding.tvPresentationLikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvPresentationUnlikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvPresentationMostLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvPresentationMostLikely.setOnClickListener(v -> {

            binding.tvPresentationMostLikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvPresentationLikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvPresentationUnlikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isPresentationlike = false;
            isPresentationUnlike = false;
            isPresentationMostlike = true;


            binding.tvPresentationMostLikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvPresentationLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvPresentationUnlikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        //Concept
        binding.tvConceptUnlikely.setOnClickListener(v -> {

            binding.tvConceptUnlikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvConceptLikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvConceptMostLikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isExplainedlike = false;
            isExplainedMostlike = false;
            isExplainedUnlike = true;


            binding.tvConceptUnlikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvConceptLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvConceptMostLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvConceptLikely.setOnClickListener(v -> {

            binding.tvConceptLikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvConceptUnlikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvConceptMostLikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isExplainedlike = true;
            isExplainedMostlike = false;
            isExplainedUnlike = false;


            binding.tvConceptLikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvConceptUnlikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvConceptMostLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvConceptMostLikely.setOnClickListener(v -> {

            binding.tvConceptMostLikely.setBackgroundResource(R.drawable.red_border_radius_22);
            binding.tvConceptLikely.setBackgroundResource(R.drawable.border_corner_radius_30);
            binding.tvConceptUnlikely.setBackgroundResource(R.drawable.border_corner_radius_30);

            isExplainedlike = false;
            isExplainedMostlike = true;
            isExplainedUnlike = false;


            binding.tvConceptMostLikely.setTextColor(activity().getResources().getColor(R.color.white));
            binding.tvConceptLikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
            binding.tvConceptUnlikely.setTextColor(activity().getResources().getColor(R.color.normal_text_color));
        });

        binding.tvSubmit.setOnClickListener(v -> {

            if (binding.ratingBar.getRating() > 0) {

                //removing the previous question and add new
                if (ratingModel != null && ratingModel.getQuestions() != null && ratingModel.getQuestions().size() > 0)
                    ratingModel.getQuestions().removeAll(ratingModel.getQuestions());

//                ratingModel = new RatingModel();

                ratingModel.setRating((int) binding.ratingBar.getRating());
                if (isConceptUnlike || isConceptlike || isConceptMostlike) {
                    RatingQuestionModel ratingQuestionModel = new RatingQuestionModel();
                    if (isConceptUnlike) {
                        ratingQuestionModel.setQuestion(binding.textView1.getText().toString());
                        ratingQuestionModel.setAnswer(1);
                        clarity = "Unclear";
                    } else if (isConceptlike) {
                        ratingQuestionModel.setQuestion(binding.textView1.getText().toString());
                        ratingQuestionModel.setAnswer(2);
                        clarity = "Clear";
                    } else if (isConceptMostlike) {
                        ratingQuestionModel.setQuestion(binding.textView1.getText().toString());
                        ratingQuestionModel.setAnswer(3);
                        clarity = "Very Clear";
                    }

                    ratingQuestionModelList.add(ratingQuestionModel);
                }

                if (isPresentationUnlike || isPresentationlike || isPresentationMostlike) {
                    RatingQuestionModel ratingQuestionMode2 = new RatingQuestionModel();
                    if (isPresentationUnlike) {
                        ratingQuestionMode2.setQuestion(binding.textView2.getText().toString());
                        ratingQuestionMode2.setAnswer(1);
                        presenetation = "Poor";
                    } else if (isPresentationlike) {
                        ratingQuestionMode2.setQuestion(binding.textView2.getText().toString());
                        ratingQuestionMode2.setAnswer(2);
                        presenetation = "Good";
                    } else if (isPresentationMostlike) {
                        ratingQuestionMode2.setQuestion(binding.textView2.getText().toString());
                        ratingQuestionMode2.setAnswer(3);
                        presenetation = "Very Good";
                    }

                    ratingQuestionModelList.add(ratingQuestionMode2);
                }

                if (isExplainedUnlike || isExplainedlike || isExplainedMostlike) {
                    RatingQuestionModel ratingQuestionModel3 = new RatingQuestionModel();
                    if (isExplainedUnlike) {
                        ratingQuestionModel3.setQuestion(binding.textView3.getText().toString());
                        ratingQuestionModel3.setAnswer(1);
                        depth = "No";
                    } else if (isExplainedlike) {
                        ratingQuestionModel3.setQuestion(binding.textView3.getText().toString());
                        ratingQuestionModel3.setAnswer(2);
                        depth = "Slightly";
                    } else if (isExplainedMostlike) {
                        ratingQuestionModel3.setQuestion(binding.textView3.getText().toString());
                        ratingQuestionModel3.setAnswer(3);
                        depth = "Completelty";
                    }
                    ratingQuestionModelList.add(ratingQuestionModel3);
                }

                if (!TextUtils.isEmpty(binding.etReview.getText().toString())) {
                    ratingModel.setFeedBackk(binding.etReview.getText().toString());
                }


                ratingModel.setQuestions(ratingQuestionModelList);

                //Post Rating Model in Background Service
                Intent intent = new Intent(activity(), HelperService.class);
                intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_RATING);
                intent.putExtra("RATING", ratingModel);
                activity().startService(intent);

                Mixpanel.addReview(ratingModel.getCatalogueName(), ratingModel.getSubjectName(), ratingModel.getUnitName(), ratingModel.getTopicName(), ratingModel.getSubTopicName(), (int) binding.ratingBar.getRating(),
                        clarity, presenetation, depth);

                //Show Dailog Box
                submitRating();

                //Update Rating Model in Topics Adapter
                ((MainActivity) activity()).updateRating(ratingModel);
                dismiss();

            } else {
//                Toast.makeText(activity(), "Please give rating for video", Toast.LENGTH_SHORT).show();
                Utility.showToast(activity(),"Please give rating for video",false);
            }

        });
    }

    private void submitRating() {

        final Dialog dialog = new Dialog(activity());
        dialog.setContentView(R.layout.dailog_review_submit);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.CustomDialogStyle;
        dialog.setCancelable(true);
        dialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        }, 2000);
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        if (dialog == null)
            return super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(bottomSheetDialog -> {
            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            // Right here!
            BottomSheetBehavior.from(bottomSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        return dialog;
    }
//
//
//    @Override
//    public int getTheme() {
//        return R.style.BaseBottomSheetDialog;
//    }
}
