package com.frost.leap.components.videoplayer;

import supporters.constants.PlayBackSpeed;

/**
 * Created by R!ZWAN SHEIKH on 2019-09-17.
 * <p>
 * Frost
 */
public interface ISpeedController {
    void setPlaybackSpeed(PlayBackSpeed playbackSpeed);
    void updateVideoFocus();
}
