package com.frost.leap.components.profile.additional;


import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentImageViewBinding;
import com.frost.leap.interfaces.IFragment;


import supporters.constants.Constants;
import supporters.utils.Utility;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ImageViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageViewFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters

    private Uri uri;
    private FragmentImageViewBinding fragmentImageViewBinding;


    public ImageViewFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ImageViewFragment newInstance(Uri uri) {
        ImageViewFragment fragment = new ImageViewFragment();
        Bundle args = new Bundle();
        args.putParcelable("Uri", uri);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            uri = getArguments().getParcelable("Uri");
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_image_view;
    }


    @Override
    public void setUp() {
        fragmentImageViewBinding = (FragmentImageViewBinding) getBaseDataBinding();
        setUpSnackBarView(fragmentImageViewBinding.coordinatorLayout);

        if (uri == null) return;

        ProgressBarDrawable progressBarDrawable = new ProgressBarDrawable();
        progressBarDrawable.setColor(ContextCompat.getColor(activity(), R.color.new_primary_blue));
        progressBarDrawable.setRadius(10);
        progressBarDrawable.setPadding(Utility.dpSize(activity(), 15));
        fragmentImageViewBinding.imgContent.getHierarchy().setProgressBarImage(progressBarDrawable);


        fragmentImageViewBinding.imgContent.setImageURI(uri == null ? Uri.parse(Constants.PLACE_HOLDER_IMAGE) : uri);

    }


    @Override
    public void changeTitle(String title) {

    }
}
