package com.frost.leap.components.topics;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemTopicBinding;
import com.frost.leap.databinding.ItemTopicHeaderBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.topics.model.StudentTopicsModel;
import apprepos.topics.model.SubTopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;
import supporters.constants.RController;
import supporters.customviews.views.LinearLayoutManagerWithSmoothScroller;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 20-08-2019.
 * <p>
 * Frost
 */
public class TopicsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StudentTopicsModel> list;
    private Activity activity;
    private RController rController;
    private ISubTopicsAdapter iSubTopicsAdapter;
    private SubTopicModel subTopicModel;

    private String subCatContent;
    private RatingModel ratingModel;
    private boolean isRattingBarClicked = true;
    private boolean isSelect = false;
    private boolean showOnlyDownloads = false;

    private int firsttime = 0;


    public TopicsAdapter(Activity activity, ISubTopicsAdapter iSubTopicsAdapter, RController rController, List<StudentTopicsModel> list) {
        this.list = list;
        this.activity = activity;
        this.rController = rController;
        this.iSubTopicsAdapter = iSubTopicsAdapter;
        firsttime = 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skelton_subject, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else if (viewType == 0) {
            return new TopivHeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_topic_header, viewGroup, false));
        } else
            return new TopicViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_topic, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof TopicViewHolder) {

            TopicViewHolder topicViewHolder = (TopicViewHolder) viewHolder;
            topicViewHolder.bindData(list.get(position - 1), position - 1);

        } else if (viewHolder instanceof TopivHeaderViewHolder) {
            ((TopivHeaderViewHolder) viewHolder).bindData(position, subTopicModel);

        }
    }


    @Override
    public int getItemCount() {
        return rController == RController.LOADING
                ? 15
                : list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void changeHeaderTitle(SubTopicModel subTopicModel) {
        this.subTopicModel = subTopicModel;
        try {
            activity.runOnUiThread(() -> {
                notifyItemChanged(0);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateList(List<StudentTopicsModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setSubCatContent(String subCatContent) {
        this.subCatContent = subCatContent;
        try {
            activity.runOnUiThread(() -> {
                notifyItemChanged(0);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateRating(RatingModel ratingModelData) {
        this.ratingModel = ratingModelData;
        isRattingBarClicked = true;
        try {
            activity.runOnUiThread(() -> {
                notifyItemChanged(0);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {
        private ItemTopicBinding binding;

        public TopicViewHolder(ItemTopicBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(activity));
            binding.recyclerView.setHasFixedSize(true);
        }

        public void bindData(StudentTopicsModel studentTopicsModel, int position) {

//            binding.constraintLayout.setVisibility(position == 0 ? View.VISIBLE : View.GONE);

            ScaleAnimation expandAnimation = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.0f);
            expandAnimation.setDuration(400);

            ScaleAnimation collapsAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.0f);
            collapsAnimation.setDuration(400);


            Logger.d("TOPIC PROGRESS UPDATE", "" + studentTopicsModel.getCompletionPercentage());
            int percentageValue = studentTopicsModel.getCompletionPercentage().intValue();

            if ((studentTopicsModel.getCompletionPercentage() > 0 && studentTopicsModel.getCompletionPercentage() < 100))
                binding.progressBar.setProgress(percentageValue + 1);
            else
                binding.progressBar.setProgress(percentageValue);

            binding.progressBar.setVisibility(percentageValue >= 99 && percentageValue <= 100 ? View.GONE : View.VISIBLE);
            binding.ivDone.setVisibility(percentageValue >= 99 && percentageValue <= 100 ? View.VISIBLE : View.GONE);

            binding.tvSubjectName.setText(studentTopicsModel.getTopicName());
            binding.recyclerView.setItemAnimator(null);
            binding.recyclerView.setAdapter(new SubTopicAdapter(position, studentTopicsModel, iSubTopicsAdapter, studentTopicsModel.getStudentContents(), activity,
                    studentTopicsModel.getLastWatched()));

//            binding.tvUnitsCount.setText("" + studentTopicsModel.getStudentContents().size() + " Subtopics");
            binding.tvUnitsCount.setText(Utility.calculateTimeDuration(studentTopicsModel.getTotalDurationInSce()));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, position == list.size() - 1 ? Utility.dpSize(activity, 120) : 0);
            binding.getRoot().setLayoutParams(params);

        }
    }

    private class TopivHeaderViewHolder extends RecyclerView.ViewHolder {

        private ItemTopicHeaderBinding binding;

        public TopivHeaderViewHolder(ItemTopicHeaderBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }

        @SuppressLint("ClickableViewAccessibility")
        void bindData(int position, SubTopicModel subTopicModel) {

            binding.ratingBar.setStepSize(1);
            binding.offlineSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {

                if (!Utility.isNetworkAvailable(activity)) {
                    showOnlyDownloads = true;
                    binding.offlineSwitch.setChecked(true);
                    iSubTopicsAdapter.showOnlyDownloads(true);
                    return;
                }

                showOnlyDownloads = isChecked;
                iSubTopicsAdapter.showOnlyDownloads(isChecked);
            });

            if (!Utility.isNetworkAvailable(activity)) {
                binding.offlineSwitch.setChecked(true);
            }

            if (list != null && list.size() > 0) {
                binding.llErrorMessage.setVisibility(View.GONE);
            } else {
                binding.llErrorMessage.setVisibility(View.VISIBLE);

                if (!Utility.isNetworkAvailable(activity))
                    binding.tvError.setText(activity.getResources().getString(R.string.empty_offline_content));
                else
                    binding.tvError.setText(activity.getResources().getString(R.string.empty_offline_content_with_Internet));
            }

            try {
                if (subTopicModel != null) {

                    if (subCatContent != null) {
                        binding.tvSubCategoryName.setText(subCatContent);
                    } else {
                        binding.tvSubCategoryName.setText("");
                    }

                    if (ratingModel != null) {
                        binding.ratingBar.setRating(ratingModel.getRating());
                    } else {
                        binding.ratingBar.setRating(0);
                    }

//                    binding.ivDownArrow.setVisibility(View.VISIBLE);
                    binding.ratingBar.setVisibility(View.VISIBLE);
                    binding.tvWriteReview.setVisibility(View.VISIBLE);
                    binding.tvSubTopicName.setText(subTopicModel.getSubTopicName());
                    binding.tvCategory.setText(subTopicModel.getDescription());

                } else {
                    binding.tvSubTopicName.setText("Quiz");
//                    binding.ivDownArrow.setVisibility(View.GONE);
                    binding.ratingBar.setVisibility(View.GONE);
                    binding.tvWriteReview.setVisibility(View.GONE);
                }

//                binding.titleLayout.setOnClickListener(v -> {
//
//
//                    if (isSelect) {
//                        isSelect = false;
//                        binding.ivDownArrow.animate().rotation(0);
////                        TransitionManager.beginDelayedTransition(binding.ratingLayout, transition);
////                        binding.ratingLayout.setVisibility(View.GONE);
//                        Utility.collapse(binding.ratingLayout);
//                    } else {
//                        isSelect = true;
//                        binding.ivDownArrow.animate().rotation(180);
////                        TransitionManager.beginDelayedTransition(binding.ratingLayout, transition);
//                        Utility.expand(binding.ratingLayout);
////                        binding.ratingLayout.setVisibility(View.VISIBLE);
//                    }
//                });

                binding.tvWriteReview.setOnClickListener(v -> {
                    iSubTopicsAdapter.reportPage();
                });

                binding.ratingBar.setOnRatingBarChangeListener(null);
                binding.ratingBar.setOnTouchListener((v, event) -> {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        iSubTopicsAdapter.ratingPage(ratingModel);
                    }
                    return true;
                });

//                binding.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
//                    @Override
//                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
//
//                        if (firsttime == 0) {
//                            firsttime++;
//                            iSubTopicsAdapter.applyRating((int) v, ratingModel, subTopicModel.getTopicName(), subTopicModel.getSubTopicName());
//                            isRattingBarClicked = false;
//                            return;
//                        } else {
//                            if (v > 0) {
//                                iSubTopicsAdapter.applyRating((int) v, ratingModel, subTopicModel.getTopicName(), subTopicModel.getSubTopicName());
//                                isRattingBarClicked = false;
//
//                                iSubTopicsAdapter.ratingPage(ratingModel);
//                            }
//                        }
//                    }
//                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
