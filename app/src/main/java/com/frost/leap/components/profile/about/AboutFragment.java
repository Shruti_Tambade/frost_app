package com.frost.leap.components.profile.about;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.frost.leap.BuildConfig;
import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentAboutBinding;

import supporters.constants.Constants;

/**
 * A simple {@link } subclass.
 * Use the {@link AboutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentAboutBinding binding;

    public AboutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AboutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AboutFragment newInstance(String param1, String param2) {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_about;
    }

    @Override
    public void setUp() {

        binding = (FragmentAboutBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.tvVersion.setText(BuildConfig.VERSION_NAME);


        binding.constraintLayout4.setOnClickListener(v -> {
            gotoTerms();
        });

        binding.constraintLayout5.setOnClickListener(v -> {
            gotoPolicies();
        });

    }

    private void gotoPolicies() {
//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, "Privacy Policy");
//        intent.putExtra("Url", Constants.UserConstants.PRIVACY_POLICIES_URL);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }

    private void gotoTerms() {
//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, "Terms of Use");
//        intent.putExtra("Url", Constants.UserConstants.TERMS_AND_USES_URL);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.TERMS_AND_USES_URL), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.TERMS_AND_USES_URL), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UserConstants.TERMS_AND_USES_URL));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }

    @Override
    public void changeTitle(String title) {

    }
}
