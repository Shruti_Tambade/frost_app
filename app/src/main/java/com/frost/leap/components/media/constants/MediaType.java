package com.frost.leap.components.media.constants;


public enum MediaType
{
    IMAGE,
    VIDEO,
    AUDIO,
    FILE,
    BOTH_IMAGE_AND_VIDEO
}
