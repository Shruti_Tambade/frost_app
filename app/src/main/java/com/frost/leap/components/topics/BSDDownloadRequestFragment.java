package com.frost.leap.components.topics;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.frost.leap.R;
import com.frost.leap.databinding.BsdRequestDownloadBinding;
import com.frost.leap.interfaces.IFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.DecimalFormat;

import apprepos.user.UserRepositoryManager;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 31-01-2020.
 * <p>
 * FROST
 */
public class BSDDownloadRequestFragment extends BottomSheetDialogFragment implements IFragment {

    private BsdRequestDownloadBinding binding;
    private IBSDDownloadRequestFragment ibsdDownloadRequestFragment;
    private UserRepositoryManager userRepositoryManager;


    public static BSDDownloadRequestFragment newInstance(IBSDDownloadRequestFragment ibsdDownloadRequestFragment) {
        BSDDownloadRequestFragment fragment = new BSDDownloadRequestFragment();
        fragment.ibsdDownloadRequestFragment = ibsdDownloadRequestFragment;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.bsd_request_download, container, false);

        setUp();


        return binding.getRoot();

    }

    private void setUp() {

        userRepositoryManager = UserRepositoryManager.getInstance();
        binding.tvDetails.setText("Available Space: " + new DecimalFormat("#.#").format(Utility.getDeviceMemory(activity(), userRepositoryManager.getVideoDownloadLocation())) + " GB");
        binding.tvNote.setText(Html.fromHtml(getString(R.string.download_note)));

        if (userRepositoryManager.getDownloadResolution() != 0) {
            if (userRepositoryManager.getDownloadResolution() == 1080) {
                binding.rb1080.setChecked(true);
            } else if (userRepositoryManager.getDownloadResolution() == 720) {
                binding.rb720.setChecked(true);
            } else if (userRepositoryManager.getDownloadResolution() == 540) {
                binding.rb540.setChecked(true);
            } else if (userRepositoryManager.getDownloadResolution() == 360) {
                binding.rb360.setChecked(true);
            } else if (userRepositoryManager.getDownloadResolution() <= 270) {
                binding.rb270.setChecked(true);
            }

        } else {
            binding.rb1080.setChecked(true);
        }

        binding.rb1080.setTypeface(Utility.getTypeface(5, activity()));
        binding.rb720.setTypeface(Utility.getTypeface(5, activity()));
        binding.rb540.setTypeface(Utility.getTypeface(5, activity()));
        binding.rb360.setTypeface(Utility.getTypeface(5, activity()));
        binding.rb270.setTypeface(Utility.getTypeface(5, activity()));


        String data = "1080p Resolution.";
        SpannableString text = new SpannableString("Best\n\n" + data);
        text.setSpan(new RelativeSizeSpan(1.4f), 0, text.toString().lastIndexOf(data) - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.3f), text.toString().indexOf(data) - 1, text.toString().indexOf(data), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.other_text_color)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.rb1080.setText(text);

        data = "720p Resolution.";
        text = new SpannableString("Better\n\n" + data);
        text.setSpan(new RelativeSizeSpan(1.4f), 0, text.toString().lastIndexOf(data) - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.3f), text.toString().indexOf(data) - 1, text.toString().indexOf(data), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.other_text_color)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.rb720.setText(text);

        data = "540p Resolution.";
        text = new SpannableString("Good\n\n" + data);
        text.setSpan(new RelativeSizeSpan(1.4f), 0, text.toString().lastIndexOf(data) - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.3f), text.toString().indexOf(data) - 1, text.toString().indexOf(data), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.other_text_color)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.rb540.setText(text);


        data = "360p Resolution.";
        text = new SpannableString("Average\n\n" + data);
        text.setSpan(new RelativeSizeSpan(1.4f), 0, text.toString().lastIndexOf(data) - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.3f), text.toString().indexOf(data) - 1, text.toString().indexOf(data), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.other_text_color)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.rb360.setText(text);


        data = "270p Resolution.";
        text = new SpannableString("Data Saver\n\n" + data);
        text.setSpan(new RelativeSizeSpan(1.4f), 0, text.toString().lastIndexOf(data) - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.3f), text.toString().indexOf(data) - 1, text.toString().indexOf(data), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.other_text_color)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.rb270.setText(text);


        data = "You can change this setting at any time in the settings menu.";
        text = new SpannableString("Make this my default setting\n\n" + data);
        text.setSpan(new RelativeSizeSpan(1.4f), 0, text.toString().lastIndexOf(data) - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(0.3f), text.toString().indexOf(data) - 1, text.toString().indexOf(data), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.other_text_color)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.defaultCheckBox.setText(text);


        binding.rb1080.setOnClickListener(v -> {
            binding.rb1080.setChecked(true);
            binding.rb720.setChecked(false);
            binding.rb540.setChecked(false);
            binding.rb360.setChecked(false);
            binding.rb270.setChecked(false);

            userRepositoryManager.setDownloadResolution(1080);

        });

        binding.rb720.setOnClickListener(v -> {
            binding.rb1080.setChecked(false);
            binding.rb720.setChecked(true);
            binding.rb540.setChecked(false);
            binding.rb360.setChecked(false);
            binding.rb270.setChecked(false);

            userRepositoryManager.setDownloadResolution(720);
        });

        binding.rb540.setOnClickListener(v ->
        {
            binding.rb1080.setChecked(false);
            binding.rb720.setChecked(false);
            binding.rb540.setChecked(true);
            binding.rb360.setChecked(false);
            binding.rb270.setChecked(false);

            userRepositoryManager.setDownloadResolution(540);
        });

        binding.rb360.setOnClickListener(v ->

        {
            binding.rb1080.setChecked(false);
            binding.rb720.setChecked(false);
            binding.rb540.setChecked(false);
            binding.rb360.setChecked(true);
            binding.rb270.setChecked(false);

            userRepositoryManager.setDownloadResolution(360);
        });

        binding.rb270.setOnClickListener(v ->
        {

            binding.rb1080.setChecked(false);
            binding.rb720.setChecked(false);
            binding.rb540.setChecked(false);
            binding.rb360.setChecked(false);
            binding.rb270.setChecked(true);

            userRepositoryManager.setDownloadResolution(270);
        });


        binding.tvYes.setOnClickListener(v ->
        {
            this.dismiss();
            ibsdDownloadRequestFragment.download(
                    binding.rb1080.isChecked() ? 1080 :
                            (binding.rb720.isChecked() ? 720 :
                                    (binding.rb540.isChecked() ? 540 :
                                            (binding.rb360.isChecked() ? 360 : 270))
                            )

            );

            userRepositoryManager.setAskMeEveryTime(!binding.defaultCheckBox.isChecked());

//            if (binding.defaultCheckBox.isChecked()) {
//                Utility.showToast(activity(), "You can change default download settings in Application Settings", false);
//            }
        });

        binding.llDownloadLayout.setOnClickListener(v ->
        {
            this.dismiss();
            ibsdDownloadRequestFragment.download(
                    binding.rb1080.isChecked() ? 1080 :
                            (binding.rb720.isChecked() ? 720 :
                                    (binding.rb540.isChecked() ? 540 :
                                            (binding.rb360.isChecked() ? 360 : 270))
                            )

            );

            userRepositoryManager.setAskMeEveryTime(!binding.defaultCheckBox.isChecked());

//            if (binding.defaultCheckBox.isChecked()) {
//                Utility.showToast(activity(), "You can change default download settings in Application Settings", false);
//            }
        });

        binding.tvNo.setOnClickListener(v ->

        {
            this.dismiss();
        });


    }


    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        if (dialog == null)
            return super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(bottomSheetDialog -> {
            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            // Right here!
            BottomSheetBehavior.from(bottomSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        return dialog;
    }


}
