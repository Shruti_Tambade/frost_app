package com.frost.leap.components.media.interfaces;


import com.frost.leap.components.media.models.GalleryFile;

public interface ICustomMediaAdapter {

    public void click(GalleryFile galleryFile, int position);

    public void longPressed(GalleryFile galleryFile, int position);
}
