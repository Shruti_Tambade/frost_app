package com.frost.leap.components.profile;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.databinding.ItemMenuProfileSettingBinding;
import com.frost.leap.databinding.ItemProfileHeaderBinding;

import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 16-04-2020.
 * Frost
 */
public class HelperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private List<MenuItem> list;
    private IProfileAdapter iProfileAdapter;
    private Context context;

    public HelperAdapter(Context context, List<MenuItem> list, IProfileAdapter iProfileAdapter) {
        this.list = list;
        this.context = context;
        this.iProfileAdapter = iProfileAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        return new MenuItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_menu_profile_setting, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof MenuItemViewHolder) {
            MenuItemViewHolder menuItemViewHolder = (MenuItemViewHolder) viewHolder;
            menuItemViewHolder.bindData(list.get(i), i);
            menuItemViewHolder.binding.llFooter.setOnClickListener(v -> {
                iProfileAdapter.onMenuItemClick(list.get(i), i);
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MenuItemViewHolder extends RecyclerView.ViewHolder {
        private ItemMenuProfileSettingBinding binding;

        public MenuItemViewHolder(ItemMenuProfileSettingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(MenuItem menuItem, int i) {
            binding.llHeader.setVisibility(menuItem.getHeader() == null ? View.GONE : View.VISIBLE);
            binding.tvHeader.setText(menuItem.getHeader() == null ? "" : menuItem.getHeader());
            binding.tvTitle.setText(menuItem.getTitle());
            binding.tvDescription.setText(menuItem.getDescription());

            binding.tvHeader.setTextColor(Color.parseColor("#80000000"));

            binding.tvTitle.setTextColor(Utility.getColorsFromAttrs(context, R.attr.text_color));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, Utility.dpSize(context, i == list.size() ? 120 : 0));
            binding.getRoot().setLayoutParams(params);


        }
    }
}
