package com.frost.leap.components.store_overview;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentStoreOverviewBinding;
import com.frost.leap.services.LeadSquaredIntentService;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.store.StoreViewModel;
import com.frost.leap.viewpager.CustomViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.store.model.CatalogueOverviewModel;
import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StoreOverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreOverviewFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "catalogue";
    private static final String ARG_PARAM2 = "CATALOGUE_OERVIEW";

    // TODO: Rename and change types of parameters

    private FragmentStoreOverviewBinding binding;
    private CatalogueModel catalogueModel;
    private StoreViewModel viewModel;
    private Dialog dialog;
    private CustomViewPagerAdapter adapter;
    public CatalogueOverviewModel catalogueOverviewModel;

    public StoreOverviewFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static StoreOverviewFragment newInstance() {
        StoreOverviewFragment fragment = new StoreOverviewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(StoreViewModel.class);
        if (viewModel.getStoreCatalogue() != null) {
            catalogueModel = viewModel.getStoreCatalogue();
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_store_overview;
    }

    @Override
    public void setUp() {

        if (catalogueModel == null)
            return;

        binding = (FragmentStoreOverviewBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case StoreViewModel.NetworkTags.SUBJECTS_LIST:
                    updateUIForSubjectList(networkCall);
                    break;
            }
        });

        viewModel.updateSubjectsData().observe(this, catalogueOverviewModel -> {
            this.catalogueOverviewModel = catalogueOverviewModel;
            binding.progressBar.setVisibility(View.GONE);
            setUpTabLayout();
            //Catalgoue Status
            viewModel.fetchCatalogueSubscriptionStatus(catalogueModel.getCatalogueId(), catalogueModel.getStudentId());
        });

        viewModel.getSubscription().observe(this, subscriptionModel -> {
            for (Fragment childFragment : getChildFragmentManager().getFragments()) {
                if (childFragment instanceof OverviewFragment) {
                    OverviewFragment overviewFragment = (OverviewFragment) childFragment;
                    overviewFragment.updateSubscriptionStatus(subscriptionModel);
                    break;
                }
            }

        });

        //Get Catalogue Info and subjects
        viewModel.fetchSubjects(catalogueModel.getCatalogueId());

        //Catalgoue Status
        viewModel.fetchCatalogueSubscriptionStatus(catalogueModel.getCatalogueId(), catalogueModel.getStudentId());


        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        break;
                    case 1:
                        Mixpanel.clickCurriculum(catalogueOverviewModel.getCatalogueName(), true);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setUpTabLayout() {

        binding.viewPager.setOffscreenPageLimit(Constants.CATALOGUE_OVERVIEW.size());
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.setAdapter(adapter = new CustomViewPagerAdapter(getChildFragmentManager(), Constants.CATALOGUE_OVERVIEW, 4, null));
        Utility.changeTabsFont(binding.tabLayout);
        binding.viewPager.setPagingEnabled(false);

    }

    private void updateUIForSubjectList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case IN_PROCESS:
                binding.progressBar.setVisibility(View.VISIBLE);
                break;

            case SUCCESS:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case DONE:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case NO_DATA:
                break;

            case ERROR:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case UNAUTHORIZED:
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    public void changeTab(int tabPosition) {

        binding.viewPager.setCurrentItem(tabPosition, false);
    }

    public void updateOrderStatus(String paymentId, boolean isPaymentSuccess) {
        if (getChildFragmentManager().getFragments().get(0) instanceof OverviewFragment) {
            OverviewFragment overviewFragment = (OverviewFragment) getChildFragmentManager().getFragments().get(0);
            if (paymentId != null) {
                overviewFragment.updateOrderStatus(paymentId);

                Intent intent = new Intent(activity(), LeadSquaredIntentService.class);
                intent.putExtra(Constants.ACTION_KEY, AppController.GET_LEAD_BY_EMAILID);
                activity().startService(intent);
            }
            overviewFragment.updateMixPanel(isPaymentSuccess);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


}



