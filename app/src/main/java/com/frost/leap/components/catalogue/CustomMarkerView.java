package com.frost.leap.components.catalogue;

import android.content.Context;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 06-08-2019.
 * <p>
 * Frost
 */
public class CustomMarkerView extends MarkerView {


    public CustomMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight()/2);
    }
}

