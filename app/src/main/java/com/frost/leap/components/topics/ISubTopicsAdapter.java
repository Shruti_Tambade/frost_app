package com.frost.leap.components.topics;

import apprepos.topics.model.QuizModel;
import apprepos.topics.model.StudentContentModel;
import apprepos.topics.model.StudentTopicsModel;
import apprepos.topics.model.SubTopicModel;
import apprepos.topics.model.reviewmodel.RatingModel;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 06-09-2019.
 * <p>
 * Frost
 */
public interface ISubTopicsAdapter {

    void onSubTopicClick(StudentTopicsModel topicModel, StudentContentModel studentContentModel, int position, int topicPosition);

    void downloadSubTopic(StudentTopicsModel topicModel, StudentContentModel studentContentModel, int position, int topicPosition);

    void lastWatchedVideoPosition(int position, SubTopicModel subTopic);

    void lastWatchedQuizPosition(int position, QuizModel quiz);

    void ratingPage(RatingModel ratingModel);

    void applyRating(int ratingValue, RatingModel ratingModel, String topicName, String subTopicName);

    void showOnlyDownloads(boolean show);

    void reportPage();
}
