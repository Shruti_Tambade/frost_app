package com.frost.leap.components.catalogue;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.updates.model.UpdateModel;

/**
 * Created by Chenna Rao on 10/15/2019.
 */
public interface ICatalogueAdapter {

    void openCatalogueOverview(CatalogueModel catalogueModel, int position);

    void openStoreOverview(CatalogueModel catalogueModel, int position);

    void changeMotion(boolean isMotion);

    void gotoStore();
}
