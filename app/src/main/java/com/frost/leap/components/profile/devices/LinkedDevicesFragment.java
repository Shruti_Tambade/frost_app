package com.frost.leap.components.profile.devices;


import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentLinkedDevicesBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.profile.LinkedDeviceViewModel;

import java.util.Arrays;

import supporters.constants.Constants;
import supporters.constants.RController;

/**
 * A simple {@link } subclass.
 * Use the {@link LinkedDevicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LinkedDevicesFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentLinkedDevicesBinding binding;
    private LinkedDeviceViewModel viewModel;

    public LinkedDevicesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LinkedDevicesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LinkedDevicesFragment newInstance(String param1, String param2) {
        LinkedDevicesFragment fragment = new LinkedDevicesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(LinkedDeviceViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_linked_devices;
    }


    @Override
    public void setUp() {
        binding = (FragmentLinkedDevicesBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchLinkedDevices();
        });


        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case LinkedDeviceViewModel.NetworkTags.LINKED_DEVICES:
                    updateUiForLinkedDevices(networkCall);
                    break;
            }
        });

        viewModel.updateLinkedDevices().observe(this, linkedDevices -> {
            binding.recyclerView.setAdapter(new LinkedDevicesAdapter(activity(), linkedDevices, RController.DONE));
        });


        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });


        viewModel.fetchLinkedDevices();


    }

    private void updateUiForLinkedDevices(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;

            case IN_PROCESS:
                binding.recyclerView.setAdapter(new LinkedDevicesAdapter(activity(), Arrays.asList(null, null), RController.LOADING));
                break;

            case NO_DATA:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.NO_DATA_AVAILABLE, R.drawable.no_data_available, 1));
                break;

            case FAIL:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }
}
