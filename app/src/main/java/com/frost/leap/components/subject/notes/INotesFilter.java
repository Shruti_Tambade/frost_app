package com.frost.leap.components.subject.notes;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 07-01-2020.
 * <p>
 * FROST
 */
public interface INotesFilter {

    void clearFilters();

    void applyFilters(String subjectId, String unitId, String topicId, String value);
}
