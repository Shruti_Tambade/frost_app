package com.frost.leap.components.profile.app_settings;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.activities.BaseActivity;
import com.frost.leap.activities.SplashActivity;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentAppSettingsBinding;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.services.video.VideoDownloadService;
import com.frost.leap.viewmodels.helper.HelperViewModel;

import apprepos.user.UserRepositoryManager;
import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Utility;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AppSettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AppSettingsFragment extends BaseFragment implements ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentAppSettingsBinding binding;
    private HelperViewModel viewModel;
    private UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();
    ;

    public AppSettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AppSettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AppSettingsFragment newInstance(String param1, String param2) {
        AppSettingsFragment fragment = new AppSettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(HelperViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_app_settings;
    }


    @Override
    public void setUp() {
        binding = (FragmentAppSettingsBinding) getBaseDataBinding();

        updateResolutions();
        setUpDownloadStorage();

        binding.llAskMeLater.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(true);
            updateResolutions();
        });
        binding.rbAskMeLater.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(true);
            updateResolutions();
        });


        binding.llBest.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(1080);
            updateResolutions();
        });
        binding.rb1080.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(1080);
            updateResolutions();
        });


        binding.llBetter.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(720);
            updateResolutions();
        });
        binding.rb720.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(720);
            updateResolutions();
        });


        binding.llGood.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(540);
            updateResolutions();
        });
        binding.rb540.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(540);
            updateResolutions();
        });


        binding.llAverage.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(360);
            updateResolutions();
        });
        binding.rb360.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(360);
            updateResolutions();
        });


        binding.llDataSaver.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(240);
            updateResolutions();
        });
        binding.rb270.setOnClickListener(v -> {
            viewModel.setAskMeEveryTime(false);
            viewModel.setSelectedDownloadResolution(270);
            updateResolutions();
        });

        binding.rdInternal.setOnClickListener(v -> {
            if (userRepositoryManager.getVideoDownloadLocation() == 2) {
                setInternalStorage();
            }
        });

        binding.rdExternal.setOnClickListener(v -> {
            if (userRepositoryManager.getVideoDownloadLocation() == 0 || userRepositoryManager.getVideoDownloadLocation() == 1) {
                setExternalStorage();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpDownloadStorage();
    }


    private void setUpDownloadStorage() {
        if (Utility.isExternalStorageWritable(activity())) {
            binding.llStorageLayout.setVisibility(View.VISIBLE);
            if (userRepositoryManager.getVideoDownloadLocation() == 2 && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
                binding.rdExternal.setChecked(true);
            } else {
                binding.rdInternal.setChecked(true);
            }
        } else {
            binding.llStorageLayout.setVisibility(View.GONE);
        }
    }

    private void setInternalStorage() {
        if (HelperApplication.getInstance().getDownloadTracker() != null) {
            if (HelperApplication.getInstance().getDownloadTracker().getAllDownloads().size() > 0) {
                Utility.requestDialog(activity(), this, "Alert!", "Offline videos will be deleted due to change of storage", 4);
                return;
            }
        }
        userRepositoryManager.appSharedPreferences.setInt(Constants.DOWNLOAD_LOCATION, 1);
        reStartApp();
    }

    public void setExternalStorage() {
        if (Utility.checkPermissionRequest(Permission.WRITE_STORAGE, getApplicationContext())) {
            if (HelperApplication.getInstance().getDownloadTracker() != null) {
                if (HelperApplication.getInstance().getDownloadTracker().getAllDownloads().size() > 0) {
                    Utility.requestDialog(activity(), this, "Alert!", "Offline videos will be deleted due to change of storage", 5);
                } else {
                    userRepositoryManager.appSharedPreferences.setInt(Constants.DOWNLOAD_LOCATION, 2);
                    reStartApp();
                }
            }
        } else {
            if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
        }
    }


    private void reStartApp() {
        ((BaseActivity) activity()).clearAllExoFiles();
        ((BaseActivity) activity()).deleteOfflineVideo();
        if (VideoDownloadService.videoDownloadService != null) {
            VideoDownloadService.videoDownloadService.clearAll();
            VideoDownloadService.videoDownloadService.stopForeground(true);
            VideoDownloadService.videoDownloadService.stopSelf();
        }
        HelperApplication.getInstance().recreateDownloadManager();
        HelperApplication.getInstance().onCreate();

        VideoDownloadService.notify = false;
        Intent intent = new Intent(activity(), VideoDownloadService.class);
        activity().startService(intent);

        if (Utility.isExternalStorageWritable(activity())) {
            binding.llStorageLayout.setVisibility(View.VISIBLE);
            if (userRepositoryManager.getVideoDownloadLocation() == 2 && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
                showToast("Successfully changed to External Storage", false);
            } else {
                showToast("Successfully changed to Internal Storage", false);
            }
        } else {
            showToast("Successfully changed to Internal Storage", false);
        }

        intent = new Intent(activity(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        activity().finish();
    }

    private void updateResolutions() {
        if (viewModel.getAskMeEveryTime()) {
            binding.rbAskMeLater.setChecked(true);
            binding.rb1080.setChecked(false);
            binding.rb720.setChecked(false);
            binding.rb540.setChecked(false);
            binding.rb360.setChecked(false);
            binding.rb270.setChecked(false);
        } else {
            switch (viewModel.getSelectedDownloadResolution()) {
                case 1080:
                    binding.rbAskMeLater.setChecked(false);
                    binding.rb1080.setChecked(true);
                    binding.rb720.setChecked(false);
                    binding.rb540.setChecked(false);
                    binding.rb360.setChecked(false);
                    binding.rb270.setChecked(false);
                    break;
                case 720:
                    binding.rbAskMeLater.setChecked(false);
                    binding.rb1080.setChecked(false);
                    binding.rb720.setChecked(true);
                    binding.rb540.setChecked(false);
                    binding.rb360.setChecked(false);
                    binding.rb270.setChecked(false);
                    break;
                case 540:
                    binding.rbAskMeLater.setChecked(false);
                    binding.rb1080.setChecked(false);
                    binding.rb720.setChecked(false);
                    binding.rb540.setChecked(true);
                    binding.rb360.setChecked(false);
                    binding.rb270.setChecked(false);
                    break;
                case 360:
                    binding.rbAskMeLater.setChecked(false);
                    binding.rb1080.setChecked(false);
                    binding.rb720.setChecked(false);
                    binding.rb540.setChecked(false);
                    binding.rb360.setChecked(true);
                    binding.rb270.setChecked(false);
                    break;
                case 270:
                case 240:
                    binding.rbAskMeLater.setChecked(false);
                    binding.rb1080.setChecked(false);
                    binding.rb720.setChecked(false);
                    binding.rb540.setChecked(false);
                    binding.rb360.setChecked(false);
                    binding.rb270.setChecked(true);
                    break;
            }
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void doPositiveAction(int id) {
        if (id == 4) {
            ((BaseActivity) activity()).deleteOfflineVideo();
            userRepositoryManager.appSharedPreferences.setInt(Constants.DOWNLOAD_LOCATION, 1);
            binding.rdInternal.setChecked(true);
            reStartApp();
//            ((BaseActivity) activity()).recreate();
        } else if (id == 5) {
            ((BaseActivity) activity()).deleteOfflineVideo();
            userRepositoryManager.appSharedPreferences.setInt(Constants.DOWNLOAD_LOCATION, 2);
            binding.rdExternal.setChecked(true);
            reStartApp();
//            ((BaseActivity) activity()).recreate();
        }

    }

    @Override
    public void doNegativeAction() {

        if (binding.radioGroup.getCheckedRadioButtonId() == R.id.rdInternal) {
            binding.rdExternal.setChecked(true);
            binding.rdInternal.setChecked(false);

        } else if (binding.radioGroup.getCheckedRadioButtonId() == R.id.rdExternal) {
            binding.rdInternal.setChecked(true);
            binding.rdExternal.setChecked(false);
        }
    }


}
