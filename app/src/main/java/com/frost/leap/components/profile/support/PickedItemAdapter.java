package com.frost.leap.components.profile.support;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.frost.leap.R;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.subject.askexpert.AskExpertFragment;
import com.frost.leap.databinding.ItemPickedBinding;

import java.util.List;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 01-10-2019.
 * <p>
 * FROST
 */
public class PickedItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<GalleryFile> list;
    private Context context;
    private IPickedItemAdapter iPickedItemAdapter;
    private int type = 0;


    public PickedItemAdapter(List<GalleryFile> list, Context context, IPickedItemAdapter iPickedItemAdapter, int type) {
        this.list = list;
        this.context = context;
        this.type = type;
        this.iPickedItemAdapter = iPickedItemAdapter;
    }

    public PickedItemAdapter(List<GalleryFile> list, Context context, IPickedItemAdapter iPickedItemAdapter) {
        this.list = list;
        this.context = context;
        this.iPickedItemAdapter = iPickedItemAdapter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new PickedViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_picked, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PickedViewHolder) {
            PickedViewHolder pickedViewHolder = (PickedViewHolder) holder;

            if (type == 0) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                pickedViewHolder.itemView.setLayoutParams(params);

                pickedViewHolder.binding.rLImageLayout.setVisibility(View.VISIBLE);
                pickedViewHolder.binding.llTextLayout.setVisibility(View.GONE);


                Glide.with(context)
                        .load(list.get(position).uri)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.ic_photo)
                                .centerCrop().skipMemoryCache(true))
                        .into(pickedViewHolder.binding.imgPicked);

                pickedViewHolder.binding.imgRemove.setOnClickListener(v -> {
                    iPickedItemAdapter.deleteItem(list.get(position), position);
                });
            } else if (type == 1) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                pickedViewHolder.itemView.setLayoutParams(params);

                pickedViewHolder.binding.rLImageLayout.setVisibility(View.GONE);
                pickedViewHolder.binding.llTextLayout.setVisibility(View.VISIBLE);

                String[] separated = list.get(position).caption.split("\\.");

                pickedViewHolder.binding.tvImageName.setText(list.get(position).name + "." + separated[1]);


                pickedViewHolder.binding.tvRemove.setOnClickListener(v -> {
                    iPickedItemAdapter.deleteItem(list.get(position), position);
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PickedViewHolder extends RecyclerView.ViewHolder {
        private ItemPickedBinding binding;

        public PickedViewHolder(ItemPickedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
