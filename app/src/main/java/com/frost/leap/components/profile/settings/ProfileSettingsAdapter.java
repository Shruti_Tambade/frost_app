package com.frost.leap.components.profile.settings;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.databinding.ItemChangePasswordBinding;
import com.frost.leap.databinding.ItemEditDobBinding;
import com.frost.leap.databinding.ItemEditEmailBinding;
import com.frost.leap.databinding.ItemEditNameBinding;
import com.frost.leap.databinding.ItemEditOtsDetailsBinding;
import com.frost.leap.databinding.ItemEditPhoneNumberBinding;
import com.frost.leap.databinding.ItemMenuProfileBinding;
import com.frost.leap.databinding.ItemProfileGenderBinding;
import com.frost.leap.databinding.ItemProfileSettingsHeaderBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;
import com.frost.leap.viewmodels.models.Message;

import java.util.List;

import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 13-09-2019.
 * <p>
 * Frost
 */
public class ProfileSettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MenuItem> list;
    private IProfileSettingsAdapter iProfileSettingsAdapter;
    private Context context;
    private Uri uri = null;
    private int previous = -1;
    private User user;
    private RController rController;
    public boolean isAllow = false;
    public boolean isCancel = false;
    private String updatedMobileNumber = null;
    private String password = null;
    private CountDownTimer countDownTimer;
    private int highQualificationIndex = -1, currentGoalIndex = -1;


    public ProfileSettingsAdapter(User user, RController rController, List<MenuItem> list, IProfileSettingsAdapter iProfileSettingsAdapter, Context context) {
        this.list = list;
        this.iProfileSettingsAdapter = iProfileSettingsAdapter;
        this.context = context;
        this.user = user;
        this.rController = rController;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : list.get(position) != null ? 1 : getExactType(position);
    }

    private int getExactType(int position) {
        switch (position) {
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
//            case 8:
//                return 8;

            default:
                return 2;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skeleton_view, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else if (type == 0) {
            return new HeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                    R.layout.item_profile_settings_header, viewGroup, false));
        } else {
            if (type == 1) {
                return new ProfileSettingsItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.item_menu_profile, viewGroup, false));
            } else {
                switch (type) {
                    case 2:
                        return new EditNameViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_name, viewGroup, false));
                    case 3:
                        return new EditDOBViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_dob, viewGroup, false));
                    case 4:
                        return new EditGenderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_profile_gender, viewGroup, false));
                    case 5:
                        return new EditPhoneViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_phone_number, viewGroup, false));
                    case 6:
                        return new EditEmailViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_email, viewGroup, false));
                    case 7:
                        return new ChangePasswordViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_change_password, viewGroup, false));
//                    case 8:
//                        return new EditOtsDetailsViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
//                                R.layout.item_edit_ots_details, viewGroup, false));
                    default:
                        return new EditNameViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                R.layout.item_edit_name, viewGroup, false));

                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.bindData();
            headerViewHolder.binding.imgEdit.setOnClickListener(v -> {
                iProfileSettingsAdapter.onChangeProfilePic();
            });

            headerViewHolder.binding.imgUser.setOnClickListener(v -> {
                iProfileSettingsAdapter.onChangeProfilePic();
            });

            headerViewHolder.binding.tvShortName.setOnClickListener(v -> {
                iProfileSettingsAdapter.onChangeProfilePic();
            });
        } else if (viewHolder instanceof ProfileSettingsItemViewHolder) {
            ProfileSettingsItemViewHolder menuItemViewHolder = (ProfileSettingsItemViewHolder) viewHolder;
            menuItemViewHolder.bindData(list.get(i), i);
            menuItemViewHolder.binding.llFooter.setOnClickListener(v -> {
                iProfileSettingsAdapter.onProfileSettingsClick(list.get(i), i);
            });
        } else if (viewHolder instanceof EditNameViewHolder) {
            EditNameViewHolder editNameViewHolder = (EditNameViewHolder) viewHolder;
            editNameViewHolder.bindData();
            editNameViewHolder.binding.tvSave.setOnClickListener(v -> {
                if (TextUtils.isEmpty(editNameViewHolder.binding.etFirstName.getText().toString())) {
                    iProfileSettingsAdapter.sendMessage(new Message("First name not be empty", 2));
                    return;
                }
                if (TextUtils.isEmpty(editNameViewHolder.binding.etLastName.getText().toString())) {
                    iProfileSettingsAdapter.sendMessage(new Message("Last name not be empty", 2));
                    return;
                }
                if (!Utility.isNetworkAvailable(context)) {
                    iProfileSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }

                user.getProfileInfo().setFirstName(Utility.getCamelCase(editNameViewHolder.binding.etFirstName.getText().toString()));
                user.getProfileInfo().setLastName(Utility.getCamelCase(editNameViewHolder.binding.etLastName.getText().toString()));
                iProfileSettingsAdapter.updateUserDetails(user, i);

                Mixpanel.updateName(user.getProfileInfo().getFirstName(), user.getProfileInfo().getLastName(), "SUCCESS");

                updateValue(user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName(), i - 1);
                deleteSubItem(i - 1);

                if (uri == null)
                    notifyItemChanged(0);

                iProfileSettingsAdapter.sendMessage(new Message("Profile is updated", 1));
            });
        } else if (viewHolder instanceof EditGenderViewHolder) {
            EditGenderViewHolder editGenderViewHolder = (EditGenderViewHolder) viewHolder;
            editGenderViewHolder.bindData();
            editGenderViewHolder.binding.imgMale.setOnClickListener(v -> {
                if (!Utility.isNetworkAvailable(context)) {
                    iProfileSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                user.getProfileInfo().setGender("MALE");
                iProfileSettingsAdapter.updateUserDetails(user, i);

                Mixpanel.updateGender(user.getProfileInfo().getGender(), "SUCCESS");

                updateValue(user.getProfileInfo().getGender(), i - 1);

                deleteSubItem(i - 1);
                iProfileSettingsAdapter.sendMessage(new Message("Gender is changed", 1));
            });
            editGenderViewHolder.binding.imgFemale.setOnClickListener(v -> {
                if (!Utility.isNetworkAvailable(context)) {
                    iProfileSettingsAdapter.sendMessage(new Message(Constants.PLEASE_CHECK_INTERNET, 2));
                    return;
                }
                user.getProfileInfo().setGender("FEMALE");
                iProfileSettingsAdapter.updateUserDetails(user, i);

                Mixpanel.updateGender(user.getProfileInfo().getGender(), "SUCCESS");

                updateValue(user.getProfileInfo().getGender(), i - 1);
                deleteSubItem(i - 1);
                iProfileSettingsAdapter.sendMessage(new Message("Gender is changed", 1));
            });
        } else if (viewHolder instanceof ChangePasswordViewHolder) {
            ChangePasswordViewHolder changePasswordViewHolder = (ChangePasswordViewHolder) viewHolder;
            changePasswordViewHolder.bindData();
            changePasswordViewHolder.binding.tvChange.setOnClickListener(v -> {
                iProfileSettingsAdapter.changePassword(i,
                        changePasswordViewHolder.binding.etOldPassword.getText().toString(),
                        changePasswordViewHolder.binding.etNewPassword.getText().toString(),
                        changePasswordViewHolder.binding.etConfirmPassword.getText().toString());
            });
        } else if (viewHolder instanceof EditPhoneViewHolder) {
            EditPhoneViewHolder editPhoneViewHolder = (EditPhoneViewHolder) viewHolder;
            editPhoneViewHolder.binding.tvChange.setAlpha(isAllow ? 0.3f : 1);
            editPhoneViewHolder.binding.tvVerify.setAlpha(isAllow ? 1f : 0.3f);
            editPhoneViewHolder.binding.tvTime.setVisibility(View.GONE);
            editPhoneViewHolder.binding.etMobileNumber.setText(isAllow ? "" + updatedMobileNumber : "");
            editPhoneViewHolder.binding.etOtp.setText("");
            editPhoneViewHolder.binding.tvChange.setVisibility(isAllow ? View.GONE : View.VISIBLE);
            editPhoneViewHolder.binding.tiLayout.setVisibility(isAllow ? View.GONE : View.VISIBLE);

            if (!isAllow) editPhoneViewHolder.binding.etPassword.setText("");

            editPhoneViewHolder.binding.etMobileNumber.setEnabled(isAllow ? false : true);
            editPhoneViewHolder.binding.etMobileNumber.setFocusable(isAllow ? false : true);
            editPhoneViewHolder.binding.etMobileNumber.setFocusableInTouchMode(isAllow ? false : true);
            editPhoneViewHolder.binding.tvChange.setOnClickListener(v -> {
                if (isAllow) {
                    return;
                }
                updatedMobileNumber = editPhoneViewHolder.binding.etMobileNumber.getText().toString();
                password = editPhoneViewHolder.binding.etPassword.getText().toString();
                iProfileSettingsAdapter.requestUpdateMobileNumber(i,
                        editPhoneViewHolder.binding.etMobileNumber.getText().toString(),
                        editPhoneViewHolder.binding.etPassword.getText().toString());
            });


            editPhoneViewHolder.binding.tvVerify.setOnClickListener(v -> {
                if (!isAllow) {
                    return;
                }
                iProfileSettingsAdapter.verifyOtp(i, updatedMobileNumber,
                        editPhoneViewHolder.binding.etOtp.getText().toString());
            });

            editPhoneViewHolder.binding.tvResend.setOnClickListener(v -> {
                if (!isAllow) {
                    return;
                }
                iProfileSettingsAdapter.requestUpdateMobileNumber(i,
                        updatedMobileNumber, password);
            });

            editPhoneViewHolder.binding.tvPrivacy.setOnClickListener(v -> {
                iProfileSettingsAdapter.gotoPrivacyPolicies();
            });

            if (isAllow) {
                if (countDownTimer == null)
                    countDownTimer = new CountDownTimer(Constants.COUNT_DOWN_TIME, Constants.COUNT_DOWN_INTERVAL) {

                        public void onTick(long millisUntilFinished) {
                            isCancel = false;
                            editPhoneViewHolder.binding.tvResend.setVisibility(View.GONE);
                            editPhoneViewHolder.binding.tvTime.setVisibility(View.VISIBLE);
                            editPhoneViewHolder.binding.tvTime.setText("00 : " +
                                    ((millisUntilFinished / 1000) < 10
                                            ? "0" + (millisUntilFinished / 1000)
                                            : millisUntilFinished / 1000));
                        }

                        public void onFinish() {
                            editPhoneViewHolder.binding.tvTime.setVisibility(View.GONE);
                            editPhoneViewHolder.binding.tvResend.setVisibility(View.VISIBLE);
                            isCancel = true;
                        }

                    }.start();
                else if (isCancel)
                    countDownTimer.start();

            }


        }
        /*
        else if (viewHolder instanceof EditOtsDetailsViewHolder){
            EditOtsDetailsViewHolder editOtsDetailsViewHolder = (EditOtsDetailsViewHolder) viewHolder;
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    R.layout.spinner_item, Constants.HIGH_QUALIFICATIONS);
            adapter.setDropDownViewResource(R.layout.dropdown_item);
            editOtsDetailsViewHolder.binding.hqSpinner.setAdapter(adapter);


            ArrayAdapter<String> adapter1 = new ArrayAdapter<>(context,
                    R.layout.spinner_item, Constants.CURRENT_GOALS);
            adapter1.setDropDownViewResource(R.layout.dropdown_item);
            editOtsDetailsViewHolder.binding.currentGoalSpinner.setAdapter(adapter1);


            editOtsDetailsViewHolder.binding.hqSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    highQualificationIndex = position;
                    ((TextView) view).setTextColor(ContextCompat.getColor(context,R.color.normal_text_color)); //Change selected text color
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            editOtsDetailsViewHolder.binding.currentGoalSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    currentGoalIndex = position;
                    ((TextView) view).setTextColor(ContextCompat.getColor(context,R.color.normal_text_color)); //Change selected text color
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            editOtsDetailsViewHolder.binding.cbStudent.setTypeface(Utility.getTypeface(5, context));
            editOtsDetailsViewHolder.binding.cbWorking.setTypeface(Utility.getTypeface(5, context));
            editOtsDetailsViewHolder.binding.cbUnEmployed.setTypeface(Utility.getTypeface(5, context));
            editOtsDetailsViewHolder.binding.cbStudent.setChecked(true);


            editOtsDetailsViewHolder.binding.cbStudent.setOnClickListener(v -> {
                editOtsDetailsViewHolder.binding.cbStudent.setChecked(true);
                editOtsDetailsViewHolder.binding.cbWorking.setChecked(false);
                editOtsDetailsViewHolder.binding.cbUnEmployed.setChecked(false);
                editOtsDetailsViewHolder.binding.etCompanyName.setVisibility(View.GONE);
                editOtsDetailsViewHolder.binding.tvCurrentCompany.setVisibility(View.GONE);
                editOtsDetailsViewHolder.binding.etCompanyName.setText("");
            });

            editOtsDetailsViewHolder.binding.cbWorking.setOnClickListener(v -> {
                editOtsDetailsViewHolder.binding.cbWorking.setChecked(true);
                editOtsDetailsViewHolder.binding.cbStudent.setChecked(false);
                editOtsDetailsViewHolder.binding.cbUnEmployed.setChecked(false);
                editOtsDetailsViewHolder.binding.etCompanyName.setVisibility(View.VISIBLE);
                editOtsDetailsViewHolder.binding.tvCurrentCompany.setVisibility(View.VISIBLE);
                editOtsDetailsViewHolder.binding.etCompanyName.setText("");
            });

            editOtsDetailsViewHolder.binding.cbUnEmployed.setOnClickListener(v -> {
                editOtsDetailsViewHolder.binding.cbUnEmployed.setChecked(true);
                editOtsDetailsViewHolder.binding.cbStudent.setChecked(false);
                editOtsDetailsViewHolder.binding.cbWorking.setChecked(false);
                editOtsDetailsViewHolder.binding.etCompanyName.setVisibility(View.GONE);
                editOtsDetailsViewHolder.binding.tvCurrentCompany.setVisibility(View.GONE);
                editOtsDetailsViewHolder.binding.etCompanyName.setText("");
            });

            editOtsDetailsViewHolder.binding.cbYes.setOnClickListener(v -> {
                editOtsDetailsViewHolder.binding.cbYes.setChecked(true);
                editOtsDetailsViewHolder.binding.cbNo.setChecked(false);
            });

            editOtsDetailsViewHolder.binding.cbNo.setOnClickListener(v -> {
                editOtsDetailsViewHolder.binding.cbNo.setChecked(true);
                editOtsDetailsViewHolder.binding.cbYes.setChecked(false);
            });

        }

         */

    }


    @Override
    public int getItemCount() {
        return rController == RController.LOADING ? 1 : list.size();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        private ItemProfileSettingsHeaderBinding binding;

        public HeaderViewHolder(ItemProfileSettingsHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            if (user.getProfileInfo() != null && user.getProfileInfo().getImage() != null && uri == null) {
                if (!TextUtils.isEmpty(user.getProfileInfo().getImage()))
                    uri = Uri.parse(user.getProfileInfo().getImage());
            }
            if (uri == null) {
                binding.tvShortName.setVisibility(View.VISIBLE);
                binding.tvShortName.setText(Utility.getShortName(user.getProfileInfo().getFirstName(),
                        user.getProfileInfo().getLastName()));

            } else {
                binding.tvShortName.setVisibility(View.GONE);
                binding.imgUser.setImageURI(uri);
            }
        }
    }

    public class ProfileSettingsItemViewHolder extends RecyclerView.ViewHolder {
        private ItemMenuProfileBinding binding;

        public ProfileSettingsItemViewHolder(ItemMenuProfileBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(MenuItem menuItem, int position) {
            binding.llHeader.setVisibility(menuItem.getHeader() == null ? View.GONE : View.VISIBLE);
            binding.tvHeader.setText(menuItem.getHeader() == null ? "" : menuItem.getHeader());
            binding.tvTitle.setText(menuItem.getTitle());
            binding.tvDescription.setText(menuItem.getDescription());
            binding.imgGo.setRotation(!menuItem.isOpen() ? 0 : 180);
            binding.imgGo.setRotation(menuItem.isExpand() ? binding.imgGo.getRotation() : -90);
            binding.viewBottom.setVisibility(menuItem.isOpen() ? View.GONE : View.VISIBLE);

            binding.imgGo.setVisibility(menuItem.getTitle().equals("Email") ? View.GONE : View.VISIBLE);

            if (position == list.size() - 1) {
//                binding.tvTitle.setTextColor(Color.parseColor("#e2452a"));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, Utility.dpSize(context, 120));
                binding.getRoot().setLayoutParams(params);
            } else {
//                binding.tvTitle.setTextColor(Utility.getColorsFromAttrs(context, R.attr.text_color));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, Utility.dpSize(context, 0));
                binding.getRoot().setLayoutParams(params);
            }
        }
    }

    public class EditNameViewHolder extends RecyclerView.ViewHolder {
        private ItemEditNameBinding binding;

        public EditNameViewHolder(ItemEditNameBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            binding.etFirstName.setText(Utility.getCamelCase(user.getProfileInfo().getFirstName()));
            binding.etLastName.setText(Utility.getCamelCase(user.getProfileInfo().getLastName()));
        }
    }


    public class EditDOBViewHolder extends RecyclerView.ViewHolder {
        private ItemEditDobBinding binding;

        public EditDOBViewHolder(ItemEditDobBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
        }
    }

    public class EditGenderViewHolder extends RecyclerView.ViewHolder {
        private ItemProfileGenderBinding binding;

        public EditGenderViewHolder(ItemProfileGenderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            binding.imgMale.setImageResource(user.getProfileInfo().getGender().equalsIgnoreCase("male") ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
            binding.imgFemale.setImageResource(user.getProfileInfo().getGender().equalsIgnoreCase("female") ? R.drawable.ic_radio_button_checked : R.drawable.ic_radio_button_unchecked);
        }
    }

    public class EditPhoneViewHolder extends RecyclerView.ViewHolder {
        private ItemEditPhoneNumberBinding binding;

        public EditPhoneViewHolder(ItemEditPhoneNumberBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {

        }
    }


    public class EditEmailViewHolder extends RecyclerView.ViewHolder {
        private ItemEditEmailBinding binding;

        public EditEmailViewHolder(ItemEditEmailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {

        }
    }

    public class ChangePasswordViewHolder extends RecyclerView.ViewHolder {
        private ItemChangePasswordBinding binding;

        public ChangePasswordViewHolder(ItemChangePasswordBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData() {
            this.binding.etOldPassword.setText("");
            this.binding.etNewPassword.setText("");
            this.binding.etConfirmPassword.setText("");
        }
    }


    public class EditOtsDetailsViewHolder extends RecyclerView.ViewHolder {
        private ItemEditOtsDetailsBinding binding;

        public EditOtsDetailsViewHolder(@NonNull ItemEditOtsDetailsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


        public void bindData() {

        }
    }

    public void updateProfilePic(Uri uri) {
        this.uri = uri;
        notifyItemChanged(0);
    }


    public void addSubItem(int position) {

        //iProfileSettingsAdapter.sendMessage(new Message("add position "+position,2));
        if (previous != -1) {
            list.remove(previous + 1);
            list.get(previous).setOpen(false);
            if (position > previous) {
                position = position - 1;
            }
            notifyItemRemoved(previous + 1);
            notifyItemChanged(previous);
        }
        list.get(position).setOpen(true);
        list.add(position + 1, null);
        previous = position;


        notifyItemInserted(position + 1);
        notifyItemChanged(position);
        notifyItemRangeChanged(1, list.size());
        //notifyDataSetChanged();
    }

    public void deleteSubItem(int position) {
        //iProfileSettingsAdapter.sendMessage(new Message("remove position "+position,2));
        list.remove(position + 1);
        list.get(position).setOpen(false);
        if (previous == position) {
            previous = -1;
        }
        notifyItemRemoved(position + 1);
        notifyItemChanged(position);
        notifyItemRangeChanged(1, list.size());
        //notifyDataSetChanged();

    }

    public void updateValue(String value, int position) {
        if (list.get(position) instanceof MenuItem) {
            list.get(position).setDescription(value);
        }
    }

    public void updateData(User user) {
        this.user = user;
    }


    public void updateUiMobileNumber(boolean isAllow, int position) {
        this.isAllow = isAllow;
        notifyItemChanged(position);
    }


}
