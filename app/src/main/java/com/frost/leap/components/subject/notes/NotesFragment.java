package com.frost.leap.components.subject.notes;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentNotesBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.services.ActionIntentService;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.subjects.NotesViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.Note;
import apprepos.subject.model.SubjectInfo;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.customviews.views.LinearLayoutManagerWithSmoothScroller;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotesFragment extends BaseFragment implements INotesAdapter, ICustomAlertDialog, INotesFilter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String value = "All Subject Notes";
    private FragmentNotesBinding binding;
    private NotesViewModel viewModel;
    private NotesAdapter notesAdapter;
    private List<Note> noteList = new ArrayList<>();
    private List<SubjectInfo> subjectInfoList;
    private CatalogueModel catalogueModel;
    private int position = -1;
    private Note note;
    private NoteActionBroadcastReceiver broadcastReceiver;
    private int filterCount = 0;
    private boolean hasSort = false;

    public NotesFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static NotesFragment newInstance(CatalogueModel catalogueModel, String param2) {
        NotesFragment fragment = new NotesFragment();
        Bundle args = new Bundle();
        fragment.catalogueModel = catalogueModel;
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        broadcastReceiver = new NoteActionBroadcastReceiver();
        viewModel = new ViewModelProvider(this).get(NotesViewModel.class);

    }

    @Override
    public int layout() {
        return R.layout.fragment_notes;
    }

    @Override
    public void setUp() {

        if (catalogueModel == null)
            return;

        binding = (FragmentNotesBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(activity()));
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setFocusable(false);

        binding.swipeRefreshLayout.setOnRefreshListener(() ->
        {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchNotes(catalogueModel.getStudentSubjectIds());
            hasSort = false;
            viewModel.hasFilters = false;
            filterCount = 0;
        });

        viewModel.updateNotesList().observe(this, list -> {
            this.noteList = new ArrayList<>();
            if (list != null) {
                noteList.addAll(list);
            }
//            noteList.add(0, null);
            binding.recyclerView.setFocusable(false);
            binding.recyclerView.setAdapter(notesAdapter = new NotesAdapter(activity(), value, RController.DONE, noteList, this, viewModel.hasFilters, filterCount, hasSort));
            binding.recyclerView.setHasFixedSize(true);
        });


        viewModel.updateSubjectsInfo().observe(this, subjectInfos -> {
            subjectInfoList = subjectInfos;
        });

        viewModel.getMessage().
                observe(this, message -> {
//                    showSnackBar(message.getMessage(), message.getType());
                });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case NotesViewModel.NetworkTags.NOTES_LIST:
                    updateUIForNotesList(networkCall);
                    break;
                case NotesViewModel.NetworkTags.SUBJECTS_INFO_LIST:
                    updateUIForSubjectsInfo(networkCall);
                    break;
            }
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });


        if (catalogueModel != null) {
            viewModel.fetchNotes(catalogueModel.getStudentSubjectIds());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    viewModel.fetchSubjectsInfo(catalogueModel.getStudentCatalogueId());
                }
            }, 1000);
        }

    }

    private void updateUIForSubjectsInfo(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case IN_PROCESS:
                binding.progressBar.setVisibility(View.VISIBLE);
                break;

            case DONE:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case ERROR:
                binding.progressBar.setVisibility(View.GONE);
                break;
        }
    }

    private void updateUIForNotesList(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;

            case IN_PROCESS:
                binding.recyclerView.setAdapter(new NotesAdapter(activity(), null, RController.LOADING, Arrays.asList(null, null), this, viewModel.hasFilters, filterCount, hasSort));
                break;

            case SUCCESS:
                break;

            case DONE:
                break;

            case NO_DATA:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), "No notes available", R.drawable.no_data_available, 1));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.something_went_wrong, 0));
                break;

            case UNAUTHORIZED:
                break;

            case SERVER_ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 0));
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void deleteNote(Note note, int position) {
        this.note = note;
        this.position = position;
        Utility.requestDialog(activity(),
                this,
                "Delete Note",
                "Do you want to delete this note?",
                101
        );
    }

    @Override
    public void editNote(Note note, int position) {
        if (activity() instanceof MainActivity) {
            ((MainActivity) activity()).updateNote(note, position, "");
        }
    }

    @Override
    public void sortNotes() {

    }

    @Override
    public void applyFilter() {

        if (subjectInfoList == null) {
            viewModel.fetchSubjectsInfo(catalogueModel.getStudentCatalogueId());
            return;
        }
        if (activity() instanceof MainActivity) {
            ((MainActivity) activity()).openFilters(catalogueModel.getCatalogueName(), viewModel.filterSubjectId,
                    viewModel.filterUnitId,
                    viewModel.filterTopicId,
                    this,
                    subjectInfoList,
                    viewModel.noteList);
        }
    }

    @Override
    public void applySort(boolean hasSort) {

        Collections.reverse(noteList);

        this.hasSort = hasSort;
        notesAdapter.changeSort(hasSort);
//        Toast.makeText(activity(), "" + hasSort, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doPositiveAction(int id) {
        if (id == 101) { // DELETE NOTE
            if (note == null)
                return;
            Intent intent = new Intent(activity(), ActionIntentService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_DELETE_NOTE);
            intent.putExtra("NoteId", note.getStudentNoteId());
            activity().startService(intent);
            if (noteList != null)
                if (noteList.size() > position) {
                    viewModel.updateJsonObject(note, true);
                    if (notesAdapter != null) {
                        noteList.remove(position);
                        notesAdapter.notifyDataSetChanged();
                        Mixpanel.deleteNote(catalogueModel.getCatalogueName());
                    }
                }

        }
    }

    @Override
    public void doNegativeAction() {
    }

    @Override
    public void clearFilters() {
        value = "All Subject Notes";
        viewModel.clearAllFilters();
        filterCount = 0;
    }

    @Override
    public void applyFilters(String subjectId, String unitId, String topicId, String value) {
        this.value = value;
        viewModel.filterNotes(subjectId, unitId, topicId);

        if (unitId == null)
            filterCount = 1;
        else if (topicId == null)
            filterCount = 2;
        else if (topicId != null)
            filterCount = 3;
    }

    public class NoteActionBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("Update", false)) {
                Note note = intent.getParcelableExtra("Note");
                int notePosition = intent.getIntExtra("Position", -1);
                if (notePosition == -1 || note == null) {
                    return;
                }
                if (noteList != null)
                    if (noteList.size() > notePosition) {
                        viewModel.updateJsonObject(note, false);
                        if (notesAdapter != null) {
                            noteList.set(notePosition, note);
                            notesAdapter.notifyDataSetChanged();
                        }
                    }
            } else if (intent.getBooleanExtra("Refresh", false)) {
                Utility.showToast(activity(), "Note added", true);
                if (viewModel != null)
                    viewModel.fetchNotes(catalogueModel.getStudentSubjectIds());
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        registerReceiver();
    }

    private void registerReceiver() {
        try {
            if (broadcastReceiver == null)
                return;

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_UPDATE_NOTES);
            activity().registerReceiver(broadcastReceiver, intentFilter);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onStop() {
        super.onStop();
        try {
            if (broadcastReceiver == null)
                return;
            activity().unregisterReceiver(broadcastReceiver);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
