package com.frost.leap.components.profile.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Type implements Parcelable {

    private String typeName;
    private List<String> topicsList;

    public Type(){

    }


    protected Type(Parcel in) {
        typeName = in.readString();
        topicsList = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeName);
        dest.writeStringList(topicsList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Type> CREATOR = new Creator<Type>() {
        @Override
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        @Override
        public Type[] newArray(int size) {
            return new Type[size];
        }
    };

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<String> getTopicsList() {
        return topicsList;
    }

    public void setTopicsList(List<String> topicsList) {
        this.topicsList = topicsList;
    }
}
