package com.frost.leap.components.quiz;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentQuizBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.quiz.QuizViewModel;

import apprepos.quiz.model.QuizQuestionsModel;
import apprepos.quiz.model.QuizReportModel;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link QuizFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String subjectId, unitId, topicId;
    private FragmentQuizBinding binding;
    private QuizQuestionsModel quizQuestionsModel;
    private QuizReportModel quizReportModel;
    private int secondaryProgress = 0, currentQuestionNumber = 0;
    private boolean start = false;
    private QuizViewModel viewModel;

    public QuizFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static QuizFragment newInstance(QuizQuestionsModel param1, String subjectId, String unitId, String topicId, boolean start) {
        QuizFragment fragment = new QuizFragment();
        Bundle args = new Bundle();
        args.putString("SubjectId", subjectId);
        args.putString("UnitId", unitId);
        args.putString("TopicId", topicId);
        args.putParcelable(ARG_PARAM1, param1);
        args.putBoolean("Start", start);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(QuizViewModel.class);
        if (getArguments() != null) {
            subjectId = getArguments().getString("SubjectId");
            unitId = getArguments().getString("UnitId");
            topicId = getArguments().getString("TopicId");
            start = getArguments().getBoolean("Start", false);
            quizQuestionsModel = getArguments().getParcelable(ARG_PARAM1);

            if (start) {
                for (int i = 0; i < quizQuestionsModel.getQuestions().size(); i++)
                    for (int j = 0; j < quizQuestionsModel.getQuestions().get(i).getOptions().size(); j++) {
                        quizQuestionsModel.getQuestions().get(i).getOptions().get(j).setTicked(false);
                        quizQuestionsModel.getQuestions().get(i).getOptions().get(j).setSelected(false);
                    }

            }
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_quiz;
    }

    @Override
    public void setUp() {
        binding = (FragmentQuizBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        binding.ivClose.setOnClickListener(v ->
                activity().finish());


        binding.ivBackQuestion.setOnClickListener(v ->
                binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() - 1));


        binding.tvNextQuestion.setOnClickListener(v -> {

            if (binding.viewPager.getCurrentItem() + 1 == quizQuestionsModel.getQuestions().size()) {
                submitQuiz();
            } else {
                binding.viewPager.setCurrentItem(binding.viewPager.getCurrentItem() + 1);

            }
        });

        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int currentPosition) {

                currentQuestionNumber = currentPosition;
                binding.ivBackQuestion.setVisibility(currentPosition == 0 ? View.GONE : View.VISIBLE);
                binding.progressBar.setSecondaryProgress(currentPosition + 1 == quizQuestionsModel.getQuestions().size() ? 100 : (currentPosition + 1) * secondaryProgress);
                binding.tvNextQuestion.setText(currentPosition + 1 == quizQuestionsModel.getQuestions().size()
                        ? "Submit" : "Next Question");

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        if (quizQuestionsModel.getQuestions() != null && quizQuestionsModel.getQuestions().size() > 0) {
            binding.tvQuizType.setText("QuizConstants . " + Utility.getCamelCase(quizQuestionsModel.getQuizName()));
            binding.viewPager.setOffscreenPageLimit(quizQuestionsModel.getQuestions().size());
            binding.viewPager.setPagingEnabled(false);

            binding.viewPager.setAdapter(new QuizViewPagerAdapter(activity(), quizQuestionsModel.getQuestions().size(), quizQuestionsModel));
            binding.viewPager.setClipToPadding(false);

            secondaryProgress = (100 / quizQuestionsModel.getQuestions().size());
            binding.progressBar.setSecondaryProgress(secondaryProgress);

            if (quizQuestionsModel.getQuestions().size() <= 1)
                binding.tvNextQuestion.setText("Submit");
            else
                binding.tvNextQuestion.setText("Next question");

        }

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case QuizViewModel.NetworkTags.SUBMIT_QUIZ:
                    updateUiForSubmitQuiz(networkCall);
                    break;
                case QuizViewModel.NetworkTags.UPDATE_QUIZ_STATUS:
                    updateUiForUpdateQuizStatus(networkCall);
                    break;
            }
        });


    }

    private void updateUiForUpdateQuizStatus(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
            case IN_PROCESS:
                binding.progressBarFooter.setVisibility(View.VISIBLE);
                binding.tvNextQuestion.setVisibility(View.GONE);
                break;
            case DONE:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
            case SUCCESS:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                gotoQuizResult();
                break;
            case FAIL:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateUiForSubmitQuiz(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
            case IN_PROCESS:
                binding.progressBarFooter.setVisibility(View.VISIBLE);
                binding.tvNextQuestion.setVisibility(View.GONE);
                break;
            case DONE:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
            case SUCCESS:
                if (networkCall.getBundle() != null)
                    quizReportModel = networkCall.getBundle().getParcelable("QuizReportModel");
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                viewModel.updateQuizStatus(subjectId, unitId, topicId, quizQuestionsModel.getQuizId());
                break;
            case FAIL:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
            case ERROR:
                binding.progressBarFooter.setVisibility(View.GONE);
                binding.tvNextQuestion.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void submitQuiz() {

        if (quizQuestionsModel != null) {
            viewModel.submitQuiz(quizQuestionsModel);
        }
    }

    private void gotoQuizResult() {
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUIZ_RESULT);
        quizQuestionsModel.setQuizReport(quizReportModel);
        intent.putExtra("QuizQuestionsModel", quizQuestionsModel);
        intent.putExtra("SubjectId", subjectId);
        intent.putExtra("UnitId", unitId);
        intent.putExtra("TopicId", topicId);
        startActivity(intent);
        activity().finish();
    }

    @Override
    public void changeTitle(String title) {

    }

}
