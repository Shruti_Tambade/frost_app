package com.frost.leap.components.profile.settings;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.frost.leap.R;
import com.frost.leap.activities.BaseActivity;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentProfileSettingsBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.services.HelperService;
import com.frost.leap.viewmodels.models.Message;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.profile.ProfileSettingsViewModel;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link ProfileSettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileSettingsFragment extends BaseFragment implements IProfileSettingsAdapter, ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProfileSettingsViewModel viewModel;
    private FragmentProfileSettingsBinding binding;
    private List<MenuItem> list;
    private User user;
    private GalleryFile galleryFile;
    private ProfileSettingsAdapter profileSettingsAdapter;
    private Calendar calendar;
    private int position = -1;
    private String mobileNumberMixpanel = "";

    public ProfileSettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileSettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileSettingsFragment newInstance(String param1, String param2) {
        ProfileSettingsFragment fragment = new ProfileSettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ProfileSettingsViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_profile_settings;
    }

    @Override
    public void setUp() {
        binding = (FragmentProfileSettingsBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchUserDetails();
        });


        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case ProfileSettingsViewModel.NetworkTags.GET_USER_DETAILS:
                    updateUiForUserDetails(networkCall);
                    break;

                case ProfileSettingsViewModel.NetworkTags.PUSH_PROFILE_PIC_S3:
                    updateUiForProfilePicUpload(networkCall);
                    break;

                case ProfileSettingsViewModel.NetworkTags.UPDATE_PASSWORD:
                    updateUiForUpdatePassword(networkCall);
                    break;
                case ProfileSettingsViewModel.NetworkTags.UPDATE_MOBILE:
                    updateUiForUpdateMobile(networkCall);
                    break;

                case ProfileSettingsViewModel.NetworkTags.VERIFY_OTP:
                    updateUiForVerifyOTP(networkCall);
                    break;
            }
        });

        viewModel.updateUser().observe(this, user ->
        {
            binding.swipeRefreshLayout.setEnabled(false);
            this.user = user;
            binding.recyclerView.setAdapter(profileSettingsAdapter = new ProfileSettingsAdapter(user, RController.DONE, viewModel.generateProfileSettingsMenu(user), this, activity()));
        });


        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        viewModel.fetchUserDetails();


    }


    private void updateUiForUserDetails(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;
            case IN_PROCESS:
                binding.recyclerView.setAdapter(new ProfileSettingsAdapter(null, RController.LOADING, Arrays.asList(null, null), this, activity()));
                break;
            case FAIL:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;

        }
    }

    private void updateUiForProfilePicUpload(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case IN_PROCESS:
                binding.progressBar.setVisibility(View.VISIBLE);
                break;

            case SUCCESS:
                binding.progressBar.setVisibility(View.GONE);
                if (networkCall.getBundle() != null) {
                    user.getProfileInfo().setImage(networkCall.getBundle().getString("ImageUrl"));
                    updateUser(user);
                }
                Mixpanel.updateProfilePic("Profile pic updated successfully");
                break;

            case FAIL:
                binding.progressBar.setVisibility(View.GONE);
                break;

            case ERROR:
                binding.progressBar.setVisibility(View.GONE);
                Mixpanel.updateProfilePic("Profile pic updated successfully");
                break;

        }
    }

    private void updateUiForUpdatePassword(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;
            case IN_PROCESS:
                showProgressDialog();
                break;
            case SUCCESS:
                closeProgressDialog();
                if (position != -1 && profileSettingsAdapter != null)
                    profileSettingsAdapter.deleteSubItem(position - 1);

                Mixpanel.updatePassword("SUCCESS");
                break;
            case FAIL:
                closeProgressDialog();

                Mixpanel.updatePassword("FAILURE");
                break;
            case ERROR:
                closeProgressDialog();

                Mixpanel.updatePassword("FAILURE");
                break;
        }
    }

    private void updateUiForUpdateMobile(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;
            case IN_PROCESS:
                showProgressDialog();
                break;
            case SUCCESS:
                closeProgressDialog();
                if (position != -1 && profileSettingsAdapter != null)
                    profileSettingsAdapter.updateUiMobileNumber(true, position);

                binding.recyclerView.scrollToPosition(position);
                break;
            case FAIL:
                closeProgressDialog();
                break;
            case ERROR:
                closeProgressDialog();
                break;
        }
    }

    private void updateUiForVerifyOTP(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;
            case IN_PROCESS:
                showProgressDialog();
                break;
            case SUCCESS:
                closeProgressDialog();
                if (networkCall.getBundle() != null) {
                    String mobile = networkCall.getBundle().getString("Mobile");
                    user.getCredentials().setMobile(mobile);

                    Mixpanel.updateMobileNumber(mobile, "SUCCESS");

                    if (position != -1 && profileSettingsAdapter != null) {
                        profileSettingsAdapter.updateData(user);
                        profileSettingsAdapter.updateValue("+91 " + mobile, position - 1);
                        profileSettingsAdapter.deleteSubItem(position - 1);
                        profileSettingsAdapter.isAllow = false;
                    }
                }
                break;
            case FAIL:
                closeProgressDialog();
                Mixpanel.updateMobileNumber(mobileNumberMixpanel, "FAILURE");
                break;
            case ERROR:
                closeProgressDialog();
                Mixpanel.updateMobileNumber(mobileNumberMixpanel, "FAILURE");
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onProfileSettingsClick(MenuItem menuItem, int position) {
        if (menuItem == null)
            return;
        if (!menuItem.isOpen() && menuItem.isExpand()) {
            profileSettingsAdapter.addSubItem(position);
        } else if (menuItem.isOpen() && menuItem.isExpand()) {
            profileSettingsAdapter.deleteSubItem(position);
        } else {
            switch (menuItem.getId()) {
                case 2:
                    openDatePicker(position);
                    break;
            }
        }
    }

    private void openDatePicker(int position) {
        if (calendar == null)
            calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(activity(), (view, year, month, dayOfMonth) ->
        {
            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            user.getProfileInfo().setDateOfBirth(Utility.getNormalToUTCDate(dayOfMonth + "-" + (month + 1) + "-" + year + " 00:00:00"));
            profileSettingsAdapter.updateValue(Utility.getUTCDateToNormal(user.getProfileInfo().getDateOfBirth()), position);
            profileSettingsAdapter.updateData(user);
            profileSettingsAdapter.notifyItemChanged(position);
            updateUser(user);
            showSnackBar("Date of birth is updated", 1);

            Mixpanel.updateDOB(dayOfMonth + "-" + (month + 1) + "-" + year, "SUCCESS");

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        if (user.getProfileInfo().getDateOfBirth() != null && !user.getProfileInfo().getDateOfBirth().isEmpty()) {
            try {
                datePickerDialog.updateDate(Integer.parseInt(Utility.getUTCDateToYear(user.getProfileInfo().getDateOfBirth()))
                        , Integer.parseInt(Utility.getUTCDateToMonth(user.getProfileInfo().getDateOfBirth())) - 1
                        , Integer.parseInt(Utility.getUTCDateToDay(user.getProfileInfo().getDateOfBirth())));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    @Override
    public void onChangeProfilePic() {
        if (activity() instanceof HelperActivity) {
            Constants.GALLERY_LIMIT = 1;
            ((HelperActivity) activity()).openCustomGallery();
        }
    }

    @Override
    public void updateUserDetails(User user, int position) {
        this.user = user;
        updateUser(user);
    }

    @Override
    public void sendMessage(Message message) {
        showSnackBar(message.getMessage(), message.getType());
    }

    @Override
    public void changePassword(int position, String oldPassword, String password, String confirmPassword) {
        if (viewModel.doValidation(oldPassword, password, confirmPassword)) {
            this.position = position;
            viewModel.updatePassword(user, oldPassword, password);
        }
    }

    @Override
    public void requestUpdateMobileNumber(int position, String mobileNumber, String password) {
        if (viewModel.doValidationMobileUpdate(mobileNumber, password)) {
            this.position = position;
            viewModel.updateMobileNumber(mobileNumber, password);
        }
    }

    @Override
    public void verifyOtp(int position, String mobileNumber, String otp) {
        if (viewModel.doValidationOTP(otp)) {
            this.position = position;
            this.mobileNumberMixpanel = mobileNumber;
            viewModel.verifyOTP(mobileNumber, otp);
        }
    }


    public void updateProfilePic(List<GalleryFile> galleryFileList) {
        if (!viewModel.isNetworkAvailable()) {
            showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
            return;
        }
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.evictFromCache(galleryFileList.get(0).uri);

        Logger.d("FILE URI", galleryFileList.get(0).uri.getPath());
        galleryFile = galleryFileList.get(0);
        if (profileSettingsAdapter != null)
            profileSettingsAdapter.updateProfilePic(galleryFileList.get(0).uri);


        viewModel.uploadProfilePic(galleryFile);
    }

    @Override
    public void doPositiveAction(int id) {
        switch (id) {
            case 1:
                ((BaseActivity) activity()).doLogout(viewModel);
                break;
        }
    }

    @Override
    public void doNegativeAction() {

    }


    public void updateUser(User user) {
        Intent intent = new Intent(activity(), HelperService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_UPDATE_USER_PROFILE);
        intent.putExtra("User", user);
        activity().startService(intent);
    }


    @Override
    public void gotoPrivacyPolicies() {
//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, "Privacy Policies");
//        intent.putExtra("Url", Constants.UserConstants.PRIVACY_POLICIES_URL);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UserConstants.PRIVACY_POLICIES_URL));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }


}
