package com.frost.leap.components.store;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentStoreBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.catalogue.CatalogueViewModel;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.store.StoreViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.catalogue.model.SubCategoryModel;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreFragment extends BaseFragment implements IStoreAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentStoreBinding binding;
    private StoreViewModel viewModel;

    private List<SubCategoryModel> subCategoryModels = new ArrayList<>();
    private StoreAdapter adapter;

    private String catalogueId;
    private int position;

    public StoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StoreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StoreFragment newInstance(String param1, String param2) {
        StoreFragment fragment = new StoreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(StoreViewModel.class);
        if (getArguments() != null) {
            catalogueId = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_store;
    }

    @Override
    public void setUp() {

        binding = (FragmentStoreBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);

        binding.swipeRefreshLayout.setOnRefreshListener(() -> {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchStoreData();
        });

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setNestedScrollingEnabled(false);

        ((SimpleItemAnimator) binding.recyclerView.getItemAnimator()).setChangeDuration(0);

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case CatalogueViewModel.NetworkTags.CATALOGUE_LIST:
                    updateUIForStoreCatalogueList(networkCall);
                    break;
            }

        });

        viewModel.updateStoreData().observe(this, list -> {
            binding.swipeRefreshLayout.setEnabled(false);
            this.subCategoryModels = list;

            binding.recyclerView.setFocusable(false);
            binding.recyclerView.setAdapter(adapter = new StoreAdapter(this, activity(), RController.DONE, subCategoryModels, viewModel.getStoreBanner()));
            binding.recyclerView.setHasFixedSize(true);

            if (catalogueId != null)
                redirectToCatalogue(catalogueId);
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });


        /**
         * get the List of Store Items from server
         */

    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.updateBanner(viewModel.getStoreBanner());
        } else {
            if (subCategoryModels.size() == 0)
                viewModel.fetchStoreData();
        }
    }

    private void updateUIForStoreCatalogueList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;

            case IN_PROCESS:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.recyclerView.setAdapter(adapter = new StoreAdapter(this, activity(), RController.LOADING, Arrays.asList(null, null), null));
                break;

            case SUCCESS:
                break;

            case DONE:
                break;

            case NO_DATA:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), "No catalogues", R.drawable.no_catalog_image_light, 0));
                break;

            case ERROR:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_wifi_signal, 0));
                break;

            case SERVER_ERROR:
                binding.swipeRefreshLayout.setEnabled(true);
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG_SERVER, R.drawable.ic_server_issue, 0));
                break;

            case UNAUTHORIZED:
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void clickToCatalogueOverview(CatalogueModel catalogueModel, int position, String subCategoryName) {

        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.STORE_OVERVIEW);
        intent.putExtra("CATALOGUE_OVERVIEW", catalogueModel.getCatalogueName() != null ? catalogueModel.getCatalogueName() : "Catalogue Overview");
        ShareDataManager.getInstance().getStoreOverviewData().setCatalogueModel(catalogueModel);
        activity().startActivityForResult(intent, Utility.generateRequestCodes().get("UPDATE_STORE_COURSE"));
        Mixpanel.clickStoreCourse(catalogueModel.getCatalogueName(), subCategoryName);

    }

    @Override
    public void enableMike() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        if (intent.resolveActivity(activity().getPackageManager()) != null) {
            startActivityForResult(intent, Utility.generateRequestCodes().get("REQUEST_MIKE_SEARCH"));
        } else {
            Utility.showToast(activity(), "Your Device Don't Support Speech Input", false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void redirectToCatalogue(String catalogueId) {

        Dialog dialog = Utility.generateProgressDialog(activity());

        for (int i = 0; i < subCategoryModels.size(); i++) {
            for (int j = 0; j < subCategoryModels.get(i).getCatalogues().size(); j++) {
                if (catalogueId.equals(subCategoryModels.get(i).getCatalogues().get(j).getCatalogueId())) {
                    clickToCatalogueOverview(subCategoryModels.get(i).getCatalogues().get(j), j, subCategoryModels.get(i).getSubCategoryName());
                    Utility.closeProgressDialog(dialog);
                    break;
                }
            }
        }
        Utility.closeProgressDialog(dialog);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Utility.generateRequestCodes().get("REQUEST_MIKE_SEARCH")) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                Logger.d("Voice Text", result.get(0));
                if (result.get(0) != null) {
                    adapter.setMikeText(result.get(0));
                }
            }
        }
    }
}
