package com.frost.leap.components.subject.resources;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemResourceBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.subject.model.ResourceModel;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/17/2019.
 */
public class ResourceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private RController rController;
    private List<ResourceModel> list;
    private IResourceAdapter iResourceAdapter;

    public ResourceAdapter(Context context, RController rController, List<ResourceModel> list, IResourceAdapter iResourceAdapter) {
        this.context = context;
        this.rController = rController;
        this.list = list;
        this.iResourceAdapter = iResourceAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skeleton_item_invoice, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else
            return new ResourcesViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_resource, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ResourcesViewHolder) {
            ResourcesViewHolder resourcesViewHolder = (ResourcesViewHolder) holder;

            resourcesViewHolder.bindData(position);
            resourcesViewHolder.itemView.setOnClickListener(v -> {
                iResourceAdapter.viewResource(list.get(position), position);
            });
        }
    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING ? 15 : list.size();
    }

    private class ResourcesViewHolder extends RecyclerView.ViewHolder {

        private ItemResourceBinding binding;

        public ResourcesViewHolder(ItemResourceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(int position) {

            if (list.get(position).getResourceName() != null && !list.get(position).getResourceName().isEmpty())
                binding.tvResourceName.setText(list.get(position).getResourceName());

            if (list.get(position).getResourceDate() != null && !list.get(position).getResourceDate().isEmpty())
                binding.tvResourceDate.setText(list.get(position).getResourceDate());

            if (list.get(position).getSize() != null && list.get(position).getSize() > 0) {
                binding.tvResourceSize.setVisibility(View.VISIBLE);
                binding.tvResourceSize.setText("" + list.get(position).getSize() + " MB");
            }
            else {
                binding.tvResourceSize.setVisibility(View.GONE);
                binding.tvResourceSize.setText("");
            }

            if (list.get(position).getResourceUrl() != null && !list.get(position).getResourceUrl().isEmpty()) {

                binding.ivResourceType.setText(Utility.getFileType(list.get(position).getResourceUrl()));

                switch (Utility.getFileType(list.get(position).getResourceUrl())) {
                    case "pdf":
                        binding.ivResourceType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.corner_radius_red_3));
                        binding.ivResourceType.setTextColor(ContextCompat.getColor(context, R.color.white));
                        break;
                    case "doc":
                        binding.ivResourceType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.corner_radius_blue_3));
                        binding.ivResourceType.setTextColor(ContextCompat.getColor(context, R.color.white));
                        break;
                    case "zip":
                        binding.ivResourceType.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.corner_radius_gray_3));
                        binding.ivResourceType.setTextColor(ContextCompat.getColor(context, R.color.normal_text_color));
                        break;
                }
            }


            binding.imgDownload.setOnClickListener(v -> {
                iResourceAdapter.downloadResource(list.get(position), position);
            });


        }
    }
}
