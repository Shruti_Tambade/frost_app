package com.frost.leap.components.videoplayer;


import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.frost.leap.R;
import com.frost.leap.databinding.FragmentBsdvideoActionBinding;
import com.frost.leap.interfaces.IFragment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;

import apprepos.topics.TopicRepositoryManager;
import apprepos.user.UserRepositoryManager;
import supporters.constants.Constants;
import supporters.constants.PlayBackSpeed;
import supporters.constants.VideoQuality;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BSDVideoActionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BSDVideoActionFragment extends BottomSheetDialogFragment implements IFragment {

    public static final String TAG = "BSDVideoActionFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FragmentBsdvideoActionBinding bsdvideoActionBinding;

    private PlayBackSpeed playBackSpeed;
    private VideoQuality videoQuality;
    private ISpeedController iSpeedController;
    private IVideoResolutionController iVideoResolutionController;

    private UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();


    public BSDVideoActionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BSDVideoActionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BSDVideoActionFragment newInstance(String param1, String param2) {
        BSDVideoActionFragment fragment = new BSDVideoActionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static BSDVideoActionFragment newInstance(PlayBackSpeed playBackSpeed, ISpeedController iSpeedController, VideoQuality videoQuality, IVideoResolutionController iVideoResolutionController) {
        BSDVideoActionFragment fragment = new BSDVideoActionFragment();
        fragment.iSpeedController = iSpeedController;
        fragment.iVideoResolutionController = iVideoResolutionController;
        Bundle args = new Bundle();
        args.putInt("PlayBackSpeed", playBackSpeed.ordinal());
        args.putInt("VideoResolution", videoQuality.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
        if (getArguments() != null) {
            playBackSpeed = PlayBackSpeed.values()[getArguments().getInt("PlayBackSpeed")];
            videoQuality = VideoQuality.values()[getArguments().getInt("VideoResolution")];
        }
    }


//    @Override
//    public int getTheme() {
//        return R.style.BaseBottomSheetDialog;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        bsdvideoActionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_bsdvideo_action, container, false);
        setUp();

        return bsdvideoActionBinding.getRoot();

    }

    private void setUp() {

        activity().overridePendingTransition(R.anim.slide_up, R.anim.slide_down);

        bsdvideoActionBinding.chipAuto.setText("Auto (" + TopicRepositoryManager.getInstance().getAutoResolutions() + "p)");

        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();


        switch (playBackSpeed) {
            case SPp25X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp1, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                        , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2);
                break;

            case SPp5X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                        , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp1);
                break;

            case SPp75X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                        , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);
                break;

            case SP1X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                        , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);
                break;

            case SP1p25X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                        , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);
                break;

            case SP1p5X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp6, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp5
                        , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);
                break;

            case SP1p75X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp6, bsdvideoActionBinding.chipSp5
                        , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);
                break;

            case SP2X:
                setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6, bsdvideoActionBinding.chipSp5
                        , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2
                        , bsdvideoActionBinding.chipSp1);
                break;

        }

        switch (videoQuality) {

            case AUTO:
                selectedVideoQualityItem(bsdvideoActionBinding.chipAuto, bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                        , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chip1080);

                //iVideoResolutionController.setVideoQuality(VideoQuality.AUTO, 0);
                break;

            case RS1080:
                selectedVideoQualityItem(bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                        , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);


                //iVideoResolutionController.setVideoQuality(VideoQuality.RS1080, Utility.getBitrate(1080));
                break;

            case RS720:
                selectedVideoQualityItem(bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip540
                        , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

                //iVideoResolutionController.setVideoQuality(VideoQuality.RS720, Utility.getBitrate(720));
                break;

            case RS540:
                selectedVideoQualityItem(bsdvideoActionBinding.chip540, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip360
                        , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

                // iVideoResolutionController.setVideoQuality(VideoQuality.RS540, Utility.getBitrate(480));
                break;

            case RS360:
                selectedVideoQualityItem(bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                        , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

                //iVideoResolutionController.setVideoQuality(VideoQuality.RS360, Utility.getBitrate(360));
                break;

            case RS270:
                selectedVideoQualityItem(bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                        , bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

                // iVideoResolutionController.setVideoQuality(VideoQuality.RS240, Utility.getBitrate(240));
                break;

            case RS144:
                selectedVideoQualityItem(bsdvideoActionBinding.chip144, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                        , bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip270, bsdvideoActionBinding.chipAuto);

                // iVideoResolutionController.setVideoQuality(VideoQuality.RS144, Utility.getBitrate(144));
                break;
        }


        bsdvideoActionBinding.chipSp1.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp1, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                    , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SPp25X);
            this.dismiss();

            decorateScreenUi();
        });

        bsdvideoActionBinding.chipSp2.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                    , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SPp5X);
            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chipSp3.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                    , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SPp75X);
            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chipSp4.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                    , bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SP1X);
            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chipSp5.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp5, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6
                    , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SP1p25X);
            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chipSp6.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp6, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp5
                    , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SP1p5X);
            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chipSp7.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp6, bsdvideoActionBinding.chipSp5
                    , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2, bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SP1p75X);
            this.dismiss();

            decorateScreenUi();
        });

        bsdvideoActionBinding.chipSp8.setOnClickListener(v -> {

            setlectedVideoSpeedItem(bsdvideoActionBinding.chipSp8, bsdvideoActionBinding.chipSp7, bsdvideoActionBinding.chipSp6, bsdvideoActionBinding.chipSp5
                    , bsdvideoActionBinding.chipSp4, bsdvideoActionBinding.chipSp3, bsdvideoActionBinding.chipSp2
                    , bsdvideoActionBinding.chipSp1);

            iSpeedController.setPlaybackSpeed(PlayBackSpeed.SP2X);
            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chipAuto.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chipAuto, bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                    , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chip1080);

            iVideoResolutionController.setVideoQuality(VideoQuality.AUTO, 0, 0);

            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chip1080.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                    , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

            userRepositoryManager.appSharedPreferences.setString(Constants.RESOLUTION, "1080");
            iVideoResolutionController.setVideoQuality(VideoQuality.RS1080, Utility.getBitrate(1080), 1080);

            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chip720.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip540
                    , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

            userRepositoryManager.appSharedPreferences.setString(Constants.RESOLUTION, "720");
            iVideoResolutionController.setVideoQuality(VideoQuality.RS720, Utility.getBitrate(720), 720);

            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chip540.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chip540, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip360
                    , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

            userRepositoryManager.appSharedPreferences.setString(Constants.RESOLUTION, "540");
            iVideoResolutionController.setVideoQuality(VideoQuality.RS540, Utility.getBitrate(540), 540);

            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chip360.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                    , bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

            userRepositoryManager.appSharedPreferences.setString(Constants.RESOLUTION, "360");
            iVideoResolutionController.setVideoQuality(VideoQuality.RS360, Utility.getBitrate(360), 360);

            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chip270.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chip270, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                    , bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip144, bsdvideoActionBinding.chipAuto);

            userRepositoryManager.appSharedPreferences.setString(Constants.RESOLUTION, "270");
            iVideoResolutionController.setVideoQuality(VideoQuality.RS270, Utility.getBitrate(270), 270);

            this.dismiss();

            decorateScreenUi();

        });

        bsdvideoActionBinding.chip144.setOnClickListener(v -> {

            selectedVideoQualityItem(bsdvideoActionBinding.chip144, bsdvideoActionBinding.chip1080, bsdvideoActionBinding.chip720, bsdvideoActionBinding.chip540
                    , bsdvideoActionBinding.chip360, bsdvideoActionBinding.chip270, bsdvideoActionBinding.chipAuto);

            userRepositoryManager.appSharedPreferences.setString(Constants.RESOLUTION, "144");
            iVideoResolutionController.setVideoQuality(VideoQuality.RS144, Utility.getBitrate(144), 144);

            this.dismiss();

            decorateScreenUi();
        });
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {

    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    private void setlectedVideoSpeedItem(Chip chip, Chip chip1, Chip chip2, Chip chip3, Chip chip4, Chip chip5, Chip chip6
            , Chip chip7) {

        chip.setChipIconResource(R.drawable.ic_check_circle);
        chip.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_1));
        chip1.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip2.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip3.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip4.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip5.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip6.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip7.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));


       /* chip.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip1.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip2.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip3.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip4.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip5.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip6.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip7.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));*/

      /*  chip.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.text_color_1)));
        chip1.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_1)));
        chip2.setChipBackgroundColor(generateColorStateList(Utility.getColorsFromAttrs(activity(),R.attr.card_background_color_2)));*/


        chip.setChipIconVisible(true);
        chip1.setChipIconVisible(false);
        chip2.setChipIconVisible(false);
        chip3.setChipIconVisible(false);
        chip4.setChipIconVisible(false);
        chip5.setChipIconVisible(false);
        chip6.setChipIconVisible(false);
        chip7.setChipIconVisible(false);
    }

    private void selectedVideoQualityItem(Chip chip, Chip chip1, Chip chip2, Chip chip3, Chip chip4, Chip chip5, Chip chip6) {

        chip.setChipIconResource(R.drawable.ic_check_circle);
        chip.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_1));
        chip1.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip2.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip3.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip4.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip5.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));
        chip6.setTextColor(Utility.getColorsFromAttrs(activity(), R.attr.text_color_2));


       /* chip.setChipBackgroundColor(generateColorStateList(R.color.light_color_primary));
        chip1.setChipBackgroundColor(generateColorStateList(R.color.app_background_color));
        chip2.setChipBackgroundColor(generateColorStateList(R.color.app_background_color));
        chip3.setChipBackgroundColor(generateColorStateList(R.color.app_background_color));
        chip4.setChipBackgroundColor(generateColorStateList(R.color.app_background_color));
        chip5.setChipBackgroundColor(generateColorStateList(R.color.app_background_color));
        chip6.setChipBackgroundColor(generateColorStateList(R.color.app_background_color));*/

        chip.setChipIconVisible(true);
        chip1.setChipIconVisible(false);
        chip2.setChipIconVisible(false);
        chip3.setChipIconVisible(false);
        chip4.setChipIconVisible(false);
        chip5.setChipIconVisible(false);
        chip6.setChipIconVisible(false);

    }

    public void decorateScreenUi() {

        if (getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || getActivity().getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                View decorView = activity().getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                View decorView = activity().getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        }


    }


    public ColorStateList generateColorStateList(int color) {
        int[][] states = new int[][]{

                new int[]{android.R.attr.state_checked}, // disabled
                new int[]{-android.R.attr.state_checked}, // unchecked
                new int[]{android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[]{
                Utility.getColorsFromAttrs(activity(), color),
                Utility.getColorsFromAttrs(activity(), color),
                Utility.getColorsFromAttrs(activity(), color),
        };

        return new ColorStateList(states, colors);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        decorateScreenUi();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = new BottomSheetDialog(requireContext(), getTheme());
        if (dialog == null)
            return super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(bottomSheetDialog -> {
            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            // Right here!
            BottomSheetBehavior.from(bottomSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        return dialog;
    }

}
