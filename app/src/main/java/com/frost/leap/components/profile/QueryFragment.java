package com.frost.leap.components.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentQueryBinding;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link QueryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QueryFragment extends BaseFragment implements IQueryAdpater {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String title;
    private String mParam2;

    private FragmentQueryBinding binding;
    private QueryAdapter adapter;

    private List<MenuItem> list = new ArrayList<>();
    private String type = "";

    public QueryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QueryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QueryFragment newInstance(String param1, String param2) {
        QueryFragment fragment = new QueryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_query;
    }

    @Override
    public void setUp() {
        binding = (FragmentQueryBinding) getBaseDataBinding();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));

        binding.recyclerView.getItemAnimator().setChangeDuration(0);
        binding.recyclerView.setNestedScrollingEnabled(false);

        if (title.equals("Help - Subsciption Related"))
            list = Utility.getSubscriptionRelatedItems();
        else if (title.equals("Help - Online Test Series"))
            list = Utility.getOnlineTestSeries();
        else if (title.equals("Help - Study Material"))
            list = Utility.getStudyMaterial();
        else if (title.equals("Help - Device and Region Compatibility"))
            list = Utility.getDeviceandRegionCompatibility();
        else if (title.equals("Help - General Issues"))
            list = Utility.getContentGeneralQueries();
        else if (title.equals("Help - Reporting Content Issues"))
            list = Utility.getReportingContentIssues();
        else if (title.equals("Help - New Content Requests"))
            list = Utility.getNewContentRequests();
        else if (title.equals("Help - Payment General Queries"))
            list = Utility.getPaymentGeneralQueries();
        else if (title.equals("Help - Payment Issues"))
            list = Utility.getPaymentIssues();
        else if (title.equals("Help - General Queries"))
            list = Utility.getTechnicalGeneralIssues();
        else if (title.equals("Help - Video Player and Streaming Related"))
            list = Utility.getVideoPlayerStreamingRelated();
        else if (title.equals("Help - Feature Requests"))
            list = Utility.getFeatureRequests();
        else if (title.equals("Help - New Course Requests"))
            list = Utility.getNewCourseRequests();
        else if (title.equals("Phone Support")) {
            list = Utility.getPhoneDetails();
            type = "Phone";
        }

//        Help - Online Test Series
        binding.recyclerView.setAdapter(adapter = new QueryAdapter(activity(), list, this, type));

    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onMenuClick(MenuItem menuItem, int position, String clickType) {
        if (menuItem == null)
            return;

        Intent intent = new Intent(activity(), HelperActivity.class);

        switch (menuItem.getId()) {
            case 2:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 4:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 7:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 8:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 16:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 31:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 32:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 37:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 38:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 39:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 41:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 42:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 43:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 44:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 45:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 46:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 48:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", false);
                startActivity(intent);
                break;
            case 50:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 51:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 52:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 53:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 54:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 55:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 56:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 57:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 58:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 59:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 60:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 61:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 62:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 63:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 64:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 65:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 66:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 67:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 68:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 69:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 70:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 71:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
            case 72:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.QUERY_PAGE);
                intent.putExtra("TITLE_NAME", "Phone Support");
                startActivity(intent);
                break;
            case 73:
                intent.putExtra(Constants.FRAGMENT_KEY, AppController.SUBMIT_REQUEST);
                intent.putExtra("Issue_Item", menuItem);
                intent.putExtra("Optional", true);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void clickContactUs(MenuItem menuItem, int position) {

    }

    @Override
    public void clickforDescription(MenuItem menuItem, int position) {

        menuItem.setExpand(!menuItem.isExpand());
//        adapter.notifyItemChanged(position);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        }, 200);

    }

    @Override
    public void clicktoCall(MenuItem menuItem, int position) {

        try {
            String phone = menuItem.getTitle();
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void redirecttoOTSPage(MenuItem menuItem, int position) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.OTS_LOGIN_URL));
        startActivity(browserIntent);
    }

    @Override
    public void redirecttoOTSAppPage(MenuItem menuItem, int position) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.OTS_APP_URL));
        startActivity(browserIntent);
    }

    @Override
    public void redirecttoACEWEBPage(MenuItem menuItem, int position) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.ACE_WEBSITE_URL));
        startActivity(browserIntent);
    }

    @Override
    public void redirectTOPDFPage(String pdfFile, String title) {

//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, title);
//        intent.putExtra("Url", pdfFile);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(pdfFile), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(pdfFile), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdfFile));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }

    }
}
