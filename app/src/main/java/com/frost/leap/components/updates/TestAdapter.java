package com.frost.leap.components.updates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemTestBinding;

import java.util.List;

import apprepos.updates.model.TestList;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 11/05/202.
 */
public class TestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private IUpdateAdapter iUpdateAdapter;
    private List<TestList> lists;

    public TestAdapter(Context context, IUpdateAdapter iUpdateAdapter, List<TestList> lists) {
        this.lists = lists;
        this.context = context;
        this.iUpdateAdapter = iUpdateAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TestAdapterViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.item_test, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof TestAdapterViewHolder) {
            TestAdapterViewHolder testAdapterViewHolder = (TestAdapterViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, position == 0 ? 0 : 15),
                    Utility.dpSize(context, 0),
                    Utility.dpSize(context, 0));
            testAdapterViewHolder.itemView.setLayoutParams(params);

            testAdapterViewHolder.bindData(position, lists.get(position));

        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }

    public class TestAdapterViewHolder extends RecyclerView.ViewHolder {

        private ItemTestBinding binding;

        public TestAdapterViewHolder(ItemTestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(int position, TestList testList) {

            if (testList.getQuestionType() != null && !testList.getQuestionType().isEmpty())
                binding.tvQuestionType.setText("" + testList.getQuestionType());

            if (testList.getNoOfQuestions() != null)
                binding.tvQuestions.setText("" + testList.getNoOfQuestions());

            if (testList.getTotalMarks() != null)
                binding.tvTotalMarks.setText("" + testList.getTotalMarks());
        }
    }
}
