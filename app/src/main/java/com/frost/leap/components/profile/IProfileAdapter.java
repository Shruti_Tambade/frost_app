package com.frost.leap.components.profile;

import com.frost.leap.components.profile.models.MenuItem;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-09-2019.
 * <p>
 * Frost
 */
public interface IProfileAdapter {
    public void onProfileClick();
    public void onMenuItemClick(MenuItem menuItem, int position);
}
