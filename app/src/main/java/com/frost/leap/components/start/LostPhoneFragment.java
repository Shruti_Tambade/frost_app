package com.frost.leap.components.start;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.frost.leap.R;
import com.frost.leap.activities.SplashActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentLostPhoneBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.start.LostMobileViewModel;

import supporters.constants.Constants;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LostPhoneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LostPhoneFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentLostPhoneBinding binding;
    private LostMobileViewModel viewModel;
    private CountDownTimer countDownTimer;
    private boolean isResend = false;
    private TextView[] textViews;
    private TextView[] textViews1;

    public LostPhoneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.etPhoneNumber
     * @return A new instance of fragment LostPhoneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LostPhoneFragment newInstance(String param1, String param2) {
        LostPhoneFragment fragment = new LostPhoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(LostMobileViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_lost_phone;
    }


    @Override
    public void setUp() {
        binding = (FragmentLostPhoneBinding) getBaseDataBinding();
        setUpSnackBarView(binding.constraintLayout);
        textViews = new TextView[]{binding.textView1, binding.textView2, binding.textView3, binding.textView4, binding.textView5, binding.textView6};
        textViews1 = new TextView[]{binding.textView11, binding.textView22, binding.textView33, binding.textView44, binding.textView55, binding.textView66};


        binding.imgClose.setOnClickListener(v -> {
            activity().onBackPressed();
        });


        binding.tvResendOtp.setOnClickListener(v -> {
            countDown();
            isResend = true;
            viewModel.resentEmailOtp(binding.etEmailAddress.getText().toString());
        });


        binding.baseEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable value) {
                updateUI(value.toString());
            }
        });


        binding.etEmailAddress.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utility.hideKeyboard(activity());

                if (viewModel.doEmailValidation(binding.etEmailAddress.getText().toString())) {
                    viewModel.requestLostPhone();
                }
                return true;
            }
            return false;
        });

        ObjectAnimator.ofFloat(binding.llEmailOtp, View.TRANSLATION_X, 1000).start();
        ObjectAnimator.ofFloat(binding.llAlernativeMobile, View.TRANSLATION_X, 1000).start();
        ObjectAnimator.ofFloat(binding.llMobileOtp, View.TRANSLATION_X, 1000).start();


        binding.tvSignUp.setOnClickListener(v -> {
            if (activity() instanceof SplashActivity) {
                ((SplashActivity) activity()).clearAndGotoSignUp();
            }
        });


        binding.tvSubmit.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            if (viewModel.doEmailValidation(binding.etEmailAddress.getText().toString())) {
                viewModel.requestLostPhone();
            }
        });


        binding.tvVerify.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            if (viewModel.doOTPValidation(binding.baseEditText.getText().toString())) {
                viewModel.verifyEmailOtp(binding.etEmailAddress.getText().toString());
            }
        });

        binding.etEmailAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlEmailAddress.setErrorEnabled(false);
                    binding.tlEmailAddress.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etPhoneNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlPhoneNumber.setErrorEnabled(false);
                    binding.tlPhoneNumber.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etPhoneNumber.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utility.hideKeyboard(activity());
                if (viewModel.doMobileValidation(binding.etPhoneNumber.getText().toString())) {
                    viewModel.requestAlternativeNumber();
                }

                return true;
            }
            return false;
        });


        binding.tvAlernativeSubmit.setOnClickListener(v ->

        {
            Utility.hideKeyboard(activity());
            if (viewModel.doMobileValidation(binding.etPhoneNumber.getText().toString())) {
                viewModel.requestAlternativeNumber();
            }

        });


        binding.baseEditText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable value) {
                updateUI1(value.toString());
            }
        });


        binding.tvResendOtp1.setOnClickListener(v ->

        {
            countDown1();
            isResend = true;
            viewModel.requestAlternativeNumberAgain();
        });


        binding.tvVerify1.setOnClickListener(v ->

        {
            Utility.hideKeyboard(activity());
            if (viewModel.doOTPValidation(binding.baseEditText1.getText().toString())) {
                viewModel.verifyMobileOtp();
            }
        });


        viewModel.getMessage().

                observe(this, message ->

                {
                    showSnackBar(message.getMessage(), message.getType());
                });


        viewModel.getNetworkCallStatus().

                observe(this, networkCall ->

                {
                    switch (networkCall.getNetworkTag()) {
                        case LostMobileViewModel.NetworkTag.REQUEST_LOST_MOBILE:
                            updateUiForRequestLostMobile(networkCall);
                            break;

                        case LostMobileViewModel.NetworkTag.RESEND_EMAIL_OTP:
                            updateUiForResendOtp(networkCall);
                            break;

                        case LostMobileViewModel.NetworkTag.VERIFY_EMAIL_OTP:
                            updateUiForEmailVerify(networkCall);
                            break;
                        case LostMobileViewModel.NetworkTag.REQUEST_ALTERNATIVE_NUMBER:
                            updateUiForAlternativeNumber(networkCall);
                            break;

                        case LostMobileViewModel.NetworkTag.REQUEST_ALTERNATIVE_NUMBER_AGAIN:
                            updateUiForResendOtp(networkCall);
                            break;

                        case LostMobileViewModel.NetworkTag.VERIFY_MOBILE_OTP:
                            updateUiForMobileOtpverify(networkCall);
                            break;
                    }
                });


        viewModel.getError().observe(this, error -> {
            if (error == null)
                return;

            switch (error.getId()) {
                case R.id.tlEmailAddress:
                    binding.tlEmailAddress.setError(error.getMessage());
                    binding.etEmailAddress.requestFocus();
                    binding.tlEmailAddress.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));
                    break;

                case R.id.tlPhoneNumber:
                    binding.tlPhoneNumber.setError(error.getMessage());
                    binding.etPhoneNumber.requestFocus();
                    binding.tlPhoneNumber.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));
                    break;
            }
        });

    }


    @Override
    public void changeTitle(String title) {

    }


    private void gotoVerifyEmailOtp() {
        binding.etEmailAddress.setEnabled(false);
        binding.etEmailAddress.setFocusable(false);
        binding.etEmailAddress.setFocusableInTouchMode(false);
        binding.llEmailRequest.setAlpha(0.5f);
        binding.llEmailOtp.setVisibility(View.VISIBLE);
        ObjectAnimator.ofFloat(binding.llEmailRequest, View.TRANSLATION_X, -1000).setDuration(1000).start();
        ObjectAnimator.ofFloat(binding.llEmailOtp, View.TRANSLATION_X, 0).setDuration(1000).start();
        String data = binding.etEmailAddress.getText().toString();
        final SpannableString text = new SpannableString("Please enter the verification code sent to " + data);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.black)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvHeading.setText(text);

    }


    public void countDown() {
        binding.tvResendOtp.setEnabled(false);
        binding.tvResendOtp.setTextColor(activity().getResources().getColor(R.color.light_gray));
        countDownTimer = new CountDownTimer(Constants.COUNT_DOWN_TIME, Constants.COUNT_DOWN_INTERVAL) {

            public void onTick(long millisUntilFinished) {
                binding.timer.setVisibility(View.VISIBLE);

                binding.timer.setText("00 : " +
                        ((millisUntilFinished / 1000) < 10
                                ? "0" + (millisUntilFinished / 1000)
                                : millisUntilFinished / 1000));
            }

            public void onFinish() {
                binding.tvResendOtp.setTextColor(ContextCompat.getColor(activity(), R.color.charcoal_gray));
                binding.tvResendOtp.setEnabled(true);
                binding.timer.setVisibility(View.GONE);
            }

        }.start();
    }


    public void countDown1() {
        binding.tvResendOtp1.setEnabled(false);
        binding.tvResendOtp1.setTextColor(activity().getResources().getColor(R.color.light_gray));
        countDownTimer = new CountDownTimer(Constants.COUNT_DOWN_TIME, Constants.COUNT_DOWN_INTERVAL) {

            public void onTick(long millisUntilFinished) {
                binding.timer1.setVisibility(View.VISIBLE);

                binding.timer1.setText("00 : " +
                        ((millisUntilFinished / 1000) < 10
                                ? "0" + (millisUntilFinished / 1000)
                                : millisUntilFinished / 1000));
            }

            public void onFinish() {
                binding.tvResendOtp1.setTextColor(ContextCompat.getColor(activity(), R.color.charcoal_gray));
                binding.tvResendOtp1.setEnabled(true);
                binding.timer1.setVisibility(View.GONE);
            }

        }.start();
    }

    public void updateUI(final String value) {
        if (value.isEmpty()) {
            for (int i = 0; i < textViews.length; i++) {
                textViews[i].setText(" ");
                textViews[i].setBackgroundResource(R.drawable.ic_tv_light_bottom);
            }
            return;
        } else {
            for (int i = 0; i < textViews.length; i++) {
                textViews[i].setText(value.length() > i ? "" + value.charAt(i) : " ");
                textViews[i].setBackgroundResource(value.length() > i ? R.drawable.ic_tv_dark_bottom : R.drawable.ic_tv_light_bottom);

            }
        }
    }


    public void updateUI1(final String value) {
        if (value.isEmpty()) {
            for (int i = 0; i < textViews1.length; i++) {
                textViews1[i].setText(" ");
                textViews1[i].setBackgroundResource(R.drawable.ic_tv_light_bottom);
            }
            return;
        } else {
            for (int i = 0; i < textViews1.length; i++) {
                textViews1[i].setText(value.length() > i ? "" + value.charAt(i) : " ");
                textViews1[i].setBackgroundResource(value.length() > i ? R.drawable.ic_tv_dark_bottom : R.drawable.ic_tv_light_bottom);

            }
        }
    }


    private void updateUiForRequestLostMobile(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);
                break;
            case IN_PROCESS:
                binding.llLoader.setVisibility(View.VISIBLE);
                binding.tvSubmit.setVisibility(View.GONE);
                break;
            case SUCCESS:
                gotoVerifyEmailOtp();
                binding.rlEmailRequest.setVisibility(View.GONE);
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.GONE);

                Mixpanel.recoveryEmailOTPSent(binding.etEmailAddress.getText().toString(), "Success");
                break;
            case FAIL:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);

                Mixpanel.recoveryEmailOTPSent(binding.etEmailAddress.getText().toString(), "Fail to sent OTP");
                break;
            case ERROR:
                binding.llLoader.setVisibility(View.GONE);
                binding.tvSubmit.setVisibility(View.VISIBLE);

                Mixpanel.recoveryEmailOTPSent(binding.etEmailAddress.getText().toString(), "Fail to sent OTP");
                break;
        }
    }

    private void updateUiForResendOtp(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case IN_PROCESS:
                break;
            case SUCCESS:
                showSnackBar("OTP resent successfully", 1);

                if (binding.etPhoneNumber.getText().toString() == null || binding.etPhoneNumber.getText().toString().isEmpty())
                    Mixpanel.verificationOTPResendRecovery(binding.etEmailAddress.getText().toString(), "OTP resent successfully");
                else
                    Mixpanel.mobileverificationOTPResendRecovery(binding.etEmailAddress.getText().toString()
                            , binding.etEmailAddress.getText().toString(), "OTP resent successfully");
                break;

            case FAIL:
            case ERROR:
                if (binding.etPhoneNumber.getText().toString() == null || binding.etPhoneNumber.getText().toString().isEmpty())
                    Mixpanel.verificationOTPResendRecovery(binding.etEmailAddress.getText().toString(), "OTP resend Failure");
                else
                    Mixpanel.mobileverificationOTPResendRecovery(binding.etEmailAddress.getText().toString()
                            , binding.etEmailAddress.getText().toString(), "OTP resend Failure");
                break;
        }
    }

    private void updateUiForEmailVerify(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoaderVerifyOtp.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoaderVerifyOtp.setVisibility(View.VISIBLE);
                binding.tvVerify.setVisibility(View.GONE);
                break;

            case SUCCESS:
                binding.llLoaderVerifyOtp.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                gotoAlternativeNumber();
                Mixpanel.recoveryEmailOTPVerfication(binding.etEmailAddress.getText().toString(), "OTP Verfication Success");
                break;

            case FAIL:
                binding.llLoaderVerifyOtp.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                break;

            case ERROR:
                binding.llLoaderVerifyOtp.setVisibility(View.GONE);
                binding.tvVerify.setVisibility(View.VISIBLE);
                Mixpanel.recoveryEmailOTPVerfication(binding.etEmailAddress.getText().toString(), "Entered Invalid OTP");
                break;
        }
    }

    private void updateUiForAlternativeNumber(NetworkCall networkCall) {

        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoaderAlernativeNumber.setVisibility(View.GONE);
                binding.tvAlernativeSubmit.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoaderAlernativeNumber.setVisibility(View.VISIBLE);
                binding.tvAlernativeSubmit.setVisibility(View.GONE);
                break;

            case SUCCESS:
                binding.llLoaderAlernativeNumber.setVisibility(View.GONE);
                binding.tvAlernativeSubmit.setVisibility(View.VISIBLE);
                gotoVerifyOTPAlternativeNumber();

                Mixpanel.recoveryMobileOTPSent(binding.etEmailAddress.getText().toString()
                        , binding.etPhoneNumber.getText().toString(), "Success");
                break;

            case FAIL:
            case ERROR:
                binding.llLoaderAlernativeNumber.setVisibility(View.GONE);
                binding.tvAlernativeSubmit.setVisibility(View.VISIBLE);

                Mixpanel.recoveryMobileOTPSent(binding.etEmailAddress.getText().toString()
                        , binding.etPhoneNumber.getText().toString(), "Fail to sent OTP");
                break;

        }
    }

    public void gotoAlternativeNumber() {

        if (countDownTimer != null)
            countDownTimer.cancel();

        binding.timer.setVisibility(View.GONE);
        binding.tvResendOtp.setVisibility(View.GONE);
        binding.rlLayout.setVisibility(View.GONE);
        binding.viewFooter.setVisibility(View.GONE);
        binding.viewFooter1.setVisibility(View.GONE);
        binding.llEmailRequest.setVisibility(View.GONE);
        binding.baseEditText.setEnabled(false);
        binding.baseEditText.setFocusable(false);
        binding.baseEditText.setFocusableInTouchMode(false);
        binding.llEmailOtp.setAlpha(0.5f);
        binding.llAlernativeMobile.setVisibility(View.VISIBLE);

        ObjectAnimator.ofFloat(binding.llEmailOtp, View.TRANSLATION_X, -1000).setDuration(1000).start();
        ObjectAnimator.ofFloat(binding.llAlernativeMobile, View.TRANSLATION_X, 0).setDuration(1000).start();
    }

    private void gotoVerifyOTPAlternativeNumber() {
        binding.llEmailOtp.setVisibility(View.GONE);
        binding.rlAlernativeNumberRequest.setVisibility(View.GONE);
        binding.llAlernativeMobile.setAlpha(0.5f);
        binding.etPhoneNumber.setEnabled(false);
        binding.etPhoneNumber.setFocusable(false);
        binding.etPhoneNumber.setFocusableInTouchMode(false);
        binding.viewFooter.setVisibility(View.GONE);
        binding.viewFooter1.setVisibility(View.GONE);
        binding.viewFooter2.setVisibility(View.GONE);
        binding.llMobileOtp.setVisibility(View.VISIBLE);

        String data = "";

        data = " " + "+91" + " " + binding.etPhoneNumber.getText().toString();

        final SpannableString text = new SpannableString("Please enter the verification code sent to " + data);
        text.setSpan(new RelativeSizeSpan(1.1f), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity(), R.color.black)), text.toString().lastIndexOf(data), text.toString().lastIndexOf(data) + data.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvHeading1.setText(text);

        ObjectAnimator.ofFloat(binding.llAlernativeMobile, View.TRANSLATION_X, -1000).setDuration(1000).start();
        ObjectAnimator.ofFloat(binding.llMobileOtp, View.TRANSLATION_X, 0).setDuration(1000).start();

    }


    private void updateUiForMobileOtpverify(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.llLoaderVerifyOtp1.setVisibility(View.GONE);
                binding.tvVerify1.setVisibility(View.VISIBLE);
                break;

            case IN_PROCESS:
                binding.llLoaderVerifyOtp1.setVisibility(View.VISIBLE);
                binding.tvVerify1.setVisibility(View.GONE);
                break;

            case SUCCESS:
                if (countDownTimer != null)
                    countDownTimer.cancel();

                binding.llLoaderVerifyOtp1.setVisibility(View.GONE);
                binding.tvVerify1.setVisibility(View.VISIBLE);
                gotoHome();

                Mixpanel.recoveryMobileOTPVerfication(binding.etEmailAddress.getText().toString()
                        , binding.etPhoneNumber.getText().toString(), "Alernative number is updated successfully");
                break;

            case FAIL:
                binding.llLoaderVerifyOtp1.setVisibility(View.GONE);
                binding.tvVerify1.setVisibility(View.VISIBLE);
                break;

            case ERROR:
                binding.llLoaderVerifyOtp1.setVisibility(View.GONE);
                binding.tvVerify1.setVisibility(View.VISIBLE);

                Mixpanel.recoveryMobileOTPVerfication(binding.etEmailAddress.getText().toString()
                        , binding.etPhoneNumber.getText().toString(), "Entered Invalid OTP");
                break;
        }
    }

    private void gotoHome() {
        Utility.showToast(activity(), "Alernative number is updated successfully. You can login now!", false);
        activity().onBackPressed();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
    }


}
