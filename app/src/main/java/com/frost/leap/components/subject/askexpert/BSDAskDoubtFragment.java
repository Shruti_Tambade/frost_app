package com.frost.leap.components.subject.askexpert;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.components.profile.support.IPickedItemAdapter;
import com.frost.leap.components.profile.support.PickedItemAdapter;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentBsdAskDoubtBinding;
import com.frost.leap.interfaces.ICustomAlertDialog;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.services.FreshDeskForegroundService;
import com.frost.leap.services.HelperService;
import com.frost.leap.viewmodels.subjects.AskExpertViewModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BSDAskDoubtFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BSDAskDoubtFragment extends BottomSheetDialogFragment implements IFragment, IPickedItemAdapter, ICustomAlertDialog {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FragmentBsdAskDoubtBinding binding;
    private List<GalleryFile> list;
    private PickedItemAdapter pickedItemAdapter;
    private int height = -1;
    private String catalogueName, subjectName, unitName, topicName, subTopicName;
    private AskExpertViewModel viewModel;

    public BSDAskDoubtFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static BSDAskDoubtFragment newInstance(String catalogueName, String subjectName, String unitName, String topicName, String subTopicName, int height) {
        BSDAskDoubtFragment fragment = new BSDAskDoubtFragment();
        Bundle args = new Bundle();
        args.putString("CatalogueName", catalogueName);
        args.putString("SubjectName", subjectName);
        args.putString("UnitName", unitName);
        args.putString("TopicName", topicName);
        args.putString("SubTopicName", subTopicName);
        args.putInt("Height", height);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(AskExpertViewModel.class);
        catalogueName = getArguments().getString("CatalogueName");
        subjectName = getArguments().getString("SubjectName");
        unitName = getArguments().getString("UnitName");
        topicName = getArguments().getString("TopicName");
        subTopicName = getArguments().getString("SubTopicName");
        height = getArguments().getInt("Height");
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bsd_ask_doubt, container, false);

        setUp();
        return binding.getRoot();
    }

    private void setUp() {


        binding.tvCatalogName.setText(catalogueName);
        binding.tvSubjectName.setText(subjectName);
        binding.tvSubjectName.setVisibility(TextUtils.isEmpty(subjectName) ? View.GONE : View.VISIBLE);
        binding.tvCancel.setOnClickListener(v -> {
            if (viewModel.doValidation(binding.etDoubt.getText().toString())) {
                cancelAlert();
            } else {
                Mixpanel.cancelExpertPage(catalogueName, subjectName, unitName, topicName, subTopicName);
                this.dismiss();
            }
        });


        binding.tvSend.setOnClickListener(v -> {
            if (viewModel.doValidation(binding.etDoubt.getText().toString())) {
                askAnExpert();
            }
        });


        binding.frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity(), RecyclerView.HORIZONTAL, false));
        ((SimpleItemAnimator) binding.recyclerView.getItemAnimator()).setChangeDuration(0L);

        binding.llSelectMedia.setOnClickListener(v -> {
            if (activity() instanceof MainActivity) {
                Constants.GALLERY_LIMIT = 5;
                openCustomGallery();
            }
        });
        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


    }

    private void openCustomGallery() {

        if (Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()) && Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.GALLERY_FRAGMENT);// gallery
            intent.putParcelableArrayListExtra("MediaList", null);
            intent.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) getSelectedGallery());
            startActivityForResult(intent, Utility.generateRequestCodes().get("MEDIA_FROM_GALLERY"));
            return;

        } else {
            if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
            else if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity()))
                Utility.raisePermissionRequest(Permission.READ_STORAGE, activity());
            else {

            }
        }
    }


    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(activity(), binding.coordinatorLayout, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    public void updateGalleryFiles(List<GalleryFile> list) {
        if (list != null && list.size() > 0) {
            this.list = list;
            binding.recyclerView.setAdapter(pickedItemAdapter = new PickedItemAdapter(list, activity(), this));
        }
    }

    @Override
    public void deleteItem(GalleryFile galleryFile, int position) {
        if (list != null && list.size() > position) {
            list.remove(position);
            if (pickedItemAdapter != null)
                pickedItemAdapter.notifyDataSetChanged();

            pickedItemAdapter.notifyItemRemoved(position);
            pickedItemAdapter.notifyItemRangeChanged(position, pickedItemAdapter.getItemCount());
        }
    }

    public List<GalleryFile> getSelectedGallery() {
        return list;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        if (dialog == null)
            return super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(bottomSheetDialog -> {
            BottomSheetDialog d = (BottomSheetDialog) bottomSheetDialog;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            // Right here!
            BottomSheetBehavior.from(bottomSheet)
                    .setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        dialog.setOnKeyListener((dialog1, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (viewModel.doValidation(binding.etDoubt.getText().toString())) {
                        cancelAlert();
                        return true;
                    }
                    dialog.dismiss();
                    return true;
                }
            }
            return true;
        });


        return dialog;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST CODE", "" + requestCode);

        if (resultCode == Activity.RESULT_OK && data != null) {
            List<GalleryFile> galleryFileList = data.getParcelableArrayListExtra("GalleryFileList");
            if (galleryFileList != null && galleryFileList.size() == 0)
                return;
            updateGalleryFiles(galleryFileList);
        }
    }

    @Override
    public int getTheme() {
        return R.style.BaseBottomSheetDialog;
    }


    private void askAnExpert() {

        Intent intent = new Intent(activity(), FreshDeskForegroundService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_ASK_AN_EXPERT);
        intent.putExtra("IssueType", "Ask an Expert");
        intent.putExtra("Message", binding.etDoubt.getText().toString().trim());
        intent.putExtra("SubjectInfo", catalogueName);
        intent.putExtra("SubjectName", subjectName);
        intent.putExtra("UnitName", unitName);
        intent.putExtra("TopicName", topicName);
        intent.putExtra("SubTopicName", subTopicName);
        intent.putParcelableArrayListExtra("GalleryFileList", (ArrayList<? extends Parcelable>) list);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity().startForegroundService(intent);
        } else {
            activity().startService(intent);
        }

        Mixpanel.submitQuestion(catalogueName, subjectName, unitName, topicName, subTopicName, list != null ? true : false);
        //Utility.showToast(activity(), "Your ticket is added, we will get back to you soon", false);
        dismiss();

    }


    public void cancelAlert() {
        Utility.requestDialog(activity(), this, "Cancel", "Do you want to cancel?", 9001);
    }

    @Override
    public void doPositiveAction(int id) {
        if (id == 9001) {
            Mixpanel.cancelExpertPage(catalogueName, subjectName, unitName, topicName, subTopicName);
            dismiss();
        }
    }

    @Override
    public void doNegativeAction() {

    }
}
