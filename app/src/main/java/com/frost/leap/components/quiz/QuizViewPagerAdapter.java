package com.frost.leap.components.quiz;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.PagerAdapter;

import com.frost.leap.R;
import com.frost.leap.databinding.FragmentQuizQuestionBinding;

import apprepos.quiz.model.QuizQuestionsModel;

/**
 * Frost Interactive
 * Created by Chenna Rao on 9/9/2019.
 */
public class QuizViewPagerAdapter extends PagerAdapter {

    private int size;
    private LayoutInflater inflater;
    private FragmentQuizQuestionBinding binding;
    private QuizQuestionsModel quizQuestionsModel;
    private Context context;
    private boolean isPreview = false;

    public QuizViewPagerAdapter(Context context, int size, QuizQuestionsModel quizQuestionsModel) {
        this.size = size;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.quizQuestionsModel = quizQuestionsModel;
    }

    public QuizViewPagerAdapter(Context context, int size, QuizQuestionsModel quizQuestionsModel, boolean isPreview) {
        this.size = size;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.quizQuestionsModel = quizQuestionsModel;
        this.isPreview = isPreview;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_quiz_question, container, false);

        binding.tvQuestionName.setText(Html.fromHtml("<font color=\"#2c8cf4\">" + "Q" + (position + 1) + " - " + "</font> "
                + "<font color=\"white\">"
                + quizQuestionsModel.getQuestions().get(position).getQuestion()
                + "</font>"));

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerView.setFocusable(false);
        binding.recyclerView.setAdapter(new QuizAnswersAdapter(context, quizQuestionsModel.getQuestions().get(position).getOptions(),isPreview));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setNestedScrollingEnabled(false);
        binding.recyclerView.setFocusable(false);


        container.addView(binding.getRoot());
        return binding.getRoot();
    }

    @Override
    public int getCount() {
        return size;
    }
}
