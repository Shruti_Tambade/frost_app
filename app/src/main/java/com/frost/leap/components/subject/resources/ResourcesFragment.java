package com.frost.leap.components.subject.resources;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.databinding.FragmentResourcesBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.services.FileDownloadService;
import com.frost.leap.services.models.FileDownload;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.subjects.ResourceViewModel;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.ResourceModel;
import supporters.constants.Constants;
import supporters.constants.Permission;
import supporters.constants.RController;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link ResourcesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResourcesFragment extends BaseFragment implements IResourceAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentResourcesBinding binding;
    private ResourceViewModel viewModel;
    private CatalogueModel catalogueModel;

    private List<ResourceModel> list;
    private ResourceModel resourceModel;
    private int position = -1;

    public ResourcesFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ResourcesFragment newInstance(CatalogueModel catalogueModel, String param2) {
        ResourcesFragment fragment = new ResourcesFragment();
        Bundle args = new Bundle();
        fragment.catalogueModel = catalogueModel;
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ResourceViewModel.class);
        if (getArguments() != null) {
            catalogueModel = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_resources;
    }

    @Override
    public void setUp() {


        if(catalogueModel==null)
            return;

        binding = (FragmentResourcesBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setFocusable(false);

        binding.swipeRefreshLayout.setOnRefreshListener(() ->
        {
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchResources(catalogueModel.getStudentCatalogueId());
        });
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        viewModel.fetchResources(catalogueModel.getStudentCatalogueId());

        viewModel.getMessage().
                observe(this, message ->
                        showSnackBar(message.getMessage(), message.getType()));

        viewModel.getNetworkCallStatus().
                observe(this, networkCall -> {
                    switch (networkCall.getNetworkTag()) {
                        case ResourceViewModel.NetworkTags.RESOURCES:
                            updateUIForResourcesList(networkCall);
                            break;
                    }
                });

        viewModel.updateResourcesData().observe(this, list -> {
            this.list = list;
            binding.recyclerView.setAdapter(new ResourceAdapter(activity(), RController.DONE, list, this));
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        super.onActivityCreated(savedInstanceState);
    }

    private void updateUIForResourcesList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;

            case IN_PROCESS:
                binding.recyclerView.setAdapter(new ResourceAdapter(activity(), RController.LOADING, Arrays.asList(null, null), this));
                break;

            case SUCCESS:
                break;

            case DONE:
                break;

            case NO_DATA:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), "Your resources are empty", R.drawable.no_data_available, 2));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.something_went_wrong, 0));
                break;

            case UNAUTHORIZED:
                break;

            case SERVER_ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 0));
                break;
        }
    }

    @Override
    public void downloadResource(ResourceModel resourceModel, int position) {
        // Utility.notifyMessage("Started","Downloaded");
        if (resourceModel.getResourceUrl() == null) {
            showSnackBar("Download url is not available", 2);
            return;
        }
        this.resourceModel = resourceModel;
        this.position = position;
        if (resourceModel != null) {
            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            if (resourceModel.getResourceId() == null)
                resourceModel.setResourceId(UUID.randomUUID().toString());

            if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
                Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
                return;
            }
            if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity())) {
                Utility.raisePermissionRequest(Permission.READ_STORAGE, activity());
                return;
            }
            if (!viewModel.checkIsExist(resourceModel)) {
                FileDownload fileDownload = new FileDownload(new Random().nextInt(9999) + 1,
                        resourceModel.getResourceName(),
                        resourceModel.getResourceUrl(),
                        resourceModel.getResourceId());
                Intent intent = new Intent(activity(), FileDownloadService.class);
                intent.putExtra("FileDownload", fileDownload);
                ContextCompat.startForegroundService(activity(), intent);
                viewModel.createResource(resourceModel, fileDownload);
                return;
            } else {
                //showSnackBar("Already its available",2);
                FileDownload fileDownload = viewModel.getFileDownload(resourceModel);

                if (fileDownload == null) {

                } else if (fileDownload.getPercentage() < 100) {
                    File file = new File("" + fileDownload.getFilePath());
                    if (file == null || !file.exists()) {
                        viewModel.deleteResource(resourceModel);
                        downloadResource(resourceModel, position);
                        return;
                    }
                    Utility.showToast(activity(), "in downloading...", true);
                } else {
                    try {
                        File file = new File("" + fileDownload.getFilePath());
                        if (file == null || !file.exists()) {
                            viewModel.deleteResource(resourceModel);
                            downloadResource(resourceModel, position);
                            return;
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Uri uri = FileProvider.getUriForFile(
                                activity(),
                                activity().getPackageName() + ".fileprovider", file);
                        intent.setDataAndType(uri, Utility.getMimeType(file.getAbsolutePath()));
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        }
    }

    @Override
    public void viewResource(ResourceModel resourceModel, int position) {
        if (resourceModel.getResourceUrl() == null) {
            showSnackBar("Download url is not available", 2);
            return;
        }
        this.resourceModel = resourceModel;
        this.position = position;
        if (resourceModel != null) {
            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            if (resourceModel.getResourceId() == null)
                resourceModel.setResourceId(UUID.randomUUID().toString());

            if (!Utility.checkPermissionRequest(Permission.WRITE_STORAGE, activity())) {
                Utility.raisePermissionRequest(Permission.WRITE_STORAGE, activity());
                return;
            }
            if (!Utility.checkPermissionRequest(Permission.READ_STORAGE, activity())) {
                Utility.raisePermissionRequest(Permission.READ_STORAGE, activity());
                return;
            }
            if (!viewModel.checkIsExist(resourceModel)) {
                FileDownload fileDownload = new FileDownload(new Random().nextInt(9999) + 1,
                        resourceModel.getResourceName(),
                        resourceModel.getResourceUrl(),
                        resourceModel.getResourceId());
                Intent intent = new Intent(activity(), FileDownloadService.class);
                intent.putExtra("FileDownload", fileDownload);
                ContextCompat.startForegroundService(activity(), intent);
                viewModel.createResource(resourceModel, fileDownload);
                return;
            } else {
                //showSnackBar("Already its available",2);
                FileDownload fileDownload = viewModel.getFileDownload(resourceModel);

                if (fileDownload == null) {

                } else if (fileDownload.getPercentage() < 100) {
                    File file = new File("" + fileDownload.getFilePath());
                    if (file == null || !file.exists()) {
                        viewModel.deleteResource(resourceModel);
                        viewResource(resourceModel, position);
                        return;
                    }
                    Utility.showToast(activity(), "in downloading...", true);
                } else {
                    try {
                        File file = new File("" + fileDownload.getFilePath());
                        if (file == null || !file.exists()) {
                            viewModel.deleteResource(resourceModel);
                            viewResource(resourceModel, position);
                            return;
                        }
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        Uri uri = FileProvider.getUriForFile(
                                activity(),
                                activity().getPackageName() + ".fileprovider", file);
                        intent.setDataAndType(uri, Utility.getMimeType(file.getAbsolutePath()));
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utility.showToast(activity(), "unable to open file in your device", false);
                    }

                }

            }
        }
    }


}
