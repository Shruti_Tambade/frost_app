package com.frost.leap.components.subject.ots;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.media.models.GalleryFile;
import com.frost.leap.databinding.FragmentClaimFormBinding;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.subjects.OTSViewModel;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import apprepos.user.model.AdditionalDetails;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClaimFormFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClaimFormFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private AdditionalDetails additionalDetails;
    private String studentCatalogueId;
    private FragmentClaimFormBinding binding;
    private int highQualificationIndex = -1, currentGoalIndex = -1;
    private OTSViewModel viewModel;
    public boolean allowCrop = true;
    public boolean isFront = true;

    private boolean isProofIconClicked = true;
    private Uri previewImageFront, previewImageBack;

    private boolean isProfilePic = false;

    public ClaimFormFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ClaimFormFragment newInstance(String studentCatalogueId, String param2) {
        ClaimFormFragment fragment = new ClaimFormFragment();
        Bundle args = new Bundle();
        args.putString("StudentCatalogueId", studentCatalogueId);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(OTSViewModel.class);
        if (getArguments() != null) {
            studentCatalogueId = getArguments().getString("StudentCatalogueId");
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_claim_form;
    }

    @Override
    public void setUp() {
        binding = (FragmentClaimFormBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        gotoClaimForm();

        binding.llClaimForm.setOnClickListener(v -> {

        });

        binding.tvPreviewFront.setVisibility(View.GONE);
        binding.tvPreviewBack.setVisibility(View.GONE);

//        binding.tvBack.setOnClickListener(v -> {
//            activity().finish();
//        });

        binding.tvNext.setOnClickListener(v -> {

            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            viewModel.fetchAdditionalDetails();
        });

        binding.tvPrrofDetails.setOnClickListener(v -> {
            if (isProofIconClicked) {
                isProofIconClicked = false;
                binding.tvProofDescription.setVisibility(View.VISIBLE);
            } else {
                isProofIconClicked = true;
                binding.tvProofDescription.setVisibility(View.GONE);
            }
        });

        binding.tvPreviewFront.setOnClickListener(v -> {
            previewImage(previewImageFront);
        });

        binding.tvPreviewBack.setOnClickListener(v -> {
            previewImage(previewImageBack);
        });

        binding.tvOTSPage.setOnClickListener(v -> {
            gotoOTSLogin();
        });


        binding.imgUser.setImageURI(Uri.parse(Constants.PLACE_HOLDER_IMAGE));
        binding.tvContinue.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
        binding.tvContinue1.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
        binding.rlAddressSubmitLayout.setBackgroundResource(R.drawable.corner_radius_light_blue_10);

        isProfilePic = false;

        binding.rlPhoto.setOnClickListener(v -> {


            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }

            allowCrop = true;
            if (activity() instanceof HelperActivity) {
                Constants.GALLERY_LIMIT = 1;
                ((HelperActivity) activity()).openCustomGallery();
            }
        });

        binding.qualificationSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                highQualificationIndex = position;
            }
        });

        binding.goalSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentGoalIndex = position;
            }
        });

        binding.rbStudent.setTypeface(Utility.getTypeface(5, activity()));
        binding.rbWorking.setTypeface(Utility.getTypeface(5, activity()));
        binding.rbUnEmployed.setTypeface(Utility.getTypeface(5, activity()));
        binding.rbStudent.setChecked(true);


        binding.rbStudent.setOnClickListener(v -> {
            binding.rbStudent.setChecked(true);
            binding.rbWorking.setChecked(false);
            binding.rbUnEmployed.setChecked(false);
            binding.etCompanyName.setVisibility(View.GONE);
            binding.tlCompanyName.setVisibility(View.GONE);
            binding.etCompanyName.setText("");
        });

        binding.rbWorking.setOnClickListener(v -> {
            binding.rbWorking.setChecked(true);
            binding.rbStudent.setChecked(false);
            binding.rbUnEmployed.setChecked(false);
            binding.etCompanyName.setVisibility(View.VISIBLE);
            binding.tlCompanyName.setVisibility(View.VISIBLE);
            binding.etCompanyName.setText("");
        });

        binding.rbUnEmployed.setOnClickListener(v -> {
            binding.rbUnEmployed.setChecked(true);
            binding.rbStudent.setChecked(false);
            binding.rbWorking.setChecked(false);
            binding.etCompanyName.setVisibility(View.GONE);
            binding.tlCompanyName.setVisibility(View.GONE);
            binding.etCompanyName.setText("");
        });

        binding.rbYes.setOnClickListener(v -> {
            binding.rbYes.setChecked(true);
            binding.rbNo.setChecked(false);
        });

        binding.rbNo.setOnClickListener(v -> {
            binding.rbNo.setChecked(true);
            binding.rbYes.setChecked(false);
        });

        binding.tvEmail.setText(viewModel.getUserEmail());

        binding.tvuploadFront.setOnClickListener(v -> {
            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            allowCrop = false;
            isFront = true;
            if (activity() instanceof HelperActivity) {
                Constants.GALLERY_LIMIT = 1;
                ((HelperActivity) activity()).openCustomGallery();
            }
        });

        binding.tvuploadBack.setOnClickListener(v -> {
            if (!viewModel.isNetworkAvailable()) {
                showSnackBar(Constants.PLEASE_CHECK_INTERNET, 2);
                return;
            }
            allowCrop = false;
            isFront = false;
            if (activity() instanceof HelperActivity) {
                Constants.GALLERY_LIMIT = 1;
                ((HelperActivity) activity()).openCustomGallery();
            }
        });

        viewModel.getMessage().observe(this, message -> {
            showSnackBar(message.getMessage(), message.getType());
        });


        viewModel.updateAdditionalDetails().observe(this, additionalDetails -> {
            gotoPage1();
            if (additionalDetails != null) {
                this.additionalDetails = additionalDetails;

                if (!TextUtils.isEmpty(viewModel.getProfilePic())) {
                    viewModel.userPicUrl = viewModel.getProfilePic();
                    binding.imgUser.setImageURI(Uri.parse(viewModel.getProfilePic()));
                    binding.tvContinue.setBackgroundResource(R.drawable.corner_radius_blue_10);
                    isProfilePic = true;
                }

                if (!TextUtils.isEmpty(additionalDetails.getFatherName())) {
                    binding.etFatherName.setText(additionalDetails.getFatherName());

                }


                /*Highest Qualification DropDown List */
                if (!TextUtils.isEmpty(additionalDetails.getHighestQualification())) {
//                    binding.hqSpinner.setSelection(additionalDetails.getHighestQualification().equalsIgnoreCase(Constants.HIGH_QUALIFICATIONS.get(1)) ? 1 : 2);

                    if (additionalDetails.getHighestQualification().equals(Constants.HIGH_QUALIFICATIONS.get(0))) {
                        binding.qualificationSpinner.setText(Constants.HIGH_QUALIFICATIONS.get(0));
                        binding.qualificationSpinner.setSelection(binding.qualificationSpinner.length());
                        highQualificationIndex = 0;
                    } else {
                        binding.qualificationSpinner.setText(Constants.HIGH_QUALIFICATIONS.get(1));
                        binding.qualificationSpinner.setSelection(binding.qualificationSpinner.length());
                        highQualificationIndex = 1;
                    }

                }

                ArrayAdapter<String> qualificationSpinner = new ArrayAdapter<String>
                        (activity(), R.layout.dropdown_item, Constants.HIGH_QUALIFICATIONS);
                //Getting the instance of AutoCompleteTextView
                binding.qualificationSpinner.setThreshold(1);//will start working from first character
                binding.qualificationSpinner.setAdapter(qualificationSpinner);//setting the adapter data into the AutoCompleteTextView
                binding.qualificationSpinner.setTextColor(Color.BLACK);

                binding.qualificationSpinner.setTag(binding.qualificationSpinner.getKeyListener());
                binding.qualificationSpinner.setKeyListener(null);


                /*Goal DropDown List */
                if (!TextUtils.isEmpty(additionalDetails.getCurrentGoal())) {
                    for (int i = 0; i < Constants.CURRENT_GOALS.size(); i++)
                        if (Constants.CURRENT_GOALS.get(i).equalsIgnoreCase(additionalDetails.getCurrentGoal())) {
                            binding.goalSpinner.setText(Constants.CURRENT_GOALS.get(i));
                            binding.goalSpinner.setSelection(binding.goalSpinner.length());
                            currentGoalIndex = i;
                            break;
                        }

                }

                ArrayAdapter<String> goalAdapter = new ArrayAdapter<String>
                        (activity(), R.layout.dropdown_item, Constants.CURRENT_GOALS);
                //Getting the instance of AutoCompleteTextView
                binding.goalSpinner.setThreshold(1);//will start working from first character
                binding.goalSpinner.setAdapter(goalAdapter);//setting the adapter data into the AutoCompleteTextView
                binding.goalSpinner.setTextColor(Color.BLACK);

                binding.goalSpinner.setTag(binding.goalSpinner.getKeyListener());
                binding.goalSpinner.setKeyListener(null);

                if (!TextUtils.isEmpty(additionalDetails.getCollegeName())) {
                    binding.etUniversity.setText(additionalDetails.getCollegeName());

                }

                if (!TextUtils.isEmpty(additionalDetails.getEmploymentStatus())) {
                    switch (additionalDetails.getEmploymentStatus().toUpperCase()) {
                        case "STUDENT":
                            binding.rbStudent.setChecked(true);
                            binding.rbWorking.setChecked(false);
                            binding.rbUnEmployed.setChecked(false);
                            break;
                        case "EMPLOYED":
                            binding.rbStudent.setChecked(false);
                            binding.rbWorking.setChecked(true);
                            binding.rbUnEmployed.setChecked(false);
                            binding.etCompanyName.setVisibility(View.VISIBLE);
                            binding.etCompanyName.setText(additionalDetails.getCompanyName());
                            break;
                        case "UNEMPLOYED":
                            binding.rbStudent.setChecked(false);
                            binding.rbWorking.setChecked(false);
                            binding.rbUnEmployed.setChecked(true);
                            break;
                    }
                }

                binding.rbYes.setChecked(additionalDetails.isDeliveryInCollege());
                binding.rbNo.setChecked(!additionalDetails.isDeliveryInCollege());

                if (additionalDetails.getAddress() != null) {
                    binding.etAddress.setText(additionalDetails.getAddress().getAddress());
                    binding.etCity.setText(additionalDetails.getAddress().getCity());
                    binding.etState.setText(additionalDetails.getAddress().getState());
                    binding.etPinCode.setText(additionalDetails.getAddress().getPin());

                    binding.rlAddressSubmitLayout.setBackgroundResource(R.drawable.corner_radius_blue_10);
                } else {

                    binding.rlAddressSubmitLayout.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
                }

                if (!TextUtils.isEmpty(additionalDetails.getProofFront())) {
                    viewModel.proofFrontSideUrl = additionalDetails.getProofFront();
//                    binding.imgFront.setImageURI(Uri.parse(viewModel.proofFrontSideUrl));
                    binding.tvPreviewFront.setVisibility(View.VISIBLE);
                    binding.tvuploadFront.setText("Edit");
                    previewImageFront = Uri.parse(viewModel.proofFrontSideUrl);

                    if (viewModel.proofFrontSideUrl.contains("---")) {
                        String[] separated = viewModel.proofFrontSideUrl.split("---");
                        Logger.d("Image_Split_Front", separated[1]);

                        binding.tvImageName.setText(separated[1]);
                    } else
                        binding.tvImageName.setText(viewModel.proofFrontSideUrl);

                }

                if (!TextUtils.isEmpty(additionalDetails.getProofBack())) {
                    viewModel.proofBackSideUrl = additionalDetails.getProofBack();
//                    binding.imgBack.setImageURI(Uri.parse(viewModel.proofBackSideUrl));
                    binding.tvPreviewBack.setVisibility(View.VISIBLE);
                    binding.tvuploadBack.setText("Edit");
                    previewImageBack = Uri.parse(viewModel.proofBackSideUrl);

                    if (viewModel.proofBackSideUrl.contains("---")) {
                        String[] separatedBack = viewModel.proofBackSideUrl.split("---");
                        Logger.d("Image_Split_Front_Back", separatedBack[1]);

                        binding.tvImageNameBack.setText(separatedBack[1]);
                    } else
                        binding.tvImageNameBack.setText(viewModel.proofBackSideUrl);

                }


            }
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case OTSViewModel.NetworkTags.PUSH_USER_PROFILE_PIC:
                    updateUiForUploadUserPic(networkCall);
                    break;

                case OTSViewModel.NetworkTags.PUSH_PROOF_FRONT_SIDE:
                    updateUiForUploadProofFrontSide(networkCall);
                    break;

                case OTSViewModel.NetworkTags.PUSH_PROOF_BACK_SIDE:
                    updateUiForUploadProofBackSide(networkCall);
                    break;
                case OTSViewModel.NetworkTags.UPDATE_OTS_DETAILS:
                    updateUiForOTS(networkCall);
                    break;
                case OTSViewModel.NetworkTags.GET_ADDITIONAL_DETAILS:
                    updateUiForAdditionalDetails(networkCall);
                    break;
            }
        });

        viewModel.getError().observe(this, error -> {
            if (error == null)
                return;
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setError(error.getMessage());
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_error));
            ((TextInputLayout) binding.getRoot().findViewById(error.getId())).setEndIconTintList(ContextCompat.getColorStateList(activity(), R.color.password_toggle_red_color));
            binding.scrollView.scrollTo(Math.round(binding.getRoot().findViewById(error.getId()).getX()),
                    Math.round(binding.getRoot().findViewById(error.getId()).getY()));


        });

        binding.tvContinue.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            if (isProfilePic)
                if (viewModel.validationFirst()) {
                    gotoPage2();
                }
        });


        binding.tvContinue1.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            if (viewModel.validationSecond(
                    binding.etFatherName.getText().toString(),
                    highQualificationIndex == -1 ? null : Constants.HIGH_QUALIFICATIONS.get(highQualificationIndex),
                    binding.rbStudent.isChecked() ? 1 : (binding.rbWorking.isChecked() ? 2 : 3),
                    binding.etCompanyName.getText().toString(),
                    currentGoalIndex == -1 ? null : Constants.CURRENT_GOALS.get(currentGoalIndex),
                    binding.etUniversity.getText().toString(),
                    binding.etPassword.getText().toString()))
                gotoPage3();

        });


        binding.tvSubmit.setOnClickListener(v -> {
            Utility.hideKeyboard(activity());
            if (viewModel.validationThree(
                    binding.rbYes.isChecked(),
                    binding.etAddress.getText().toString(),
                    binding.etCity.getText().toString(),
                    binding.etState.getText().toString(),
                    binding.etPinCode.getText().toString()
            )) {
                viewModel.updateOTSDetails(studentCatalogueId);
            }
        });


        binding.imgClose.setOnClickListener(v -> {
            if (binding.llFinishLayout.getVisibility() == View.VISIBLE) {
                Intent returnData = new Intent();
                returnData.putExtra("Refresh", true);
                activity().setResult(Activity.RESULT_OK, returnData);
                activity().finish();
            } else if (binding.llPhotoLayout.getVisibility() == View.VISIBLE) {
                activity().finish();
            } else if (binding.llAdditionalLayout.getVisibility() == View.VISIBLE) {
                gotoPage1();
            } else if (binding.llAddressLayout.getVisibility() == View.VISIBLE) {
                gotoPage2();
            } else if (binding.llClaimForm.getVisibility() == View.VISIBLE) {
                activity().finish();
            }
        });


        binding.tvDone.setOnClickListener(v -> {
            Intent returnData = new Intent();
            returnData.putExtra("Refresh", true);
            activity().setResult(Activity.RESULT_OK, returnData);
            activity().finish();
        });

        editTextListeners();


        viewModel.fetchAdditionalDetails();

    }

    private void gotoOTSLogin() {
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.OTS_LOGIN_URL));
            startActivity(browserIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void editTextListeners() {

        binding.etFatherName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlFatherName.setErrorEnabled(false);
                    binding.tlFatherName.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.qualificationSpinner.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() == 0)
                    highQualificationIndex = -1;

                if (s.length() != 0) {
                    binding.qualificationTextInputLayout.setErrorEnabled(false);
                    binding.qualificationTextInputLayout.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.goalSpinner.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() == 0)
                    currentGoalIndex = -1;

                if (s.length() != 0) {
                    binding.goalTextInputLayout.setErrorEnabled(false);
                    binding.goalTextInputLayout.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });


        binding.etUniversity.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlUniversityName.setErrorEnabled(false);
                    binding.tlUniversityName.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });
        binding.etCompanyName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlCompanyName.setErrorEnabled(false);
                    binding.tlCompanyName.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etPassword.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlPassword.setErrorEnabled(false);
                    binding.tlPassword.setEndIconTintList(ContextCompat.getColorStateList(activity(), R.color.normal_text_color));
                    binding.tlPassword.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                    binding.tvContinue1.setBackgroundResource(R.drawable.corner_radius_blue_10);
                } else {
                    binding.tvContinue1.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
                }
            }
        });


        binding.etAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlAddress.setErrorEnabled(false);
                    binding.tlAddress.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });

        binding.etCity.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlCity.setErrorEnabled(false);
                    binding.tlCity.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });


        binding.etState.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    binding.tlState.setErrorEnabled(false);
                    binding.tlState.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                }
            }
        });


        binding.etPinCode.addTextChangedListener(new TextWatcher() {


            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    binding.tlPinCode.setErrorEnabled(false);
                    binding.rlAddressSubmitLayout.setBackgroundResource(R.drawable.corner_radius_blue_10);
                    binding.tlPinCode.setBoxBackgroundColor(ContextCompat.getColor(activity(), R.color.text_filed_box_background_color_normal));
                } else {
                    binding.rlAddressSubmitLayout.setBackgroundResource(R.drawable.corner_radius_light_blue_10);
                }
            }

        });
    }


    private void gotoClaimForm() {
        binding.llClaimForm.setVisibility(View.VISIBLE);
        binding.llPhotoLayout.setVisibility(View.GONE);
        binding.llAdditionalLayout.setVisibility(View.GONE);
        binding.llAddressLayout.setVisibility(View.GONE);
    }

    private void gotoPage1() {
        changeTitle("Registration");
        binding.llClaimForm.setVisibility(View.GONE);
        binding.llPhotoLayout.setVisibility(View.VISIBLE);
        binding.llAdditionalLayout.setVisibility(View.GONE);
        binding.llAddressLayout.setVisibility(View.GONE);
    }

    private void gotoPage2() {
        changeTitle("Additional Information");
        binding.llClaimForm.setVisibility(View.GONE);
        binding.llPhotoLayout.setVisibility(View.GONE);
        binding.llAdditionalLayout.setVisibility(View.VISIBLE);
        binding.llAddressLayout.setVisibility(View.GONE);
    }


    private void gotoPage3() {
        changeTitle("Delivery Details");
        binding.llClaimForm.setVisibility(View.GONE);
        binding.llPhotoLayout.setVisibility(View.GONE);
        binding.llAdditionalLayout.setVisibility(View.GONE);
        binding.llAddressLayout.setVisibility(View.VISIBLE);
    }


    private void gotoPage4() {
        changeTitle("Registration");
        binding.llClaimForm.setVisibility(View.GONE);
        binding.llPhotoLayout.setVisibility(View.GONE);
        binding.llAdditionalLayout.setVisibility(View.GONE);
        binding.llAddressLayout.setVisibility(View.GONE);
        binding.llFinishLayout.setVisibility(View.VISIBLE);
    }

    private void updateUiForUploadUserPic(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;

            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                break;

            case FAIL:
                closeProgressDialog();
                break;

            case ERROR:
                closeProgressDialog();
                break;

        }
    }


    private void updateUiForOTS(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;

            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                gotoPage4();
                break;

            case FAIL:
                closeProgressDialog();
                break;

            case ERROR:
                closeProgressDialog();
                break;

        }
    }


    private void updateUiForUploadProofBackSide(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;

            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                break;

            case FAIL:
                closeProgressDialog();
                break;

            case ERROR:
                closeProgressDialog();
                break;

        }
    }

    private void updateUiForUploadProofFrontSide(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                closeProgressDialog();
                break;

            case IN_PROCESS:
                showProgressDialog();
                break;

            case SUCCESS:
                closeProgressDialog();
                break;

            case FAIL:
                closeProgressDialog();
                break;

            case ERROR:
                closeProgressDialog();
                break;

        }
    }

    private void updateUiForAdditionalDetails(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {


            case NO_INTERNET:
            case SUCCESS:
            case FAIL:
            case ERROR:
                binding.llLoaderFirst.setVisibility(View.GONE);
                binding.tvNext.setVisibility(View.VISIBLE);
                break;


            case IN_PROCESS:
                binding.llLoaderFirst.setVisibility(View.VISIBLE);
                binding.tvNext.setVisibility(View.GONE);
                break;


        }
    }


    @Override
    public void changeTitle(String title) {
        binding.scrollView.fullScroll(View.FOCUS_UP);
        binding.tvTitle.setText(title);
    }


    public void updateProfilePic(List<GalleryFile> galleryFileList) {
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.evictFromCache(galleryFileList.get(0).uri);
        Logger.d("FILE URI", galleryFileList.get(0).uri.getPath());
        binding.imgUser.setImageURI(galleryFileList.get(0).uri);
        binding.tvContinue.setBackgroundResource(R.drawable.corner_radius_blue_10);
        isProfilePic = true;
        viewModel.uploadProfilePic(galleryFileList.get(0));

    }


    public void updateFront(List<GalleryFile> list) {
//        binding.imgFront.setImageURI(list.get(0).uri);
        binding.tvImageName.setText(list.get(0).name);
        binding.tvPreviewFront.setVisibility(View.VISIBLE);
        binding.tvuploadFront.setText("Edit");

        previewImageFront = list.get(0).uri;

        viewModel.uploadProofFrontSidePic(list.get(0));
    }

    public void updateBack(List<GalleryFile> list) {
//        binding.imgBack.setImageURI(list.get(0).uri);
        binding.tvImageNameBack.setText(list.get(0).name);
        binding.tvPreviewBack.setVisibility(View.VISIBLE);
        binding.tvuploadBack.setText("Edit");

        previewImageBack = list.get(0).uri;

        viewModel.uploadProofBackSidePic(list.get(0));
    }

    public void onBackPressed() {
        if (binding.llFinishLayout.getVisibility() == View.VISIBLE) {
            Intent returnData = new Intent();
            returnData.putExtra("Refresh", true);
            activity().setResult(Activity.RESULT_OK, returnData);
            activity().finish();
        } else if (binding.llPhotoLayout.getVisibility() == View.VISIBLE) {
            activity().finish();
        } else if (binding.llAdditionalLayout.getVisibility() == View.VISIBLE) {
            gotoPage1();
        } else if (binding.llAddressLayout.getVisibility() == View.VISIBLE) {
            gotoPage2();
        } else if (binding.llClaimForm.getVisibility() == View.VISIBLE) {
            activity().finish();
        }
    }

    public void previewImage(Uri uri) {
        if (uri == null) return;
        if (activity() instanceof HelperActivity) {
            ((HelperActivity) activity()).openFullImageView(uri);
        }
    }
}
