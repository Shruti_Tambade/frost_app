package com.frost.leap.components.subject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemSubjectBinding;
import com.frost.leap.databinding.ItemSubjectsHeaderBinding;
import com.frost.leap.generic.viewholder.SkeletonViewHolder;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.SubjectModel;
import supporters.constants.RController;
import supporters.customviews.views.LinearLayoutManagerWithSmoothScroller;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 14-08-2019.
 * <p>
 * Frost
 */
public class SubjectsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SubjectModel> list;
    private Context context;
    private RController rController;
    private CatalogueModel catalogueModel;
    private IUnitAdapter iUnitAdapter;


    public SubjectsAdapter(Context context,
                           RController rController,
                           List<SubjectModel> list,
                           CatalogueModel catalogueModel,
                           IUnitAdapter iUnitAdapter) {
        this.list = list;
        this.context = context;
        this.rController = rController;
        this.catalogueModel = catalogueModel;
        this.iUnitAdapter = iUnitAdapter;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        if (rController == RController.LOADING) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skelton_subject, viewGroup, false);
            return new SkeletonViewHolder(itemView);
        } else if (i == 0) {
            return new HeaderViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_subjects_header, viewGroup, false));
        } else
            return new SubjectViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_subject, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof SubjectViewHolder) {
            SubjectViewHolder subjectViewHolder = (SubjectViewHolder) viewHolder;
            subjectViewHolder.bindData(list.get(position - 1), position);

            if (position == list.size()) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, 0),
                        Utility.dpSize(context, position == list.size() ? 90 : 0));
                subjectViewHolder.itemView.setLayoutParams(params);
            } else {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                subjectViewHolder.itemView.setLayoutParams(params);
            }

        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.binding.offlineSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {

                if (!Utility.isNetworkAvailable(context)) {
                    headerViewHolder.binding.offlineSwitch.setChecked(true);
                    iUnitAdapter.showOnlyOfflineDownloads(true);
                    return;
                }

                iUnitAdapter.showOnlyOfflineDownloads(isChecked);
            });

            headerViewHolder.binding.llClaimLayout.setVisibility(catalogueModel.isOts() ? View.GONE : View.VISIBLE);

            headerViewHolder.binding.tvClaimLayout.setOnClickListener(v -> {
                iUnitAdapter.gotoClaimStudyMaterial();
            });

            if (!Utility.isNetworkAvailable(context)) {
                headerViewHolder.binding.offlineSwitch.setChecked(true);
            }

            if (list != null && list.size() > 0) {
                headerViewHolder.binding.llErrorMessage.setVisibility(View.GONE);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                headerViewHolder.binding.llHeaderLayout.setLayoutParams(params);
            } else {
                headerViewHolder.binding.llErrorMessage.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

                headerViewHolder.binding.llHeaderLayout.setLayoutParams(params);

                if (!Utility.isNetworkAvailable(context))
                    headerViewHolder.binding.tvError.setText(context.getResources().getString(R.string.empty_offline_content));
                else
                    headerViewHolder.binding.tvError.setText(context.getResources().getString(R.string.empty_offline_content_with_Internet));
            }

        }
    }

    @Override
    public int getItemCount() {
        return rController == RController.LOADING
                ? 15
                : list.size() + 1;
    }

    public void updateList(List<SubjectModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public class SubjectViewHolder extends RecyclerView.ViewHolder {
        private ItemSubjectBinding binding;

        public SubjectViewHolder(ItemSubjectBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(context));
            this.binding.recyclerView.setFocusable(false);
            this.binding.recyclerView.setHasFixedSize(true);
            this.binding.recyclerView.setNestedScrollingEnabled(false);
        }

        public void bindData(SubjectModel subject, int position) {

            ScaleAnimation expandAnimation = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.0f);
            expandAnimation.setDuration(400);

            ScaleAnimation collapsAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.0f);
            collapsAnimation.setDuration(400);

            binding.tvTotalVideoDuaration.setText(Utility.calculateTimeDuration(subject.getTotalDurationInSce()));

            binding.tvUpdated.setVisibility(subject.getStudentUnits().size() > 0 && subject.getTotalDurationInSce() > 0 ? View.GONE : View.VISIBLE);

            int percentageValue = 0;

            if (subject.getCompletionPercentage() != null && subject.getCompletionPercentage() > 9)
                percentageValue = subject.getCompletionPercentage().intValue();

            if ((subject.getCompletionPercentage() > 0 && subject.getCompletionPercentage() < 100))
                binding.progressBar.setProgress(percentageValue + 1);
            else
                binding.progressBar.setProgress(percentageValue);

            if (subject.getSubjectName() != null && !subject.getSubjectName().isEmpty())
                binding.tvSubjectName.setText(subject.getSubjectName());

            binding.recyclerView.setItemAnimator(null);
            binding.recyclerView.setAdapter(new UnitsAdapter(subject, subject.getStudentUnits(), context, catalogueModel, iUnitAdapter, position));

            binding.recyclerView.setVisibility(subject.isSelect()
                    ? View.GONE
                    : View.VISIBLE);

            binding.imgOptions.animate().rotation(subject.isSelect()
                    ? 0
                    : 180)
                    .start();

            binding.llLayout.setOnClickListener(v -> {

                Mixpanel.accessSubjects(catalogueModel.getCatalogueName(), subject.getSubjectName());

                subject.setSelect(!subject.isSelect());
                notifyItemChanged(position);
            });
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        private ItemSubjectsHeaderBinding binding;

        public HeaderViewHolder(ItemSubjectsHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }
}
