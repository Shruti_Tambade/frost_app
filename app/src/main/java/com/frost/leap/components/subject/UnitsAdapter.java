package com.frost.leap.components.subject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemUnitBinding;

import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 14-08-2019.
 * <p>
 * Frost
 */
public class UnitsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UnitModel> list;
    private Context context;
    private CatalogueModel catalogueModel;
    private SubjectModel subjectModel;
    private int lastPosition = -1;
    private IUnitAdapter iUnitAdapter;
    private int subjectPosition;

    public UnitsAdapter(SubjectModel subjectModel, List<UnitModel> list, Context context,
                        CatalogueModel catalogueModel, IUnitAdapter iUnitAdapter, int subjectPosition) {
        this.list = list;
        this.context = context;
        this.catalogueModel = catalogueModel;
        this.subjectModel = subjectModel;
        this.iUnitAdapter = iUnitAdapter;
        this.subjectPosition = subjectPosition;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return new UnitViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_unit, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof UnitViewHolder) {
            UnitViewHolder unitViewHolder = (UnitViewHolder) viewHolder;
            unitViewHolder.bindData(list.get(position), position);

            unitViewHolder.binding.llLayout.setOnClickListener(v -> {
                iUnitAdapter.onUnitClicked(subjectPosition, position, list.get(position), catalogueModel, subjectModel, lastPosition, list);
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class UnitViewHolder extends RecyclerView.ViewHolder {

        private ItemUnitBinding binding;

        public UnitViewHolder(ItemUnitBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(UnitModel unit, int position) {
            binding.tvUnitName.setText(unit.getUnitName());
            binding.tvTotalVideoDuaration.setText(Utility.calculateTimeDuration(unit.getTotalDurationInSce()));

            int percentageValue = 0;

            if (unit.getCompletionPercentage() != null) {

                binding.tvUpdated.setVisibility(unit.getTotalDurationInSce() > 0 ? View.GONE : View.VISIBLE);

                percentageValue = unit.getCompletionPercentage().intValue();

                binding.ivPlayIcon.setVisibility(unit.getLastWatched()
                        ? View.VISIBLE
                        : unit.getCompletionPercentage() > 99 && unit.getCompletionPercentage() <= 100 ? View.VISIBLE : View.GONE);

                binding.ivPlayIcon.setImageResource(unit.getCompletionPercentage() > 99 && unit.getCompletionPercentage() <= 100
                        ? R.drawable.ic_progress_done
                        : R.drawable.ic_playing);

                binding.progressBar.setVisibility(unit.getLastWatched()
                        ? View.GONE
                        : unit.getCompletionPercentage() > 99 && unit.getCompletionPercentage() <= 100 ? View.GONE : View.VISIBLE);

                if ((unit.getCompletionPercentage() > 0 && unit.getCompletionPercentage() < 100))
                    binding.progressBar.setProgress(percentageValue + 1);
                else
                    binding.progressBar.setProgress(percentageValue);

            }

            if (unit.getLastWatched()) {
                binding.llLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.last_played_background_color));
                binding.tvUnitName.setTextColor(context.getResources().getColor(R.color.text_color));
                lastPosition = position;
            } else
                binding.llLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));

        }
    }
}
