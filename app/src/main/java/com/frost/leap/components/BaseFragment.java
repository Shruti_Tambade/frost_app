package com.frost.leap.components;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.frost.leap.activities.BaseActivity;
import com.frost.leap.interfaces.IFragment;
import com.frost.leap.viewmodels.BaseViewModel;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.lang.ref.WeakReference;

import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 08-05-2019.
 * <p>
 * Frost
 */
public abstract class BaseFragment extends Fragment implements IFragment {

    private ViewDataBinding baseDataBinding;
    private View snackBarView;
    private Dialog dialog;
    private MixpanelAPI mixpanel;

    @LayoutRes
    public abstract int layout();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        baseDataBinding = DataBindingUtil.inflate(inflater, layout(), container, false);
        setUp();

        //MixPanel Initilization
        if (mixpanel == null)
            mixpanel = MixpanelAPI.getInstance(activity(), Constants.MIXPANEL_TOKEN);

        return baseDataBinding.getRoot();
    }

    public ViewDataBinding getBaseDataBinding() {
        return baseDataBinding;
    }

    public abstract void setUp();

    public void setUpSnackBarView(View view) {
        this.snackBarView = view;
    }

    @Override
    public void showSnackBar(String snackBarText, int type) {
        Utility.showSnackBar(activity(), snackBarView, snackBarText, type);
    }

    @Override
    public Activity activity() {
        return getActivity();
    }


    public void showProgressDialog() {
        if (dialog == null)
            dialog = Utility.generateProgressDialog(activity());

        dialog.show();
    }

    public void closeProgressDialog() {
        if (dialog != null && dialog.isShowing())
            Utility.closeProgressDialog(dialog);
    }


    public void updateUiForRefreshToken(int value, BaseViewModel viewModel) {

        switch (value) {

            case -1: // ERROR
                closeProgressDialog();
                break;

            case 0: // PROCESSING TOKEN
                showProgressDialog();
                break;

            case 1: // TOKEN REFRESHED
                closeProgressDialog();
                break;

            case 2: // UNAUTHORIZED
                ((BaseActivity) activity()).doLogout(viewModel);
                break;

            default:
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (mixpanel != null)
            mixpanel.flush();

        closeProgressDialog();
        super.onDestroy();
    }

    public boolean isNetworkAvailable() {
        return Utility.isNetworkAvailable(Utility.getContext());
    }


    public void showToast(String message, boolean isLong) {
        Utility.showToast(activity(), message, isLong);
    }

}
