package com.frost.leap.components.store_overview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.frost.leap.R;
import com.frost.leap.databinding.ItemFeatureBinding;

import java.util.List;

import apprepos.store.model.FeaturesModel;

/**
 * Created by Chenna Rao on 10/1/2019.
 */
public class FeaturesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<FeaturesModel> featuresList;
    ;


    public FeaturesAdapter(Context context, List<FeaturesModel> featuresList) {
        this.context = context;
        this.featuresList = featuresList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new FeaturesViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_feature, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeaturesViewHolder) {

            FeaturesViewHolder featuresViewHolder = (FeaturesViewHolder) holder;

            featuresViewHolder.bindData(position);
        }

    }

    @Override
    public int getItemCount() {
        return featuresList.size();
    }

    private class FeaturesViewHolder extends RecyclerView.ViewHolder {

        private ItemFeatureBinding binding;

        public FeaturesViewHolder(ItemFeatureBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindData(int position) {

            binding.lineView.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
            binding.tvFeatureTitle.setText(featuresList.get(position).getFeatureName());
            binding.ivFeature.setImageResource(featuresList.get(position).getFeatureImage());
        }
    }
}
