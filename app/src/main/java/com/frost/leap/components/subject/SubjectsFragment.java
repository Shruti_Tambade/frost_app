package com.frost.leap.components.subject;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.activities.MainActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.components.catalogue.CatalogueFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentSubjectsBinding;
import com.frost.leap.generic.EmptyDataAdapter;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.subjects.SubjectViewModel;

import java.util.Arrays;
import java.util.List;

import apprepos.catalogue.model.CatalogueModel;
import apprepos.subject.model.SubjectModel;
import apprepos.subject.model.UnitModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.constants.RController;
import supporters.customviews.views.LinearLayoutManagerWithSmoothScroller;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;
import supporters.utils.Utility;

/**
 * A simple {@link } subclass.
 * Use the {@link SubjectsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubjectsFragment extends BaseFragment implements IUnitAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private CatalogueModel catalogueModel;
    private String mParam2;

    private FragmentSubjectsBinding binding;
    private SubjectViewModel viewModel;
    private SubjectsAdapter adapter;
    private List<SubjectModel> subjectModelList;
    private int subjectPosition;
    private int position;
    private String studentSubjectId;
    private String studentUnitId;
    private SubjectBroadcastReceiver broadcastReceiver;
    private boolean showOnlyDownloads = false;
    private int offset = 0;

    private boolean isClicked = true;

    public SubjectsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubjectsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SubjectsFragment newInstance(CatalogueModel param1, String param2) {
        SubjectsFragment fragment = new SubjectsFragment();
        fragment.catalogueModel = param1;
        fragment.mParam2 = param2;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(SubjectViewModel.class);
        broadcastReceiver = new SubjectBroadcastReceiver();
        super.onCreate(savedInstanceState);

    }


    @Override
    public int layout() {
        return R.layout.fragment_subjects;
    }

    @Override
    public void setUp() {


        if (catalogueModel == null) {
            activity().finish();
            return;
        }
        binding = (FragmentSubjectsBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);
        binding.recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(activity()));
        ((SimpleItemAnimator) binding.recyclerView.getItemAnimator()).setChangeDuration(0);
        binding.recyclerView.setItemAnimator(null);
        binding.recyclerView.setFocusable(false);

        checkCatalogueSubjects();

        binding.swipeRefreshLayout.setOnRefreshListener(() ->
        {
            if (catalogueModel == null) {
                activity().onBackPressed();
                return;
            }
            binding.swipeRefreshLayout.setRefreshing(false);
            viewModel.fetchSubjects(catalogueModel.getStudentCatalogueId(), true);
        });

        viewModel.getMessage().
                observe(this, message -> {
                    showSnackBar(message.getMessage(), message.getType());
                });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {
            switch (networkCall.getNetworkTag()) {
                case SubjectViewModel.NetworkTags.SUBJECTS_LIST:
                    updateUIForSubjectList(networkCall);
                    break;
            }
        });

        viewModel.updateSubjectsData().observe(this, list -> {

            this.subjectModelList = list;
            binding.recyclerView.setAdapter(adapter = new SubjectsAdapter(activity(), RController.DONE, list, catalogueModel, this));
            binding.recyclerView.setHasFixedSize(true);
        });

        viewModel.updateOfflineSubjectsData().observe(this, list -> {
            binding.progressBar.setVisibility(View.GONE);
            this.subjectModelList = list;
            if (adapter != null) {
                adapter.updateList(subjectModelList);
            }
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        viewModel.fetchSubjects(catalogueModel.getStudentCatalogueId(), false);

    }

    private void checkCatalogueSubjects() {
            viewModel.getOfflineSubjects(catalogueModel.getStudentCatalogueId());
    }

    private void updateUIForSubjectList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.PLEASE_CHECK_INTERNET, R.drawable.ic_wifi_signal, 0));
                break;

            case IN_PROCESS:
                binding.recyclerView.setAdapter(new SubjectsAdapter(activity(), RController.LOADING, Arrays.asList(null, null), catalogueModel, this));
                break;

            case SUCCESS:
                break;

            case DONE:
                break;

            case NO_DATA:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.NO_DATA_AVAILABLE, R.drawable.no_data_available, 1));
                break;

            case ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.something_went_wrong, 0));
                break;

            case UNAUTHORIZED:
                break;

            case SERVER_ERROR:
                binding.recyclerView.setAdapter(new EmptyDataAdapter(activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 0));
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void gotoClaimStudyMaterial() {
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.CLAIM_FORM);
        intent.putExtra("StudentCatalogueId", catalogueModel.getStudentCatalogueId());
        startActivityForResult(intent, Utility.generateRequestCodes().get("OTS_REGISTRATION"));
    }

    @Override
    public void showOnlyOfflineDownloads(boolean show) {
        this.showOnlyDownloads = show;
        if (show) {
            binding.progressBar.setVisibility(View.VISIBLE);
            viewModel.getDownloadSubjects(catalogueModel.getStudentCatalogueId());
        } else {
            binding.progressBar.setVisibility(View.GONE);
            viewModel.stopOfflineData();
            subjectModelList = viewModel.getSubjectList();
            adapter.updateList(subjectModelList);
        }
    }

    @Override
    public void onUnitClicked(int subjectPostion, int position, UnitModel unitModel, CatalogueModel catalogueModel, SubjectModel subjectModel
            , int lastPosition, List<UnitModel> list) {

        if (unitModel != null && unitModel.getTotalDurationInSce() != null) {
            if (unitModel.getTotalDurationInSce() == 0) {
                if (activity() instanceof MainActivity) {
                    ((MainActivity) activity()).showSnackBar("No lecture videos have been uploaded in this unit yet. Please check back again later.", 2);
                }
                return;
            }
        }
        if (isClicked) {
            isClicked = false;
            this.subjectPosition = subjectPostion - 1;
            this.position = position;
            this.studentSubjectId = subjectModel.getStudentSubjectId();
            this.studentUnitId = unitModel.getStudentUnitId();

            if (lastPosition >= 0) {
                if (position != lastPosition) {
                    list.get(position).setLastWatched(true);
//                    list.get(position).setCompletionPercentage(1.0);
                    list.get(lastPosition).setLastWatched(false);
                    adapter.notifyDataSetChanged();
                }
            } else {
                list.get(position).setLastWatched(true);
//                list.get(position).setCompletionPercentage(1.0);
                adapter.notifyDataSetChanged();
            }

            adapter.notifyDataSetChanged();
            if (activity() instanceof MainActivity) {
                Mixpanel.accessUnits(catalogueModel.getCatalogueName(), subjectModel.getSubjectName(), unitModel.getUnitName());
                ((MainActivity) activity()).openUnitPage(unitModel, catalogueModel, subjectModel);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isClicked = true;
                    }
                }, 500);
            }
        }
    }


    public class SubjectBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("Update", false)) {
                if (adapter == null || subjectModelList == null) {
                    return;
                }
                Observable<Integer> observable = Observable.create(emitter ->
                {
                    try {
                        if (subjectModelList == null) {
                            emitter.onNext(-1);
                            emitter.onComplete();
                            return;
                        }
                        String subjectId = intent.getStringExtra("StudentSubjectId");
                        String unitId = intent.getStringExtra("UnitId");
                        double subjectCompletionPercentage = intent.getDoubleExtra("SubjectCompletionPercentage", 0);
                        double unitCompletionPercentage = intent.getDoubleExtra("UnitCompletionPercentage", 0);

                        for (int i = 0; i < subjectModelList.size(); i++) {
                            if (subjectModelList.get(i).getStudentSubjectId().equals(subjectId)) {
                                subjectModelList.get(i).setCompletionPercentage(subjectCompletionPercentage);
                                List<UnitModel> unitModelList = subjectModelList.get(i).getStudentUnits();
                                if (unitModelList == null) {
                                    emitter.onNext(i);
                                    emitter.onComplete();
                                    return;
                                }
                                for (int j = 0; j < unitModelList.size(); j++) {
                                    if (unitModelList.get(j).getStudentUnitId().equals(unitId)) {
                                        subjectModelList.get(i).getStudentUnits().get(j).setCompletionPercentage(unitCompletionPercentage);
                                        emitter.onNext(i);
                                        emitter.onComplete();
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        emitter.onError(e);
                    }
                    emitter.onComplete();

                });
                observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Integer>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Integer position) {
                                if (position != null && position != -1) {
                                    if (adapter != null)
                                        adapter.notifyItemChanged(position + 1);
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        registerReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void registerReceiver() {

        try {
            if (broadcastReceiver == null)
                return;

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_UPDATE_SUBJECT_PROGRESS);
            activity().registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (broadcastReceiver == null)
                return;

            activity().unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("REQUEST CODE", "" + requestCode);
        if (requestCode == Utility.generateRequestCodes().get("OTS_REGISTRATION")) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                catalogueModel.setOts(true);
                if (adapter != null)
                    adapter.notifyItemChanged(0);

                if (getParentFragment() instanceof CatalogueOverviewFragment) {
                    CatalogueOverviewFragment catalogueOverviewFragment = (CatalogueOverviewFragment) getParentFragment();
                    catalogueOverviewFragment.updateOtsDetails();
                }

                if (activity() instanceof MainActivity) {
                    List<Fragment> list = ((MainActivity) activity()).getSupportFragmentManager().getFragments();
                    for (Fragment fragment : list) {
                        if (fragment instanceof CatalogueFragment) {
                            CatalogueFragment catalogueFragment = (CatalogueFragment) fragment;
                            catalogueFragment.refreshCatalogues();
                            break;
                        }
                    }
                }
            }
        }
    }


}
