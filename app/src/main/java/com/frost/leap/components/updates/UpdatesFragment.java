package com.frost.leap.components.updates;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.frost.leap.R;
import com.frost.leap.activities.HelperActivity;
import com.frost.leap.components.BaseFragment;
import com.frost.leap.controllers.AppController;
import com.frost.leap.databinding.FragmentUpdatesBinding;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.viewmodels.models.NetworkCall;
import com.frost.leap.viewmodels.updates.UpdateViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import apprepos.updates.model.UpdateModel;
import supporters.constants.Constants;
import supporters.constants.RController;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UpdatesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UpdatesFragment extends BaseFragment implements IUpdateAdapter {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private CMSBroadcastReceiver broadcastReceiver;
    private UpdateViewModel viewModel;

    private FragmentUpdatesBinding binding;
    private List<String> activeInactiveCatalogueIDs = new ArrayList<>();
    private List<String> activeCatalogueIds = new ArrayList<>();

    private List<UpdateModel> list;
    private CatalogueUpdatesAdapter adapter;
    private int currentPosition = -1;

    public UpdatesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UpdatesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UpdatesFragment newInstance(String param1, String param2) {
        UpdatesFragment fragment = new UpdatesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static UpdatesFragment newInstance(ArrayList<String> param1, ArrayList<String> param2) {
        UpdatesFragment fragment = new UpdatesFragment();

        Bundle bundle = new Bundle();
        bundle.putStringArrayList("All", param1);
        bundle.putStringArrayList("Active", param2);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            activeInactiveCatalogueIDs = getArguments().getStringArrayList("All");
            activeCatalogueIds = getArguments().getStringArrayList("Active");
        }
        broadcastReceiver = new CMSBroadcastReceiver();
        viewModel = new ViewModelProvider(this).get(UpdateViewModel.class);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public int layout() {
        return R.layout.fragment_updates;
    }

    @Override
    public void setUp() {

        binding = (FragmentUpdatesBinding) getBaseDataBinding();
        setUpSnackBarView(binding.coordinatorLayout);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity()));
        binding.recyclerView.setHasFixedSize(true);

        viewModel.getMessage().observe(this, message -> {
//            showSnackBar(message.getMessage(), message.getType());
        });

        viewModel.getNetworkCallStatus().observe(this, networkCall -> {

            switch (networkCall.getNetworkTag()) {
                case UpdateViewModel.NetworkTags.UPDATES_LIST:
                    updateUIForUpdatesList(networkCall);
                    break;
            }

        });

        viewModel.getUpdatesList().observe(this, list -> {
            binding.tvPastUpdatesContent.setVisibility(View.GONE);
            if (list != null) {
                this.list = list;
                binding.recyclerView.setAdapter(adapter = new CatalogueUpdatesAdapter(activity(), this, list, activeCatalogueIds, false, RController.DONE));
            }
        });

        viewModel.controlRefreshToken().observe(this, value -> {
            updateUiForRefreshToken(value, viewModel);
        });

        binding.tvPastUpdatesContent.setOnClickListener(v -> {
            Intent intent = new Intent(activity(), HelperActivity.class);
            intent.putExtra(Constants.FRAGMENT_KEY, AppController.ALL_UPDATES_PAGE);
            intent.putStringArrayListExtra("CatalogueIDS", (ArrayList<String>) activeInactiveCatalogueIDs);
            startActivity(intent);
        });

        ShareDataManager.getInstance().getDashBoardUpdateLiveData().observe(this, updateModel -> {

            if (updateModel != null && list != null && currentPosition != -1
                    && list.size() > currentPosition && adapter != null) {
                if (updateModel.getUpdateId().equals(list.get(currentPosition).getUpdateId())) {
                    list.get(currentPosition).setChoiceType(updateModel.getChoiceType());
                    adapter.notifyDataSetChanged();
                }
            }
        });

        viewModel.fetchUpdatesList(activeCatalogueIds, null, null);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void updateUIForUpdatesList(NetworkCall networkCall) {
        switch (networkCall.getNetworkStatus()) {
            case NO_INTERNET:
                binding.tvPastUpdatesContent.setVisibility(View.GONE);
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), "Looks like your device isn't connected to the internet right now.", R.drawable.cloud_icon, 1, false));
                break;

            case IN_PROCESS:
                binding.recyclerView.setAdapter(adapter = new CatalogueUpdatesAdapter(activity(), this, Arrays.asList(null, null), activeCatalogueIds, true, RController.LOADING));
                break;

            case SUCCESS:
                binding.tvPastUpdatesContent.setVisibility(View.GONE);
                break;

            case DONE:
                binding.tvPastUpdatesContent.setVisibility(View.GONE);
                break;

            case NO_DATA:
                binding.tvPastUpdatesContent.setVisibility(View.VISIBLE);
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), "No new updates to show you. You’re all caught up!", R.drawable.update_icon, 2, false));

                break;

            case ERROR:
                binding.tvPastUpdatesContent.setVisibility(View.VISIBLE);
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 1, false));
                break;

            case SERVER_ERROR:
                binding.tvPastUpdatesContent.setVisibility(View.VISIBLE);
                binding.recyclerView.setAdapter(new EmptyUpdateAdapter(this, activity(), Constants.SOMETHING_WENT_WRONG, R.drawable.ic_server_issue, 1, false));
                break;

            case UNAUTHORIZED:
                break;
        }
    }

    @Override
    public void changeTitle(String title) {

    }

    @Override
    public void redirectToDoubtPage(UpdateModel updateModel, int position) {

        this.currentPosition = position;
        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.DOUBT_CLEARING_PAGE);
        intent.putExtra("Update_Item", updateModel);
        intent.putExtra("TAG", 1);
        startActivity(intent);

    }

    @Override
    public void retryUpdateAPI() {

//        if (!isNetworkAvailable()) {
//            return;
//        }

        viewModel.fetchUpdatesList(activeCatalogueIds, null, null);
    }

    @Override
    public void redirectTOAllUpdates() {

        Intent intent = new Intent(activity(), HelperActivity.class);
        intent.putExtra(Constants.FRAGMENT_KEY, AppController.ALL_UPDATES_PAGE);
        intent.putStringArrayListExtra("CatalogueIDS", (ArrayList<String>) activeInactiveCatalogueIDs);
        startActivity(intent);
    }

    @Override
    public void openPDF(String pdfFile, String title) {
//        Intent intent = new Intent(activity(), HelperActivity.class);
//        intent.putExtra(Constants.FRAGMENT_KEY, AppController.PDF_OVERVIEW);
//        intent.putExtra(Constants.TITLE, title);
//        intent.putExtra("Url", pdfFile);
//        startActivity(intent);

        try {
            try {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setClassName("com.google.android.apps.docs", "com.google.android.apps.viewer.PdfViewerActivity");
                browserIntent.setDataAndType(Uri.parse(pdfFile), "application/pdf");
                startActivity(browserIntent);
            } catch (Exception e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(pdfFile), "application/pdf");
                startActivity(browserIntent);
            }
        } catch (Exception e) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pdfFile));
                startActivity(browserIntent);
            } catch (Exception e1) {
                showSnackBar(Constants.SOMETHING_WENT_WRONG, 2);

            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        registerReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (broadcastReceiver == null)
                return;

            activity().unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerReceiver() {

        try {
            if (broadcastReceiver == null)
                return;

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Constants.ACTION_CMS_UPDATES);
            activity().registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public class CMSBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("Refresh", false)) {
                viewModel.fetchUpdatesList(activeCatalogueIds, null, null);
            }
        }
    }

}
