package com.frost.leap.components.videoplayer;

import supporters.constants.VideoQuality;

/**
 * Created by R!ZWAN SHEIKH on 2019-09-17.
 * <p>
 * Frost
 */
public interface IVideoResolutionController {
    void setVideoQuality(VideoQuality videoQuality, int bitrate, int resolution);
}
