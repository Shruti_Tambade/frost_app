package com.frost.leap.services.video;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Pair;

import androidx.annotation.Nullable;
import androidx.collection.ArraySet;
import androidx.core.app.NotificationCompat;

import com.frost.leap.applications.HelperApplication;
import com.frost.leap.broadcasts.AlarmReceiver;
import com.frost.leap.controllers.AppController;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DeepLearnOfflineLicenseHelper;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.source.dash.DashUtil;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.upstream.ByteArrayDataSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSourceInputStream;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;

import apprepos.topics.TopicRepositoryManager;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.utils.Logger;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 29-01-2020.
 * <p>
 * FROST
 */
public class DownloadLicenseService extends Service {

    private TopicRepositoryManager repositoryManager;
    private Set<String> set;
    private List<Disposable> disposableList;
    public static final int DOWNLOAD_LICENSE_SERVICE_ID = 89770;

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(DOWNLOAD_LICENSE_SERVICE_ID, getNotification(null, "Adding download request"));
        disposableList = new ArrayList<>();
        set = new ArraySet<>();
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            removeAll();
            stopForeground(true);
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }
        if (intent.getBooleanExtra("CancelAll", false)) {
            removeAll();
            stopForeground(true);
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }
        switch (intent.getIntExtra(Constants.ACTION_KEY, 0)) {
            case AppController.ACTION_DOWNLOAD_DRM_LICENSE:
                if (!set.contains(intent.getStringExtra("VideoUrl"))) {
                    set.add(intent.getStringExtra("VideoUrl"));
                    downloadLicense(this, intent);
                }
                break;
        }
        return START_STICKY;
    }


    public void downloadLicense(Context context, Intent intent) {
        Observable observable = Observable.create(emitter ->
        {
            String url = intent.getStringExtra("VideoUrl");
            String pageContentId = intent.getStringExtra("PageContentId");
            if (url == null) {
                checkClearAll(url);
                emitter.onError(new Exception());
                emitter.onComplete();
                return;
            }

            if (repositoryManager == null)
                repositoryManager = TopicRepositoryManager.getInstance();

            HttpMediaDrmCallback httpMediaDrmCallback = new HttpMediaDrmCallback(
                    repositoryManager.getOfflineDRMLicense(),
                    false, HelperApplication.getInstance().buildHttpDataSourceFactory());
            httpMediaDrmCallback.setKeyRequestProperty("authorization", repositoryManager.getAuthToken());
            httpMediaDrmCallback.setKeyRequestProperty("key", repositoryManager.getDRMKey());

            try {
                DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto> offlineLicenseHelper = new DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto>(C.WIDEVINE_UUID,
                        FrameworkMediaDrm.DEFAULT_PROVIDER,
                        httpMediaDrmCallback,
                        null);

                DataSource dataSource = HelperApplication.getInstance().buildHttpDataSourceFactory().createDataSource();
                DataSpec dataSpec = new DataSpec(Uri.parse(url));
                DataSourceInputStream inputStream = new DataSourceInputStream(dataSource, dataSpec);
                String manifestData = Util.fromUtf8Bytes(Util.toByteArray(inputStream));
                DashManifest dashManifest = DashUtil.loadManifest(new ByteArrayDataSource(manifestData.getBytes()), Uri.parse(url));
                DrmInitData drmInitData = DashUtil.loadDrmInitData(dataSource, dashManifest.getPeriod(0));
                byte[] offlineLicenses = offlineLicenseHelper.downloadLicense(drmInitData);
                Logger.d("OfflineKey", "OfflineKey " + offlineLicenses.toString() + " NEW " + Arrays.toString(offlineLicenses));
                Logger.d("License", "key " + decode(Arrays.toString(offlineLicenses)));
                Pair<Long, Long> durationRemainingSec = offlineLicenseHelper.getLicenseDurationRemainingSec(offlineLicenses);
                Logger.d("Duration Remaining Sec", durationRemainingSec.first + " " + durationRemainingSec.second);
                repositoryManager.saveOfflineLicense(url, manifestData, offlineLicenses, pageContentId, durationRemainingSec.first * 1000);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent alarmIntent = new Intent(context, AlarmReceiver.class);
                alarmIntent.putExtra("VideoUrl", url);
                alarmIntent.putExtra("PageContentId", pageContentId);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1 + new Random().nextInt(9999), alarmIntent, 0);
                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + durationRemainingSec.first * 1000, pendingIntent);

                checkClearAll(url);
                emitter.onComplete();

            } catch (Exception e) {
                checkClearAll(url);
                emitter.onError(e);
            }
        });

        observable
                .observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        if (disposableList != null)
                            disposableList.add(d);
                    }

                    @Override
                    public void onNext(Object o) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    public byte[] decode(String stringArray) {
        if (stringArray != null) {
            String[] split = stringArray.substring(1, stringArray.length() - 1).split(", ");
            byte[] array = new byte[split.length];
            for (int i = 0; i < split.length; i++) {
                array[i] = Byte.parseByte(split[i]);
            }
            Logger.d("DATA", "" + array.toString());
            return array;
        }

        return null;
    }

    public void checkClearAll(String url) {
        try {
            if (set != null && set.size() > 0) {
                set.remove(url);
            }
            if (set.size() == 0) {
                stopForeground(true);
                stopSelf();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (set != null && set.size() == 0) {
                stopForeground(true);
                stopSelf();
            }
        }
    }


    private Notification getNotification(String title, String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
                "ILEAP_NOTIFY_CHANNEL")
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setAutoCancel(false);


        if (title != null) {
            mBuilder.setContentTitle(title);
        } else {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                mBuilder.setContentTitle(Constants.APP_NAME);
            }
        }
        if (message != null) {
            mBuilder.setContentText(message);
        }

        mBuilder.setColor(Color.parseColor("#007aff"));
        mBuilder.setChannelId("ILEAP_NOTIFY_CHANNEL");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_NONE);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_NO_CLEAR;
        return notification;
    }


    public void removeAll() {
        if (disposableList != null) {
            for (Disposable disposable : disposableList) {
                if (disposable != null && disposable.isDisposed())
                    disposable.dispose();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }
}
