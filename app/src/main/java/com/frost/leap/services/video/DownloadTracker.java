/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.frost.leap.services.video;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.frost.leap.R;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadCursor;
import com.google.android.exoplayer2.offline.DownloadHelper;
import com.google.android.exoplayer2.offline.DownloadIndex;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.offline.ExoDownloadService;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.Log;
import com.google.android.exoplayer2.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

import apprepos.topics.TopicRepositoryManager;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Tracks media that has been downloaded.
 */
public class DownloadTracker {

    /**
     * Listens for changes in the tracked downloads.
     */
    public interface Listener {

        /**
         * Called when the tracked downloads changed.
         */
        void onDownloadsChanged();

        void onProgressChanged(int id, int percentage);
    }

    private static final String TAG = "DownloadTracker";

    private final Context context;
    private final DataSource.Factory dataSourceFactory;
    private final CopyOnWriteArraySet<Listener> listeners;
    private final HashMap<Uri, Download> downloads;
    private final DownloadIndex downloadIndex;


    @Nullable
    private StartDownloadDialogHelper startDownloadDialogHelper;

    public DownloadTracker(
            Context context,
            DataSource.Factory dataSourceFactory, DownloadManager downloadManager) {
        this.context = context.getApplicationContext();
        this.dataSourceFactory = dataSourceFactory;
        listeners = new CopyOnWriteArraySet<>();
        downloads = new HashMap<>();
        downloadIndex = downloadManager.getDownloadIndex();
        downloadManager.addListener(new DownloadManagerListener());
        loadDownloads();
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public boolean isDownloaded(Uri uri) {
        Download download = downloads.get(uri);
        return download != null && download.state != Download.STATE_FAILED;
    }


    public Download getDownload(Uri uri) {
        return downloads.get(uri);
    }

    public void deleteDownload(Uri uri) {
        Download download = downloads.get(uri);
        if (download != null) {
            ExoDownloadService.sendRemoveDownload(context, VideoDownloadService.class, download.request.id, /* foreground= */ false);
        }
    }


    public void deleteDownload(Uri uri, boolean foreground) {
        Download download = downloads.get(uri);
        if (download != null) {
            VideoDownloadService.notify = true;
            ExoDownloadService.sendRemoveDownload(context, VideoDownloadService.class, download.request.id, /* foreground= */ foreground);
        }
    }


    @SuppressWarnings("unchecked")
    public DownloadRequest getDownloadRequest(Uri uri) {
        Download download = downloads.get(uri);
        return download != null && download.state != Download.STATE_FAILED ? download.request : null;
    }

    public void toggleDownload(
            FragmentManager fragmentManager,
            String name,
            Uri uri,
            String extension,
            int quality,
            RenderersFactory renderersFactory) {
        Download download = downloads.get(uri);
        if (download != null) {
            if (download.getPercentDownloaded() < 100) {
                Utility.showToast(context, "Already in queue", false);
                return;
            }
            ExoDownloadService.sendRemoveDownload(context, VideoDownloadService.class, download.request.id, /* foreground= */ false);
            return;
        } else {

            new AsyncTask<Void, Void, Void>() {
                private DownloadHelper downloadHelper;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    downloadHelper = getDownloadHelper(quality, uri, extension, renderersFactory);
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    new StartDownloadDialogHelper(quality, fragmentManager, downloadHelper, name, uri);
                    return null;
                }
            }.execute();

        }
    }

    private void loadDownloads() {
        try (DownloadCursor loadedDownloads = downloadIndex.getDownloads()) {
            while (loadedDownloads.moveToNext()) {
                Download download = loadedDownloads.getDownload();
                downloads.put(download.request.uri, download);
            }
        } catch (Exception e) {
            Log.w(TAG, "Failed to query downloads", e);
        }
    }

    public HashMap<Uri, Download> getAllDownloads() {
        return downloads;
    }

    public DownloadHelper getDownloadHelper(
            int quality,
            Uri uri, String extension, RenderersFactory renderersFactory) {
        int type = Util.inferContentType(uri, extension);
        switch (type) {
            case C.TYPE_DASH:
                DefaultTrackSelector.Parameters parameters = new DefaultTrackSelector.ParametersBuilder(context)
                        .setMaxVideoBitrate(Utility.getBitrate(quality))
                        .build();

                Logger.d("Video download quality", "" + quality);
                return DownloadHelper.forDash(uri, dataSourceFactory,
                        renderersFactory,
                        TopicRepositoryManager.getInstance().generateDRMManager(Constants.APP_NAME),
                        parameters);
            case C.TYPE_SS:
                return DownloadHelper.forSmoothStreaming(context, uri, dataSourceFactory, renderersFactory);
            case C.TYPE_HLS:
                return DownloadHelper.forHls(context, uri, dataSourceFactory, renderersFactory);
            case C.TYPE_OTHER:
                return DownloadHelper.forProgressive(context, uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }


    }


    private class DownloadManagerListener implements DownloadManager.Listener {


        @Override
        public void onDownloadChanged(DownloadManager downloadManager, Download download) {
            downloads.put(download.request.uri, download);
            for (Listener listener : listeners) {
                listener.onDownloadsChanged();
            }
        }

        @Override
        public void onDownloadRemoved(DownloadManager downloadManager, Download download) {
            downloads.remove(download.request.uri);
            for (Listener listener : listeners) {
                listener.onDownloadsChanged();
            }
        }
    }

    private final class StartDownloadDialogHelper implements DownloadHelper.Callback,
            DialogInterface.OnClickListener,
            DialogInterface.OnDismissListener {

        private final FragmentManager fragmentManager;
        private final DownloadHelper downloadHelper;
        private final String name;
        private TrackSelectionDialog trackSelectionDialog;
        private MappedTrackInfo mappedTrackInfo;
        private Uri downloadUri;
        private int quality;

        public StartDownloadDialogHelper(int quality, FragmentManager fragmentManager, DownloadHelper downloadHelper, String name, Uri downloadUri) {
            this.quality = quality;
            this.fragmentManager = fragmentManager;
            this.downloadHelper = downloadHelper;
            this.name = name;
            this.downloadUri = downloadUri;
            downloadHelper.prepare(this);
        }

        public void release() {
            downloadHelper.release();
            if (trackSelectionDialog != null) {
                trackSelectionDialog.dismiss();
            }
        }

        // DownloadHelper.Callback implementation.

        @Override
        public void onPrepared(DownloadHelper helper) {
            if (helper.getPeriodCount() == 0) {
                Log.d(TAG, "No periods found. Downloading entire stream.");
                startDownload();
                downloadHelper.release();
                return;
            }
            mappedTrackInfo = downloadHelper.getMappedTrackInfo(/* periodIndex= */ 0);
            if (!TrackSelectionDialog.willHaveContent(mappedTrackInfo)) {
                Log.d(TAG, "No dialog content. Downloading entire stream.");
                startDownload();
                downloadHelper.release();
                return;
            }
            int trackGroup = -1;
            List<DefaultTrackSelector.SelectionOverride> selectionOverrides = new ArrayList<>();
            try {

                for (int i = 0; i < mappedTrackInfo.getTrackGroups(0).length; i++) {
                    TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(i);
                    for (int j = 0; j < trackGroupArray.get(i).length; j++) {
                        Logger.d("Resolutions", "" + trackGroupArray.get(i).getFormat(j).height);

                        if (quality == trackGroupArray.get(i).getFormat(j).height) {
                            trackGroup = i;
                            selectionOverrides.add(new DefaultTrackSelector.SelectionOverride(i, new int[]{j}, C.SELECTION_REASON_MANUAL, 0));
                            break;
                        }
                        if (quality > trackGroupArray.get(i).getFormat(j).height) {
                            trackGroup = i;
                            selectionOverrides.add(new DefaultTrackSelector.SelectionOverride(i, new int[]{j}, C.SELECTION_REASON_MANUAL, 0));
                            break;
                        }
                    }
                }

                if (trackGroup == -1) {
                    startDownload();
                    downloadHelper.release();
                    return;
                }

                for (int periodIndex = 0; periodIndex < downloadHelper.getPeriodCount(); periodIndex++) {
                    downloadHelper.clearTrackSelections(periodIndex);
                    for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
                        downloadHelper.addTrackSelectionForSingleRenderer(
                                periodIndex,
                                /* rendererIndex= */ i,
                                DownloadHelper.getDefaultTrackSelectorParameters(context),
                                periodIndex == 0 && i == trackGroup ? selectionOverrides : new ArrayList<>());
                    }
                }

                DownloadRequest downloadRequest = buildDownloadRequest();
                startDownload(downloadRequest);

            } catch (Exception e) {
                e.printStackTrace();
                startDownload();
                downloadHelper.release();
            }

        }

        @Override
        public void onPrepareError(DownloadHelper helper, IOException e) {
            helper.release();
            if (context != null)
                Utility.showToast(context, context.getString(R.string.download_start_error), false);
            if (downloadUri != null)
                TopicRepositoryManager.getInstance().deleteDownloadSubTopicInfo(downloadUri.toString());
            Log.e(TAG, "Failed to start download", e);
        }
        // DialogInterface.OnClickListener implementation.

        @Override
        public void onClick(DialogInterface dialog, int which) {
            for (int periodIndex = 0; periodIndex < downloadHelper.getPeriodCount(); periodIndex++) {
                downloadHelper.clearTrackSelections(periodIndex);
                for (int i = 0; i < mappedTrackInfo.getRendererCount(); i++) {
                    if (!trackSelectionDialog.getIsDisabled(/* rendererIndex= */ i)) {
                        downloadHelper.addTrackSelectionForSingleRenderer(
                                periodIndex,
                                /* rendererIndex= */ i,
                                DownloadHelper.getDefaultTrackSelectorParameters(context),
                                trackSelectionDialog.getOverrides(/* rendererIndex= */ i));
                    }
                }
            }
            DownloadRequest downloadRequest = buildDownloadRequest();
            if (downloadRequest.streamKeys.isEmpty()) {
                // All tracks were deselected in the dialog. Don't start the download.
                return;
            }
            startDownload(downloadRequest);
        }
        // DialogInterface.OnDismissListener implementation.

        @Override
        public void onDismiss(DialogInterface dialogInterface) {
            trackSelectionDialog = null;
            downloadHelper.release();
        }

        // Internal methods.

        private void startDownload() {
            startDownload(buildDownloadRequest());
        }

        private void startDownload(DownloadRequest downloadRequest) {
            try {
                ExoDownloadService.sendAddDownload(
                        context, VideoDownloadService.class, downloadRequest, /* foreground= */ false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            downloadHelper.release();
        }

        private DownloadRequest buildDownloadRequest() {
            return downloadHelper.getDownloadRequest(Util.getUtf8Bytes(name));
        }
    }


    public void retryDownload(Context context, Download download) {
        DownloadRequest request = download.request;
        ExoDownloadService.sendAddDownload(context, VideoDownloadService.class, request, /* foreground= */ false);
    }
}
