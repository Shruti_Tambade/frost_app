package com.frost.leap.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;

import android.widget.Toast;

import com.frost.leap.services.models.FileDownload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.net.ssl.SSLException;

import apprepos.subject.ResourceRepositoryManager;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 17-09-2019.
 * <p>
 * Frost
 */
public class FileDownloadService extends Service {

    private NotificationManager notificationManager;
    private Map<String, FileDownload> downloadMap;
    private int foregroundNotifyId = 1439;
    private Context context;
    private ResourceRepositoryManager repositoryManager;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();
        downloadMap = new HashMap<>();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        repositoryManager = ResourceRepositoryManager.getInstance();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.d("onStartCommand", "onStartCommand");

        if (intent == null) {
            return super.onStartCommand(intent, flags, startId);
        }
        if (intent.hasExtra("Cancel")) {
//            Toast.makeText(getApplicationContext(), "Download resources cancelled", Toast.LENGTH_SHORT).show();
            Utility.showToast(getApplicationContext(), "Download resources cancelled", false);
            Set<Thread> allThreads = Thread.getAllStackTraces().keySet();
            for (Thread thread : allThreads) {
                if (("" + thread.getName()).contains("FD_THREAD_".toLowerCase())) {
                    thread.interrupt();
                }
            }

            if (notificationManager == null)
                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (downloadMap != null)
                for (String key : downloadMap.keySet()) {
                    updateStatus(downloadMap.get(key), true);
                    File file = new File(getExternalFilesDir(null) + File.separator + "local" + File.separator +
                            downloadMap.get(key).getName());
                    if (file.exists())
                        file.delete();
                }
            stopForeground(true);
            stopSelf();
            notificationManager.cancel(foregroundNotifyId);
            repositoryManager.deleteAllPendingDownloads();
            return super.onStartCommand(intent, flags, startId);
        }
        if (intent.hasExtra("CrossCheck")) {
            if (repositoryManager == null)
                repositoryManager = ResourceRepositoryManager.getInstance();
            Set<String> pendingList = repositoryManager.getAllPendingDownloads();
            if (pendingList != null && pendingList.size() > 0) {
                if (downloadMap != null && downloadMap.size() > 0) {
                    for (String key : downloadMap.keySet()) {
                        String downloadedUrl = downloadMap.get(key).getDownloadUrl();
                        if (downloadedUrl != null) {
                            int value = -1;
                            for (int i = 0; i < pendingList.size(); i++) {
                                if (pendingList.contains(downloadedUrl.toLowerCase())) {
                                    value = i;
                                }
                            }
                            if (value != -1)
                                pendingList.remove(value);
                        }
                    }
                } else {
                    downloadMap = new HashMap<>();
                }
                for (String url : pendingList) {
                    FileDownload fileDownload = repositoryManager.getFileDownload(url);
                    if (fileDownload != null)
                        downloadMap.put(fileDownload.getKey(), fileDownload);
                }

                if (downloadMap != null && downloadMap.size() > 0) {
                    if (notificationManager == null)
                        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(foregroundNotifyId, updateNotification());

                    File file = new File(getExternalFilesDir(null) + File.separator + "local");
                    if (!file.exists()) {
                        if (file.mkdirs()) {
                            Logger.d("Folder created");
                        } else {
                            Logger.d("Folder not created");
                        }
                    }

                    for (FileDownload fileDownload : downloadMap.values()) {
                        DownloadFileThread fileThread = new DownloadFileThread(fileDownload, fileDownload.getDownloadUrl(), fileDownload.getKey(),
                                new File(getExternalFilesDir(null) + File.separator + "local" + File.separator + fileDownload.getName()));
                        Thread thread = new Thread(fileThread);
                        thread.setName(("FD_THREAD_" + fileDownload.getKey() + "_" + System.currentTimeMillis()).toLowerCase());
                        thread.start();
                    }
                }

            }

            return super.onStartCommand(intent, flags, startId);
        }

        FileDownload fileDownload = intent.getParcelableExtra("FileDownload");
        if (downloadMap.containsKey(fileDownload.getKey())) {
            if (notificationManager == null)
                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(foregroundNotifyId, updateNotification());
        } else {
            downloadMap.put(fileDownload.getKey(), fileDownload);
            if (downloadMap.size() == 1) {
                startForeground(foregroundNotifyId, getNotification());
            } else {
                if (notificationManager == null)
                    notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                startForeground(foregroundNotifyId, getNotification());
            }
        }
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return super.onStartCommand(intent, flags, startId);
        }


        File file = new File(getExternalFilesDir(null) + File.separator + "local");
        if (!file.exists()) {
            if (file.mkdirs()) {
                Logger.d("Folder created");
            } else {
                Logger.d("Folder not created");
            }
        } else {
            Logger.d("Folder already created");
            int padding = 0;
            String name = fileDownload.getName();
            while (true) {
                padding++;
                if (new File(getExternalFilesDir(null) + File.separator + "local" + File.separator + fileDownload.getName()).exists()) {
                    fileDownload.setName("(" + padding + ") " + name);
                    downloadMap.put(fileDownload.getKey(), fileDownload);
                } else {
                    break;
                }
            }
        }
        DownloadFileThread fileThread = new DownloadFileThread(fileDownload, fileDownload.getDownloadUrl(), fileDownload.getKey(),
                new File(getExternalFilesDir(null) + File.separator + "local" + File.separator + fileDownload.getName()));
        Thread thread = new Thread(fileThread);
        thread.setName(("FD_THREAD_" + fileDownload.getKey() + "_" + System.currentTimeMillis()).toLowerCase());
        thread.start();

        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * provide the notification
     *
     * @return {@link Notification}
     */
    private Notification getNotification() {

        Intent intent = new Intent(getApplicationContext(), FileDownloadService.class);
        intent.putExtra("Cancel", true);
        PendingIntent cancelPendingIntent = null;
        cancelPendingIntent = PendingIntent.getService(getApplicationContext(), new Random().nextInt(9999)
                , intent, 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "ILEAP")
                .setContentTitle("Downloading ResourcesConstatns")
                .setAutoCancel(false)
                .setSmallIcon(android.R.drawable.stat_sys_download);


        builder.addAction(android.R.drawable.ic_delete, "CANCEL", cancelPendingIntent);

        builder.setContentText(downloadMap.size() + " in process");
        builder.setProgress(100, 0, true);

        if (notificationManager == null)
            notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("ILEAP", "Deep", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Notification notification = builder.build();
        //notificationManager.notify(foregroundNotifyId,notification);
        notification.flags = Notification.FLAG_NO_CLEAR;
        return notification;

    }

    /**
     * to update the progress bar in notification
     */
    private void updateProgress() {
        try {

            Intent intent = new Intent(getApplicationContext(), FileDownloadService.class);
            intent.putExtra("Cancel", true);
            PendingIntent cancelPendingIntent = null;
            cancelPendingIntent = PendingIntent.getService(getApplicationContext(), new Random().nextInt(9999)
                    , intent, 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "ILEAP")
                    .setContentTitle("Downloading ResourcesConstatns")
                    .setAutoCancel(false)
                    .setSmallIcon(android.R.drawable.stat_sys_download);


            builder.addAction(android.R.drawable.ic_delete, "CANCEL", cancelPendingIntent);


            builder.setProgress(100, getFileDownloadsPercentage(), false);
            builder.setContentText(downloadMap.size() + " in process");


            Notification notification = builder.build();
            notification.flags = Notification.FLAG_NO_CLEAR;

            if (notificationManager == null)
                notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("ILEAP", "Deep", NotificationManager.IMPORTANCE_LOW);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(foregroundNotifyId, notification);

            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * to update the network info
     *
     * @param isNetwork
     */
    private void updateNetworkInfo(boolean isNetwork) {

        try {

            Intent intent = new Intent(getApplicationContext(), FileDownloadService.class);
            intent.putExtra("Cancel", true);
            PendingIntent cancelPendingIntent = null;

            cancelPendingIntent = PendingIntent.getService(getApplicationContext(), new Random().nextInt(9999)
                    , intent, 0);


            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "ILEAP")
                    .setContentTitle("Downloading ResourcesConstatns")
                    .setContentText(isNetwork ? "network active" : "retrying...")
                    .setAutoCancel(false)
                    .setSmallIcon(android.R.drawable.stat_sys_download);

            builder.addAction(android.R.drawable.ic_delete, "CANCEL", cancelPendingIntent);

            //builder.setDefaults(Notification.DEFAULT_ALL);
            // builder.setProgress(100, 0, true);
            //builder.setSound(null, AudioManager.STREAM_VOICE_CALL);

            Notification notification = builder.build();
            notification.flags = Notification.FLAG_NO_CLEAR;

            if (notificationManager == null)
                notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("ILEAP", "Deep", NotificationManager.IMPORTANCE_LOW);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(foregroundNotifyId, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * to update process
     *
     * @param key
     * @param filePath
     */
    private void updateProcess(String key, File filePath) {

        try {

            if (notificationManager == null)
                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            int notifyId = downloadMap.get(key).getId();
            notificationManager.cancel(notifyId);
            notifyUrl(downloadMap.get(key).getName(), "Downloaded successfully!", filePath);
            if (repositoryManager != null)
                repositoryManager.removePendingDownloads(downloadMap.get(key).getDownloadUrl());
            downloadMap.remove(key);
            if (downloadMap.size() == 0) {
                if (repositoryManager != null)
                    repositoryManager.deleteAllPendingDownloads();
                stopForeground(true);
                stopSelf();
                notificationManager.cancel(foregroundNotifyId);
            } else {
                updateProgress();
            }
            return;
        } catch (Exception e) {
            e.printStackTrace();
            notificationManager.cancel(foregroundNotifyId);
        }
    }

    /**
     * @return
     */
    private Notification updateNotification() {
        try {

            Intent intent = new Intent(getApplicationContext(), FileDownloadService.class);
            intent.putExtra("Cancel", true);
            PendingIntent cancelPendingIntent = null;
            cancelPendingIntent = PendingIntent.getService(getApplicationContext(), new Random().nextInt(9999)
                    , intent, 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "ILEAP")
                    .setContentTitle("Downloading ResourcesConstatns")
                    .setAutoCancel(false)
                    .setSmallIcon(android.R.drawable.stat_sys_download);


            builder.addAction(android.R.drawable.ic_delete, "CANCEL", cancelPendingIntent);


            builder.setProgress(100, getFileDownloadsPercentage(), false);
            builder.setContentText(downloadMap.size() + " in process");


            Notification notification = builder.build();
            notification.flags = Notification.FLAG_NO_CLEAR;

            if (notificationManager == null)
                notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel("ILEAP", "Deep", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(notificationChannel);
            }


            return notification;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Notification();
    }

    /**
     * @param title   downloaded title
     * @param message
     * @param file
     */
    private void notifyUrl(String title, String message, File file) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Uri uri = FileProvider.getUriForFile(getApplicationContext(),
                getApplicationContext()
                        .getPackageName() + ".fileprovider", file);
        intent.setDataAndType(uri, Utility.getMimeType(file.getAbsolutePath()));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, intent, PendingIntent.FLAG_ONE_SHOT);

        Utility.notifyMessage(title, message, pendingIntent);
    }

    /**
     * to check internet
     *
     * @return
     */
    public boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                return false;
                /* aka, do nothing */
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public int getFileDownloadsPercentage() {
        int percentage = 0;
        long downloadedBytes = 0;
        long totalBytes = 0;
        if (downloadMap != null)
            for (String key : downloadMap.keySet()) {
                FileDownload fileDownload = downloadMap.get(key);
                downloadedBytes += fileDownload.getDownloadedBytes();
                totalBytes += fileDownload.getTotalBytes();
            }
        if (totalBytes != 0)
            percentage = (int) ((downloadedBytes * 100) / totalBytes);

        return percentage;
    }


    public void updateStatus(FileDownload fileDownload, boolean isDelete) {
        new UpdateFileDownload(fileDownload, isDelete).start();
    }


    /**
     * THREAD UPDATE THE FILE DOWNLOAD
     */

    public class UpdateFileDownload extends Thread {
        private FileDownload fileDownload;
        private boolean isDelete;

        public UpdateFileDownload(FileDownload fileDownload, boolean isDelete) {
            this.fileDownload = fileDownload;
            this.isDelete = isDelete;
        }

        @Override
        public void run() {
            super.run();
            if (repositoryManager == null)
                repositoryManager = ResourceRepositoryManager.getInstance();
            if (!isDelete)
                repositoryManager.createOrUpdateResource(fileDownload.getDownloadUrl(), fileDownload);
            else {
                repositoryManager.deleteResource(fileDownload.getDownloadUrl());
                repositoryManager.removePendingDownloads(fileDownload.getDownloadUrl());
            }
        }
    }

    /**
     * FILE DOWNLOAD RUNNABLE THREAD
     */
    public class DownloadFileThread implements Runnable {
        private String downloadUrl;
        private String key;
        private File file;
        private long previous = 0;
        private HttpURLConnection httpURLConnection;
        private FileDownload fileDownload;

        public DownloadFileThread(FileDownload fileDownload, String downloadUrl, String key, File file) {
            this.downloadUrl = downloadUrl;
            this.key = key;
            this.file = file;
            this.fileDownload = fileDownload;
            previous = fileDownload.getDownloadedBytes();
        }


        @Override
        public void run() {

            try {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                Logger.d("File path", file.getAbsolutePath());
                if (!file.exists()) {
                    file.createNewFile();
                }
                if (file.length() != previous) {
                    previous = file.length();
                }
                fileDownload.setFilePath(file.getAbsolutePath());
                updateStatus(fileDownload, false);
                downloadMap.put(key, fileDownload);

                try {

                    URL url = new URL(downloadUrl);// Create Download URl
                    httpURLConnection = (HttpURLConnection) url.openConnection();// Open Url Connection
                    httpURLConnection.setRequestMethod("GET"); //Set Request Method to "GET" since we are getting data
                    httpURLConnection.connect();//connect the URL Connection
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        fileDownload.setTotalBytes(httpURLConnection.getContentLengthLong());
                        downloadMap.get(key).setTotalBytes(httpURLConnection.getContentLengthLong());
                    } else {
                        fileDownload.setTotalBytes(httpURLConnection.getContentLength());
                        downloadMap.get(key).setTotalBytes(httpURLConnection.getContentLength());
                    }

                    //If Connection response is not OK then show Logs
                    if (httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Logger.d("FILE DOWNLOAD", "Server returned HTTP " + httpURLConnection.getResponseCode() + " " + httpURLConnection.getResponseMessage());
                    }

                    FileOutputStream fos = new FileOutputStream(file, previous == 0 ? false : true);//Get OutputStream for NewFile Location
                    InputStream is = httpURLConnection.getInputStream();//Get InputStream for connection
                    byte[] buffer = new byte[Constants.BUFFER_SIZE];//Set buffer type
                    int length = 0;


                    if (previous != 0)
                        is.skip(previous);


                    int showProgress = 0;
                    while ((length = is.read(buffer)) != -1) {

                        Logger.d("" + previous + " , " + fileDownload.getTotalBytes());
                        fos.write(buffer, 0, length);//Write new file
                        previous += length;
                        fileDownload.setInProcess(1);
                        fileDownload.setDownloadedBytes(previous);
                        fileDownload.setPercentage((int) (previous * 100 / fileDownload.getTotalBytes()));
                        Logger.d("PERCENTAGE " + fileDownload.getPercentage());
                        downloadMap.put(key, fileDownload);
                        if (showProgress == 5) {
                            updateStatus(fileDownload, false);
                            updateProgress();
                            showProgress = 0;
                        }
                        showProgress++;

                    }
                    fileDownload.setStatus(true);
                    fileDownload.setInProcess(2);
                    fileDownload.setPercentage(100);
                    downloadMap.put(key, fileDownload);
                    updateStatus(fileDownload, false);

                    //Close all connection after doing task
                    fos.close();
                    is.close();
                    fos.flush();

                    updateProcess(key, file);
                    Logger.d("Key: " + key, downloadUrl);
                    Logger.d("File: " + key, file.getAbsolutePath());

                } catch (SSLException e) {
                    httpURLConnection.disconnect();
                    e.printStackTrace();
                    updateNetworkInfo(false);
                    while (!isNetworkAvailable()) {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    updateNetworkInfo(true);
                    run();
                } catch (UnknownHostException e) {
                    httpURLConnection.disconnect();
                    e.printStackTrace();
                    updateNetworkInfo(false);
                    while (!isNetworkAvailable()) {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    updateNetworkInfo(true);
                    run();
                } catch (SocketException e) {
                    httpURLConnection.disconnect();
                    e.printStackTrace();
                    updateNetworkInfo(false);
                    while (!isNetworkAvailable()) {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }
                    }
                    updateNetworkInfo(true);
                    run();
                }
                //notifyUrl(downloadUrl);

            } catch (InterruptedIOException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                // SELF CANCEL DUE TO SOMETHING WENT WRONG
                Intent intent = new Intent(getApplicationContext(), FileDownloadService.class);
                intent.putExtra("Cancel", true);
                startService(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }


}
