package com.frost.leap.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import com.frost.leap.R;
import com.frost.leap.controllers.AppController;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.UserRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 20-05-2020.
 * <p>
 * Frost
 */
public class FreshDeskForegroundService extends Service {

    private UserRepositoryManager userRepositoryManager;
    private int NOTIFY_ID = 7891;
    private List<Disposable> disposables;

    @Override
    public void onCreate() {
        super.onCreate();
        disposables = new ArrayList<>();
        userRepositoryManager = UserRepositoryManager.getInstance();
        startForeground(NOTIFY_ID, getNotification(null, "Requesting for Ask an expert", android.R.drawable.stat_sys_upload));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent != null && intent.getIntExtra(Constants.ACTION_KEY, -1) != -1) {
            switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {

                case AppController.ACTION_ASK_AN_EXPERT:
                    askAnExpert(intent);
                    break;

            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void askAnExpert(Intent intent) {
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {
                disposables.add(d);
            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response.code() == 201 || response.code() == 200) {
                    Utility.notifyMessage(null, "Your ticket is added, we will get back to you soon", true);
                    Logger.d("Your ticket is added to fresh-desk");
                } else {
                    Utility.notifyMessage(null, "Your ticket is failed to add. Please try again", false);
                    Logger.d("Your ticket is added to fresh-desk is failed");
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                Utility.notifyMessage(null, "Your ticket is failed to add. Please try again", false);
                clearAll();

            }

            @Override
            public void onComplete() {
                clearAll();
            }
        };

        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

//        String[] tags = {intent.getStringExtra("SubjectName"), intent.getStringExtra("UnitName"), intent.getStringExtra("TopicName"), intent.getStringExtra("SubTopicName")};

//        Logger.d("tags", tags.toString());

        String description = "Subject: " + intent.getStringExtra("SubjectName") + "<br/>"
                + "Unit: " + intent.getStringExtra("UnitName") + "<br/>"
                + "Topic: " + intent.getStringExtra("TopicName") + "<br/>"
                + "SubTopic: " + intent.getStringExtra("SubTopicName") + "<br/>" + "<br/>"
                + "Description: " + "<br/>"
                + intent.getStringExtra("Message");

        userRepositoryManager.askAnExpert(
                intent.getStringExtra("IssueType"),
                description,
                intent.getStringExtra("SubjectInfo"),
                intent.getParcelableArrayListExtra("GalleryFileList"),
                null)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);

    }


    public void clearAll() {
        if (disposables != null && disposables.size() > 0) {
            boolean done = true;
            for (Disposable disposable : disposables) {
                if (disposable != null && !disposable.isDisposed()) {
                    done = false;
                    break;
                }
            }
            if (done) {
                stopForeground(true);
                stopSelf();
            }
        } else {
            stopForeground(true);
            stopSelf();
        }
    }


    private Notification getNotification(String title, String message, int drawable) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
                "ILEAP_NOTIFY_CHANNEL_ALERT")
                .setSmallIcon(drawable)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(false);


        if (title != null) {
            mBuilder.setContentTitle(title);
        } else {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                mBuilder.setContentTitle(Constants.APP_NAME);
            }
        }
        if (message != null) {
            mBuilder.setContentText(message);
        }

        mBuilder.setColor(Color.parseColor("#007aff"));
        mBuilder.setChannelId("ILEAP_NOTIFY_CHANNEL_ALERT");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL_ALERT", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_NO_CLEAR;

        notificationManager.notify(NOTIFY_ID, notification);

        return notification;
    }


}
