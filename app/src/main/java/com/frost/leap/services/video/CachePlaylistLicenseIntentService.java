package com.frost.leap.services.video;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.frost.leap.applications.HelperApplication;
import com.frost.leap.controllers.AppController;
import com.google.android.exoplayer2.upstream.DataSourceInputStream;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

import java.util.List;

import apprepos.topics.TopicRepositoryManager;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 12-05-2020.
 * <p>
 * Frost
 */
public class CachePlaylistLicenseIntentService extends IntentService {


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public CachePlaylistLicenseIntentService() {
        super(CachePlaylistLicenseIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null)
            return;

        if (!Utility.isNetworkAvailable(this)) {
            return;
        }

        switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
            case AppController.ACTION_CACHE_PLAYLIST_URLS_LICENSE:
                downloadPendingLicense(intent);
                break;
        }
    }

    private void downloadPendingLicense(Intent intent) {
        List<String> ids = intent.getStringArrayListExtra("PageContentIds");
        Observable<Boolean> observable = Observable.create(emitter -> {
            try {
                if (!Utility.isNetworkAvailable(this)) {
                    emitter.onNext(true);
                    emitter.onComplete();
                    return;
                }
                DashUrlDAO dashUrlDAO = TopicRepositoryManager.getInstance().getDashUrlDAO();

                List<DashUrlsModel> list = dashUrlDAO.getDashUrlsEmptyDashManifest(ids);

                if (list == null || list.size() == 0) {
                    emitter.onNext(true);
                    emitter.onComplete();
                    return;
                }

                for (DashUrlsModel dashUrlsModel : list) {

                    if (dashUrlsModel == null)
                        continue;

                    if (TextUtils.isEmpty(dashUrlsModel.getDashUrl()))
                        continue;

                    try {
                        HttpDataSource dataSource = HelperApplication.getInstance().buildHttpDataSourceFactory().createDataSource();
                        DataSpec dataSpec = new DataSpec(Uri.parse(dashUrlsModel.getDashUrl()));
                        DataSourceInputStream inputStream = new DataSourceInputStream(dataSource, dataSpec);
                        String manifestData = Util.fromUtf8Bytes(Util.toByteArray(inputStream));
                        dashUrlsModel.setManifestData(manifestData);
                        dashUrlDAO.pushUpdatedDashUrl(dashUrlsModel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                emitter.onComplete();

            } catch (Exception e) {
                e.printStackTrace();
                if (!emitter.isDisposed()) {
                    emitter.onError(e);
                }
            }
        });
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
