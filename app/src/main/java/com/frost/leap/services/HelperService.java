package com.frost.leap.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.frost.leap.components.profile.models.MenuItem;
import com.frost.leap.controllers.AppController;
import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.JsonObject;

import apprepos.subject.NoteRepositoryManager;
import apprepos.topics.model.reviewmodel.RatingModel;
import apprepos.user.UserRepositoryManager;
import apprepos.user.model.AdditionalDetails;
import apprepos.user.model.User;
import apprepos.video.VideoRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 18-09-2019.
 * <p>
 * Frost
 */
public class HelperService extends Service {


    private UserRepositoryManager userRepositoryManager;
    private VideoRepositoryManager videoRepositoryManager;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getIntExtra(Constants.ACTION_KEY, -1) != -1) {
            switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {

                case AppController.ACTION_UPDATE_USER_PROFILE:
                    updateUser(intent.getParcelableExtra("User"));
                    break;


                case AppController.ACTION_UPDATE_ADDITIONAL_DETAILS:
                    updateUserAdditionalDetails(intent.getParcelableExtra("AdditionalDetails"));
                    break;

                case AppController.ACTION_ADD_FRESH_DESK_TICKET:
                    addFreshDeskTicket(intent);
                    break;

                case AppController.ACTION_ASK_AN_EXPERT:
                    askAnExpert(intent);
                    break;

                case AppController.ACTION_UPDATE_RATING:
                    updateRating(intent.getParcelableExtra("RATING"));
                    break;
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    // ADD REFRESH TOKEN
    public void updateUser(User user) {

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {
                    userRepositoryManager.saveUserDetails(user);

                    Logger.d("USER PROFILE", "UPDATED");
                } else if (response.code() == 500) {
                    Logger.d("ERROR", Utility.showErrorMessage(response.errorBody()));
                } else if (response.code() == 401) {
                    Logger.d("USER PROFILE", "TOKEN EXIPRED");
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateUser(user);
                        }
                    });
                } else {
                    Logger.d("ERROR", Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };


        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        userRepositoryManager.updateUserDetails(user)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }


    public void updateUserAdditionalDetails(AdditionalDetails additionalDetails) {

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response != null && response.code() == 200) {

                    Logger.d("USER ADDITIONAL DETAILS", "UPDATED");
                } else if (response.code() == 500) {
                    Logger.d("ERROR", Utility.showErrorMessage(response.errorBody()));
                } else if (response.code() == 401) {
                    Logger.d("USER ADDITIONAL DETAILS", "TOKEN EXPIRES");
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateUserAdditionalDetails(additionalDetails);
                        }
                    });
                } else {
                    Logger.d("ERROR", Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };


        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        userRepositoryManager.updateAdditionalDetails(additionalDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }


    private void addFreshDeskTicket(Intent intent) {

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                if (response.code() == 201 || response.code() == 200) {
                    Logger.d("Your ticket is added to freshdesk");
                } else {
                    Logger.d("Your ticket is added to freshdesk is failed");
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };


        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        MenuItem menuItem = null;

        if (intent.getParcelableExtra("MenuItem") != null) {
            menuItem = new MenuItem();
            menuItem = intent.getParcelableExtra("MenuItem");

        }

        userRepositoryManager.addFreshDeskTicket(
                intent.getStringExtra("IssueType"),
                intent.getStringExtra("Message"),
                intent.getStringExtra("CourseType"),
                intent.getParcelableArrayListExtra("GalleryFileList"),
                menuItem)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }

    private void refreshToken(BaseViewModel.IRefreshToken iRefreshToken) {
        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();
        userRepositoryManager.refreshToken()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if (response.code() == 200) {
                            userRepositoryManager.saveUserInfo(response.headers().get(Constants.AUTHORIZATION),
                                    response.body());
                            iRefreshToken.isTokenRefreshed(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    // Submit Review
    public void updateRating(RatingModel ratingModel) {

        if (!Utility.isNetworkAvailable(this)) {
            return;
        }
        Observer<Response<RatingModel>> observer = new Observer<Response<RatingModel>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Response<RatingModel> response) {
                if (response != null && response.code() == 200) {
                    Logger.d("RATING", "UPDATED");
                } else if (response.code() == 500) {
                    Logger.d("ERROR", Utility.showErrorMessage(response.errorBody()));
                } else if (response.code() == 401) {
                    Logger.d("RATING", "TOKEN EXIPRED");
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateRating(ratingModel);
                        }
                    });
                } else {
                    Logger.d("ERROR", Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };


        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        userRepositoryManager.updateRatingDetails(ratingModel)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }

    // Ask an expert
    private void askAnExpert(Intent intent) {
        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                if (response.code() == 201 || response.code() == 200) {
                    Logger.d("Your ticket is added to freshdesk");
                } else {
                    Logger.d("Your ticket is added to freshdesk is failed");
                }

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();

        String[] tags = {intent.getStringExtra("SubjectName"), intent.getStringExtra("UnitName"), intent.getStringExtra("TopicName"), intent.getStringExtra("SubTopicName")};

        Logger.d("tags", tags.toString());

        userRepositoryManager.askAnExpert(
                intent.getStringExtra("IssueType"),
                intent.getStringExtra("Message"),
                intent.getStringExtra("SubjectInfo"),
                intent.getParcelableArrayListExtra("GalleryFileList"),
                tags)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);

    }

}
