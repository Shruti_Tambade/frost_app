package com.frost.leap.services.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 17-09-2019.
 * <p>
 * Frost
 */
public class FileDownload implements Parcelable {
    private int id;
    private String name;
    private String downloadUrl;
    private int completed = 0;
    private int inProcess = 0;
    private boolean status = false;
    private String key = null;
    private long totalBytes = 0;
    private long downloadedBytes = 0;
    private int percentage = 0;
    private String filePath = null;

    protected FileDownload(Parcel in) {
        id = in.readInt();
        name = in.readString();
        downloadUrl = in.readString();
        completed = in.readInt();
        inProcess = in.readInt();
        status = in.readByte() != 0;
        key = in.readString();
        totalBytes = in.readLong();
        downloadedBytes = in.readLong();
        percentage = in.readInt();
        filePath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(downloadUrl);
        dest.writeInt(completed);
        dest.writeInt(inProcess);
        dest.writeByte((byte) (status ? 1 : 0));
        dest.writeString(key);
        dest.writeLong(totalBytes);
        dest.writeLong(downloadedBytes);
        dest.writeInt(percentage);
        dest.writeString(filePath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FileDownload> CREATOR = new Creator<FileDownload>() {
        @Override
        public FileDownload createFromParcel(Parcel in) {
            return new FileDownload(in);
        }

        @Override
        public FileDownload[] newArray(int size) {
            return new FileDownload[size];
        }
    };

    public long getDownloadedBytes() {
        return downloadedBytes;
    }

    public void setDownloadedBytes(long downloadedBytes) {
        this.downloadedBytes = downloadedBytes;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public FileDownload(int id, String name, String downloadUrl, String key) {
        this.id = id;
        this.name = name;
        this.downloadUrl = downloadUrl;
        this.key = key;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getInProcess() {
        return inProcess;
    }

    public void setInProcess(int inProcess) {
        this.inProcess = inProcess;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }
}
