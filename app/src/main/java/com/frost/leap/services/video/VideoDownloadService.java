/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.frost.leap.services.video;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.frost.leap.R;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.controllers.AppController;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.ExoDownloadService;
import com.google.android.exoplayer2.scheduler.PlatformScheduler;
import com.google.android.exoplayer2.ui.DownloadNotificationHelper;
import com.google.android.exoplayer2.util.NotificationUtil;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

import apprepos.topics.TopicRepositoryManager;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Mixpanel;

/**
 * A service for downloading media.
 */
public class VideoDownloadService extends ExoDownloadService {

    public static boolean notify = true;
    public static boolean forceStop = false;
    private static final String CHANNEL_ID = "ILEAP";
    private static final int JOB_ID = 1;
    private static final int FOREGROUND_NOTIFICATION_ID = 1;
    private static int nextNotificationId = FOREGROUND_NOTIFICATION_ID + 1;
    public static final int EXO_DEFAULT_FOREGROUND_NOTIFICATION_UPDATE_INTERVAL = 5000;
    public static VideoDownloadService videoDownloadService;
    private DownloadNotificationHelper notificationHelper;

    public VideoDownloadService() {
        super(
                FOREGROUND_NOTIFICATION_ID,
                EXO_DEFAULT_FOREGROUND_NOTIFICATION_UPDATE_INTERVAL,
                CHANNEL_ID,
                R.string.exo_download_notification_channel_name,
                /* channelDescriptionResourceId= */ 0);
        nextNotificationId = FOREGROUND_NOTIFICATION_ID + 1;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        videoDownloadService = this;
        notificationHelper = new DownloadNotificationHelper(this, CHANNEL_ID);
        if (notify) {
            startForeground(FOREGROUND_NOTIFICATION_ID, getNotification(null, "Checking"));
        }
        notify = true;
    }

    @Override
    protected DownloadManager getDownloadManager() {
        DownloadManager downloadManager = ((HelperApplication) getApplication()).getDownloadManager();
        downloadManager.addListener(
                new TerminalStateNotificationHelper(
                        this, notificationHelper, FOREGROUND_NOTIFICATION_ID + 1));
        return downloadManager;
    }

    @Override
    protected PlatformScheduler getScheduler() {
        return Util.SDK_INT >= 21 ? new PlatformScheduler(this, JOB_ID) : null;
    }

    @Override
    protected Notification getForegroundNotification(List<Download> downloads) {
        Logger.d("RUNNING", "NOTIFICATION " + System.currentTimeMillis() / 1000);
        return buildProgressNotification("Videos", downloads);
    }


    private Notification getNotification(String key, int noOfTasks, int progress, boolean indeterminate, String download) {

//        Intent intent = new Intent(this, SplashActivity.class);
//        intent.putExtra("VIEW", true);
//        intent.putExtra("Key", key);
//        PendingIntent cancelPendingIntent = null;
//        cancelPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setOngoing(false)
                .setContentTitle(indeterminate ? "Processing" : "Downloading " + key)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setSmallIcon(android.R.drawable.stat_sys_download);

        builder.setProgress(100, progress, indeterminate);

        if (noOfTasks != 0)
            builder.setContentText(progress + "% ( " + download + " )");
        if (noOfTasks != 0)
            builder.setSubText(noOfTasks + " in process");
        builder.setColor(Color.parseColor("#007aff"));

        Notification notification = builder.build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, Constants.APP_NAME, NotificationManager.IMPORTANCE_NONE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        return notification;

    }

    public Notification buildProgressNotification(
            String title,
            List<Download> downloads) {
        String downloadedMB = "0 MB";
        String totalMB = "0 MB";
        long downloadedBytes = 0;
        long totalBytes = 0;
        float totalPercentage = 0;
        int downloadTaskCount = 0;
        boolean allDownloadPercentagesUnknown = true;
        boolean haveDownloadedBytes = false;
        boolean haveDownloadTasks = false;
        boolean haveRemoveTasks = false;
        for (int i = 0; i < downloads.size(); i++) {
            Download download = downloads.get(i);
            if (download.state == Download.STATE_REMOVING) {
                haveRemoveTasks = true;
                continue;
            }
            if (download.state != Download.STATE_RESTARTING
                    && download.state != Download.STATE_DOWNLOADING) {
                continue;
            }
            haveDownloadTasks = true;
            totalBytes += download.contentLength;
            downloadedBytes += download.getBytesDownloaded();
            float downloadPercentage = download.getPercentDownloaded();
            if (downloadPercentage != C.PERCENTAGE_UNSET) {
                allDownloadPercentagesUnknown = false;
                totalPercentage += downloadPercentage;
            }
            haveDownloadedBytes |= download.getBytesDownloaded() > 0;
            downloadTaskCount++;

            if (download.getPercentDownloaded() >= 20 && download.getPercentDownloaded() < 22) {
                checkLicenseExistElseReDownload(this, download.request.uri.toString());
            }
        }


        int progress = 0;
        boolean indeterminate = true;
        if (haveDownloadTasks) {
            progress = (int) (totalPercentage / downloadTaskCount);
            indeterminate = allDownloadPercentagesUnknown && haveDownloadedBytes;
        }
        DecimalFormat df = new DecimalFormat("#.#");
        downloadedMB = "" + df.format((double) downloadedBytes / (1024 * 1024)) + " MB";


        return getNotification(title, downloadTaskCount, progress, indeterminate, downloadedMB);
    }


    private static final class TerminalStateNotificationHelper implements DownloadManager.Listener {

        private final Context context;
        private DownloadNotificationHelper downloadNotificationHelper;
        private int nextNotificationId;

        public TerminalStateNotificationHelper(
                Context context, DownloadNotificationHelper downloadNotificationHelper, int firstNotificationId) {
            this.context = context.getApplicationContext();
            nextNotificationId = firstNotificationId;
            this.downloadNotificationHelper = downloadNotificationHelper;
        }

        @Override
        public void onDownloadChanged(DownloadManager manager, Download download) {
            Notification notification;

            if (download.state == Download.STATE_COMPLETED) {

                if (videoDownloadService != null)
                    videoDownloadService.checkLicenseExistElseReDownload(context, download.request.uri.toString());
                JSONObject data = TopicRepositoryManager.getInstance().getDownloadSubTopicInfo(download.request.uri.toString());
                Mixpanel.videoDownloadSuccess(data);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ILEAP")
                        .setContentText("" + Util.fromUtf8Bytes(download.request.data))
                        .setContentTitle("Downloaded Successfully")
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_LOW)
                        .setColor(Color.parseColor("#007aff"))
                        .setSmallIcon(R.drawable.notify_logo);

                notification = builder.build();
            } else if (download.state == Download.STATE_FAILED) {


                Intent resumeDownload = ExoDownloadService.buildAddDownloadIntent(context, VideoDownloadService.class, download.request, /* foreground= */ false);
                resumeDownload.putExtra("NotificationId", nextNotificationId);
                PendingIntent pendingIntent;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    pendingIntent = PendingIntent.getForegroundService(context, 1 + new Random().nextInt(9999), resumeDownload, 0);
                } else {
                    pendingIntent = PendingIntent.getService(context, 1 + new Random().nextInt(9999), resumeDownload, 0);
                }


                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ILEAP")
                        .setContentText("" + Util.fromUtf8Bytes(download.request.data))
                        .setContentTitle("Downloaded Failed")
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_LOW)
                        .setSmallIcon(R.drawable.ic_info_outline);

                if (download.getPercentDownloaded() > 0)
                    builder.addAction(R.drawable.ic_autorenew, "Resume", pendingIntent);

                notification = builder.build();
                notification.flags = Notification.FLAG_AUTO_CANCEL;

            } else {
                return;
            }
            NotificationUtil.setNotification(context, nextNotificationId++, notification);
        }


    }


    public void checkLicenseExistElseReDownload(Context context, String url) {

        Observable<DashUrlsModel> observable = Observable.create(emitter -> {
            try {
                DashUrlsModel dashUrlsModel = TopicRepositoryManager.getInstance().getDeepBaseProvider().dashUrlDAO().getDashUrlsByUrl(url);
                if (TopicRepositoryManager.getInstance().verifyUnderLicense(dashUrlsModel) == null) {
                    emitter.onNext(dashUrlsModel);
                } else
                    emitter.onNext(null);
                emitter.onComplete();
            } catch (Exception e) {
                if (!emitter.isDisposed())
                    emitter.onError(e);
            }

        });

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<DashUrlsModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(DashUrlsModel dashUrlsModel) {
                        if (dashUrlsModel != null) {
                            try {
                                Intent intent = new Intent(context, DownloadLicenseService.class);
                                intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_DOWNLOAD_DRM_LICENSE);
                                intent.putExtra("VideoUrl", url);
                                intent.putExtra("PageContentId", dashUrlsModel.getPageContentId());
                                ContextCompat.startForegroundService(context, intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }


    private Notification getNotification(String title, String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
                CHANNEL_ID)
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setAutoCancel(false);


        if (title != null) {
            mBuilder.setContentTitle(title);
        } else {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                mBuilder.setContentTitle(Constants.APP_NAME);
            }
        }
        if (message != null) {
            mBuilder.setContentText(message);
        }

        mBuilder.setColor(Color.parseColor("#007aff"));
        mBuilder.setChannelId(CHANNEL_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, Constants.APP_NAME, NotificationManager.IMPORTANCE_NONE);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_NO_CLEAR;
        return notification;
    }

}
