package com.frost.leap.services;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.frost.leap.controllers.AppController;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import apprepos.lead.LeadRepositoryManager;
import apprepos.lead.model.LeadModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 2020-07-13.
 * <p>
 * Frost Interactive
 */
public class LeadSquaredIntentService extends IntentService {

    public LeadSquaredIntentService() {
        super(null);// That was the lacking constructor, we have to discuss on this
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null && intent.getIntExtra(Constants.ACTION_KEY, -1) != -1) {
            switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
                case AppController.CREATE_LEAD:
                    createLead(intent);
                    break;
                case AppController.UPDATE_LEAD:
                    updateLead("");
                    break;
                case AppController.GET_LEAD_BY_EMAILID:
                    getLeadByLeadID(intent);
                    break;
            }
        }
    }

    private void createLead(Intent intent) {

        if (!Utility.isNetworkAvailable(this))
            return;

        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 201 || response.code() == 200) {
                    Logger.d("LEAD_SQUARED: ", "CreateLead operation done!");
                } else {
                    Logger.d("LEAD_SQUARED: ", "CreateLead operation failed");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        LeadRepositoryManager leadRepositoryManager = LeadRepositoryManager.getInstance();

        leadRepositoryManager.createLead("", "", "", "", "")
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);

    }

    private void getLeadByLeadID(Intent intent) {

        if (!Utility.isNetworkAvailable(this))
            return;


        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 201 || response.code() == 200) {
                    if (response.body() != null) {

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<LeadModel>>() {
                        }.getType();

                        List<LeadModel> leadModelList = gson.fromJson(response.body().toString(), type);

                        if (leadModelList != null && leadModelList.size() > 0)
                            updateLead(leadModelList.get(0).getProspectID());

                    }
                    Logger.d("LEAD_SQUARED: ", "GetLeadByID operation done!");
                } else {
                    Logger.d("LEAD_SQUARED: ", "GetLeadByID operation failed");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        LeadRepositoryManager leadRepositoryManager = LeadRepositoryManager.getInstance();

        leadRepositoryManager.getLeadByEmailID(leadRepositoryManager.appSharedPreferences.getString(Constants.UserInfo.EMAIL))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }

    private void updateLead(String leadID) {

        if (!Utility.isNetworkAvailable(this))
            return;

        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 201 || response.code() == 200) {
                    Logger.d("LEAD_SQUARED: ", "UpdateLead operation done!");
                } else {
                    Logger.d("LEAD_SQUARED: ", "UpdateLead operation failed");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        LeadRepositoryManager leadRepositoryManager = LeadRepositoryManager.getInstance();

        leadRepositoryManager.updateLead(leadID)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);

    }
}
