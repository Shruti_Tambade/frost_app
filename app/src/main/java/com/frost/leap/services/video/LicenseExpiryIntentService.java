package com.frost.leap.services.video;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;

import androidx.collection.ArraySet;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.frost.leap.R;
import com.frost.leap.applications.HelperApplication;
import com.frost.leap.controllers.AppController;
import com.google.android.exoplayer2.offline.Download;
import com.google.android.exoplayer2.offline.DownloadCursor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import apprepos.topics.TopicRepositoryManager;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.utils.Logger;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 20-02-2020.
 * <p>
 * FROST
 */
public class LicenseExpiryIntentService extends Service {

    /**
     * * <p>
     * *  important only for debugging.
     */
    private TopicRepositoryManager repositoryManager;
    private List<Disposable> list;
    private Set<String> urlsSet;
    public static final int LICENSE_EXPIRY_SERVICE_ID = 89771;
    public static int GROUP_COUNT = 10;
    public static final int EXACT_SIZE = 10;
    public Disposable verifyDisposable = null;
    public Disposable isCompleteDisposable = null;

    @Override
    public void onCreate() {
        super.onCreate();
        list = new ArrayList<>();
        urlsSet = new ArraySet<>();
        GROUP_COUNT = EXACT_SIZE;
        startForeground(LICENSE_EXPIRY_SERVICE_ID, getNotification(null, "Looking for incomplete downloads"));
        if (repositoryManager == null)
            repositoryManager = TopicRepositoryManager.getInstance();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForeground(3914, getNotification(null, "Looking for incomplete downloads"));
        if (intent == null) {
            removeAll();
            stopForeground(true);
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        switch (intent.getIntExtra(Constants.ACTION_KEY, 0)) {
            case AppController.ACTION_DELETE_VIDEO_AND_LICENSE_EXPIRY:
                if (intent.getStringExtra("PageContentId") != null && intent.getStringExtra("VideoUrl") != null) {
                    deleteLicense(Uri.parse(intent.getStringExtra("VideoUrl")));
                } else {
                    stopForeground(true);
                    stopSelf();
                }
                break;
            case AppController.ACTION_LICENCE_VERIFY:
                GROUP_COUNT = EXACT_SIZE;
                verifyLicense();
                break;
            case AppController.ACTION_DISMISS_LICENCE_VERIFY:
                removeAll();
                stopForeground(true);
                stopSelf();
                return super.onStartCommand(intent, flags, startId);

            default:
                stopForeground(true);
                stopSelf();
                return super.onStartCommand(intent, flags, startId);

        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Notification getNotification(String title, String message) {

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,
                "ILEAP_NOTIFY_CHANNEL")
                .setSmallIcon(android.R.drawable.stat_sys_download_done)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setAutoCancel(false);


        if (title != null) {
            mBuilder.setContentTitle(title);
        } else {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                mBuilder.setContentTitle(Constants.APP_NAME);
            }
        }
        if (message != null) {
            mBuilder.setContentText(message);
        }

        mBuilder.setColor(Color.parseColor("#007aff"));
        mBuilder.setChannelId("ILEAP_NOTIFY_CHANNEL");

        Intent intent = new Intent(this, LicenseExpiryIntentService.class);
        intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_DISMISS_LICENCE_VERIFY);
        PendingIntent pendingIntent = PendingIntent.getService(this, AppController.ACTION_DISMISS_LICENCE_VERIFY, intent, 0);
        mBuilder.addAction(R.drawable.ic_clear, "DISMISS", pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_NONE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Notification notification = mBuilder.build();
        notification.flags = Notification.FLAG_NO_CLEAR;
        return notification;
    }

    private void verifyLicense() {
        try {

            if (!TopicRepositoryManager.getInstance().getIsLicenseMigrated()) {
                stopForeground(true);
                stopSelf();
                return;
            }
            Logger.d("Complete", "Started verify License");
            if (verifyDisposable != null && !verifyDisposable.isDisposed()) {
                verifyDisposable.dispose();
                removeAll();
            }
            list = new ArrayList<>();
            Observable<Boolean> observable = Observable.create(emitter ->
            {
                try {
                    DownloadCursor cursor = HelperApplication.getInstance().getDownloadManager().getDownloadIndex().getDownloads(Download.STATE_COMPLETED);
                    if (cursor == null) {
                        stopForeground(true);
                        stopSelf();
                        emitter.onComplete();
                        return;
                    }
                    List<Download> items = new ArrayList<>();
                    List<Download> itemsClone = null;

                    int itemsCount = 0;
                    int groups = 1;
                    while (cursor.moveToNext()) {
                        if (groups == GROUP_COUNT + 1) {
                            GROUP_COUNT = GROUP_COUNT * 2;
                        }
                        Download download = cursor.getDownload();
                        if (items == null) {
                            items = new ArrayList<>();
                            itemsCount = 0;
                            groups++;
                        }
                        itemsCount++;
                        items.add(download);
                        if (itemsCount == GROUP_COUNT) {
                            itemsClone = new ArrayList<>();
                            itemsClone.addAll(items);
                            createObservable(groups, itemsClone);
                            items = null;
                        }

                    }
                    if (itemsCount != GROUP_COUNT) {
                        itemsClone = new ArrayList<>();
                        itemsClone.addAll(items);
                        createObservable(groups, itemsClone);
                    }
                    if (!cursor.isClosed())
                        cursor.close();
                    emitter.onComplete();
                } catch (Exception e) {
                    e.printStackTrace();
                    emitter.onError(e);
                }
            });
            observable.observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<Boolean>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            verifyDisposable = d;
                        }

                        @Override
                        public void onNext(Boolean aBoolean) {

                        }

                        @Override
                        public void onError(Throwable e) {
                            stopForeground(true);
                            stopSelf();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        } catch (Exception e) {
            e.printStackTrace();
            stopForeground(true);
            stopSelf();
        }
    }

    public void createObservable(int tag, List<Download> downloadList) {
        Observable<Boolean> observable = Observable.create(emitter ->
        {
            try {
                for (int i = 0; i < downloadList.size(); i++) {
                    try {
                        Download download = downloadList.get(i);
                        DashUrlDAO dashUrlDAO = TopicRepositoryManager.getInstance().getDeepBaseProvider().dashUrlDAO();
                        DashUrlsModel dashUrlsModel = dashUrlDAO.getDashUrlsByUrl(download.request.uri.toString());
                        if (dashUrlsModel == null) {
                            continue;
                        }
                        if (dashUrlsModel.getLicense() == null || dashUrlsModel.getIsDownloaded() == 0) {
                            downloadLicense(dashUrlsModel.getDashUrl(), dashUrlsModel.getPageContentId());
                            continue;
                        }

                        if (TopicRepositoryManager.getInstance().getOfflineLicenseByDashUrlsModel(dashUrlsModel, dashUrlsModel.getDashUrl()) == null) {
                            Logger.d("DELETE DOWNLOAD VIDEO URL");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                emitter.onNext(true);
                emitter.onComplete();

            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }
        });

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        if (list != null)
                            list.add(d);
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Logger.d("Throwable", tag + " " + downloadList.size() + " " + e.getMessage());
                        isCompleteAllDisposable();
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("Complete", tag + " " + downloadList.size());
                        isCompleteAllDisposable();
                    }
                });


    }

    private void downloadLicense(String videoUrl, String pageContentId) {
        try {
            Intent intent = new Intent(this, DownloadLicenseService.class);
            intent.putExtra(Constants.ACTION_KEY, AppController.ACTION_DOWNLOAD_DRM_LICENSE);
            intent.putExtra("VideoUrl", videoUrl);
            intent.putExtra("PageContentId", pageContentId);
            ContextCompat.startForegroundService(this, intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeAll() {
        try {
            if (list != null && list.size() > 0) {
                List<Disposable> cloneList = new ArrayList<>();
                cloneList.addAll(list);
                for (Disposable disposable : cloneList) {
                    if (disposable != null && !disposable.isDisposed())
                        disposable.dispose();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void isCompleteAllDisposable() {

        if (isCompleteDisposable != null && !isCompleteDisposable.isDisposed())
            isCompleteDisposable.dispose();

        Observable<Boolean> observable = Observable.create(emitter -> {

            try {

                if (list == null) {
                    emitter.onNext(true);
                    emitter.onComplete();
                    return;
                }
                if (list.size() == 0) {
                    emitter.onNext(true);
                    emitter.onComplete();
                    return;
                }
                List<Disposable> cloneList = new ArrayList<>();
                cloneList.addAll(list);
                for (Disposable d : cloneList) {
                    if (d != null && d.isDisposed()) {
                        list.remove(d);
                    }
                }

                if (list.size() == 0) {
                    emitter.onNext(true);
                    emitter.onComplete();
                }
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(e);
            }
        });
        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        isCompleteDisposable = d;
                    }

                    @Override
                    public void onNext(Boolean result) {
                        if (result) {
                            stopForeground(true);
                            stopSelf();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        stopForeground(true);
                        stopSelf();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    private void deleteLicense(Uri videoUrl) {
        if (urlsSet != null && !urlsSet.contains(videoUrl.toString())) {
            urlsSet.add(videoUrl.toString());
        }
        Observable<Boolean> observable = Observable.create(emitter ->
        {
            try {
                HashMap<Uri, Download> downloadHashMap = HelperApplication.getInstance().getDownloadTracker().getAllDownloads();
                if (downloadHashMap.get(videoUrl) != null) {
                    DashUrlDAO dashUrlDAO = TopicRepositoryManager.getInstance().getDeepBaseProvider().dashUrlDAO();
                    DashUrlsModel dashUrlsModel = dashUrlDAO.getDashUrlsByUrl(videoUrl.toString());
                    if (dashUrlsModel == null) {
                        urlsSet.remove(videoUrl.toString());
                        emitter.onNext(true);
                        emitter.onComplete();
                        return;
                    }
                    if (TopicRepositoryManager.getInstance().getOfflineLicenseByDashUrlsModel(dashUrlsModel, dashUrlsModel.getDashUrl()) == null) {
                        Logger.d("DELETE DOWNLOAD VIDEO URL");
                    }

                }
                urlsSet.remove(videoUrl.toString());
                emitter.onNext(true);
                emitter.onComplete();
            } catch (Exception e) {
                urlsSet.remove(videoUrl.toString());
                e.printStackTrace();
                emitter.onError(e);
            }

        });

        observable.observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean o) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        isDeleteAllDisposable();
                    }

                    @Override
                    public void onComplete() {
                        isDeleteAllDisposable();
                    }
                });


    }

    public void isDeleteAllDisposable() {
        if (urlsSet == null) {
            stopForeground(true);
            stopSelf();
            return;
        }
        if (urlsSet.size() == 0) {
            stopForeground(true);
            stopSelf();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }

}
