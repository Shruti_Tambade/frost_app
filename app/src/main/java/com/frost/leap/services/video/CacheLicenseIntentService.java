package com.frost.leap.services.video;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;

import androidx.annotation.Nullable;

import com.frost.leap.applications.HelperApplication;
import com.frost.leap.controllers.AppController;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.DeepLearnOfflineLicenseHelper;
import com.google.android.exoplayer2.source.dash.DashUtil;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.upstream.ByteArrayDataSource;
import com.google.android.exoplayer2.upstream.DataSourceInputStream;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

import java.util.Arrays;
import java.util.List;

import apprepos.topics.TopicRepositoryManager;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 11-05-2020.
 * <p>
 * Frost
 */
public class CacheLicenseIntentService extends IntentService {


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */

    public CacheLicenseIntentService() {
        super(CacheLicenseIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null)
            return;

        if (!Utility.isNetworkAvailable(this)) {
            return;
        }

        switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
            case AppController.ACTION_CACHE_PENDING_LICENSE:
                downloadPendingLicense();
                break;
        }
    }

    private void downloadPendingLicense() {
        Observable<Boolean> observable = Observable.create(emitter -> {
            try {
                if (!Utility.isNetworkAvailable(this)) {
                    emitter.onNext(true);
                    emitter.onComplete();
                    return;
                }
                DashUrlDAO dashUrlDAO = TopicRepositoryManager.getInstance().getDashUrlDAO();
                TopicRepositoryManager repositoryManager = TopicRepositoryManager.getInstance();

                List<DashUrlsModel> list = dashUrlDAO.getDashUrlsPendingDownloads(System.currentTimeMillis());

                if (list == null || list.size() == 0) {
                    emitter.onNext(true);
                    emitter.onComplete();
                    return;
                }

                for (DashUrlsModel dashUrlsModel : list) {

                    if (dashUrlsModel == null)
                        continue;

                    if (TextUtils.isEmpty(dashUrlsModel.getDashUrl()))
                        continue;

                    HttpMediaDrmCallback httpMediaDrmCallback = new HttpMediaDrmCallback(
                            repositoryManager.getOfflineDRMLicense(),
                            false, HelperApplication.getInstance().buildHttpDataSourceFactory());
                    httpMediaDrmCallback.setKeyRequestProperty("authorization", repositoryManager.getAuthToken());
                    httpMediaDrmCallback.setKeyRequestProperty("key", repositoryManager.getDRMKey());

                    try {
                        DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto> offlineLicenseHelper = new DeepLearnOfflineLicenseHelper<FrameworkMediaCrypto>(C.WIDEVINE_UUID,
                                FrameworkMediaDrm.DEFAULT_PROVIDER,
                                httpMediaDrmCallback,
                                null);

                        HttpDataSource dataSource = HelperApplication.getInstance().buildHttpDataSourceFactory().createDataSource();


                        DataSpec dataSpec = new DataSpec(Uri.parse(dashUrlsModel.getDashUrl()));
                        DataSourceInputStream inputStream = new DataSourceInputStream(dataSource, dataSpec);
                        String manifestData = Util.fromUtf8Bytes(Util.toByteArray(inputStream));
                        DashManifest dashManifest = DashUtil.loadManifest(new ByteArrayDataSource(manifestData.getBytes()), Uri.parse(dashUrlsModel.getDashUrl()));
                        DrmInitData drmInitData = DashUtil.loadDrmInitData(dataSource, dashManifest.getPeriod(0));

                        byte[] offlineLicenses = offlineLicenseHelper.downloadLicense(drmInitData);
                        Logger.d("CacheKey", "OfflineKey " + offlineLicenses.toString() + " NEW " + Arrays.toString(offlineLicenses));
                        Logger.d("Cache License Key", "key " + decode(Arrays.toString(offlineLicenses)));
                        Pair<Long, Long> durationRemainingSec = offlineLicenseHelper.getLicenseDurationRemainingSec(offlineLicenses);
                        Logger.d("Cache Duration Remaining Sec", durationRemainingSec.first + " " + durationRemainingSec.second);
                        dashUrlsModel.setTimeLimit(System.currentTimeMillis() + durationRemainingSec.first * 1000);
                        dashUrlsModel.setLicense(Arrays.toString(offlineLicenses));
                        dashUrlsModel.setManifestData(manifestData);

                        dashUrlDAO.pushUpdatedDashUrl(dashUrlsModel);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
                if (!emitter.isDisposed()) {
                    emitter.onError(e);
                }
            }
            emitter.onComplete();
        });
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public byte[] decode(String stringArray) {
        if (stringArray != null) {
            String[] split = stringArray.substring(1, stringArray.length() - 1).split(", ");
            byte[] array = new byte[split.length];
            for (int i = 0; i < split.length; i++) {
                array[i] = Byte.parseByte(split[i]);
            }
            Logger.d("DATA", "" + array.toString());
            return array;
        }

        return null;
    }

    private void downloadLicense(Intent intent) {

    }


}
