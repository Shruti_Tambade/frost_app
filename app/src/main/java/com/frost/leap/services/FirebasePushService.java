package com.frost.leap.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.frost.leap.BuildConfig;
import com.frost.leap.controllers.AppController;
import com.frost.leap.generic.ShareDataManager;
import com.frost.leap.services.models.Report;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apprepos.user.UserRepositoryManager;
import apprepos.user.model.TermsAccept;
import apprepos.user.model.User;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

public class FirebasePushService extends IntentService {


    private UserRepositoryManager userRepositoryManager;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public FirebasePushService() {
        super(FirebasePushService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null)
            return;
        if (intent != null && intent.getIntExtra(Constants.ACTION_KEY, -1) != -1) {
            switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
                case AppController.ACTION_REPORT_VIDEO:
                    sendVideoReportToFirebase(intent.getParcelableExtra("Report"));
                    break;
                case AppController.ACTION_IS_ACCEPT_TERMS:
                    isAcceptTerms();
                    break;
                case AppController.ACTION_ACCEPT_TERMS:
                    acceptTerms();
                    break;

            }
        }
    }


    private void isAcceptTerms() {
        try {
            userRepositoryManager = UserRepositoryManager.getInstance();
            User user = userRepositoryManager.getUser();
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();


            if (firebaseAuth.getCurrentUser() != null) {
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                DatabaseReference terms = firebaseDatabase.getReference(BuildConfig.DEBUG ? "test-terms-policies" : "terms-policies");
                terms.child("show").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot != null) {
                            if ((Boolean) dataSnapshot.getValue()) {
                                if (user.getStudentId() == null)
                                    return;

                                terms.child("users").child(user.getStudentId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue() == null) {
                                            ShareDataManager.getInstance().setIsAccept(false);
                                        } else {
                                            ShareDataManager.getInstance().setIsAccept(true);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            } else
                                ShareDataManager.getInstance().setIsAccept(true);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            } else {
                if (TextUtils.isEmpty(userRepositoryManager.getCredentials("key")))
                    return;
                firebaseAuth.signInWithEmailAndPassword(userRepositoryManager.getCredentials("key"),
                        userRepositoryManager.getCredentials("value")).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            isAcceptTerms();
                        } else {
                            task.getException().printStackTrace();
                            Logger.d("SignInWithEmailAndPassword", "Failed");
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void acceptTerms() {
        try {
            userRepositoryManager = UserRepositoryManager.getInstance();
            User user = userRepositoryManager.getUser();
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

            if (firebaseAuth.getCurrentUser() != null) {
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                DatabaseReference terms = firebaseDatabase.getReference(BuildConfig.DEBUG ? "test-terms-policies" : "terms-policies");
                if (user.getStudentId() == null)
                    return;

                TermsAccept termsAccept = new TermsAccept();
                termsAccept.setName(user.getProfileInfo().getFirstName() + " " + user.getProfileInfo().getLastName());
                termsAccept.setEmail(user.getCredentials().getEmail());
                termsAccept.setPhoneNumber(user.getCredentials().getMobile());
                termsAccept.setDateOfAccept(Utility.getCurrentDate());
                termsAccept.setTimeOfAccept(Utility.getCurrentTime());
                terms.child("users").child(user.getStudentId()).setValue(termsAccept).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Logger.d("Accept-Terms", "Accepted successfully");
                            ShareDataManager.getInstance().setIsAccept(true);
                            Utility.showToast(Utility.getContext(), true, "Thank you for accepting our updated Terms of Use & Privacy Policy. Happy Learning!");
                        } else {
                            task.getException().printStackTrace();
                            Logger.d("Accept-Terms", "Not accepted");
                        }
                        stopSelf();
                    }
                });


            } else {

                if (TextUtils.isEmpty(userRepositoryManager.getCredentials("key")))
                    return;

                firebaseAuth.signInWithEmailAndPassword(userRepositoryManager.getCredentials("key"),
                        userRepositoryManager.getCredentials("value")).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            acceptTerms();
                        } else {
                            task.getException().printStackTrace();
                            Logger.d("SignInWithEmailAndPassword", "Failed");
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void sendVideoReportToFirebase(Report report) {
        userRepositoryManager = UserRepositoryManager.getInstance();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() != null) {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference reports = firebaseDatabase.getReference(BuildConfig.DEBUG ? "test-reports" : "reports");
            report.setName(userRepositoryManager.getUser().getProfileInfo().getFirstName() + " " + userRepositoryManager.getUser().getProfileInfo().getLastName());
            report.setEmail(userRepositoryManager.getUser().getCredentials().getEmail());
            report.setMobile(userRepositoryManager.getUser().getCredentials().getMobile());
            report.setDateOfReport(Utility.getCurrentDate());
            report.setTimeOfReport(Utility.getCurrentTime());

            report.setOs("Android");
            report.setAndroidappversion(BuildConfig.VERSION_NAME);
            report.setDevicemodel(Build.MODEL);
            report.setBrand(Build.BRAND);
            report.setOsversion(Build.VERSION.RELEASE);
            report.setManifacture(Build.MANUFACTURER);


            reports.child("" + System.currentTimeMillis()).setValue(report).addOnCompleteListener(task -> {

                if (task.isSuccessful()) {
                    Logger.d("Report", "Created successfully");
                    Utility.showToast(Utility.getContext(), true, "Your report has been sent successfully. Thank you very much for your feedback.");
                } else {
                    task.getException().printStackTrace();
                    Logger.d("Report", "Creation Failed");
                }
                stopSelf();
            });

        } else {

            if (TextUtils.isEmpty(userRepositoryManager.getCredentials("key")))
                return;

            firebaseAuth.signInWithEmailAndPassword(userRepositoryManager.getCredentials("key"),
                    userRepositoryManager.getCredentials("value")).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        sendVideoReportToFirebase(report);
                    } else {
                        task.getException().printStackTrace();
                        Logger.d("SignInWithEmailAndPassword", "Failed");
                    }
                }
            });
        }

    }


}
