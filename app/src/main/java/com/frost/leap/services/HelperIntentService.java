package com.frost.leap.services;

import android.app.IntentService;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.util.Log;

import androidx.annotation.Nullable;

import com.frost.leap.controllers.AppController;
import com.frost.leap.viewmodels.BaseViewModel;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import apprepos.user.UserRepositoryManager;
import apprepos.video.VideoRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by GOKUL KALAGARA on 2019-10-04.
 * <p>
 * Frost
 */
public class HelperIntentService extends IntentService {

//    public HelperIntentService() {
//        super(HelperIntentService.class.getName());
//    }

    public HelperIntentService() {
        super(null);// That was the lacking constructor, we have to discuss on this
    }

    public HelperIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null && intent.getIntExtra(Constants.ACTION_KEY, -1) != -1) {
            switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
                case AppController.ACTION_UPDATE_VIDEO_STREAM:
                    updateVideoStream(intent);
                    break;
                case AppController.ACTION_LOGOUT:
                    doLogout(intent.getStringExtra(Constants.TOKEN));
                    break;

                case AppController.ACTION_UPDATE_DEVICE_DETAILS:
                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(task ->
                            {
                                if (task.isSuccessful()) {
                                    String fcmToken = task.getResult().getToken();
                                    Logger.d(Constants.USER_FCM_TOKEN, "Refreshed token: " + fcmToken);
                                    UserRepositoryManager.getInstance().setFcmToken(fcmToken);
                                    if (Utility.isNetworkAvailable(this))
                                        updateDeviceInfo(intent);
                                }
                            });

                    break;
            }
        }
    }

    // ADD REFRESH TOKEN
    private void updateVideoStream(Intent intent) {
        String studentSubjectId = intent.getStringExtra("StudentSubjectId");
        String unitId = intent.getStringExtra("UnitId");
        String topicId = intent.getStringExtra("TopicId");
        String subTopicId = intent.getStringExtra("SubTopicId");
        long videoCompletion = intent.getLongExtra("VideoCompletion", 0);

        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {

                if (response != null && response.code() == 200) {
                    if (response.body() != null) {
                        Intent broadCastIntent = new Intent();
                        broadCastIntent.putExtra("Update", true);
                        broadCastIntent.putExtra("StudentSubjectId", intent.getStringExtra("StudentSubjectId"));
                        broadCastIntent.putExtra("UnitId", intent.getStringExtra("UnitId"));
                        broadCastIntent.putExtra("SubjectCompletionPercentage", response.body().get("subjectCompletionPercentage").getAsDouble());
                        broadCastIntent.putExtra("UnitCompletionPercentage", response.body().get("unitCompletionPercentage").getAsDouble());
                        broadCastIntent.setAction(Constants.ACTION_UPDATE_SUBJECT_PROGRESS);
                        sendBroadcast(broadCastIntent);

                        broadCastIntent = new Intent();
                        broadCastIntent.putExtra("Update", true);
                        broadCastIntent.putExtra("TopicId", intent.getStringExtra("TopicId"));
                        broadCastIntent.putExtra("SubTopicId", intent.getStringExtra("SubTopicId"));
                        broadCastIntent.putExtra("TopicCompletionPercentage", response.body().get("topicCompletionPercentage").getAsDouble());
                        broadCastIntent.putExtra("SubTopicCompletionPercentage", response.body().get("subTopicCompletionPercentage").getAsDouble());
                        broadCastIntent.setAction(Constants.ACTION_UPDATE_TOPIC_PROGRESS);
                        sendBroadcast(broadCastIntent);
                        Logger.d("VIDEO STREAM UPDATED");
                    } else {
                        Logger.d("VIDEO STREAM UPDATION IS FAILED");
                    }

                } else if (response != null && response.code() == 401) {
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            updateVideoStream(intent);
                        }
                    });

                } else {

                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };


        VideoRepositoryManager videoRepositoryManager = VideoRepositoryManager.getInstance();

        videoRepositoryManager.updateVideoStream(studentSubjectId, unitId, topicId, videoCompletion, subTopicId)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    private void doLogout(String authorization) {
        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 201 || response.code() == 200) {
                    Logger.d("Logout operation done!");
                } else {
                    Logger.d("Logout operation failed");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };


        UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();

        userRepositoryManager.doLogout(authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }

    private void updateDeviceInfo(Intent intent) {


        Observer<Response<JsonObject>> observer = new Observer<Response<JsonObject>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<JsonObject> response) {
                if (response.code() == 200) {
//                    if (response.body().get("upToDate").getAsBoolean()) {
//                        return;
//                    } else {
//                        ResultReceiver resultReceiver = intent.getParcelableExtra(Constants.RECEIVER);
//                        if (resultReceiver == null)
//                            return;
//
//                        Bundle bundle = new Bundle();
//                        bundle.putBoolean("IsCancel", response.body().get("appUpdateType").getAsString().equals("SOFT") ? true : false);
//                        resultReceiver.send(Utility.generateRequestCodes().get("UPDATE_DEVICE_DETAILS"), bundle);
//                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };


        UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();

        userRepositoryManager.updateDeviceDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }


    private void refreshToken(BaseViewModel.IRefreshToken iRefreshToken) {

        UserRepositoryManager userRepositoryManager = UserRepositoryManager.getInstance();
        userRepositoryManager.refreshToken()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if (response.code() == 200) {
                            userRepositoryManager.saveUserInfo(response.headers().get(Constants.AUTHORIZATION),
                                    response.body());
                            iRefreshToken.isTokenRefreshed(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
