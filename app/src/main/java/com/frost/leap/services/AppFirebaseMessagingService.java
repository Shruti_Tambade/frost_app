package com.frost.leap.services;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.frost.leap.R;
import com.frost.leap.activities.SplashActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import apprepos.user.UserRepositoryManager;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;


public class AppFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "NOTIFICATION DATA";
    private UserRepositoryManager userRepositoryManager;


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Logger.d(Constants.USER_FCM_TOKEN, "Refreshed token: " + token);
        if (token != null && token.length() > 10) {
            try {
                userRepositoryManager = UserRepositoryManager.getInstance();
                userRepositoryManager.setFcmToken(token);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.d(TAG, e.getMessage());
            }
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Logger.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logger.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        try {

            String content = remoteMessage.getData().get("message");
            String bitmapUrl = remoteMessage.getData().get("url");
            String title = remoteMessage.getData().get("title");
            String bigText = remoteMessage.getData().get("bigText");
            String summary = remoteMessage.getData().get("summary");
            String color = remoteMessage.getData().get("color");
            String allowBig = remoteMessage.getData().get("bigAllowIcon");
            String bigIconUrl = remoteMessage.getData().get("bigIconUrl");
            String notificationId = remoteMessage.getData().get("notificationId");
            String optionType = remoteMessage.getData().get("type");
            sendNotification(content, bitmapUrl, title, bigText, summary, color, allowBig, bigIconUrl, notificationId, optionType);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sendNotification(String messageBody, String bitmapUrl, String title, String bigText, String summary, String color, String allowBig, String bigIconUrl, String notificationId, String optionType) {


        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("notify", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "ILEAP_NOTIFY_CHANNEL")
                .setTicker(title).setWhen(0)
                .setSmallIcon(R.drawable.deep_logo)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (title != null) {
            notificationBuilder.setContentTitle(title);
        } else {
            notificationBuilder.setContentTitle(Constants.APP_NAME);
        }
        if (color != null) {
            try {
                notificationBuilder.setColor(Color.parseColor(color));
            } catch (Exception e) {
                e.printStackTrace();
                notificationBuilder.setColor(Color.parseColor("#000000"));
            }
        } else {
            notificationBuilder.setColor(Color.parseColor("#000000"));
        }

        if (summary != null)
            bigPictureStyle.setSummaryText(messageBody);
        if (bigText != null) {
            bigPictureStyle.setBigContentTitle(bigText);
        } else {

        }
        if (bitmapUrl != null && bitmapUrl.length() > 0) {
            Bitmap bitmap1 = Utility.urlToBitMap(bitmapUrl);
            if (bitmap1 != null) {
                bigPictureStyle.bigPicture(bitmap1);
                notificationBuilder.setStyle(bigPictureStyle);

            }
        } else {
            //notificationBuilder.setStyle(bigPictureStyle);
        }
        if (allowBig != null) {
            if (allowBig.equals("1")) {
                if (bigIconUrl != null && bigIconUrl.trim().length() > 0) {
                    try {
                        notificationBuilder.setLargeIcon(Utility.getRoundBitmap(Utility.urlToBitMap(bigIconUrl)));
                    } catch (Exception e) {
                        e.printStackTrace();
                        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.deep_logo));
                    }
                } else
                    notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.deep_logo));
            } else {

            }
        } else {

        }
        notificationBuilder.setChannelId("ILEAP_NOTIFY_CHANNEL");

        if (notificationId != null) {
            try {
                int notificationNumber = Integer.parseInt(notificationId);
                if (notificationNumber > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationManager oreoNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
                        oreoNotificationManager.createNotificationChannel(notificationChannel);
                        oreoNotificationManager.notify(notificationNumber, notificationBuilder.build());
                    } else {
                        notificationManager.notify(notificationNumber, notificationBuilder.build());
                    }
                }
            } catch (Exception e) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager oreoNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationChannel notificationChannel = new NotificationChannel("ILEAP_NOTIFY_CHANNEL", Constants.APP_NAME, NotificationManager.IMPORTANCE_HIGH);
                    oreoNotificationManager.createNotificationChannel(notificationChannel);
                    oreoNotificationManager.notify(new Random().nextInt(99999), notificationBuilder.build());
                } else {
                    notificationManager.notify(new Random().nextInt(99999), notificationBuilder.build());
                }
            }


        } else {
            notificationManager.notify(0, notificationBuilder.build());
        }
    }


}

