package com.frost.leap.services.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Report implements Parcelable {
    private String name;
    private String email;
    private String mobile;
    private String course;
    private String subject;
    private String unit;
    private String topic;
    private String subTopic;
    private String issue;
    private String dateOfReport;
    private String timeOfReport;

    private String os;
    private String androidappversion;
    private String devicemodel;
    private String brand;
    private String osversion;
    private String manifacture;

    public Report() {

    }

    protected Report(Parcel in) {
        name = in.readString();
        email = in.readString();
        mobile = in.readString();
        course = in.readString();
        subject = in.readString();
        unit = in.readString();
        topic = in.readString();
        subTopic = in.readString();
        issue = in.readString();
        dateOfReport = in.readString();
        timeOfReport = in.readString();
        os = in.readString();
        androidappversion = in.readString();
        devicemodel = in.readString();
        brand = in.readString();
        osversion = in.readString();
        manifacture = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(mobile);
        dest.writeString(course);
        dest.writeString(subject);
        dest.writeString(unit);
        dest.writeString(topic);
        dest.writeString(subTopic);
        dest.writeString(issue);
        dest.writeString(dateOfReport);
        dest.writeString(timeOfReport);
        dest.writeString(os);
        dest.writeString(androidappversion);
        dest.writeString(devicemodel);
        dest.writeString(brand);
        dest.writeString(osversion);
        dest.writeString(manifacture);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getSubTopic() {
        return subTopic;
    }

    public void setSubTopic(String subTopic) {
        this.subTopic = subTopic;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getDateOfReport() {
        return dateOfReport;
    }

    public void setDateOfReport(String dateOfReport) {
        this.dateOfReport = dateOfReport;
    }

    public String getTimeOfReport() {
        return timeOfReport;
    }

    public void setTimeOfReport(String timeOfReport) {
        this.timeOfReport = timeOfReport;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getAndroidappversion() {
        return androidappversion;
    }

    public void setAndroidappversion(String androidappversion) {
        this.androidappversion = androidappversion;
    }

    public String getDevicemodel() {
        return devicemodel;
    }

    public void setDevicemodel(String devicemodel) {
        this.devicemodel = devicemodel;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getManifacture() {
        return manifacture;
    }

    public void setManifacture(String manifacture) {
        this.manifacture = manifacture;
    }
}
