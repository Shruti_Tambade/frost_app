package com.frost.leap.services;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.frost.leap.controllers.AppController;

import apprepos.subject.NoteRepositoryManager;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;

/**
 * Created by Gokul Kalagara (Mr. Pyscho) on 04-01-2020.
 * <p>
 * FROST
 */
public class ActionIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public ActionIntentService() {
        super(ActionIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null)
            return;

        switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
            case AppController.ACTION_DELETE_NOTE:
                deleteNote(intent);
                break;
            case AppController.ACTION_UPDATE_NOTE:
                updateNote(intent);
                break;
            case AppController.ACTION_CREATE_NOTE:
                createNote(intent);
                break;
        }


    }


    private void createNote(Intent intent) {
        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 200) {
                    Logger.d("Note created successfully!");
                    Intent broadCastIntent = new Intent();
                    broadCastIntent.putExtra("Refresh", true);
                    broadCastIntent.setAction(Constants.ACTION_UPDATE_NOTES);
                    sendBroadcast(broadCastIntent);

                } else {
                    Logger.d("Note not created");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        NoteRepositoryManager noteRepositoryManager = NoteRepositoryManager.getInstance();
        noteRepositoryManager.createnote(intent.getParcelableExtra("Note"))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);
    }

    private void deleteNote(Intent intent) {
        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 200) {
                    Logger.d("Note deleted successfully!");
                } else {
                    Logger.d("Note not deleted");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        NoteRepositoryManager noteRepositoryManager = NoteRepositoryManager.getInstance();
        noteRepositoryManager.deleteNote(intent.getStringExtra("NoteId"))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }

    private void updateNote(Intent intent) {
        Observer<Response> observer = new Observer<Response>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response response) {
                if (response.code() == 200) {
                    Logger.d("Note updated successfully!");
                } else {
                    Logger.d("Note not updated");
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        NoteRepositoryManager noteRepositoryManager = NoteRepositoryManager.getInstance();
        noteRepositoryManager.updateNote(intent.getParcelableExtra("Note"))
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(observer);

    }
}
