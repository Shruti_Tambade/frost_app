package com.frost.leap.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;

import androidx.annotation.Nullable;

import com.frost.leap.controllers.AppController;
import com.frost.leap.viewmodels.BaseViewModel;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import apprepos.user.UserRepositoryManager;
import apprepos.video.VideoRepositoryManager;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.model.video.DashUrlsModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import supporters.constants.Constants;
import supporters.utils.Logger;
import supporters.utils.Utility;

/**
 * Created by Chenna Rao on 10/22/2019.
 */
public class PushIntentService extends IntentService {

    private UserRepositoryManager userRepositoryManager;
    private VideoRepositoryManager videoRepositoryManager;
    private DashUrlDAO dashUrlDAO;


    public PushIntentService() {
        super(PushIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null && intent.getIntExtra(Constants.ACTION_KEY, -1) != -1) {
            switch (intent.getIntExtra(Constants.ACTION_KEY, -1)) {
                case AppController.ACTION_GET_VIDEO_URLS:
                    getPageContentIdUrls(intent.getStringArrayListExtra("VIDEO_URLS"), intent);
                    break;
            }
        }
    }

    private void getPageContentIdUrls(ArrayList<String> pageContentIds, Intent intent) {

        Observer<Response<List<DashUrlsModel>>> observer = new Observer<Response<List<DashUrlsModel>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Response<List<DashUrlsModel>> response) {
                if (response != null && response.code() == 200) {

                    if (response.body() == null) {
                        return;
                    }

                    dashUrlDAO = videoRepositoryManager.getDeepBaseProvider().dashUrlDAO();
                    try {
                        List<DashUrlsModel> list = dashUrlDAO.getDashUrlsBasedOnPageContentIds(pageContentIds);
                        if (list != null && list.size() > 0) {
                            List<DashUrlsModel> newList = response.body();
                            for (int i = 0; i < newList.size(); i++) {
                                for (int j = 0; j < list.size(); j++) {
                                    if (newList.get(i).getPageContentId().equals(list.get(j).getPageContentId())
                                            && newList.get(i).getDashUrl().equals(list.get(j).getDashUrl())
                                            && list.get(j).getIsDownloaded() == 1) {
                                        newList.get(i).setLicense(list.get(j).getLicense());
                                        newList.get(i).setTimeLimit(list.get(j).getTimeLimit());
                                        newList.get(i).setManifestData(list.get(j).getManifestData());
                                        newList.get(i).setIsDownloaded(list.get(j).getIsDownloaded());
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dashUrlDAO.insertDashModel(response.body());
                    ResultReceiver mReceiver = intent.getParcelableExtra(Constants.RECEIVER);
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("PageContentIds", pageContentIds);
                    mReceiver.send(1, bundle);

                    Logger.d("PAGE CONTENT IDS", "UPDATED");
                } else if (response.code() == 500) {
                    Logger.d("ERROR", Utility.showErrorMessage(response.errorBody()));
                } else if (response.code() == 401) {
                    Logger.d("USER PROFILE", "TOKEN EXIPRED");
                    refreshToken(isRefresh -> {
                        if (isRefresh) {
                            getPageContentIdUrls(pageContentIds, intent);
                        }
                    });
                } else {
                    Logger.d("ERROR", Constants.SOMETHING_WENT_WRONG);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };


        if (videoRepositoryManager == null)
            videoRepositoryManager = VideoRepositoryManager.getInstance();

        videoRepositoryManager.updateVideoUrls(pageContentIds)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(observer);
    }


    private void refreshToken(BaseViewModel.IRefreshToken iRefreshToken) {
        if (userRepositoryManager == null)
            userRepositoryManager = UserRepositoryManager.getInstance();
        userRepositoryManager.refreshToken()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Response<JsonObject>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<JsonObject> response) {
                        if (response.code() == 200) {
                            userRepositoryManager.saveUserInfo(response.headers().get(Constants.AUTHORIZATION),
                                    response.body());
                            iRefreshToken.isTokenRefreshed(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
