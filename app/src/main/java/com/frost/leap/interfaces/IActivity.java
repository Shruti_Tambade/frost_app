package com.frost.leap.interfaces;

import android.app.Activity;

/**
 * Created by Gokul Kalagara on 11/08/2019.
 */

public interface IActivity
{
    public void changeTitle(String title);
    public void showSnackBar(String snackBarText, int type);
    public Activity activity();
}
