package com.frost.leap.interfaces;

/**
 * Created by Gokul Kalagara on 11/08/2019.
 */

public interface ICustomAlertDialog
{
    public void doPositiveAction(int id);
    public void doNegativeAction();
}
