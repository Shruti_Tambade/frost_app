package com.frost.leap.interfaces;

import java.util.List;

/**
 * Created by Gokul Kalagara on 11/08/2019.
 */

public interface IRecyclerViewAdapter
{
    public void loadMore(List<Object> subList);
    public void addBottomLoader();
    public void addRetryOption();
    public void onRetry();
    public void donePagination();
}
