package com.frost.leap.interfaces;

import android.app.Activity;

/**
 * Created by Chenna Rao on 08/14/2019.
 */
public interface IFragment {
    public void changeTitle(String title);

    public void showSnackBar(String snackBarText, int type);

    public Activity activity();
}
