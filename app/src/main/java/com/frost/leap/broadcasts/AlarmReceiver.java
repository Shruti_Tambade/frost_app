package com.frost.leap.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;

import com.frost.leap.controllers.AppController;
import com.frost.leap.services.video.LicenseExpiryIntentService;

import supporters.constants.Constants;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent expiryIntent = new Intent(context, LicenseExpiryIntentService.class);
            if (intent.getStringExtra("VideoUrl") != null) {
                expiryIntent.putExtra(Constants.ACTION_KEY, AppController.ACTION_DELETE_VIDEO_AND_LICENSE_EXPIRY);
                expiryIntent.putExtra("VideoUrl", intent.getStringExtra("VideoUrl"));
                expiryIntent.putExtra("PageContentId", intent.getStringExtra("PageContentId"));
            } else {
                expiryIntent.putExtra(Constants.ACTION_KEY, AppController.ACTION_LICENCE_VERIFY);
            }
            ContextCompat.startForegroundService(context, expiryIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
