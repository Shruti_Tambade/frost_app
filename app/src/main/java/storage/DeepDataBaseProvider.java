package storage;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import apprepos.topics.dao.IDownloadSubTopicDAO;
import apprepos.topics.model.DownloadSubTopicModel;
import apprepos.user.dao.ICacheUrlDAO;
import apprepos.user.model.CacheUrlModel;
import apprepos.video.dao.DashUrlDAO;
import apprepos.video.dao.IOfflineProgressDAO;
import apprepos.video.model.video.DashUrlsModel;
import apprepos.video.model.video.OfflineProgressModel;
import supporters.constants.Constants;

/**
 * Created by Chenna Rao on 10/22/2019.
 */
@Database(entities = {DashUrlsModel.class, DownloadSubTopicModel.class, OfflineProgressModel.class, CacheUrlModel.class},
        version = 8, exportSchema = false)
public abstract class DeepDataBaseProvider extends RoomDatabase {
    private static DeepDataBaseProvider INSTANCE;

    public static DeepDataBaseProvider getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (DeepDataBaseProvider.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DeepDataBaseProvider.class, Constants.APP_DATABASE)
                            .addMigrations(MIGRATION_1_4, MIGRATION_2_4,
                                    MIGRATION_3_4, MIGRATION_4_5,
                                    MIGRATION_5_6, MIGRATION_6_7,
                                    MIGRATION_7_8)
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    static final Migration MIGRATION_1_4 = new Migration(1, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE download_subtopic (catalogueId TEXT NOT NULL, subjectId TEXT NOT NULL, unitId TEXT NOT NULL, topicId TEXT NOT NULL, subTopicId TEXT NOT NULL, pageContentId TEXT NOT NULL, videoUrl TEXT NOT NULL, position INTEGER NOT NULL default 0,  PRIMARY KEY(pageContentId))");
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN size INTEGER NOT NULL default 0");
        }
    };

    static final Migration MIGRATION_2_4 = new Migration(2, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN size INTEGER NOT NULL default 0");
            database.execSQL("ALTER TABLE download_subtopic " + " ADD COLUMN position INTEGER NOT NULL default 0");
        }
    };

    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN size INTEGER NOT NULL default 0");
        }
    };

    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN timeLimit INTEGER NOT NULL default 0");
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN license TEXT");
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN manifestData TEXT");
        }
    };


    static final Migration MIGRATION_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE offline_progress(studentSubTopicId TEXT NOT NULL, " +
                    "studentTopicId TEXT NOT NULL, " +
                    "studentUnitId TEXT NOT NULL, " +
                    "studentSubjectId TEXT NOT NULL," +
                    "videoCompletion DOUBLE NOT NULL default 0,  " +
                    "PRIMARY KEY(studentSubTopicId))");

        }
    };

    static final Migration MIGRATION_6_7 = new Migration(6, 7) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE cache_url(" +
                    "tag TEXT NOT NULL, " +
                    "data TEXT NOT NULL, " +
                    "PRIMARY KEY(tag))");

        }
    };

    static final Migration MIGRATION_7_8 = new Migration(7, 8) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE dash_urls " + " ADD COLUMN isDownloaded INTEGER NOT NULL default 0");
        }
    };


    public abstract DashUrlDAO dashUrlDAO();

    public abstract IDownloadSubTopicDAO getDownloadSubTopicDAO();

    public abstract IOfflineProgressDAO getOfflineProgressDAO();

    public abstract ICacheUrlDAO getCacheUrlDAO();


}
