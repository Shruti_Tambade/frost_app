package storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

import supporters.constants.Constants;
import supporters.utils.Utility;


public class AppSharedPreferences {


    private SharedPreferences sharedPreferences;
    private static volatile AppSharedPreferences appSharedPreferences;

    private AppSharedPreferences() {
    }

    public static AppSharedPreferences getInstance() {
        if (appSharedPreferences == null) {
            synchronized (AppSharedPreferences.class) {
                appSharedPreferences = new AppSharedPreferences();
            }
        }
        return appSharedPreferences;
    }


    public void setContext(Context context) {
        if (context == null)
            return;
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
    }

    private SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null)
            sharedPreferences = Utility.getContext().getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE);
        return sharedPreferences;
    }


    public boolean contains(String key) {
        return getSharedPreferences().contains(key);
    }

    public void setString(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public String getString(String key) {
        return getSharedPreferences().getString(key, null);
    }

    public void setInt(String key, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.apply();
        editor.commit();
    }

    public int getInt(String key) {
        return getSharedPreferences().getInt(key, 0);
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.apply();
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return getSharedPreferences().getBoolean(key, false);
    }

    public void delete(String key) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
    }

    public void setKeySet(String key, Set<String> set) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putStringSet(key, set);
        editor.apply();
        editor.commit();
    }

    public Set<String> getKeySet(String key) {
        return getSharedPreferences().getStringSet(key, null);
    }

    public void deleteSharedPreferences() {
        String fcmToken = null;
        String securityLevel = null;
        if (contains(Constants.USER_FCM_TOKEN)) {
            fcmToken = getString(Constants.USER_FCM_TOKEN);
        }
        if (contains(Constants.DRM_SECURITY_LEVEL)) {
            securityLevel = getString(Constants.DRM_SECURITY_LEVEL);
        }
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear().apply();
        editor.commit();
        if (fcmToken != null)
            setString(Constants.USER_FCM_TOKEN, fcmToken);
        if (securityLevel != null)
            setString(Constants.DRM_SECURITY_LEVEL, fcmToken);
    }

    public Set<String> getAllKeySet() {
        return getSharedPreferences().getAll().keySet();
    }

}
