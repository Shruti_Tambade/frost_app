package network;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import network.apis.retrofit2.AppRetrofitAdapter;
import retrofit2.Retrofit;


public class NetworkManager {

    /*
     *   This methods are used to control the retrofit
     */


    private static volatile NetworkManager networkManager;

    private NetworkManager() {
    }

    public static NetworkManager getInstance() {
        if (networkManager == null) {
            synchronized (NetworkManager.class) {
                networkManager = new NetworkManager();
            }
        }
        return networkManager;
    }

    public Retrofit getRetrofit() {
        return AppRetrofitAdapter.getRetrofit();
    }

    public Retrofit getRetrofitByBaseUrl(String baseUrl) {
        return AppRetrofitAdapter.getRetrofitByBaseUrl(baseUrl);
    }

    public Object getBufferedReaderByURL(String url) {
        try {
            URL u = new URL(url);
            URLConnection conn = u.openConnection();
            return new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
            return e;
        }
    }
}
