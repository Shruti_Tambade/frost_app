package network.apis.retrofit2;


import com.frost.leap.BuildConfig;
import com.frost.leap.R;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import supporters.constants.Constants;
import supporters.utils.Utility;


public class AppRetrofitAdapter {

    /*
     *   To send network requests to an API, we need to use the Retrofit Builder class and specify the base URL for the service.
     *   So, create a class named AppRetrofitAdapter.java .

     *   Here BASE_URL – it is basic URL of our API. We will use this URL for all requests later.
     */

    private static volatile Retrofit retrofit;
    private static RetrofitNetworkLogger.Level logLevel = BuildConfig.DEBUG ? RetrofitNetworkLogger.Level.BODY : RetrofitNetworkLogger.Level.NONE;

    public static Retrofit getRetrofit() {

        if (retrofit == null) {
            synchronized (AppRetrofitAdapter.class) {
                return retrofit = (new Retrofit.Builder().client(getClient())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl(Constants.BASE_URL)
                        .build());
            }
        }
        return retrofit;


    }

    /**
     * @param baseUrl
     * @return
     */
    public static Retrofit getRetrofitByBaseUrl(String baseUrl) {

        return (new Retrofit.Builder().client(getUnsafeOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build());
    }


    /*
     *   you can use an interceptor for this.
     *   An interceptor is used to modify each request before it is performed and alters the request loggers
     */
    private static OkHttpClient getClient() {
        RetrofitNetworkLogger logging = new RetrofitNetworkLogger();
        // set your desired log logLevel -> NONE
        logging.setLevel(logLevel);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.sslSocketFactory(newSslSocketFactory(),getSystemDefaultTrustManager());
        builder.addInterceptor(logging)
                .connectTimeout(Constants.RETROFIT_CONNECTION_TIME_OUT, TimeUnit.MINUTES)
                .readTimeout(Constants.RETROFIT_CONNECTION_TIME_OUT, TimeUnit.MINUTES)
                .build();

        OkHttpClient okHttpClient = builder.build();
        return okHttpClient;
    }


    public static SSLSocketFactory newSslSocketFactory() {
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs)
            InputStream in = Utility.getContext().getResources().openRawResource(
                    Constants.environment == Constants.Environment.TEST ? R.raw.ileapssl_test :
                            (Constants.environment == Constants.Environment.DEV ? R.raw.ileapssl_dev :
                                    R.raw.ileapssl_prod));
            try {
                // Initialize the keystore with the provided trusted certificates
                // Provide the password of the keystore
                trusted.load(in, Constants.KEYSTORE_PASSWORD.toCharArray());
            } finally {
                in.close();
            }

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(trusted);

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

            SSLSocketFactory sf = context.getSocketFactory();
            return sf;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AssertionError(e);
        }
    }


    private static X509TrustManager getSystemDefaultTrustManager() {
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:"
                        + Arrays.toString(trustManagers));
            }
            return (X509TrustManager) trustManagers[0];
        } catch (GeneralSecurityException e) {
            throw new AssertionError(); // The system has no TLS. Just give up.
        }
    }


    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager


            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(new TLSSocketFactory(), (X509TrustManager) trustAllCerts[0]);

            RetrofitNetworkLogger logging = new RetrofitNetworkLogger();
            // set your desired log logLevel -> NONE
            logging.setLevel(logLevel);

            builder.addInterceptor(logging)
                    .connectTimeout(Constants.RETROFIT_CONNECTION_TIME_OUT, TimeUnit.MINUTES)
                    .readTimeout(Constants.RETROFIT_CONNECTION_TIME_OUT, TimeUnit.MINUTES)
                    .build();

            OkHttpClient okHttpClient = builder.build();

            return okHttpClient;
        } catch (Exception e) {
            e.printStackTrace();
            return getClient();
        }
    }
}
