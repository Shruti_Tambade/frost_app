package com.frost.leap;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import supporters.utils.Utility;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented ic_thumb, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4ClassRunner.class)
public class ExampleInstrumentedTest {


    @Test
    public void useAppContext() {
        // Context of the app under ic_thumb.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.frost.leap", appContext.getPackageName());
    }




}
