package com.frost.leap.espresso.start;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import com.frost.leap.R;
import com.frost.leap.activities.SplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by Chenna Rao on 11/09/20.
 * <p>
 * Frost Interactive
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SignupUITest {

    @Rule
    public ActivityScenarioRule<SplashActivity> activityRule
            = new ActivityScenarioRule<>(SplashActivity.class);


    @Test
    public void firstNameTextFieldTesting() {
        onView(withId(R.id.etPhoneNumber))
                .perform(click())
                .check(matches(isDisplayed()));
    }

    @Test
    public void lastNameTextFieldTesting() {
        onView(withId(R.id.etPhoneNumber))
                .perform(click())
                .check(matches(isDisplayed()));
    }

    @Test
    public void emailTextFieldTesting() {
        onView(withId(R.id.etPhoneNumber))
                .perform(click())
                .check(matches(isDisplayed()));
    }

    @Test
    public void passwordTextFieldTesting() {
        onView(withId(R.id.etPhoneNumber))
                .perform(click())
                .check(matches(isDisplayed()));
    }

    @Test
    public void confirmPasswordTextFieldTesting() {
        onView(withId(R.id.etPhoneNumber))
                .perform(click())
                .check(matches(isDisplayed()));
    }

    @Test
    public void mobileNumberTextFieldTesting() {
        onView(withId(R.id.etPhoneNumber))
                .perform(click())
                .check(matches(isDisplayed()));
    }


}
