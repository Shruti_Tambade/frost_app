package com.frost.leap.viewmodels;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.filters.MediumTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.frost.leap.viewmodels.start.StartViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import supporters.utils.Utility;

/**
 * Created by Gokul Kalagara (Mr. Psycho) on 31-08-2020.
 * <p>
 * Frost
 */

@RunWith(AndroidJUnit4ClassRunner.class)
@MediumTest
public class StartViewModelTest1 {

    StartViewModel startViewModel;

    @Before
    public void setUp() {
        startViewModel = new StartViewModel();
    }

    @Test
    public void checkMobileNumber_isNull() {
        String mobileNumber = null;
        boolean expectedResult = false;
        boolean actualResult = startViewModel.doMobileValidation(mobileNumber);
        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void checkMobileNumber_isString() {
        String mobileNumber = "";
        boolean expectedResult = false;
        boolean actualResult = startViewModel.doMobileValidation(mobileNumber);
        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void checkMobileNumber_isInvalid() {
        String mobileNumber = "72078";
        boolean expectedResult = false;
        boolean actualResult = startViewModel.doMobileValidation(mobileNumber);
        Assert.assertEquals(actualResult, expectedResult);
    }


    @Test
    public void checkMobileNumber_isValid() {
        String mobileNumber = "1234567890";
        boolean expectedResult = true;
        boolean actualResult = startViewModel.doMobileValidation(mobileNumber);
        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void requestLogin_isNotInternet() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Assert.assertFalse(Utility.isNetworkAvailable(appContext));
    }



}
